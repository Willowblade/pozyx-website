<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * European vat validation
 * 
 * @author Manuel João Silva mail[at]manueljoaosilva.com
 * @version 1.0
 * @uses http://codeigniter.com/wiki/Curl_library/
 *
 */
class Vat_checker
{
    var $CI;

    /**
     * Contructor: Loads curl library and vat_checker config
     *
     * @return void
     */
    function Vat_checker()
    {
        $this->CI =& get_instance();
        $this->CI->load->library('curl');        
    }
      

    /**
     * Validates against http://ec.europa.eu/taxation_customs/vies/ 
     * if vat number is valid. If it's valid 
     * returns true or false
     *
     * @param string $vat
     * @param string $country_iso
     * @return string or boolean
     */
    function valid_EU_VAT($vat, $country_iso)
    {
        $country_iso = strtoupper($country_iso);
        $vat = str_replace($country_iso, '', $vat);

        $client = new SoapClient("http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl");
        $result = $client->checkVat(array(
          'countryCode' => $country_iso,
          'vatNumber' => $vat
        ));

        // check out all the extra information we can get!
        //echo($result->name);
        //echo($result->address);
        //var_dump($result);

        return $result->valid;
    }

    /**
     * Based on rules in http://ec.europa.eu/taxation_customs/vies/faqvies.do
     * check if nif is valid
     *
     * @param string $vat
     * @param string $country_iso
     * @return boolean
     */

    function is_valid_regex($vat, $country_iso)
    {

        $country_iso = strtoupper($country_iso);
        $regex = '';

        switch ($country_iso)
        {
            case 'AT':
                $regex = '/^U[0-9]{8}$/';
                break;
            case 'BE':
                $regex = '/^0?[0-9]{*}$/';
                break;
            case 'CZ':
                $regex = '/^[0-9]{8,10}$/';
                break;
            case 'DE':
                $regex = '/^[0-9]{9}$/';
                break;
            case 'CY':
                $regex = '/^[0-9]{8}[A-Z]$/';
                break;
            case 'DK':
                $regex = '/^[0-9]{8}$/';
                break;
            case 'EE':
                $regex = '/^[0-9]{9}$/';
                break;
            case 'GR':
                $regex = '/^[0-9]{9}$/';
                break;
            case 'ES':
                $regex = '/^[0-9A-Z][0-9]{7}[0-9A-Z]$/';
                break;
            case 'FI':
                $regex = '/^[0-9]{8}$/';
                break;
            case 'FR':
                $regex = '/^[0-9A-Z]{2}[0-9]{9}$/';
                break;
            case 'GB':
                $regex = '/^([0-9]{9}|[0-9]{12})~(GD|HA)[0-9]{3}$/';
                break;
            case 'HU':
                $regex = '/^[0-9]{8}$/';
                break;
            case 'IE':
                $regex = '/^[0-9][A-Z0-9\\+\\*][0-9]{5}[A-Z]$/';
                break;
            case 'IT':
                $regex = '/^[0-9]{11}$/';
                break;
            case 'LT':
                $regex = '/^([0-9]{9}|[0-9]{12})$/';
                break;
            case 'LU':
                $regex = '/^[0-9]{8}$/';
                break;
            case 'LV':
                $regex = '/^[0-9]{11}$/';
                break;
            case 'MT':
                $regex = '/^[0-9]{8}$/';
                break;
            case 'NL':
                $regex = '/^[0-9]{9}B[0-9]{2}$/';
                break;
            case 'PL':
                $regex = '/^[0-9]{10}$/';
                break;
            case 'PT':
                $regex = '/^[0-9]{9}$/';
                break;
            case 'SE':
                $regex = '/^[0-9]{12}$/';
                break;
            case 'SI':
                $regex = '/^[0-9]{8}$/';
                break;
            case 'SK':
                $regex = '/^[0-9]{10}$/';
                break;
            default:
                return FALSE;
                break;
        }

        $vat = str_replace($country_iso, '', $vat);
        return (preg_match($regex,$vat));
    }

    public function EU_country($countryCode){      
        $EU_countries = array("AT","BE","BG","HR","CY","CZ","DK","EE","FI","FR","DE","EL","HU","IE","IT","LV","LT","LU","MT","NL","PL","PT","RO","SK","SI","ES","SE");
        $extra_territories = array("MC", "IM", "IB", "PT-20", "PT-30");
        $eu_vat_list = array_merge($EU_countries, $extra_territories);

        return in_array ( $countryCode , $eu_vat_list );
    } 
}