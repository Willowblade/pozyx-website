<?php

namespace Wunderlist;

use Exception;

/*
 * This file is part of the Wunderlist PHP class from Jeroen Desloovere.
 *
 * For the full copyright and license information, please view the license
 * file that was distributed with this source code.
 */

/**
 * Wunderlist Exception
 *
 * @author Jeroen Desloovere <info@jeroendesloovere.be>
 */
class WunderlistException extends Exception
{
}
