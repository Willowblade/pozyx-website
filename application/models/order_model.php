<?php
require_once(APPPATH .'libraries/dompdf/dompdf_config.inc.php');
class Order_model extends CI_Model { 
	
	/* Generate a pdf document.
	doc_type can be 'invoice', 'pro forma invoice' or 'quote'
	*/

	/* should be update with duplicate and credit note
		Assumptions:
		Only one possible invoice per order is possible, no splitting of amount in this stage
		Duplicate is always on the last invoice (tobe implemented)
		credit note is always a mirror invoice of the last invoice (not credited)
		TO BE DONE UPDATE of invoice statusses, legaly you can only send the invoice once and only once
	*/
	
	public function generate_pdf_invoice($doc_type, $order_id){

		// load the order data
		$data = $this->get_details($order_id);
		$data["order_items"] = $this->get_items($order_id);
		$data["document_type"] = $doc_type;

		$document_id = "";
		$invoice_dir = "./documents/invoices/";		
		//$invoice_dir = "/home/pozydhjv/public_html/tmp2/documents/invoices/";

		if(empty($data))
			log_message('error', 'generate pdf invoice: incorrect order id.');

		//create the invoice record and add to $data
		if($doc_type == 'invoice'){
			// validate if invoice exists
			$query = $this->db->query("select * from invoices where order_id=" . $order_id . " and invoice_type = 'IN' and invoice_status = 'send'");
			//$query = $this->db->query("select SUM(quantity) as nrTags from purchase_orders where delivery_date<=now() and product_id=4");
			
			$firstrow = $query->first_row('array');
			if(isset($firstrow['id']))	{
				//NORMALLY RETURN DUPLICATE
				//NOW RETURN DE EXISTING LINK
				$document_id = $firstrow['legal_id'];
				$invoice_path = $invoice_dir . str_replace(' ', '_', $doc_type) . '_' . $document_id . '_' . $this->encode_orderID($order_id) . '.pdf';
				return $invoice_path;
			}

			//else create the invoice record
			$query = $this->db->query("select MAX(legal_id) as legal_id from invoices where invoice_type = 'IN' and invoice_status = 'send' and invoice_year = ".date('Y'));
			$firstrow = $query->first_row('array');
			if(!$firstrow['legal_id']){
				$document_id = 'IN' 
						. date('Y') 
						. str_pad(1, 4, '0',STR_PAD_LEFT);
			}
			else{
				$document_id = 'IN' 
							. date('Y') 
							. str_pad(
								intval(substr($firstrow['legal_id'] ,-4))+1, 
							4, '0',STR_PAD_LEFT);
			}
			
			$query = $this->db->query("select vat_code from countries inner join customers on countries.countryCode = customers.country_code where customers.id = " . $data['customer_id']);
			$firstrow = $query->first_row('array');
			$vat_code = $firstrow['vat_code'];

			// Should be calculated on the 50-50 rule
			$vat_type = 'goods';

			$invoice = array(
				'invoice_type' => 'IN',
				'legal_id' => $document_id,
				'order_id' => $order_id,
				'payment_term' => ($data['payment'] == 'completed' ? 'PAID' : 'N15'),
				'invoice_date' => date('Y-m-d'),
				'due_date' => ($data['payment'] == 'completed' ? 0 : date('Y-m-d', time() + (15 * 24 * 60 * 60))),
				'invoice_status' => 'send',
				'invoice_year' => date('Y'),
				'vat_code' => $vat_code,
				'vat_type' => $vat_type
				);
			
		    if(!$this->db->insert('invoices', $invoice)){
		    	return -1;
		    }	

		    $query = $this->db->query("select vat_text from vat_text where vat_code = '" . $vat_code . "' and vat_type = '". $vat_type . "'");
		    $firstrow = $query->first_row('array');
		    $invoice['vat_text'] = $firstrow['vat_text'];
		    $invoice['invoice_date'] = date('d-m-Y',strtotime($invoice['invoice_date']));
		    $invoice['due_date'] = date('d-m-Y',strtotime($invoice['due_date']));
		    $invoice['delivery_date'] = date('d-m-Y',strtotime($invoice['invoice_date']) + (14 * 24 * 60 * 60));
		    $query = $this->db->query("select payment_text from payment_terms where payment_term = '" . $invoice['payment_term'] . "'");
		    $firstrow = $query->first_row('array');
		    $invoice['payment_text'] = $firstrow['payment_text'];

		    $data["invoice_data"] = $invoice;
		}

		//Calculate discount values and tax value
		$tax_amount = $data['grand_total'] / (100 + $data['tax_rate']) * $data['tax_rate'];
		$discount_amount = ($data['grand_total'] - $tax_amount - $data['shipping_cost'] - $data['subtotal']) * -1;

		$data['tax_amount']	= $tax_amount;
		$data['discount_amount'] = $discount_amount;


		//Multiple generations to create bulk invoices
		$view = $this->load->view('templates/pdf/invoice', $data, true);
		$this->pdf = new DOMPDF();
		$this->pdf->load_html($view);

		// issue in code ignitor only one instance possible
		//$this->load->library('pdf');
		//$this->pdf->load_view('templates/pdf/invoice', $data);
		$this->pdf->render();

		// save the pdf to the generated file system
		$output = $this->pdf->output();

		$invoice_path = $invoice_dir . str_replace(' ', '_', $doc_type) . '_' . $document_id . '_' . $this->encode_orderID($order_id) . '.pdf';
    	try{
    		file_put_contents($invoice_path, $output);
    	}catch ( Exception $e ) {
    		log_message('error', 'could not save file: ' . $e->getMessage());
    	}

    	return $invoice_path;

	}

	public function encode_orderID($order_id){

		$key = md5($this->config->item("encryption_key"), true); 

		$id = base_convert($order_id, 10, 36); // Save some space
	    $data = mcrypt_encrypt(MCRYPT_BLOWFISH, $key, $id, 'ecb');
	    $data = bin2hex($data);

		return $data;
	}

	public function decode_orderID($encrypted_order_id){

		$key = md5($this->config->item("encryption_key"), true); 

		$data = pack('H*', $encrypted_order_id); // Translate back to binary
	    $data = mcrypt_decrypt(MCRYPT_BLOWFISH, $key, $data, 'ecb');
	    $data = base_convert($data, 36, 10);

	    return $data;
	}

	/*
	$data: the data from the form
	$cart: an array with the cart contents (every row must have an id and a quantity)
	*/
	public function new_order($data, $cart, $subtotal){

		// is the billing address the same as the shipping address?		
		if($data['billingAddressIsShipping']==1){
			$data['addressLine1'] = $data['billing_addressLine1'];
			$data['addressLine2'] = $data['billing_addressLine2'];
			$data['State'] = $data['billing_State'];
			$data['City'] = $data['billing_City'];
			$data['ZIP'] = $data['billing_ZIP'];
		}else{
			$data['billingAddressIsShipping']=0;
		}

		if(!isset($data["user_remarks"]))
			$data["user_remarks"] = "";

		if(!isset($data["reduction_percent"]) || !$this->ion_auth->logged_in())
			$data["reduction_percent"] = 0;

		// add the customer to the database
	    $customer = array(
		   'firstname' => $data['firstName'],
		   'lastname' => $data['lastName'],
		   'email_address' => $data['email'],		
		   'phone' => $data['tel'],		
		   'country_code' => $data['country'],	
		   'company_name' => $data['companyName'],		      
		   'company_vat' => $data['vatNumber'],	
		   'address_line1' => $data['addressLine1'],
		   'address_line2' => $data['addressLine2'],
		   'state' => $data['State'],	
		   'city' => $data['City'],	
		   'zip' => $data['ZIP'],	
		   'billing_equals_shipping' => $data['billingAddressIsShipping'],
		   'billing_address_line1' => $data['billing_addressLine1'],
		   'billing_address_line2' => $data['billing_addressLine2'],
		   'billing_state' => $data['billing_State'],	
		   'billing_city' => $data['billing_City'],	
		   'billing_zip' => $data['billing_ZIP']
		);	
		$this->db->set('date_created', 'NOW()', FALSE);
	    if(!$this->db->insert('customers', $customer)){
	    	return -1;
	    }	
	    $customerID = $this->db->insert_id();	    

	    if($data["reduction_percent"]=='')
	    	$data["reduction_percent"] = 0.0;

		// add the order to the database		    
		$order = array(
			'customer_id' => $customerID,
	   		'payment' => 'pending' ,
		   	'delivery' => 'pending',	  	
		  	'tax_rate' =>$data["tax_rate"],	
		  	'subtotal' => $subtotal,	
		  	'discount_rate' => $data["reduction_percent"],
		  	'grand_total' => round($data["grand_total"],0),	
		  	'shipping_cost' => $data["shipping_cost"],	
		  	'payment_method' => $data["paymentOption"],
		  	'remarks' => $data["user_remarks"],
		  	'sector' => $data["sector"],
		  	'tracking' => $data["tracking"],
		  	'order_type' => $data['OrderType']	 
		);	
		$this->db->set('date_created', 'NOW()', FALSE);
		if(!$this->db->insert('orders', $order)){
	    	return -1;
	    }		
		$orderID = $this->db->insert_id();
		// update order with encoded_id
		$this->db->query("UPDATE orders SET encoded ='" . $this->encode_orderID($orderID) . "' where id = " . $orderID);

		// store every item from the cart in the database
		foreach ($cart as $items){
			$data = array(
			   'order_id' => $orderID ,
			   'product_id' =>  $items['id'],
			   'product_name' => $items['name'],
			   'quantity' => $items['qty'],
			   'unit_price' => ($items['subtotal']*100/$items['qty'])
			);

			if(!$this->db->insert('baskets', $data)){
				return -1;
			} 			
		} 

		return $orderID;
	}

	public function update_order($data){

		$this->db->where('id', $data["order_id"]);
		$result = $this->db->update('orders', array('plug_type'=>$data["plugtype"], 'user_remarks'=>$data["whatFor"]));

		// get the customer id
		$this->db->select('*');
		$this->db->select('customers.id AS customer_id');		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left');			
		$this->db->where('orders.id', $data["order_id"]);
		$query = $this->db->get();
		$customer_id = $query->row()->customer_id;

		// is the billing address the same as the shipping address?		
		if(isset($data['billingAddressIsShipping']) && $data['billingAddressIsShipping']==1){
			$data['billing_addressLine1'] = $data['addressLine1'];
			$data['billing_addressLine2'] = $data['addressLine2'];
			$data['billing_State'] = $data['State'];
			$data['billing_City'] = $data['City'];
			$data['billing_ZIP'] = $data['ZIP'];
		}else{
			$data['billingAddressIsShipping']=0;
		}

		// update customer information
		$this->db->where('id', $customer_id);
		$customer_update = array(
			'firstname' => $data['firstName'],
		   'lastname' => $data['lastName'],		  
		   'phone' => $data['tel'],	
		   'company_name' => $data['companyName'],		      
		   'company_vat' => $data['vatNumber'],				  
		   'address_line1' => $data['addressLine1'],
		   'address_line2' => $data['addressLine2'],
		   'state' => $data['State'],	
		   'city' => $data['City'],	
		   'zip' => $data['ZIP'],	
		   'billing_equals_shipping' => $data['billingAddressIsShipping'],
		   'billing_address_line1' => $data['billing_addressLine1'],
		   'billing_address_line2' => $data['billing_addressLine2'],
		   'billing_state' => $data['billing_State'],	
		   'billing_city' => $data['billing_City'],	
		   'billing_zip' => $data['billing_ZIP']	
		);
        $result = $this->db->update('customers', $customer_update);
	}

	public function get_details($order_id){
		$this->db->select('*');
		$this->db->select('orders.id AS orderid');
		$this->db->select("DATE_FORMAT(DATE_ADD(orders.date_created, INTERVAL 2 WEEK), '%d/%m/%Y') AS due_date", FALSE);
		$this->db->select("DATE_FORMAT(orders.date_created, '%d/%m/%Y') AS created_at", FALSE);
		$this->db->select("DATEDIFF(NOW(), orders.date_created) AS daysold", FALSE);
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left');
		$this->db->join('countries', 'customers.country_code = countries.countryCode COLLATE utf8_unicode_ci', 'left', FALSE);		
		$this->db->where('orders.id', $order_id);
		$query = $this->db->get();

		$data = $query->row_array();
		$data["encodedOrderID"] = $this->encode_orderID($order_id);

		return $data;
	}

	public function get_orders($offset, $limit){
		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		$this->db->select('orders.id AS orderid');
		$this->db->select("DATE_FORMAT(DATE_ADD(orders.date_created, INTERVAL 2 WEEK), '%d/%m/%Y') AS due_date", FALSE);
		$this->db->select("DATE_FORMAT(orders.date_created, '%d/%m/%Y') AS created_at", FALSE);
		$this->db->select("DATEDIFF(NOW(), orders.date_created) AS daysold", FALSE);
		//$this->db->select('SQL_CALC_FOUND_ROWS ',FALSE);			// so we can count the number of rows
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left');
		$this->db->join('countries', 'customers.country_code = countries.countryCode COLLATE utf8_unicode_ci', 'left', FALSE);	
		$this->db->order_by("orders.date_created", "desc"); 	
		$this->db->limit($limit, $offset); 	
		$this->db->where('deleted', 0); 	
		$query = $this->db->get();

		return $query->result_array();		
	}

	public function get_order_overview(){
		$this->db->select('*');		
		$this->db->select("DATE_FORMAT(date_created, '%b') AS whatmonth", FALSE);
		$this->db->select("COUNT(CASE WHEN payment='completed' THEN 1 END) AS payed_orders", FALSE);
		$this->db->select("SUM(CASE WHEN payment='completed' THEN subtotal*(100-discount_rate)/100 END) AS sum_payed", FALSE);
		$this->db->select("SUM(CASE WHEN payment != 'completed' AND payment='quote' THEN subtotal*(100-discount_rate)/100 END) AS sum_quoted", FALSE);
		$this->db->select("SUM(CASE WHEN payment='completed' THEN subtotal*discount_rate/100 END) AS sum_discount", FALSE);
		$this->db->select("SUM(CASE WHEN payment != 'completed' AND payment_method='wire transfer' THEN subtotal END) AS sum_unpayed", FALSE);
		$this->db->select("SUM(subtotal*(100-discount_rate)/100) AS monthTotal", FALSE);
		$this->db->select("COUNT(id) AS orderTotal", FALSE);
		$this->db->from('orders');
		$this->db->group_by('MONTH(date_created), YEAR(date_created)', FALSE);
		$this->db->where("date_created >= last_day(now()) + interval 1 day - interval 4 month", NULL, FALSE);
		$this->db->order_by("date_created", 'ASC');
		$this->db->where('deleted', 0); 	
		$this->db->where('!(payment_method = "credit card" AND payment="pending")', NULL, FALSE); 	
		$query = $this->db->get();

		return $query->result_array();			

	}

	public function get_order_shipnumber($order_id)
	{
		// get the shipping number of the order

		if(isset($order_id))
		{
			// use a variable for rowcounting
			$this->db->query('SET @row := 0;');

			// select the order from the ordered list of orders
			$query = $this->db->query("SELECT row FROM (SELECT orderid, @row := @row + 1 AS row FROM (SELECT orders.id AS orderid FROM orders WHERE orders.deleted=0 AND orders.delivery='pending' ORDER by orders.is_priority DESC, orders.date_created ASC) AS t) AS s WHERE s.orderid=".$order_id . ";");
			
			if ($query->num_rows() > 0)
			{
				$result = $query->result();			
				return $result[0]->row;
			}else{
				return -1;
			}
		}
	}

	public function get_order_delivery_date($order_id)
	{
		// get the shipping number of the order

		if(isset($order_id))
		{

			// select the order from the ordered list of orders
			$query = $this->db->query("SELECT DATE_FORMAT(date_shipped, '%d-%m-%Y') AS date_shipped FROM orders WHERE id=".$order_id . ";");
			$firstrow = $query->first_row('array');
			return $firstrow['date_shipped'];

		}
	}

	public function get_error_orders($offset, $limit)
	{
		if(!isset($offset) || !isset($limit))
		{
			$offset = 0;
			$limit = 15;
		}

		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		$this->db->select('orders.id AS orderid');
		$this->db->select("DATE_FORMAT(DATE_ADD(orders.date_created, INTERVAL 2 WEEK), '%d/%m/%Y') AS due_date", FALSE);
		$this->db->select("DATE_FORMAT(orders.date_created, '%d/%m/%Y') AS created_at", FALSE);
		$this->db->select("DATEDIFF(NOW(), orders.date_created) AS daysold", FALSE);				
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left');
		$this->db->join('countries', 'customers.country_code = countries.countryCode COLLATE utf8_unicode_ci', 'left', FALSE);	
		$this->db->order_by("orders.is_priority", "desc"); 	
		$this->db->order_by("orders.date_created", "asc"); 	
		$this->db->limit($limit, $offset); 
		$st="deleted=0 AND (orders.is_error = 1 
						OR (orders.payment_method != 'wire transfer'
							AND orders.payment = 'pending'
							AND orders.delivery = 'pending')
						)";
  		$this->db->where($st, NULL, FALSE); 	
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_shipping_orders($offset, $limit)
	{
		if(!isset($offset) || !isset($limit))
		{
			$offset = 0;
			$limit = 15;
		}

		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		$this->db->select('orders.id AS orderid');
		$this->db->select("DATE_FORMAT(DATE_ADD(orders.date_created, INTERVAL 2 WEEK), '%d/%m/%Y') AS due_date", FALSE);
		$this->db->select("DATE_FORMAT(orders.date_created, '%d/%m/%Y') AS created_at", FALSE);
		$this->db->select("DATEDIFF(NOW(), orders.date_created) AS daysold", FALSE);				
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left');
		$this->db->join('countries', 'customers.country_code = countries.countryCode COLLATE utf8_unicode_ci', 'left', FALSE);	
		$this->db->order_by("orders.is_priority", "desc"); 	
		$this->db->order_by("orders.date_created", "asc"); 	
		$this->db->limit($limit, $offset); 	
		$this->db->where('deleted', 0); 	
		$this->db->where('delivery', 'pending');
		$this->db->where('is_error',0);
		$this->db->where('payment', 'completed'); 		

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_unpaid_shipped_orders($offset, $limit)
	{
		if(!isset($offset) || !isset($limit))
		{
			$offset = 0;
			$limit = 15;
		}

		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		$this->db->select('orders.id AS orderid');
		$this->db->select("DATE_FORMAT(DATE_ADD(orders.date_created, INTERVAL 2 WEEK), '%d/%m/%Y') AS due_date", FALSE);
		$this->db->select("DATE_FORMAT(orders.date_created, '%d/%m/%Y') AS created_at", FALSE);
		$this->db->select("DATEDIFF(NOW(), orders.date_created) AS daysold", FALSE);				
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left');
		$this->db->join('countries', 'customers.country_code = countries.countryCode COLLATE utf8_unicode_ci', 'left', FALSE);	
		$this->db->order_by("orders.is_priority", "desc"); 	
		$this->db->order_by("orders.date_created", "asc"); 	
		$this->db->limit($limit, $offset); 
		$this->db->where('deleted', 0); 	
		$this->db->where('is_error',0);
		$this->db->where('delivery', 'shipped');	
		$this->db->where('payment !=', 'completed');  

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_unpaid_orders($offset, $limit)
	{
		if(!isset($offset) || !isset($limit))
		{
			$offset = 0;
			$limit = 15;
		}

		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		$this->db->select('orders.id AS orderid');
		$this->db->select("DATE_FORMAT(DATE_ADD(orders.date_created, INTERVAL 2 WEEK), '%d/%m/%Y') AS due_date", FALSE);
		$this->db->select("DATE_FORMAT(orders.date_created, '%d/%m/%Y') AS created_at", FALSE);
		$this->db->select("DATEDIFF(NOW(), orders.date_created) AS daysold", FALSE);				
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left');
		$this->db->join('countries', 'customers.country_code = countries.countryCode COLLATE utf8_unicode_ci', 'left', FALSE);	
		$this->db->order_by("orders.is_priority", "desc"); 	
		$this->db->order_by("orders.date_created", "asc"); 	
		$this->db->limit($limit, $offset); 
		$this->db->where('deleted', 0); 	
		$this->db->where('is_error',0);	
		$this->db->where('delivery', 'pending');
		$this->db->where('payment', 'pending');	
		$this->db->where('payment_method', 'wire transfer');	

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_open_quotes($offset, $limit)
	{
		if(!isset($offset) || !isset($limit))
		{
			$offset = 0;
			$limit = 15;
		}

		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		$this->db->select('orders.id AS orderid');
		$this->db->select("DATE_FORMAT(DATE_ADD(orders.date_created, INTERVAL 2 WEEK), '%d/%m/%Y') AS due_date", FALSE);
		$this->db->select("DATE_FORMAT(orders.date_created, '%d/%m/%Y') AS created_at", FALSE);
		$this->db->select("DATEDIFF(NOW(), orders.date_created) AS daysold", FALSE);				
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left');
		$this->db->join('countries', 'customers.country_code = countries.countryCode COLLATE utf8_unicode_ci', 'left', FALSE);	
		$this->db->order_by("orders.is_priority", "desc"); 	
		$this->db->order_by("orders.date_created", "asc"); 	
		$this->db->limit($limit, $offset); 
		$this->db->where('deleted', 0); 	
		$this->db->where('is_error',0);	
		$this->db->where('delivery', 'pending');
		$this->db->where('payment', 'quote');		

		$query = $this->db->get();

		return $query->result_array();
	}

	public function search_orders($keyword, $field, $offset, $limit){

		$keyword = strtolower($keyword);
		$keyword = preg_replace('/\s+/', ' ', $keyword);		// remove excess whitespace
		$keyword = ltrim($keyword);

		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		$this->db->select('orders.id AS orderid');
		$this->db->select("DATE_FORMAT(DATE_ADD(orders.date_created, INTERVAL 2 WEEK), '%d/%m/%Y') AS due_date", FALSE);
		$this->db->select("DATE_FORMAT(orders.date_created, '%d/%m/%Y') AS created_at", FALSE);
		$this->db->select("DATEDIFF(NOW(), orders.date_created) AS daysold", FALSE);	
		$this->db->select("LOWER(CONCAT(customers.firstname, ' ', customers.lastname)) AS fullname", FALSE);	
		$this->db->select('customers.lastname AS fullname');	
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left');
		$this->db->join('countries', 'customers.country_code = countries.countryCode COLLATE utf8_unicode_ci', 'left', FALSE);	
		$this->db->order_by("orders.date_created", "desc"); 	
		$this->db->limit($limit, $offset); 	
		$this->db->where('deleted', 0); 	

		if($field == ""){			
			$this->db->like('LOWER(customers.firstname)', $keyword);
			$this->db->or_like('LOWER(customers.lastname)', $keyword);
			$this->db->or_like("LOWER(CONCAT(customers.firstname, ' ', customers.lastname))", $keyword);
			$this->db->or_like("LOWER(CONCAT(customers.lastname, ' ', customers.firstname))", $keyword);
			$this->db->or_like('countryName', $keyword);
			$this->db->or_where('customers.country_code', $keyword);
			$this->db->or_where('orders.id', $keyword);
			$this->db->or_where('payment_method', $keyword);
			$this->db->or_where('payment', $keyword);
			$this->db->or_where('orders.plug_type', $keyword);
			$this->db->or_where('email_address', $keyword);
		}else if($field == "name"){			
			$this->db->like('LOWER(customers.firstname)', $keyword);			
			$this->db->or_like('LOWER(customers.lastname)', $keyword);
			$this->db->or_like("LOWER(CONCAT(customers.firstname, ' ', customers.lastname))", $keyword);
			$this->db->or_like("LOWER(CONCAT(customers.lastname, ' ', customers.firstname))", $keyword);
		}else if($field == "month"){
			$this->db->where("MONTH(orders.date_created) = month(str_to_date('".$keyword."','%b'))",NULL, FALSE);
		}else{			
			$this->db->where($field, $keyword); 
		}

		$query = $this->db->get();

		return $query->result_array();		
		
	}

	public function search_orders_helpscout($email, $first_name, $last_name){
		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		$this->db->select('orders.id AS orderid');
		$this->db->select("DATE_FORMAT(DATE_ADD(orders.date_created, INTERVAL 2 WEEK), '%d/%m/%Y') AS due_date", FALSE);
		$this->db->select("DATE_FORMAT(orders.date_created, '%d/%m/%Y') AS created_at", FALSE);
		$this->db->select("DATEDIFF(NOW(), orders.date_created) AS daysold, is_priority", FALSE);		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left');
		$this->db->join('countries', 'customers.country_code = countries.countryCode COLLATE utf8_unicode_ci', 'left', FALSE);	
		$this->db->order_by("orders.date_created", "desc"); 	
		$this->db->where("((customers.firstname LIKE '".$first_name."' AND customers.lastname LIKE '".$last_name."') OR email_address = '".$email."')", NULL, FALSE);
		$this->db->where('deleted', 0); 	

		$query = $this->db->get();

		return $query->result_array();		
		
	}

	public function get_num_returned_rows(){
		 return $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
	}

	public function num_orders(){
		$this->db->select('id');		
		$this->db->from('orders');	
		$this->db->where('deleted', 0); 		
		$query = $this->db->get();

		return $query->num_rows();
	}

	public function num_error_orders(){
		$this->db->select('id');		
		$this->db->from('orders');	
		$st="deleted=0 AND (orders.is_error = 1 
						OR (orders.payment_method != 'wire transfer'
							AND orders.payment = 'pending'
							AND orders.delivery = 'pending')
						)";
  		$this->db->where($st, NULL, FALSE); 		
		$query = $this->db->get();

		return $query->num_rows();
	}

	public function num_shipping_orders(){
		$this->db->select('id');		
		$this->db->from('orders');	
		$this->db->where('deleted', 0); 
		$this->db->where('is_error',0);		
		$this->db->where('delivery', 'pending');
		$this->db->where('payment', 'completed');  		
		$query = $this->db->get();

		return $query->num_rows();
	}

	public function num_unpaid_shipped_orders(){
		$this->db->select('id');		
		$this->db->from('orders');	
		$this->db->where('deleted', 0);
		$this->db->where('is_error',0); 	
		$this->db->where('delivery', 'shipped');	
		$this->db->where('payment !=', 'completed');  		
		$query = $this->db->get();

		return $query->num_rows();
	}

	public function num_unpaid_orders(){
		$this->db->select('id');		
		$this->db->from('orders');	
		$this->db->where('deleted', 0);
		$this->db->where('is_error',0); 
		$this->db->where('delivery', 'pending');		
		$this->db->where('payment', 'pending'); 
		$this->db->where('payment_method', 'wire transfer'); 		
		$query = $this->db->get();

		return $query->num_rows();
	}

	public function num_open_quotes(){
		$this->db->select('id');		
		$this->db->from('orders');	
		$this->db->where('deleted', 0);
		$this->db->where('is_error',0); 
		$this->db->where('delivery', 'pending');		
		$this->db->where('payment', 'quote'); 		
		$query = $this->db->get();

		return $query->num_rows();
	}

	public function get_kickstarter_order($email){
		$this->db->select('orders.id AS orderid');		
		$this->db->from('customers');
		$this->db->join('orders', 'customers.id = orders.customer_id', 'left');
		$this->db->where('customers.email_address', $email); 	
		$this->db->where('payment_method', 'kickstarter'); 	
		$query = $this->db->get();

		if($query->num_rows() != 1){
			return -1;
		}else{
			$orderid = $query->row()->orderid;	
			return $orderid;
		}
	}

	public function get_items($order_id){
		$this->db->select('*, IFNULL(baskets.product_name, products.name) as productname', FALSE);
		$this->db->select('products.id AS productID');
		$this->db->from('baskets');
		$this->db->join('products', 'products.id = baskets.product_id', 'left');
		$this->db->where('baskets.order_id', $order_id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function update_payment($order_id, $new_payment_value){
		$this->db->where('id', $order_id);
        $result = $this->db->update('orders', array('payment'=>$new_payment_value));

        if($new_payment_value == 'completed')
        {        	
        	$this->db->set('date_payed', 'NOW()', FALSE);
        	$this->db->where('id', $order_id);
        	$result = $this->db->update('orders');
    	}

        return $result;
	}

	public function update_payment_method($order_id, $new_payment_method){
		$this->db->where('id', $order_id);
        $result = $this->db->update('orders', array('payment_method'=>$new_payment_method));

        return $result;
	}

	public function update_plug_type($order_id, $plug_type){
		$this->db->where('id', $order_id);
        $result = $this->db->update('orders', array('plug_type'=>$plug_type));

        return $result;
	}

	public function update_priority($order_id, $priority){
		if($priority == 1 || $priority == 0){
			$this->db->where('id', $order_id);
        	$result = $this->db->update('orders', array('is_priority'=>$priority));

        	return $result;
    	}

    	return NULL;
	}

	public function order_shipped($order_id){
		$this->db->where('id', $order_id);
        $result = $this->db->update('orders', array('delivery'=>'shipped', 'date_shipped'=>date('Y-m-d H:i:s',now())));

        return $result;
	}

	public function edit_remark_order($order_id, $remarks){
		$this->db->where('id', $order_id);
        $result = $this->db->update('orders', array('remarks' => $remarks));
		return $result;
	}

	public function delete_order($order_id, $remarks){
		$this->db->where('id', $order_id);
        $result = $this->db->update('orders', array('deleted'=>1, 'remarks' => $remarks));	
        return $result;
	}

	/*----------------------------- STATS ----------------------------------------*/
	public function get_country_stats($year)
	{
		// group all orders per country
		$this->db->select('*, COUNT(customers.id) as numCustomers, SUM(grand_total) as country_total, SUM(grand_total*tax_rate/100.0) as country_tax, SUM(shipping_cost) as country_shipping, countries.countryCode as countryCode');		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left' );
		$this->db->join('countries', 'countries.countryCode = customers.country_code COLLATE utf8_unicode_ci', 'left', false);	
		//$this->db->join('shipping_rates', 'shipping_rates.countryCode = countries.countryCode COLLATE utf8_unicode_ci', 'left', false);	
		$this->db->group_by("customers.country_code");
		$this->db->order_by("country_total", "DESC", FALSE);		
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");		
		$this->db->where("YEAR(orders.date_created)", $year);		
		$query = $this->db->get();
		$result = $query->result();

		// get the lowest shipping rate for each country
		$this->load->model('shipping_rates_model'); 
		foreach($result as &$country_result)
		{
			$rate = $this->shipping_rates_model->get_lowest_rate($country_result->countryCode);
			if($rate != NULL)
			{
				$country_result = (object) array_merge( (array)$country_result, array( 'shiprate' => $rate->rate_cents ) );
			}else{
				$country_result = (object) array_merge( (array)$country_result, array( 'shiprate' => '0' ) );
			}
		}

		return $result;	
	}

	public function get_sector_stats($year)
	{
		// group all orders per country
		$this->db->select('*, COUNT(customers.id) as numCustomers, SUM(grand_total) as sector_total');		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left' );		
		$this->db->group_by("sector");
		$this->db->order_by("sector", "ASC", FALSE);		
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");
		$this->db->where("sector != ", "");		
		$this->db->where("YEAR(orders.date_created)", $year);		
		$query = $this->db->get();
		$result = $query->result();

		return $result;	
	}

	public function get_tracking_stats($year)
	{
		// group all orders per country
		$this->db->select('*, COUNT(customers.id) as numCustomers, SUM(grand_total) as tracking_total');		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left' );		
		$this->db->group_by("tracking");
		$this->db->order_by("tracking", "ASC", FALSE);		
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");
		$this->db->where("tracking != ", "");		
		$this->db->where("YEAR(orders.date_created)", $year);		
		$query = $this->db->get();
		$result = $query->result();

		return $result;	
	}

	public function get_monthly_stats($year)
	{
		// group all orders per country
		$this->db->select('*,DATE_FORMAT(orders.date_created, "%M") as month, COUNT(customers.id) as numCustomers, SUM(grand_total) as monthly_total');		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left' );		
		$this->db->group_by("MONTH(orders.date_created)");
		$this->db->order_by("MONTH(orders.date_created)", "ASC", FALSE);		
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");
		$this->db->where("YEAR(orders.date_created)", $year);		
		$query = $this->db->get();
		$result = $query->result();

		return $result;	
	}

	public function get_paymentmethod_stats($year)
	{
		// group all orders per payment method
		$this->db->select('*, COUNT(customers.id) as numCustomers');		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left' );		
		$this->db->group_by("payment_method");			
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");
		$this->db->where("YEAR(orders.date_created)", $year);		
		$query = $this->db->get();
		$result = $query->result();

		return $result;	
	}

	public function get_product_stats($year)
	{
		// group all orders per country
		$this->db->select('SUM(baskets.quantity) as numProducts, products.name as product_name');		
		$this->db->from('baskets');
		$this->db->join('orders', 'orders.id = baskets.order_id', 'both' );		
		$this->db->join('products', 'products.id = baskets.product_id', 'left' );		
		$this->db->group_by("products.name");			
		$this->db->where("product_id < ", "999");
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");
		$this->db->where("YEAR(orders.date_created)", $year);		
		$query = $this->db->get();
		$result = $query->result();

		return $result;		
	}

	public function get_anchortag_stats($year)
	{
		// group all orders per country
		$this->db->select('SUM(products.anchors) as numAnchors, SUM(products.tags) as numTags');		
		$this->db->from('baskets');
		$this->db->join('orders', 'orders.id = baskets.order_id', 'left' );		
		$this->db->join('products', 'products.id = baskets.product_id', 'left' );		
		//$this->db->group_by("products.name");			
		$this->db->where("product_id < ", "999");
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");
		$this->db->where("YEAR(orders.date_created)", $year);		
		$query = $this->db->get();
		$result = $query->result();

		return $result;		
	}

	public function get_stock_info(){
		$result = array(array("tags",0,0,0,0,0),array("anchors",0,0,0,0,0));
		
		//Physical stock (recieved from supplier minus delivered)
		$query = $this->db->query("select SUM(quantity) as nrTags from purchase_orders where delivery_date<=now() and product_id=4");
		$firstrow = $query->first_row('array');
		$result[0][1] = $firstrow['nrTags'];
		
		$query = $this->db->query("select SUM(products.tags * baskets.quantity) as nrTagsDel from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery!='pending'");
		$firstrow = $query->first_row('array');
		$result[0][1] = $result[0][1] - $firstrow['nrTagsDel'];
		
		$query = $this->db->query("select SUM(quantity) as nrAnchors from purchase_orders where delivery_date<=now() and product_id=5");
		$firstrow = $query->first_row('array');
		$result[1][1] = $firstrow['nrAnchors'];
		
		$query = $this->db->query("select SUM(products.anchors * baskets.quantity) as nrAnchorsDel from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery!='pending'");
		$firstrow = $query->first_row('array');
		$result[1][1] = $result[1][1] - $firstrow['nrAnchorsDel'];

		//Quotes
		$query = $this->db->query("select SUM(products.tags * baskets.quantity) as tagsQuotes from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.is_error=0 and orders.delivery='pending' and orders.payment='quote'");
		$firstrow = $query->first_row('array');
		$result[0][2] = $firstrow['tagsQuotes'];		

		$query = $this->db->query("select SUM(products.anchors * baskets.quantity) as anchorsQuotes from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.is_error=0 and orders.delivery='pending' and orders.payment='quote'");
		$firstrow = $query->first_row('array');
		$result[1][2] = $firstrow['anchorsQuotes'];

		//Ordered at supplier
		/*
		$query = $this->db->query("select SUM(quantity) as nrTags from purchase_orders where delivery_date>now() and product_id=4");
		$firstrow = $query->first_row('array');
		$result[0][2] = $firstrow['nrTags'];		

		$query = $this->db->query("select SUM(quantity) as nrAnchors from purchase_orders where delivery_date>now() and product_id=5");
		$firstrow = $query->first_row('array');
		$result[1][2] = $firstrow['nrAnchors'];
		*/

		//Wire Transfer
		$query = $this->db->query("select SUM(products.tags * baskets.quantity) as tagsWireTransfer from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.is_error=0 and orders.delivery='pending' and orders.payment='pending' and orders.payment_method='wire transfer'");
		$firstrow = $query->first_row('array');
		$result[0][3] = $firstrow['tagsWireTransfer'];
		
		$query = $this->db->query("select SUM(products.anchors * baskets.quantity) as anchorsWireTransfer from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.is_error=0 and orders.delivery='pending' and orders.payment='pending' and orders.payment_method='wire transfer'");
		$firstrow = $query->first_row('array');
		$result[1][3] = $firstrow['anchorsWireTransfer'];

		//Undelivered sales
		$query = $this->db->query("select SUM(products.tags * baskets.quantity) as tagsSold from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.is_error=0 and orders.delivery='pending' and orders.payment='completed'");
		$firstrow = $query->first_row('array');
		$result[0][4] = $firstrow['tagsSold'];
		
		$query = $this->db->query("select SUM(products.anchors * baskets.quantity) as anchorsSold from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.is_error=0 and orders.delivery='pending' and orders.payment='completed'");
		$firstrow = $query->first_row('array');
		$result[1][4] = $firstrow['anchorsSold'];

		$result[0][5] = $result[0][1]-$result[0][2]-$result[0][3]-$result[0][4];
		$result[1][5] = $result[1][1]-$result[1][2]-$result[1][3]-$result[1][4];

/* SQL statements
// PHYSICAL STOCK;
select SUM(quantity) as nrTags from purchase_orders where delivery_date<now() and product_id=4;
select SUM(quantity) from purchase_orders where delivery_date<now() and product_id=5;
select SUM(products.tags * baskets.quantity) from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery!='pending';
select SUM(products.anchors * baskets.quantity) from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery!='pending';

// ORDERED;
select SUM(quantity) from purchase_orders where delivery_date>now() and product_id=4;
select SUM(quantity) from purchase_orders where delivery_date>now() and product_id=5;

//PENDING SALES;
select SUM(products.tags * baskets.quantity) from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery='pending' and orders.payment!='completed';
select SUM(products.anchors * baskets.quantity) from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery='pending' and orders.payment!='completed';

//UNDELIVERED SALES;
select SUM(products.tags * baskets.quantity) from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery='pending' and orders.payment='completed';
select SUM(products.anchors * baskets.quantity) from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery='pending' and orders.payment='completed';
*/
	
		return $result;
	}

	public function generate_invoices_kickstarter(){
		// one time function!
		
		// maybe used more than once only generate invoice for full filled in accounts
		/*
		$query = $this->db->query("select orders.id as orderid from orders inner join customers on orders.customer_id=customers.id where orders.deleted = 0 and orders.payment_method = 'kickstarter' and customers.firstname !='' order by orders.id");
		
		foreach ($query->result_array() as $row)
		{
			$this->generate_pdf_invoice('invoice', $row['orderid']);
		}
		*/

	}

	public function correct_kickstarter_orders(){
		// one time function!

		// All invoicing done from BE
		// correction for DOM -> 21% vat
		// correction for EU noVAT -> 21% vat
		// Amount in VAT will be given as discount

		/*
		$query = $this->db->query("select orders.id,orders.subtotal,orders.shipping_cost,orders.grand_total from orders inner join customers on orders.customer_id = customers.id inner join countries on customers.country_code = countries.countryCode where payment_method = 'kickstarter' and vat_code = 'DOM'");
		foreach ($query->result_array() as $row)
		{
			$discount_perc = ($row['subtotal'] + $row['shipping_cost'] - $row['grand_total'] / 1.21) / $row['subtotal'] * 100;
			$update_query = $this->db->query("update orders set tax_rate = 21, discount_rate = " . $discount_perc . " where id = " . $row['id']);

		}

		$query = $this->db->query("select orders.id,orders.subtotal,orders.shipping_cost,orders.grand_total from orders inner join customers on orders.customer_id = customers.id inner join countries on customers.country_code = countries.countryCode where payment_method = 'kickstarter' and vat_code = 'EU' and company_vat != ''");
		foreach ($query->result_array() as $row)
		{
			$discount_perc = ($row['subtotal'] + $row['shipping_cost'] - $row['grand_total'] / 1.21) / $row['subtotal'] * 100;
			$update_query = $this->db->query("update orders set tax_rate = 21, discount_rate = " . $discount_perc . " where id = " . $row['id']);

		}
		*/
	}




	public function insert_kickstarter_orders(){
		// one time function!

		/*
		$this->db->select('*');
		$this->db->from('foundersclub');
		$this->db->where('Shipping_Country IS NOT NULL', null, false);
		$query = $this->db->get();

		$number = 1;
		foreach ($query->result() as $row)
		{
			$pledge_amount = 100*((int)preg_replace("/([^0-9\\.])/i", "", $row->Pledge_Amount));
		    $total = 100*((int)preg_replace("/([^0-9\\.])/i", "", $row->Total));
		    $date = str_replace("/", "-", str_replace(",", "", $row->Pledged_At)) . ":00";

		    // add the customer to the database
		    $customer = array(			   
			   'email_address' => $row->Email,				  
			   'country_code' => $row->Shipping_Country,
			   'date_created' => $date			  
			);				
		    $this->db->insert('customers', $customer);
		    $customerID = $this->db->insert_id();	

			//echo ($number++) . " " . $row->Email . " " . $row->Shipping_Country	. "<br>";
			//echo $total . " " . $pledge_amount . " ". ($total - $pledge_amount) . "<br>";
			//echo str_replace("/", "-", str_replace(",", "", $row->Pledged_At)) . ":00<br><br>";

			// add the order to the database		    
			$order = array(
				'id' => $row->Backer_Number,
				'customer_id' => $customerID,
		   		'payment' => 'completed' ,
			   	'delivery' => 'pending', 	
			   	'date_created' => $date,			  	
			  	'grand_total' => $total,	
			  	'subtotal' => $pledge_amount,	
			  	'shipping_cost' => ($total - $pledge_amount),	
			  	'payment_method' => 'kickstarter',
			  	'tax_rate' => 0,
			  	'remarks' => $row->Notes	   
			);				
			$this->db->insert('orders', $order);
			$orderID = $this->db->insert_id();

			// store the item from the cart in the database	
			$item_id = 0;
			if($pledge_amount == 18900)
				$item_id = 2;	
			elseif($pledge_amount == 39900 || $pledge_amount == 44900)
				$item_id = 2;
			elseif($pledge_amount == 89900)
				$item_id = 3;

			$data = array(
			   'order_id' => $orderID ,
			   'product_id' =>  $item_id,
			   'quantity' => 1,
			   'unit_price' => $pledge_amount
			);

			$this->db->insert('baskets', $data);
			 
		}
		*/
	}

	public function update_encode(){
		// one time function!

		// Update all orders with correct encoded value
		/*
		$query = $this->db->query("select orders.id from orders");
		foreach ($query->result_array() as $row)
		{
			$update_query = $this->db->query("update orders set encoded = '" . $this->order_model->encode_orderID($row['id']) . "' where id = " . $row['id']);
		}
		*/

	}

}
