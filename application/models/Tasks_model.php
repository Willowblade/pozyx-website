<?php

class Tasks_model extends CI_Model { 

	public function get_tasks($id = 0){

		$this->db->select('*, tasklist.id as taskid, first_name, DATE_FORMAT(start_date, "%W %e %b") AS day_start, DATE_FORMAT(end_date, "%W %e %b") as day_end', FALSE);		
		$this->db->select("DATEDIFF(NOW(), start_date) AS daysold", FALSE);
		$this->db->from('tasklist');		
		$this->db->join('users', 'users.id = tasklist.user_id', 'left');
		$this->db->order_by("end_date", "ASC"); 			
		if($id > 0){
			$this->db->where('user_id', $id);	
		}
		$this->db->where('status', 'open');		
		$query = $this->db->get();

		return $query->result();		
	}

	public function get_user_open_tasks($user_id)
	{
		$this->db->select('COUNT(id) AS num', FALSE);				
		$this->db->from('tasklist');				
		$this->db->where('status', 'open');	
		$this->db->where('user_id', $user_id);			
		$query = $this->db->get();
		$result = $query->result();	

		return $result[0]->num;
	}

	public function get_user_completed_tasks($user_id)
	{
		$this->db->select('COUNT(id) AS num', FALSE);				
		$this->db->from('tasklist');				
		$this->db->where('status', 'completed');		
		$this->db->where('user_id', $user_id);		
		$query = $this->db->get();
		$result = $query->result();	

		return $result[0]->num;
	}	

	public function add_task($post){
		$this->db->insert('tasklist', $post);
	}

	public function update_task($post){

        $this->db->update('tasklist', $post, array('id' => $post->id));
	}

	public function complete_task($id){

        $this->db->update('tasklist', array('status' => 'completed'), array('id' => $id));
	}

}
