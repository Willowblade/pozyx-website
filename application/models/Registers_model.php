<?php

class Registers_model extends CI_Model { 

	public function get_regs_data($version){

		$this->db->select('*');		
		$this->db->from('registers');		
		$this->db->order_by(" CAST(CONV( SUBSTRING( reg_address, 3 ) , 16, 10 ) AS UNSIGNED) ", "asc", FALSE); 			
		$this->db->where('type !=', 'F'); 		
		$this->db->where('version', $version);		
		$query = $this->db->get();

		return $query->result();		
	}

	public function get_regs_function($version){

		$this->db->select('*');		
		$this->db->from('registers');		
		$this->db->order_by(" CAST(CONV( SUBSTRING( reg_address, 3 ) , 16, 10 ) AS UNSIGNED) ", "asc", FALSE);		
		$this->db->where('type', 'F'); 	
		$this->db->where('version', $version);		
		$query = $this->db->get();

		return $query->result();		
	}

	public function get_reg($id){
		$this->db->select('*');		
		$this->db->from('registers');		
		$this->db->where('id', $id);		
		$query = $this->db->get();

		$result = $query->result();	
		return $result[0];
	}

	public function update_reg($post){

        $this->db->update('registers', $post, array('id' => $post->id));
	}

}
