<?php
require_once(APPPATH .'libraries/dompdf/dompdf_config.inc.php');
class Statistics_model extends CI_Model { 
	
	
	/*----------------------------- STAT CONFIUGURATION ----------------------------------------*/

	public $_where_clause = '';

	function __construct()
    {
        parent::__construct();

        $this->_where_clause = '';
    }

	public function set_data_period($option)
	{
		if($option == 'thisyear')
			$this->_where_clause = "YEAR(orders.date_created) = YEAR(NOW())";
		else if($option == 'lastyear')
			$this->_where_clause = "YEAR(orders.date_created) = (YEAR(NOW())-1)";
		else if($option == 'last3months')
			$this->_where_clause = "orders.date_created > DATE_FORMAT(CURDATE(), '%Y-%m-01') - INTERVAL 2 MONTH";
		else if($option == 'last6months')
			$this->_where_clause = "orders.date_created > DATE_FORMAT(CURDATE(), '%Y-%m-01') - INTERVAL 5 MONTH";
		else if($option == 'last12months')
			$this->_where_clause = "orders.date_created > DATE_FORMAT(CURDATE(), '%Y-%m-01') - INTERVAL 11 MONTH";
		else //($option == 'alltime')
			$this->_where_clause = "orders.date_created > '2000-01-01'";	
	}

	/*----------------------------- STATS ----------------------------------------*/
	public function get_country_stats()
	{
		// group all orders per country
		$this->db->select('*, COUNT(customers.id) as numCustomers, SUM(grand_total) as country_total, SUM(grand_total*tax_rate/100.0) as country_tax, SUM(shipping_cost) as country_shipping, countries.countryCode as countryCode');		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left' );
		$this->db->join('countries', 'countries.countryCode = customers.country_code COLLATE utf8_unicode_ci', 'left', false);	
		//$this->db->join('shipping_rates', 'shipping_rates.countryCode = countries.countryCode COLLATE utf8_unicode_ci', 'left', false);	
		$this->db->group_by("customers.country_code");
		$this->db->order_by("country_total", "DESC", FALSE);		
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");		
		$this->db->where($this->_where_clause);			
		$query = $this->db->get();
		$result = $query->result();

		return $result;

	}

	/*
	 * get_product_stats
	 *
	 * Return for which sectors the orders are used by the customers
	 */
	public function get_sector_stats()
	{
		// group all orders per country
		$this->db->select('*, COUNT(customers.id) as numCustomers, SUM(grand_total) as sector_total');		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left' );		
		$this->db->group_by("sector");
		$this->db->order_by("sector", "ASC", FALSE);		
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");
		$this->db->where("sector != ", "");		
		$this->db->where($this->_where_clause);		
		$query = $this->db->get();
		$result = $query->result();

		return $result;	
	}

	/*
	 * get_tracking_stats
	 *
	 * Return what the orders are meant to track
	 */
	public function get_tracking_stats()
	{
		// group all orders per country
		$this->db->select('*, COUNT(customers.id) as numCustomers, SUM(grand_total) as tracking_total');		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left' );		
		$this->db->group_by("tracking");
		$this->db->order_by("tracking", "ASC", FALSE);		
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");
		$this->db->where("tracking != ", "");		
		$this->db->where($this->_where_clause);			
		$query = $this->db->get();
		$result = $query->result();

		return $result;	
	}

	/*
	 * get_quarterly_stats
	 *
	 * Return the sales results (number of orders and revenue) per quarter
	 */
	public function get_quarterly_stats($order_type = 'weborder')
	{
		// group all orders per quarter
		$this->db->select('YEAR(orders.date_created) as year, QUARTER(orders.date_created) as quarter, COUNT(customers.id) as numCustomers, SUM(grand_total) as quarterly_total');		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left' );		
		$this->db->group_by("YEAR(orders.date_created), QUARTER(orders.date_created)");
		$this->db->order_by("YEAR(orders.date_created), QUARTER(orders.date_created)");		
		$this->db->where("payment", "completed");	
		$this->db->where("order_type", $order_type);		
		$this->db->where("deleted", "0");
		$this->db->where($this->_where_clause);				
		$query = $this->db->get();
		$result = $query->result();

		return $result;	
	}

	/*
	 * get_monthly_stats
	 *
	 * Return the sales results (number of orders and revenue) per month
	 */
	public function get_monthly_stats($order_type = 'weborder')
	{
		// group all orders per country
		$this->db->select('YEAR(orders.date_created) as year, MONTH(orders.date_created) as month_num , DATE_FORMAT(orders.date_created, "%M") as month, COUNT(customers.id) as numCustomers, SUM(grand_total) as monthly_total');		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left' );		
		$this->db->group_by("YEAR(orders.date_created), MONTH(orders.date_created)");
		$this->db->order_by("YEAR(orders.date_created), MONTH(orders.date_created)");			
		$this->db->where("payment", "completed");	
		$this->db->where("order_type", $order_type);	
		$this->db->where("deleted", "0");
		$this->db->where($this->_where_clause);			
		$query = $this->db->get();
		$result = $query->result();

		return $result;	
	}

	/*
	 * get_product_stats
	 *
	 * Return how often each payment method was used for the selected orders
	 */
	public function get_paymentmethod_stats()
	{
		// group all orders per payment method
		$this->db->select('*, COUNT(customers.id) as numCustomers');		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left' );		
		$this->db->group_by("payment_method");	
		$this->db->where("order_type", "weborder");
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");
		$this->db->where($this->_where_clause);			
		$query = $this->db->get();
		$result = $query->result();

		return $result;	
	}

	/*
	 * get_monthly_wiretransfers_stats
	 *
	 * Return how long it takes for a wire transfer to complete for each month
	 */
	public function get_monthly_wiretransfers_stats()
	{
		$this->db->select("SUM( IF( (TO_DAYS(date_payed)-TO_DAYS(date_created)) <= 3 AND
									(payment = 'completed' ) , 1, 0 )) as cat1", FALSE);
		$this->db->select("SUM( IF( ((TO_DAYS(date_payed)-TO_DAYS(date_created)) > 4) AND  
									((TO_DAYS(date_payed)-TO_DAYS(date_created)) < 7) AND
									(payment = 'completed' ), 1, 0 )) as cat2", FALSE);
		$this->db->select("SUM( IF( ((TO_DAYS(date_payed)-TO_DAYS(date_created)) > 7) AND 
									((TO_DAYS(date_payed)-TO_DAYS(date_created)) < 21) AND
									(payment = 'completed' ) , 1, 0 )) as cat3", FALSE);
		$this->db->select("SUM( IF( ((TO_DAYS(date_payed)-TO_DAYS(date_created)) >= 21) AND 
									(payment = 'completed' ) , 1, 0 )) as cat4", FALSE);
		$this->db->select("SUM( IF( payment != 'completed' , 1, 0 )) as cat5", FALSE);
		$this->db->select("DATE_FORMAT(orders.date_created, '%M') as month, YEAR(orders.date_created)");				
		$this->db->from('orders');	
		$this->db->group_by("YEAR(orders.date_created), MONTH(orders.date_created)");
		$this->db->order_by("YEAR(orders.date_created), MONTH(orders.date_created)");		
		$this->db->where("deleted", "0");
		$this->db->where("payment_method", "wire transfer");
		$this->db->where("order_type", "weborder");
		$this->db->where($this->_where_clause);			
		$query = $this->db->get();
		$result = $query->result();

		return $result;		
	}

	/*
	 * get_product_stats
	 *
	 * Return how many of each product has been sold
	 */
	public function get_product_stats()
	{
		// group all orders per country
		$this->db->select('SUM(baskets.quantity) as numProducts, products.name as product_name');		
		$this->db->from('baskets');
		$this->db->join('orders', 'orders.id = baskets.order_id', 'both' );		
		$this->db->join('products', 'products.id = baskets.product_id', 'left' );		
		$this->db->group_by("products.name");			
		$this->db->where("product_id < ", "999");
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");
		$this->db->where($this->_where_clause);	
		$query = $this->db->get();
		$result = $query->result();

		return $result;		
	}

	/*
	 * get_avg_week_stats
	 *
	 * Return the number of tags and anchors shipped
	 */
	public function get_anchortag_stats()
	{
		$this->db->select('SUM(products.anchors) as numAnchors, SUM(products.tags) as numTags');		
		$this->db->from('baskets');
		$this->db->join('orders', 'orders.id = baskets.order_id', 'left' );		
		$this->db->join('products', 'products.id = baskets.product_id', 'left' );			
		$this->db->where("product_id < ", "999");
		$this->db->where("delivery", "shipped");		
		$this->db->where("deleted", "0");
		$this->db->where($this->_where_clause);	
		$query = $this->db->get();
		$result = $query->result();

		return $result;		
	}

	/*
	 * get_week_stats
	 *
	 * Return the total number of orders for each of the 7 days in the week
	 */
	public function get_week_stats()
	{
		$this->db->select('COUNT(orders.id) as avg_orders, DAYNAME(orders.date_created) as day_of_week');	
		$this->db->from('orders');	
		$this->db->group_by("DAYNAME(orders.date_created)");	
		$this->db->order_by("WEEKDAY(orders.date_created)", "ASC", FALSE);	
		$this->db->where("payment", "completed");	
		$this->db->where("order_type", "weborder");	
		$this->db->where("deleted", "0");
		$this->db->where($this->_where_clause);		
		$query = $this->db->get();
		$result = $query->result();

		return $result;		
	}

	/*
	 * get_avg_week_stats() --- buggy: it will not take the real averages per day because it doesn't count days without an order.
	 *
	 * Return the average number of orders for each of the 7 days in the week
	 */
	public function get_avg_week_stats()
	{
		$this->db->select('day_of_week, AVG(num_orders) as avg_orders');	
		// subquery (not supported by activerecords)
		$this->db->from("(
                  SELECT
                  DAYNAME(date_created) as day_of_week,
                  WEEKDAY(date_created) as day_num,
                  TO_DAYS(date_created) as date,
                  COUNT(id) as num_orders             
                  FROM orders 
                  WHERE payment='completed' AND order_type='weborder' AND orders.deleted=0 AND ".$this->_where_clause."
                  GROUP BY date                   
              ) temp");
        $this->db->group_by("day_of_week");	
		$this->db->order_by("day_num");


		$query = $this->db->get();
		$result = $query->result();

		return $result;			
	}

	/*
	 * get_avg_hourly_stats
	 *
	 * Return the total number of orders for each hour of the day
	 */
	public function get_avg_hourly_stats()
	{
		$this->db->select('COUNT(orders.id) as numCustomers, HOUR(DATE_ADD(orders.date_created, INTERVAL 6 HOUR)) as hour_of_day', FALSE);	
		$this->db->from('orders');	
		$this->db->group_by("HOUR(DATE_ADD(orders.date_created, INTERVAL 6 HOUR))", FALSE);	
		$this->db->order_by("HOUR(DATE_ADD(orders.date_created, INTERVAL 6 HOUR))", "ASC", FALSE);	
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");
		$this->db->where("order_type", "weborder");
		$this->db->where($this->_where_clause);		
		$query = $this->db->get();
		$result = $query->result();

		return $result;		
	}

	/*
	 * get_shipping_stats
	 *
	 * Return the total number of orders for each hour of the day
	 */
	public function get_shipping_stats()
	{
		$this->db->select("COUNT(orders.id) as numOrders, (TO_DAYS(date_shipped)-TO_DAYS(date_payed)) as shipping_days, CASE
			    WHEN (TO_DAYS(date_shipped)-TO_DAYS(date_payed)) <= 3 THEN 'up to 3 days'
			    WHEN (TO_DAYS(date_shipped)-TO_DAYS(date_payed)) < 7 THEN '4 to 7 days'
			    WHEN (TO_DAYS(date_shipped)-TO_DAYS(date_payed)) < 15 THEN '1 to 2 weeks'
			    WHEN (TO_DAYS(date_shipped)-TO_DAYS(date_payed)) < 29 THEN '2 to 4 weeks'
			    ELSE 'more than 4 weeks'
			END AS shipping_time");	
		$this->db->from('orders');	
		$this->db->group_by("shipping_time");	
		$this->db->order_by("FIELD(shipping_time, 'up to 3 days','4 to 7 days','1 to 2 weeks','2 to 4 weeks', 'more than 4 weeks')", "ASC", FALSE);	
		$this->db->where("delivery", "shipped");		
		$this->db->where("deleted", "0");
		$this->db->where("payment", "completed");
		$this->db->where($this->_where_clause);		
		$query = $this->db->get();
		$result = $query->result();

		return $result;		
	}

	/*
	 * get_shipping_stats
	 *
	 * Return the 5 categories of shipping time for each month
	 */
	public function get_monthly_shipping_stats()
	{
		$this->db->select("SUM( IF( (TO_DAYS(date_shipped)-TO_DAYS(date_payed)) <= 3 , 1, 0 )) as cat1", FALSE);
		$this->db->select("SUM( IF( ((TO_DAYS(date_shipped)-TO_DAYS(date_payed)) > 3) AND  
									((TO_DAYS(date_shipped)-TO_DAYS(date_payed)) < 7), 1, 0 )) as cat2", FALSE);
		$this->db->select("SUM( IF( ((TO_DAYS(date_shipped)-TO_DAYS(date_payed)) > 7) AND 
									((TO_DAYS(date_shipped)-TO_DAYS(date_payed)) < 15 ) , 1, 0 )) as cat3", FALSE);
		$this->db->select("SUM( IF( ((TO_DAYS(date_shipped)-TO_DAYS(date_payed)) >= 15) AND 
									((TO_DAYS(date_shipped)-TO_DAYS(date_payed)) < 29 ) , 1, 0 )) as cat4", FALSE);
		$this->db->select("SUM( IF( (TO_DAYS(date_shipped)-TO_DAYS(date_payed)) >= 29 , 1, 0 )) as cat5", FALSE);

		$this->db->select("DATE_FORMAT(orders.date_created, '%M') as month");				
		$this->db->from('orders');	
		$this->db->group_by("YEAR(orders.date_created), MONTH(orders.date_created)");
		$this->db->order_by("YEAR(orders.date_created), MONTH(orders.date_created)");		
		$this->db->where("delivery", "shipped");		
		$this->db->where("deleted", "0");
		$this->db->where("payment", "completed");
		$this->db->where($this->_where_clause);			
		$query = $this->db->get();
		$result = $query->result();

		return $result;		
	}

	/*
	 * get_shipping_stats
	 *
	 * Return the total number of sales per geographical region (continent)
	 */
	public function get_region_stats()
	{
		// group all orders per country
		$this->db->select('COUNT(orders.id) as numCustomers, SUM(grand_total) as region_total, countries.region as region');		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left' );
		$this->db->join('countries', 'countries.countryCode = customers.country_code COLLATE utf8_unicode_ci', 'left', false);	
		//$this->db->join('shipping_rates', 'shipping_rates.countryCode = countries.countryCode COLLATE utf8_unicode_ci', 'left', false);	
		$this->db->group_by("region");
		$this->db->order_by("region_total", "DESC", FALSE);		
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");		
		$this->db->where($this->_where_clause);		
		$query = $this->db->get();
		$result = $query->result();

		return $result;
	}

	/*
	 * get_customer_type_stats
	 *
	 * Return the number of sales per type of customer: corporate or individual
	 */
	public function get_customer_type_stats()
	{
		$this->db->select('COUNT(orders.id) as numCustomers, SUM(grand_total) as total');
		$this->db->select("CASE
			    WHEN company_name != '' THEN 'company'
			    ELSE 'individual'
			END AS customer_type");
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left' );
		$this->db->group_by("customer_type");
		$this->db->order_by("customer_type");		
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");		
		$this->db->where($this->_where_clause);		
		$query = $this->db->get();
		$result = $query->result();

		return $result;
	}


}
