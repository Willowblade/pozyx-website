<?php
class Order_model extends CI_Model { 

	/* Generate a pdf document.
	doc_type can be 'invoice', 'pro forma invoice' or 'quote'
	*/
	public function generate_pdf_invoice($doc_type, $order_id){

		// load the order data
		$data = $this->get_details($order_id);
		$data["order_items"] = $this->get_items($order_id);
		$data["document_type"] = $doc_type;

		if(empty($data))
			log_message('error', 'generate pdf invoice: incorrect order id.');

		$invoice_dir = "./documents/invoices/";		
		//$invoice_dir = "/home/pozydhjv/public_html/tmp2/documents/invoices/";

		$this->load->library('pdf');
		$this->pdf->load_view('templates/pdf/invoice', $data);
		$this->pdf->render();

		// save the pdf to the generated file system
		$output = $this->pdf->output();

		$invoice_path = $invoice_dir . str_replace(' ', '_', $doc_type) . '_' . $this->encode_orderID($order_id) . '.pdf';
    	try{
    		file_put_contents($invoice_path, $output);
    	}catch ( Exception $e ) {
    		log_message('error', 'could not save file: ' . $e->getMessage());
    	}

    	return $invoice_path;

	}

	public function encode_orderID($order_id){

		$key = md5($this->config->item("encryption_key"), true); 

		$id = base_convert($order_id, 10, 36); // Save some space
	    $data = mcrypt_encrypt(MCRYPT_BLOWFISH, $key, $id, 'ecb');
	    $data = bin2hex($data);

		return $data;
	}

	public function decode_orderID($encrypted_order_id){

		$key = md5($this->config->item("encryption_key"), true); 

		$data = pack('H*', $encrypted_order_id); // Translate back to binary
	    $data = mcrypt_decrypt(MCRYPT_BLOWFISH, $key, $data, 'ecb');
	    $data = base_convert($data, 36, 10);

	    return $data;
	}

	/*
	$data: the data from the form
	$cart: an array with the cart contents (every row must have an id and a quantity)
	*/
	public function new_order($data, $cart, $subtotal){

		// is the billing address the same as the shipping address?		
		if($data['billingAddressIsShipping']==1){
			$data['addressLine1'] = $data['billing_addressLine1'];
			$data['addressLine2'] = $data['billing_addressLine2'];
			$data['State'] = $data['billing_State'];
			$data['City'] = $data['billing_City'];
			$data['ZIP'] = $data['billing_ZIP'];
		}else{
			$data['billingAddressIsShipping']=0;
		}

		if(!isset($data["user_remarks"]))
			$data["user_remarks"] = "";

		if(!isset($data["reduction_percent"]) || !$this->ion_auth->logged_in())
			$data["reduction_percent"] = 0;

		// add the customer to the database
	    $customer = array(
		   'firstname' => $data['firstName'],
		   'lastname' => $data['lastName'],
		   'email_address' => $data['email'],		
		   'phone' => $data['tel'],		
		   'country_code' => $data['country'],	
		   'company_name' => $data['companyName'],		      
		   'company_vat' => $data['vatNumber'],	
		   'address_line1' => $data['addressLine1'],
		   'address_line2' => $data['addressLine2'],
		   'state' => $data['State'],	
		   'city' => $data['City'],	
		   'zip' => $data['ZIP'],	
		   'billing_equals_shipping' => $data['billingAddressIsShipping'],
		   'billing_address_line1' => $data['billing_addressLine1'],
		   'billing_address_line2' => $data['billing_addressLine2'],
		   'billing_state' => $data['billing_State'],	
		   'billing_city' => $data['billing_City'],	
		   'billing_zip' => $data['billing_ZIP']	
		);	
		$this->db->set('date_created', 'NOW()', FALSE);
	    if(!$this->db->insert('customers', $customer)){
	    	return -1;
	    }	
	    $customerID = $this->db->insert_id();	    

		// add the order to the database		    
		$order = array(
			'customer_id' => $customerID,
	   		'payment' => 'pending' ,
		   	'delivery' => 'pending',	  	
		  	'tax_rate' =>$data["tax_rate"],	
		  	'subtotal' => $subtotal,	
		  	'discount_rate' => $data["reduction_percent"],
		  	'grand_total' => round($data["grand_total"],0),	
		  	'shipping_cost' => $data["shipping_cost"],	
		  	'payment_method' => $data["paymentOption"],
		  	'remarks' => $data["user_remarks"]	   
		);	
		$this->db->set('date_created', 'NOW()', FALSE);
		if(!$this->db->insert('orders', $order)){
	    	return -1;
	    }		
		$orderID = $this->db->insert_id();


		// store every item from the cart in the database
		foreach ($cart as $items){
			$data = array(
			   'order_id' => $orderID ,
			   'product_id' =>  $items['id'],
			   'quantity' => $items['qty'],
			   'unit_price' => ($items['subtotal']*100/$items['qty'])
			);

			if(!$this->db->insert('baskets', $data)){
				return -1;
			} 			
		} 

		return $orderID;
	}

	public function update_order($data){

		$this->db->where('id', $data["order_id"]);
		$result = $this->db->update('orders', array('plug_type'=>$data["plugtype"], 'user_remarks'=>$data["whatFor"]));

		// get the customer id
		$this->db->select('*');
		$this->db->select('customers.id AS customer_id');		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left');			
		$this->db->where('orders.id', $data["order_id"]);
		$query = $this->db->get();
		$customer_id = $query->row()->customer_id;

		// is the billing address the same as the shipping address?		
		if(isset($data['billingAddressIsShipping']) && $data['billingAddressIsShipping']==1){
			$data['billing_addressLine1'] = $data['addressLine1'];
			$data['billing_addressLine2'] = $data['addressLine2'];
			$data['billing_State'] = $data['State'];
			$data['billing_City'] = $data['City'];
			$data['billing_ZIP'] = $data['ZIP'];
		}else{
			$data['billingAddressIsShipping']=0;
		}

		// update customer information
		$this->db->where('id', $customer_id);
		$customer_update = array(
			'firstname' => $data['firstName'],
		   'lastname' => $data['lastName'],		  
		   'phone' => $data['tel'],	
		   'company_name' => $data['companyName'],		      
		   'company_vat' => $data['vatNumber'],				  
		   'address_line1' => $data['addressLine1'],
		   'address_line2' => $data['addressLine2'],
		   'state' => $data['State'],	
		   'city' => $data['City'],	
		   'zip' => $data['ZIP'],	
		   'billing_equals_shipping' => $data['billingAddressIsShipping'],
		   'billing_address_line1' => $data['billing_addressLine1'],
		   'billing_address_line2' => $data['billing_addressLine2'],
		   'billing_state' => $data['billing_State'],	
		   'billing_city' => $data['billing_City'],	
		   'billing_zip' => $data['billing_ZIP']	
		);
        $result = $this->db->update('customers', $customer_update);
	}

	public function get_details($order_id){
		$this->db->select('*');
		$this->db->select('orders.id AS orderid');
		$this->db->select("DATE_FORMAT(DATE_ADD(orders.date_created, INTERVAL 2 WEEK), '%d/%m/%Y') AS due_date", FALSE);
		$this->db->select("DATE_FORMAT(orders.date_created, '%d/%m/%Y') AS created_at", FALSE);
		$this->db->select("DATEDIFF(NOW(), orders.date_created) AS daysold", FALSE);
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left');
		$this->db->join('countries', 'customers.country_code = countries.countryCode COLLATE utf8_unicode_ci', 'left', FALSE);		
		$this->db->where('orders.id', $order_id);
		$query = $this->db->get();

		$data = $query->row_array();
		$data["encodedOrderID"] = $this->encode_orderID($order_id);

		return $data;
	}

	public function get_orders($offset, $limit){
		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		$this->db->select('orders.id AS orderid');
		$this->db->select("DATE_FORMAT(DATE_ADD(orders.date_created, INTERVAL 2 WEEK), '%d/%m/%Y') AS due_date", FALSE);
		$this->db->select("DATE_FORMAT(orders.date_created, '%d/%m/%Y') AS created_at", FALSE);
		$this->db->select("DATEDIFF(NOW(), orders.date_created) AS daysold", FALSE);
		//$this->db->select('SQL_CALC_FOUND_ROWS ',FALSE);			// so we can count the number of rows
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left');
		$this->db->join('countries', 'customers.country_code = countries.countryCode COLLATE utf8_unicode_ci', 'left', FALSE);	
		$this->db->order_by("orders.date_created", "desc"); 	
		$this->db->limit($limit, $offset); 	
		$this->db->where('deleted', 0); 	
		$query = $this->db->get();

		return $query->result_array();		
	}

	public function get_order_overview(){
		$this->db->select('*');		
		$this->db->select("DATE_FORMAT(date_created, '%b') AS whatmonth", FALSE);
		$this->db->select("COUNT(CASE WHEN payment='completed' THEN 1 END) AS payed_orders", FALSE);
		$this->db->select("SUM(CASE WHEN payment='completed' THEN grand_total END) AS sum_payed", FALSE);
		$this->db->select("SUM(CASE WHEN payment != 'completed' THEN grand_total END) AS sum_unpayed", FALSE);
		$this->db->select("SUM(grand_total) AS monthTotal", FALSE);
		$this->db->select("COUNT(id) AS orderTotal", FALSE);
		$this->db->from('orders');
		$this->db->group_by('MONTH(date_created), YEAR(date_created)', FALSE);
		$this->db->where("MONTH(date_created) >= MONTH(NOW() - INTERVAL 3 MONTH)",NULL, FALSE); 		// last 4 months
		$this->db->where('deleted', 0); 	
		$query = $this->db->get();

		return $query->result_array();		

	}

	public function search_orders($keyword, $field, $offset, $limit){
		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		$this->db->select('orders.id AS orderid');
		$this->db->select("DATE_FORMAT(DATE_ADD(orders.date_created, INTERVAL 2 WEEK), '%d/%m/%Y') AS due_date", FALSE);
		$this->db->select("DATE_FORMAT(orders.date_created, '%d/%m/%Y') AS created_at", FALSE);
		$this->db->select("DATEDIFF(NOW(), orders.date_created) AS daysold", FALSE);		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left');
		$this->db->join('countries', 'customers.country_code = countries.countryCode COLLATE utf8_unicode_ci', 'left', FALSE);	
		$this->db->order_by("orders.date_created", "desc"); 	
		$this->db->limit($limit, $offset); 	
		$this->db->where('deleted', 0); 	

		if($field == ""){			
			$this->db->like('customers.firstname', $keyword);
			$this->db->or_like('customers.lastname', $keyword);
			$this->db->or_like('countryName', $keyword);
			$this->db->or_where('customers.country_code', $keyword);
			$this->db->or_where('orders.id', $keyword);
			$this->db->or_where('payment_method', $keyword);
			$this->db->or_where('payment', $keyword);
			$this->db->or_where('plug_type', $keyword);
			$this->db->or_where('email_address', $keyword);
		}else if($field == "name"){			
			$this->db->like('customers.firstname', $keyword);
			$this->db->or_like('customers.lastname', $keyword);
		}else if($field == "month"){
			$this->db->where("MONTH(orders.date_created) = month(str_to_date('".$keyword."','%b'))",NULL, FALSE);
		}else{			
			$this->db->where($field, $keyword); 
		}

		$query = $this->db->get();

		return $query->result_array();		
		
	}

	public function get_num_returned_rows(){
		 return $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
	}

	public function num_orders(){
		$this->db->select('id');		
		$this->db->from('orders');	
		$this->db->where('deleted', 0); 		
		$query = $this->db->get();

		return $query->num_rows();
	}

	public function get_kickstarter_order($email){
		$this->db->select('orders.id AS orderid');		
		$this->db->from('customers');
		$this->db->join('orders', 'customers.id = orders.customer_id', 'left');
		$this->db->where('customers.email_address', $email); 	
		$this->db->where('payment_method', 'kickstarter'); 	
		$query = $this->db->get();

		if($query->num_rows() != 1){
			return -1;
		}else{
			$orderid = $query->row()->orderid;	
			return $orderid;
		}
	}

	public function get_items($order_id){
		$this->db->select('*');
		$this->db->select('products.id AS productID');
		$this->db->from('baskets');
		$this->db->join('products', 'products.id = baskets.product_id', 'left');
		$this->db->where('baskets.order_id', $order_id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function update_payment($order_id, $new_payment_value){
		$this->db->where('id', $order_id);
        $result = $this->db->update('orders', array('payment'=>$new_payment_value));

        return $result;
	}

	public function update_plug_type($order_id, $plug_type){
		$this->db->where('id', $order_id);
        $result = $this->db->update('orders', array('plug_type'=>$plug_type));

        return $result;
	}

	public function delete_order($order_id){
		$this->db->where('id', $order_id);
        $result = $this->db->update('orders', array('deleted'=>1));

        return $result;
	}

	public function get_stock_info(){
		$result = array(array("tags",0,0,0,0,0),array("anchors",0,0,0,0,0));
		
		//Physical stock (recieved from supplier minus delivered)
		$query = $this->db->query("select SUM(quantity) as nrTags from purchase_orders where delivery_date<=now() and product_id=4");
		$result[0][1] = $query->first_row('array')['nrTags'];
		$query = $this->db->query("select SUM(products.tags * baskets.quantity) as nrTagsDel from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery!='pending'");
		$result[0][1] = $result[0][1] - $query->first_row('array')['nrTagsDel'];
		
		$query = $this->db->query("select SUM(quantity) as nrAnchors from purchase_orders where delivery_date<=now() and product_id=5");
		$result[1][1] = $query->first_row('array')['nrAnchors'];
		$query = $this->db->query("select SUM(products.anchors * baskets.quantity) as nrAnchorsDel from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery!='pending'");
		$result[1][1] = $result[1][1] - $query->first_row('array')['nrAnchorsDel'];

		//Ordered at supplier
		$query = $this->db->query("select SUM(quantity) as nrTags from purchase_orders where delivery_date>now() and product_id=4");
		$result[0][2] = $query->first_row('array')['nrTags'];
		$query = $this->db->query("select SUM(quantity) as nrAnchors from purchase_orders where delivery_date>now() and product_id=5");
		$result[1][2] = $query->first_row('array')['nrAnchors'];

		//Pending sales
		$query = $this->db->query("select SUM(products.tags * baskets.quantity) as tagsPending from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery='pending' and orders.payment!='completed'");
		$result[0][3] = $query->first_row('array')['tagsPending'];
		$query = $this->db->query("select SUM(products.anchors * baskets.quantity) as anchorsPending from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery='pending' and orders.payment!='completed'");
		$result[1][3] = $query->first_row('array')['anchorsPending'];

		//Undelivered sales
		$query = $this->db->query("select SUM(products.tags * baskets.quantity) as tagsSold from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery='pending' and orders.payment='completed'");
		$result[0][4] = $query->first_row('array')['tagsSold'];
		$query = $this->db->query("select SUM(products.anchors * baskets.quantity) as anchorsSold from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery='pending' and orders.payment='completed'");
		$result[1][4] = $query->first_row('array')['anchorsSold'];

		$result[0][5] = $result[0][1]+$result[0][2]-$result[0][3]-$result[0][4];
		$result[1][5] = $result[1][1]+$result[1][2]-$result[1][3]-$result[1][4];

/* SQL statements
// PHYSICAL STOCK;
select SUM(quantity) as nrTags from purchase_orders where delivery_date<now() and product_id=4;
select SUM(quantity) from purchase_orders where delivery_date<now() and product_id=5;
select SUM(products.tags * baskets.quantity) from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery!='pending';
select SUM(products.anchors * baskets.quantity) from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery!='pending';

// ORDERED;
select SUM(quantity) from purchase_orders where delivery_date>now() and product_id=4;
select SUM(quantity) from purchase_orders where delivery_date>now() and product_id=5;

//PENDING SALES;
select SUM(products.tags * baskets.quantity) from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery='pending' and orders.payment!='completed';
select SUM(products.anchors * baskets.quantity) from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery='pending' and orders.payment!='completed';

//UNDELIVERED SALES;
select SUM(products.tags * baskets.quantity) from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery='pending' and orders.payment='completed';
select SUM(products.anchors * baskets.quantity) from products inner join baskets on products.`id`=baskets.`product_id` inner join orders on baskets.`order_id`=orders.id where orders.deleted=0 and orders.delivery='pending' and orders.payment='completed';
*/
	
		return $result;
	}




	public function insert_kickstarter_orders(){
		// one time function!

		/*
		$this->db->select('*');
		$this->db->from('foundersclub');
		$this->db->where('Shipping_Country IS NOT NULL', null, false);
		$query = $this->db->get();

		$number = 1;
		foreach ($query->result() as $row)
		{
			$pledge_amount = 100*((int)preg_replace("/([^0-9\\.])/i", "", $row->Pledge_Amount));
		    $total = 100*((int)preg_replace("/([^0-9\\.])/i", "", $row->Total));
		    $date = str_replace("/", "-", str_replace(",", "", $row->Pledged_At)) . ":00";

		    // add the customer to the database
		    $customer = array(			   
			   'email_address' => $row->Email,				  
			   'country_code' => $row->Shipping_Country,
			   'date_created' => $date			  
			);				
		    $this->db->insert('customers', $customer);
		    $customerID = $this->db->insert_id();	

			//echo ($number++) . " " . $row->Email . " " . $row->Shipping_Country	. "<br>";
			//echo $total . " " . $pledge_amount . " ". ($total - $pledge_amount) . "<br>";
			//echo str_replace("/", "-", str_replace(",", "", $row->Pledged_At)) . ":00<br><br>";

			// add the order to the database		    
			$order = array(
				'id' => $row->Backer_Number,
				'customer_id' => $customerID,
		   		'payment' => 'completed' ,
			   	'delivery' => 'pending', 	
			   	'date_created' => $date,			  	
			  	'grand_total' => $total,	
			  	'subtotal' => $pledge_amount,	
			  	'shipping_cost' => ($total - $pledge_amount),	
			  	'payment_method' => 'kickstarter',
			  	'tax_rate' => 0,
			  	'remarks' => $row->Notes	   
			);				
			$this->db->insert('orders', $order);
			$orderID = $this->db->insert_id();

			// store the item from the cart in the database	
			$item_id = 0;
			if($pledge_amount == 18900)
				$item_id = 2;	
			elseif($pledge_amount == 39900 || $pledge_amount == 44900)
				$item_id = 2;
			elseif($pledge_amount == 89900)
				$item_id = 3;

			$data = array(
			   'order_id' => $orderID ,
			   'product_id' =>  $item_id,
			   'quantity' => 1,
			   'unit_price' => $pledge_amount
			);

			$this->db->insert('baskets', $data);
			 
		}
		*/
	}

}
