<?php

class Shipping_rates_model extends MY_Model { 

	public $_table = 'shipping_rates';

	public function get_pozyx_rate($country_code, $cart)
	{
		$this->db->from('shipping_rates');	
		$this->db->where("countryCode", $country_code);
		$this->db->where("carrier", "pozyx");
		$query = $this->db->get();

		$rate = $query->row();	

		$this->load->model('product_model');
		
		// now see how many items are being shipped
		$nItems = 0;
		foreach ($cart as $items): 
			$row = $this->product_model->get_by('id', $items['id']);
			if(isset($row))
			{
				$nItems += $items['qty']*($row->anchors + $row->tags);
			}
		endforeach;	

		if($nItems < 5)
			$rate->rate_cents *= 0.7;
		$rate->weight_g = $nItems;

		return $rate;
	}

	public function create_pozyx_shipping_rates()
	{
		
		// first delete all pozyx rates	
		$this->db->where('carrier', 'pozyx');
		$this->db->delete('shipping_rates'); 

		$this->db->select('MIN(shipping_rates.rate_cents) as min_price, countries.countryCode as countryCode');		
		$this->db->from('countries');		
		$this->db->join('shipping_rates', 'shipping_rates.countryCode = countries.countryCode COLLATE utf8_unicode_ci', 'left', false);	
		$this->db->where("shipping_rates.weight_g", 3000);
		$this->db->where("service", "GlobalExpress");	
		$this->db->where("countries.is_available", 1);		
		$this->db->order_by("shipping_rates.rate_cents", "asc");	
		$this->db->group_by("countries.countryCode");				
			
		$query = $this->db->get();
		$result = $query->result();	

		// now we add the rates as pozyx rates, where USD = EUR and rounded up to an integer with 5

		$rate = array(
			'weight_g' => '3000',
			'carrier' => 'Pozyx',
			'service' => 'TNT',
			'currency' => 'EUR',
			'delivery_days' => 5,
			'box_size3D_inch' => '15x12x6'
			);

		echo "<table>";
		foreach($result as $country)
		{
			$rate_euro = round(($country->min_price/100.0+5/2)/5)*5;
			//$rate_cents = roundUpToAny($country->rate_cents);
			$this->db->set('last_updated', 'NOW()', FALSE);
			$this->db->set('countryCode', $country->countryCode);
			$this->db->set('rate_cents', $rate_euro*100, FALSE);
	    	$this->db->insert('shipping_rates', $rate);
	    	
	    	echo "<tr><td>".$country->countryCode . "</td><td>$" . $country->min_price/100.0 . "</td><td>" . $rate_euro . "euro</td></tr>";

	    }	
	    echo "</table>";


	    // now show how many countries do not have a shipping rate
	    $this->db->select('*, COUNT(shipping_rates.id) as numRates, countries.countryCode as countryCode');		
		$this->db->from('countries');		
		$this->db->join('shipping_rates', 'shipping_rates.countryCode = countries.countryCode COLLATE utf8_unicode_ci', 'left', false);	
		$this->db->where("countries.is_available", 1);		
		$this->db->group_by("countries.countryCode");	
			

		$query = $this->db->get();
		$result = $query->result();	


		echo "<h1>No shipping</h1><table>";
		foreach($result as $country)
		{
			if($country->numRates == 0)
			{
				echo "<tr><td>".$country->countryCode . "</td><td>".$country->countryName."</td><td>".$country->capital."</td><td>".$country->capital_postalcode."</td><td>".$country->numRates."</td></tr>";
			}
		}
		echo "</table>";		
	    
	}
}
