<?php

class Country_model extends MY_Model { 

	public $_table = 'countries';

	public function load_country_data()
	{
		// we use a public REST api to fill in this table
		$curl = curl_init();
	    $url = 'https://restcountries.eu/rest/v1/all';
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);			// only do this on localhost
	    $result = curl_exec($curl);
	    if($result === FALSE) {
		    die(curl_error($curl));
		}		
	    curl_close($curl);

	    foreach (json_decode($result) as $country)
		{
			$db_country = $this->as_object()->get_by(array("countryCode"=>$country->alpha2Code));
			if(is_object($db_country))
			{
				// update the country
				$updated_country = array(					
		        	'countryName' => $country->name,
		        	'capital' => $country->capital,
		        	'capital_postalcode' => '',
		        	'currency' => $country->currencies[0],
		        	'calling_code' => $country->callingCodes[0],
		        	'region' => $country->region,
		        );

				$this->update_by(array("countryCode"=> $country->alpha2Code), $updated_country);
			}else
			{
				// add country to the database
				$new_country = array(
					'countryCode' => $country->alpha2Code,
		        	'countryName' => $country->name,
		        	'capital' => $country->capital,
		        	'capital_postalcode' => '',
		        	'currency' => $country->currencies[0],
		        	'calling_code' => $country->callingCodes[0],
		        	'region' => $country->region,
		        	'vat_code' => 'EU'
		        );
				//$this->insert($new_country);
			}
		}
	}

	public function get_plug_type($country_code)
	{
		$this->db->select('plug_type');		
		$this->db->from('countries');		
		$this->db->where("countryCode", $country_code);					
		$query = $this->db->get();
		$result = $query->result();

		return ($result[0]->plug_type);
	}

	public function roundUpToAny($n,$x=5) {
	    return round(($n+$x/2)/$x)*$x;
	}

	public function get_shipping_rates($carrier = 'pozyx')
	{
		$this->db->select('*, countries.countryCode as countryCode');		
		$this->db->from('countries');		
		$this->db->join('shipping_rates', 'shipping_rates.countryCode = countries.countryCode COLLATE utf8_unicode_ci', 'left', false);	
		$this->db->where("countries.is_available", 1);		
		$this->db->where("carrier", $carrier);		
		//$this->db->where("weight_g", 3000);		
		$this->db->order_by("region, countries.countryCode", "asc", FALSE);	
		//$this->db->order_by("shipping_rates.rate_cents", "asc", FALSE);		
			
		$query = $this->db->get();

		return $query->result();	
	}

	public function get_popular_shipping_rates()
	{
		// group all orders per country
		$this->db->select('*, COUNT(customers.id) as numCustomers, SUM(grand_total) as country_total, SUM(grand_total*tax_rate/100.0) as country_tax, SUM(shipping_cost) as country_shipping, countries.countryCode as countryCode');		
		$this->db->from('orders');
		$this->db->join('customers', 'customers.id = orders.customer_id', 'left' );
		$this->db->join('countries', 'countries.countryCode = customers.country_code COLLATE utf8_unicode_ci', 'left', false);	
		//$this->db->join('shipping_rates', 'shipping_rates.countryCode = countries.countryCode COLLATE utf8_unicode_ci', 'left', false);	
		$this->db->group_by("customers.country_code");
		$this->db->order_by("country_total", "DESC", FALSE);		
		$this->db->where("payment", "completed");		
		$this->db->where("deleted", "0");		
		$this->db->where("YEAR(orders.date_created)", "2016");		
		$query = $this->db->get();
		$result = $query->result();

		// get the lowest shipping rate for each country
		$this->load->model('shipping_rates_model'); 
		foreach($result as &$country_result)
		{
			$rate = $this->shipping_rates_model->get_lowest_rate($country_result->countryCode);
			if($rate != NULL)
			{
				$country_result = (object) array_merge( (array)$country_result, array( 'shiprate' => $rate->rate_cents ) );
			}else{
				$country_result = (object) array_merge( (array)$country_result, array( 'shiprate' => '0' ) );
			}
		}

		return $result;	
	}

}
