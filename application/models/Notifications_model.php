<?php

class Notifications_model extends CI_Model { 

	public function add_notification($order_id, $event, $type='email'){

		// add the customer to the database
	    $notification = array(
		   
		   'type' => $type,
		   'orderID' => $order_id,
		   'event' => $event
		);	
		$this->db->set('date', 'NOW()', FALSE);
	    if(!$this->db->insert('notifications', $notification)){
	    	return -1;
	    }
	}

	public function is_notified($order_id, $event){

		$this->db->select('id');		
		$this->db->from('notifications');	
		$this->db->where('orderID', $order_id); 	
		$this->db->where('event', $event); 		
		$query = $this->db->get();

		return $query->num_rows();
	}

}
