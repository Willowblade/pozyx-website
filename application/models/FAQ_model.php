<?php

class FAQ_model extends MY_Model { 

	public $_table = 'faq';

	function get_search($keywords) {	  

		// full text search with Mysql
		$keywords = $this->db->escape($keywords);

		$this->load->database();
		$query = $this->db->query("SELECT * FROM faq WHERE MATCH(answer) AGAINST(".$keywords.") OR MATCH(question) AGAINST(".$keywords.");");
		return $query->result();

	  /*$this->db->like('question',$match);
	  $this->db->or_like('answer',$match);	  
	  $query = $this->db->get('faq');
	  return $query->result();*/
	}
}
