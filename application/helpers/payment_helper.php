<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('paypal_process'))
{
    /*
     *  paypal_process()
     *
     *  This function processes the payment of an order with Paypal: this is where the customer gets charged.
     */
    function paypal_process($orderID, $token, $payer_id){		

        require_once(APPPATH . "libraries/paypal.class.php");  
        $CI = get_instance();   
        $CI->load->config('credentials');
        $CI->load->model('order_model');

        $data = $CI->order_model->get_details($orderID);
        if(empty($data))
            return "Error: order not found";   

        $order_items = $CI->order_model->get_items($orderID);  

        /*----------------------------------- Start paypal --------------------------------------------*/

        $paypalmode = ($CI->config->item("PayPalMode")=='sandbox') ? '.sandbox' : '';

        $paypal= new MyPayPal();

        // Get User details
        $CheckoutDetails = $paypal->PPHttpPost('GetExpressCheckoutDetails', '&TOKEN='.urlencode($token), $CI->config->item("PayPalApiUsername"), $CI->config->item("PayPalApiPassword"), $CI->config->item("PayPalApiSignature"), $CI->config->item("PayPalMode"));

        //echo "<br><br>" . $CheckoutDetails["SHIPTOCOUNTRYCODE"];
        
        //get session variables
        $HandalingCost      = 0.00;  //Handling cost for this order.
        $InsuranceCost      = 0.00;  //shipping insurance cost for this order.
        $ShippinDiscount    = 0.00; //Shipping discount for this order. Specify this as negative number.
        $ShippinCost        = round($data['shipping_cost']/100.00, 2); //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.

        $paypal_data = '';
        $ItemTotalPrice = 0;

        $number = 0;
        print_r($order_items);
        foreach ($order_items as $items)
        {
            echo "<br><br>";
            print_r($items);
            $paypal_data .= '&L_PAYMENTREQUEST_0_NAME'.$number.'='.urlencode($items['productname']);
            $paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER'.$number.'='.urlencode($items['product_id']);
            $paypal_data .= '&L_PAYMENTREQUEST_0_AMT'.$number.'='.urlencode(round($items['unit_price']/100.00,2));      
            $paypal_data .= '&L_PAYMENTREQUEST_0_QTY'.$number.'='. urlencode($items['quantity']);    
        
            //total price
            $ItemTotalPrice = $ItemTotalPrice + round($items['quantity'] * $items['unit_price']/100.00 ,2);     
            $number++;      
        } 

        // add shipping as a seperate item
        $paypal_data .= '&L_PAYMENTREQUEST_0_NAME'.$number.'='.urlencode('Shipping');
        $paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER'.$number.'='.urlencode('1000');
        $paypal_data .= '&L_PAYMENTREQUEST_0_AMT'.$number.'='.urlencode($ShippinCost);      
        $paypal_data .= '&L_PAYMENTREQUEST_0_QTY'.$number.'='. urlencode('1');  
        $ItemTotalPrice +=  $ShippinCost;

        $tax_rate = round($data['tax_rate'], 2);
        $TotalTaxAmount = round($ItemTotalPrice*($tax_rate/100), 2);  //Sum of tax for all items in this order.   

        //Grand total including all tax, insurance, shipping cost and discount
        $GrandTotal = round($data['grand_total']/100.00, 2);           

        $padata =   '&TOKEN='.urlencode($token).
                    '&PAYERID='.urlencode($payer_id).
                    '&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE").
                    $paypal_data.
                    '&PAYMENTREQUEST_0_ITEMAMT='.urlencode($ItemTotalPrice).
                    '&PAYMENTREQUEST_0_TAXAMT='.urlencode($TotalTaxAmount).
                    '&PAYMENTREQUEST_0_SHIPPINGAMT='.urlencode(0).
                    '&PAYMENTREQUEST_0_HANDLINGAMT='.urlencode($HandalingCost).
                    '&PAYMENTREQUEST_0_SHIPDISCAMT='.urlencode($ShippinDiscount).
                    '&PAYMENTREQUEST_0_INSURANCEAMT='.urlencode($InsuranceCost).
                    '&PAYMENTREQUEST_0_AMT='.urlencode($GrandTotal).
                    '&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode($CI->config->item("PayPalCurrencyCode"));

        //We need to execute the "DoExpressCheckoutPayment" at this point to Receive payment from user.         
        $httpParsedResponseAr = $paypal->PPHttpPost('DoExpressCheckoutPayment', $padata, $CI->config->item("PayPalApiUsername"), $CI->config->item("PayPalApiPassword"), $CI->config->item("PayPalApiSignature"), $CI->config->item("PayPalMode"));
        


        //Check if everything went ok..
        if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) 
        {               
            // echo 'Your Transaction ID : '.urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]);
            

            //Sometimes Payment are kept pending even when transaction is complete. 
            //hence we need to notify user about it and ask him manually approve the transiction
            $payment_status = strtolower(urldecode($httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]));
                                            

            // we can retrieve transection details using either GetTransactionDetails or GetExpressCheckoutDetails
            // GetTransactionDetails requires a Transaction ID, and GetExpressCheckoutDetails requires Token returned by SetExpressCheckOut
            $padata =   '&TOKEN='.urlencode($token);
            $paypal= new MyPayPal();
            $httpParsedResponseAr = $paypal->PPHttpPost('GetExpressCheckoutDetails', $padata, $CI->config->item("PayPalApiUsername"), $CI->config->item("PayPalApiPassword"), $CI->config->item("PayPalApiSignature"), $CI->config->item("PayPalMode"));

            if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) 
            {
                /*
                // we can ignore this
                $country_code = (isset($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE"]):$this->session->userdata('countryCode'));

                // save the order in the database
                $order_information = array(
                    'firstName'=>(isset($httpParsedResponseAr["FIRSTNAME"]) ? urldecode($httpParsedResponseAr["FIRSTNAME"]):""),
                    'lastName'=>(isset($httpParsedResponseAr["LASTNAME"]) ? urldecode($httpParsedResponseAr["LASTNAME"]):""),
                    'email'=>urldecode($httpParsedResponseAr["EMAIL"]),
                    'tel'=>(isset($httpParsedResponseAr["PHONENUM"]) ? urldecode($httpParsedResponseAr["PHONENUM"]):""),
                    'country'=>(isset($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE"]):""),
                    'companyName'=>(isset($httpParsedResponseAr["BUSINESS"]) ? urldecode($httpParsedResponseAr["BUSINESS"]):""),
                    'vatNumber'=>"",
                    'billing_addressLine1'=>(isset($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOSTREET"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOSTREET"]):""),
                    'billing_addressLine2'=>(isset($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOSTREET2"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOSTREET2"]):""),
                    'billing_State'=>(isset($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOSTATE"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOSTATE"]):""),
                    'billing_City'=>(isset($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOCITY"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOCITY"]):""),
                    'billing_ZIP'=>(isset($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOZIP"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOZIP"]):""),
                    'billingAddressIsShipping'=>1,
                    'tax_rate' => $tax_rate,
                    'grand_total'=> $GrandTotal*100,
                    'shipping_cost' => $ShippinCost*100,
                    'paymentOption' => 'paypal',
                    'user_remarks' => (isset($httpParsedResponseAr["PAYMENTREQUEST_0_NOTETEXT"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_NOTETEXT"]):"")
                );

                

                $orderID = $this->order_model->new_order($order_information, $this->cart->contents(), $ItemTotalPrice*100);
                if($orderID == -1) throw new Exception("Website Error: database error. Please report this problem to the webmaster.");
                $encodedOrderID = $this->order_model->encode_orderID($orderID);

                // payment status: pending, completed or failed
                $this->order_model->update_payment($orderID, $payment_status);

                $this->_send_confirmation_email($orderID);

                // destroy the shopping cart of the user
                $this->cart->destroy();         

                // redirect to the thank you page
                redirect('store/thank_you/'.$encodedOrderID, 'refresh');

                */

                                // If no exception was thrown, the charge was successful! 
                $CI->order_model->update_payment($orderID, 'completed');
                $CI->order_model->update_payment_method($orderID, 'paypal');

                return "";
                                
                
            }else{                  
                // error could not retrieve customer details
                return '<b>GetTransactionDetails failed:</b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]) . '<br><br>We were unable to obtain the transaction details. Please contact sales@pozyx.io.';                

            }

        }else{
            // error, could not make payment
            return '<b>Error : </b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]). '<br><br>Payment was not received. Please try again.';                               
        }
	} 
}

if ( ! function_exists('stripe_process'))
{
    /*
     *  stripe_process()
     *
     *  This function processes the payment of an order with Stripe: this is where the customer gets charged.
     */
    function stripe_process($orderID, $token){      

        require_once(APPPATH . 'libraries/Stripe/init.php');  // change this path to wherever you put the Stripe PHP library!
        $CI = get_instance();   
        $CI->load->config('credentials');
        $CI->load->model('order_model');

        $data = $CI->order_model->get_details($orderID);
        if(empty($data))
            return "Error: order not found";        

        // launch stripe
        try
        {                              
            \Stripe\Stripe::setApiKey($CI->config->item("StripeSecretKey"));
            

            // create the charge on Stripe's servers. THIS WILL CHARGE THE CARD!
            $charge = \Stripe\Charge::create(array(
                "amount" => $data['grand_total'],
                "currency" => "EUR",
                "source" => $token,
                "description" => "weborder #". $orderID . " - ". $data['email_address'], 
                "statement_descriptor" => "Pozyx labs webshop", 
                "metadata" => array(
                    "orderID"=>$orderID,
                    "emailaddress"=>$data['email_address']
                )
            ));

            if($charge->paid){                                   
                // If no exception was thrown, the charge was successful! 
                $CI->order_model->update_payment($orderID, 'completed');
                $CI->order_model->update_payment_method($orderID, 'credit card');
                
            }
        }
        catch (Exception $e)
        {
            
            // notify the database of a failed payment transaction, the order remains in the database
            $CI->order_model->update_payment($orderID, 'failed');

            // The charge failed for some reason. Stripe's message will explain why.
            $message = $e->getMessage();
            
            return $message;
        }

        return "";
    } 
}


