<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('EU_country'))
{
    function EU_country($countryCode){		
		$EU_countries = array("AT","BE","BG","HR","CY","CZ","DK","EE","FI","FR","DE","EL","HU","IE","IT","LV","LT","LU","MT","NL","PL","PT","RO","SK","SI","ES","SE");
		$extra_territories = array("MC", "IM", "IB", "PT-20", "PT-30");
		$eu_vat_list = array_merge($EU_countries, $extra_territories);

		return in_array ( $countryCode , $eu_vat_list );
	} 
}


if ( ! function_exists('valid_EU_VAT'))
{
	function valid_EU_VAT($vat, $country_iso)
    {
    	$this->CI->load->library('curl');

        $country_iso = strtoupper($country_iso);
        $vat = str_replace($country_iso, '', $vat);

        $post = array(
	        'ms' => $country_iso,
	        'iso' => $country_iso,
	        'vat' => $vat,
	        'requesterMs' =>  $this->CI->config->item('PT'),
	        'requesterIso' => $this->CI->config->item('PT'),
	        'requesterVat' => $this->CI->config->item('000000000'),
	        'BtnSubmitVat' => 'Verify'
        );

        $resp = $this->CI->curl->simple_post('http://ec.europa.eu/taxation_customs/vies/viesquer.do', $post);

        if (preg_match('/\svalid\sVAT\s/',$resp))
        {
            preg_match_all('/(WAPIAAAA.*)/', $resp, $match);
            return trim($match[0][0]);
        }
        else
        {
            return FALSE;
        }
    }
}

	