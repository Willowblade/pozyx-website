<style>
.portrait{
  height: 120px;
  margin-bottom: 10px;
  padding-right:7px;
  padding-left:7px;
}

.golden img{
  border: 2px solid gold;
}

#center_movie{

}

.founder{
  margin-right: 20px; 
  cursor: pointer;
}
</style>


<div class="container">
      <!-- Example row of columns -->
      <div class="row" style=" margin-top: 20px;">        

        <div class="col-md-12">
          <h2>Pozyx Labs jobs</h2>  

          <h3> Our vision</h3>
          <p>

            Imagine the home of the future, a home packed with sensors to ease your daily living and assists in daily chores. 
            A helper robot delivering your beer, cleaning the house, playing with your pets and siblings. 
            Just like the WiFi router today, or the electric cables running through the house, your future home will have 
            accurate location infrastructure, enabling a ton of use cases making your life easier.<br><br>
            Your home is just one place that will evolve the next decades, we believe in a future were the world we live in 
            is embedded with technology. And yes, it is already happening for many years, but the new technology revolution 
            has just begun: Self driving vehicles, cars, trucks, planes, boats... Drones delivering goods door to door. 
            Robots clerks in stores, robots assisting the elderly, helping educate our children, cleaning our houses, play with our dog...<br><br> 
            We believe that technology enhances our time on earth, how we communicate with each other, how we move from one spot to another, 
            taking the time to live like we want to live, to know exactly where things are… <br><br>
            We believe that Pozyx is just a small gear in that embedded world, enabling accurate positioning for all these use cases 
            and many more, getting closer to an enhanced world via automation.<br><br>


            If you are as passionate about electronics, programming, positioning, automation, robotics as we are:
            <br>Feel free to apply to our job openings below, or contact us directly at <a href="mailto:jobs@pozyx.io">jobs@pozyx.io</a>
          </p>
          <h3>Primary job openings</h3>
            <ul>
              <li><a href="#rnd"><b>R&amp;D engineer</b></a></li>                
              <!-- <li><a href="#embedded"><b>Embedded programmer</b></a></li>  -->          
            </ul>
            <h3>Future job openings</h3>
              <ul>  
              <li>Network engineer</li>  
              <li>Software developer</li>  
              <li>Hardware engineer</li>  
              <!--<li><a href="#sales">Sales &amp; project manager</a></li> -->
            </ul>
            <p>
            <br><br>Side note: You must have already found on our site what we are doing. We try to focus on our product as much as possible 
            and try to deliver the best solution in the market. But we are still a startup so other things might be little  chaotic. 
            You won't find neat job descriptions or profile expectations. 
            We just try to find the right person, the person not the CV and try to build a long term commitment. 
            Our offer? We try to meet industry standards (rent needs to be paid right).
            But as far as experience we already can tell you that the journey so far, is very exciting, international, very diverse, of course
            with ups and downs, but we would like you to help us smooth the path. 
            <br>
          </p>


        </div>
      </div>

<div id="rnd"></div>
      <div class="row" style=" margin-top: 80px;">
        <div class="col-md-2 col-sm-4" style='text-align: center'>
          <img src="<?php echo(base_url('assets/images/hire.jpg')); ?>" style='width:100%; max-width:200px; margin-top: 20px'>
          </a>
        </div>


        <div class="col-md-10 col-sm-8">
        
          <h3>R&amp;D engineer</h3>
          <p>
          We are currently looking for an -allround- reseach and development (R&amp;D) engineer that will help build the research department of Pozyx Labs.
          The R&amp;D engineer will actively do research to further improve the performance of the product, will search for new features, and will take the lead in several (international) research projects. Candiates should have a PhD in electrical engineering or similar with a strong focus towards signal processing or algorithmic design. Please contact us for more information.
            <p><a class="btn btn-primary btn-lg" href="mailto:jobs@pozyx.io?Subject=rnd%20engineer" role="button">Apply now!</a></p>
          </p>
        </div>  
</div>

<!--
<div id="embedded"></div>
      <div class="row" style=" margin-top: 80px;">

        <div class="col-md-2 col-sm-4" style='text-align: center'>
          <img src="<?php echo(base_url('assets/images/hire.jpg')); ?>" style='width:100%; max-width:200px; margin-top: 20px'>
          </a>
        </div>

        <div class="col-md-10 col-sm-8">
        
          <h3>Embedded programmer</h3>
          <p>
          We are looking for an embedded programmer to work on the embedded Pozyx positioning platform. 
          At a software level, the job consists of the design, implementation, testing and upgrading of the core software components of the Pozyx platform.
          The job also consists of a hardware part to physically test the software functionalities and to compare different hardware components such as sensors, busses, clocks, etc.. The candidate should have a good knowledge of the C/C++ programming languages and experience in embedded programming.
          </p>
          <p><a class="btn btn-primary btn-lg" href="mailto:jobs@pozyx.io?Subject=Embedded%20programmer" role="button">Apply now!</a></p>
        </div>  
        </div>

-->

<!--
<div id="software"></div>
      <div class="row" style=" margin-top: 80px;">      
        
         <div class="col-md-2 col-sm-4" style='text-align: center'>
          <img src="<?php echo(base_url('assets/images/hire.jpg')); ?>" style='width:100%; max-width:200px; margin-top: 20px'>
          </a>
        </div>

        <div class="col-md-10 col-sm-8">
        
          <h3>Software engineer</h3>
          <p>
          As a software engineer you are not just passionate about code, but what code stands for. It's a way of communicating between the machine and the person.
          You have an eye for perfection, understanding what the end user needs and are responsible for the end solution.<br>
          We are looking for someone that can help us design and develop the front-end application for our accurate positioning solution.
          </p>
          <p><a class="btn btn-primary btn-lg" href="mailto:jobs@pozyx.io?Subject=Software%20engineer" role="button">Apply now!</a></p>
        </div>
        </div>
-->        

        <!--
<div id="support"></div>
      <div class="row" style=" margin-top: 80px;">  


        <div class="col-md-2 col-sm-4" style='text-align: center'>
          <img src="<?php echo(base_url('assets/images/job.png')); ?>" style='width:100%; max-width:200px; margin-top: 20px'>
          </a>
        </div>


        <div class="col-md-10 col-sm-8">
        
          <h3>Demo &amp; Support engineer</h3>
          <p>
          We are looking for a person that is driven by helping out other people. In our case our customers. We offer a prototyping solution, so customers can implement 
          accurate positioning in there use-cases. A variety of use-cases comes with a custom support approach, you are able to detect the common issues and guide our 
          customers to the solution. Apart from the support you will be responsible for setting up test cases and demos for our application. It's still secret but if you 
          know how to play splinter cell, that's a plus.
          </p>
          <p><a class="btn btn-primary btn-lg" href="mailto:jobs@pozyx.io?Subject=support%engineer" role="button">Apply now!</a></p>
        </div>

        
        </div>
        --> 



<!--
<div id="network"></div>
      <div class="row" style=" margin-top: 80px;">

        <div class="col-md-2 col-sm-4" style='text-align: center'>
          <img src="<?php echo(base_url('assets/images/hire.jpg')); ?>" style='width:100%; max-width:200px; margin-top: 20px'>
          </a>
        </div>

        <div class="col-md-10 col-sm-8">
        
          <h3>Network engineer</h3>
          <p>
          You will not be travelling around to globe attending fares and networking events. With large scale projects the issue of size comes in to play. We are 
          looking for a person that can assist in building a network stack from scratch for the UWB technology. Allowing communication between nodes, cell detection  
          and many other requirements. If you can patch us, apply now.
        </p>
        <p><a class="btn btn-primary btn-lg" href="mailto:jobs@pozyx.io?Subject=network%20engineer" role="button">Apply now!</a></p>
        </div>

</div>
<div id="hardware"></div>
      <div class="row" style=" margin-top: 80px;">
        <div class="col-md-2 col-sm-4" style='text-align: center'>
          <img src="<?php echo(base_url('assets/images/hire.jpg')); ?>" style='width:100%; max-width:200px; margin-top: 20px'>
          </a>
        </div>


        <div class="col-md-10 col-sm-8">
        
          <h3>Hardware engineer</h3>
          <p>We are looking for someone to join the hardware team, it's not just designing the board based on the input of our customers and our CTO. But you are
            actively involved in this design process and decision process. You are in charge of the BOM, component selection, purchasing, production follow up, bad returns,
            and everything else that touches your hardware board. You have to ability to spot improvements both in hardware and software and are not afraid to speak up.
            <p><a class="btn btn-primary btn-lg" href="mailto:jobs@pozyx.io?Subject=hardware%20engineer" role="button">Apply now!</a></p>
          </p>
        </div>  

</div>
-->

<!--
<div id="sales"></div>
      <div class="row" style=" margin-top: 80px;">
        <div class="col-md-2 col-sm-4" style='text-align: center'>
          <img src="<?php echo(base_url('assets/images/hire.jpg')); ?>" style='width:100%; max-width:200px; margin-top: 20px'>
          </a>
        </div>

        <div class="col-md-10 col-sm-8">
        
          <h3>Sales &amp; project manager</h3>
          <p> Please fill in your job.
          </p>
          <p><a class="btn btn-primary btn-lg" href="mailto:jobs@pozyx.io?Subject=network%20engineer" role="button">Apply now!</a></p>
        </div> 
        
      </div> 

</div>
-->

      
</div> <!-- /container -->
