<script>
function openEmail(id){
    event.preventDefault();

    var ajax_call = $.ajax({url:'<?php echo(site_url("admin/dashboard/view_email")); ?>', method: "post", data:{emailID: id}, dataType: "json"});
    ajax_call.done( function(data){        
        swal({   
            title: data.subject,   
            html: data.message,
            width: 600     
        }); 
    });

    
}
</script>
<style>

.mailbox { background:#fff; padding:15px; margin:0 0 15px 0;}

.email_item { border:1px solid #ddd; margin:0 0 3px 0; font-size:12px; font-family:arial, sans-serif; background:#f3f3f3; padding:7px 10px;  -moz-border-radius:2px; -webkit-border-radius:2px; border-radius:2px; }
.email_item.read { background:#f3f3f3 url(email_open.png) no-repeat 8px center; padding-left:34px; }
.email_item.unread { border:1px solid #ccc; background:#e2e2e2 url(email.png) no-repeat 8px center; padding-left:34px;  }
.email_item.unread .subject { font-weight:bold; }
.email_item:hover {  }

.email_item .subject { display:block; float:left; width:45%; color:#222; line-height:1.2em; white-space:nowrap; overflow:hidden; }
.email_item .from { display:block; float:left; width:29%; padding-left:1%; color:#444; line-height:1.2em;  white-space:nowrap; overflow:hidden; }
.email_item .date { display:block; float:right; width:24%; padding-left:1%; color:#555; font-size:11px; line-height:1.3em; white-space:nowrap;  overflow:hidden; }
</style>


<?php

function decode_imap_text($str){
    $result = '';
    $decode_header = imap_mime_header_decode($str);
    foreach ($decode_header AS $obj) {
        $result .= htmlspecialchars(rtrim($obj->text, "\t"));
    }
    return $result;
}

// Configure your imap mailboxes
$mailboxes = array(
    array(
        'label'     => 'info@pozyx.io',
        'mailbox'   => '{mail.pozyx.io:143/notls}INBOX',
        'username'  => 'info+pozyx.io',
        'password'  => 'kickstart2015'
    ),   
    array(
        'label'     => 'sales@pozyx.io',
        'mailbox'   => '{mail.pozyx.io:143/notls}INBOX',
        'username'  => 'sales+pozyx.io',
        'password'  => 'kickstart2015'
    ),
    array(
        'label'     => $username.'@pozyx.io',
        'mailbox'   => '{mail.pozyx.io:143/notls}INBOX',
        'username'  => $username.'+pozyx.io',
        'password'  => 'kickstart2015'
    )     
);
?>

<div id="mailboxes">
<?php
foreach ($mailboxes as $current_mailbox) {
?>
    <div class="mailbox">
    <h3><?php echo $current_mailbox['label']; ?></h3>
    <?php     
    // Open an IMAP stream to our mailbox
    $stream = imap_open($current_mailbox['mailbox'], $current_mailbox['username'], $current_mailbox['password']);
         
    if (!$stream) { 
    ?>
        <p>Could not connect to: <?php echo $current_mailbox['label']; ?>. Error: <?php echo imap_last_error(); ?></p>

    <?php
    } else {
        // Get our messages from the last week
        $emails = imap_search($stream, 'SINCE '. date('d-M-Y',strtotime("-3 day")));
 
        // Instead of searching for this week's messages, you could search 
        // for all the messages in your inbox using: $emails = imap_search($stream, 'ALL');
         
        if (!count($emails)){
        ?>
            <p>No e-mails found.</p>
        <?php
        } else {
 
            // If we've got some email IDs, sort them from new to old and show them
            rsort($emails);
             
            foreach($emails as $email_id){
             
                // Fetch the email's overview and show subject, from and date. 
                $overview = imap_fetch_overview($stream,$email_id,0);                       
                ?>
                <div class="email_item clearfix <?=$overview[0]->seen?'read':'unread'?>"> <!-- add a different class for seperating read and unread e-mails -->
                    <span class="subject"><a href="" onclick='javascript:openEmail(<?php echo $email_id; ?>)'><?php echo decode_imap_text($overview[0]->subject); ?></a></span>
                    <span class="from"><?php echo decode_imap_text($overview[0]->from); ?></span>
                    <span class="date"><?php echo decode_imap_text($overview[0]->date); ?></span>
                </div>
                <?php
            } 
        } 
 
        // Close our imap stream.
        imap_close($stream); 
    }
    ?>
    </div>
<?php
} // end foreach 
?>
</div>
