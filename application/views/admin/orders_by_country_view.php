
<div class="container">
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

    <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('admin'); ?>">Admin</a> &gt;
            Orders by country
        </p>           
        </div>  

    <div class="col-md-12">
        <h3>Orders by country - 2016</h3>
        <p>All in euros</p>

        <table class='table table-condensed table-hover'>
        <thead><tr> 
        <td><b>Num</b></td>
        <td><b>Country code</b></td>
        <td><b>Country name</b></td>
        <td><b>Region</b></td>
        <td><b>count</b></td>
        <td style='text-align:right'><b>Gross revenue</b></td>
        <td style='text-align:right'><b>country tax</b></td>
        <td style='text-align:right'><b>Shipping costs</b></td>
        <td style='text-align:right'><b>Ship rate</b></td>
        </tr>
        </thead>
        <tbody>

        <?php
            $total_orders = 0;
            $total_gross = 0;
            $total_shipping = 0;
            $total_tax = 0;
         
            $i = 1;
            foreach ($shipping_rates as $rate) 
            {                        

                $total_orders = $total_orders + $rate->numCustomers;
                $total_gross = $total_gross + round($rate->country_total)/100.0;
                $total_shipping = $total_shipping + round($rate->country_shipping)/100.0;
                $total_tax = $total_tax + round($rate->country_tax)/100.0;

                echo "<tr><td>" . $i++ . "</td><td>". $rate->countryCode ."</td><td>". $rate->countryName ."</td><td>". $rate->region ."</td><td>";
                echo $rate->numCustomers;
                echo "</td><td style='text-align:right'>"; 

                //echo number_format((round($rate->rate_cents*0.89)/100.0),2,",",".");
                
                
                echo number_format((round($rate->country_total)/100.0),2,",",".");                
                echo "</td><td style='text-align:right'>"; 
                echo number_format((round($rate->country_tax)/100.0),2,",",".");                
                echo "</td><td style='text-align:right'>"; 
                echo number_format((round($rate->country_shipping)/100.0),2,",",".");
                echo "</td><td style='text-align:right'>"; 
                echo number_format((round($rate->shiprate)/100.0),2,",",".");

                //echo $rate->service . "<br>";
                echo "</td><tr>";
            }
         
        ?>

        <tr>
            <th colspan='4' style="text-align:right">TOTALS</th>
            <th><?php echo $total_orders; ?></th>
            <th style='text-align:right'><?php echo number_format($total_gross,2,",","."); ?></th>
            <th style='text-align:right'><?php echo number_format($total_tax,2,",","."); ?></th>
            <th style='text-align:right'><?php echo number_format($total_shipping,2,",","."); ?></th>
        </tr>

        </tbody></table>

    </div>

    </div>
</div>