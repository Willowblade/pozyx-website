

<link href="<?php echo(base_url('assets/css/bootstrap-datetimepicker.min.css')); ?>" rel="stylesheet">
<script src="<?php echo(base_url('assets/js/bootstrap-datetimepicker.min.js')); ?>"></script>
<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<div class="container">
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

    <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('admin'); ?>">Admin</a> &gt;
            <a href="<?php echo site_url('admin/dashboard/view_calendar'); ?>">Calendar</a> &gt;
            Add task
        </p>           
        </div>  

    <div class="col-md-12">
    <h3>Add task</h3>
    </div>

<form action="<?php echo site_url('admin/dashboard/task_add'); ?>" method="post">
        

        <?php
       
        $this->form_builder->text('title', 'Task title', '', 'col-xs-6');

        $options = array();
        foreach($users as $user){

            if($user->first_name != "Admin"){
                $user_option = array('id' => $user->id, 'name' => $user->first_name);            
                array_push($options, (object)$user_option);
            }
        }
        
        $this->form_builder->option('user_id', 'Assignee', $options, $userid, 'col-xs-6', 'form-control');

        $date = date('Y-m-d H:i'); 
        $this->form_builder->custom_element("<label>Start datum</label><div class='input-group date'><input type='text' value='".$date."' name='start_date' class='form-control form_datetime' ><span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span> </div>", 'col-xs-6');
        $date = date("Y-m-d H:i", strtotime("+1 week"));
        $this->form_builder->custom_element("<label>Eind datum</label><div class='input-group date'><input type='text' value='".$date."' name='end_date' class='form-control form_datetime' /><span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span> </div>", 'col-xs-6');


        echo "<div class='clearfix'></div>";
        $this->form_builder->textarea('description', 'Task description', '', 'col-xs-12', 'form-control', 5);
       

        echo "<div class='clearfix'></div>";

        $this->form_builder->button('submit_id', 'Add this task', 'col-xs-3', 'btn-primary', '', 'submit');
        ?>
</form>


</div>
</div>
</div>

<script type="text/javascript">
    $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
</script> 
