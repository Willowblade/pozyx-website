<div id="container">
  <h1>Welcome to the dashboard <?php echo $username; ?>!</h1>  
  <p>
  Last login <?php echo date('Y-m-d', $lastlogin); ?>
  </p>

  <h3>Options</h3>
  <ul>
    <?php if ($this->ion_auth->is_admin()) { ?>
    <li><a href="<?php echo site_url('admin/dashboard/orders'); ?>">View all orders</a> <span class="badge"><?php echo $num_orders; ?></span></li>            
    <?php } // end admin ?>
    <li><a href="<?php echo site_url('admin/dashboard/view_error_orders'); ?>">Orders in error</a> <span class="badge"><?php echo $num_error; ?></span></li>   
    <li><a href="<?php echo site_url('admin/dashboard/view_shipping_list'); ?>">Shipping list</a> <span class="badge"><?php echo $num_shipping; ?></span></li>
    <li><a href="<?php echo site_url('admin/dashboard/view_unpaid_shipped_orders'); ?>">Unpaid shipped orders</a> <span class="badge"><?php echo $num_unpaid_shipped; ?></span></li>
    <li><a href="<?php echo site_url('admin/dashboard/view_unpaid_orders'); ?>">Unpaid orders (WT)</a> <span class="badge"><?php echo $num_unpaid; ?></span></li>
    <li><a href="<?php echo site_url('admin/dashboard/view_open_quotes'); ?>">Open quotes</a> <span class="badge"><?php echo $num_open_quotes; ?></span></li>  
    <!--<li><a href="<?php echo site_url('admin/dashboard/view_emails'); ?>">View emails</a></li>  
    <li><a href="<?php echo site_url('admin/dashboard/view_calendar'); ?>">View calendar</a> <span class="badge"><?php echo $num_open_tasks; ?></span></li>         -->   
    <li><a href="<?php echo site_url('admin/dashboard/view_shipping_rates'); ?>">Shipping rates</a></li>   
    <li><a href="<?php echo site_url('admin/dashboard/view_orders_stats'); ?>">Order statistics</a></li> 

    <?php if ($this->ion_auth->is_admin()) { ?>
    <!--<li><a href="<?php echo site_url('admin/crud'); ?>">Go to the CRUD</a></li>      -->    
    <li><a href="<?php echo site_url('admin/dashboard/view_orders_by_country'); ?>">Orders by country</a></li>   
    <?php } // end admin ?>
    <li><a href="<?php echo site_url('admin/user/change_password'); ?>">Change password</a></li>    
    <li><a href="<?php echo site_url('admin/user/logout'); ?>">Logout</a></li>    

  </ul>

<?php if ($this->ion_auth->is_admin()) { ?>
  <div class="row">
  
  <?php
  foreach($order_data as $month){
    echo "<div class='col-md-3'>";
    echo "<a href='".site_url('admin/dashboard/search_orders?search_keyword='.$month["whatmonth"]."&search_in=month")."'><h3>". $month["whatmonth"] ."</h3></a>";
    echo $month["payed_orders"] . "/" . $month["orderTotal"] ." orders<br>";
    echo "Payed for: " .number_format($month["sum_payed"]/100,2,",",".") ."€<br>";
    echo "Open wire transfers: ". number_format($month["sum_unpayed"]/100,2,",",".") ."€<br>";
    echo "Quoted: " .number_format($month["sum_quoted"]/100,2,",",".") ."€<br>";    
    echo "Discounted: " .number_format($month["sum_discount"]/100,2,",",".") ."€<br>";
    echo "<br><b>Total: " .number_format($month["monthTotal"]/100,2,",",".") ." €</b>";    
    echo "</div>";
  }
  ?>
  </div>
  <p class='small'>All information above is excl. tax.</p>

  <br><br>

  <h3>Stock information</h3>
  <table class="table table-striped table-hover">
    <thead>
      <tr>
      <th style='text-align:center'>item</th>
      <th style='text-align:center'>physical stock</th>
      <th style='text-align:center'>quotes</th>
      <th style='text-align:center'>wire transfers</th>
      <th style='text-align:center'>undelivered sales</th>
      <th style='text-align:center'>remaining available</th>
      </tr>
    </thead>
    <tbody>
      <?php
        $this->load->model('order_model');
        if($stock_info){
          foreach ($stock_info as $product){
            echo "<tr>";
            foreach ($product as $product_info) {
              echo "<td style='text-align:center'>".  $product_info ."</td>";
            }
            echo "</tr>";
          }
        }
      ?>      

    </tbody>
  </table>


  <?php } // end admin ?>

  


</div>
 