<script src="<?php echo(base_url('assets/js/highcharts/highcharts.js'));?>"></script>
<script src="<?php echo(base_url('assets/js/highcharts/modules/exporting.js'));?>"></script>
<script src="<?php echo(base_url('assets/js/highcharts/modules/offline-exporting.js'));?>"></script>
<script src="<?php echo(base_url('assets/js/highcharts/modules/export-csv.js'));?>"></script>

<script src="<?php echo(base_url('assets/js/highmaps/map.js'));?>"></script>
<script src="<?php echo(base_url('assets/js/highmaps/modules/data.js'));?>"></script>
<script src="<?php echo(base_url('assets/js/highmaps/mapdata/custom/world.js'));?>"></script>

<div class="container">
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

    <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('admin'); ?>">Admin</a> &gt;
            Order statistics
        </p>           
        </div>  

    <div class="col-md-3 form-group">
        <h3>Statistic options</h3>

        <label>Select data range</label>
        <select type="sector" id="data_range" name="data_range" class="form-control">
          <option value="thisyear" <?php if($option == "thisyear") echo("selected"); ?>>this year</option>
          <option value="lastyear" <?php if($option == "lastyear") echo("selected"); ?>>last year</option>
          <option value="alltime" <?php if($option == "alltime") echo("selected"); ?>>all time</option>
          <option value="last3months" <?php if($option == "last3months") echo("selected"); ?>>last 3 months</option>
          <option value="last6months" <?php if($option == "last6months") echo("selected"); ?>>last 6 months</option>
          <option value="last12months" <?php if($option == "last12months") echo("selected"); ?>>last 12 months</option>
          
        </select>
        <br>

        <button class="btn btn-success btn-lg btn-block" id='form_submit_btn' onclick="javascript:change_range()">Select</button>
    </div>  

    <div class="clearfix">
        
    </div>  

    <div class="col-md-12">

        <h3>Orders per month</h3>
        <div id="sales_per_month_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        <div id="sales_per_quarter_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

        <h3>Sales insight</h3>
        <div id="sales_per_sector_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        <div id="sales_per_tracking_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        <div id="customertype_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

        <h3>Geographical stats</h3>        
        <div id="sales_per_country_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        <div id="worldmap_sales_chart" style="min-width: 310px; height: 400px;  margin: 0 auto"></div>
        <div id="sales_per_region_chart" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>

        <h3>Types of orders</h3>
        <div id="types_of_sales_chart" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
        <div id="types_of_device_chart" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>

        <h3>Payment methods</h3>      
        <div id="payment_method_chart" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>  
        <div id="monthly_wiretransfer_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>        

        <h3>Order behavior<h3>
        <div id="orders_per_day_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        <div id="orders_per_hour_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        
        <h3>Shipping stats<h3>
        <div id="shipping_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        <div id="monthly_shipping_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

    </div>

    </div>
</div>

<script>
function change_range()
{
    //alert('<?php echo site_url('admin/dashboard/view_orders_stats'); ?>/' + encodeURIComponent($("#data_range").val()));
    window.location.href='<?php echo site_url('admin/dashboard/view_orders_stats'); ?>/' + encodeURIComponent($("#data_range").val());
}

/**
 * Pie title plugin
 * Author: Torstein Hønsi
 * Original: http://jsfiddle.net/highcharts/tnSRA/
 * Last revision: 2015-08-31
 */
(function (Highcharts) {
    Highcharts.seriesTypes.pie.prototype.setTitle = function (titleOption) {
        var chart = this.chart,
            center = this.center || (this.yAxis && this.yAxis.center),
            labelBox,
            box,
            format;
        
        if (center && titleOption) {
            box = {
                x: chart.plotLeft + center[0] - 0.5 * center[2],
                y: chart.plotTop + center[1] - 0.5 * center[2],
                width: center[2],
                height: center[2]
            };
            
            format = titleOption.text || titleOption.format;
            format = Highcharts.format(format, this);

            if (this.title) {
                this.title.attr({
                    text: format
                });
                
            } else {
                this.title = this.chart.renderer.label(format)
                    .css(titleOption.style)
                    .add()
            }
            labelBBox = this.title.getBBox();
            titleOption.width = labelBBox.width;
            titleOption.height = labelBBox.height;
            this.title.align(titleOption, null, box);
        }
    };
    
    Highcharts.wrap(Highcharts.seriesTypes.pie.prototype, 'render', function (proceed) {
        proceed.call(this);
        this.setTitle(this.options.title);
    });

    } (Highcharts));


/* ------------------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------------------ */    
/* Start the charts */
$(function () {

    Highcharts.chart('sales_per_month_chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Sales per month'
        },
        xAxis:{
            type: 'datetime',
            tickInterval: 30 * 24 * 3600 * 1000,
         
        },        
        yAxis: [{
            min: 0,
            title: {
                text: 'Number of customers'
            }
        },{
            title: {
                text: 'Revenue (Euro)'
            },
            opposite: true
        }],
        tooltip: {
            formatter: function() {
                var tip = '<b>';
                var stackSelected = this.point.series.options.stack;
                var categorySelected = this.point.category;

                tip += stackSelected+'</b>';
                tip += '<br/><br/>';

                var index = 0;
                var stackTotal = 0;
                $.each(this.series.chart.series, function(i, s) {
                    if(s.options.stack == stackSelected){

                        $.each(s.data, function(j, point){
                            if(point.category == categorySelected){
                                tip += s.name+' : <b>'+point.y+'</b><br>';
                                stackTotal += point.y;
                            }
                        });
                    }

                });

                tip += '<br>Total: <b>' + stackTotal + '</b>';

                return tip;
            },
            shared : false
            /*shared: false,
            useHTML: true,
            /*
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }*/
        },
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'web orders',
            stack: 'Orders',
            tooltip: {
                valueSuffix: ' customers'
            },             
            data: [
                <?php
                    foreach ($monthly_webstats as $stat) 
                    {
                        
                        echo "[Date.UTC(". $stat->year.",".($stat->month_num-1).",1),".$stat->numCustomers ."],";
                        //echo $stat->numCustomers.",";
                    }
                ?> 
            ]

        },
<?php if ($this->ion_auth->is_admin()) { ?>
        {
            name: 'web revenue (€)',
            stack : 'Revenue',
            data: [<?php
                    foreach ($monthly_webstats as $stat) 
                    {
                        //echo($stat->monthly_total/100.0).",";
                        echo "[Date.UTC(". $stat->year.",".($stat->month_num-1).",1),".$stat->monthly_total/100.0 ."],";
                    }
                ?>],
            tooltip: {
                valueSuffix: ' euro'
            },  
            yAxis: 1
        },
<?php } ?>
        {
            name: 'quote orders',
            stack: 'Orders',
            tooltip: {
                valueSuffix: ' customers'
            },             
            data: [
                <?php
                    foreach ($monthly_quotestats as $stat) 
                    {
                        echo "[Date.UTC(". $stat->year.",".($stat->month_num-1).",1),".$stat->numCustomers ."],";
                        //echo $stat->numCustomers.",";
                    }
                ?> 
            ]

        },
<?php if ($this->ion_auth->is_admin()) { ?>
        {
            name: 'quote Revenue (€)',
            stack : 'Revenue',
            data: [<?php
                    foreach ($monthly_quotestats as $stat) 
                    {
                        //echo($stat->monthly_total/100.0).",";
                        echo "[Date.UTC(". $stat->year.",".($stat->month_num-1).",1),".$stat->monthly_total/100.0 ."],";
                    }
                ?>],
            tooltip: {
                valueSuffix: ' euro'
            },  
            yAxis: 1
        }
<?php } ?>
        ]
    });



    Highcharts.chart('sales_per_quarter_chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Web sales per quarter'
        },
        xAxis: {
            categories: [
                <?php
                    foreach ($quarterly_stats as $stat) 
                    {
                        echo "'".$stat->year."Q".$stat->quarter."',";
                    }
                ?>                 
            ],
            crosshair: true
        },
        yAxis: [{
            min: 0,
            title: {
                text: 'Number of customers'
            }
        },{
            title: {
                text: 'Revenue (Euro)'
            },
            opposite: true
        }],
        tooltip: {
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Customers',
            tooltip: {
                valueSuffix: ' customers'
            },  
            data: [
                <?php
                    foreach ($quarterly_stats as $stat) 
                    {
                        echo $stat->numCustomers.",";
                    }
                ?> 
            ]

        },
<?php if ($this->ion_auth->is_admin()) { ?>
        {
            name: 'Revenue',
            data: [<?php
                    foreach ($quarterly_stats as $stat) 
                    {
                        echo $stat->quarterly_total/100.0 .",";
                    }
                ?>],
            tooltip: {
                valueSuffix: ' euro'
            },  
            yAxis: 1
        }
<?php } ?>
        ]
    });


    /* use cases */
    Highcharts.chart('sales_per_sector_chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Sales per sector'
        },
        xAxis: {
            categories: [
                <?php
                    foreach ($sector_stats as $stat) 
                    {
                        echo "'".$stat->sector."',";
                    }
                ?>                
            ],
            crosshair: true
        },
        yAxis: [{
            min: 0,
            title: {
                text: 'Number of customers'
            }
        },{
            title: {
                text: 'Revenue (Euro)'
            },
            opposite: true
        }],
        tooltip: {
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Customers',
            tooltip: {
                valueSuffix: ' customers'
            },  
            data: [
                <?php
                    foreach ($sector_stats as $stat) 
                    {
                        echo $stat->numCustomers.",";
                    }
                ?> 
            ]

        },
<?php if ($this->ion_auth->is_admin()) { ?>
        {
            name: 'Revenue',
            data: [<?php
                    foreach ($sector_stats as $stat) 
                    {
                        echo $stat->sector_total/100.0 .",";
                    }
                ?>],
            tooltip: {
                valueSuffix: ' euro'
            },  
            yAxis: 1
        }
<?php } ?>
        ]
    });

    Highcharts.chart('sales_per_tracking_chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Sales per sector'
        },
        xAxis: {
            categories: [
                <?php
                    foreach ($tracking_stats as $stat) 
                    {
                        echo "'".$stat->tracking."',";
                    }
                ?> 
            ],
            crosshair: true
        },
        yAxis: [{
            min: 0,
            title: {
                text: 'Number of customers'
            }
        },{
            title: {
                text: 'Revenue (Euro)'
            },
            opposite: true
        }],
        tooltip: {
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Customers',
            tooltip: {
                valueSuffix: ' customers'
            },  
            data: [
                <?php
                    foreach ($tracking_stats as $stat) 
                    {
                        echo $stat->numCustomers.",";
                    }
                ?> 
            ]

        },
<?php if ($this->ion_auth->is_admin()) { ?>
        {
            name: 'Revenue',
            data: [<?php
                    foreach ($tracking_stats as $stat) 
                    {
                        echo $stat->tracking_total/100.0 .",";
                    }
                ?>],
            tooltip: {
                valueSuffix: ' euro'
            },  
            yAxis: 1
        }
<?php } ?>
        ]
    });



/* Pie Chart to see which type of customer */
    Highcharts.chart('customertype_chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
        },
        title: {
            text: 'Customer type'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            type: 'pie',
            name: 'number of customers',
            colorByPoint: true,
            center: ['25%', null],
            size: '60%',
            title: {
                align: 'center',
                format: '<b>{name}</b>',
                verticalAlign: 'top',
                y: -40
            },
            dataLabels: {
                enabled: false
            },
            showInLegend: true,
            data: [
            <?php
                foreach ($customertype_stats as $stats) 
                {
                    echo "{name: \"" . $stats->customer_type ."\" ,y:".$stats->numCustomers."},";
                }
            ?>
            ]
        },{
            type: 'pie',
            name: 'Revenue',
            colorByPoint: true,
            center: ['75%', null],
            size: '60%',
            title: {
                align: 'center',
                format: '<b>{name}</b>',
                verticalAlign: 'top',
                y: -40
            },
            dataLabels: {
                enabled: false
            },            
            data: [
            <?php
                foreach ($customertype_stats as $stats) 
                {
                    echo "{name: \"" . $stats->customer_type ."\" ,y:".$stats->total."},";
                }
            ?>
            ]
        }

        ]
    },
    function(chart) {
            
        $(chart.series[0].data).each(function(i, e) {
            e.legendItem.on('click', function(event) {
                var legendItem=e.name;
                
                event.stopPropagation();
                
                $(chart.series).each(function(j,f){
                       $(this.data).each(function(k,z){
                           if(z.name==legendItem)
                           {
                               if(z.visible)
                               {
                                   z.setVisible(false);
                               }
                               else
                               {
                                   z.setVisible(true);
                               }
                           }
                       });
                });
                
            });
        });
    });

    /* Bar chart for the sales per country */
    Highcharts.chart('sales_per_country_chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Web sales per country'
        },
        xAxis: {
            categories: [
                <?php
                    foreach ($shipping_rates as $rate) 
                    {
                        echo "'".$rate->countryName."',";
                    }
                ?>                
            ],
            crosshair: true
        },
        yAxis: [{
            min: 0,
            title: {
                text: 'Number of customers'
            }
        },{
            title: {
                text: 'Revenue (Euro)'
            },
            opposite: true
        }],
        tooltip: {
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Customers',
            tooltip: {
                valueSuffix: ' customers'
            },  
            data: [
                <?php
                    foreach ($shipping_rates as $rate) 
                    {
                        echo $rate->numCustomers.",";
                    }
                ?>

            ]

        },
<?php if ($this->ion_auth->is_admin()) { ?>
        {
            name: 'Revenue',
            data: [
                <?php
                    foreach ($shipping_rates as $rate) 
                    {
                        echo $rate->country_total/100.0.",";
                    }
                ?>
            ],
            tooltip: {
                valueSuffix: ' euro'
            },  
            yAxis: 1
        }
<?php } ?>
        ]
    });

    data = [
    <?php
        foreach ($shipping_rates as $rate) 
        {                                      
            echo "{'code':'". $rate->countryCode ."',";
            echo "'value':" . $rate->numCustomers . ",";
            echo "'revenue':'". $rate->country_total ."',";
            echo "'name':'". $rate->countryName ."'},";
        }
    ?>
    ];  

    // Initiate the chart
    Highcharts.mapChart('worldmap_sales_chart', {
        chart: {
            map: 'custom/world'
        },
        title: {
            text: 'number of customers'
        },
        legend: {
            enabled: false
        },
        colorAxis: {
            dataClasses: [{
                to: 3
            }, {
                from: 3,
                to: 10
            }, {
                from: 10,
                to: 30
            }, {
                from: 30,
                to: 100
            }, {
                from: 100
            }]
        },
        legend: {
            title: {
                text: 'Sales per country',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                }
            },
            align: 'left',
            verticalAlign: 'bottom',
            floating: true,
            layout: 'vertical',
            valueDecimals: 0,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255, 255, 255, 0.85)',
            symbolRadius: 0,
            symbolHeight: 14
        },

        mapNavigation: {
            enabled: true,
            enableDoubleClickZoomTo: true
        },
        tooltip: {
            formatter: function(){
                var s = '<b>' + this.key + '</b><br/>';
                s += 'Number of customers: <b>' + this.point.value + '</b><br/>';
<?php if ($this->ion_auth->is_admin()) { ?>
                s += 'Revenue: <b>' + this.point.revenue/100.0 + ' euro</b>';
<?php } ?>
                return s;
            },
        },        
        credits: {
            enabled: false
        },
        series: [{
            enableMouseTracking: true,
            data: data,            
            joinBy: ['iso-a2', 'code'],
            name: 'worldwide sales',
            states: {
                hover: {
                    color: '#BADA55'
                }
            },                       
        }]
    });


/* Pie Chart to see which type of kit we sell the most */
    Highcharts.chart('sales_per_region_chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Revenues per region'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Revenue',
            colorByPoint: true,
            data: [
            <?php
                foreach ($region_stats as $stats) 
                {
                    echo "{name: \"" . $stats->region ."\" ,y:".$stats->region_total/100.0."},";
                }
            ?>
            ]
        }]
    });



    /* Pie Chart to see which type of kit we sell the most */
    Highcharts.chart('types_of_sales_chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Product distribution'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b><br>Number of units: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Products',
            colorByPoint: true,
            data: [
            <?php
                foreach ($product_stats as $stats) 
                {
                    echo "{name: \"" . $stats->product_name ."\" ,y:".$stats->numProducts."},";
                }
            ?>
            ]
        }]
    });

    /* Pie Chart to see how many anchors and tags are sold*/
    Highcharts.chart('types_of_device_chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Devices shipped'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b><br>Number of units: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %<br>Number of units: <b>{point.y}</b>',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Products',
            colorByPoint: true,
            data: [
            <?php
                foreach ($anchortag_stats as $stats) 
                {
                    //print_r($stats);
                    echo "{name: 'anchor' ,y:".$stats->numAnchors."},";
                    echo "{name: 'tag' ,y:".$stats->numTags."},";
                }
            ?>
            ]
        }]
    });

    /* Pie Chart to see which type of payment method */
    Highcharts.chart('payment_method_chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Payment method distribution'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Payment method',
            colorByPoint: true,
            data: [
            <?php
                foreach ($paymentmethod_stats as $stats) 
                {
                    echo "{name: '" . $stats->payment_method ."' ,y:".$stats->numCustomers."},";
                }
            ?>
            ]
        }]
    });


/* Show monthly wire transfer rate */
    Highcharts.chart('monthly_wiretransfer_chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Wire transfer payment chart'
        },
        xAxis: {
            categories: [
                <?php
                    foreach ($wiretransfer_stats as $stat) 
                    {
                        echo "'".$stat->month."',";
                    }
                ?>    
            ]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of wire transfers'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Within 3 days',
            color: '#8ee77c',
            data: [
                <?php
                    foreach ($wiretransfer_stats as $stat) 
                    {
                        echo $stat->cat1.",";
                    }
                ?>    
            ]
        }, {
            name: '4 to 7 days',
            color: 'green',
            data: [
            <?php
                    foreach ($wiretransfer_stats as $stat) 
                    {
                        echo $stat->cat2.",";
                    }
                ?> 
            ]
        }, {
            name: '1 to 3 weeks',
            color: 'orange',
            data: [
            <?php
                    foreach ($wiretransfer_stats as $stat) 
                    {
                        echo $stat->cat3.",";
                    }
                ?> 
            ]
        }, {
            name: 'More than 3 weeks',
            color: '#d11b1b',
            data: [
            <?php
                    foreach ($wiretransfer_stats as $stat) 
                    {
                        echo $stat->cat4.",";
                    }
                ?> 
            ]
        }, {
            name: 'Still open',
            color: '#610e0e',
            data: [
            <?php
                    foreach ($wiretransfer_stats as $stat) 
                    {
                        echo $stat->cat5.",";
                    }
                ?> 
            ]
        }]
    });

/* The number of orders per day */
Highcharts.chart('orders_per_day_chart', {
        chart: {
            type: 'areaspline'
        },
        title: {
            text: 'Average number of orders during one week'
        },
         legend: {
            enabled: false
        },
        xAxis: {
            categories: [
                <?php
                    foreach ($week_stats as $stat) 
                    {
                        echo "'".$stat->day_of_week."',";
                    }
                ?>  
            ],
            plotBands: [{ // visualize the weekend
                from: 4.5,
                to: 6.5,
                color: 'rgba(68, 170, 213, .2)'
            }]
        },
        yAxis: {
            title: {
                text: 'Number of orders'
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' units'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        series: [{
            name: 'Orders',
            data: [
                <?php
                    foreach ($week_stats as $stat) 
                    {
                        echo $stat->avg_orders.",";
                    }
                ?> 

            ]
        }]
    });


/* The number of orders per hour */
Highcharts.chart('orders_per_hour_chart', {
        chart: {
            type: 'areaspline'
        },
        title: {
            text: 'Average number of orders per hour'
        },
         legend: {
            enabled: false
        },        
        xAxis: {
            type: 'datetime',
            tickInterval: 3600 * 1000,
            min: Date.UTC(2013,4,22),
            max: Date.UTC(2013,4,22,23),
            dateTimeLabelFormats: { hour: '%H:%M', day: '%H:%M', },
            plotBands: [{ // visualize the weekend
                from: Date.UTC(2013,4,22,8),
                to: Date.UTC(2013,4,22,17),
                color: 'rgba(68, 170, 213, .2)'
            }]
        }, 
        yAxis: {
            title: {
                text: 'Number of orders'
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' units'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        series: [{
            name: 'Orders',
            data: [
                <?php
                    foreach ($hour_stats as $stat) 
                    {
                        echo "[Date.UTC(2013,4,22,". $stat->hour_of_day.",30),".$stat->numCustomers."],";
                    }
                ?> 

            ]
        }]
    });

/* Pie Chart to see which type of kit we sell the most */
    Highcharts.chart('shipping_chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Shipping delay'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Shipping delay',
            colorByPoint: true,
            data: [
            <?php
                $colors = array("#8ee77c", "green", "orange", "#d11b1b", "#610e0e");
                $i = 0;
                foreach ($shipping_stats as $stats) 
                {
                    echo "{name: \"" . $stats->shipping_time ."\" ,y:".$stats->numOrders.", color: '".$colors[$i++]."'},";
                }
            ?>
            ]
        }]
    });

/* Bar chart to see the  */
    Highcharts.chart('monthly_shipping_chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Monthly shipping chart'
        },
        xAxis: {
            categories: [
                <?php
                    foreach ($monthly_shipping_stats as $stat) 
                    {
                        echo "'".$stat->month."',";
                    }
                ?>    
            ]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'percentage of shipped orders'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'percent'
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Up to 3 days',
            color: '#8ee77c',
            data: [
                <?php
                    foreach ($monthly_shipping_stats as $stat) 
                    {
                        echo $stat->cat1.",";
                    }
                ?>    
            ]
        }, {
            name: '4 to 7 days',
            color: 'green',
            data: [
            <?php
                    foreach ($monthly_shipping_stats as $stat) 
                    {
                        echo $stat->cat2.",";
                    }
                ?> 
            ]
        }, {
            name: '1 to 2 weeks',
            color: 'orange',
            data: [
            <?php
                    foreach ($monthly_shipping_stats as $stat) 
                    {
                        echo $stat->cat3.",";
                    }
                ?> 
            ]
        }, {
            name: '2 to 4 weeks',
            color: '#d11b1b',
            data: [
            <?php
                    foreach ($monthly_shipping_stats as $stat) 
                    {
                        echo $stat->cat4.",";
                    }
                ?> 
            ]
        }, {
            name: 'More than 4 weeks',
            color: '#610e0e',
            data: [
            <?php
                    foreach ($monthly_shipping_stats as $stat) 
                    {
                        echo $stat->cat5.",";
                    }
                ?> 
            ]
        }]
    });

});
</script>