
<div class="container">
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

    <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('admin'); ?>">Admin</a> &gt;
            Calendar
        </p>           
        </div>  

    <div class="col-md-6">
<h3>Calendar</h3>

<table class='table table-condensed table-hover'>
<thead><tr> 
<td><b>Day</b></td>
<td><b>Date</b></td>
<td><b>Time</b></td>
<td><b>Description</b></td>
</tr>
</thead>
<tbody>

<?php
$calTimeZone = $events->timeZone; //GET THE TZ OF THE CALENDAR

//SET THE DEFAULT TIMEZONE SO PHP DOESN'T COMPLAIN. SET TO YOUR LOCAL TIME ZONE.
 date_default_timezone_set($calTimeZone);
 
 //START THE LOOP TO LIST EVENTS
    foreach ($events->getItems() as $event) {
 
        //Convert date to month and day
 
         $eventDateStr = $event->start->dateTime;
         if(empty($eventDateStr))
         {
             // it's an all day event
             $eventDateStr = $event->start->date;
         }
 
         $temp_timezone = $event->start->timeZone;
 		 //THIS OVERRIDES THE CALENDAR TIMEZONE IF THE EVENT HAS A SPECIAL TZ
         if (!empty($temp_timezone)) {
            $timezone = new DateTimeZone($temp_timezone); //GET THE TIME ZONE
            //Set your default timezone in case your events don't have one
         } else { 
            $timezone = new DateTimeZone($calTimeZone);
         }
 
         $eventdate = new DateTime($eventDateStr,$timezone);
 		 $link = $event->htmlLink;
         $TZlink = $link . "&ctz=" . $calTimeZone; //ADD TZ TO EVENT LINK
				 							//PREVENTS GOOGLE FROM DISPLAYING EVERYTHING IN GMT
         $day = $eventdate->format("l");//CONVERT REGULAR EVENT DATE TO LEGIBLE MONTH
         $date = $eventdate->format("j M");//CONVERT REGULAR EVENT DATE TO LEGIBLE MONTH
         $hour = $eventdate->format("H:i");//CONVERT REGULAR EVENT DATE TO LEGIBLE MONTH
 
?>
        <tr>
        <td><?php echo $day; ?></td>
        <td><?php echo $date; ?></td>
        <td><?php echo $hour; ?></td>
        <td>
        <a href="<?php echo $TZlink;?>">    <?php echo $event->summary; //SUMMARY = TITLE       ?>        </a>
        </td></tr>
 <?php
  }
 
?>

</tbody></table>

</div>
<div class="col-md-6">
<h3>Task list</h3>

<a href='<?php echo site_url('admin/dashboard/task_add'); ?>' class='btn btn-primary'><span class="glyphicon glyphicon-plus"></span>  Add task</a>
<?php if($all_users == 1){ ?>
<a href='<?php echo site_url('admin/dashboard/view_calendar/0'); ?>' class='btn btn-primary'><span class="glyphicon glyphicon-user"></span>  See all</a>
<?php }else{ ?>
<a href='<?php echo site_url('admin/dashboard/view_calendar/1'); ?>' class='btn btn-primary'><span class="glyphicon glyphicon-user"></span>  Just me</a>
<?php } ?>

<br><br>


<table class='table table-condensed table-hover'>
<thead><tr> 
<td width='30%'><b>Start</b></td>
<td><b>Age</b></td>
<td width='45%'><b>Task</b></td>
<td><b>Assignee</b></td>
</tr>
</thead>
<tbody>

<?php

foreach($task_data as $task){
    echo "<tr id='taskrow".$task->taskid."'><td>";
    echo $task->day_start;
    echo "</td><td>";
    echo $task->daysold;
    echo "</td><td><a href='' onclick='seeTask(".$task->taskid."); return false' id='task_title".$task->taskid."'>";
    echo $task->title;
    echo "</a></td><td>";
    echo $task->first_name;
    echo "<div style='display:none' id='task".$task->taskid."'>";
    echo $task->description;
    echo "</div></td></tr>";
}

?>

</tbody>
</table>

<p>You have <?php echo $num_open_tasks; ?> open tasks.<br>
You have <?php echo $num_completed_tasks; ?> completed tasks.</p>


</div>
</div>
</div>

<script>

function seeTask(id){

    swal({   
      title: $("#task_title" + id).text(),   
      html: $("#task" + id).html(),                
      type: "info",   
      width: 600,
      showCancelButton: true,   
      confirmButtonColor: '#3085d6',   
      cancelButtonColor: '#d33',   
      confirmButtonText: 'Yes, Complete it!',   
      closeOnConfirm: false 
   },function() {   

        $.post( "<?php echo site_url('admin/dashboard/complete_task'); ?>", { task_id: id } );

        $("#taskrow"+id).remove();

        swal(     'Completed!',     'You completed a task. Good job bro!',     'success'   ); 
    });
}
</script>  