<div class="row">
  <div class="col-lg-4 col-lg-offset-4">
    <h1><?php echo lang('change_password_heading');?></h1>
    <?php echo $this->session->flashdata('message');?>
    <?php echo form_open('');?>
      <div class="form-group">   

        <?php echo lang('change_password_old_password_label', 'old_password');?> <br />
        <?php echo form_error('old');?>
        <?php echo form_input($old_password,'','class="form-control"');?>

      </div>
      <div class="form-group">
        
        <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label> <br />
        <?php echo form_error('new');?>
        <?php echo form_input($new_password,'','class="form-control"');?>

      </div>

      <div class="form-group">

        <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?> <br />
        <?php echo form_error('new_confirm');?>
        <?php echo form_input($new_password_confirm,'','class="form-control"');?>

      </div>

      <?php echo form_input($user_id);?>
      <?php echo form_submit('submit', lang('change_password_submit_btn'), 'class="btn btn-primary btn-lg btn-block"');?>
    <?php echo form_close();?>
  </div>
</div>

