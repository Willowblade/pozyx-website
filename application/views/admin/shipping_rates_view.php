<script src="<?php echo(base_url('assets/js/highmaps/highmaps.js'));?>"></script>
<script src="<?php echo(base_url('assets/js/highmaps/modules/data.js'));?>"></script>
<script src="<?php echo(base_url('assets/js/highmaps/modules/exporting.js'));?>"></script>
<script src="<?php echo(base_url('assets/js/highmaps/modules/offline-exporting.js'));?>"></script>
<script src="<?php echo(base_url('assets/js/highmaps/mapdata/custom/world.js'));?>"></script>


<div class="container">
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

    <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('admin'); ?>">Admin</a> &gt;
            Shipping rates
        </p>           
        </div>  

    <div class="col-md-12">
        <h3>Worldwide shipping rates</h3>

        <div id="worldmap_chart" style="max-width: 1000px"></div>

        <h3>Shipping rate list</h3>

        <table class='table table-condensed table-hover'>
        <thead><tr> 
        <td><b>Num</b></td>
        <td><b>Country code</b></td>
        <td><b>Country name</b></td>
        <td><b>Rates</b></td>
        </tr>
        </thead>
        <tbody>

        <?php
            $country = "";
            $region = "";
            $i = 1;
            foreach ($shipping_rates as $rate) 
            {                        
                if($country != $rate->countryCode )
                {                    
                    if($i != 1)  {
                        echo "</td><tr>";
                    }

                    if($region != $rate->region)
                    {
                        echo "<tr><td colspan=5><h3>". $rate->region ."</h3></td></tr>";
                        $region = $rate->region;
                    }
                    echo "<tr><td>" . $i++ . "</td><td>". $rate->countryCode ."</td><td>". $rate->countryName ."</td><td>";
                    $country = $rate->countryCode;
                }



                echo number_format((round($rate->rate_cents)/100.0),2,",",".");
                echo " EUR " . $rate->service . "<br>";
            }
         
        ?>
        </td></tr>
        </tbody></table>

    </div>

    </div>
</div>

<script>
$(function () {

    data = [
    <?php
        $country = "";
        foreach ($shipping_rates as $rate) 
        {                        
            if($country != $rate->countryCode )
            {                 
                if($rate->rate_cents == 0)
                    continue;
                echo "{'code':'". $rate->countryCode ."',";
                echo "'value':" . number_format((round($rate->rate_cents)/100.0),2,".","") . ",";
                echo "'service':'". $rate->service ."',";
                echo "'name':'". $rate->countryName ."'},";
                $country = $rate->countryCode;
            }
        }
     
    ?>
    ];  

    // Initiate the chart
    Highcharts.mapChart('worldmap_chart', {

        title: {
            text: 'shipping rates in euro'
        },

        mapNavigation: {
            enabled: true,
            enableDoubleClickZoomTo: true
        },
        tooltip: {
            formatter: function(){
                var s = '<b>' + this.key + '</b><br/>';
                s += 'Price: <b>' + this.point.value + ' euro</b><br/>';
                s += 'Service: ' + this.point.service;
                return s;
            },
        },
        colorAxis: {
            min: 10
            
        },
        credits: {
            enabled: false
        },
        series: [{
            data: data,
            mapData: Highcharts.maps['custom/world'],
            joinBy: ['iso-a2', 'code'],
            name: 'shipping rate',
            states: {
                hover: {
                    color: '#BADA55'
                }
            },
            tooltip: {
                valueSuffix: ' euro'
            }
        }]
    });

});
</script>