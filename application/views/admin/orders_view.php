<script>
function payment_check(id){
	event.preventDefault();

	swal({   
		title: "Are you sure?",   
		text: "Confirm that the payment is completed! This will send a confirmation email to the customer.",   
		type: "warning",   
		showCancelButton: true,     
		confirmButtonText: "Yes, payment complete!",   
		closeOnConfirm: true },
	function(){   
		var ajax_call = $.ajax({url:'<?php echo(site_url("admin/dashboard/order_paid")); ?>', method: "post", data:{orderID: id}});
		ajax_call.done( function(data){
			$('#row'+id).removeClass('warning').addClass('success');
		});
	});	
}

function edit_remark(id, remarks){
	event.preventDefault();

	swal({   
        title: "Edit remark",  
        html: "<input id='input-field2' class='dialogInput' value='"+remarks+"'>",   
        type: "warning",
        showCancelButton: true,
		confirmButtonClass: "btn-danger",
		closeOnConfirm: true,           
    }, 
    function(){  
        inputValue = $('#input-field2').val(); 
		if (inputValue === false) return false;      
		if (inputValue === "") {      
            return false;   
        }   
        
        var ajax_call = $.ajax({url:'<?php echo(site_url("admin/dashboard/order_edit_remark")); ?>', method: "post", data:{orderID: id, remarks: inputValue}});
		ajax_call.done( function(data){
			$('#remarkbtn_'+id).remove();
			$('#viewbtn_'+id).append(" <a onclick='javascript:sweetAlert(\"Remark\", \""+inputValue+"\"); return false' href='' title='view remark'><span class='glyphicon glyphicon-bell'></span></a>");      
        });
	});
}

function delete_order(id,remarks){
	event.preventDefault();

	if (remarks === ""){
		remarks = "Remark";
	}

	swal({   
        title: "Are you sure?",  
        html: "Are you sure you wish to (soft) delete order "+id+"?<br><br><input id='input-field2' class='dialogInput' placeholder='"+remarks+"'>",   
        type: "warning",
        showCancelButton: true,
		confirmButtonClass: "btn-danger",
		confirmButtonText: "Yes, delete it!",
		closeOnConfirm: true,           
    }, 
    function(){  
        inputValue = $('#input-field2').val(); 
		if (inputValue === false) return false;      
		if (inputValue === "" && remarks === "Remark") {  
			swal({
			  type: "error",
			  html: "Reason required!",
			  showCloseButton: true
			});   

		  	//This returns an error need to check with Sam
            swal.showValidationError("Reason required!");     
            return false;   
        }   

        var ajax_call = $.ajax({url:'<?php echo(site_url("admin/dashboard/order_delete")); ?>', method: "post", data:{orderID: id, remarks: inputValue}});
		ajax_call.done( function(data){
			// remove the row
			$('#row'+id).remove();         
        });
	});


/*
	swal({
		title: "Are you sure?",
		text: "Are you sure you wish to (soft) delete this order?",
		type: "warning",
		showCancelButton: true,
		confirmButtonClass: "btn-danger",
		confirmButtonText: "Yes, delete it!",
		closeOnConfirm: true
	},
	function(){
		var ajax_call = $.ajax({url:'<?php echo(site_url("admin/dashboard/order_delete")); ?>', method: "post", data:{orderID: id}});
		ajax_call.done( function(data){
			// remove the row
			$('#row'+id).remove();
		});
	});
*/
}


function delivery_info(id){
	event.preventDefault();

	var ajax_call = $.ajax({url:'<?php echo(site_url("admin/dashboard/get_order_shipinfo")); ?>', method: "post", data:{orderID: id}, dataType: 'json'});
	ajax_call.done( function(data){
		
		var ship_number = data['ship_number'];
		var ship_date = data['ship_date'];

		if (ship_number != -1){
		
			swal({
				title: "Delivery status",
				text: "Currently, this order ("+id+") is number " + ship_number + "th in the shipping list.",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Make priority",
				closeOnConfirm: true
			},
			function(){
				var ajax_call = $.ajax({url:'<?php echo(site_url("admin/dashboard/make_priority")); ?>', method: "post", data:{orderID: id}});
				ajax_call.done( function(data){
					// remove the row
					$('#daysleft'+id).append(" <span class='glyphicon glyphicon-fire' title='priority'></span>");
				});
			});
		}
		else{
			swal({
				title: "Delivery status",
				text: "Order ("+id+") has been shipped on " + ship_date,
				type: "warning",
				confirmButtonClass: "btn-danger",
				closeOnConfirm: true
			});
		}
	});	

}

function complete_shipping(id){
	event.preventDefault();

	var d = new Date();

	swal({
		title: "Shipping completed?",
		html: "Are you sure you shipped this order. Shipping date is: " + d,
		type: "warning",
		showCancelButton: true,
		confirmButtonClass: "btn-danger",
		confirmButtonText: "Yes, it's shipped!",
		closeOnConfirm: true
	},
	function(){
		var ajax_call = $.ajax({url:'<?php echo(site_url("admin/dashboard/order_shipped")); ?>', method: "post", data:{orderID: id}});
		ajax_call.done( function(data){
			// remove the ship button
			$('#shipbtn_'+id).remove();
		});
	});
}

</script>

<div id="container">

	<div class="row" style=" margin-top: 20px;">       
        <div class="col-md-12">

	        <p>
	            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
	            <a href="<?php echo site_url('admin'); ?>">Admin</a> &gt;
	            <a href="<?php echo $base_url; ?>">Orders</a> &gt;
	            Page <?php echo $page; ?>
	        </p>  

		  <h1>Orders</h1>  
		  <?php if(isset($keyword)){ echo "Search results for the keyword: '".$keyword."'. ";} ?>
		  A total of <?php echo $num_results; ?> orders are found.<br>
		  <br>
		  <form action='<?php echo site_url('admin/dashboard/search_orders');?>' method="get" class="form-inline">
	      <div class="form-group has-feedback" style='width: 30%'>
	          <input type="text" class="form-control" name="search_keyword" placeholder="search for order" style='width: 100%'>
	          <span class="glyphicon form-control-feedback glyphicon-search"></span>    	          
	      </div>  
	      <select id="search_in" name="search_in" class="form-control">
          	<option value="" selected>all fields</option>	
          	<option value="orders.id">order id</option>	
          	<option value="name">name</option>	
          	<option value="email_address">email</option>	
          	<option value="product">product ordered</option>	
          	<option value="payment_method">payment method</option>	
          	<option value="payment">payment status</option>	
          	<option value="countryName">country</option>	
          	<option value="plug_type">plug type</option>	
          </select>      
	      <input type='submit' class="btn btn-primary" value='Search'>
	      </form>
	      <br>

		  <table class="table table-striped table-hover">
		  <thead>
		  <tr>
		  <th>ID</th>
		  <th style='text-align:center'>days old</th>
		  <th>Country</th>
		  <th>name</th>
		  <th>email address</th>
		  <th style='text-align:center'>payment</th>
		  <th style='text-align:right'>amount (€)</th>
		  <th>attachments</th>
		  <th style='text-align:center'>actions</th>
		  </tr>
		  </thead>
			<tbody>

			<?php
				$this->load->model('order_model'); 

				if($num_results > 0){
				    foreach($orders as $order){
				    	
				    	$encodedOrderID = $this->order_model->encode_orderID($order["orderid"]);

				    	echo "<tr id='row".$order["orderid"]."' ";
				    	if($order["payment"] == "completed")
				    		echo('class="success">');
				    	else if($order["payment"] == "pending")
							echo('class="warning">');
						else if($order["payment"] == "quote")
							echo('class="info">');
						else if($order["payment"] == "failed")
							echo('class="danger">');

				    	echo "<td>";
				    	echo "<a onclick='javascript:edit_remark(".$order["orderid"].",\"".$order["remarks"]."\"); return false' href='' title='see details'>";
				    	echo $order["orderid"];				    	
				    	echo "</a>";
				    	echo "</td>";		


				    	echo "<td style='text-align:center' id='daysleft".$order["orderid"]."'>";
				    	echo "<a onclick='javascript:delivery_info(".$order["orderid"]."); return false' href='' title='see details'>";
				    	echo $order["daysold"];				    	
				    	echo "</a>";		
				    	if($order["is_priority"])
				    		echo " <span class='glyphicon glyphicon-fire' title='priority'></span>";
				    	echo "</td>";
				    	echo "<td>".  $order["countryName"] ."</td>";	    	
				    	echo "<td>".  $order["firstname"] . " " . $order["lastname"]  ."</td>";
				    	echo "<td>".  $order["email_address"] ."</td>";
				    	echo "<td style='text-align:center'>".  $order["payment_method"] ."</td>";
				    	echo "<td style='text-align:right'>".  number_format(($order["grand_total"])/100,2,",",".") ."</td>";

				    	// attachment dropdown menu
				    	echo "<td><div class='dropdown'><button class='btn btn-default dropdown-toggle' id='dropdownMenu". $order["orderid"]."' data-toggle='dropdown' >select <span class='caret'></span></button><ul class='dropdown-menu' aria-labelledby='dropdownMenu". $order["orderid"]."'>";
				    	echo "<li style='padding-left:22px'><a href='".site_url('store/invoice/'. $encodedOrderID) . "' target='_new'>Invoice</a></li>";
				    	echo "<li style='padding-left:22px'><a href='".site_url('store/proformainvoice/'. $encodedOrderID) . "' target='_new'>Pro forma Invoice</a></li>";
				    	echo "<li style='padding-left:22px'><a href='".site_url('store/quote/'. $encodedOrderID) . "' target='_new'>Quote</a></li>";
			  			echo "</ul></div></td>";

			  			// actions
				    	echo "<td style='text-align:center'>";
				    	echo "<a id='viewbtn_".$order["orderid"]."' href='".site_url('store/thank_you/'. $encodedOrderID) . "' title='view details'><span class='glyphicon glyphicon-eye-open'></span></a> ";
				    	if($order["remarks"] != ""){
							echo "<a id='remarkbtn_".$order["orderid"]."' onclick='javascript:sweetAlert(\"Remark\", \"".$order["remarks"]."\"); return false' href='' title='view remark'><span class='glyphicon glyphicon-bell'></span></a> ";
						}
						if($order["payment"] != "completed"){
				    		echo "<a onclick='javascript:payment_check(".$order["orderid"].")' href='' title='mark as paid'><span class='glyphicon glyphicon-ok'></span></a> ";
						}

						if($order["delivery"] == "pending"){
							echo "<a id='shipbtn_".$order["orderid"]."' onclick='javascript:complete_shipping(".$order["orderid"].")' href='' title='Complete shipping..'><span class='glyphicon glyphicon-road'></span></a> ";
						}
   

						echo "<a onclick='javascript:delete_order(".$order["orderid"].",\"".$order["remarks"]."\")' href='' title='delete'><span class='glyphicon glyphicon-remove' style='color:#d9534f'></span></a> ";
						   
						echo "</td></tr>";     				
						
				    } 
				}else{
					echo "<tr><td colspan='9'><i>No results found for your query.</i></td></tr> ";
				}
			?>

			</tbody>
			</table>
			<?php
			if($num_pages > 1){
			?>

			<div>
			  <ul class="pagination" style='width:100%'>
			    <li>
			      <a href='<?php echo($base_url ."/1?".$_SERVER['QUERY_STRING']); ?>' aria-label="First">
			        <span aria-hidden="true">&laquo;</span>
			      </a>
			    </li>
			    <?php 
			    	for($i=1; $i<=$num_pages; $i++){
			    		echo "<li";
			    		if($i == $page)
			    			echo " class='active'";
			    			echo "><a href='" . $base_url ."/" . $i . "?".$_SERVER['QUERY_STRING']."'>". $i ."</a></li>";
			    	}
			    ?>	    
			    <li>
			      <a href='<?php echo($base_url ."/" . $num_pages . "?".$_SERVER['QUERY_STRING']); ?>' aria-label="Last">
			        <span aria-hidden="true">&raquo;</span>
			      </a>
			    </li>
			  </ul>
			</div>

			<?php
			} // end if ($numpages > 1)
			?>

			<br><br>
			
			<p>
	            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
	            <a href="<?php echo site_url('admin'); ?>">Admin</a> &gt;
	            <a href="<?php echo $base_url; ?>">Orders</a> &gt;
	            Page <?php echo $page; ?>
	        </p>  

		</div>
	</div>

</div>
 