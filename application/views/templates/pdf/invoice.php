<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style type="text/css">
	@font-face{
        font-family: 'headerFont2';
        src: url('<?php echo(base_url("assets/fonts/Belizio.ttf"));?>');        
    }

    @font-face{
      font-family: 'GothamNarrow';
      src: 	url('<?php echo(base_url("assets/fonts/GothamNarrow.ttf")); ?>');
    }

    @font-face{
      font-family: 'GothamNarrowMedium';
      src: url('<?php echo(base_url("assets/fonts/GothamNarrowbold.ttf"));?>');
    }        	

	body, html{
		margin: 0;
		padding: 0;
		border: 0;			
	}	

	table {
		border-radius: 10px;
		border-collapse: collapse;
		border-spacing: 0;
	}

	td{
		vertical-align: top;
	}

	p, li, td, strong{
      font-family:  'GothamNarrow', Arial;
      font-size: 14px;
      letter-spacing: 0em;
      line-height: 16px;      
    }
   
    h2, b, strong{
    	font-family: 'GothamNarrow', Arial;
    }

	#page-wrap {
		width: 700px;
		margin: 0 auto;
	}	
	
	.table-item td{
		border-bottom: 1px solid #eee;
		border-right: 1px solid #eee;
		border-radius: 10px;
	}
	.table-header{
		font-family: 'GothamNarrow', Arial;
		background: #eee;
		font-weight: bold;
	}

	.items-table{
		border: 1px solid #eee;		
	}	

	.items-table td{
		padding: 5px;	
	}	

</style>
</head>
<body>	
<br>
	<div id="page-wrap">
		<table width="100%">
			<tbody>
				<tr>
					<td width="50%">						
						<p style="font-family: 'headerFont2'; font-size: 3em;">Pozyx Labs<br><br></p>

						<p>
						<br>
						Spellewerkstraat 46<br>
						9030 Gent, Belgium <br>
						VAT: BE0634767208<br>
						info@pozyx.io / www.pozyx.io<br>
						</p>
						<br><br>
					</td>
					<td width="20%"></td>
					<td width="30%">
						<br><br>
						<h2><?php echo($document_type);?></h2>
						<?php 
							if($document_type == "invoice") { 
								echo "<b>Invoice date: </b>" . $invoice_data['invoice_date'] . "<br>";
								echo "<b>Invoice number: </b>" . $invoice_data['legal_id'] . "<br>";
		
							}else{
								echo "<b>Date: </b>" . $created_at . "<br>";
							}
						?>					
						<b><?php if($document_type == "quote") echo("Quote "); else echo "Order"; ?> number:</b> <?php echo($orderid); ?><br>	
						<?php if($document_type == "quote") echo("Quote valid for 30 days."); ?>					
					</td>
				</tr>				
			</tbody>
		</table>

		<table width="100%" >
			<tbody>				
				<tr>
					<td width="40%">

						<table width="100%" class="items-table">
							<tbody>				
								<tr class="table-header">
									<td>
									<?php 
									if($document_type != "quote") { 
										echo "BILL TO";
									}else{
										echo "QUOTE FOR";
									}
									?>
									</td>									
								</tr>
								<tr>
								<td>
									<?php 
							          echo $firstname . " " . $lastname . "<br>";           
							          if($company_name != "") echo $company_name . "<br>";
							          if($company_vat != "") echo $company_vat . "<br>";
							          echo $billing_address_line1 . "<br>";            
							          if($billing_address_line2 !="") echo $billing_address_line2 . "<br>";
							          echo $billing_zip ." ". $billing_city ." ". $billing_state . "<br>";           							          
							          echo $countryName . "<br>";  
							          if($phone !="") echo $phone ."<br>";        
							          if($email_address !="") echo $email_address . "<br>"; 
							        ?>
									
								</td></tr>
							</tbody>
						</table>
						
					</td>

					<?php if($document_type != "quote") { ?>

					<td width="5%">&nbsp;</td>

					<td width="40%">
						<table width="100%" class="items-table">
							<tbody>				
								<tr class="table-header">
								<td width="100%">SHIP TO</td>									
								</tr>
								<tr>
								<td>
									<?php 
							          echo $firstname . " " . $lastname . "<br>";
							          if($company_name != "") echo $company_name . "<br>";   
							          if($company_vat != "") echo $company_vat . "<br>";        
							          echo $address_line1 . "<br>"; 
							          if($address_line2 !="") echo $address_line2 . "<br>"; 
							          echo $zip ." ". $city ." ". $state . "<br>"; 
							          echo $countryName . "<br>";           
							          if($phone !="") echo $phone ."<br>";        
							          if($email_address !="") echo $email_address . "<br>"; 
							        ?>
								</td></tr>
							</tbody>
						</table>
					</td>

					<td width="15%"></td>

					<?php }else{ echo "<td width='60%''></td>";} ?>
					
				</tr>
			</tbody>
		</table>



		<p>&nbsp;</p>
		<table width="100%" class="items-table">
			<tbody>				
				<tr class="table-header">
					<td width="8%">ITEM #</td>
					<td width="35%">DESCRIPTION</td>
					<td width="10%" style="text-align:center">WEIGHT</td>
					<td width="10%" style="text-align:center">HS CODE</td>
					<td width="7%" style="text-align:center">QTY</td>
					<?php
					if($document_type != 'pro forma invoice'){
					?>
					<td width="15%" style="text-align:right;">UNIT PRICE €</td>
					<td width="15%" style="text-align:right;">TOTAL €</td>
					<?php } // end if != pro forma	?>
				</tr>				

				<?php

				foreach ($order_items as $items): 
				?>  

				<tr class="table-item">   
				  <td><?php echo $items['id']; ?></td>
				  <td><?php echo $items['productname']; ?></td>  
				  <td style="text-align:center">350 gr</td>
				  <td>
				  <?php 
				  // only show HS code for products in the database, we expect the other products to be services without HS code
				  if(isset($items['id'])){ echo "8542.31.9000"; } ?>
				  </td> 
				  <td style='text-align:center'><?php echo $items['quantity']; ?></td> 
				  <?php
					if($document_type != 'pro forma invoice'){
					?>
				  <td style="text-align:right"><?php echo  number_format(($items['unit_price']/100),2,",","."); ?></td>
				  <td style="text-align:right"><?php echo  number_format(($items['quantity']*$items['unit_price']/100),2,",","."); ?></td>
				  <?php } // end if != pro forma	?>
				</tr>

				<?php endforeach; ?>			
				
				<tr class="table-item">
					<td>&nbsp;</td>
					<td></td>
					<td></td>
					<td></td>
					<td style='text-align:center'> </td>
					<?php
					if($document_type != 'pro forma invoice'){
					?>
					<td style='text-align:right'></td>
					<td style='text-align:right'></td>	
					<?php } // end if != pro forma	?>				
				</tr>		
				<?php
				if($document_type != 'pro forma invoice'){
				?>	

				<tr class="table-summary">
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>					
					<td style='text-align:right'><b>SUBTOTAL</b></td>
					<td style='text-align:right'><b><?php echo number_format(($subtotal/100),2,",","."); ?></b></td>									
				</tr>

				<?php if($discount_rate != 0) { ?>
		        <tr class="table-summary">   
				  <td></td>   
				  <td></td> 
				  <td></td>   
				  <td></td> 
				  <td></td>         
				  <td style='text-align:right'>Discount (<?php echo number_format($discount_rate,0,",","."); ?>%)</td>                      
				  <td style="text-align:right"><?php echo  number_format($discount_amount/100,2,",","."); ?></td>              
				</tr>
				<?php } // end if discount ?>

				<tr class="table-summary">   
				  <td></td>
				  <td></td>
				  <td></td>
				  <td></td>
				  <td></td>
				  <td style='text-align:right'>Shipping cost</td>    
				  <td style="text-align:right"><?php echo  number_format(($shipping_cost/100),2,",","."); ?></td>          
				</tr>	
				
				<tr class="table-summary">
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td style='text-align:right'>VAT (<?php echo $tax_rate; ?>%)</td>
					<td style='text-align:right'><?php echo number_format($tax_amount/100,2,",","."); ?></td>	
							
				</tr>
				<?php } // end if != pro forma	?>		
				<tr class="table-summary">
					<td></td>
					<td></td>
					<td></td>
					<?php
					if($document_type != 'pro forma invoice'){
					?>
					<td></td>
					<td></td>
					<?php } // end if != pro forma	?>
					<td style='text-align:right'><b>TOTAL</b></td>
					<td style='text-align:right'><b><?php echo number_format(($grand_total/100),2,",","."); ?></b></td>					
				</tr>
			</tbody>
		</table>		
		<p>&nbsp;</p>		
		<table width="100%">
			<tbody>
				<tr>
					<td width="50%" style='font-size:10px;line-height:80%'>
						<?php
						if($document_type == 'invoice'){
							if($invoice_data['vat_text'] != ""){
								echo "<b>VAT exemption:</b><br>" . $invoice_data['vat_text'] . "<br><br>";
							}

							echo "<b>Delivery terms:</b><br>Delivered at place<br>" . "Expected latest delivery date: " . $invoice_data['delivery_date'] . "<br><br>";

						}
						?>

						
					</td>
					<td width="50%" style='font-size:10px;line-height:80%'>
						<?php
						if($document_type == 'invoice'){
							echo "<b>Payment terms:</b><br>" . $invoice_data['payment_text'] . "<br>";

							if($invoice_data['payment_term']!= "PAID"){
								echo "Due date: " . $invoice_data['due_date'] . "<br><br>";
								echo "<b>Bank information:</b><br>";
								echo "KBC Bank<br>";
								echo "Havenlaan 2 - 1080 Brussels - BE<br>";	
								echo "IBAN: BE40 7360 1689 7363<br>";
								echo "BIC: KRED BE BB<br><br>";
							}
							else{
								echo "<br>";
							}
						}
						?>
						
					</td>
				</tr>
				<tr>
					<td width="50%" style='font-size:10px;line-height:80%'>
						<b>Terms and conditions: </b><br><?php echo site_url('welcome/pozyxLabs'); ?>
					</td>
				</tr>
			</tbody>
		</table>
		<p>&nbsp;</p>
		<table width="100%">
			<tbody>
				<tr>
					<td style="text-align:center">
						<i>Thank you for your business!</i>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>