 <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: rgba(255,255,255, 0.95 ); height: 65px; border:none; box-shadow: 0px 10px 100px 2px rgba(100, 100, 100, 0.1);">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#" style="font-size: 18px; color: black; font-family: 'GothamNarrowMedium'; margin-top: 5px">Pozyx Laboratories</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class='menu-item'><a href="<?php echo site_url('/'); ?>">Home</a></li>
            <!--<li class='menu-item'><a onclick="kickstartInfo();" style="cursor:pointer">Kickstarter</a></li>-->            
            <li class='menu-item'><a href="<?php echo site_url('Documentation'); ?>">Documentation</a></li>   
            <li class='menu-item'><a href="<?php echo site_url('store'); ?>">Store</a></li>                  
            <li class='menu-item'><a href="<?php echo site_url('welcome/about'); ?>">About</a></li>
            <li class='menu-item'><a href="<?php echo site_url('admin'); ?>">Admin</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron main_header">
      <div class="container">

        <div class="row">
        
          <div class="col-sm-6 col-md-6">
          </div>
          <div class="col-sm-6 col-md-6" id="title-compartiment">           

            <!--<div id="stamp" class="hidden-xs">
              accurate<br>
              positioning<br>
              <b>made easy</b>
            </div>-->            
            <h1 class="header-title">Pozyx<img src="<?php echo(base_url('assets/images/pozyx_dude.png')); ?>" id="pozyx_dude" class="hidden-sm hidden-md"></h1>
            <h1 class="header-subtitle">Accurate positioning</h1>         

          </div>
        </div>      
        
      </div>       

    </div>