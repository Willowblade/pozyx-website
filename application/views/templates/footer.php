</div></div> <!-- <div id="main"> and <div id="wrap"> from the header -->

<footer class="footer" id="footer">
      <div class="container">
        
          <div class="row" style='padding-top: 60px;'>                

          <a name="contact"></a>

            <div class="col-md-3 col-sm-6 col-xs-6">
              <h4>POZYX</h4>
              <a href="">Home</a><br>
              <a href="https://www.kickstarter.com/projects/pozyx/pozyx-accurate-indoor-positioning-for-arduino">Kickstarter</a>
            </div>
            <div class="col-sm-6 col-xs-6 hidden-md hidden-lg">
              <h4>CONTACT</h4>
              <span style="color: #B3B3B3">Info: info<!-- go away spammers! -->&#64;<!-- leave now! -->pozyx.io</span><br>
              <span style="color: #B3B3B3">Sales: sales<!-- go away spammers! -->&#64;<!-- leave now! -->pozyx.io</span><br>
              <span style="color: #B3B3B3">Support: support<!-- go away spammers! -->&#64;<!-- leave now! -->pozyx.io</span><br>
              <a href="https://www.facebook.com/pozyx">Facebook</a><br><br>    
            </div>
              <!-- Begin MailChimp Signup Form -->           
            
            <div class="col-md-3 col-sm-6 col-xs-6">                      
                <h4>STAY INFORMED</h2>            
              
                <label for="mce-EMAIL" style="color:#B3B3B3; font-weight:normal">Email Address  <span class="asterisk">*</span>
              </label><br>
                <input type="email" value="" name="input_email" id='input_email' class="required email" id="mce-EMAIL">              
                <input type="submit" value="Subscribe" name="subscribe" onclick='form_subscribe()' class="button">              
            </div>

            <div class="col-md-3 col-sm-6 col-xs-6 hidden-sm hidden-xs">
              <h4>CONTACT</h4>
              <span style="color: #B3B3B3">Email: info<!-- go away spammers! -->&#64;<!-- leave now! -->pozyx<!-- you shall not pass! -->&#46;io</span><br>
              <span style="color: #B3B3B3">Sales: sales<!-- go away spammers! -->&#64;<!-- leave now! -->pozyx.io</span><br>
              <span style="color: #B3B3B3">Support: support<!-- go away spammers! -->&#64;<!-- leave now! -->pozyx.io</span><br>
              <a href="https://www.facebook.com/pozyx">Go to our facebook page</a><br>   

              <!--<a href="https://www.facebook.com/dialog/share?display=popup&href=http%3A%2F%2Fwww.pozyx.io&redirect_uri=http%3A%2F%2Fwww.pozyx.io">share</a>   -->
            </div>    

            <div class="col-md-3 col-sm-6 col-xs-6"> 
            	<div>
            	<h4>SHARE</h4>
    				  <div id="shareme" data-url="https://www.pozyx.io" data-text="Pozyx - indoor positioning voor Arduino." style='float:left'></div>
    				  </div>
            </div>
         
            

          </div>

      </div>
    </footer>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
    <!--<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>-->    
    <!--<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>-->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo(base_url('assets/js/ie10-viewport-bug-workaround.js')); ?>"></script>

    <script src="<?php echo(base_url('assets/js/sweetalert2.min.js')); ?>"></script>
    <script src="<?php echo(base_url('assets/js/jquery.sharrre.min.js')); ?>"></script>
    

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-62452989-1', 'auto');
	  ga('send', 'pageview');

    C = {
      // Number of days before the cookie expires, and the banner reappears
      cookieDuration : 100,
      // Name of our cookie
      cookieName: 'complianceCookie',
      // Value of cookie
      cookieValue: 'on',
      // Message banner title
      bannerTitle: "Cookies:",
      // Message banner message
      bannerMessage: "This site uses cookies to deliver its services.",
      // Message banner dismiss button
      bannerButton: "OK",
      // Link to your cookie policy.
      bannerLinkURL: "<?php echo site_url('welcome/pozyxLabs'); ?>#cookiePolicy",
      // Link text
      bannerLinkText: "Read more",

      createDiv: function () {
          var banner = $(
              '<div class="alert alert-success alert-dismissible fade in" ' +
              'role="alert" style="position: fixed; bottom: 0; width: 100%; ' +
              'margin-bottom: 0"><strong>' + this.bannerTitle + '</strong> ' +
              this.bannerMessage + ' <a class="alert-link" href="' + this.bannerLinkURL + '">' +
              this.bannerLinkText + '</a> <button type="button" class="btn ' +
              'btn-success" onclick="C.createCookie(C.cookieName, C.cookieValue' +
              ', C.cookieDuration)" data-dismiss="alert" aria-label="Close">' +
              this.bannerButton + '</button></div>'
          )
          $("body").append(banner)
      },

      createCookie: function(name, value, days) {
          console.log("Create cookie")
          var expires = ""
          if (days) {
              var date = new Date()
              date.setTime(date.getTime() + (days*24*60*60*1000))
              expires = "; expires=" + date.toGMTString()
          }
          document.cookie = name + "=" + value + expires + "; path=/";
      },

      checkCookie: function(name) {
          var nameEQ = name + "="
          var ca = document.cookie.split(';')
          for(var i = 0; i < ca.length; i++) {
              var c = ca[i]
              while (c.charAt(0)==' ')
                  c = c.substring(1, c.length)
              if (c.indexOf(nameEQ) == 0) 
                  return c.substring(nameEQ.length, c.length)
          }
          return null
      },

      init: function() {
          if (this.checkCookie(this.cookieName) != this.cookieValue)
              this.createDiv()
      }
  }

  if(isValidEmailAddress === undefined){
   function isValidEmailAddress(emailAddress) {
          var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
          return pattern.test(emailAddress);
      };     
  }

  function subscribe(email_address){
        $.ajax({url:"https://pozyx.us10.list-manage.com/subscribe/post-json?u=7d0d0ce6e538b59cfe819636b&id=7d285f719b&c=?", 
          type: 'GET', 
          data: {EMAIL : email_address},      
          cache       : false,
          dataType    : 'json',
          contentType: "application/json; charset=utf-8",
          error       : function(err) { alert("Could not connect to the registration server. Please try again later."); },
          success     : function(data) {
              if (data.result != "success") {
                  swal({   
                    title: "Oops...",  
                    html: data.msg, 
                    type: "error"           
                  });

                    //sweetAlert("Oops...", data.msg, "error");                    
              } else {
                  swal({   title: "Sweet!",   text: "Almost finished... We need to confirm your email address.To complete the subscription process, please click the link in the email we just sent you. ",   imageUrl: "images/thumbs-up.jpg" });
              }
          }
        });
    }

    function form_subscribe(){
        var email = $('#input_email').val();          

        if(email == "" || !isValidEmailAddress(email)){
          sweetAlert("Oops...", "Please enter a valid email address.", "error");  
        }else{
          subscribe(email);
        }
    }

  $(document).ready(function() {
      C.init();

      $('#shareme').sharrre({
        share: {
          googlePlus: true,
          facebook: true,
          twitter: true,
          digg: false,
          delicious: false,
          stumbleupon: false,
          linkedin: true,
          pinterest: false
        },
        buttons: {
          googlePlus: {size: 'tall', annotation:'bubble'},
          facebook: {layout: 'box_count'},
          twitter: {count: 'vertical'},
          digg: {type: 'DiggMedium'},
          delicious: {size: 'tall'},
          stumbleupon: {layout: '5'},
          linkedin: {counter: 'top'},
          pinterest: {layout: 'vertical'}
        },
        enableHover: false,
        enableCounter: false,
        enableTracking: true
      });
  });

	</script>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P6XPFM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P6XPFM');</script>
<!-- End Google Tag Manager -->
  </body>
</html>
