<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta property="og:url"                content="https://www.pozyx.io" />
    <meta property="fb:app_id"             content="9869919170" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="Pozyx - centimeter positioning for Arduino" />
    <meta property="og:description"        content="Pozyx makes indoor positioning with centimeter accuracy a reality. It's compatibility with Arduino makes it a must-have for every DIY fan." />    
    <meta property="og:image"              content="http://www.pozyx.io/assets/images/pozyx_with_arduino.jpg" />

    <title>Pozyx - centimeter positioning for arduino</title>
    <meta name="description" content="Pozyx makes indoor positioning with centimeter accuracy a reality. It's compatibility with Arduino makes it a must-have for every DIY fan.">
    <meta name="keyword" content="pozyx, positioning, arduino, pozyx positioning, shield, arduino shield, UWB, ultra-wideband">

    <link rel="shortcut icon" href="<?php echo(base_url('assets/images/favicon.ico')); ?>">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo(base_url('assets/css/bootstrap.min.css'));?>" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="css/sweetalert.min.css">-->
    <link rel="stylesheet" type="text/css" href="<?php echo(base_url('assets/css/sweetalert2.css')); ?>">
    <!--<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" href="<?php echo(base_url('assets/css/templates/pozyx.css')); ?>">

    <script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
    <script src="<?php echo(base_url('assets/js/bootstrap.min.js')); ?>"></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>    

    .menu-item{
      margin-top: 5px;
      margin-left:5px;
      /*border-left: 3px solid white;*/
      margin-right: 5px;
      padding-right: 0px;
      padding-left: 0px;
      font-size: 18px;
      color: black;
      font-family: 'GothamNarrowMedium';
    }

    .menu-item a{
      font-size: 18px;
      color: black;
      font-family: 'GothamNarrowMedium';
    }

    html {
      overflow: -moz-scrollbars-vertical; 
      overflow-y: scroll;
      height: 100%;
    }

    body{
      height:100%;
    }

    #wrap{
      min-height:100%;
    }

    #main {
      overflow:auto;
      padding-bottom:390px; /* this needs to be bigger than footer height*/
    }

    /* Large devices (large desktops, 1200px and up) */
    @media (min-width: 1400px) { 
        .main_header{
          background-image:url('<?php echo(base_url('assets/images/HEADER_narrow_2000px.jpg')); ?>'); 
          height: 381px; 
          background-position: 50% 50%; 
          background-repeat: no-repeat;
        }

        .header-title{
          font-size: 99px;
          line-height: 99px; 
          color: white; 
          padding-top: 0px; 
          padding-left: 5px; 
          position:relative; 
          z-index: 99;
        }

        .header-subtitle{
        	font-size: 50px;
        	color: white;
        	/*font-style: italic;*/
        	position:relative; 
        	padding-left: 5px; 
        	margin-top: -10px;
        }

        #arduino{
        	top: 511px;
        	display: none;
        }

        #stamp{
         left: 415px; 
         top: 300px; 
         display: none;
        }
    }
     @media (max-width: 1400px) { 
        .main_header{
          background-image:url('<?php echo(base_url('assets/images/HEADER_narrow_2000px.jpg')); ?>'); 
          height: 381px; 
          background-position: 50% 50%; 
          background-repeat: no-repeat;
        }

        .header-title{
          font-size: 99px;
          line-height: 99px;
          color: white; 
          padding-top: 0px; /*54px; */
          padding-left: 10px; 
          position:relative; 
          z-index: 99;
        }


        .header-title img{
        	height:200px;        	
        }


		    .header-subtitle{
        	font-size: 50px;
        	color: white;
        	/*font-style: italic;*/
        	position:relative; 
        	padding-left: 10px; 
        	margin-top: -10px;
        }

        #stamp{
          display: none;
          left: 330px; 
          top: 260px; 
        }

        #arduino{
        	display: none;
        	top: 471px;
        }
    }

    @media (min-width: 767px) { 
        .headerText{
          margin-top: 25px;
          margin-bottom: 30px;
        }
        .offers{
          height: 390px;
        }

        #footer{
          margin-top: -240px; /* negative value of footer height */
          height: 240px;
        }
    }

    /*@media (max-width: 991px) { */
    @media (max-width: 1199px) {	
    	.header-title{
        	margin-top: 65px;
        }
    }

    /* tipping point for bootstrap medium device (md) */
    @media (max-width: 992px) {  
        #footer{
          margin-top: -360px; /* negative value of footer height */
          height: 360px;
        }
    }

    @media (max-width: 767px) { 
      .main_header{
          background-image:url('<?php echo(base_url('assets/images/HEADER_XSMALL_narrow.jpg'));?>'); 
          height: 280px; 
          background-position: 50% 50%; 
          background-repeat: no-repeat;
        }


        .headerText{
          padding-top: 25px;
          margin-top: 25px;          
        }

        #title-compartiment{
          padding-top: 60px;
          text-align: center;
        }

        .header-title{
            font-size: 80px;
            padding: auto;
            margin: auto;
        } 

        .header-subtitle{
          font-size: 40px;
          color: white;
          /*font-style: italic;*/
          position:relative; 
          padding: auto;
          margin: auto;
        }

        #pozyx_dude{
          display: none;
        }       

        .navbar-collapse{
	    	background-color: #333;
	    }      

	    .navbar-collapse a{
	    	color: white;
	    }
    }

    @media (max-width: 501px) { 
        .header-subtitle{          
          font-size: 30px;
          color: white;
          /*font-style: italic;*/
          position:relative; 
          padding: auto;
          margin: auto;
          margin-top: -5px;
        }

        .header-title{            
            margin-top: 0px;
        } 

    }

    #pozyx_dude{
       vertical-align: bottom;
       margin-top: -40px;
    }

    #arduino{
    	position: absolute;    	
    	font-family: headerFont;
    	color: black;
    	font-size: 16px;
    	right:3%;
    	width: 250px;

    }

    p, li, td, th{
      font-family: GothamNarrow, Arial;
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
    }

    b{
      font-family: 'GothamNarrowMedium';
    }

    h3{
      color: #5F945F;
      font-family:'GothamNarrowMedium';
    }

    h2{
      
      font-family: headerFont;
      color: #272727;
      margin-bottom: 30px;
      font-size: 36px;
    }

    h1{
      font-family: headerFont;
    }

    .borderleft{
      border-left: 2px solid #F7F3EF;
    }

    .highlight:hover{
      /*background-color: #F7F3EF;*/
      background-image:url('images/achtergrondkleur2.jpg')
    }

    .headerNumber{
      font-size: 54px;
      font-family: headerFont;
      color: #898989;
      float: left;
      width: 45px;
    }

    ::selection {
      background: #82DA82; /* WebKit/Blink Browsers */
    }

    ::-moz-selection {
      background: #82DA82; /* Gecko Browsers */
    }

    #stamp{
      width: 210px; 
      height: 210px;
      background-image:url('images/HEADER_cirkels.png'); 
      background-size: 210px 210px; 
      position: absolute;            
      z-index: 0; 
      text-align: center; 
      padding-top: 60px; 
      font-size: 27px; 
      line-height: 28px; 
      font-family: GothamNarrow;
      color: #272727;
    }

    #stamp b{
      font-family: 'GothamNarrowMedium';
    }

    li{
      margin-left: -22px;
      padding-left: 0px;
    }

    h4{
      font-family: 'GothamNarrowMedium';
      letter-spacing: 1px;
      color: white;
      margin-bottom: 20px;
      font-size: 22px;
    }

    #footer{
      background-color: #2E2A2B; 

      /* footer at bottom */
      position: relative;
      clear:both;
    }

    #footer a{
      color: #B3B3B3;
    }

    #button{
        background-image: url('images/button.png');
        position: relative;
        top: -150px;  
        float: right;      
        background-size: 300px 300px;
        width: 300px;
        height: 300px;
        cursor: pointer;
    }

    #button-small{
        background-image: url('images/button.png');             
        background-size: 300px 300px;        
        width: 300px;
        height: 300px;
        cursor: pointer;
        margin: auto;
        margin-bottom: -170px;
        margin-top: 50px;
    }

    #button-small:hover, #button:hover{
        background-image: url('images/button_hover.png');
    }

    .container{      
      /*margin-bottom: 50px;*/
    }

    .profilepic{
      width: 70%; 
      margin-bottom: 15px;
    }

    .profilepic:hover{
      opacity: 0.9;
    }

    .FAQ-answer{
      display: hidden;
      color: #5F945F;
      text-decoration: none;
      margin-left: 15px;
    }


    #FAQ li{
      cursor: pointer;
    }

    .FAQ-question:hover{
      text-decoration: underline;
    }

    .jumbotron{
    	font-family: GothamNarrow, Arial;
    	color: #454545;
      background-color: white;
    }

    .sharrre .button{
	    float:left;
	    width:60px;
	  }

	#playVideo{
		float: right; 
		margin-top: 60px; 
		opacity: 0.75;
		filter: alpha(opacity=75); 
		width: 250px; 
		font-size: 36px;
        color: white; 
        position:relative; 
        padding-left: 5px; 
        display: block; 
        font-family: headerFont; 
        cursor: pointer; 
        line-height: 46px;
	}

	#playVideo:hover{
		opacity: 1;
		filter: alpha(opacity=100); 
	}

	.dialogInput{
		width: 100%;
		box-sizing: border-box;
		border-radius: 3px;
		border: 1px solid #d7d7d7;
		height: 43px;
		margin-top: 10px;
		margin-bottom: 17px;
		font-size: 18px;
		padding: 0px 12px;		
	}
  </style>

  </head>

  <body>

<div id="wrap">
<div id="main" class="clear-top">  