<?php defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('templates/header');
if($this->ion_auth->logged_in()) {
	$this->load->view('templates/admin_nav'); 
}else{
	$this->load->view('templates/nav'); 
}
?>
 
<div class="container">
  <?php echo $the_view_content; ?>
</div>

<!--
<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo (ENVIRONMENT === 'development') ? 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
-->

<?php $this->load->view('templates/backend_footer');?>