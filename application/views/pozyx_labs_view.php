<div class="container">
      <!-- Example row of columns -->
      <div class="row" style=" margin-top: 20px;">        

        <div class="col-md-12">
          <h2>Pozyx Labs</h2>  
          <p>
            Pozyx Labs is a company specialized in indoor positioning hardware and software. <br>
            <br>
            Pozyx Labs BVBA<br>
            Spellewerkstraat 46<br>
            9030 Gent, Belgium<br>
            VAT: BE0634767208<br>
            <br>
            contact: <a href='mailto:info@pozyx.io'>info@pozyx.io</a>
          </p>

          <p><a name="terms"></a></p>
          <br>
          <br>
          <h2>Terms and conditions</h2>
          <h3>Overview</h3>
          <p>
            This website is operated by Pozyx Labs BVBA. Throughout the site, the terms “we”, “us” and “our” refer to Pozyx Labs BVBA. Pozyx Labs BVBA offers this website, including all information, tools and services available from this site or any other contact form to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here.
          </p>
          <p>
            By visiting our site and/ or purchasing something from us, you engage in our “Service” and agree to be bound by the following terms and conditions (“Terms of Service”, “Terms”), including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These Terms of Conditions apply to all users of the site, including without limitation users who are browsers, vendors, customers, merchants, and/ or contributors of content.
          </p>
          <p>
            Please read these Terms of Conditions carefully before accessing, using our website, purchasing our products or relying on our services. By accessing or using any part of the site, you agree to be bound by these Terms of Conditions. If you do not agree to all the terms and conditions of this agreement, then you may not access the website or use any services. If these Terms of Conditions are considered an offer, acceptance is expressly limited to these Terms of Conditions.
          </p>
          <p>
            Any new features or tools which are added to the current store shall also be subject to the Terms of Conditions. You can review the most current version of the Terms of Conditions at any time on this <a href="<?php echo site_url('welcome/pozyxLabs'); ?>#terms">page</a>. We reserve the right to update, change or replace any part of these Terms of Conditions by posting updates and/or changes to our website. It is your responsibility to check this page periodically for changes. Your continued use of or access to the website following the posting of any changes constitutes acceptance of those changes.
          </p>


          <br>
          <h3>Online store terms</h3>
          <p>
            By agreeing to these Terms and Conditions, you represent that you are at least the age of majority in your state or province of residence, or that you are the age of majority in your state or province of residence and you have given us your consent to allow any of your minor dependents to use this site.
          </p>
          <p>
            You may not use our products for any illegal or unauthorized purpose nor may you, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright laws).
          </p>
          <p>
            You must not transmit any worms or viruses or any code of a destructive nature.
          </p>
          <p>
            A breach or violation of any of the Terms will result in an immediate termination of your Services.
          </p>


          <br>
          <h3>General conditions</h3>
          <p>
            We reserve the right to refuse service to anyone for any reason at any time.
          </p>
          <p>
            You understand that your content (not including credit card information), may be transferred unencrypted and involve (a) transmissions over various networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices. Credit card information is always encrypted during transfer over networks.
          </p>
          <p>
            You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service or any contact on the website through which the service is provided, without express written permission by us.
          </p>
          <p>
            The headings used in this agreement are included for convenience only and will not limit or otherwise affect these Terms.
          </p>


          <br>
          <h3>Accuracy, completeness and timeliness of information</h3>
          <p>
            We are not responsible if information made available on this site is not accurate, complete or current. The material on this site is provided for general information only and should not be relied upon or used as the sole basis for making decisions without consulting primary, more accurate, more complete or more timely sources of information. Any reliance on the material on this site is at your own risk.
          </p>
          <p>
            This site may contain certain historical information. Historical information, necessarily, is not current and is provided for your reference only. We reserve the right to modify the contents of this site at any time, but we have no obligation to update any information on our site. You agree that it is your responsibility to monitor changes to our site.
          </p>
          

          <br>
          <h3>Modifications to the service and prices</h3>
          <p>
            Prices for our products are subject to change without notice.
          </p>
          <p>
            We reserve the right at any time to modify or discontinue the Service (or any part or content thereof) without notice at any time.
          </p>
          <p>
            We shall not be liable to you or to any third-party for any modification, price change, suspension or discontinuance of the Service.
          </p>


          <br>
          <h3>Products or services</h3>
          <p>
            Certain products or services may be available exclusively online through the website. These products or services may have limited quantities and are subject to return or exchange only according to our <a href="<?php echo site_url('welcome/pozyxLabs'); ?>#returnPolicy">Return Policy</a>.
          </p>
          <p>
            We have made every effort to display as accurately as possible the colors and images of our products that appear at the store. We cannot guarantee that your computer monitor's display of any color will be accurate.
          </p>
          <p>
            We reserve the right, but are not obligated, to limit the sales of our products or Services to any person, geographic region or jurisdiction. We may exercise this right on a case-by-case basis. We reserve the right to limit the quantities of any products or services that we offer. All descriptions of products or product pricing are subject to change at anytime without notice, at the sole discretion of us. We reserve the right to discontinue any product at any time. Any offer for any product or service made on this site is void where prohibited.
          </p>
          <p>
            We do not warrant that the quality of any products, services, information, or other material purchased or obtained by you will meet your expectations, or that any errors in the Service will be corrected.
          </p>


          <br>
          <h3>Accuracy of billing and account information</h3>
          <p>
            We reserve the right to refuse any order you place with us. We may, in our sole discretion, limit or cancel quantities purchased per person, per household or per order. These restrictions may include orders placed by or under the same customer account, the same credit card, and/or orders that use the same billing and/or shipping address. In the event that we make a change to or cancel an order, we may attempt to notify you by contacting the e-mail and/or billing address/phone number provided at the time the order was made. We reserve the right to limit or prohibit orders that, in our sole judgment, appear to be placed by dealers, resellers or distributors.
          </p>
          <p>
            You agree to provide current, complete and accurate purchase and account information for all purchases made at our store. You agree to promptly update your account and other information, including your email address and credit card numbers and expiration dates, so that we can complete your transactions and contact you as needed.
          </p>
          <p>
            For more detail, please review our <a href="<?php echo site_url('welcome/pozyxLabs'); ?>#returnPolicy">Return Policy</a>.
          </p>


          <br>
          <h3>Optional tools</h3>
          <p>
            We may provide you with access to third-party tools over which we neither monitor nor have any control nor input.
          </p>
          <p>
            You acknowledge and agree that we provide access to such tools ”as is” and “as available” without any warranties, representations or conditions of any kind and without any endorsement. We shall have no liability whatsoever arising from or relating to your use of optional third-party tools.
          </p>
          <p>
            Any use by you of optional tools offered through the site is entirely at your own risk and discretion and you should ensure that you are familiar with and approve of the terms on which tools are provided by the relevant third-party provider(s).
          </p>
          <p>
            We may also, in the future, offer new services and/or features through the website (including, the release of new tools and resources). Such new features and/or services shall also be subject to these Terms and Conditions.
          </p>


          <br>
          <h3>Third-Party links</h3>
          <p>
            Certain content, products and services available via our Service may include materials from third-parties.
          </p>
          <p>
            Third-party links on this site may direct you to third-party websites that are not affiliated with us. We are not responsible for examining or evaluating the content or accuracy and we do not warrant and will not have any liability or responsibility for any third-party materials or websites, or for any other materials, products, or services of third-parties.
          </p>
          <p>
            We are not liable for any harm or damages related to the purchase or use of goods, services, resources, content, or any other transactions made in connection with any third-party websites. Please review carefully the third-party's policies and practices and make sure you understand them before you engage in any transaction. Complaints, claims, concerns, or questions regarding third-party products should be directed to the third-party.
          </p>


          <br>
          <h3>User comments, feedback and other submissions</h3>
          <p>
            If, at our request, you send certain specific submissions (for example contest entries) or without a request from us you send creative ideas, suggestions, proposals, plans, or other materials, whether online, by email, by postal mail, or otherwise (collectively, 'comments'), you agree that we may, at any time, without restriction, edit, copy, publish, distribute, translate and otherwise use in any medium any comments that you forward to us. We are and shall be under no obligation (1) to maintain any comments in confidence; (2) to pay compensation for any comments; or (3) to respond to any comments.
          </p>
          <p>
            We may, but have no obligation to, monitor, edit or remove content that we determine in our sole discretion are unlawful, offensive, threatening, libelous, defamatory, pornographic, obscene or otherwise objectionable or violates any party’s intellectual property or these Terms and Conditions.
          </p>
          <p>
            You agree that your comments will not violate any right of any third-party, including copyright, trademark, privacy, personality or other personal or proprietary right. You further agree that your comments will not contain libelous or otherwise unlawful, abusive or obscene material, or contain any computer virus or other malware that could in any way affect the operation of the Service or any related website. You may not use a false e-mail address, pretend to be someone other than yourself, or otherwise mislead us or third-parties as to the origin of any comments. You are solely responsible for any comments you make and their accuracy. We take no responsibility and assume no liability for any comments posted by you or any third-party.
          </p>


          <br>
          <h3>Personal information</h3>
          <p>
            Your submission of personal information through the store is governed by our <a href="<?php echo site_url('welcome/pozyxLabs'); ?>#privacyPolicy">Privacy Policy</a>.
          </p>


          <br>
          <h3>Errors, inaccuracies and omissions</h3>
          <p>
            Occasionally there may be information on our site or in the Service that contains typographical errors, inaccuracies or omissions that may relate to product descriptions, pricing, promotions, offers, product shipping charges, transit times and availability. We reserve the right to correct any errors, inaccuracies or omissions, and to change or update information or cancel orders if any information in the Service or on any related website is inaccurate at any time without prior notice (including after you have submitted your order).
          </p>
          <p>
            We undertake no obligation to update, amend or clarify information in the Service or on any related website, including without limitation, pricing information, except as required by law. No specified update or refresh date applied in the Service or on any related website, should be taken to indicate that all information in the Service or on any related website has been modified or updated.
          </p>


          <br>
          <h3>Prohibited uses</h3>
          <p>
            In addition to other prohibitions as set forth in the Terms and Conditions, you are prohibited from using the site or its content: (a) for any unlawful purpose; (b) to solicit others to perform or participate in any unlawful acts; (c) to violate any international, federal, provincial or state regulations, rules, laws, or local ordinances; (d) to infringe upon or violate our intellectual property rights or the intellectual property rights of others; (e) to harass, abuse, insult, harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual orientation, religion, ethnicity, race, age, national origin, or disability; (f) to submit false or misleading information; (g) to upload or transmit viruses or any other type of malicious code that will or may be used in any way that will affect the functionality or operation of the Service or of any related website, other websites, or the Internet; (h) to collect or track the personal information of others; (i) to spam, phish, pharm, pretext, spider, crawl, or scrape; (j) for any obscene or immoral purpose; or (k) to interfere with or circumvent the security features of the Service or any related website, other websites, or the Internet. We reserve the right to terminate your use of the Service or any related website for violating any of the prohibited uses.
          </p>


          <br>
          <h3>Disclaimer of warranties, limitation of liability</h3>
          <p>
            We do not guarantee, represent or warrant that your use of our service will be uninterrupted, timely, secure or error-free.
          </p>
          <p>
            We do not warrant that the results that may be obtained from the use of the service will be accurate or reliable.
          </p>
          <p>
            You agree that from time to time we may remove the service for indefinite periods of time or cancel the service at any time, without notice to you.
          </p>
          <p>
            You expressly agree that your use of, or inability to use, the service is at your sole risk. The service and all products and services delivered to you through the service are (except as expressly stated by us) provided 'as is' and 'as available' for your use, without any representation, warranties or conditions of any kind, either express or implied, including all implied warranties or conditions of merchantability, merchantable quality, fitness for a particular purpose, durability, title, and non-infringement.
          </p>
          <p>
            In no case shall Pozyx Labs BVBA, our directors, officers, employees, affiliates, agents, contractors, interns, suppliers, service providers or licensors be liable for any injury, loss, claim, or any direct, indirect, incidental, punitive, special, or consequential damages of any kind, including, without limitation lost profits, lost revenue, lost savings, loss of data, replacement costs, or any similar damages, whether based in contract, tort (including negligence), strict liability or otherwise, arising from your use of any of the service or any products procured using the service, or for any other claim related in any way to your use of the service or any product, including, but not limited to, any errors or omissions in any content, or any loss or damage of any kind incurred as a result of the use of the service or any content (or product) posted, transmitted, or otherwise made available via the service, even if advised of their possibility. Because some states or jurisdictions do not allow the exclusion or the limitation of liability for consequential or incidental damages, in such states or jurisdictions, our liability shall be limited to the maximum extent permitted by law.
          </p>


          <br>
          <h3>Indemnification</h3>
          <p>
            You agree to indemnify, defend and hold harmless Pozyx Labs BVBA and our parent, subsidiaries, affiliates, partners, officers, directors, agents, contractors, licensors, service providers, subcontractors, suppliers, interns and employees, harmless from any claim or demand, including reasonable attorneys’ fees, made by any third-party due to or arising out of your breach of these Terms and Conditions or the documents they incorporate by reference, or your violation of any law or the rights of a third-party.
          </p>


          <br>
          <h3>Severability</h3>
          <p>
            In the event that any provision of these Terms and Conditions is determined to be unlawful, void or unenforceable, such provision shall nonetheless be enforceable to the fullest extent permitted by applicable law, and the unenforceable portion shall be deemed to be severed from these Terms and Conditions, such determination shall not affect the validity and enforceability of any other remaining provisions.
          </p>


          <br>
          <h3>Termination</h3>
          <p>
            The obligations and liabilities of the parties incurred prior to the termination date shall survive the termination of this agreement for all purposes.
          </p>
          <p>
            These Terms and Conditions are effective unless and until terminated by either you or us. You may terminate these Terms and Conditions at any time by notifying us that you no longer wish to use our Services, or when you cease using our site.
          </p>
          <p>
            If in our sole judgment you fail, or we suspect that you have failed, to comply with any term or provision of these Terms and Conditions, we also may terminate this agreement at any time without notice and you will remain liable for all amounts due up to and including the date of termination; and/or accordingly may deny you access to our Services (or any part thereof).
          </p>


          <br>
          <h3>Entire agreement</h3>
          <p>
            The failure of us to exercise or enforce any right or provision of these Terms and Conditions shall not constitute a waiver of such right or provision.
          </p>
          <p>
            These Terms and Conditions and any policies or operating rules posted by us on this site or in respect to The Service constitutes the entire agreement and understanding between you and us and govern your use of the Service, superseding any prior or contemporaneous agreements, communications and proposals, whether oral or written, between you and us (including, but not limited to, any prior versions of the Terms and Conditions).
          </p>
          <p>
            Any ambiguities in the interpretation of these Terms and Conditions shall not be construed against the drafting party.
          </p>


          <br>
          <h3>Governing Law</h3>
          <p>
            These Terms and Conditions and any separate agreements whereby we provide you Services shall be governed by and construed in accordance with the Belgian law. In case of dispute , only the courts of the district of Ghent have the jurisdiction to adjudicate.
          </p>


          <br>
          <h3>Changes to Terms and Conditions</h3>
          <p>
            These terms and conditions were last updated on [August 10, 2015]. You can review the most current version of the Terms and Conditions at any time at this <a href="<?php echo site_url('welcome/pozyxLabs'); ?>#terms">page</a>.
          </p>
          <p>
            We reserve the right, at our sole discretion, to update, change or replace any part of these Terms and Conditions by posting updates and changes to our website. It is your responsibility to check our website periodically for changes. Your continued use of or access to our website or the Service following the posting of any changes to these Terms and Conditions constitutes acceptance of those changes.
          </p>


          <br>
          <h3>Contact information</h3>
          <p>
            Questions about the Terms and Conditions should be sent to us at <a href='mailto:policy@pozyx.io'>policy@pozyx.io</a>.
          </p>

          <p><a name="privacyPolicy"></a></p>
          <br>
          <br>
          <h2>Privacy Policy</h2>
          <h3>Information gathering and usage</h3>
          <p>
            Pozyx Labs BVBA gathers information about you from data that you provide through such things as optional or mandatory registration and the Web pages you visit. The types of personal information collected can include your name, mailing address, phone number, email address and internet protocol (IP) address. 
          </p>
          <p>
            We generally collect this information at the time you register on our website, subscribe to our service, post material or request further services. Pozyx Labs BVBA may enhance or merge your information collected on our website with other operational.
          </p>
          <p>
            The information you provide will be used to support your professional relationship with Pozyx Labs BVBA. Among other things we use this information to help you complete a transaction and in some cases to contact you for market research.
          </p>


          <br>
          <h3>Links to other websites</h3>
          <p>
            This website contains links to other websites that are not owned or controlled by Pozyx Labs BVBA. Links to other websites will open in a new browser window. Please be aware that Pozyx Labs BVBA is not responsible for the privacy practices of such other websites. We encourage you to be aware when you leave our website and to read the privacy statements of each and every website that collects personally identifiable information. This Privacy Policy applies only to information collected by this website.
          </p>


          <br>
          <h3>Message boards/forums</h3>
          <p>
            This website may make forums and message boards available to you. Please remember that any information that is disclosed in these areas becomes public information. You should exercise caution when deciding to disclose your personal information.
          </p>


          <br>
          <h3>Electronic newsletters</h3>
          <p>
            Pozyx Labs BVBA offers various free electronic newsletters. Users can sign up filling their e-mail address when visiting the website. We don't spam, never sell or rent email addresses, and there's an easy unsubscribe in every email you'll receive. Alternately users may remove themselves from mailing lists by sending an email to <a href='mailto:policy@pozyx.io'>policy@pozyx.io</a>.
          </p>


          <br>
          <h3>Log files</h3>
          <p>
            Like most standard website servers, we use log files to automatically log information such as IP addresses, browser type, internet service provider (ISP), referring/exit pages, platform type, date/time stamp, and number of clicks. We log this information to analyze trends, administer our website, track user movement in the aggregate, and gather broad demographic information for aggregate use. We do not use this information to identify you. For areas of our website that require registration, we log the user's email address and specific information concerning the user's activity on our website. This information may be tied to the user's other personal information and, if so, it is used to support the user's professional relationship with Pozyx Labs BVBA and our products.
          </p>


          <br>
          <h3>Information sharing</h3>
          <p>
            Pozyx Labs BVBA will not share, sell or rent your personal information for promotional purposes and will not disclose your personal information to outside organizations unless they are affiliated with Pozyx Labs BVBA. Distributors, rental partners, affiliates, contractors, or other business partners of Pozyx Labs BVBA may have access to your personal information but will only use your information in connection with your customer relationship with Pozyx Labs BVBA and our products.
          </p>


          <br>
          <h3>Where we store your personal data</h3>
          <p>
            We may transfer your personal information to, or store it at, a destination outside the European Economic Area (“EEA”). It may also be processed by staff operating outside the EEA who work for us or for one of our suppliers. Such staff maybe engaged in, among other things, the fulfilment of your order and the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.
          </p>


          <br>
          <h3>Legal requirements</h3>
          <p>
            Pozyx Labs BVBA may be required to disclose personal information in response to a court order or other legal requirement.    
          </p>

          <p><a name="cookiePolicy"></a></p>
          <br>
          <br>
          <h3>Cookies</h3>
          <p>
            Cookies are used to enhance your experience on our websites. A cookie is a small file of letters and numbers that does not contain any personal information, and that we store on your browser or the hard drive of your device if you agree. A cookie can contain information, such as a user ID or preferences, which the website uses to personalize content and track the pages you've visited. We don't track your personal identifiable information through cookies, unless you choose to provide this information to us by, for example, registering or logging on to our website. In the case of registering or logging on, we track information necessary to be able to follow up through contacting you. We will notify you of our use of cookies when you visit our website.
          </p>
          <p>
            We use cookies on our website to remember if you already visited our website and to remember your country and the items you have stored in your cart in the store. By using our services you accept our use of cookies.
          </p>


          <br>
          <h3>Information correction and updating</h3>
          <p>
            Pozyx Labs BVBA offers you the ability to correct or change the information collected during registration. You may change this information by returning to that website or emailing <a href='mailto:policy@pozyx.io'>policy@pozyx.io</a>. You may unsubscribe to any of our electronic newsletters by following the instructions provided in each electronic newsletter or emailing <a href='mailto:policy@pozyx.io'>policy@pozyx.io</a>. 
          </p>


          <br>
          <h3>Deleting/Opt-Out policy</h3>
          <p>
            Pozyx Labs BVBA does not offer you a means by which to completely delete personal information, but you can opt-out of any future communications from us by emailing <a href='mailto:policy@pozyx.io'>policy@pozyx.io</a>.
          </p>


          <br>
          <h3>Security</h3>
          <p>
            Pozyx Labs BVBA operates secure data networks protected by industry standard firewall and password protection systems. Any particularly sensitive information, such as a customer's credit card number collected for a commerce transaction, is encrypted prior to transmission using secure socket layer (SSL) technology. We follow generally accepted industry standards to protect the personal information submitted to us, both during transmission and once we receive it. No method of transmission over the Internet, or method of electronic storage is 100% secure, however. Therefore, we will strive commercially acceptable means to protect your personal information, we cannot guarantee its absolute security. If you have any questions about security on our website, you can email us at <a href='mailto:policy@pozyx.io'>policy@pozyx.io</a>.
          </p>


          <br>
          <h3>Children's privacy protection</h3>
          <p>
            Pozyx Labs BVBA websites do not target and are not intended for children under the age of 13. We will not knowingly collect personal data from children, and will delete such data upon discovery.
          </p>


          <br>
          <h3>Sale of business</h3>
          <p>
            Pozyx Labs BVBA may transfer personal information to a third party that acquires a business from Pozyx Labs BVBA where the information is provided as an important asset of that acquired business. You will be notified of the transfer in accordance with our Notification of Change procedure outlined at the end of this policy.
          </p>


          <h3>Contact</h3>
          <p>
            For Privacy Policy questions, email <a href='mailto:policy@pozyx.io'>policy@pozyx.io</a> or via postal mail at: <br>
            Pozyx Labs BVBA<br>
            Spellewerkstraat 46<br>
            9030 Gent, Belgium<br>
          </p>

          <h3>Changes in this privacy policy</h3>
          <p>
            This Privacy Policy was last updated on [August 10, 2015]. If we decide to change our privacy policies, we will post those changes here, and other places we deem appropriate so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it. We reserve the right to modify this privacy policy at any time, so please review it frequently. If we make material changes to this privacy policy, we will notify you here, by email, or by means of a notice on our home page.
          </p>



          <p><a name="returnPolicy"></a></p>
          <br>
          <br>
          <h2>Return Policy</h2>
          <h3>Overview</h3>
          <p>
            At Pozyx Labs BVBA, we do everything we can to ensure you are happy with your purchase, but we know that sometimes a product is just not right. We’ve made our return policy as easy as possible. 
          </p>


          <br>
          <h3>Requirements</h3>
          <p>
            30 Days: You must return your product within 30 days of having received it.
          </p>
          <p>
            Include the packing slip: You must include the packing slip as part of your return. Otherwise we will be unable to associate the return to you, and will be unable to refund your purchase.
          </p>
          <p>
            Original packaging and condition: Any purchased product(s) must be returned in all of the original packaging, and in the original condition. A return can be rejected due to damage to the product.
          </p>
          <p>
            No partial returns: We do not facilitate any partial returns or exchanges for hardware kits that we offer.
          </p>


          <br>
          <h3>Instructions</h3>
          <p>
            As long as your return adheres to the requirements provided, follow these steps to return your product:
          </p>
          <p>
            <ol type="1">
              <li>Send a mail to <a href='mailto:policy@pozyx.io'>policy@pozyx.io</a> stating you want to return products.</li>
              <li>After reviewing your return request, we will email you a return label. Print out the return label and securely attach it to the original packaging used to ship your products.</li>
              <li>Drop the package off to your preferred shipping carrier. and mail it to: <br>
                  Pozyx Labs BVBA<br>
                  Spellewerkstraat 46<br>
                  9030 Gent, Belgium</li>
            </ol> 
          </p>
          <p>
            We will receive and review the products you’ve returned. If you’ve met all of our return policy requirements, we will process the refund and send you a confirmation email. We are only able to credit the original credit card used to make the purchase. Please allow up to 8 business days for this process.
          </p>
          <p>
            If you have any questions or concerns about making a return, feel free to contact us at <a href='mailto:policy@pozyx.io'>policy@pozyx.io</a>.
          </p> 


          <br>
          <h3>Returns and refunds</h3>
          <p>
            At Pozyx Labs BVBA, we do everything we can to ensure you are happy with your purchase, but we know that sometimes a product is just not right. You may return your hardware kit within 30 days of purchase for free, as long as it is in the original condition and packaging. Free return shipping is not provided. No returns are accepted over 30 days. We do not facilitate exchanges or partial returns for hardware kits that we offer. We will cover the cost of a hardware kits return as long as the product is shipped using the included return label, and with the appropriate carrier. You must include the original packing slip as part of the return, otherwise we will be unable to refund your purchase.
          </p>


          <br>
          <h3>Warranty</h3>
          <p>
            The Pozyx boards compatible with the arduino board are provided "as is" with no warranties whatsoever, including any warranty of merchantability, noninfringement, fitness for any particular purpose, or any warranty otherwise arising out of any proposal, specification or sample. Pozyx Labs BVBA disclaims all liability, including liability for infringement of any proprietary rights, relating to use of information in any documents and files and software and no license, express or implied, by Pozyx Labs BVBA or otherwise, to any intellectual property rights is granted herein. Pozyx Labs BVBA assumes no responsibility or liability for any errors or inaccuracies that may appear in any documentation or files or any software that may be provided. The information in any documents or files is furnished for informational use only, is subject to change without notice, and should not be construed as a commitment by Pozyx Labs BVBA.
          </p>


        </div>
      </div>     

</div> <!-- /container -->

 