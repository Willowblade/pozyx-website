<style>
.table-item td, th{
    border-bottom: 1px solid gray;
    /*border-right: 1px solid #91a3c7;*/
  }
  .table-header{
    font-weight: bolder;
    background-color: #91a3c7;
  }

  .table-summary td{
    font-weight: bolder;
  }

  .items-table{
    border: 1px solid gray;
  } 

  .items-table td, .items-table th{
    padding: 5px; 
  } 
</style>

<script>
  $( document ).ready(function() {
    
    // payment method changed
    $('input[name=plugtype]').change(function(){
        checkedValue = $('input[name=plugtype]:checked').val();

        var ajax_call = $.ajax({url:'<?php echo(site_url("store/change_plug_type")); ?>', method: "post", data:{orderID: <?php echo $orderid; ?>, plugType: checkedValue}});        
      });
  });


function change_plug(id){
  event.preventDefault();

  swal({   
    title: "Are you sure?",   
    text: "Confirm that the payment is completed!",   
    type: "warning",   
    showCancelButton: true,     
    confirmButtonText: "Yes, payment complete!",   
    closeOnConfirm: true },
  function(){   
    var ajax_call = $.ajax({url:'<?php echo(site_url("admin/dashboard/order_paid")); ?>', method: "post", data:{orderID: id}});
    ajax_call.done( function(data){
      $('#row'+id).removeClass('warning').addClass('success');
    });
  }); 
}

</script>

<div class="container">
      <!-- Example row of columns -->
      <div class="row" style=" margin-top: 20px;">        

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('store'); ?>">Store</a> &gt;
            Thank you!
        </p>  

          <h2>Thank you!</h2>  

          <img src="<?php echo(base_url('assets/images/little_guy_kid.jpg')); ?>" style='float:left; width: 120px; margin-right: 20px;'>    
          <p>Your order has been registered! <br>
          order ID: <?php echo $orderid; ?><br>
          order date: <?php echo($created_at);?><br> 
          <br>
          You will be notified by email when shipment begins. Our goal is to process your order within 7 days.
          </p>    

          <p>
          Share on twitter that you are now part of the Pozyx community!<br>
          <a href="https://twitter.com/intent/tweet?button_hashtag=pozyx&text=I%20just%20preordered%20the%20pozyx%20indoor%20positioning%20system!%20Can't%20wait%20to%20get%20started!%20www.pozyx.io" class="twitter-hashtag-button" data-size="large" data-related="pozyxLabs">Tweet #pozyx</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
          </p>      

        </div>
        <div class="col-md-12">
          <h3>Payment details</h3>
          <p>
          <?php
          if($payment_method == "wire transfer" && $payment != "completed"){            
          ?>
          Please transfer the total amount of <?php echo number_format($grand_total/100,2,",","."); ?> &#8364; to the account:<br>
          Pozyx Labs BVBA<br>
          IBAN: BE40 7360 1689 7363<br>
          BIC: KREDBEBB<br>
          with the message: WEBORDER <?php echo $orderid; ?><br>
          <br>      

          <!--
          <p>Alternatively, you can still choose to pay by credit card:</p>
          <a href="<?php echo site_url('store/creditcard_payment/'.$encodedOrderID); ?>">
<button type='button' class='btn btn-success btn-lg'><span class='glyphicon glyphicon-credit-card' aria-hidden='true'></span> Pay by credit card</button></a>
          </a>
          -->
          
          <?php 
          }else if($payment_method == "wire transfer" && $payment == "completed"){
            echo "Your payment was succesfully received. You will receive an email with your shipping number and invoice once your order has been shipped to you.";
          }else if($payment_method == "credit card" && $payment == "completed"){      
            echo "Credit card payment succesfully received. You will receive an email with your shipping number and invoice once your order has been shipped to you.";
          }else if($payment_method == "paypal" && $payment == "completed"){      
            echo "Paypal transaction succesful. You will receive an email with your shipping number and invoice once your order has been shipped to you.";
          }else if($payment_method == "paypal" && $payment == "pending"){      
            echo "Paypal Transaction successful, but payment is still pending!<br>";
            echo "You need to manually authorize this payment in your <a target='_new' href='http://www.paypal.com'>Paypal Account</a> <span class='glyphicon glyphicon-exclamation-sign'></span><br>";
            echo "Once payment is authorized, we will send you a confirmation email and proceed with the order.";
          }else{
            echo "Payment with ". $payment_method . " failed. No funds were transferred.";
          }            
          ?>
          </p>          
        </div>
        <div class="col-md-6">
          <h3>Shipping Address</h3>

          <p>
          <?php 
          echo $firstname . " " . $lastname . "<br>";
          if($company_name != "") echo $company_name . "<br>";           
          echo $address_line1 . "<br>"; 
          if($address_line2 !="") echo $address_line2 . "<br>"; 
          echo $zip ." ". $city ." ". $state . "<br>"; 
          echo $countryName; 
          ?>
          </p>

          <?php        
          if($this->ion_auth->logged_in()) {   
          ?>  
          <br><a href="<?php echo site_url('store/edit_details/'.$encodedOrderID); ?>" class="btn btn-primary" role="button">Edit these details</a>
          <?php
          }  
          ?>
        </div>

        <div class="col-md-6">
          <h3>Billing Address</h3>

          <p>
          <?php 
          echo $firstname . " " . $lastname . "<br>";           
          if($company_name != "") echo $company_name . "<br>";
          if($company_vat != "") echo $company_vat . "<br>";
          echo $billing_address_line1 . "<br>";            
          if($billing_address_line2 !="") echo $billing_address_line2 . "<br>";
          echo $billing_zip ." ". $billing_city ." ". $billing_state . "<br>";           
          echo $countryName; 
          ?>
          </p>
        </div>

        <div class="col-md-12">
          <h3>Order details</h3>
          <table style="width: 100%; max-width:700px" class="items-table">
          <tr>
            <th width="10%">Quanity</th>
            <th width="50%">Description</th>
            <th width="20%" style="text-align:right">unit price</th>
            <th width="20%" style="text-align:right">subtotal</th>
          </tr>

          <?php

          foreach ($order_items as $items): 
          ?>  

            <tr class="table-item">   
              <td><?php echo $items['quantity']; ?>x</td>
              <td>

              <?php
              if(isset($items['id']))                
                echo "<a href='" . site_url('store/detail/'.$items['id']) . "'>".$items['productname']. "</a>";
              else
                echo $items['productname'];
              ?>              
              
              </td>          
              <td style="text-align:right"><?php echo  number_format(($items['unit_price']/100),2,",","."); ?> &#8364;</td>
              <td style="text-align:right"><?php echo  number_format(($items['quantity']*$items['unit_price']/100),2,",","."); ?> &#8364;</td>
            </tr>

          <?php endforeach; ?>
          <tr class="table-item">   
              <td>1x</td>              
              <td>Shipping cost</td>   
              <td></td> 
              <td style="text-align:right"><?php echo  number_format(($shipping_cost/100),2,",","."); ?> &#8364;</td>          
          </tr>

          <tr>          
            <td>&nbsp;</td>
            <td style='text-align:center'> </td>
            <td style='text-align:right'></td>
            <td style='text-align:right'></td>          
          </tr>      

          <tr class="table-summary" style='border-bottom: 1px solid gray'>
            <td></td>
            <td></td>
            <td style='text-align:right'>SUBTOTAL</td>
            <td style='text-align:right'><?php echo number_format((($subtotal+$shipping_cost)/100),2,",","."); ?> &#8364;</td>         
          </tr>           

          <?php if($discount_rate != 0) { ?>
          <tr class="table-item">   
              <td></td>   
              <td></td>              
              <td style='text-align:right'>Discount (<?php echo $discount_rate; ?>%)</td>                      
              <td style="text-align:right"><?php echo  number_format(-($subtotal/100)*($discount_rate/100),2,",","."); ?> &#8364;</td>              
          </tr>
          <?php } // end if discount ?>    
          
          <tr class="table-item">
            <td></td>
            <td></td>
            <td style='text-align:right'>TAX (<?php echo $tax_rate; ?>%)</td>
            <td style='text-align:right'><?php echo number_format(($tax_rate/100)*(($subtotal*(1-$discount_rate/100)+$shipping_cost)/100),2,",","."); ?> &#8364;</td>         
          </tr> 
          <tr class="table-summary">
            <td></td>
            <td></td>
            <td style='text-align:right'><b>TOTAL</b></td>
            <td style='text-align:right'><b><?php echo number_format($grand_total/100,2,",","."); ?> &#8364;</b></td>          
          </tr> 

          </table>

           <?php        
          if($this->ion_auth->logged_in()) {   
          ?>  
          <p><b>Information</b><br>
          Sector: <?php echo $sector ?><br>
          Tracking: <?php echo $tracking ?><br>
          </p>
          <?php
          }  
          ?>
          </div>

          <!--
          <br><br>

          <label>Please select the type of power plug</label>
          </div>          
          <div class='col-md-2'><div class="radio">         
            <img src="<?php echo(base_url('assets/images/products/plug_A.jpg')); ?>"><br>
            <label>
              <input type="radio" name="plugtype" value="A" <?php if($plug_type=='A'){echo('checked');} ?>> type A (US, Canada, Japan, ...) 
            </label>
          </div></div>
          <div class='col-md-2'><div class="radio">
            <img src="<?php echo(base_url('assets/images/products/plug_C.jpg')); ?>"><br>
            <label>
              <input type="radio" name="plugtype" value="C" <?php if($plug_type=='C'){echo('checked');} ?>> type C (EU, Russia, Africa, ...) 
            </label>
          </div></div>
          <div class='col-md-2'><div class="radio">
            <img src="<?php echo(base_url('assets/images/products/plug_D.jpg')); ?>"><br>
            <label>
              <input type="radio" name="plugtype" value="D" <?php if($plug_type=='D'){echo('checked');} ?>> type D (India)
            </label>
          </div></div>
          <div class='col-md-2'><div class="radio">
            <img src="<?php echo(base_url('assets/images/products/plug_G.jpg')); ?>"><br>
            <label>
              <input type="radio" name="plugtype" value="G" <?php if($plug_type=='G'){echo('checked');} ?>> type G (UK)
            </label>
          </div></div>
          <div class='col-md-2'><div class="radio">
            <img src="<?php echo(base_url('assets/images/products/plug_I.jpg')); ?>"><br>
            <label>
              <input type="radio" name="plugtype" value="I" <?php if($plug_type=='I'){echo('checked');} ?>> type I (China, Australia, ...)
            </label>
          </div></div>
          -->

         

          <div class="col-md-12">
          <h3>Firmware details</h3>

          <?php
          if($allow_download){
          ?>
          <p>
            A new firmware version (<?php echo $firmware_version?>) is available please <a href="<?php echo site_url('store/firmwareDownload/'. $orderid . '/' . $encodedOrderID)?>">DOWNLOAD</a> the DFU files and follow the <a href="<?php echo site_url('/Documentation/Tutorials/gettingStarted'); ?>">installation instructions</a>.
          </p>
          <?php
          } else{
          ?>
          <p>
            You have the latest firmware version (<?php echo $firmware_version?>). 
            If you haven any questions or remarks regarding your firmware version please contact <a href="mailto:support@pozyx.io?Subject=Firmware">support@pozyx.io</a>.
            You will be informed via e-mail when a new firmware version is available.
          </p>
          <?php
          }
          ?>

        </div>

          <div class="col-md-12">
          <br><br>

          <a href="<?php echo site_url('welcome/pozyxLabs'); ?>#terms">Terms and conditions.</a><br><br>
               
          <?php 
          if( ($payment_method == "wire transfer" && $payment == "completed") ||
              ($payment_method == "credit card")){
          ?>
          <a href="<?php echo base_url('documents/invoices/pro_forma_invoice_'.$encodedOrderID.'.pdf'); ?>" target="_new">
          <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-down" aria-hidden="true"></span> Download pro forma invoice</button>
          </a>

          <?php
          }   
          ?>

        </div>

      </div>
      

</div> <!-- /container -->