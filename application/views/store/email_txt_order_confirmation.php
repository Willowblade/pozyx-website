Your order has been registered!
order ID: <?php echo $orderid; ?>

order date: <?php echo $created_at; ?>

order details: <?php echo site_url('store/thank_you/' . $encodedOrderID); ?>


Payment details
<?php
if($payment_method == "wire transfer" && $payment == "pending"){            
?>
Please transfer the total amount of <?php echo number_format($grand_total/100,2,",","."); ?> € to the account:
Pozyx Labs BVBA
IBAN: BE40 7360 1689 7363
BIC: KREDBEBB
with the message: WEBORDER <?php echo $orderid; ?>


You will receive an email after we verified the wire transfer. 
You will also receive an email with your shipping number and invoice once your order has been shipped to you. Our goal is to process your order within 7 days after payment.   

<?php 
}else if($payment_method == "credit card"){      
  echo "Credit card payment succesfully received. You will receive an email with your shipping number once your order has been shipped to you. Our goal is to process your order within 7 days.";
}else if($payment_method == "paypal" && $payment == "completed"){      
	echo "Paypal transaction succesful. You will receive an email with your shipping number and invoice once your order has been shipped to you. Our goal is to process your order within 7 days.";
}else if($payment_method == "paypal" && $payment == "pending"){      
	echo "Paypal Transaction successful, but payment is still pending!";
	echo "You need to manually authorize this payment in your <a target='_new' href='http://www.paypal.com'>Paypal Account</a> <span class='glyphicon glyphicon-exclamation-sign'></span>";
	echo "Once payment is authorized, we will send you a confirmation email and proceed with the order. Our goal is to process your order within 7 days after payment.";
}          
?>

Thank you for placing your order with us!

The pozyx team.


--------------------------------------------------
Pozyx Labs BVBA
Spellewerkstraat 46
9030 Gent, Belgium
VAT: BE0634767208
contact: info@pozyx.io

  
      