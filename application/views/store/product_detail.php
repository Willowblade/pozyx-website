
<style>
	.btn {
	  background: #3498db;
	  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
	  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
	  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
	  background-image: -o-linear-gradient(top, #3498db, #2980b9);
	  background-image: linear-gradient(to bottom, #3498db, #2980b9);
	  -webkit-border-radius: 18;
	  -moz-border-radius: 18;
	  border-radius: 18px;
	  font-family: Arial;
	  color: #ffffff;
	  font-size: 18px;
	  padding: 10px 20px 10px 20px;
	  text-decoration: none;
	}

	.btn:hover {
	  background: #3cb0fd;
	  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
	  text-decoration: none;
	}
</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
		<p>
          	<a href="<?php echo site_url('/'); ?>">Home</a> &gt;
          	<a href="<?php echo site_url('store'); ?>">Store</a> &gt;
          	Product detail: <?php echo($productName); ?>
  		</p>	

          <h2><?php echo($productName); ?></h2>

         			

        </div>

        <div class="col-md-3 col-sm-6">          
          <img src="<?php echo(base_url('assets/images/pozyx_with_arduino.jpg')); ?>" alt="localization" title="localization" style="text-align:center; margin:auto; display:block; margin-bottom: 5px; width: 100%">              
        </div>
        <div class="col-md-9 col-sm-12">   
        	<h3><?php echo($productName); ?></h3>       
          
          	<?php echo($description); ?>

          	<p><b><?php echo($price); ?>€</b></p>

          	<div class='btn'>Order now</div>

       </div>

       <div class="col-md-12 col-sm-12">   
       		<h3>Documents</h3>
       		<ul>
				<li>datasheet</li>
				<li>application note</li>				
			</ul>

			<h3>Features</h3>     
			<ul>
				<li>Accurate positioning</li>
				<li>IMU</li>
				<li>wireless communication</li>
				<li></li>
				<li></li>
			</ul>
       </div>

   </div>

   <a href="<?php echo site_url('store'); ?>">return to the store.</a>

</div>

<div class="jumbotron" style="background-image:url('images/achtergrondkleur2.jpg');">
  <div class="container">        
    <div class="row" style='margin-top: 20px; margin-bottom: 50px'>        

      <div class="col-md-12">
        <h2>Technical Specs</h2>      
      </div>
      <div class="col-md-3 col-sm-6" style="padding-left: 25px; padding-right: 25px">
        <h3>Board</h3>
        <ul>
          <li>Arduino compatible</li>
          <li>STM32F4 microcontroller</li>
          <li>I2C for serial communication</li>
          <li>micro USB for firmware updates</li>
          <li>4 general purpose LEDS</li>                  
          <li class="hidden-lg hidden-md">4 general purpose LEDS</li>
          <li class="hidden-lg hidden-md">2 LEDs for UWB connectivity</li>
          <li class="hidden-lg hidden-md">4 optional GPIO pins</li>         
        </ul>
      </div>
      <div class="col-md-3 hidden-xs hidden-sm" style="padding-left: 25px; padding-right: 25px">
        <h3>&nbsp;</h3>
        <ul>    
          <li>2 LEDs for UWB connectivity</li>              
          <li>10 optional GPIO pins</li>   
          <li>Onboard 3.3V regulator:
          automatic power selection
          from battery, arduino or usb.</li>         
        </ul>         
     </div>
      <div class="col-md-3 col-sm-6" style="padding-left: 25px; padding-right: 25px">
        <h3>UWB module</h3>
        <ul>
          <li>DWM1000 module</li>
          <li>10 cm accurate ranging</li>
          <li>up to 100m range</li>
          <li>frequency range 3.5-4GHz</li>
          <li>up to 6.8Mbps communication</li>
        </ul>
      </div>
      <div class="col-md-3 col-sm-6" style="padding-left: 25px; padding-right: 25px">
        <h3>Motion sensors</h3>
        <ul>
          <li>MPU9250 module</li>
          <li>3 axis accelerometer</li>
          <li>3 axis gyroscope</li>
          <li>3 axis compass</li>
          <li>MPL3115A2 pressure sensor</li>
        </ul>
      </div>
    </div>
  </div> <!-- /container -->
</div>    



