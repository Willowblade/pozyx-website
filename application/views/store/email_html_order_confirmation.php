<table>
<tr>
  <td colspan="2">
    <h2>Thank you!</h2>  

    <p>Your order has been registered! <br>
    order ID: <?php echo $orderid; ?><br>
    order date: <?php echo($created_at);?><br> 
    <br>
    You will be notified by email when shipment begins. Our goal is to process your order within 7 days.
    </p>       

 
    <h3>Payment details</h3>
    <p>
    <?php
    if($payment_method == "wire transfer" && $payment == "pending"){            
    ?>
    Please transfer the total amount of <?php echo number_format($grand_total/100),2,",","."); ?> &#8364; to the account:<br>
    Pozyx Labs BVBA<br>
    IBAN: BE40 7360 1689 7363<br>
    BIC: KREDBEBB<br>
    with the message: WEBORDER <?php echo $orderid; ?><br>
    <br>          

    <?php 
    }else if($payment_method == "wire transfer" && $payment == "completed"){
      echo "Your payment was succesfully received. You will receive an email with your shipping number once your order has been shipped to you.";
    }else if($payment_method == "credit card"){      
      echo "Credit card payment succesfully received. You will receive an email with your shipping number once your order has been shipped to you.";
    }      
    ?>
    </p> 
  </td>
</tr>
<tr>
  <td>         
    <h3>Shipping Address</h3>

    <p>
    <?php 
    echo $firstname . " " . $lastname . "<br>";
    if($company_name != "") echo $company_name . "<br>";           
    echo $address_line1 . "<br>"; 
    if($address_line2 !="") echo $address_line2 . "<br>"; 
    echo $zip ." ". $city ." ". $state . "<br>"; 
    echo $countryName; 
    ?>
    </p>
  </td>
  <td>
    <h3>Billing Address</h3>

    <p>
    <?php 
    echo $firstname . " " . $lastname . "<br>";           
    if($company_name != "") echo $company_name . "<br>";
    if($company_vat != "") echo $company_vat . "<br>";
    echo $billing_address_line1 . "<br>";            
    if($billing_address_line2 !="") echo $billing_address_line2 . "<br>";
    echo $billing_zip ." ". $billing_city ." ". $billing_state . "<br>";           
    echo $countryName; 
    ?>
    </p>
  </td>
<tr>
  <td colspan="2">
        
    <h3>Order details</h3>
    <table style="width: 100%; max-width:700px" class="items-table">
    <tr>
      <th width="10%">Quanity</th>
      <th width="50%">Description</th>
      <th width="20%" style="text-align:right">unit price</th>
      <th width="20%" style="text-align:right">subtotal</th>
    </tr>

    <?php

    foreach ($order_items as $items): 
    ?>  

      <tr class="table-item">   
        <td><?php echo $items['quantity']; ?>x</td>
        <td>
          <?php
            if(isset($items['id']))                
              echo "<a href='" . site_url('store/detail/'.$items['id']) . "'>".$items['productname']. "</a>";
            else
              echo $items['productname'];
          ?>    
        </td>          
        <td style="text-align:right"><?php echo  number_format(($items['unit_price']/100),2,",","."); ?> &#8364;</td>
        <td style="text-align:right"><?php echo  number_format(($items['quantity']*$items['unit_price']/100),2,",","."); ?> &#8364;</td>
      </tr>

    <?php endforeach; ?>

    <tr class="table-item">   
        <td>1x</td>
        <td>Shipping cost</td>          
        <td style="text-align:right"><?php echo  number_format(($shipping_cost/100),2,",","."); ?> &#8364;</td>
        <td style="text-align:right"><?php echo  number_format(($shipping_cost/100),2,",","."); ?> &#8364;</td>
      </tr>
    <tr class="table-item">          
      <td>&nbsp;</td>
      <td style='text-align:center'> </td>
      <td style='text-align:right'></td>
      <td style='text-align:right'></td>          
    </tr>       
    <tr class="table-summary">
      <td></td>
      <td></td>
      <td style='text-align:right'>SUBTOTAL</td>
      <td style='text-align:right'><?php echo number_format((($subtotal+$shipping_cost)/100),2,",","."); ?> &#8364;</td>         
    </tr> 
    <tr class="table-summary">
      <td></td>
      <td></td>
      <td style='text-align:right'>TAX (<?php echo $tax_rate; ?>%)</td>
      <td style='text-align:right'><?php echo number_format(($tax_rate/100)*(($subtotal+$shipping_cost)/100),2,",","."); ?> &#8364;</td>         
    </tr> 
    <tr class="table-summary">
      <td></td>
      <td></td>
      <td style='text-align:right'><b>TOTAL</b></td>
      <td style='text-align:right'><b><?php echo number_format($grand_total/100),2,",","."); ?> &#8364;</b></td>          
    </tr> 

    </table>

  </td>
</tr>
</table>
      