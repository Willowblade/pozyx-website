<style>

@media (min-width: 992px) {      
  #checkout-overview{  
      background-color: #EEEEEE;

      -moz-border-radius: 10px;
      -webkit-border-radius: 10px;
      border-radius: 10px; /* future proofing */
      -khtml-border-radius: 10px; /* for old Konqueror browsers */    
      width: 100%;
      padding: 5px 25px 5px 25px;
      margin-top: -5px;      
  }
}

@media (max-width: 992px) {      
  #checkout-overview{
    margin-top: 30px;
  }
}



#checkout-table{
	width: 100%;
}

#checkout-table td{
	padding-top: 5px;	
	padding-bottom: 5px;	
	border-bottom:1px solid #DDDDDD;
}

#checkout-table th{
	padding-bottom: 5px;
}
</style>
<script>

  function payment_options(){
      event.preventDefault();

      swal({   
        title: 'Payment options',   
        html: "<div style='text-align:left'>You can pay in multiple ways:<br><br><b>Wire transfer</b><br>Transfer the required amount manually from your bank to our account:<br>Pozyx Labs BVBA<br>IBAN: BE40 7360 1689 7363<br>BIC: KREDBEBB<br>"+
      "After the money is transferred (which usually takes a few days) we send you a confirmation email and will proceed with your order."+
      "<br><br><b>Credit card</b><br>We accept credit card payments from all the major credit card companies. All your credit card information is encrypted and securely processed with <a href='https://www.stripe.com' target='_new'>Stripe</a>, a major credit card handler. Your credit card information never touches our servers."+
      "<br><br><b>Paypal</b><br>Checkout can be performed with Paypal. Selecting this option redirects you to the Paypal website where you can enter your Paypal credentials OR enter your credit card information.</div>"  
      });      
  }

  function vat_rules(){
      event.preventDefault();

      swal({   
        title: 'Tax rules',   
        html: "<div style='text-align:left'>Taxes are computed depending on the country we're shipping to:<br><br><b>Inside Europe</b><br>Inside the European Union, our goods are subject to value added tax (VAT). The VAT rate is equal to the rate in the exporting country (i.e., 21% because Pozyx Labs BVBA is located in Belgium)."
        + " However, companies with a valid VAT number are not charged VAT (except companies from Belgium, which can deduct the VAT later).<br>For more information please check <a href='http://europa.eu/youreurope/business/vat-customs/cross-border/index_en.htm' target='_new'>cross-border VAT</a>.<br>"+
      "<br><br><b>Outside Europe</b><br>Outside the European Union, no value added taxes are charged (VAT). However, at the point of import, the goods may be subject to local taxes and duties. In most countries however, duties on electronic goods are zero-rated (0%). Please check with your local authorities for specific information."
      });     
  }
</script>


<?php
if($this->cart->format_number($this->cart->total()) > 0)
{    
?>

<div id="checkout-overview">

<h3>Your order</h3>

<table id="checkout-table">

<tr>
  <th style="width: 40px">Qty</th>
  <th>Item</th>
  <th style="text-align:right; width: 90px">Price</th>			  
</tr>

<?php 

echo "<input type='hidden' name='tax_rate' id='tax_rate' value='0'>";
echo "<input type='hidden' name='grand_total' id='grand_total' value='0'>";
foreach ($this->cart->contents() as $items): 
?>  

  <tr>	 
	  <td><?php echo $items['qty']; ?>x</td>
	  <?php 
    $arr = explode("_", $items['id']);
    $numeric_id = $arr[0];
    if($numeric_id != 999){ 
      echo "<td><a href='" . site_url('store/detail/'.$items['id']) ."'>" . $items['name'] . "</a>";
    }else{
      echo "<td>" . $items['name'] . "</a>";
    }
    ?>
    </td>				  
	  <td style="text-align:right"><?php echo $this->cart->format_number($items['subtotal']); ?> &#8364;</td>
	</tr>

<?php endforeach; ?>

<!--
<tr>   
    <td>1x</td>
    <td>Free shipping</td>          
    <td style="text-align:right"><?php echo $this->cart->format_number(0); ?> &#8364;</td>
  </tr>
-->

<?php if($checkout && $this->ion_auth->logged_in()) {    ?>
<tr class='reduction_row'>
  <td class="right"  colspan="2"><input type="tel" class="form-control" name="reduction_percent" id="reduction_percent" placeholder="% reduction"></td>
  <td class="right" align="right"><span id='val_reduction'>0.00</span> &#8364;</td>
</tr>
<?php  
  // end logged in 
} ?>

<?php if($checkout && $this->ion_auth->logged_in()) {    ?>
  <tr class='shipping_row'>
    <td></td>
    <td class="right" >
    Shipping cost<br>
    <input class="form-control" name="shipping_cost" id="shipping_cost" value='0' placeholder="custom shipping price"></td>
    <td style="text-align:right"><span id="shipping_cost_txt">0.00</span> &#8364;</td>
  </tr>
<?php  
  // end logged in 
}else{ ?>
  <input type='hidden' name='shipping_cost' id='shipping_cost' value='0'>
  <tr id="shipping_row" style="display:hidden">   
    <td>1x</td>
    <td id="shipping_description">shipping</td>          
    <td style="text-align:right; vertical-align:top"><span id="shipping_cost_txt">0.00</span> &#8364;</td>
</tr>
<?php } ?>


<tr class='tax_row' style="display:none">
	<td colspan="1"></td>
  <td class="right">VAT 21%</td>
  <td id="total_price_excl" class="right" align="right"><span id='val_tax'><?php echo $this->cart->format_number(($this->cart->total())*0.21); ?></span> &#8364;</td>  
</tr>
<tr class='tax_row' style="display:none">
  <td colspan="3"><br></td>
</tr>

<tr class='tax_row' style="display:none">
  <td class="right" colspan='2'><strong>Total (VAT incl.)</strong></td>
  <td class="right" align="right"><strong><span id='val_total_tax'><?php echo $this->cart->format_number($this->cart->total()*1.21); ?></span> &#8364;</strong></td>
</tr>

<tr class='notax_row'>
  <td colspan="3"><br></td>
</tr>
<tr class='notax_row'>
  <td colspan="1"></td>
  <td class="right"><strong>Total</strong></td>
  <td class="right" align="right"><strong><span id='val_total'><?php echo $this->cart->format_number($this->cart->total()); ?></span> &#8364;</strong></td>
</tr>
<tr id='tax_note_row' style="display:none">
  <td colspan="3" style="font-size: 12px; line-height: 16px">Note: shipment may be subject to import taxes and duties upon delivery.</td>  
</tr>

</table>
<div class='hidden-sm hidden-xs'><br>
  <img src="<?php echo(base_url('assets/images/little_guy_cart.jpg')); ?>" style='float: left; width: 120px;'>    
</div>

<?php 
if(!$checkout){
    echo "<div style='float: right; text-align:center'><br><a href='". site_url('store/checkout') ."''>";
    echo "<button type='button' class='btn btn-success btn-lg'><span class='glyphicon glyphicon-shopping-cart' aria-hidden='true'></span> Checkout</button></a>";
    //if($this->ion_auth->logged_in()) {       
        echo "<br> -- OR  -- <br> ";
        echo "<a href='". site_url('paypal/paypal_checkout') ."''>";
        echo "<img src='https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif'></a>";
    //}
    echo "</div><div class='clearfix'></div><div><br>";
    echo "<a href='". site_url('store/empty_cart') ."'>empty cart</a> | ";
    echo "<a onclick='javascript:payment_options()' href=''>payment options</a> | ";
    echo "<a onclick='javascript:vat_rules()' href=''>Tax rules</a> ";
    echo " <br><br></div>";
}else{
    echo "<div style='float: right'><br>";
    echo "<a href='".site_url('store') ."'>Continue shopping</a> <br>";
    echo "<a href='". site_url('store/empty_cart') ."'>empty cart</a> <br>";
    echo "<a onclick='javascript:payment_options()' href=''>payment options</a> <br>";
    echo "<a onclick='javascript:vat_rules()' href=''>Tax rules</a> ";
    echo "</div><div class='clearfix'></div><br>";
}
?>

</div>

<script>
    var subtotal = <?php echo $this->cart->total(); ?>;
    var tax_rate = 21;
    var shipping_cost = 0;
    var total = subtotal + shipping_cost;
    var total_tax = total*(1+tax_rate/100);
    var discount_pct = 0;


<?php if($this->ion_auth->logged_in()) {    ?>
    $('#reduction_percent').change(function(){
      discount_pct = $('#reduction_percent').val()/100;      

      updateTotal();
    });

    $("#shipping_cost").change(function(){
      shipping_cost = $("#shipping_cost").val()/100.0;
      total = (subtotal)*(1-discount_pct) + shipping_cost;

      $('#shipping_cost_txt').text(shipping_cost.toFixed(2));

      updateTotal();

    });
<?php  } // end logged in  ?>

function updateCart(countryCode, vat_number){
    var vat_area = ["AT","BE","BG","HR","CY","CZ","DK","EE","FI","FR","DE","EL","HU","IE","IT","LV","LT","LU","MT","NL","PL","PT","RO","SK","SI","ES","SE","MC", "IM", "IB", "PT-20", "PT-30"];
    if($.inArray(countryCode, vat_area) !== -1){      

        if(!vat_number || countryCode=="BE"){
            tax_rate = 21;
            $(".tax_row").show();            
            $(".notax_row").hide();        
        }else{
            tax_rate = 0;
            $(".tax_row").hide();            
            $(".notax_row").show();        
        }
        $("#tax_note_row").hide();
    }else{
        tax_rate = 0;
        $(".tax_row").hide();            
        $(".notax_row").show(); 
        $("#tax_note_row").show();      
    }



    // compute the shipping connection_status
    //alert(countryCode);
    $.ajax({
      dataType: "json",
      url: '<?php echo(site_url("store/get_shipping_rate/")); ?>/'+countryCode,
      async: false,
      data: {countryCode : countryCode}
    }).done(function(data)
    {
        //console.log("success");
        //console.log(data);
        // TODO: add shipping cost
        shipping_cost = data.rate_cents/100.0;
        total = (subtotal)*(1-discount_pct) + shipping_cost;

        //console.log(shipping_cost);
        //console.log(total);

        //$('#shipping_description').text(data.carrier);
        $('#shipping_description').text("Shipping");

        $('#shipping_cost').val(data.rate_cents);
        $('#shipping_cost_txt').text(shipping_cost.toFixed(2));
        $('#shipping_row').show();
    });    

    //console.log("final: " + total);
    // update these values, no worries, everything is double checked at the server side!

    updateTotal();

    //console.log("final: " + total_tax*100);
}

// this function is called when the total cost is known
function updateTotal()
{
    var discount = subtotal*(-1)*discount_pct;
    total = subtotal*(1-discount_pct) + shipping_cost;
    var tax = total*(tax_rate/100);
    total_tax = total*(1+tax_rate/100);

    
    if(discount != 0){
      $("#val_reduction").text(discount.toFixed(2));
    }
    $("#val_tax").text(tax.toFixed(2));
    $("#tax_rate").val(tax_rate);    
    $("#grand_total").val(total_tax*100);
    $("#val_total_tax").text(total_tax.toFixed(2));
    $("#val_total").text(total.toFixed(2));
}

<?php
    // we use the session values for the country to inialize the tax information
    if ($this->session->userdata('countryCode'))
    {
        echo("updateCart('". $this->session->userdata('countryCode') . "',0);" );
    }else{
        echo("updateCart(0,0);" );
    }    
?>

</script>

<?php
}else
{
  // cart is empty!
?>

<div id="checkout-overview">

<h3>Your order</h3>

<table id="checkout-table">

<tr>
  <th style="width: 40px">Qty</th>
  <th>Item</th>
  <th style="text-align:right; width: 90px">Price</th>        
</tr>

<tr class='notax_row'>
  <td colspan="3" style="text-align:center"><i>Your cart is empty</i></td>  
</tr>

</table>
<br>
<h3>Ordering info</h3>
<ul>
<li><a onclick='javascript:payment_options()' href=''>payment options</a> </li>
<li><a onclick='javascript:vat_rules()' href=''>Tax rules</a></li>
</ul>
<h3>Get a quote</h3>
<p>
Please contact <a href='mailto:sales@pozyx.io'>sales@pozyx.io</a> for bulk orders or specific localization services and consultancy requests.
</p>


</div>


<?php
}
?>