
<style>
	.store-item{
		margin-bottom: 0px;
		border-bottom: 1px solid #EEEEEE;
		padding-bottom: 15px;
		padding-top: 30px;
	}

	.store-item:hover{
		background-color: #EEEEEE;
	}

	.store-item b{
		font-size: 18px;		
	}

	.item-image{
		float:left; 
		width: 120px; 
		margin-right: 15px;
		border: 1px solid lightgray;
	}

	.item-title{
		margin-bottom: 7px;
	}

	#checkout-overview{
	    background-color: #EEEEEE;

	    -moz-border-radius: 10px;
	    -webkit-border-radius: 10px;
	    border-radius: 10px; /* future proofing */
	    -khtml-border-radius: 10px; /* for old Konqueror browsers */
	    
	    padding: 25px 25px 5px 25px; 
	    margin-bottom: 20px;
	}	
</style>

<script>
$( document ).ready(function() {
	$('[data-toggle="popover"]').popover();
});

function addItem(id, name, price){
	
	var ajax_call = $.ajax({url:'<?php echo(site_url("store/addItem")); ?>', method: "post", data:{product_id: id}});
	ajax_call.done( function(data){
		$('#shopping_cart_col').html(data);
	});

	setTimeout(function () {
        $('[data-toggle="popover"]').popover('hide');
    }, 1000);	
}

function addCustomItem(){
	
	var name = $("#custom_item").val();
	var price = $("#custom_price").val();
	var ajax_call = $.ajax({url:'<?php echo(site_url("store/addItem")); ?>', method: "post", data:{product_id: 999, prod_name:name, prod_price:price}});
	ajax_call.done( function(data){
		$('#shopping_cart_col').html(data);
	});
	
	setTimeout(function () {
        $('[data-toggle="popover"]').popover('hide');
    }, 1000);	
}

</script>

<div class="container">
<h1>Store</h1>
<p>Welcome to the Pozyx Labs store. <br>
Any new orders will be shipped within 5 working days with TNT express.
</p>

	<div class="row row-fluid" style="padding-bottom: 50px; padding-top: 30px;">
		
		<div class="col-md-4 col-sm-12 col-md-push-8" id='shopping_cart_col'>
			<?php
				$data["checkout"] = false;
		 		$this->view('store/shopping_cart', $data);
		 	?>	
	        
		</div>

		<div class="col-md-7 col-sm-12 col-md-pull-4">

			<!-- Example row of columns -->
		  	<div class="row">

		  		<?php 
		  		if($this->ion_auth->logged_in()) { 		          
					echo "<div class='col-md-12'><h3>Custom products</h3></div>";
					echo "<div class='col-md-12 store-item'>";
					echo "<div class='item-title'><b>Custom item</b></div>";
					echo "<div>";
					echo "Describe the custom item that you want to include in the shopping basket.";
					echo "<br><b>Description</b><br>";
					echo "<input type='text' class='form-control' id='custom_item' placeholder='Item description'>";
					echo "<br><b>Price (&#8364;)</b><br>";
					echo "<input type='text' class='form-control' id='custom_price' placeholder='0.0' style='width:150px'>";
					echo "</div>";            			            			
					echo "<button class='btn btn-success' onclick='javascript:addCustomItem()' type='button' style='float:right'
					 data-original-title='success' data-toggle='popover' data-placement='bottom' data-content='test product added to your order'>Order now</button>";
					echo "<a href=''><button class='btn btn-default' type='button' style='float:right; margin-right: 15px'>More info</button></a>";            			
					echo "<br></div>";

		  		} // end admin 
		  		?> 	


		        <div class="col-md-12">
		          <h3>Kits</h3>
		        </div>


		        <?php 				             
            		foreach($products_kits as $product){
            			echo "<div class='col-md-12 store-item'>";
            			echo "<div class='item-title'><b>";//<a href=". site_url('store/detail/'.$product->id) .">";					
            			echo htmlentities($product->name, ENT_QUOTES);
            			echo "</b>";
            			if($product->id == 2)
            				echo "&nbsp;&nbsp; <span class='glyphicon glyphicon-star' style='color:Chocolate; font-size: 16px;'></span> <i>most popular !</i> <span class='glyphicon glyphicon-star' style='color:Chocolate; font-size: 16px;'></span>";
            			echo "</div>";
            			//echo "</b></a></div>";
            			echo "<div>";
						echo $product->description;
            			echo "</div>";            			            			
            			echo "<button class='btn btn-success' onclick='javascript:addItem(".$product->id.")' type='button' style='float:right'
            			 data-original-title='success' data-toggle='popover' data-placement='bottom' data-content='".htmlentities($product->name, ENT_QUOTES)." added to your order'>Order now</button>";
            			echo "<a href='". site_url('store/detail/'.$product->id) ."'><button class='btn btn-default' type='button' style='float:right; margin-right: 15px'>More info</button></a>";            			
            			echo "<b>Price ". ($product->price_eurocent/100) . "€</b><br></div>";
            			//echo "<a href='?id=". $product->id ."''><div class='btn'>Pre-order</div></a></div>";
            		}

		        ?>

	        </div>

	        <div class="row">

		        <div class="col-md-12">
		        	<br><br>
		          <h3>Individual units</h3>
		        </div>


		        <?php 				             
            		foreach($products_units as $product){
            			echo "<div class='col-md-12 store-item'>";
            			echo "<img src=". base_url('assets/images/products/product_'. $product->id .'.jpg') ." alt='".$product->name."' title='".$product->name."' class='item-image'>";   
            			echo "<div><b>";//<a href=". site_url('store/detail/'.$product->id) .">";					
            			echo htmlentities($product->name, ENT_QUOTES);
            			echo "</b></div>";
            			//echo "</b></a></div>";
            			echo "<div>";
						echo $product->description;
            			echo "</div>";            			
            			echo "<button class='btn btn-success' onclick='javascript:addItem(".$product->id.")' type='button' style='float:right'
            			 data-original-title='success' data-toggle='popover' data-placement='bottom' data-content='".htmlentities($product->name, ENT_QUOTES)." added to your order'>Order now</button>";
            			echo "<a href='". site_url('store/detail/'.$product->id) ."'><button class='btn btn-default' type='button' style='float:right; margin-right: 15px'>More info</button></a>";            			
            			echo "<b>Price ". ($product->price_eurocent/100) . "€</b><br></div>";  
            		}

		        ?>

	        </div>
        </div>

        <!--<div class="col-md-1 hidden-sm hidden-xs"></div>        -->
    </div>

</div>