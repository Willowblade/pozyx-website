<style>
/* CSS for Credit Card Payment form */
.credit-card-box .panel-title {
    display: inline;
    font-weight: bold;
}
.credit-card-box .form-control.error {
    border-color: red;
    outline: 0;
    box-shadow: inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(255,0,0,0.6);
}
.credit-card-box label.error {
  font-weight: bold;
  color: red;
  padding: 2px 8px;
  margin-top: 2px;
}
.credit-card-box .payment-errors {
  font-weight: bold;
  color: red;
  padding: 2px 8px;
  margin-top: 2px;
}
.credit-card-box label {
    display: block;
}
/* The old "center div vertically" hack */
.credit-card-box .display-table {
    display: table;
}
.credit-card-box .display-tr {
    display: table-row;
}
.credit-card-box .display-td {
    display: table-cell;
    vertical-align: middle;
    width: 50%;
}
/* Just looks nicer */
.credit-card-box .panel-heading img {
    min-width: 180px;
}

.payment-box{
    margin-left: 20px;
}

</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
<!--<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>-->


<!--<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.2.3/jquery.payment.min.js"></script>-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.3.2/jquery.payment.min.js"></script>


<!-- If you're using Stripe for payments -->
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>


<form role="form" id="payment-form" action="process_order" method="post">
<div class="container">
      <!-- Example row of columns -->
      <div class="row" style=" margin-top: 20px;">        

        <div class="col-xs-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('store'); ?>">Store</a> &gt;
            Checkout
        </p>  

          <h2>Checkout</h2>       

        </div>

        <div class="col-md-4 col-xs-12 col-md-push-8" style='margin-bottom: -60px'>
            <?php
              $data["checkout"] = true;
              $this->view('store/shopping_cart', $data);
            ?>  
        </div>  
        
        <div class="col-md-8 col-xs-12 col-md-pull-4">
        <div class="row">        

          <div class="col-xs-12">
            <h3>Contact information</h3>  
          </div>        

        <div class="panel">
            <div class="panel-body">

                <div class="col-xs-12 form-group">
                    <label>Email Address*</label>
                    <input type="email" class="form-control" name="email" placeholder="email address" required >
                </div>         

                <div class="col-xs-12 form-group">
                    <label for="IndividualOrCompany">On what behalf are you making this order?</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="IndividualOrCompany" value="individual" checked> Individual
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="IndividualOrCompany" value="company"> Company
                    </label>
                </div>

                <div class="col-xs-6 form-group">
                    <label>First Name*</label>
                    <input type="text" class="form-control" name="firstName" placeholder="First Name" autofocus required >
                </div>

                <div class="col-xs-6 form-group">
                    <label>Last Name*</label>
                    <input type="text"  class="form-control"  name="lastName" placeholder="Last Name" required >
                </div>                       

                <div class="col-xs-6 form-group">
                    <label for="country">Country*</label>
                    <select type="country" id="country" name="country" class="form-control" required>
                    <option value="">Select country</option>
                    <?php                         
                    foreach ($countries as $country) {
                      if ($country->countryName != "") {                            
                        echo("<option value='".$country->countryCode."'");
                        if($country->countryCode == $this->session->userdata('countryCode'))
                            echo(" selected");

                        echo(">".$country->countryName."</option>");
                      }
                    }

                    ?>
                    </select>                        
                </div>

                <div class="col-xs-6 form-group">
                    <label for="tel">Phone number<span class='visible-lg-inline'> (required for shipping carrier)</span>*</label>
                    <input type="tel" class="form-control" name="tel" placeholder="phone number" required/>
                </div>

                <div class="col-xs-6 form-group">
                    <label>Sector*</label>
                    <select type="purpose" id="purpose" name="purpose" class="form-control" required>
                    <option value="Educational">Educational</option>
                      <option value="Emergency">Emergency</option>
                      <option value="Entertainment &amp; arts">Entertainment &amp; arts</option>
                      <option value="Industrial">Industrial</option>
                      <option value="logistics">logistics</option>
                      <option value="Medical">Medical</option>
                      <option value="Military">Military</option>                      
                      <option value="Retail">Retail</option>
                      <option value="Sports">Sports</option>
                      <option value="Other">Other</option>
                    </select>
                </div> 

                <div class="col-xs-6 form-group">
                    <label>What do you want to track?*</label>
                    <select type="tracking" id="tracking" name="tracking" class="form-control" required>
                      <option value="AGV tracking">AGV tracking</option>
                      <option value="Asset tracking">Asset tracking</option>
                      <option value="Drone tracking">Drone tracking</option>
                      <option value="People tracking">People tracking</option>
                      <option value="VR tracking">VR tracking</option>
                      <option value="Other">Other</option>
                    </select>
                </div> 

                <div class="clearfix"></div>                

                <div id='companyDetails' style='display:none'>
                  <div class="col-xs-6 form-group">
                      <div class="form-group">
                          <label>Company Name*</label>
                          <input type="text" class="form-control" name="companyName" placeholder="company name" required />
                      </div>
                  </div>

                  <div class="col-xs-6 form-group has-feedback">                      
                      <label for="vatNumber">VAT Number (EU only)</label>
                      <input type="text" class="form-control" id="vatNumber" name="vatNumber" placeholder="valid VAT number">
                      <!--<span class="glyphicon form-control-feedback" id="vatNumber1"></span>-->   
                  </div>
                </div>


            </div>
        </div>     

        <div class="col-xs-12">
            <h3>Billing information</h3>  
        </div>

        <div class="panel credit-card-box">
            <div class="panel-body"> 

                  <div class="col-xs-12 form-group">
                      <label for="billing_addressLine1">Address line 1*</label>
                      <input type="text" class="form-control" name="billing_addressLine1" placeholder="Address line 1" required />
                  </div>

                  <div class="col-xs-12 form-group">
                      <label for="billing_addressLine2">Address line 2</label>
                      <input type="text"  class="form-control"  name="billing_addressLine2" placeholder="Address line 2" />
                  </div>                

                  <div class="col-sm-5 col-xs-12 form-group" id="billing_form_state">
                      <label for="billing_state">State*</label>
                      <input type="text" class="form-control"  name="billing_State" id="billing_state" placeholder="State" required />
                  </div>  

                  <div class="col-sm-5 col-xs-6 form-group">
                      <label for="billing_City">City*</label>
                      <input type="text"  class="form-control"  name="billing_City" placeholder="City" required />
                  </div>

                  <div class="col-sm-2 col-xs-6 form-group">
                      <label for="billing_ZIP"><span class='hidden-lg'>ZIP code*</span><span class='visible-lg-inline'><span id="PostalOrZip">Postal</span> Code*</span></label>
                      <input type="text"  class="form-control"  name="billing_ZIP" placeholder="ZIP" required />
                  </div>                 
 
            </div>
        </div>     

        <div class="col-xs-12">
            <h3>Shipping information</h3>  
        </div>

        <div class="panel">
            <div class="panel-body">

                <div class="col-xs-12">
                  <div class="checkbox">
                      <label>
                          <input type="checkbox" name="billingAddressIsShipping" id="billingAddressIsShipping" value="1" checked> <p>Shipping address is the same as billing address</p>
                      </label>
                  </div>                    
                </div>  

                <div id="shipping_address_div" style="display:none">
                  <div class="col-xs-12 form-group">
                      <div class="form-group">
                          <label>Address line 1*</label>
                          <input type="addressLine1" class="form-control" name="addressLine1" placeholder="Address line 1" autocomplete="address" />
                      </div>
                  </div>

                  <div class="col-xs-12 form-group">
                      <label>Address line 2</label>
                      <input type="addressLine2"  class="form-control"  name="addressLine2" placeholder="Address line 2" autocomplete="address2" />
                  </div>                

                  <div class="col-sm-5 col-xs-12 form-group" id="form_state">
                      <label for="state">State*</label>
                      <input type="text" class="form-control"  name="State" id="state" placeholder="State" required />
                  </div>  

                  <div class="col-sm-5 col-xs-6 form-group">
                      <label for="City">City*</label>
                      <input type="text"  class="form-control"  name="City" placeholder="City" autocomplete="City" />
                  </div>

                  <div class="col-sm-2 col-xs-6 form-group">
                      <label for="ZIP"><span class='hidden-lg'>ZIP code*</span><span class='visible-lg-inline'><span id="PostalOrZip">Postal</span> Code*</span></label>
                      <input type="text"  class="form-control"  name="ZIP" placeholder="ZIP" autocomplete="ZIP" />
                  </div> 
                </div>              

                
            </div>  

        </div>
   


        <div class="col-xs-12">
            <h3>Payment information</h3>  
        </div>


        <!-- You can make it whatever width you want. I'm making it full width
             on <= small devices and 4/12 page width on >= medium devices -->
        <div class="col-xs-12">
            <div class="panel-body">

              <?php if($this->ion_auth->logged_in()) {   ?>
              <div class="col-xs-12">
                  <div class="checkbox">
                      <label>
                          <input type="checkbox" name="orderIsQuote" id="orderIsQuote" value="1" checked> <p>This is a quote <span  class="text-muted">(only wire transfer is possible for a quote)</span></p>
                      </label>
                  </div>                    
              </div>   
              <?php   } // end logged in      ?>

              <p>Select you payment option</p>

              <div class="radio">
                <label><input type="radio" name="paymentOption" value="wire transfer" checked>Pay by wire transfer <span  class="text-muted">(Payment details will be provided by email after confirmation of the order)</span>.</label>
              </div>

              <!--<div class="payment-box" id="wire-transfer-payment">
                  
                  Please transfer the total amount tot the account number:<br>
                  IBAN: BE40 7360 1689 7363<br>
                  BIC: KREDBEBB<br>
                  
              </div>
              -->

              <div class="radio">
                <label><input type="radio" name="paymentOption" value="credit card">Pay securely by credit card &nbsp;&nbsp;<img src="<?php echo(base_url('assets/images/credit_card_logos.png')); ?>" style='height:16px'>   
                </label>
              </div>      
          
              <div class="payment-box" id="credit-card-payment" style="display:none">
                  <!-- CREDIT CARD FORM STARTS HERE -->
                  <div class="panel-default credit-card-box">
                      
                      <div class="panel-body">                    
                              <div class="row">
                                  <div class="col-md-12 form-group">
                                      <label for="cardName">Name on card*</label>
                                      <input type="text" class="form-control" id="cardName" name="cardName" placeholder="Name on card" required>
                                  </div>

                                  <div class="col-sm-6 col-xs-12">
                                      <div class="form-group has-feedback">
                                        <label for="cardNumber">Card Number</label>
                                        <!--<div class="input-group">-->
                                            <input type="tel" class="form-control" id="cardNumber" name="cardNumber" placeholder="Valid Card Number"autocomplete="cc-number" required>
                                            <!--<span class="input-group-addon"><i class="fa fa-credit-card"></i></span>-->
                                            <span class="glyphicon form-control-feedback glyphicon-credit-card"></span>    
                                            
                                        <!--</div>-->  
                                      </div>                         
                                  </div>

                                  <div class="col-sm-3 col-xs-6 form-group">
                                      <label><span class="hidden-xs">Expiration</span><span class="visible-xs-inline">Exp.</span> Date</label>
                                      <input type="tel" class="form-control" id="cardExpiry" name="cardExpiry" placeholder="MM / YY" autocomplete="cc-exp" required>
                                  </div>
                                  <div class="col-sm-3 col-xs-6 form-group">
                                      <label for="cardCVC">CVC CODE</label>
                                      <input type="tel" class="form-control" id="cardCVC" name="cardCVC" placeholder="CVC" autocomplete="cc-csc" required>
                                  </div>

                                  <div class="col-md-12">
                                    <p class="text-muted">Your credit card information is processed securely through <a href="https://www.stripe.com" target="_new">Stripe</a> and never touches our servers.</p> 
                                  </div>

                              </div>   
                      </div>
                  </div>            
                  <!-- CREDIT CARD FORM ENDS HERE -->
              </div>

                  <div class="row">
                      <div class="col-xs-4">
                        <br><br>
                          <button class="btn btn-success btn-lg btn-block" id='form_submit_btn' type="submit">Finish</button>
                      </div>
                    </div>
                  
                  <div class="row" style="display:none;">
                      <div class="col-xs-12">
                          <p class="payment-errors"></p>
                      </div>
                  </div>

                  <div class="row" style="margin-top:10px">
                      <div class="col-xs-12">
                          <p class="text-muted">                          
                              By confirming you agree to the <a href="<?php echo site_url('welcome/pozyxLabs'); ?>#terms">terms and conditions</a> of Pozyx Labs.<br>
                              If you are experiencing any troubles with this form, please email us at <a href='mailto:sales@pozyx.io'>sales@pozyx.io</a>.
                          </p>                          
                      </div>
                  </div>
              </form>
            </div>

        </div>   
        </div>
        </div> 
        
      </div>

</div> <!-- /container -->


<script>
    
  var $form = $('#payment-form');
  var validator;  

  $( document ).ready(function() {

    /* Fancy restrictive input formatting via jQuery.payment library*/
    $('input[name=cardNumber]').payment('formatCardNumber');
    $('input[name=cardCVC]').payment('formatCardCVC');
    $('input[name=cardExpiry]').payment('formatCardExpiry');

    /* Form validation using Stripe client-side validation helpers */
    jQuery.validator.addMethod("cardNumber", function(value, element) {
        return this.optional(element) || Stripe.card.validateCardNumber(value);
    }, "Please specify a valid credit card number.");

    jQuery.validator.addMethod("cardExpiry", function(value, element) {    
        /* Parsing month/year uses jQuery.payment library */
        value = $.payment.cardExpiryVal(value);
        return this.optional(element) || Stripe.card.validateExpiry(value.month, value.year);
    }, "Invalid expiration date.");

    jQuery.validator.addMethod("cardCVC", function(value, element) {
        return this.optional(element) || Stripe.card.validateCVC(value);
    }, "Invalid CVC.");  

    jQuery.validator.addMethod("vatNumber", function(value, element) {
        vat_regex = /^((AT)?U[0-9]{8}|(BE)?0[0-9]{9}|(BG)?[0-9]{9,10}|(CY)?[0-9]{8}L|(CZ)?[0-9]{8,10}|(DE)?[0-9]{9}|(DK)?[0-9]{8}|(EE)?[0-9]{9}|(EL|GR)?[0-9]{9}|(ES)?[0-9A-Z][0-9]{7}[0-9A-Z]|(FI)?[0-9]{8}|(FR)?[0-9A-Z]{2}[0-9]{9}|(GB)?([0-9]{9}([0-9]{3})?|[A-Z]{2}[0-9]{3})|(HU)?[0-9]{8}|(IE)?[0-9]S[0-9]{5}L|(IT)?[0-9]{11}|(LT)?([0-9]{9}|[0-9]{12})|(LU)?[0-9]{8}|(LV)?[0-9]{11}|(MT)?[0-9]{8}|(NL)?[0-9]{9}B[0-9]{2}|(PL)?[0-9]{10}|(PT)?[0-9]{9}|(RO)?[0-9]{2,10}|(SE)?[0-9]{12}|(SI)?[0-9]{8}|(SK)?[0-9]{10})$/;
        return this.optional(element) || vat_regex.test(value);
    }, "Please specify a correct VAT number");  

    jQuery.validator.addMethod('intlphone', function(value) { 
        intRegex = /[0-9 -()+]+$/;
        return ((value.length > 6) && (intRegex.test(value)))
    }, 'Please enter a valid phone number');

    validator = $form.validate({
        focusInvalid : false,   /* dont focus on the invalid fields, this is a problem for mobile! */
        rules: {
            email: {
              required: true,
              email: true
            },
            tel: {
              required: true,
              intlphone: true
            },
            firstName: { required: true },
            lastName: { required: true },
            addressLine1: { required: true },
            City: { required: true },
            ZIP: { required: true },
            cardNumber: {
                required: true,
                cardNumber: true            
            },
            cardExpiry: {
                required: true,
                cardExpiry: true
            },
            cardCVC: {
                required: true,
                cardCVC: true
            },
            vatNumber: {
                vatNumber: true                                                  
            },
            country:{
                required: true
            }           
        },
        highlight: function(element) {
          var id_attr = "#" + $( element ).attr("id") + "1";
          $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
          $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
        },
        unhighlight: function(element) {
            var id_attr = "#" + $( element ).attr("id") + "1";
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
        },
        errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.length) {
                    error.insertAfter(element);
                } else {
                error.insertAfter(element);
                }
            } 
     });

    // trigger the form validation automatically
    //validator.form();
    validator.resetForm();

    updateCountry();

    $form.on('submit', function(e){
      e.preventDefault();

      if(paymentFormReady()){

        // Visual feedback: form is spinning
        $form.find('[type=submit]').prop({"disabled": true}).html('Validating <i class="fa fa-spinner fa-pulse"></i>');

        checkedValue = $('input[name=paymentOption]:checked').val();
        if(checkedValue == "wire transfer"){        
          submitForm("");
        }else if(checkedValue == "credit card"){        
          payWithStripe();
        }

      }else{
        sweetAlert('Almost ready...', 'Please fill in all mandatory fields.', 'warning');      
      }


    });

    // payment method changed
    $('input[name=paymentOption]').change(function(){
        checkedValue = $('input[name=paymentOption]:checked').val();
        
        if(checkedValue == "wire transfer"){
            $(".payment-box").not("#wire-transfer-payment").hide();
            $("#wire-transfer-payment").show();
        }else if (checkedValue == "credit card"){            

            $(".payment-box").not("#credit-card-payment").hide();
            $("#credit-card-payment").show();
        }
    });

    // individual or company
    $("input[name=IndividualOrCompany]:radio").change(function () {
      checkedValue = $('input[name=IndividualOrCompany]:checked').val();
      
      if(checkedValue == "individual"){
        $('#companyDetails').hide();
      }else{
        $('#companyDetails').show();
      }
    });

    // country changed
    $("#country").change(function(){
      validator.element( "#country" );
      updateCountry();
    });

    $("#vatNumber").change(function(){
      // if the VAT number is given and valid, set taxes to zero
      if($form.find('#vatNumber').val().length > 0 && valid_formfield('vatNumber')){
        updateCart($("#country").val(), true);
      }else{
        updateCart($("#country").val(), false);
      }
    });

    $("#billingAddressIsShipping").change(function(){
      validator.element( "#country" );
      
      if($("#billingAddressIsShipping").prop('checked')){
        $("#shipping_address_div").hide();
      }else{
        $("#shipping_address_div").show();
        validator.form();
      }
    });

    validator.resetForm();

  });

  function updateCountry(){
      bVAT = $("#vatNumber").val().length>0 && valid_formfield('vatNumber');
      updateCart($("#country").val(), bVAT);
        
      if($("#country").val() === "US"){
          $("#PostalOrZip").text("ZIP");
      }else{
          $("#PostalOrZip").text("Postal");
      }

      // source: http://webmasters.stackexchange.com/questions/3206/what-countries-require-a-state-province-in-the-mailing-address
      var state_required = ["US","CA","AU","CN","MX","MY","IT"];
      if($.inArray($("#country").val(), state_required) > -1){
          $("#form_state").show();
          $("#state").prop('required', true);
          $("#billing_form_state").show();
          $("#billing_state").prop('required', true);
      }else{          
          $("#form_state").hide();
          $("#state").prop('required', false);
          $("#billing_form_state").hide();
          $("#billing_state").prop('required', false);
      }
  }

  /* If you're using Stripe for payments */
  function payWithStripe() {

      // connect with Stripe
      <?php 
        $this->load->config('credentials');
        echo "Stripe.setPublishableKey('" . $this->config->item("StripePublishableKey") . "');";
      ?>
      
      /* Create token */
      var expiry = $form.find('[name=cardExpiry]').payment('cardExpiryVal');
      var ccData = {
          number: $form.find('[name=cardNumber]').val().replace(/\s/g,''),
          cvc: $form.find('[name=cardCVC]').val(),
          exp_month: expiry.month, 
          exp_year: expiry.year, 
          name: $form.find('[name=cardName]').val(),
          address_line1: $form.find('[name=billing_addressLine1]').val(),
          address_line2: $form.find('[name=billing_addressLine2]').val(),
          address_city: $form.find('[name=billing_City]').val(),
          address_state: $form.find('[name=billing_State]').val(),
          address_zip: $form.find('[name=billing_ZIP]').val(),
          address_country: $form.find('[name=country]').val()
      };
      
      Stripe.card.createToken(ccData, function stripeResponseHandler(status, response) {
          if (response.error) {
              /* Visual feedback */
              $form.find('[type=submit]').html('Try again');
              /* Show Stripe errors on the form */
              $form.find('.payment-errors').text(response.error.message);
              $form.find('.payment-errors').closest('.row').show();
          } else {
              /* Visual feedback */
              $form.find('[type=submit]').html('Processing <i class="fa fa-spinner fa-pulse"></i>');
              /* Hide Stripe errors on the form */
              $form.find('.payment-errors').closest('.row').hide();
              $form.find('.payment-errors').text("");
              // response contains id and card, which contains additional card details            
              console.log(response.id);
              console.log(response.card);
              var token = response.id;
              
              //alert("token created");
              submitForm(token); 
          }
      });
  }

  function submitForm(token){
    // combine all the form input date to be posted by ajax
    form_data = $('#payment-form').serializeObject();
    delete form_data.cardNumber;
    delete form_data.cardExpiry;
    delete form_data.cardCVC;  
    var post_data = $.extend({
        stripeToken: token
    }, form_data);
                

    //console.log(post_data);

    
    var ajax_call = $.ajax({url:'<?php echo(site_url("store/process_order")); ?>', method: "POST", data: post_data, dataType: "json"});
    
    ajax_call.done(function(data, textStatus, jqXHR) {   
        if(data.result){
          // something went wrong
          $form.find('[type=submit]').html('There was a problem').removeClass('success').addClass('error');
          // Show Stripe errors on the form 
          $form.find('.payment-errors').text('Payment error: ' + data.errorMessage);
          $form.find('.payment-errors').closest('.row').show();
          $form.find('[type=submit]').prop({"disabled": false});

        }else{
          // success !
          $form.find('[type=submit]').html('Success! <i class="fa fa-check"></i>').prop('disabled', true);
          window.location.href = "<?php echo(site_url('store/thank_you')); ?>/" + data.encodedOrderID;  

        }
        
    });
    ajax_call.fail(function(jqXHR, textStatus, errorThrown) {
        $form.find('[type=submit]').html('There was a problem').removeClass('success').addClass('error');
        // Show Stripe errors on the form 
        $form.find('.payment-errors').text('Try refreshing the page and try again. ' + textStatus);
        $form.find('.payment-errors').closest('.row').show();
        $form.find('[type=submit]').prop({"disabled": false});
    });    
             
  }
  
  // check to test the form for valid input
  function paymentFormReady() { 
      var cc_ok = valid_formfield('cardNumber') && valid_formfield('cardExpiry') && $form.find('[name=cardCVC]').val().length > 1 && valid_formfield('cardName');
      var payment_ok = $form.find('[name=paymentOption]:checked').val() == 'wire transfer' || ($form.find('[name=paymentOption]:checked').val() == 'credit card' && cc_ok);
      var contact_ok = valid_formfield('tel') && valid_formfield('firstName') && valid_formfield('lastName') && valid_formfield('email') && valid_formfield('vatNumber');
      var shipping_ok = valid_formfield('addressLine1') && valid_formfield('country') && valid_formfield('City') && valid_formfield('ZIP');
      var billing_ok  = valid_formfield('billing_addressLine1') && valid_formfield('billing_City') && valid_formfield('billing_ZIP');

      if($("#state").prop('required')){
        shipping_ok = shipping_ok && valid_formfield('State');
        billing_ok = billing_ok && valid_formfield('billing_State');
      }

      if($("#billingAddressIsShipping").prop('checked')){        
        billing_ok = true;
      }

      //$('#status').text('payment: ' + payment_ok + ", contact: " +contact_ok + " " + valid_formfield('firstName') + " " + valid_formfield('lastName')  + " " + valid_formfield('email') );
      
      if ( payment_ok && contact_ok && shipping_ok && billing_ok){
          return true;
      } else {
          return false;
      }      
  }

  /*
  $form.find('[type=submit]').prop('disabled', true);
  var form_ready = false;
  var readyInterval = setInterval(function() {
      if (!form_ready && paymentFormReady()) {
          $form.find('[type=submit]').prop('disabled', false);
          form_ready = true;          
          //clearInterval(readyInterval);
      }else if(form_ready && !paymentFormReady()){
          $form.find('[type=submit]').prop('disabled', true);
          form_ready = false;          
      }
  }, 250);
  */

  function valid_formfield(form_field) {
      return $('[name='+form_field+']').valid();//.parent('.form-group').hasClass('has-success');
  }


$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

</script>