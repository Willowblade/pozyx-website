
<style>
	.btn {
	  background: #3498db;
	  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
	  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
	  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
	  background-image: -o-linear-gradient(top, #3498db, #2980b9);
	  background-image: linear-gradient(to bottom, #3498db, #2980b9);
	  -webkit-border-radius: 18;
	  -moz-border-radius: 18;
	  border-radius: 18px;
	  font-family: Arial;
	  color: #ffffff;
	  font-size: 18px;
	  padding: 10px 20px 10px 20px;
	  text-decoration: none;
	}

	.btn:hover {
	  background: #3cb0fd;
	  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
	  text-decoration: none;
	}

  .product-pic{
    width: 100%;
    position: relative;
    margin-top: 10px;
  }

  .product-pic img{
    width: 100%;
    -webkit-border-radius: 5;
    -moz-border-radius: 5;
    border-radius: 5px;
  }

  .product-pic p{
    font-weight: bold;
    position: absolute; 
    /*top: 170px; */
    bottom: 10px;
    left: 10px; 
    width: 100%; 
    margin-bottom: 0px;
    padding-left: 5px;
    background: rgb(255, 255, 255); /* Fall-back for browsers that don't support rgba */
    background: rgba(255, 255, 255, .7);
  }

  h3{
    margin-top: 40px;
  }
</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
  		    <p>
            	<a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            	<a href="<?php echo site_url('store'); ?>">Store</a> &gt;
            	Product detail: Ready to Range
    		  </p>	

          <h2>Pozyx Ready to Range</h2>

        </div>

       <div class="col-md-3">
         <div  class='product-pic'>
         <a href="<?php echo(base_url('assets/images/products/tag_wood.jpg')); ?>">
         <img src="<?php echo(base_url('assets/images/products/tag_wood_small.jpg')); ?>"></a>
         <p>2x Pozyx tag</p>
         </div>
       </div> 

      <div class="col-md-3">
         <div  class='product-pic'>
         <a href="<?php echo(base_url('assets/images/products/remote_imu_processing.png')); ?>">
         <img src="<?php echo(base_url('assets/images/products/remote_imu_processing.png')); ?>"></a>
         <p>Lot's of examples</p>
         </div>
       </div> 

       <div class="col-md-12">

       
       <h3>Content of the kit</h3>    
        <ul>
          <li>2x <a href="<?php echo site_url('store/detail/4'); ?>">Pozyx tag</a></li>   
        </ul>

  			<h3>Description</h3>    
        <p> 
        The ready to range kit comes with 2 Pozyx tags, allowing to measure the range between the two tags. You can also use the UWB-signal to send data from one tag to the other. 
        The Pozyx tag is an Arduino compatible shield that can provide accurate positioning and motion information. The tag is meant to work in combination with the <a href="<?php echo site_url('store/detail/5'); ?>">Pozyx anchors</a>. 
        However, every tag can also function as an anchor. With only one other tag only one signal can be captured. Based on this signal the range can be calculated.
        For more details on how UWB works please read the documentation "<a href="<?php echo site_url('Documentation/doc_howDoesUwbWork'); ?>">How does ultra-wideband work?</a>".
        </p>

        <h3>Features</h3>

        <ul>
          <li>Accurate ranging between the tags</li>
          <li>Accurate motion sensing (using 9-axis sensor fusion)</li>       
          <li>Wireless communication between the tags</li>
          <li>Easy to use with the Arduino API</li>
        </ul>

        <h3>Technical Specifications</h3>
        </div>
        <div class="col-md-4">
        <p>
          <b>Pozyx board</b>
          <ul>
            <li>Arduino Uno compatible</li>
            <li>STM32F4 microcontroller</li>
            <li>I2C for serial communication</li>
            <li>Micro USB for firmware updates</li>
            <li>4 general purpose LEDS</li>
            <li>2 LEDs for UWB connectivity</li>
            <li>4 optional GPIO pins</li>
            <li>Onboard 3.3V regulator: automatic power selection from battery, arduino or usb</li>     
            <li>Tag-size: 6cm x 5.3cm</li>    
            <li>Tag-weight: 12g</li>
          </ul>
        </p>
        </div>
        <div class="col-md-4">
        <p>
          <b>Ultra-wideband</b>
          <ul>
            <li>Decawave DW1000 transceiver</li>
            <li>onboard chip antenna</li>
            <li>10cm accurate ranging</li>
            <li>Up to 100m range</li>
            <li>Frequency range 3.5-6.5GHz</li>
            <li>Up to 6.8Mbps communication</li>       
          </ul>
        </p>
        </div>
        <div class="col-md-4">
        <p>
          <b>Motion sensors</b>
          <ul>
            <li>BNO-055 chip</li>
            <li>3 axis accelerometer (tag only)</li>
            <li>3 axis gyroscope (tag only)</li>
            <li>3 axis compass (tag only)</li>
            <li>MPL3115A2 pressure sensor</li>       
          </ul>
        </p>
    
    </div>

    <div class="col-md-12">
      <h3>More information</h3>
      <p>
        More information can be found in our <a href="<?php echo site_url('/Documentation'); ?>">Documentation</a>, including tutorials, datasheets and technical descriptions.
        The <a href="https://github.com/pozyxlabs">Pozyx Arduino library</a> and the <a href="https://github.com/pozyxLabs/Pozyx-processing">processing sketches</a> can be downloaded from github <i class="fa fa-github"></i>.


      </p>
    </div>  

    <div class="col-md-12">
        <br><br>
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('store'); ?>">Store</a> &gt;
            Product detail: Ready to Localize
        </p>  

    </div>


   </div>

   

</div>


