
<style>
	.btn {
	  background: #3498db;
	  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
	  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
	  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
	  background-image: -o-linear-gradient(top, #3498db, #2980b9);
	  background-image: linear-gradient(to bottom, #3498db, #2980b9);
	  -webkit-border-radius: 18;
	  -moz-border-radius: 18;
	  border-radius: 18px;
	  font-family: Arial;
	  color: #ffffff;
	  font-size: 18px;
	  padding: 10px 20px 10px 20px;
	  text-decoration: none;
	}

	.btn:hover {
	  background: #3cb0fd;
	  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
	  text-decoration: none;
	}
</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
  		    <p>
            	<a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            	<a href="<?php echo site_url('store'); ?>">Store</a> &gt;
            	Product detail: Pozyx anchor
    		  </p>	

          <h2>Pozyx anchor</h2>

        </div>

       <div class="col-md-12">

       <div style='float: right; margin-left: 30px; width: 300px'>
       <img src="<?php echo(base_url('assets/images/products/product_5.jpg')); ?>" style='width:100%'>
       <p>
       <i>Image of the Pozyx anchor mounted inside its protective casing (power adapter not shown)</i>
       </p>
       </div>

  			<h3>Overview</h3>    
        <p> 
        The Pozyx anchor is an infrastructure element that provides the <a href="<?php echo site_url('store/detail/5'); ?>">Pozyx tags</a> with the necessary information required for accurate positioning. 
        In a sense, they can be compared to the GPS-satellites in the sky. For more details, please read the documentation "<a href="<?php echo site_url('Documentation/doc_howDoesPositioningWork'); ?>">How does positioning work?</a>".
        For 2D positioning, at least 3 anchors must be within range of a tag. For 3D positioning, 4 anchors or more are required.
        </p>

  			<ul>  
          <li>Protective casing and power adapter included</li>
          <li>On-board ultra-wideband transceiver</li>          
          <li>On-board pressure sensor</li>
          <li>Operating voltage 3.3V</li>  				      
          <li>micro-USB for updating the Pozyx firmware or powering</li>
          <li>SWD pins for programming/debugging</li>				
  			</ul>

        <h3>Description</h3>
        <p>The Pozyx tag provides accurate positioning and motion information. For this, it utilizes a set of sensors and an ultra-wideband (UWB) transceiver. 
        The principal component is the DW1000 UWB-chip from Decawave that is used for wireless ranging and messaging, which is used for positioning. 
        </p>

        <h3>On board interfaces</h3>
        <ol>  
          <li><p>
          <b>Micro USB.</b> The micro USB can be used for two purposes: either, for updating the pozyx firmware in case new features are provided through us. 
          Secondly, it can be used to connect (and power) the tag to a computer. This way, the tag can be controlled through the computer without requiring an Arduino.
          </p></li>

          <li><p>
          <b>Serial Wire Debug.</b> The serial wire debug (SWD) can be used to reprogram the tag entirely. This requires a programmer such as the ST-Link and a development environment.
          This approach allows the user to access all the sensors directly.
          </p></li>
        </ol>

        <h3>On board indicators</h3>
        <p>
        The tag contains a number of informational LEDs and GPIOs:
        </p>
        <ul>
          <li>An UWB RX led: indicates the reception of an UWB frame</li>
          <li>An UWB TX led: indicates the transmission of an UWB frame</li>
          <li>4 all-purpose LEDs</li>
          <li>4 all-purpose GPIOs</li>
        </ul>

        <p>
        The all-purpose LEDs and GPIOs can be controlled wirelessly by the tag which makes it suitable for remote control and automation. 
        For example, a GPIO can be programmed as a digital output that is turned on whenever the tag is close (less than 1m). This could be used to open doors or turn on lights automatically.
        </p>



       </div>

       <div class="col-md-12">
          <p>
              <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
              <a href="<?php echo site_url('store'); ?>">Store</a> &gt;
              Product detail: Pozyx anchor
          </p>  

        </div>

   </div>

   

</div>


