Your payment has been received!
order ID: <?php echo $orderid; ?>

order date: <?php echo $created_at; ?>

order details: <?php echo site_url('store/thank_you/' . $encodedOrderID); ?>


You will receive an email with your shipping number and invoice once your order has been shipped to you.   

Thank you for placing your order with us!

The pozyx team.


--------------------------------------------------
Pozyx Labs BVBA
Spellewerkstraat 46
9030 Gent, Belgium
VAT: BE0634767208
contact: info@pozyx.io

  
      