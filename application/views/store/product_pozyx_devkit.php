
<style>
	.btn {
	  background: #3498db;
	  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
	  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
	  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
	  background-image: -o-linear-gradient(top, #3498db, #2980b9);
	  background-image: linear-gradient(to bottom, #3498db, #2980b9);
	  -webkit-border-radius: 18;
	  -moz-border-radius: 18;
	  border-radius: 18px;
	  font-family: Arial;
	  color: #ffffff;
	  font-size: 18px;
	  padding: 10px 20px 10px 20px;
	  text-decoration: none;
	}

	.btn:hover {
	  background: #3cb0fd;
	  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
	  text-decoration: none;
	}

  .product-pic{
    width: 100%;
    position: relative;
    margin-top: 10px;
  }

  .product-pic img{
    width: 100%;
    -webkit-border-radius: 5;
    -moz-border-radius: 5;
    border-radius: 5px;
  }

  .product-pic p{
    font-weight: bold;
    position: absolute; 
    /*top: 170px; */
    bottom: 10px;
    left: 10px; 
    width: 100%; 
    margin-bottom: 0px;
    padding-left: 5px;
    background: rgb(255, 255, 255); /* Fall-back for browsers that don't support rgba */
    background: rgba(255, 255, 255, .7);
  }

  h3{
    margin-top: 40px;
  }
</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
  		    <p>
            	<a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            	<a href="<?php echo site_url('store'); ?>">Store</a> &gt;
            	Product detail: developer's kit
    		  </p>	

          <h2>Pozyx developer's kit</h2>

        </div>

        <div class="col-md-3">
         <div  class='product-pic'>
         <a href="<?php echo(base_url('assets/images/products/tag_wood.jpg')); ?>">
         <img src="<?php echo(base_url('assets/images/products/tag_wood_small.jpg')); ?>"></a>
         <p>5x Pozyx tag</p>
         </div>
       </div> 

       <div class="col-md-3">
       <div  class='product-pic'>
          <a href="<?php echo(base_url('assets/images/products/anchor_wood2.jpg')); ?>">
         <img src="<?php echo(base_url('assets/images/products/anchor_wood2_small.jpg')); ?>"></a>
         <p>4x Pozyx anchor</p>
         </div>
       
       </div> 

       <div class="col-md-3">
        <div  class='product-pic'>
        <a href="<?php echo(base_url('assets/images/products/power.jpg')); ?>">
         <img src="<?php echo(base_url('assets/images/products/power_small.jpg')); ?>"></a>
         <p>4x cable + adapter</p>
         </div>
       
       </div>

       <div class="col-md-3">
        <div  class='product-pic'>
        <a href="<?php echo(base_url('assets/images/products/ready_to_localize_open.jpg')); ?>">
         <img src="<?php echo(base_url('assets/images/products/ready_to_localize_open_small.jpg')); ?>"></a>
         <p>Ready to localize box</p>
         </div>
       </div> 

       <div class="col-md-12">      

  			<h3>Content of the kit</h3>    
        <ul>
          <li>5x <a href="<?php echo site_url('store/detail/4'); ?>">Pozyx tag</a></li>   
          <li>4x <a href="<?php echo site_url('store/detail/5'); ?>">Pozyx anchor</a> (with protective casing)</li>                 
          <li>4x usb cable 3m long</li>              
          <li>4x usb power adapter (choice in plug-types: A, C, G or I)</li>
          <li>4x velcro strips for easy attachment of the anchors</li>
        </ul>

        <h3>Description</h3>
        <p> 
          The developer's kit is more for the advanced user. It contains multiple tags and multiple anchors to allow different scenario setups.
          By default the Pozyx tag is programmed with our firmware for localization and tracking for multiple scenario's (multi-tag and multi-anchor).
          However, it is possible to reprogram the microcontroller to suit your own needs. Using the debug pins (SWD), you can load your custom code on the microcontroller of the Pozyx tag. We provide you with detailed tutorials and example files that take care of all the configuration: gpio pins, leds, and all the sensors (through SPI or I2C).
        </p>

        <p>
          The Pozyx tag provides accurate positioning and motion information. For this, it utilizes a set of sensors and an ultra-wideband (UWB) transceiver. 
          The principal component is the DW1000 UWB-chip from Decawave that is used for wireless ranging and messaging.  
          For more details on how UWB works please read the documentation "<a href="<?php echo site_url('Documentation/doc_howDoesUwbWork'); ?>">How does ultra-wideband work?</a>".
        </p>

        <h3>Features</h3>
        <ul>
          <li>Accurate 2D and 3D indoor positioning</li>
          <li>Multi-anchor support</li>
          <li>Multi-tag support (at reduced update rate)</li>
          <li>Accurate motion sensing (using 9-axis sensor fusion)</li>
          <li>Accurate ranging with tags and anchors</li>
          <li>Arduino compatible tags</li>
          <li>Wireless communication between all devices</li>
          <li>Remote control the anchors</li>         
        </ul>

        <h3>Technical Specifications</h3>
        </div>
        <div class="col-md-4">
        <p>
          <b>Pozyx board</b>
          <ul>
            <li>Arduino Uno compatible</li>
            <li>STM32F4 microcontroller</li>
            <li>I2C for serial communication</li>
            <li>Micro USB for firmware updates</li>
            <li>4 general purpose LEDS</li>
            <li>2 LEDs for UWB connectivity</li>
            <li>4 optional GPIO pins</li>
            <li>Onboard 3.3V regulator: automatic power selection from battery, arduino or usb</li>     
            <li>Tag-size: 6cm x 5.3cm</li>    
            <li>Tag-weight: 12g</li>
          </ul>
        </p>
        </div>
        <div class="col-md-4">
        <p>
          <b>Ultra-wideband</b>
          <ul>
            <li>Decawave DW1000 transceiver</li>
            <li>onboard chip antenna</li>
            <li>10cm accurate ranging</li>
            <li>Up to 100m range</li>
            <li>Frequency range 3.5-6.5GHz</li>
            <li>Up to 6.8Mbps communication</li>       
          </ul>
        </p>
        </div>
        <div class="col-md-4">
        <p>
          <b>Motion sensors</b>
          <ul>
            <li>BNO-055 chip</li>
            <li>3 axis accelerometer (tag only)</li>
            <li>3 axis gyroscope (tag only)</li>
            <li>3 axis compass (tag only)</li>
            <li>MPL3115A2 pressure sensor</li>       
          </ul>
        </p>
    
    </div>

    <div class="col-md-12">
      <h3>More information</h3>
      <p>
        More information can be found in our <a href="<?php echo site_url('/Documentation'); ?>">Documentation</a>, including tutorials, datasheets and technical descriptions.
        The <a href="https://github.com/pozyxlabs">Pozyx Arduino library</a> and the <a href="https://github.com/pozyxLabs/Pozyx-processing">processing sketches</a> can be downloaded from github <i class="fa fa-github"></i>.


      </p>
    </div>
        


    <div class="col-md-12">
        <br><br>
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('store'); ?>">Store</a> &gt;
            Product detail: Ready to Localize
        </p>  

    </div>

</div>


