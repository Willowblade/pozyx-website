
<style>
	.btn {
	  background: #3498db;
	  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
	  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
	  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
	  background-image: -o-linear-gradient(top, #3498db, #2980b9);
	  background-image: linear-gradient(to bottom, #3498db, #2980b9);
	  -webkit-border-radius: 18;
	  -moz-border-radius: 18;
	  border-radius: 18px;
	  font-family: Arial;
	  color: #ffffff;
	  font-size: 18px;
	  padding: 10px 20px 10px 20px;
	  text-decoration: none;
	}

	.btn:hover {
	  background: #3cb0fd;
	  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
	  text-decoration: none;
	}
</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
  		    <p>
            	<a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            	<a href="<?php echo site_url('store'); ?>">Store</a> &gt;
            	Product detail: Pozyx tag
    		  </p>	

          <h2>Pozyx tag</h2>

        </div>

       <div class="col-md-6 col-sm-12">

       <img src="<?php echo(base_url('assets/images/docs/pozyx_pins.jpg')); ?>" style="width:100%">
       <p>
       <i>Pin layout of the latest version of the pozyx tag.</i>
       </p>
       </div>

       <div class='clearfix'></div>

       <div class="col-md-12">

       <div style='float: right; margin-left: 30px; width: 300px'>
       <img src="<?php echo(base_url('assets/images/pozyx_with_arduino.jpg')); ?>" style='width:100%'>
       <p>
       <i>Image of the Pozyx tag mounted on an Arduino (this image is from an older version of the tag).</i>
       </p>
       </div>

  			<h3>Overview</h3>    
        <p> 
        The Pozyx tag is an Arduino compatible shield that provides accurate positioning and motion information. The tag is meant to work in combination with the <a href="<?php echo site_url('store/detail/5'); ?>">Pozyx anchors</a>. 
        However, every tag can also function as an anchor. 
        The pozyx tag connects to an Arduino board using long wire-wrap headers which extend through the shield. This keeps the pin layout intact and allows another shield to be stacked on top.
        </p>

  			<ul>
  				<li>Compatible with an Arduino board</li>
          <li>On-board ultra-wideband transceiver</li>
          <li>On-board inertial measurement unit</li>
          <li>On-board pressure sensor</li>
          <li>Operating voltage 3.3V</li>
  				<li>Connection with Arduino on I2C</li>        
          <li>micro-USB for updating the Pozyx firmware or used as virtual COM port</li>
          <li>SWD pins for programming/debugging</li>				
  			</ul>

        <h3>Description</h3>
        <p>The Pozyx tag provides accurate positioning and motion information. For this, it utilizes a set of sensors and an ultra-wideband (UWB) transceiver. 
        The principal component is the DW1000 UWB-chip from Decawave that is used for wireless ranging and messaging.  
        Furthermore, the board uses a 9-axis inertial measurement unit
        for</p>

        <h3>On board interfaces</h3>
        <!--
        <div style='float: right; margin-left: 30px; width: 150px'>
          <img src="<?php echo(base_url('assets/images/little_guy4.png')); ?>" alt="Arduino compatibility" title="Arduino compatibility">
        </div>
        -->

        <ol>
          <li><p>
          <b>I2C.</b> Arduino communicates with the pozyx tag by means of I2C (SCL and SDA pin) and two optional interrupt pins (pins 2 or 3). 
          Because the I2C bus can be shared with other peripherals, the tag will not interfere with other Arduino shields.
          The ICSP header from the Arduino is not passed through, hence other boards requiring this interface must be stacked lower than the pozyx tag.
          </p>
          </li>

          <li><p>
          <b>Micro USB.</b> The micro USB can be used for two purposes: either, for updating the pozyx firmware in case new features are provided through us. 
          Secondly, it can be used to connect (and power) the tag to a computer. This way, the tag can be controlled through the computer without requiring an Arduino.
          </p></li>

          <li><p>
          <b>Serial Wire Debug.</b> The serial wire debug (SWD) can be used to reprogram the tag entirely. This requires a programmer such as the ST-Link and a development environment.
          This approach allows the user to access all the sensors directly.
          </p></li>
        </ol>

        <h3>On board indicators</h3>
        <p>
        The tag contains a number of informational LEDs and GPIOs:
        </p>
        <ul>
          <li>An UWB RX led: indicates the reception of an UWB frame</li>
          <li>An UWB TX led: indicates the transmission of an UWB frame</li>
          <li>4 custom programmable LEDs</li>
          <li>4 custom programmable GPIOs</li>
        </ul>



       </div>

       <div class="col-md-12">
          <br><br>
          <p>
              <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
              <a href="<?php echo site_url('store'); ?>">Store</a> &gt;
              Product detail: Pozyx tag
          </p>  

        </div>

   </div>

    

</div>


