<style>
/* CSS for Credit Card Payment form */
.credit-card-box .panel-title {
    display: inline;
    font-weight: bold;
}
.credit-card-box .form-control.error {
    border-color: red;
    outline: 0;
    box-shadow: inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(255,0,0,0.6);
}
.credit-card-box label.error {
  font-weight: bold;
  color: red;
  padding: 2px 8px;
  margin-top: 2px;
}
.credit-card-box .payment-errors {
  font-weight: bold;
  color: red;
  padding: 2px 8px;
  margin-top: 2px;
}
.credit-card-box label {
    display: block;
}
/* The old "center div vertically" hack */
.credit-card-box .display-table {
    display: table;
}
.credit-card-box .display-tr {
    display: table-row;
}
.credit-card-box .display-td {
    display: table-cell;
    vertical-align: middle;
    width: 50%;
}
/* Just looks nicer */
.credit-card-box .panel-heading img {
    min-width: 180px;
}

.payment-box{
    margin-left: 20px;
}

/* styles for the order information table */
.table-item td, th{
    border-bottom: 1px solid gray;
    /*border-right: 1px solid #91a3c7;*/
  }
  .table-header{
    font-weight: bolder;
    background-color: #91a3c7;
  }

  .table-summary td{
    font-weight: bolder;
  }

  .items-table{
    border: 1px solid gray;
  } 

  .items-table td, .items-table th{
    padding: 5px; 
  } 

</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
<!--<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>-->


<!--<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.2.3/jquery.payment.min.js"></script>-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.3.2/jquery.payment.min.js"></script>


<!-- If you're using Stripe for payments -->
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>


<form role="form" id="payment-form" action="process_order" method="post">
<div class="container">
      <!-- Example row of columns -->
      <div class="row" style=" margin-top: 20px;">        

        <div class="col-xs-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('store'); ?>">Store</a> &gt;
            Checkout
        </p>  

          <h2>Checkout</h2>       

        </div>

        <div class="col-xs-12">
            
            <h3>Order information</h3>  

        <div class="panel-body">
            

          <table style="width: 100%; max-width:700px" class="items-table">
          <tr>
            <th width="10%">Quanity</th>
            <th width="50%">Description</th>
            <th width="20%" style="text-align:right">unit price</th>
            <th width="20%" style="text-align:right">subtotal</th>
          </tr>

          <?php

          foreach ($order_items as $items): 
          ?>  

            <tr class="table-item">   
              <td><?php echo $items['quantity']; ?>x</td>
              <td><a href='<?php echo site_url('store/detail/'.$items['id']); ?>'><?php echo $items['product_name']; ?></a></td>          
              <td style="text-align:right"><?php echo  number_format(($items['unit_price']/100),2,",","."); ?> &#8364;</td>
              <td style="text-align:right"><?php echo  number_format(($items['quantity']*$items['unit_price']/100),2,",","."); ?> &#8364;</td>
            </tr>

          <?php endforeach; ?>

          <tr>          
            <td>&nbsp;</td>
            <td style='text-align:center'> </td>
            <td style='text-align:right'></td>
            <td style='text-align:right'></td>          
          </tr>      

          <tr class="table-summary" style='border-bottom: 1px solid gray'>
            <td></td>
            <td></td>
            <td style='text-align:right'>SUBTOTAL</td>
            <td style='text-align:right'><?php echo number_format((($subtotal+$shipping_cost)/100),2,",","."); ?> &#8364;</td>         
          </tr>           

          <?php if($discount_rate != 0) { ?>
          <tr class="table-item">   
              <td></td>   
              <td></td>              
              <td style='text-align:right'>Discount (<?php echo $discount_rate; ?>%)</td>                      
              <td style="text-align:right"><?php echo  number_format(-($subtotal/100)*($discount_rate/100),2,",","."); ?> &#8364;</td>              
          </tr>
          <?php } // end if discount ?>

          <tr class="table-item">   
              <td></td>
              <td></td>
              <td style='text-align:right'>Shipping cost</td>    
              <td style="text-align:right"><?php echo  number_format(($shipping_cost/100),2,",","."); ?> &#8364;</td>          
          </tr>
          
          <tr class="table-item">
            <td></td>
            <td></td>
            <td style='text-align:right'>TAX (<?php echo $tax_rate; ?>%)</td>
            <td style='text-align:right'><?php echo number_format(($tax_rate/100)*(($subtotal*(1-$discount_rate/100)+$shipping_cost)/100),2,",","."); ?> &#8364;</td>         
          </tr> 
          <tr class="table-summary">
            <td></td>
            <td></td>
            <td style='text-align:right'><b>TOTAL</b></td>
            <td style='text-align:right'><b><?php echo number_format($grand_total/100,2,",","."); ?> &#8364;</b></td>          
          </tr> 

          </table>
          <br><br>

          </div>
        </div>
     

         
        <div class="col-xs-12 col-md-8">
            <h3>Payment information</h3>  

            <div class="panel-body">
              <p>Pay securely by credit card &nbsp;&nbsp;<img src="<?php echo(base_url('assets/images/credit_card_logos.png')); ?>" style='height:16px'></p>  
         
              <div class="payment-box" id="credit-card-payment">
                  <!-- CREDIT CARD FORM STARTS HERE -->
                  <div class="panel-default credit-card-box">
                      
                      <div class="panel-body">                    
                              <div class="row">
                                  <div class="col-md-12 form-group">
                                      <label for="cardName">Name on card*</label>
                                      <input type="text" class="form-control" id="cardName" name="cardName" placeholder="Name on card" required>
                                  </div>

                                  <div class="col-sm-6 col-xs-12">
                                      <div class="form-group has-feedback">
                                        <label for="cardNumber">Card Number</label>
                                        <!--<div class="input-group">-->
                                            <input type="tel" class="form-control" id="cardNumber" name="cardNumber" placeholder="Valid Card Number"autocomplete="cc-number" required>
                                            <!--<span class="input-group-addon"><i class="fa fa-credit-card"></i></span>-->
                                            <span class="glyphicon form-control-feedback glyphicon-credit-card"></span>    
                                            
                                        <!--</div>-->  
                                      </div>                         
                                  </div>

                                  <div class="col-sm-3 col-xs-6 form-group">
                                      <label><span class="hidden-xs">Expiration</span><span class="visible-xs-inline">Exp.</span> Date</label>
                                      <input type="tel" class="form-control" id="cardExpiry" name="cardExpiry" placeholder="MM / YY" autocomplete="cc-exp" required>
                                  </div>
                                  <div class="col-sm-3 col-xs-6 form-group">
                                      <label for="cardCVC">CVC CODE</label>
                                      <input type="tel" class="form-control" id="cardCVC" name="cardCVC" placeholder="CVC" autocomplete="cc-csc" required>
                                  </div>

                                  <div class="col-md-12">
                                    <p class="text-muted">Your credit card information is processed securely through <a href="https://www.stripe.com" target="_new">Stripe</a> and never touches our servers.</p> 
                                  </div>

                              </div>   
                      </div>
                  </div>            
                  <!-- CREDIT CARD FORM ENDS HERE -->
              </div>

                  <div class="row">
                      <div class="col-xs-4">
                        <br><br>
                          <button class="btn btn-success btn-lg btn-block" id='form_submit_btn' type="submit">Finish</button>
                      </div>

                      <div class="col-xs-2">
                        <br><br>
                          <button class="btn btn-lg btn-block" id='form_submit_btn'>Cancel</button>
                      </div>
                    </div>
                  
                  <div class="row" style="display:none;">
                      <div class="col-xs-12">
                          <p class="payment-errors"></p>
                      </div>
                  </div>

                  <div class="row" style="margin-top:10px">
                      <div class="col-xs-12">
                          <p class="text-muted">                          
                              By confirming you agree to the <a href="<?php echo site_url('welcome/pozyxLabs'); ?>#terms">terms and conditions</a> of Pozyx Labs.<br>
                              If you are experiencing any troubles with this form, please email us at <a href='mailto:sales@pozyx.io'>sales@pozyx.io</a>.
                          </p>                          
                      </div>
                  </div>
              </form>
            </div>

        </div>   
        </div>
        </div> 
        
      </div>

</div> <!-- /container -->


<script>
    
  var $form = $('#payment-form');
  var validator;  

  $( document ).ready(function() {

    /* Fancy restrictive input formatting via jQuery.payment library*/
    $('input[name=cardNumber]').payment('formatCardNumber');
    $('input[name=cardCVC]').payment('formatCardCVC');
    $('input[name=cardExpiry]').payment('formatCardExpiry');

    /* Form validation using Stripe client-side validation helpers */
    jQuery.validator.addMethod("cardNumber", function(value, element) {
        return this.optional(element) || Stripe.card.validateCardNumber(value);
    }, "Please specify a valid credit card number.");

    jQuery.validator.addMethod("cardExpiry", function(value, element) {    
        /* Parsing month/year uses jQuery.payment library */
        value = $.payment.cardExpiryVal(value);
        return this.optional(element) || Stripe.card.validateExpiry(value.month, value.year);
    }, "Invalid expiration date.");

    jQuery.validator.addMethod("cardCVC", function(value, element) {
        return this.optional(element) || Stripe.card.validateCVC(value);
    }, "Invalid CVC.");  

    jQuery.validator.addMethod("vatNumber", function(value, element) {
        vat_regex = /^((AT)?U[0-9]{8}|(BE)?0[0-9]{9}|(BG)?[0-9]{9,10}|(CY)?[0-9]{8}L|(CZ)?[0-9]{8,10}|(DE)?[0-9]{9}|(DK)?[0-9]{8}|(EE)?[0-9]{9}|(EL|GR)?[0-9]{9}|(ES)?[0-9A-Z][0-9]{7}[0-9A-Z]|(FI)?[0-9]{8}|(FR)?[0-9A-Z]{2}[0-9]{9}|(GB)?([0-9]{9}([0-9]{3})?|[A-Z]{2}[0-9]{3})|(HU)?[0-9]{8}|(IE)?[0-9]S[0-9]{5}L|(IT)?[0-9]{11}|(LT)?([0-9]{9}|[0-9]{12})|(LU)?[0-9]{8}|(LV)?[0-9]{11}|(MT)?[0-9]{8}|(NL)?[0-9]{9}B[0-9]{2}|(PL)?[0-9]{10}|(PT)?[0-9]{9}|(RO)?[0-9]{2,10}|(SE)?[0-9]{12}|(SI)?[0-9]{8}|(SK)?[0-9]{10})$/;
        return this.optional(element) || vat_regex.test(value);
    }, "Please specify a correct VAT number");  

    jQuery.validator.addMethod('intlphone', function(value) { 
        intRegex = /[0-9 -()+]+$/;
        return ((value.length > 6) && (intRegex.test(value)))
    }, 'Please enter a valid phone number');

    validator = $form.validate({
        focusInvalid : false,   /* dont focus on the invalid fields, this is a problem for mobile! */
        rules: {            
            cardNumber: {
                required: true,
                cardNumber: true            
            },
            cardExpiry: {
                required: true,
                cardExpiry: true
            },
            cardCVC: {
                required: true,
                cardCVC: true
            }           
        },
        highlight: function(element) {
          var id_attr = "#" + $( element ).attr("id") + "1";
          $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
          $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
        },
        unhighlight: function(element) {
            var id_attr = "#" + $( element ).attr("id") + "1";
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
        },
        errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.length) {
                    error.insertAfter(element);
                } else {
                error.insertAfter(element);
                }
            } 
     });

    // trigger the form validation automatically
    //validator.form();
    validator.resetForm();

    $form.on('submit', function(e){
      e.preventDefault();

      if(paymentFormReady()){

        // Visual feedback: form is spinning
        $form.find('[type=submit]').prop({"disabled": true}).html('Validating <i class="fa fa-spinner fa-pulse"></i>');
      
        payWithStripe();       

      }else{
        sweetAlert('Almost ready...', 'Please fill in all mandatory fields.', 'warning');      
      }


    });    

    validator.resetForm();

  });

  /* If you're using Stripe for payments */
  function payWithStripe() {

      // connect with Stripe
      <?php 
        $this->load->config('credentials');
        echo "Stripe.setPublishableKey('" . $this->config->item("StripePublishableKey") . "');";
      ?>
      
      /* Create token */
      var expiry = $form.find('[name=cardExpiry]').payment('cardExpiryVal');
      var ccData = {
          number: $form.find('[name=cardNumber]').val().replace(/\s/g,''),
          cvc: $form.find('[name=cardCVC]').val(),
          exp_month: expiry.month, 
          exp_year: expiry.year, 
          name: $form.find('[name=cardName]').val(),
          address_line1: $form.find('[name=billing_addressLine1]').val(),
          address_line2: $form.find('[name=billing_addressLine2]').val(),
          address_city: $form.find('[name=billing_City]').val(),
          address_state: $form.find('[name=billing_State]').val(),
          address_zip: $form.find('[name=billing_ZIP]').val(),
          address_country: $form.find('[name=country]').val()
      };
      
      Stripe.card.createToken(ccData, function stripeResponseHandler(status, response) {
          if (response.error) {
              /* Visual feedback */
              $form.find('[type=submit]').html('Try again');
              /* Show Stripe errors on the form */
              $form.find('.payment-errors').text(response.error.message);
              $form.find('.payment-errors').closest('.row').show();
          } else {
              /* Visual feedback */
              $form.find('[type=submit]').html('Processing <i class="fa fa-spinner fa-pulse"></i>');
              /* Hide Stripe errors on the form */
              $form.find('.payment-errors').closest('.row').hide();
              $form.find('.payment-errors').text("");
              // response contains id and card, which contains additional card details            
              console.log(response.id);
              console.log(response.card);
              var token = response.id;
              
              //alert("token created");
              submitForm(token); 
          }
      });
  }

  function submitForm(token){
    // combine all the form input date to be posted by ajax
    form_data = $('#payment-form').serializeObject();
    delete form_data.cardNumber;
    delete form_data.cardExpiry;
    delete form_data.cardCVC;  
    var post_data = $.extend({
        stripeToken: token
    }, form_data);
                

    //console.log(post_data);

    
    var ajax_call = $.ajax({url:'<?php echo(site_url("store/process_order/".$encodedOrder_id)); ?>', method: "POST", data: post_data, dataType: "json"});
    
    ajax_call.done(function(data, textStatus, jqXHR) {   
        if(data.result){
          // something went wrong
          $form.find('[type=submit]').html('There was a problem').removeClass('success').addClass('error');
          // Show Stripe errors on the form 
          $form.find('.payment-errors').text('Payment error: ' + data.errorMessage);
          $form.find('.payment-errors').closest('.row').show();
          $form.find('[type=submit]').prop({"disabled": false});

        }else{
          // success !
          $form.find('[type=submit]').html('Success! <i class="fa fa-check"></i>').prop('disabled', true);
          window.location.href = "<?php echo(site_url('store/thank_you')); ?>/" + data.encodedOrderID;  

        }
        
    });
    ajax_call.fail(function(jqXHR, textStatus, errorThrown) {
        $form.find('[type=submit]').html('There was a problem').removeClass('success').addClass('error');
        // Show Stripe errors on the form 
        $form.find('.payment-errors').text('Try refreshing the page and try again. ' + textStatus);
        $form.find('.payment-errors').closest('.row').show();
        $form.find('[type=submit]').prop({"disabled": false});
    });    
             
  }
  
  // check to test the form for valid input
  function paymentFormReady() { 
      var cc_ok = valid_formfield('cardNumber') && valid_formfield('cardExpiry') && $form.find('[name=cardCVC]').val().length > 1 && valid_formfield('cardName');
      
      if ( cc_ok){
          return true;
      } else {
          return false;
      }      
  }

  /*
  $form.find('[type=submit]').prop('disabled', true);
  var form_ready = false;
  var readyInterval = setInterval(function() {
      if (!form_ready && paymentFormReady()) {
          $form.find('[type=submit]').prop('disabled', false);
          form_ready = true;          
          //clearInterval(readyInterval);
      }else if(form_ready && !paymentFormReady()){
          $form.find('[type=submit]').prop('disabled', true);
          form_ready = false;          
      }
  }, 250);
  */

  function valid_formfield(form_field) {
      return $('[name='+form_field+']').valid();//.parent('.form-group').hasClass('has-success');
  }


$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

</script>