<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta property="og:url"                content="https://www.pozyx.io" />
    <meta property="fb:app_id"             content="9869919170" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="Pozyx - centimeter positioning for Arduino" />
    <meta property="og:description"        content="Pozyx makes indoor positioning with centimeter accuracy a reality. It's compatibility with Arduino makes it a must-have for every DIY fan." />    
    <meta property="og:image"              content="http://www.pozyx.io/images/pozyx_with_arduino.jpg" />

    <title>Pozyx - centimeter positioning for arduino</title>
    <meta name="description" content="Pozyx makes indoor positioning with centimeter accuracy a reality. It's compatibility with Arduino makes it a must-have for every DIY fan.">
    <meta name="keyword" content="pozyx, positioning, arduino, pozyx positioning, shield, arduino shield, UWB, ultra-wideband">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo(base_url('assets/css/bootstrap.min.css'));?>" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="css/sweetalert.min.css">-->
    <link rel="stylesheet" type="text/css" href="<?php echo(base_url('assets/css/sweetalert2.css')); ?>">
    <!--<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" href="<?php echo(base_url('assets/css/templates/pozyx.css')); ?>">
    <!--<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">-->

    <link rel="shortcut icon" href="<?php echo(base_url('assets/images/favicon.ico')); ?>">
     

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    
    .menu-item{
      margin-top: 5px;
      margin-left:5px;
      /*border-left: 3px solid white;*/
      margin-right: 5px;
      padding-right: 0px;
      padding-left: 0px;
      font-size: 18px;
      color: black;
      font-family: 'GothamNarrowMedium';
    }

    .menu-item a{
      font-size: 18px;
      color: black;
      font-family: 'GothamNarrowMedium';
    }

    /* Large devices (large desktops, 1200px and up) */
    @media (min-width: 1400px) { 
        .main_header{
          background-image:url('<?php echo(base_url('assets/images/HEADER_big.jpg')); ?>'); 
          height: 600px; 
          background-position: 50% 50%; 
          background-repeat: no-repeat;
          background-color: white;
        }

        .header-title{
          font-size: 108px;
          line-height: 108px; 
          color: white; 
          padding-top: 55px; 
          padding-left: 5px; 
          position:relative; 
          z-index: 99;
        }

        .header-subtitle{
        	font-size: 50px;
        	color: white;
        	/*font-style: italic;*/
        	position:relative; 
        	padding-left: 5px; 
        	margin-top: -10px;
        }

        #arduino{
        	top: 511px;
        	display: none;
        }

        #stamp{
         left: 415px; 
         top: 300px; 
         display: none;
        }
    }
     @media (max-width: 1400px) { 
        .main_header{
          background-image:url('<?php echo(base_url('assets/images/HEADER_medium.jpg'));?>'); 
          height: 560px; 
          background-position: 50% 50%; 
          background-repeat: no-repeat;
        }

        .header-title{
          font-size: 99px;
          line-height: 99px;
          color: white; 
          padding-top: 54px; 
          padding-left: 10px; 
          position:relative; 
          z-index: 99;
        }


        .header-title img{
        	height:200px;        	
        }


		    .header-subtitle{
        	font-size: 50px;
        	color: white;
        	/*font-style: italic;*/
        	position:relative; 
        	padding-left: 10px; 
        	margin-top: -10px;
        }

        #stamp{
          display: none;
          left: 330px; 
          top: 260px; 
        }

        #arduino{
        	display: none;
        	top: 471px;
        }
    }

    @media (min-width: 767px) { 
        .headerText{
          margin-top: 25px;
          margin-bottom: 30px;
        }
        .offers{
          height: 390px;
        }
    }

    /*@media (max-width: 991px) { */
    @media (max-width: 1199px) {	
    	.header-title{
        	margin-top: 65px;
        }
    }
    
    @media (max-width: 767px) { 
      .main_header{
          background-image:url('<?php echo(base_url('assets/images/HEADER_XSMALL.jpg'));?>'); 
          height: 560px; 
          background-position: 50% 50%; 
          background-repeat: no-repeat;
        }


        .headerText{
          padding-top: 25px;
          margin-top: 25px;          
        }

        #title-compartiment{
          text-align: center;
        }

        .header-title{
            font-size: 80px;
            padding: auto;
            margin: auto;
        } 

        .header-subtitle{
          font-size: 40px;
          color: white;
          /*font-style: italic;*/
          position:relative; 
          padding: auto;
          margin: auto;
        }

        #pozyx_dude{
          display: none;
        }       

        .navbar-collapse{
	    	background-color: #333;
	    }

	    .navbar-collapse a{
	    	color: white;
	    }
    }

    @media (max-width: 501px) { 
        .header-subtitle{          
          font-size: 30px;
          color: white;
          /*font-style: italic;*/
          position:relative; 
          padding: auto;
          margin: auto;
          margin-top: -5px;
        }

        .header-title{            
            margin-top: 0px;
        } 

    }

    #pozyx_dude{
       vertical-align: bottom;
    }

    #arduino{
    	position: absolute;    	
    	font-family: headerFont;
    	color: black;
    	font-size: 16px;
    	right:3%;
    	width: 250px;

    }

    p, li{
      font-family: GothamNarrow, Arial;
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
    }

    b{
      font-family: 'GothamNarrowMedium';
    }

    h3{
      color: #5F945F;
      font-family:'GothamNarrowMedium';
    }

    h2{
      
      font-family: headerFont;
      color: #272727;
      margin-bottom: 30px;
      font-size: 36px;
    }

    h1{
      font-family: headerFont;
    }

    .borderleft{
      border-left: 2px solid #F7F3EF;
    }

    .highlight:hover{
      /*background-color: #F7F3EF;*/      
    }

    .headerNumber{
      font-size: 54px;
      font-family: headerFont;
      color: #898989;
      float: left;
      width: 45px;
    }

    ::selection {
      background: #82DA82; /* WebKit/Blink Browsers */
    }

    ::-moz-selection {
      background: #82DA82; /* Gecko Browsers */
    }

    #stamp{
      width: 210px; 
      height: 210px;
      background-image:url('images/HEADER_cirkels.png'); 
      background-size: 210px 210px; 
      position: absolute;            
      z-index: 0; 
      text-align: center; 
      padding-top: 60px; 
      font-size: 27px; 
      line-height: 28px; 
      font-family: GothamNarrow;
      color: #272727;
    }

    #stamp b{
      font-family: 'GothamNarrowMedium';
    }

    li{
      margin-left: -22px;
      padding-left: 0px;
    }

    h4{
      font-family: 'GothamNarrowMedium';
      letter-spacing: 1px;
      color: white;
      margin-bottom: 20px;
      font-size: 22px;
    }

    #footer{
      background-color: #2E2A2B; 
      height: 300px;
      margin-top: 250px;
      margin-bottom: 0px;
      position: relative;
    }

    /* tipping point for bootstrap medium device (md) */
    @media (max-width: 992px) {  
        #footer{
          height: 360px;       
        }
    }

    #footer a{
      color: #B3B3B3;
    }

    #button{
        background-image: url('images/button.png');
        position: relative;
        top: -150px;  
        float: right;      
        background-size: 300px 300px;
        width: 300px;
        height: 300px;
        cursor: pointer;
    }

    #button-small{
        background-image: url('images/button.png');             
        background-size: 300px 300px;        
        width: 300px;
        height: 300px;
        cursor: pointer;
        margin: auto;
        margin-bottom: -170px;
        margin-top: 50px;
    }

    #button-small:hover, #button:hover{
        background-image: url('images/button_hover.png');
    }

    .container{      
      /*margin-bottom: 50px;*/
    }

    .profilepic{
      width: 70%; 
      margin-bottom: 15px;
    }

    .profilepic:hover{
      opacity: 0.9;
    }

    .FAQ-answer{
      display: hidden;
      color: #5F945F;
      text-decoration: none;
      margin-left: 15px;
    }


    #FAQ li{
      cursor: pointer;
    }

    .FAQ-question:hover{
      text-decoration: underline;
    }

    .jumbotron{
    	font-family: GothamNarrow, Arial;
    	color: #454545;
    }

    .sharrre .button{
	    float:left;
	    width:60px;
	  }

	#playVideo{
		float: right; 
		margin-top: 60px; 
		opacity: 0.75;
		filter: alpha(opacity=75); 
		width: 250px; 
		font-size: 36px;
        color: white; 
        position:relative; 
        padding-left: 5px; 
        display: block; 
        font-family: headerFont; 
        cursor: pointer; 
        line-height: 46px;
	}

  @media (min-width: 1800px) { 
    #playVideo{
        float: left;     
        width: 250px;     
        margin-left: 650px; 
        display: block; 
        left: 50%;      
        
    }
  }

	#playVideo:hover{
		opacity: 1;
		filter: alpha(opacity=100); 
	}

	.dialogInput{
		width: 100%;
		box-sizing: border-box;
		border-radius: 3px;
		border: 1px solid #d7d7d7;
		height: 43px;
		margin-top: 10px;
		margin-bottom: 17px;
		font-size: 18px;
		padding: 0px 12px;		
	}
  </style>

  </head>

  <body>

  	<div id="arduino">Arduino compatible shield</div>

    <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: rgba(255,255,255, 0.95 ); height: 65px; border:none; box-shadow: 0px 10px 100px 2px rgba(100, 100, 100, 0.1);">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"  style='background-color:#333; margin-top: 15px'>
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo site_url('welcome/pozyxLabs'); ?>" style="font-size: 18px; color: black; font-family: 'GothamNarrowMedium'; margin-top: 5px">Pozyx Labs</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class='menu-item'><a href="<?php echo site_url('/'); ?>">Home</a></li>
            <!--<li class='menu-item'><a onclick="kickstartInfo();" style="cursor:pointer">Kickstarter</a></li>-->            
            <li class='menu-item'><a href="<?php echo site_url('Documentation'); ?>">Documentation</a></li>
            <li class='menu-item'><a href="<?php echo site_url('store'); ?>">Store</a></li>            
            <li class='menu-item'><a href="<?php echo site_url('welcome/about'); ?>">About</a></li>   
            <?php if($this->ion_auth->logged_in()) { ?>
            <li class='menu-item'><a href="<?php echo site_url('admin'); ?>">Admin</a></li>      
            <?php } ?>         
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron main_header">
      <div class="container">

        <div class="row">
	      
          <div class="col-sm-6 col-md-6">
          </div>
          <div class="col-sm-6 col-md-6" id="title-compartiment">           

            <!--<div id="stamp" class="hidden-xs">
              accurate<br>
              positioning<br>
              <b>made easy</b>
            </div>-->            
            <h1 class="header-title">Pozyx<img src="<?php echo(base_url('assets/images/pozyx_dude.png')); ?>" id="pozyx_dude" class="hidden-sm hidden-md"></h1>
            <h1 class="header-subtitle">Accurate positioning</h1>   			

          </div>
        </div>      
        
      </div>	

      <div id="playVideo" class="hidden-xs" onclick="showVideo();">
      	<img src='<?php echo(base_url('assets/images/play.png')); ?>' style='float: left; margin-right: 20px'>Play demo
      </div>  

    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
          <h2>The pozyx system offers</h2>
        </div>

        <div class="col-md-3 col-sm-6 highlight offers" style="padding-left:25px; padding-right: 25px">
          <div class="headerNumber hidden-md">1</div><h3 class="headerText">Localization</h3>
          <img src="<?php echo(base_url('assets/images/little_guy1.png')); ?>" alt="localization" title="localization" style="text-align:center; margin:auto; display:block; margin-bottom: 5px">
          <p>With the use of ultra-wideband technology, it is possible to achieve <b>indoor</b> 3D positioning with <b>centimeter accuracy</b>.</p>
          <!--<p>With a full system (i.e., 5 boards), you can obtain the 3D position with <b>centimeter accuracy</b> for all your arduino projects.</p> -->         
        </div>
        <div class="col-md-3 col-sm-6 borderleft highlight offers" style="padding-left:25px; padding-right: 25px">
          <div class="headerNumber hidden-md">2</div><h3 class="headerText">Motion sensing</h3>
          <img src="<?php echo(base_url('assets/images/little_guy2.png'));?>" alt="Motion sensing" title="motion sensing" style="text-align:center; margin:auto; display:block; margin-bottom: 5px">
          <p>Pozyx comes packed with <b>accurate motion sensors</b> such as an accelerometer, gyroscope, magnetometer and a pressure sensor.</p>
          <!--
          <p>Pozyx comes with the same motion sensor present in the iPhone 6. It allows you to measure acceleration, angular rotation and the magnetic north.</p>-->
            <!--PinPoint localization shield comes with the most recent IMU chip. Providing all your acceleration, angular rotation and magnetic north direction requirements for your Arduino project.</p>-->
       </div>
        <div class="col-md-3 col-sm-6 borderleft highlight offers" style="padding-left:25px; padding-right: 25px">
          <div class="headerNumber hidden-md">3</div><h3 class="headerText">Communication</h3>
          <img src="<?php echo(base_url('assets/images/little_guy3.png')); ?>" alt="wireless Communication" title="wireless communication" style="text-align:center; margin:auto; display:block; margin-bottom: 5px">
          <p>The ultra-wideband technology provides <b>wireless</b> message communication that enables automation and communication for your (Arduino) projects.</p>
          <!--<p>PinPoint localization shield provides <b>ultra-wideband</b> wireless message communication enabling automation and communication projects for your arduino.</p> -->
        </div>
        <div class="col-md-3 col-sm-6 borderleft highlight offers" style="padding-left:25px; padding-right: 25px">
          <div class="headerNumber hidden-md">4</div><h3 class="headerText">Compatibility</h3>
          <img src="<?php echo(base_url('assets/images/little_guy4.png')); ?>" alt="Arduino compatibility" title="Arduino compatibility" style="text-align:center; margin:auto; display:block; margin-bottom: 5px">
          <p>Pozyx can function by itself or as an <b>Arduino shield</b>. We provide an Arduino C library and a Python library with lots of examples and tutorials to make positioning <b>super easy</b>!
          </p>
          <!--
           is easy to  use and comes with Arduino <b>examples and tutorials</b> allowing you to start your localization project as fast as possible.</p>
          <p>PinPoint localization shield is easy to use and comes with out of the box software libraries providing all the necessary <b>tools and tutorials</b> to get you directly started on your localization projects. </p>-->
        </div>
      

        <div class="col-md-6">
            <h2>Order now!</h2>
            <div style='padding-left:0px;'>
              <img src='<?php echo base_url("assets/images/products/product_4.jpg"); ?>' style='float: left; margin-right: 20px; width: 150px; border: 1px solid #DDD'>
              <p>Pozyx is already tracking thousands of people and objects in over 45 countries. Order your Pozyx system today and join the growing community!</p><p> Order now and get <b>free international shipping</b> (limitied offer). Delivery within 7 days.<br><br></p>
              <p><a class="btn btn-primary btn-lg" href="<?php echo site_url('store'); ?>" role="button">Order now</a></p>
            </div>
        </div>

        <style>
        .pozyx-news{
          list-style:none;  
          margin-left: 0px;
          padding-left: 0px;

        }

        .pozyx-news li{
          margin-left: 0px;
          color: gray;
        }
        </style>

        <div class="col-md-6">
            <h2>Latest news</h2>
              <ul class='pozyx-news'>
                <li>November 2nd 2016 - <a href="<?php echo site_url('Welcome/news#nov2016_spinoff'); ?>">Ghent University spinoff and expanding the team.</a></li>
                <li>August 22nd 2016 - <a href="<?php echo site_url('Welcome/news#august2016_firmware'); ?>">Firmware update v1.0.</a></li>
                <li>June 15th 2016 - <a href="<?php echo site_url('Welcome/news#july2016_team'); ?>">Pozyx expands the team.</a></li>
                <li>April 1st 2016 - <a href="<?php echo site_url('Welcome/news#april2016_pilotproject'); ?>">Pozyx Labs starts pilot project in retail.</a></li>
                <li>March 1st 2016 - <a href="<?php echo site_url('Welcome/news#march2016_kickstarter'); ?>">All Kickstarter orders shipped.</a></li>

              </ul>
              <p>
              <a href="<?php echo site_url('Welcome/news'); ?>">&raquo; read all news.</a>
              </p>
        </div>
      </div>

    </div> <!-- /container -->

    <a name="UWB"></a>  
    <div class="jumbotron">
      <div class="container">
        <div class="row">

		  <div class="col-md-12"> 

          	<h2>Ultra-wideband technology</h2>
		  </div>

          <div class="col-md-6" style="padding-left:25px; padding-right: 25px; font-size: 16px">   
                 
          
            The pozyx system is a hardware solution that provides accurate positioning and motion information. The systems consists of 5 modules, of which 4 anchors and one tag. In order to achieve a positioning accuracy of a few centimeters, the pozyx system relies on a novel wireless radio technology called ultra-wideband (UWB). 
          

          
            The accuracy achieved with this ultra-wideband technology is several times better than traditional positioning systems based on WiFi, bluetooth, RFID or GPS signals. Furthermore, the signals can penetrate walls and make it suitable for indoor environments.</p>

          Some things you can do with pozyx: 
            <ul>
            <li>Let your robot or drone navigate safely inside your house.</li>
            <li>Let the environment respond to your presence.</li>
            <li>Measure any distance, even through walls!</li>
            </ul>
          

          
            With 4 years of research experience and some recent developments in wireless UWB technology we are now able to deliver the best possible positioning system out there at an affordable price!
           
            
          </div>

          <div class="col-md-6" style="padding-left:25px; padding-right: 25px; font-size: 16px">
            
              <object width="550"  type="image/svg+xml" data="<?php echo(base_url('assets/images/demo/vadim_appartment/indoordemo.svg'));?>"></object>
              <!--<img id='demo_img' src="<?php echo(base_url('assets/images/demo_landscape.jpg'));?>" alt="pozyx shield for arduino" title="pozyx shield for arduino" style="width:100%">-->
            

           <!--
            Our first working prototypes are ready, and now we would like to share this unique piece of hardware with the world. Support us on Kickstarter to bring our prototype into production. You will be the first one to experience our system.</p>
            
            <a href="https://www.kickstarter.com/projects/pozyx/pozyx-accurate-indoor-positioning-for-arduino">The kickstarter campaign is now available until the end of June!</a>
            -->
          </div>
        </div>

      </div>
    </div>  


    
    <div class="container" style="align:center; margin-top: 50px; margin-bottom: 80px">
      <img src="<?php echo(base_url('assets/images/pozyx_accuracy.jpg'));?>" alt="positioning accuracy" title="positioning accuracy" width="100%" style="width:100%; margin-top:50px;">      
    </div>

	 <a name="Features"></a>      
      <div class="container">      
        <div class="row" style='margin-top: 20px; margin-bottom: 50px'>        

          <div class="col-md-12">
            <h2>Features</h2>     
          </div>

          <div class="col-md-6" style="padding-left: 25px; padding-right: 25px">
            <h3>Indoor ranging and 3D positioning</h3>
            <p>
              Ultra-wideband (UWB) technology is the key to accurate ranging an positioning. However, the UWB-module itself only provides accurate timestamps. We have implemented state-of-the art algorithms to obtain the most accurate range and positioning information.
              
            </p>
            <h3>Automatic anchor calibration</h3>
            <p>
              3D positioning requires at least 4 anchors to be within range (3 for 2D positioning). In general the position of these anchors must be known in advance. 
              Our shield provides the feature to obtain the anchor positions with a single push of a button. No manual measuring required!
            </p>
            <h3>Remote control</h3>
            <p>
              With the use of ultra-wideband wireless technology, messages can be transmitted over the air. These messages can be text or user data, but also commands to control remote pozyx modules. 
              We made it possible to remotely turn on LEDs, toggle pins, read out sensor data, and much more.  
            </p>
          </div>
          <div class="col-md-6" style="padding-left: 25px; padding-right: 25px">
            <h3>9-axis sensor fusion</h3>
                <p>
                  Pozyx is equipped with an accelerometer, gyroscope and magnetometer. With these sensors it is possible to obtain the orientation of the device. However, separately these sensors all have their flaws. 
                  For example, the accelerometer is noisy and the gyroscope is biased. Together these flaws can be mitigated. Pozyx offers 9-axis sensor fusion (3 axes for every sensor) to get the best possible measurements.
                </p>

            <h3>Firmware updates</h3>
            	<p>
            	   Our team keeps on working on the Pozyx system and we provide free firmware updates to fix issues, to improve positioning and to add new features. New firmwares can be uploaded easily and in no time using the USB interface.
            	</p>


                <!--<h3>Responsiveness</h3>
                By default, we program the board with our firmware for localization and tracking. However, it is possible to reprogram the microcontroller to suit your own needs.              
              Using the debug pins (SWD), you can load your custom code on the microcontroller of the board. We provide you with detailed tutorials and example files that take care of all the configuration: gpio pins, leds, and all the sensors (through SPI or I2C).  

                <p>
                  Our shield is doing many things at the same time: ranging, positioning, communicating, etc,.. To make everything run smoothly we use the popular FreeRTOS system for real time control.
                </p>-->
          </div>        

        </div>
      </div> <!-- /container -->

      <div class="jumbotron">
        <div class="container">        
          <div class="row" style='margin-top: 20px; margin-bottom: 50px'>        

            <div class="col-md-12">
              <h2>Press</h2>  

              <img src="<?php echo(base_url('assets/images/press.jpg'));?>" alt="pozyx in the press" title="Pozyx in the press" width="100%" style="width:100%; margin-bottom:25px;">

              <p  style='text-align: center'>
               <a href="http://www.popularmechanics.com/technology/a15921/pozyx-indoor-positioning-data/" target="_blank">popular mechanics</a> | 
               <a href="https://www.elektormagazine.com/articles/pozyx-shield-gives-position" target="_blank">elektor</a> | 
               <a href="http://www.engadget.com/2015/06/21/pozyx-is-this-the-end-of-ibeacons/" target="_blank">engadget </a>| 
               <a href="http://www.geeky-gadgets.com/pozyx-indoor-arduino-positioning-development-board-01-06-2015/" target="_blank">geeky gadgets</a> | 
               <a href="https://fabcross.jp/news/2015/06/20150605_pozyx.html" target="_blank">fabcross (jp)</a> | 
               <a href="http://www.analog-eetimes.com/en/kickstarter-project-offers-positioning-on-arduino.html?cmp_id=7&amp;news_id=222907306" target="_blank">EEtimes europe</a> | 
               <a href="http://postscapes.com/arduino-compatible-indoor-positioning-pozyx" target="_blank">postscapes</a> | 
               <a href="https://www.technology.org/2015/06/09/a-platform-that-provides-centimeter-accurate-positioning-and-motion-information/" target="_blank">technology.org</a> | 
               <a href="http://www.makery.info/2015/06/09/le-top-10-des-kits-de-prototypage-hardware-suite/" target="_blank">makery</a>
            </p>   

            </div>
            
          </div>
        </div> <!-- /container -->
      </div>    

      <style>
        #footer{
          margin-top: -30px;
        }
      </style>

    


     <script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
     <script src="<?php echo(base_url('assets/js/bootstrap.min.js')); ?>"></script>
     <script>
      $( document ).ready(function() {

        // bugfix for the dropdown menu not closing 
        $('.dropdown-menu select, .dropdown-menu textearea').click(function(e) {
          e.stopPropagation();
        });

        // Hide Twitter Bootstrap nav collapse on click
        $("#navbar a").click(function(e) {
          //$(".btn-navbar").click(); //bootstrap 2.x
          //$(".navbar-toggle").click() //bootstrap 3.x by Richard
          $('.nav-collapse').collapse('hide');
        });


        $(".FAQ-answer").hide();

        $("#FAQ li").on("click", function(){
          $(this).find(".FAQ-answer").toggle();
        });   

        /* no longer popups
        //popup only for non-mobiles
        if( !(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ) {
          if(getCookie('welcome') == ""){
            StartInfo(); 
            setCookie('welcome',1,1000);
          }
        }
        */
              
      });  

      function setCookie(cname, cvalue, exdays) {
          var d = new Date();
          d.setTime(d.getTime() + (exdays*24*60*60*1000));
          var expires = "expires="+d.toUTCString();
          document.cookie = cname + "=" + cvalue + "; " + expires;
      }

      function getCookie(cname) {
          var name = cname + "=";
          var ca = document.cookie.split(';');
          for(var i=0; i<ca.length; i++) {
              var c = ca[i];
              while (c.charAt(0)==' ') c = c.substring(1);
              if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
          }
          return "";
      }

      $(window).load(function() {
          // this will fire after the entire page is loaded, including images
          $('#demo_img').attr('src', "<?php echo(base_url('assets/images/demo_appartement.gif'));?>");
      });

      function isValidEmailAddress(emailAddress) {
          var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
          return pattern.test(emailAddress);
      };          

      /*
      function StartInfo(){
        swal({   
          title: "Kickstarter Success",   
          html: "In June 2015 we succesfully finished our kickstarter campaign! For those who have missed it. You can order the pozyx system now in our store! <br><br>Follow us on:<br><div id='social-icons'><a href='https://www.facebook.com/pozyx'><img src='<?php echo(base_url('assets/images/social/facebook.png')); ?>' width='48' height='48' alt='Facebook'></a><a href='https://twitter.com/pozyxLabs'><img src='<?php echo(base_url('assets/images/social/twitter.png'));?>' width='48' height='48' alt='Twitter' /></a><a href='https://www.linkedin.com/company/9438658'><img src='<?php echo(base_url('assets/images/social/linkedin.png')); ?>' width='48' height='48' alt='LinkedIn'></a><a href='https://google.com/+PozyxIo1'><img src='<?php echo(base_url('assets/images/social/googleplus.png')); ?>' width='48' height='48' alt='Google+' /></a></div>And subscribe to get an email notification with more news <br><input id='input-field2' class='dialogInput' placeholder='enter your emailaddress'>",   
          confirmButtonText: "Subscribe",           
          type: "input",   
          showCancelButton: true,   
          closeOnConfirm: false,            
          inputPlaceholder: "Your email address for an email reminder"
        }, function(){  
          inputValue = $('#input-field2').val(); 
          if (inputValue === false) return false;      
          if (inputValue === "" || !isValidEmailAddress(inputValue)) {     
            swal.showInputError("Please enter a valid email address");     
            return false;   
          }      

          subscribe(inputValue);          
        });
      }
      */

      function kickstartInfo(){
        swal({   
          title: "Kickstarter info",   
          html: "We estimate to launch our kickstarter campaign at the beginning of May. Check out our timeline:<br><br><img src='images/timeline.jpg' width='100%'><br><br>Subscribe to get an email notification when we launch!<input id='input-field' class='dialogInput' placeholder='enter your emailaddress'>",   
          confirmButtonText: "Subscribe",          
          type: "input",   
          width: 500,
          showCancelButton: true,   
          closeOnConfirm: false,            
          inputPlaceholder: "Your email address for an email reminder"
        }, function(){
          inputValue = $('#input-field').val();

          if (inputValue === false) return false;      
          if (inputValue === "" || !isValidEmailAddress(inputValue)) {     
            swal.showInputError("Please enter a valid email address");     
            return false;   
          }      

          subscribe(inputValue); 
          
        });
      }

      // url for home movie: https://www.youtube.com/embed/B9qny4G3-r8?autoplay=1&enablejsapi=1
      // url for beach movie: https://www.youtube.com/embed/UsN5NSzHFl0?autoplay=1&enablejsapi=1
      function showVideo(){
        swal({  
          title: '',           
          html: '<iframe id="yt_player" width="640" height="360" src="https://www.youtube.com/embed/B9qny4G3-r8?autoplay=1&enablejsapi=1" frameborder="0" allowfullscreen style="background-color:black">Loading video..</iframe>', 
          width: 680,
          showConfirmButton: true,
          confirmButtonText: "Close",
          allowEscapeKey: true,
          allowOutsideClick: false          
        }, function(isConfirm){             
        var iframe = document.getElementById("yt_player").contentWindow;        
        iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*'); 

        //$('#yt_player').remove();
        return false;
        });
      }

    </script>     