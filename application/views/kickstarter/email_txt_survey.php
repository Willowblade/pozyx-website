
Hi!

Thank you for supporting us on Kickstarter! 
We have been working hard this summer to finish your reward, and we believe we will deliver what we promised!
For us to go further we need some information from you, please follow the link below and enter the requested information.

Please fill in your details here: 
<?php echo site_url('kickstarter/edit_details/' . $encodedOrderID); ?>


Thanks again for supporting us. Without you, none of this would have been possible!

The Pozyx Laboratories team.

Samuel, Vadim, Koen and Michael
contact: info@pozyx.io
