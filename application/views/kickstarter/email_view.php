<div class="row">
  <div class="col-sm-4 col-sm-offset-4">
    <h1>Hello!</h1>
    <p>Please enter your kickstarter email address</p>
    <?php echo $this->session->flashdata('message');?>
    <?php echo form_open('');?>
      <div class="form-group">
        <?php echo form_label('Email','identity');?>
        <?php echo form_error('identity');?>
        <?php echo form_input('identity','','class="form-control"');?>
      </div>     
      <?php echo form_submit('submit', 'Submit', 'class="btn btn-primary btn-lg"');?>
    <?php echo form_close();?>
  </div>
</div>
