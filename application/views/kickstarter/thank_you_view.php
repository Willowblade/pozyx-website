<style>
.table-item td, th{
    border-bottom: 1px solid gray;
    /*border-right: 1px solid #91a3c7;*/
  }
  .table-header{
    font-weight: bolder;
    background-color: #91a3c7;
  }

  .table-summary td{
    font-weight: bolder;
  }

  .items-table{
    border: 1px solid gray;
  } 

  .items-table td, .items-table th{
    padding: 5px; 
  } 
</style>

<script>
  $( document ).ready(function() {
    
    // payment method changed
    $('input[name=plugtype]').change(function(){
        checkedValue = $('input[name=plugtype]:checked').val();

        var ajax_call = $.ajax({url:'<?php echo(site_url("store/change_plug_type")); ?>', method: "post", data:{orderID: <?php echo $orderid; ?>, plugType: checkedValue}});        
      });
  });


function change_plug(id){
  event.preventDefault();

  swal({   
    title: "Are you sure?",   
    text: "Confirm that the payment is completed!",   
    type: "warning",   
    showCancelButton: true,     
    confirmButtonText: "Yes, payment complete!",   
    closeOnConfirm: true },
  function(){   
    var ajax_call = $.ajax({url:'<?php echo(site_url("admin/dashboard/order_paid")); ?>', method: "post", data:{orderID: id}});
    ajax_call.done( function(data){
      $('#row'+id).removeClass('warning').addClass('success');
    });
  }); 
}

</script>

<div class="container">
      <div class="row" style=" margin-top: 20px;">        

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('kickstarter'); ?>">Kickstarter</a> &gt;
            Thank you!
        </p>  

          <h2>Thank you!</h2>  

          <img src="<?php echo(base_url('assets/images/little_guy_karate.jpg')); ?>" style='float:left; width: 120px; margin-right: 20px;' alt='Kiiiiiick Start!' title='Kiiiiiick Start!'>    
          <p>We are working hard on your reward: <a href='<?php echo site_url('store/detail/'.$order_items[0]['id']); ?>'><?php echo $order_items[0]['name']; ?></a>.<br>
          You are backer number: <?php echo $orderid; ?><br>
          backing date: <?php echo($created_at);?><br> 
          Reward price: <?php echo number_format(($order_items[0]['unit_price']/100),2,",","."); ?> &#8364;<br> 
          Shipping price: <?php echo  number_format(($shipping_cost/100),2,",","."); ?> &#8364;<br> 
          <br>
          You will be notified by email when shipment begins. This is estimated in October/November.</p>
          <p>
          In the meantime, feel free to browse our website and <a href="<?php echo site_url('documentation'); ?>">check out the documentation</a>. 
          We have added a lot of extra FAQ and a section called the Pozyx Academy explaining how positioning and ultrawideband works. 
          We will be adding more information, tutorials and academy posts on a regular basis. 
          At the end of every post we made some room for comments, so don't hesitate to join the discussion!
          </p>              
          <p>
          Here is an overview of your information:
          </p>

        </div> 
       
        <div class="col-md-6">
          <h3>Shipping Address</h3>

          <p>
          <?php 
          echo $firstname . " " . $lastname . "<br>";
          if($company_name != "") echo $company_name . "<br>";           
          echo $address_line1 . "<br>"; 
          if($address_line2 !="") echo $address_line2 . "<br>"; 
          echo $zip ." ". $city ." ". $state . "<br>"; 
          echo $countryName; 
          ?>
          </p>
        </div>

        <div class="col-md-6">
          <h3>Billing Address</h3>

          <p>
          <?php 
          echo $firstname . " " . $lastname . "<br>";           
          if($company_name != "") echo $company_name;
          if($company_vat != "") echo " - " . $company_vat . "<br>"; else echo "<br>";
          echo $billing_address_line1 . "<br>";            
          if($billing_address_line2 !="") echo $billing_address_line2 . "<br>";
          echo $billing_zip ." ". $billing_city ." ". $billing_state . "<br>";           
          echo $countryName; 
          ?>
          </p>
        </div>

        <div class="col-md-12">
            <?php
              if($order_items[0]['id'] != 1)
              {
                // don't show the power plug selection for the ready to range reward
            ?>   
            <p>You selected power plug type <?php echo $plug_type; ?></p>
            <img src="<?php echo(base_url('assets/images/products/plug_'.$plug_type.'.jpg')); ?>">
            <?php 
              } // end if (! ready to range)
            ?>
            <br><br>
            <p>You want to use Pozyx for:</p>
            <p><i>
            <?php echo ($user_remarks==""?".. it's a secret, but it's gonna be big!":$user_remarks); ?>
            </i></p>
            <br><br><br>
            <a href="<?php echo site_url('kickstarter/edit_details/'.$encodedOrderID); ?>" class="btn btn-primary" role="button">Edit these details</a>

        </div>

        
      </div>
      

</div> <!-- /container -->