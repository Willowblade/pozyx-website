<style>
.table-item td, th{
    border-bottom: 1px solid gray;
    /*border-right: 1px solid #91a3c7;*/
  }
  .table-header{
    font-weight: bolder;
    background-color: #91a3c7;
  }

  .table-summary td{
    font-weight: bolder;
  }

  .items-table{
    border: 1px solid gray;
  } 

  .items-table td, .items-table th{
    padding: 5px; 
  } 
</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>

<form role="form" id="details-form" action="" method="post">
<input type="hidden" name="order_id" value="<?php echo $orderid; ?>">
<input type="hidden" name="encoded_order_id" value="<?php echo $encodedOrderID; ?>">

<div class="container">
      <!-- Example row of columns -->
      <div class="row" style=" margin-top: 20px;">        

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('kickstarter'); ?>">Kickstarter</a> &gt;
            Edit details
        </p>  

          <h2>Thank you!</h2>  

          <img src="<?php echo(base_url('assets/images/little_guy_karate.jpg')); ?>" style='float:left; width: 120px; margin-right: 20px;' alt='Kiiiiiick Start!' title='Kiiiiiick Start!'>    
          <p>We are working hard on your reward: <a href='<?php echo site_url('store/detail/'.$order_items[0]['id']); ?>'><?php echo $order_items[0]['name']; ?></a>.<br>
          You are backer number: <?php echo $orderid; ?><br>
          backing date: <?php echo($created_at);?><br> 
          Reward price: <?php echo number_format(($order_items[0]['unit_price']/100),2,",","."); ?> &#8364;<br> 
          Shipping to <?php echo $country_code ?>: <?php echo  number_format(($shipping_cost/100),2,",","."); ?> &#8364;<br> 
          <br>
          Please enter your information in the form below so we can prepare your reward!
          </p>  
            

        </div>
      </div>

      <div class="row">  

          <div class="col-xs-12">
            <h3>Contact information</h3>  
          </div>        

        <div class="panel">
            <div class="panel-body">   

                <div class="col-xs-12 form-group">
                    <label for="IndividualOrCompany">Identify yourself as:</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="IndividualOrCompany" value="individual"  <?php if($company_name == "") echo "checked"; ?>> Individual
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="IndividualOrCompany" value="company" <?php if($company_name != "") echo "checked"; ?>> Company
                    </label>
                </div>             

                <div class="col-sm-4 form-group">
                    <label>First Name*</label>
                    <input type="text" class="form-control" name="firstName" placeholder="First Name" required value="<?php echo $firstname; ?>">
                </div>

                <div class="col-sm-4 form-group">
                    <label>Last Name*</label>
                    <input type="text"  class="form-control"  name="lastName" placeholder="Last Name" required value="<?php echo $lastname; ?>">
                </div>

                <div class="col-sm-4 form-group">
                    <label for="tel">Phone number<span class='visible-lg-inline'> (required for shipping carrier)</span>*</label>
                    <input type="tel" class="form-control" name="tel" placeholder="phone number" required  value="<?php echo $phone; ?>"/>
                </div>  

                <div class="clearfix"></div>                

                <div id='companyDetails' style='display:none'>
                  <div class="col-xs-6 form-group">
                      <div class="form-group">
                          <label>Company Name*</label>
                          <input type="text" class="form-control" name="companyName" placeholder="company name" value="<?php echo $company_name; ?>" required />
                      </div>
                  </div>

                  <div class="col-xs-6 form-group has-feedback">                      
                      <label for="vatNumber">VAT Number (EU only)</label>
                      <input type="text" class="form-control" id="vatNumber" name="vatNumber" placeholder="valid VAT number" value="<?php echo $company_vat; ?>">
                      <!--<span class="glyphicon form-control-feedback" id="vatNumber1"></span>-->   
                  </div>
                </div>

            </div>
        </div>     

        <div class="col-xs-12">
            <h3>Shipping information</h3>  
        </div>

        <div class="panel credit-card-box">
            <div class="panel-body">                

                <div class="col-xs-12 form-group">
                    <div class="form-group">
                        <label>Address line 1*</label>
                        <input type="addressLine1" class="form-control" name="addressLine1" placeholder="Address line 1" autocomplete="address"  value="<?php echo $address_line1; ?>">
                    </div>
                </div>

                <div class="col-xs-12 form-group">
                    <label>Address line 2</label>
                    <input type="addressLine2"  class="form-control"  name="addressLine2" placeholder="Address line 2" autocomplete="address2" value="<?php echo $address_line2; ?>">
                </div>                

                <div class="col-sm-5 col-xs-12 form-group" id="form_state">
                    <label for="state">State*</label>
                    <input type="text" class="form-control"  name="State" id="state" placeholder="State" required value="<?php echo $state; ?>">
                </div>  

                <div class="col-sm-5 col-xs-6 form-group">
                    <label for="City">City*</label>
                    <input type="text"  class="form-control"  name="City" placeholder="City" autocomplete="City" value="<?php echo $city; ?>">
                </div>

                <div class="col-sm-2 col-xs-6 form-group">
                    <label for="ZIP"><span class='hidden-lg'>ZIP code*</span><span class='visible-lg-inline'><span id="PostalOrZip">Postal</span> Code*</span></label>
                    <input type="text"  class="form-control"  name="ZIP" placeholder="ZIP" autocomplete="ZIP" value="<?php echo $zip; ?>">
                </div>  
            </div>
        </div>     

        <div class="col-xs-12">
            <h3>Billing information</h3>  
        </div>

        <div class="panel">
            <div class="panel-body">

                <div class="col-xs-12">
                  <div class="checkbox">
                      <label>
                          <input type="checkbox" name="billingAddressIsShipping" id="billingAddressIsShipping" value="1" <?php if($billing_equals_shipping==1){echo('checked');} ?>> <p>Billing address is the same as shipping address</p>
                      </label>
                  </div>                    
                </div>                

                <div id="billing_address_div" style="display:none">
                  <div class="col-xs-12 form-group">
                      <label for="billing_addressLine1">Address line 1*</label>
                      <input type="text" class="form-control" name="billing_addressLine1" placeholder="Address line 1" required value="<?php echo $billing_address_line1; ?>">
                  </div>

                  <div class="col-xs-12 form-group">
                      <label for="billing_addressLine2">Address line 2</label>
                      <input type="text"  class="form-control"  name="billing_addressLine2" placeholder="Address line 2" value="<?php echo $billing_address_line2; ?>"/>
                  </div>                

                  <div class="col-sm-5 col-xs-12 form-group" id="billing_form_state">
                      <label for="billing_state">State*</label>
                      <input type="text" class="form-control"  name="billing_State" id="billing_state" placeholder="State" required value="<?php echo $billing_state; ?>"/>
                  </div>  

                  <div class="col-sm-5 col-xs-6 form-group">
                      <label for="billing_City">City*</label>
                      <input type="text"  class="form-control"  name="billing_City" placeholder="City" required value="<?php echo $billing_city; ?>">
                  </div>

                  <div class="col-sm-2 col-xs-6 form-group">
                      <label for="billing_ZIP"><span class='hidden-lg'>ZIP code*</span><span class='visible-lg-inline'><span id="PostalOrZip">Postal</span> Code*</span></label>
                      <input type="text"  class="form-control"  name="billing_ZIP" placeholder="ZIP" required value="<?php echo $billing_zip; ?>">
                  </div>   
                </div>
            </div>  

        </div> 

        <?php
          if($order_items[0]['id'] != 1)
          {
            // don't show the power plug selection for the ready to range reward
        ?>          

        <div class="col-md-12">
          <h3>Order details</h3>
        </div>

        <div class="panel">
            <div class="panel-body">

            <div class="col-md-12"> 
              <label>Please select the type of power plug</label>
              </div>          
              <div class='col-md-2'><div class="radio">         
                <img src="<?php echo(base_url('assets/images/products/plug_A.jpg')); ?>"><br>
                <label>
                  <input type="radio" name="plugtype" value="A" <?php if($plug_type=='A'){echo('checked');} ?>> type A (US, Canada, Japan, ...) 
                </label>
              </div></div>
              <div class='col-md-2'><div class="radio">
                <img src="<?php echo(base_url('assets/images/products/plug_C.jpg')); ?>"><br>
                <label>
                  <input type="radio" name="plugtype" value="C" <?php if($plug_type=='C'){echo('checked');} ?>> type C (EU, Russia, Africa, ...) 
                </label>
              </div></div>
              <div class='col-md-2'><div class="radio">
                <img src="<?php echo(base_url('assets/images/products/plug_D.jpg')); ?>"><br>
                <label>
                  <input type="radio" name="plugtype" value="D" <?php if($plug_type=='D'){echo('checked');} ?>> type D (India)
                </label>
              </div></div>
              <div class='col-md-2'><div class="radio">
                <img src="<?php echo(base_url('assets/images/products/plug_G.jpg')); ?>"><br>
                <label>
                  <input type="radio" name="plugtype" value="G" <?php if($plug_type=='G'){echo('checked');} ?>> type G (UK)
                </label>
              </div></div>
              <div class='col-md-2'><div class="radio">
                <img src="<?php echo(base_url('assets/images/products/plug_I.jpg')); ?>"><br>
                <label>
                  <input type="radio" name="plugtype" value="I" <?php if($plug_type=='I'){echo('checked');} ?>> type I (China, Australia, ...)
                </label>
              </div>
            </div>

            <div class="col-md-12 form-group">
              <br><br>
              <label for="comment">What will you use Pozyx for?</label>
              <textarea class="form-control" rows="4" name="whatFor"><?php echo $user_remarks; ?></textarea>
              <p class='text-muted'>We will analyse this information and try to make tutorials that would help the most people with their projects.</p>
            </div>

          </div>
        </div> 

        <?php 
          } // end if (! ready to range)
        ?>

        
      </div>

      <div class="row">
          <div class="col-xs-4">
            <br><br>
              <button class="btn btn-success btn-lg btn-block" type="submit">Confirm</button>
          </div>
        </div>
      
      <div class="row" style="display:none;">
          <div class="col-xs-12">
              <p class="payment-errors"></p>
          </div>
      </div>

      

</div> <!-- /container -->
</form>


<script>
    
  var $form = $('#details-form');
  var validator;  

  $( document ).ready(function() {

    jQuery.validator.addMethod('intlphone', function(value) { 
        intRegex = /[0-9 -()+]+$/;
        return ((value.length > 6) && (intRegex.test(value)))
    }, 'Please enter a valid phone number');

    jQuery.validator.addMethod("vatNumber", function(value, element) {
        vat_regex = /^((AT)?U[0-9]{8}|(BE)?0[0-9]{9}|(BG)?[0-9]{9,10}|(CY)?[0-9]{8}L|(CZ)?[0-9]{8,10}|(DE)?[0-9]{9}|(DK)?[0-9]{8}|(EE)?[0-9]{9}|(EL|GR)?[0-9]{9}|(ES)?[0-9A-Z][0-9]{7}[0-9A-Z]|(FI)?[0-9]{8}|(FR)?[0-9A-Z]{2}[0-9]{9}|(GB)?([0-9]{9}([0-9]{3})?|[A-Z]{2}[0-9]{3})|(HU)?[0-9]{8}|(IE)?[0-9]S[0-9]{5}L|(IT)?[0-9]{11}|(LT)?([0-9]{9}|[0-9]{12})|(LU)?[0-9]{8}|(LV)?[0-9]{11}|(MT)?[0-9]{8}|(NL)?[0-9]{9}B[0-9]{2}|(PL)?[0-9]{10}|(PT)?[0-9]{9}|(RO)?[0-9]{2,10}|(SE)?[0-9]{12}|(SI)?[0-9]{8}|(SK)?[0-9]{10})$/;
        return this.optional(element) || vat_regex.test(value);
    }, "Please specify a correct VAT number");  



    validator = $form.validate({
        focusInvalid : false,   /* dont focus on the invalid fields, this is a problem for mobile! */
        rules: {            
            tel: {
              required: true,
              intlphone: true
            },
            firstName: { required: true },
            lastName: { required: true },
            addressLine1: { required: true },
            City: { required: true },
            ZIP: { required: true },
             vatNumber: { vatNumber: true }
        },
        highlight: function(element) {
          var id_attr = "#" + $( element ).attr("id") + "1";
          $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
          $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
        },
        unhighlight: function(element) {
            var id_attr = "#" + $( element ).attr("id") + "1";
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
        },
        errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.length) {
                    error.insertAfter(element);
                } else {
                error.insertAfter(element);
                }
            } 
     });

    // trigger the form validation automatically
    //validator.form();
    validator.resetForm();

    updateCountry();
    if($("#billingAddressIsShipping").prop('checked')){
      $("#billing_address_div").hide();
    }else{
      $("#billing_address_div").show();
    }

    // individual or company
    checkedValue = $('input[name=IndividualOrCompany]:checked').val();
    if(checkedValue == "individual"){
      $('#companyDetails').hide();
    }else{
      $('#companyDetails').show();
    }


    $form.on('submit', function(e){
      
      if(!paymentFormReady()){
        e.preventDefault();
        sweetAlert('Almost ready...', 'Please fill in all mandatory fields.', 'warning');      
      }
    });  

    $("#billingAddressIsShipping").change(function(){
            
      if($("#billingAddressIsShipping").prop('checked')){
        $("#billing_address_div").hide();
      }else{
        $("#billing_address_div").show();
      }
    });

    // individual or company
    $("input[name=IndividualOrCompany]:radio").change(function () {
      checkedValue = $('input[name=IndividualOrCompany]:checked').val();
      
      if(checkedValue == "individual"){
        $('#companyDetails').hide();
      }else{
        $('#companyDetails').show();
      }
    });

    validator.resetForm();

  });

  function updateCountry(){      
      // source: http://webmasters.stackexchange.com/questions/3206/what-countries-require-a-state-province-in-the-mailing-address
      var state_required = ["US","CA","AU","CN","MX","MY","IT"];
      if($.inArray("<?php echo $country_code ?>", state_required) > -1){
          $("#form_state").show();
          $("#state").prop('required', true);
          $("#billing_form_state").show();
          $("#billing_state").prop('required', true);
      }else{          
          $("#form_state").hide();
          $("#state").prop('required', false);
          $("#billing_form_state").hide();
          $("#billing_state").prop('required', false);
      }
  }    
  
  // check to test the form for valid input
  function paymentFormReady() {      
     
      var contact_ok = valid_formfield('tel') && valid_formfield('firstName') && valid_formfield('lastName') && valid_formfield('vatNumber');;
      var shipping_ok = valid_formfield('addressLine1') && valid_formfield('country') && valid_formfield('City') && valid_formfield('ZIP');
      var billing_ok  = valid_formfield('billing_addressLine1') && valid_formfield('billing_City') && valid_formfield('billing_ZIP');

      if($("#state").prop('required')){
        shipping_ok = shipping_ok && valid_formfield('State');
        billing_ok = billing_ok && valid_formfield('billing_State');
      }

      if($("#billingAddressIsShipping").prop('checked')){        
        billing_ok = true;
      }
      
      if ( contact_ok && shipping_ok && billing_ok){
          return true;
      } else {
          return false;
      }      
  }

  function valid_formfield(form_field) {
      return $('[name='+form_field+']').valid();//.parent('.form-group').hasClass('has-success');
  }


</script>

