<style>
  .news-date{
    color: gray;
    font-size: 11px;
  }

  h1
{
  margin: 0;
  font-weight: normal;
}

a.anchor {
    display: block;
    position: relative;
    top: -50px;
    visibility: hidden;
}

time.icon
{
  font-size: 1em; /* change icon size */
  display: inline-block;
  float: left;
  position: relative;
  width: 7em;
  height: 7em;
  background-color: #fff;
  margin: 2em auto;
  margin-right: 20px;
  border-radius: 0.6em;
  box-shadow: 0 1px 0 #bdbdbd, 0 2px 0 #fff, 0 3px 0 #bdbdbd, 0 4px 0 #fff, 0 5px 0 #bdbdbd, 0 0 0 1px #bdbdbd;
  overflow: hidden;
  -webkit-backface-visibility: hidden;
  -webkit-transform: rotate(0deg) skewY(0deg);
  -webkit-transform-origin: 50% 10%;
  transform-origin: 50% 10%;
}

time.icon *
{
  display: block;
  width: 100%;
  font-size: 1em;
  font-weight: bold;
  font-style: normal;
  text-align: center;
}

time.icon strong
{
  position: absolute;
  top: 0;
  padding: 0.4em 0;
  color: #fff;
  background-color: #5F945F;
  border-bottom: 1px dashed #2d452d;
  box-shadow: 0 2px 0 #5F945F;
}

time.icon em
{
  position: absolute;
  bottom: 0.3em;
  color: #5F945F;
}

time.icon span
{
  width: 100%;
  font-size: 2.8em;
  letter-spacing: -0.05em;
  padding-top: 0.8em;
  color: #2f2f2f;
}
</style>

<div class="container">
      <!-- Example row of columns -->
      <div class="row" style=" margin-top: 20px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('/Welcome'); ?>">News</a> &gt;
            firmware update v1.0
        </p>
        <h1>News overview</h1>
        <br><br>
        </div>

<!-- Team expanded -->

        <div class="col-md-1">
          <time datetime="2014-09-20" class="icon">
            <em>2016</em>
            <strong>Nov</strong>
            <span>2</span>
          </time>

          <!-- fix for small device sizes -->
          <h2 class='visible-sm-inline' style='float: left; margin-top:70px; height: 55px'>Ghent University spinoff and expanding the team</h2>
          <div class='visible-sm-inline visible-xs-inline clearfix'></div>

        </div>
        <div class="col-md-11" style='padding-left:50px'>
            <div class="row">

            <div class="col-md-12">

            <a class="anchor" name="nov2016_spinoff"></a><h2 class='hidden-sm' >Ghent University spinoff and expanding the team</h2>

            <p>
            We are proud to announce that Pozyx is now an official spin-off of <a href='http://www.ugent.be/en'>Ghent University</a>. 
            The spin-off label means that Ghent University has transferred the IP rights of the relevant research on indoor localization from the past years to Pozyx. In short-term, this will allow us to incorporate the IP and further improve the Pozyx system. The spin-off label also strenthens our long-term strategic relation with the University related to R&amp;D for the years to come. </p>

            <img src="<?php echo(base_url('assets/images/news/logo_UGent.png')); ?>" style="margin:auto" alt='Ghent university' title='Ghent University'>

            <p>
            To top things off, we have further expanded the team with three new employees, Michiel, Jan-Frederik and (yet another) Michiel. They will be working on improving the current system and the further development of a large-scale positioning solution.
            </p>
            <p>
            Welcome to the team boys!
            </p>

            <br><br>
            
             </div>
          </div>
        </div>

<!-- Firmware version update -->

        <div class="col-md-1">
          <time datetime="2014-09-20" class="icon">
            <em>2016</em>
            <strong>August</strong>
            <span>22</span>
          </time>
          <!-- fix for small device sizes -->
          <h2 class='visible-sm-inline' style='float: left; margin-top:70px; height: 55px'>Firmware update v1.0</h2>
          <div class='visible-sm-inline visible-xs-inline clearfix'></div>


        </div>
        <div class="col-md-11" style='padding-left: 50px'>
          <div class="row">
          <div class="col-md-12">
          <a class="anchor" name="august2016_firmware"></a><h2 class='hidden-sm'>Firmware update v1.0</h2>

          <h3>Overview</h3>
          <br>
          <p>
            Today, we have released the new firmware update v1.0. It is the first major update since the release of the pozyx product and it contains a
            large number of new features and fixes. The firmware files will be emailed to all our customers, all pozyx sets available on the store will have the
            new firmware installed by default. The Arduino library has also been updated and new sketches have been added to showcase the new features.
            An overview of all the new firmware features is listed below:
          </p>

          <ul>
          <li>Update rate up to 100Hz depending on the UWB settings.</li>
          <li>USB interface (virtual COM port).</li>
          <li>Ability to save the configuration to non-volatile memory.</li>
          <li>Revised interrupt configuration to allow compatibility with more Arduino models such as the Arduino Yùn.</li>
          <li>Added message retransmission protocol to increase UWB reliability.</li>
          <li>Improved overall stability.</li>
          <li>Various bug fixes:
          <ul>
            <li>UWB preamble length 64 available.</li>
            <li>UWB channel 7 available.</li>
            <li>Arduino does not reset together with the Pozyx reset.</li>
            <li>Temperature register now reads chip temperature.</li>
            <li>More stable polling.</li>
            <li>New register addresses for the GPIO configuration</li>
            <li>...</li>
          </ul>
          </li>

          </ul>

          <p>
            The next firmware update will be more focused towards the accuracy and robustness of ranging, positioning and auto-calibration.
            This will include some new algorithms and fine-tuning of the current algorithms.
            This next firmware release is scheduled in 2-3 months. More details will be released later.
          </p>

          <h3>Variable ranging and positioning update rate</h3>
          <p>In the previous firmware version, the update rate for ranging and positioning was fixed at about 66Hz and 15Hz, respectively.
          This was mainly because of the strict timing requirements of the ranging protocol. In this new version the update rate will
          depend on the ultra-wideband settings. More specifically, the preamble length and the bitrate. In general, a shorter preamble length and
          a higher bitrate will result in a higher update rate. However, this also results in a reduced operating range.
          </p>
          <p>
          In the Fig.&#8202;1, the average time it takes for ranging is shown for the different UWB settings. 
          The timing is measured starting before the ranging command is initiated by an Arduino Uno and stopping when Pozyx triggers the interrupt for success.
          It can be seen that the minimum time for ranging is about 3 milliseconds, allowing for a maximum update rate of 330 Hz when continuously ranging. However, if we take into account the additional time it takes for reading out the ranging data and printing it to the serial monitor, we obtain a <i><u>maximum ranging update rate of 100Hz</u></i>. This highest update rate is available when using a preamble length of 64 and a bitrate of 6.81Mbit/sec.
          </p>
          <br>

        </div>
        <div class="col-md-6">
          <img src="<?php echo(base_url('assets/images/news/ranging_speed_64MHz.png')); ?>" style="margin:auto" alt='ranging update rate Pozyx' title='ranging update rate Pozyx' width='100%'>
          <p style='text-align: center'><b>Fig 1. Ranging duration on the Pozyx device.</b></p><br>
        </div>
        <div class="col-md-6">
          <img src="<?php echo(base_url('assets/images/news/positioning_speed_64MHz.png')); ?>" style="margin:auto" alt='ranging update rate Pozyx' title='ranging update rate Pozyx' width='100%'>
          <p style='text-align: center'><b>Fig 2. Positioning duration on the Pozyx device.</b></p><br>
        </div>
        <div class="col-md-12">


          <p>
          In Fig.&#8202;2, the average time for positioning with 4 anchors using the Least Squares algorithm is shown for the different UWB settings. 
          The timing is measured starting before the positioning command is initiated by an Arduino Uno and stopping when Pozyx triggers the interrupt for success.
          It can be seen that the minimum time for positioning is about 10 milliseconds, allowing for a <i><u>maximum positioning update rate of 100 Hz</u></i>. This highest update rate is again available when using a preamble
          length of 64 and a bitrate of 6.81Mbit/sec. Note that for positioning the I²C delays can be circumvented by setting the Pozyx device for continuous positioning. This way, the data can be read over I²C in the background while the device is performing positioning.
          Note that using the UWB_ONLY algorithm adds roughly 2 milliseconds to positioning.
          </p>

          <h3>Save configuration to non-volatile memory</h3>
          <p>
          In this new firmware version, it becomes possible to save the content of any writable register to non-volatile memory. This way, the register
          content will be loaded after resetting the device (even if the device is without power). Similarly the content of the device list and
          the anchors can be saved. This new feature allows to set up the Pozyx device in a fixed configuration and to save anchor coordinates such
          that this must not be given at startup.
          </p>
          <p>The following new library functions are available for this use:</p>
          <ul>
          <li><a href="<?php echo site_url('Documentation/Datasheet/arduino#saveConfiguration'); ?>">saveConfiguration()</a></li>
          <li><a href="<?php echo site_url('Documentation/Datasheet/arduino#clearConfiguration'); ?>">clearConfiguration()</a></li>
          <li><a href="<?php echo site_url('Documentation/Datasheet/arduino#isRegisterSaved'); ?>">isRegisterSaved()</a></li>
          <li><a href="<?php echo site_url('Documentation/Datasheet/arduino#getNumRegistersSaved'); ?>">getNumRegistersSaved()</a></li>
          </ul>

          <h3>USB interface</h3>
          <p>
          With the new firmware, it becomes possible to communicate with the Pozyx device without an Arduino.
          We have implemented a simple to use protocol over USB that uses the virtual COM port. It must be noted that the interrupt line
          is not connected through the USB protocol, so only polling is available. Hence, for the best performance, I²C is still recommended.
          <a href="<?php echo site_url('Documentation/Datasheet/Interfaces#usb'); ?>">More details on how to use the USB protocol can be found here</a>.
          </p>
          <p>
          Furthermore, we will be releasing a Python library that uses the USB protocol that will allow you to easily use the Pozyx device from a computer, robot or Raspberry Pi.</p>


          <h3>Revised interrupt configuration</h3>
          <p>
          In the previous firmware version, it was possible to choose between two interrupt pins, with pin 2 activated by default on the Pozyx
          device. Unfortunately, the different Arduino models often have different pin configurations, making it incompatible with the Pozyx shield.
          For example, on the Arduino Yun, pin 2 is internally connected with the I²C pin that is used for communication. Because of this, the
          pozyx interrupt blocked the I²C and communication with the pozyx device was not possible.
          </p>
          <p>
          In this firmware update we have changed the interrupt configuration, turning it down by default and allowing the user to select between
          6 different pins on the Pozyx shield to act as an interrupt. Furthermore, the interrupts can now be configured as active low or active high
          with or without latching.
          </p>
          <p>This functionality has been added to the Pozyx.begin() function of the Arduino library and is also made available
          through the function <a href="<?php echo site_url('Documentation/Datasheet/arduino#configInterruptPin'); ?>">configInterruptPin()</a>.</p>
          </ul>
      </div>
      </div>
      </div>

<!-- team expanded -->

      <div class="col-md-1">
          <time datetime="2014-09-20" class="icon">
            <em>2016</em>
            <strong>July</strong>
            <span>15</span>
          </time>

          <!-- fix for small device sizes -->
          <h2 class='visible-sm-inline' style='float: left; margin-top:70px; height: 55px'>Pozyx expands the team</h2>
          <div class='visible-sm-inline visible-xs-inline clearfix'></div>

      </div>
      <div class="col-md-11" style='padding-left:50px'>
          <div class="row">

          <div class="col-md-12">

          <a class="anchor" name="july2016_team"></a><h2 class='hidden-sm' >Pozyx expands the team</h2>

          <p>
          We're proud to welcome Ruben and Laurent to the Pozyx team.
          They have already started a few weeks ago, but now they are all up to speed and ready to help people with their questions about Pozyx.
          Apart from support, they will create exciting new prototype applications for pozyx and help with the upcoming Python and ROS library.
          </p>
          <p>
          Welcome to the team boys!
          </p>
          <img src="<?php echo(base_url('assets/images/news/new_teammembers.jpg')); ?>" style="margin:auto" alt='New teammembers' title='new teammembers'>

            <br><br><br>
           </div>
        </div>
      </div>

<!-- pilot in retail -->      

      <div class="col-md-1">
          <time datetime="2014-09-20" class="icon">
            <em>2016</em>
            <strong>April</strong>
            <span>1</span>
          </time>

          <!-- fix for small device sizes -->
          <h2 class='visible-sm-inline' style='float: left; margin-top:40px; height: 55px'>Pozyx Labs starts pilot<br>project in retail</h2>
          <div class='visible-sm-inline visible-xs-inline clearfix'></div>

        </div>
        <div class="col-md-11" style='padding-left: 50px'>
          <div class="row">

          <div class="col-md-12">

          <a class="anchor" name="april2016_pilotproject"></a><h2 class='hidden-sm'>Pozyx Labs starts pilot project in retail</h2>
          <p>
          On April 1st, the LUNAR project <i>(Localization based on Ultra wide band technology: optimizing Network protocols, Antennas and localization Algorithms for the Retail industry)</i> has started with the aim to bring accurate indoor positioning to retail. 
          The project is a collaboration of several research and industry partners and is supported by the Belgian government. The partners of the project are Ghent University, Colruyt, Decathlon, Made and Pozyx Labs.
          In this project, Pozyx Labs is the industrial lead and is focused on providing real-time multi-target positioning information in a large area (1800sqm) filled with metal racks and items. The project includes an in-store pilot to evaluate the system's performance in a live environment.</p>
          
          <p>
          More information can be found on the <a href='https://www.iminds.be/en/projects/LUNAR'>official project webpage</a>.
          The project is partially funded by IMEC and Vlaio.<br><br><br>
          </p>

          </div>
          </div>
        </div>

<!-- All kickstarter orders shipped -->

      <div class="col-md-1">
          <time datetime="2014-09-20" class="icon">
            <em>2016</em>
            <strong>March</strong>
            <span>1</span>
          </time>

          <!-- fix for small device sizes -->
          <h2 class='visible-sm-inline' style='float: left; margin-top:70px; height: 55px'>All Kickstarter orders shipped</h2>
          <div class='visible-sm-inline visible-xs-inline clearfix'></div>

      </div>
      <div class="col-md-11" style='padding-left: 50px'>
          <div class="row">

          <div class="col-md-12">

          <a class="anchor" name="march2016_kickstarter"></a><h2 class='hidden-sm'>All Kickstarter orders shipped</h2>
          <p>
          We are proud to announce that all orders from Kickstarter have been launched.
          They are the first people to have the first Pozyx system in their hands. The system contains the first public firmware version v0.9.
          </p>

          <p>
          <i>Some kickstarter backers still haven't provided us with their shipping details, so we haven't been able to send those few people
          their reward. If you are one of these people, please email us at support@pozyx.io</i>
          </p>

          </div>
          </div>
      </div>

      <div class="col-md-12">

          <br><br><br>
          <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">News</a> &gt;
            firmware update v1.0
          </p>

        </div>
      </div>

</div> <!-- /container -->
