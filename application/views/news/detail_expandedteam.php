<div class="container">
      <!-- Example row of columns -->
      <div class="row" style=" margin-top: 20px;">        

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">News</a> &gt;
            firmware update v1.0
        </p>  
          <h2>Firmware update v1.0</h2>  
          <p> June 29th 2016</p>
          <h3>Overview</h3>
          <br>
          <p>            
            Today, we have released the new firmware update v1.0. It is the first major update since the release of the pozyx product and it contains a
            large number of new features and fixes. The firmware files have been sent to all our customers, all pozyx sets available on the store will have the
            new firmware installed by default. An overview of all the new firmware features is listed below:
          </p>

          <ul>
          <li>Variable update rate for ranging and positioning depending on the UWB settings.</li>
          <li>USB protocol (virtual COM port).</li>
          <li>Ability to save the configuration to non-volatile memory.</li>          
          <li>Revised interrupt configuration to allow compatibility with more Arduino models such as the Arduino Yùn.</li>
          <li>Added message retransmission protocol to increase UWB reliability.</li>
          <li>Improved overall stability.</li>
          <li>Various bug fixes:
          <ul>
            <li>UWB preamble length 64 available.</li>
            <li>UWB channel 7 available.</li>
            <li>Arduino does not reset together with the Pozyx reset.</li>
            <li>Temperature register now reads chip temperature.</li>
            <li>More stable polling.</li>
            <li>...</li>
          </ul>
          </li>

          </ul>

          <p>
            The next firmware update will be more focused towards the accuracy and robustness of ranging, positioning and auto-calibration. 
            This will include some new algorithms and fine-tuning of the current algorithms. 
            This next firmware release is scheduled in 2-3 months. More details will be released later.
          </p>

          <h3>Variable ranging and positioning update rate</h3>
          <p>In the previous firmware version, the update range for ranging and positioning was fixed at 66Hz and 15Hz, respectively.
          This was mainly because of the strict timing requirements of the ranging protocol. In this new version the update rate will 
          depend on the ultra-wideband settings. More specifically, the preamble length and the bitrate. In general, a shorter preamble length and 
          a higher bitrate will result in a higher update rate. However, this also results in a reduced operating range.
          </p>
          <p>
          In the Fig.&#8202;1, the time it takes for ranging is shown for the different UWB settings. It can be seen that the minimum time for ranging
          is about 3.5 milliseconds, allowing for a maximum update rate of 280 Hz when continuously ranging. However, taking into account the i2c communication to the Pozyx
          device to initiate ranging and fetch the range data, results in a <i><u>maximum ranging update rate of 100Hz</u></i>. The highest update rate is available when using a preamble
          length of 64 and a bitrate of 8.5Mbit/sec.
          </p>
          <br>

        </div>
        <div class="col-md-6">
          <img src="<?php echo(base_url('assets/images/news/ranging_speed_64MHz.png')); ?>" style="margin:auto" alt='ranging update rate Pozyx' title='ranging update rate Pozyx'>  
          <p style='text-align: center'><b>Fig 1. Ranging duration on the Pozyx device.</b></p><br>
        </div>
        <div class="col-md-6">
          <img src="<?php echo(base_url('assets/images/news/positioning_speed_64MHz.png')); ?>" style="margin:auto" alt='ranging update rate Pozyx' title='ranging update rate Pozyx'>  
          <p style='text-align: center'><b>Fig 2. Positioning duration on the Pozyx device.</b></p><br>
        </div>
        <div class="col-md-12">


          <p>
          In Fig.&#8202;2, the time it takes for positioning is shown for the different settings. It can be seen that the minimum time for positioning
          is about 20 milliseconds, allowing for a <i><u>maximum positioning update rate of 50 Hz</u></i>. This highest update rate is again available when using a preamble
          length of 64 and a bitrate of 8.5Mbit/sec. Note that for positioning the i2c delays can be circumvented by setting the Pozyx device to automatic 
          positioning. This way, the data can be read over i2c in the background without delaying the positioning.
          </p>

          <h3>Save configuration to non-volatile memory</h3>
          <p>
          In this new firmware version, it becomes possible to save the content of any writable register to non-volatile memory. This way, the register
          content will be loaded after resetting the device (even if the device is without power). Similarly the content of the device list and 
          the anchors can be saved. This new feature allows to set up the Pozyx device in a fixed configuration and to save anchor coordinates such
          that this must not be given at startup.
          </p>
          <p>The following new library functions are available for this use:</p>
          <ul>
          <li>saveConfiguration()</li>
          <li>clearConfiguration()</li>
          <li>isRegisterSaved()</li>
          <li>getNumRegistersSaved()</li>
          </ul>

          <h3>USB protocol</h3>
          <p>
          In this new firmware version, it becomes possible to communicate with the Pozyx device without an Arduino. 
          We have implemented a simple to use protocol over USB that used the virtual COM port. It must be noted that the interrupt line
          is not connected with through USB so only polling is available. Hence, for the best performance, I2C still recommended.          
          <a href="<?php echo site_url('Documentation/Datasheet/i2cProtocol#usb'); ?>">More details on how to use the USB protocol can be found here</a>.
          </p>
          <p>
          Furthermore, we will be releasing a Python library that uses the USB protocol that allows you to easily use the Pozyx device
          from a computer, robot or raspberry pi.</p>
          

          <h3>Revised interrupt configuration</h3>
          <p>
          In the previous firmware version, it was possible to choose between two interrupt pins, with pin 2 activated by default on the Pozyx
          device. Unfortunately, the different Arduino models often have different pin configurations, making it incompatible with the Pozyx shield. 
          For example, on the Arduino Yun, pin 2 is internally connected with the i2c pin that is used for communication. Because of this, the
          pozyx interrupt blocked the i2c and communication with the pozyx device was not possible.
          </p>
          <p>
          In this firmware update we have changed the interrupt configuration, turning it down by default and allowing the user to select between
          6 different pins on the Pozyx shield to act as an interrupt. Furthermore, the interrupts can now be configured as active low or active high
          with or without latching.
          </p>
          <p>This functionality has been added to the Pozyx.begin() function of the Arduino library and is also made available 
          through the function configInterruptPin().</p>
          </ul>


          <br><br><br>
          <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">News</a> &gt;
            firmware update v1.0
          </p>  

        </div>
      </div>     

</div> <!-- /container -->

 