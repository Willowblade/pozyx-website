<style>
.portrait{
  height: 120px;
  margin-bottom: 10px;
  padding-right:7px;
  padding-left:7px;
}

.golden img{
  border: 2px solid gold;
}

#center_movie{

}

.founder{
  margin-right: 20px; 
  cursor: pointer;
}
</style>


<div class="container">
      <!-- Example row of columns -->

      <div class="row" style="padding-bottom: 30px; padding-top: 30px;">
        
        <div class="col-md-12">
          <h2>About</h2>  

          <h3> Pozyx Labs</h3>
          <p>
            Pozyx labs is a company specialized in indoor positioning founded in August 2015 in Ghent, Belgium. 
            We provide indoor and outdoor hardware solutions for accurate localization.
          </p>

        </div>
      </div>

      <div class="row">    
          
          <div class="col-md-2 col-sm-4" style='text-align: center'>
            
              <img src='<?php echo base_url("assets/images/hire.jpg"); ?>' style='width:100%; max-width:200px; margin-top: 20px'>
          </div>
          <div class="col-md-10 col-sm-8">
            <h3>We are hiring!</h3>
              <p>If you are interested in working for a technical startup in full expansion, do not hesitate to take a look at our job openings. <br>
                We are both hiring the more experienced guru that can help us grow, or the graduate looking for his or her first job.
                <br><br>
              </p>
              <p><a class="btn btn-primary btn-lg" href="<?php echo site_url('welcome/jobs'); ?>" role="button">Jobs</a></p>
          </div>
        
      </div>

</div> <!-- /container -->

 <div class="container">
      <!-- Example row of columns -->
      <div class="row" style="margin-top: 40px;">        

        <div class="col-md-12">
          <h3>The team</h3>    
        </div>
        
        <div class="col-md-3 col-sm-3 col-xs-6" style='height: 350px'>
          <img src='<?php echo(base_url('assets/images/samuel.jpg'));?>' class="profilepic">
          <div style='font-size: 18px'><b>Samuel Van de Velde</b></div>
          <p>
            He is an electrical engineer doing Ph.D. research on the topic of localization. 
            <a href="https://www.linkedin.com/in/samuelvandevelde" title="view Samuel's profile on LinkedIn"><img src="<?php echo(base_url('assets/images/linkedin-icon-small.jpg'));?>" alt="LinkedIn profile Samuel Van de Velde" border="0"></a>
          </p>    
       </div>
        <div class="col-md-3 col-sm-3 col-xs-6" style='height: 350px'>
          <img src='<?php echo(base_url('assets/images/vadim.jpg'));?>' class="profilepic">
          <div style='font-size: 18px'><b>Vadim Vermeiren</b></div>
          <p>He is an engineer with a background and interest in entrepreneurship.
			<a href="https://be.linkedin.com/pub/vadim-vermeiren/41/21a/a02" title="view Vadim's profile on LinkedIn"><img src="<?php echo(base_url('assets/images/linkedin-icon-small.jpg'));?>" alt="LinkedIn profile Vadim Vermeiren" border="0"></a>
          </p>
        </div>

        <div class='visible-xs clearfix'></div>  

        <div class="col-md-3 col-sm-3 col-xs-6" style="overflow: hidden" style='height: 350px'>
          <img src='<?php echo(base_url('assets/images/koen.jpg'));?>' class="profilepic">
          <div style='font-size: 18px'><b>Koen Verheyen</b></div>
          <p>He is an electrical engineer doing Ph.D. research on advanced hardware.
			<a href="https://be.linkedin.com/in/koenverheyen1987" title="view Koen's profile on LinkedIn"><img src="<?php echo(base_url('assets/images/linkedin-icon-small.jpg'));?>" alt="LinkedIn profile Koen Verheyen" border="0"></a>
          </p>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6" style='height: 350px'>
          <img src='<?php echo(base_url('assets/images/michael.jpg'));?>' class="profilepic">
          <div style='font-size: 18px'><b>Michael Van de Velde</b></div>
          <p>
            He is an economist with a passion in sales. 
            <a href="https://ie.linkedin.com/in/vandeveldemichael" title="view Michael's profile on LinkedIn"><img src="<?php echo(base_url('assets/images/linkedin-icon-small.jpg'));?>" alt="LinkedIn profile Michael Van de Velde" border="0"></a>
          </p>    
       </div>

       <div class='visible-xs clearfix'></div>  

       <div class="col-md-3 col-sm-3 col-xs-6"  style='height: 350px'>
          <img src='<?php echo(base_url('assets/images/ruben.jpg'));?>' class="profilepic">
          <div style='font-size: 18px'><b>Ruben Speybrouck</b></div>
          <p>
            Support and prototype engineer            
          </p>    
       </div>

       <div class="col-md-3 col-sm-3 col-xs-6" style='height: 350px'>
          <img src='<?php echo(base_url('assets/images/laurent.jpg'));?>' class="profilepic">
          <div style='font-size: 18px'><b>Laurent Van Acker</b></div>
          <p>
            Support and prototype engineer
          </p>    
       </div>

       <div class="col-md-3 col-sm-3 col-xs-6" style='height: 350px'>
          <img src='<?php echo(base_url('assets/images/michiel.jpg'));?>' class="profilepic">
          <div style='font-size: 18px'><b>Michiel De Witte</b></div>
          <p>
            Embedded software developer
          </p>    
       </div>

       <div class="col-md-3 col-sm-3 col-xs-6" style='height: 350px'>
          <img src='<?php echo(base_url('assets/images/jan-frederik.jpg'));?>' class="profilepic">
          <div style='font-size: 18px'><b>Jan-Frederik Van Wijmeersch</b></div>
          <p>
            Electrical engineer
          </p>    
       </div>

       <div class='visible-xs clearfix'></div>  

       <div class="col-md-3 col-sm-3 col-xs-6" style='height: 350px'>
          <img src='<?php echo(base_url('assets/images/hire.jpg'));?>' class="profilepic">
          <div style='font-size: 18px'><b>R&amp;D engineer</b></div>
          <p>
            Interested? <a href="<?php echo site_url('welcome/jobs'); ?>">Learn more</a>.
          </p>    
       </div>

      </div>



</div> <!-- /container -->

<div class="container">
    
     <div class="row">   

        <div class="col-md-12">
          <h3>Kickstarter</h3>    
        </div>   
        
         <div class="col-md-2 col-sm-4" style='text-align: center'>
          <a href="https://www.kickstarter.com/projects/pozyx/pozyx-accurate-indoor-positioning-for-arduino/description" target="_new">
          <img src="<?php echo(base_url('assets/images/kickstarter_badge_funded.png')); ?>" style='width:100%; max-width:200px; margin-top: 20px'>
          </a>
        </div>

        <div class="col-md-10 col-sm-8">
        
          <h3>The campaign</h3>
          <p>
          In June 2015, we launched a kickstarter campaign to see if people were interested in our positioning solution. 
          Our campaign was staff-picked and received a lot of international attention. On June 1st, our campaign ended and <b>we reached 207% of our funding goal</b>, a total of 56.124€ from 
          164 backers. We will always be grateful to all the backers who have supported us, because of them, we were able to launch our product and start with our company.

          To relive the project you can still visit it on <a href="https://www.kickstarter.com/projects/pozyx/pozyx-accurate-indoor-positioning-for-arduino/description" target="_new">kickstarter</a>
          </p>
        </div>   
      </div> 
       

      <div class="row">
        <div class="col-md-12"  style=" margin-top: 40px;">
        <h3>Founder's club</h3>
        <p>
          To thank all the people who have supported us on kickstarter, we have made the Founder's club. 
        </p>
        </div>

        <div class="col-md-12">
          <?php             
            foreach($foundersWall as $row){
                   echo "<a onclick='FounderWallInfo(".$row->Backer_UID.")' class='founder'>"; // javascript:alert(\"".$row->Backer_Name." was backer number #".$row->Backer_Number." and pledged " .$row->Total. " to make Pozyx succeed. \")' class='founder'>";
                   echo $row->Backer_Name;
                   echo '</a>';
                   echo "<div id='founderName".$row->Backer_UID."' style='display:none'>". $row->Backer_Name ."</div> ";
                   echo "<div id='founderTotal".$row->Backer_UID."' style='display:none'>". $row->Total ."</div> ";
                   echo "<div id='founderBackerNumber".$row->Backer_UID."' style='display:none'>". $row->Backer_Number ."</div> ";
                   echo "<div id='founderhasPicture".$row->Backer_UID."' style='display:none'>". $row->hasPicture ."</div> ";
                   echo "<div id='founderwhyPozyx".$row->Backer_UID."' style='display:none'>". $row->whyPozyx ."</div> ";

                   if ($row !== end($foundersWall))
                     echo ' | ';
            } 

          ?>

          <h3>Video club</h3>
          <?php
            foreach($foundersVideo as $row){
                   echo "<a onclick='FounderVideoInfo(".$row->Backer_UID.")' class='founder'>"; // javascript:alert(\"".$row->Backer_Name." was backer number #".$row->Backer_Number." and pledged " .$row->Total. " to make Pozyx succeed. \")' class='founder'>";
                   echo $row->Backer_Name;
                   echo '</a>';
                   echo "<div id='founderName".$row->Backer_UID."' style='display:none'>". $row->Backer_Name ."</div> ";
                   echo "<div id='founderTotal".$row->Backer_UID."' style='display:none'>". $row->Total ."</div> ";
                   echo "<div id='founderBackerNumber".$row->Backer_UID."' style='display:none'>". $row->Backer_Number ."</div> ";
                   echo "<div id='founderhasPicture".$row->Backer_UID."' style='display:none'>". $row->hasPicture ."</div> ";
                   echo "<div id='founderwhyPozyx".$row->Backer_UID."' style='display:none'>". $row->whyPozyx ."</div> ";

                   if ($row !== end($foundersVideo))
                     echo ' | ';
            } 
          ?>
        </div>
        </div>


</div> <!-- /container -->

<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<script>
function FounderWallInfo(id){

  f_name = $('#founderName'+id).text();
  f_total = $('#founderTotal'+id).text();
  f_backernumber = $('#founderBackerNumber'+id).text();
  f_hasPicture = $('#founderhasPicture'+id).text();
  f_whyPozyx = $('#founderwhyPozyx'+id).text();

  if (f_hasPicture == '1'){
    f_imageURL = "<?php echo(base_url('assets/images/walloffame/picture_" +  f_backernumber + ".jpg')); ?>";
  } else {
    f_imageURL = "<?php echo(base_url('assets/images/thumbs-up.jpg')); ?>" 
  }
  

  swal({   
    title: "Sweet!",   
    html: "Thank you for backing us " + f_name + ". You were backer number #" +  f_backernumber + ".<br><br>" + f_whyPozyx,   
    imageUrl: f_imageURL 
  });
}

function FounderVideoInfo(id){

  f_name = $('#founderName'+id).text();
  f_total = $('#founderTotal'+id).text();
  f_backernumber = $('#founderBackerNumber'+id).text();
  f_hasPicture = $('#founderhasPicture'+id).text();
  f_whyPozyx = $('#founderwhyPozyx'+id).text();

  if (f_hasPicture == '1'){
    f_imageURL = "<?php echo(base_url('assets/images/walloffame/picture_" +  f_backernumber + ".jpg')); ?>";
  } else {
    f_imageURL = "<?php echo(base_url('assets/images/thumbs-up.jpg')); ?>" 
  }
  

  swal({   
    title: "Sweet!",   
    html: "Thank you for backing us " + f_name + ". You were backer number #" +  f_backernumber + ".<br><br>" + f_whyPozyx + "<br><br>Video coming soon!!",   
    imageUrl: f_imageURL 
  });

}
</script>