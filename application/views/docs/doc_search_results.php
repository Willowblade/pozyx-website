<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            Search
        </p>  

        <h2>Documentation</h2>            

        </div>  

      <div class="col-md-12">

      <h3>Search results for keywords '<?php echo($keywords); ?>':</h3>
		
		<?php
		if(empty($query)){
		?>
		<p>Sorry, there are no matching results. Please search again:</p>
		<form action='<?php echo site_url('Documentation/search');?>' method="post" class="form-inline">
		<div class="form-group has-feedback">
		  <input type="text" class="form-control" name="search" autofocus placeholder="search for keywords" required>
		  <span class="glyphicon form-control-feedback glyphicon-search"></span>    
		  
		</div>  
		<?php 
		$this->load->helper('form');    
		echo form_submit('submit', 'Search', 'class="btn btn-primary"');
		?>

		</form>

		<p>
		<br>
		1. Try more general words. 
		<br > 2. Try different words with similar meaning. 
		<br > 3. Please check your spelling.<br><br>
		</p>

		

		<?php
			}else{
				echo '<ul id="FAQ">';

				// remove words with 3 or less letters
				$keywords = trim( preg_replace("/[^a-z0-9']+([a-z0-9']{1,3}[^a-z0-9']+)*/i",  " ",  " $keywords ") );

	            foreach($query as $item){
	              echo "<li><div class='FAQ-question'>";	              
	              $question = preg_replace('/'.str_replace(' ', '|', trim($keywords)).'/i', '<b>$0</b>', $item->question);
	              echo $question;
	              echo "</div><div class='FAQ-answer'>"; 
	              $answer = preg_replace('/'.str_replace(' ', '|', trim($keywords)).'/i', '<b>$0</b>', $item->answer);	              
	              echo $answer;
	              echo '</div></li>';
	            }  

	            echo '</ul>';
	        }
          ?>          
    	</div>
	</div>
</div>

<script>
    $( document ).ready(function() {

      $(".FAQ-answer").hide();

      $("#FAQ li").on("click", function(){
        $(this).find(".FAQ-answer").toggle();
      });  
    });

</script>