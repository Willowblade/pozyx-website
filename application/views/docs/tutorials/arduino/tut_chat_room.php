<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

<style>
.bittable{
      width:700px;
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}

h3 a{
      text-decoration: none;
      color: #5F945F;
}

h3 a:hover{
      text-decoration: none;
      color: #5F945F;
}

h4{
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
      font-weight: bold;
      margin-bottom: 0px;
}

.step:hover{
      cursor: pointer;
      text-decoration: underline;
}

.txrx-ol {
    display: block;
    padding-left: 25px;
    counter-reset:li; /* Initiate a counter */
}
.txrx-ol li {
      height: 24px;
      overflow-y: hidden;
      position: relative;
      padding-left: 40px;
    list-style:none; /* Disable the normal item numbering */
    /*background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
    background-position: -5px;
    background-repeat: no-repeat;
    padding-left: 7px;*/
}

.txrx-ol > li:before {
    font-weight: bold;
    content:counter(li); /* Use the counter as content */
    counter-increment:li; /* Increment the counter by 1 */
    /* Position and style the number */
    position:absolute;
    /*left:-2.2em;*/
    left: 0px;
    width:2em;
    /* Some space between the number and the content in browsers that support
       generated content but not positioning it (Camino 2 is one example) */
    margin-right:8px;
    color:#fff;
    font-weight:bold;
    text-align:center;
    background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
}

li.L0, li.L1, li.L2, li.L3,
li.L5, li.L6, li.L7, li.L8
{ list-style-type: decimal !important }

.prettyprint{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint li{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint code{
      font-size: 14px;
      line-height: 17px;
}


</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Example 4: Chat room
        </p>

        <h2>Example 4: Chat room</h2>

        </div>

      <div id="content" class="col-md-12">

<h3><a>Chat room</a></h3>
      <p>
      Open up the chat room example in the Arduino IDE under File > Examples > Pozyx > chat_room.
      This example requires two or more pozyx shields and an equal number of Arduino's. In this example,
      it will be possible to chat wirelessly with all pozyx devices in range. Below is a figure of two
      devices talking. To run this demo, make sure that the serial monitor runs at 115200 baud with "both NL & CR" enabled.
      </p>

      <img src="<?php echo(base_url('assets/images/docs/tutorials/pozyx_chat_room.png')); ?>" style="margin: auto; margin-bottom: 20px; margin-left: 25px">


      <p>
      <b>Interpreting the LEDs:</b> When this program is running, the Rx LED should be blinking, indicating that the Pozyx shield is listening for incoming data.
      Also LED1 should be blinking at a low frequency to indicate that the pozyx system is working properly. When this LED stops blinking, something went wrong.
      Finally, the TX led will blink shortly whenever you transmit a message.
      </p>

<h3><a>The Arduino code explained</a></h3>
      <p>
            The Arduino sketch code begins as follows:
      </p>
<pre class="prettyprint linenums"style="padding-left: 20px"><code>#include &lt;Pozyx.h&gt;
#include &lt;Pozyx_definitions.h&gt;
#include &lt;Wire.h&gt;

uint16_t source_id;                 // the network id of this device
uint16_t destination_id = 0;        // the destination network id. 0 means the message is broadcasted to every device in range
String inputString = "";            // a string to hold incoming data
boolean stringComplete = false;     // whether the string is complete
</code></pre>

      <p>
      It can be seen that the sketch requires two pozyx files: "Pozyx.h" and "Pozyx_definitions.h" which contains the
      Pozyx Arduino library,and the "Wire.h" file for the I2C. After that, there are a number of global variables defined.</p>
      <p>
      Next, we look at the setup() function. This function runs only once when the Arduino starts up.
      </p>

<pre class="prettyprint linenums"style="padding-left: 20px"><code>void setup(){
  Serial.begin(115200);

  // initialize Pozyx
  if(! Pozyx.begin(false, MODE_INTERRUPT, POZYX_INT_MASK_RX_DATA, 0)){
    Serial.println("ERROR: Unable to connect to POZYX shield");
    Serial.println("Reset required");
    abort();
  }

  // read the network id of this device
  Pozyx.regRead(POZYX_NETWORK_ID, (uint8_t*)&source_id, 2);

  // reserve 100 bytes for the inputString:
  inputString.reserve(100);

  Serial.println("--- Pozyx Chat started ---");
}</code></pre>

      <p>
      From the code it can be seen that the Serial port is used at 115200 baud (~= bits per second).
      The other parameters for the serial port are default, i.e., 8bits, no parity, 1 stop bit.
      Next, the Pozyx device is initialized.
      The <code  class="prettyprint">Pozyx.begin(boolean print_result, int mode, int interrupts, int interrupt_pin)</code> function takes up to 4 parameters.
      This function checks that the Pozyx device is present and working correctly.
      The first parameter indicates if we want debug information printed out, the second describes the mode: <code>MODE_POLLING</code> or <code>MODE_INTERRUPT</code>. Using the polling mode,
      the Arduino will constantly poll (ask) the Pozyx shield if anything happened. Alternatively, the interrupt mode configures
      the pozyx device to give an interrupt signal every time an event has occured (this is the preferred way).
      Th events that should trigger in interrupt are configured by the
      <code>interrupts</code> parameter (<a href="<?php echo site_url('Documentation/Datasheet/FunctionalDescription#Interrupts');?>">more information about interrupts here</a>). Lastly, with <code>interrupt_pin</code> it is
      possible to select between the two possible interrupt pins (digital pin 2 or 3 on the arduino).
      </p>
      <p>
      Here, the pozyx device is configured such that it doesn't output debug data, and that it uses interrupt using interrupt pin
      0. With <code>POZYX_INT_MASK_RX_DATA</code> interrupts will be triggered each time wireless data is received.
      </p>
      <p>
            Next, the setup function reads out the 16 bit network id of the pozyx shield (this is the hexadecimal number printed on the label),
            and stores it in the global variable <code>source_id</code>. Notice that the <code>regRead</code> function works with bytes, so we indicate that we want to read 2 bytes
            starting from the register address <?php register_url("POZYX_NETWORK_ID");?> and store those to bytes in the byte pointer.
            Finally,  100 bytes are reserved for the input string.
      </p>
      <p>
      The code for the main loop is listed below:
      </p>
<pre class="prettyprint linenums"style="padding-left: 20px"><code>void loop(){

  // check if we received a newline character and if so, broadcast the inputString.
  if(stringComplete){
    Serial.print("Ox");
    Serial.print(source_id, HEX);
    Serial.print(": ");
    Serial.println(inputString);

    int length = inputString.length();
    uint8_t buffer[length];
    inputString.getBytes(buffer, length);

    // write the message to the transmit (TX) buffer
    int status = Pozyx.writeTXBufferData(buffer, length);
    // broadcast the contents of the TX buffer
    status = Pozyx.sendTXBufferData(destination_id);

    inputString = "";
    stringComplete = false;
  }

  // we wait up to 50ms to see if we have received an incoming message (if so we receive an RX_DATA interrupt)
  if(Pozyx.waitForFlag(POZYX_INT_STATUS_RX_DATA,50))
  {
    // we have received a message!

    uint8_t length = 0;
    uint16_t messenger = 0x00;
    delay(1);
    // Let's read out some information about the message (i.e., how many bytes did we receive and who sent the message)
    Pozyx.getLastDataLength(&length);
    Pozyx.getLastNetworkId(&messenger);

    char data[length];

    // read the contents of the receive (RX) buffer, this is the message that was sent to this device
    Pozyx.readRXBufferData((uint8_t *) data, length);
    Serial.print("Ox");
    Serial.print(messenger, HEX);
    Serial.print(": ");
    Serial.println(data);
  }
}</code></pre>

      <p>In the main loop, the system waits for any of the following two events:</p>
      <p>
      <ol><li>
      <b>The user has written some text and pressed enter</b> which is checked by the if-statement <code>if(stringComplete)</code>.
      When this happened, the program will show the text in the user terminal and broadcast the text.
      The text is broadcasted using the following two statements: <code>Pozyx.writeTXBufferData(buffer, length);</code>, which
      puts the data in a transmit buffer, ready for transmission, and <code>Pozyx.sendTXBufferData(destination_id);</code> to acutally
      transmit the data to the device with the give <code>destination_id</code>. In this example, <code>destination_id</code> is
      set to 0, meaning that the message is broadcasted (everyone will receive it).
      </li>
      <li>
      <b>The Pozyx device has received a wireless message</b>. This is checked by <code>Pozyx.waitForFlag(POZYX_INT_STATUS_RX_DATA,50)</code>.
      The <code>waitForFlag</code> waits for the RX_DATA interrupt for a maximum of 50ms. If the interrupt happened, the function returns success.
      At this point, the length of the message, and the network id of the device sending the message is obtained using <code> Pozyx.getLastDataLength(&length);
      Pozyx.getLastNetworkId(&amp;messenger);</code>. On the Pozyx device, the content of the received message is stored in the RX buffer.
      This content can be read using <code>Pozyx.readRXBufferData((uint8_t *) data, length);</code>.

      </li>
      </ol>

      </p>

      <p>This concludes the last example that is included in the Pozyx Arduino Library.
      Make sure to check out our Tutorials section where we will regularly post some new tutorials.
      If you have any requests for tutorials or if you have a tutorial of your own, please send us a message at info@pozyx.io.
      </p>

      </div>

      <div class="col-md-12" style="margin-top:100px;">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Example 4: Chat room
        </p>

        </div>

    </div>
</div>

<script>
 $( document ).ready(function() {
    // add links to function calls
    $("#content").html($("#content").html().replace(/Pozyx\.([a-zA-Z0-9\_]{3,})/g, "Pozyx.<a href='<?php echo site_url('Documentation/Datasheet/Arduino#');?>$1'>$1</a>"));

 });
</script>
