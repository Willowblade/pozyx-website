<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

<style>
.bittable{
      width:700px; 
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}

h3 a{
      text-decoration: none;
      color: #5F945F;
}

h3 a:hover{
      text-decoration: none;
      color: #5F945F;
}

h4{
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
      font-weight: bold;
      margin-bottom: 0px;
}

.step:hover{
      cursor: pointer;
      text-decoration: underline;
}

.txrx-ol {    
    display: block;
    padding-left: 25px;
    counter-reset:li; /* Initiate a counter */
}
.txrx-ol li {
      height: 24px;
      overflow-y: hidden;
      position: relative;
      padding-left: 40px;
    list-style:none; /* Disable the normal item numbering */
    /*background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
    background-position: -5px;
    background-repeat: no-repeat;
    padding-left: 7px;*/
}

.txrx-ol > li:before {
    font-weight: bold;
    content:counter(li); /* Use the counter as content */
    counter-increment:li; /* Increment the counter by 1 */
    /* Position and style the number */
    position:absolute;    
    /*left:-2.2em;*/
    left: 0px;
    width:2em;
    /* Some space between the number and the content in browsers that support
       generated content but not positioning it (Camino 2 is one example) */
    margin-right:8px;
    color:#fff;    
    font-weight:bold;
    text-align:center;
    background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
}

li.L0, li.L1, li.L2, li.L3,
li.L5, li.L6, li.L7, li.L8
{ list-style-type: decimal !important }

.prettyprint{
      font-size: 14px;
      line-height: 17px;
      margin-left: 20px;
      max-width: 600px;
}

.prettyprint li{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint code{
      font-size: 14px;
      line-height: 17px;
}

.serialMonitor{
      margin-left: 20px;
      max-width: 600px;
      padding-left: 20px;
      font-size: 14px;
      line-height: 17px;
}


</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Troubleshoot basics
        </p>  

        <h2>Troubleshoot basics</h2>            

        </div>  

      <div id="content" class="col-md-12">

<h3><a>Overview</a></h3>
      <p>
      In this tutorial, some basic Arduino code snippets are provided 
      that can help you figure out any problems when things aren't going as they should.
      If you haven't read the <a href="<?php echo site_url('Documentation/Tutorials/gettingStarted'); ?>">getting started</a>, 
      please do so and install the necessary tools and libraries.  
      Here is an overview of the troubleshooting basics:     
      </p>

      <ol style="margin-left: 25px">
        <li><a href="#readRegs">Device check</a></li>
        <li><a href="#readRemoteRegs">Remote device check</a></li>
        <li><a href="#discovery">Network check</a></li>
        <li><a href="#LEDS">Interpreting the LEDs</a></li>
      </ol>
      <br>
     

<h3><a name="readRegs">Device check</a></h3>
      <p>
            The first thing to do to get to know the Pozyx device, is read out some of the status registers. Registers can be read using the function
            <code>Pozyx.regRead()</code>. A full overview of all the registers with their description can be found <a href="<?php echo site_url('Documentation/Datasheet/RegisterOverview'); ?>">here</a>.
            Let's read out the first 5 bytes starting from the memory address <?php register_url("POZYX_WHO_AM_I");?> and display the result.
      </p>
<pre class="prettyprint" style="padding-left: 20px"><code>
uint8_t data[5] = {0,0,0,0,0};

Pozyx.regRead(POZYX_WHO_AM_I, data, 5);

Serial.print("who am i: ");
Serial.println(data[0], HEX);
Serial.print("firmware version: ");
Serial.println(data[1], HEX);
Serial.print("hardware version: ");
Serial.println(data[2], HEX);
Serial.print("self test result: ");
Serial.println(data[3], BIN);
Serial.print("error: ");
Serial.println(data[4], HEX);


</code></pre>

      <p>
      For a tag, the Serial monitor should show the following result:
      </p>
      <pre class='serialMonitor'>who am i: 43
firmware version: 1
hardware version: 23
self test result: 111111
error: 0</pre>

      <p>
        From these results you can already learn a great deal:
      </p>

      <ul style="margin-left: 20px;">
        <li><b>who am i</b> is wrong: the Pozyx device is not running or it is badly connected with the Arduino. Make sure that the jumper is on the BOOT0 pins.</li>
        <li><b>firmware version</b> is wrong: you must update the firmware as explained in <a href="<?php echo site_url('Documentation/Tutorials/gettingStarted'); ?>">getting started</a>.
        Make sure that all devices, anchors and tags are running on the same firmware version.</li>
        <li><b>Hardware version</b> is wrong: is your device on fire?</li>
        <li><b>Self test</b> is different: for the tag the result should be 111111 in binary, for the anchor it should be 110000. If this is different it indicates that some internal components 
        may not be working properly. Check out <?php register_url("POZYX_ST_RESULT");?> for more information. Check if the problem remains after resetting the device.</li>
        <li><b>The error code</b> is not 0: Something went wrong, this isn't necessarily dramatic. Check out <?php register_url("POZYX_ERRORCODE");?> to see which error was triggered.</li>
      </ul>

      <p>
      Note that the function <code>Pozyx.begin()</code> will check most of these registers as well to see if everything is working properly.
      </p>
      <br>

<h3><a name="readRemoteRegs">Remote device check</a></h3>      

      <p>
          At this point we would like to verify the same on a remote tag or anchor. We can easily do so by using the <code>Pozyx.remoteRegRead()</code> function.
          The following code reads out the requested information:
      </p>

<pre class="prettyprint"style="padding-left: 20px"><code>
uint8_t data[5] = {0,0,0,0,0};
uint16_t remote_id = 0x0001;      // replace this with the network id of the device you want to check.

Pozyx.remoteRegRead(remote_id, POZYX_WHO_AM_I, data, 5);

Serial.print("who am i: ");
Serial.println(data[0], HEX);
Serial.print("firmware version: ");
Serial.println(data[1], HEX);
Serial.print("hardware version: ");
Serial.println(data[2], HEX);
Serial.print("self test result: ");
Serial.println(data[3], BIN);
Serial.print("error: ");
Serial.println(data[4], HEX);


</code></pre>

     <p>When the <b>who am i</b> is not correct when reading out the register remotely, there may be additional causes:</p> 
     <ul style="margin-left: 20px;">
       <li>The remote address is wrong. It is printed on the label on the device.</li>
       <li>The devices are not within range or their signal is blocked for some reason.</li>
       <li>The devices are configured with different UWB settings. The UWB settings must be the same to enable communication. See <?php register_url("POZYX_UWB_CHANNEL");?> and the subsequent registers.</li>
       <li>Multiple Pozyx device are transmitting at the same time and are interfering with one-another.</li>

     </ul>

<br>

<h3><a name="discovery">Network check</a></h3>

      <p>
      For positioning, a number tags and anchors are required. Despite the hardware differences, each device can operate in both modes. This can be selected with jumper on the T/A pins.
      </p>

      <ul style="margin-left: 20px;">
        <li><b>Tag mode:</b> jumper is present. This device can move around and perform positioning</li>
        <li><b>Anchor mode:</b> jumper not present. This device must remain stationary and will support other devices in positioning.</li>
      </ul>

      <p>
        When a tag is performing auto calibration, it will only look for anchors. So make sure all the anchors are actually in anchor mode.
        With the function <code>Pozyx.doDiscovery()</code> it is possible to obtain a list of all the devices within range. The function takes one parameter to filter
        on tags, anchors or both. The following code will find and display all devices within range:
      </p>

<pre class="prettyprint" style="padding-left: 20px"><code>
uint8_t filter = 2;         // this shows all devices. Use 0 to only show the anchors. Use 1 to only show the tags

if( Pozyx.doDiscovery(filter) == POZYX_SUCCESS)
{    
  uint8_t numDevices = 0;
  Pozyx.getDeviceListSize(&amp;numDevices);
  Serial.print("Discovery found: ");
  Serial.print(numDevices);
  Serial.println(" device(s).");
  uint16_t tags[numDevices];
  Pozyx.getDeviceIds(tags, numDevices);

  int i;
  for(i=0; i&lt;numDevices; i++){
    Serial.print("0x");
    Serial.println(tags[i], HEX);
  }
}


</code></pre>
<br>

<h3><a name="LEDS">Interpreting the LEDs</a></h3>   

      <p>
          The Pozyx device has a number of indicator LEDs that can be used to analyze the Pozyx behavior. 
      </p>

      <ul style="margin-left: 25px">
        <li>LED 1: This LED should be blinking slowly to indicate that the system is responsive.</li>
        <li>LED 2: This LED indicates that the Pozyx device is performing a specific function (for example, calibration or positioning).</li>
        <li>LED 3: This LED has no function.</li>
        <li>LED 4: This LED will indicate that there was an error somewhere. The error can be found by reading the register <?php register_url("POZYX_ERRORCODE");?>.</li>
        <li>RX LED: This indicates that the UWB receiver is enabled and that the device can receive wireless signals.</li>
        <li>TX LED: This LED indicates that the device has transmitted a wireless signal.</li>
      </ul>

      <p>
          Using the register <?php register_url(" POZYX_CONFIG_LEDS");?> the indicator LEDs can be configured to be turned off. 
      </p>



      </div>

      <div class="col-md-12" style="margin-top:100px;">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Troubleshoot basics
        </p>           

        </div>  
        
    </div>
</div>

<script>
 $( document ).ready(function() {
    // add links to function calls
    $("#content").html($("#content").html().replace(/Pozyx\.([a-zA-Z0-9\_]{3,})/g, "Pozyx.<a href='<?php echo site_url('Documentation/Datasheet/Arduino#');?>$1'>$1</a>"));
    
 });
</script>