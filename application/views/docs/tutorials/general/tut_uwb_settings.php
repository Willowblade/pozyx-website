<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

<style>
.bittable{
      width:700px;
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}

h3 a{
      text-decoration: none;
      color: #5F945F;
}

h3 a:hover{
      text-decoration: none;
      color: #5F945F;
}

h4{
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
      font-weight: bold;
      margin-bottom: 0px;
}

.step:hover{
      cursor: pointer;
      text-decoration: underline;
}

.txrx-ol {
    display: block;
    padding-left: 25px;
    counter-reset:li; /* Initiate a counter */
}
.txrx-ol li {
      height: 24px;
      overflow-y: hidden;
      position: relative;
      padding-left: 40px;
    list-style:none; /* Disable the normal item numbering */
    /*background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
    background-position: -5px;
    background-repeat: no-repeat;
    padding-left: 7px;*/
}

.txrx-ol > li:before {
    font-weight: bold;
    content:counter(li); /* Use the counter as content */
    counter-increment:li; /* Increment the counter by 1 */
    /* Position and style the number */
    position:absolute;
    /*left:-2.2em;*/
    left: 0px;
    width:2em;
    /* Some space between the number and the content in browsers that support
       generated content but not positioning it (Camino 2 is one example) */
    margin-right:8px;
    color:#fff;
    font-weight:bold;
    text-align:center;
    background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
}

li.L0, li.L1, li.L2, li.L3,
li.L5, li.L6, li.L7, li.L8
{ list-style-type: decimal !important }

.prettyprint{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint li{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint code{
      font-size: 14px;
      line-height: 17px;
}


</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Configuration of the UWB parameters
        </p>

        <h2>Configuration of the UWB parameters</h2>

        </div>

      <div id="content" class="col-md-12">

<h3>The different UWB parameters</a></h3>
      <p>
        The selection of the ultra-wideband (UWB) parameters can be an important design choice.
        In this tutorial we explain how you can change the UWB settings and how this impacts the system's performance.
        Typically, the UWB parameters will impact the update rate, the range and the connectivity between devices.
        It is very important to realize that devices with different UWB settings may not be able to communicate with each other. Always make sure that
        all devices (tags and anchors) are using the same UWB settings in your application.

      </p>
      
      <p>
      For this part, no coding is necessary as we will use one of the Arduino sketches included in the Pozyx library that will guide you through the configuration.
      Start by opening the sketch pozyx_UWB_configurator that can be found under File > Examples > Pozyx > useful.
      Upload the sketch to an Arduino equipped with a Pozyx shield and open the serial monitor.
      Once the sketch starts, it will search and show all Pozyx devices in range with their respective UWB configuration settings.
      This is an example output of the sketch as viewed in the serial monitor:
      </p>

      <img src="<?php echo(base_url('assets/images/docs/tutorials/UWB_configurator.png')); ?>" style="margin: auto; margin-bottom: 20px; margin-left: 25px">
      <p><b>Fig. The UWB configurator has found 6 pozyx anchors, of which 5 are configured on channel 2 and, one is configured on channel 5.</b></p>
      <br><br>
      <p>      
      After scanning, it is possible to select one of the devices and change its UWB settings. 
      The new settings will be saved to the flash memory of the selected device and the device will keep using these settings even after restart.
      The following settings can be set:
      </p>

      <ul>
      <li><b>channel</b> 
      This sets the UWB channel. The pozyx device can use 6 independent UWB channels. Devices on different UWB channels
      cannot communicate and do not interfere with each other.
      In general, lower frequencies (i.e., lower channel numbers) also result in an increased communication range.
       More details can be found in the register description of <?php register_url("POZYX_UWB_CHANNEL");?>
      </li>

      <li><b>bitrate</b> 
      This sets the UWB bitrate. Three possible settings are possible: 110kbit/sec, 850kbit/sec and 6.81Mbit/sec.
      A higher bitrate will result is shorter messages and thus faster communication. However, this comes at the expense of a reduced operating range.
      The effect of the bitrate on the duration of ranging or positioning is shown in the figures below. 
      More details can be found in the register description of <?php register_url("POZYX_UWB_RATES");?>
      </li>

      <li><b>pulse repetition frequency (PRF)</b> 
      This sets the UWB pulse repetition frequency. Two possible settings are possible: 16MHz or 64MHz. This settings has little effect on the communication rate. However, on the same UWB channel, these two settings can live next to each other without interfering.
      More details can be found in the register description of <?php register_url("POZYX_UWB_RATES");?>
      </li>

      <li><b>preamble length</b> 
      This sets the UWB preamble length. This setting has 8 different options: 4096, 2048, 1536, 1024, 512 , 256 , 128 , or 64 symbols.
      A shorter preamble length results in shorter messages and thus faster communication. However, this again comes at the expense of a reduced operating range.
      The effect of the preamble length on the duration of ranging or positioning is shown in the figures below. 
      More details can be found in the register description of <?php register_url("POZYX_UWB_PLEN");?>
      </li>
      </ul>      

      </div>
      <div class="col-md-6">
        <img src="<?php echo(base_url('assets/images/news/ranging_speed_64MHz.png')); ?>" style="margin:auto" alt='ranging update rate Pozyx' title='ranging update rate Pozyx' width='100%'>
        <p style='text-align: center'><b>Fig 1. Ranging duration on the Pozyx device.</b></p><br>
      </div>
      <div class="col-md-6">
        <img src="<?php echo(base_url('assets/images/news/positioning_speed_64MHz.png')); ?>" style="margin:auto" alt='ranging update rate Pozyx' title='ranging update rate Pozyx' width='100%'>
        <p style='text-align: center'><b>Fig 2. Positioning duration on the Pozyx device.</b></p><br>
      </div>
      <div class="col-md-12">
      <p>
      From this, we can conclude that by changing the channel and PRF settings, up to 12 systems can run independently from each other.
      For the highest possible update rate (with the shortest range), the bitrate should be set to 6.81Mbit/sec and the preamble length to 64 symbols. Conversely, for the lowest possible update rate with the best range the bitrate should be set to 110kbit/sec with a preamble length of 4096 symbols, preferably on the first channel. 
      </p>

      <p><span class='text-warning'><span class='glyphicon glyphicon-alert'></span> <b>Note: </b></span><i>
      Make sure to enter 'save' after reconfiguring each device with the UWB_configurator sketch to save the settings in flash.
      </i></p>
      

      <h3>Doing it in code</h3>
      <p>      
      An overview of all the Arduino functions that can be used to manipulate the UWB settings in code can be found in the <a href="<?php echo site_url('Documentation/Datasheet/arduino#communication_functions'); ?>">Arduino library documentation</a>. Note that in order to save the settings for the 
      next reset, it is required to use the function  
      <a href="<?php echo site_url('Documentation/Datasheet/arduino#saveConfiguration'); ?>">saveConfiguration()</a>.
      </p>

      </div>

      <div class="col-md-12" style="margin-top:100px;">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Configuration of the UWB parameters
        </p>

        </div>

    </div>
</div>

<!--
<script>
 $( document ).ready(function() {
    // add links to function calls
    $("#content").html($("#content").html().replace(/Pozyx\.([a-zA-Z0-9\_]{3,})/g, "Pozyx.<a href='<?php echo site_url('Documentation/Datasheet/Arduino#');?>$1'>$1</a>"));

 });
</script>
-->
