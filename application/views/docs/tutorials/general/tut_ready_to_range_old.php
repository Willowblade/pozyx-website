<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

<style>
.bittable{
      width:700px;
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}

h3 a{
      text-decoration: none;
      color: #5F945F;
}

h3 a:hover{
      text-decoration: none;
      color: #5F945F;
}

h4{
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
      font-weight: bold;
      margin-bottom: 0px;
}

.step:hover{
      cursor: pointer;
      text-decoration: underline;
}

.txrx-ol {
    display: block;
    padding-left: 25px;
    counter-reset:li; /* Initiate a counter */
}
.txrx-ol li {
      height: 24px;
      overflow-y: hidden;
      position: relative;
      padding-left: 40px;
    list-style:none; /* Disable the normal item numbering */
    /*background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
    background-position: -5px;
    background-repeat: no-repeat;
    padding-left: 7px;*/
}

.txrx-ol > li:before {
    font-weight: bold;
    content:counter(li); /* Use the counter as content */
    counter-increment:li; /* Increment the counter by 1 */
    /* Position and style the number */
    position:absolute;
    /*left:-2.2em;*/
    left: 0px;
    width:2em;
    /* Some space between the number and the content in browsers that support
       generated content but not positioning it (Camino 2 is one example) */
    margin-right:8px;
    color:#fff;
    font-weight:bold;
    text-align:center;
    background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
}

li.L0, li.L1, li.L2, li.L3,
li.L5, li.L6, li.L7, li.L8
{ list-style-type: decimal !important }

.prettyprint{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint li{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint code{
      font-size: 14px;
      line-height: 17px;
}


</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Example 1: Ready to range
        </p>

        <h2>Example 1: Ready to range</h2>

        </div>

      <div id='content' class="col-md-12">

<h3><a>Ready to range</a></h3>
      <p>
      This is the first example that is included in the Pozyx Arduino library.
      If you haven't read the <a href="<?php echo site_url('Documentation/Tutorials/gettingStarted'); ?>">getting started</a>,
      please do so and install the necessary tools and libraries.
      If all tools are installed, open up the ready to range example in the Arduino IDE under File > Examples > Pozyx > ready_to_range.
      This example requires two Pozyx devices and one Arduino. In this example,
      the distance between the two devices is measured and the onboard LEDs will show in which range the
      devices are to each other. The Arduino will send a wireless signal such that the LEDs on the remote
      device also change. How the LEDs change as a function of the distance is sketched in the figure below:
      </p>

      <img src="<?php echo(base_url('assets/images/docs/tutorials/sketch_ranging.png')); ?>" style="margin: auto; margin-bottom: 20px; margin-left: 25px">


<h3><a>The setup of the Arduino sketch</a></h3>
      <p>
            We begin with the setup.
      </p>
<pre class="prettyprint linenums"style="padding-left: 20px"><code>include &lt;Pozyx.h&gt;
#include &lt;Pozyx_definitions.h&gt;
#include &lt;Wire.h&gt;

////////////////////////////////////////////////
////////////////// PARAMETERS //////////////////
////////////////////////////////////////////////

uint16_t destination_id = 0x6670;     // the network id of the other pozyx device: fill in the network id of the other device
signed int range_step_mm = 1000;      // every 1000mm in range, one LED less will be giving light.

////////////////////////////////////////////////

void setup(){
  Serial.begin(115200);

  if(Pozyx.begin() == POZYX_FAILURE){
    Serial.println("ERROR: Unable to connect to POZYX shield");
    Serial.println("Reset required");
    delay(100);
    abort();
  }

  Serial.println("------------POZYX RANGING V1.0------------");
  Serial.println("NOTES:");
  Serial.println("- Change the parameters:\n\tdestination_id (target device)\n\trange_step (mm)\n\t");
  Serial.println();
  Serial.println("- Approach target device to see range and\n led control");
  Serial.println("------------POZYX RANGING V1.0------------");
  Serial.println();
  Serial.println("START Ranging:");

  // make sure the pozyx system has no control over the LEDs, we're the boss
  uint8_t configuration_leds = 0x0;
  Pozyx.regWrite(POZYX_CONFIG_LEDS, &amp;configuration_leds, 1);

  // do the same with the remote device
  Pozyx.remoteRegWrite(destination_id, POZYX_CONFIG_LEDS, &amp;configuration_leds, 1);
}
</code></pre>

      <p>
      It can be seen that the sketch requires two pozyx files: "Pozyx.h" and "Pozyx_definitions.h" which contains the
      Pozyx Arduino library, and the "Wire.h" file for the I2C. After that, there are a number of global variables defined.
      The <code>destination_id</code> is the network id of the remote Pozyx tag (the network id is the 16 bit hexadecimal printed on the label,
      note that the prefix 0x is used to denote a hexadecimal number). You must change this network id so that it matches with
      your device. The <code>range_step_mm</code> is the interval which
      describes when the LEDs turn on or off (in the figure this is 1000mm = 1m or approximately 3.3feet ).
      </p>

      <p>
      Next, we look at the setup() function. This function runs only once when the Arduino starts up.
      it opens the Serial monitor at 115200 baud. The other parameters for the serial port are default, i.e., 8bits, no parity, 1 stop bit.
      Then the setup initializes the Pozyx device by calling the function <code>Pozyx.begin()</code>
      which checks if the pozyx device is connected and working properly. After printing some text
      to the serial monitor, the Pozyx on-board LEDs are configured. By default, the LEDs will blink to show system status. For
      this example we don't want this behavior, we want full control over the LEDs. This is performed by writing the byte-value 0 to the register
      <?php register_url("POZYX_CONFIG_LEDS"); ?>. This is done by the following line, which writes a single byte to the
      specified register.
      </p>

      <pre class="prettyprint"style="padding-left: 20px"><code>
uint8_t configuration_leds = 0x0;
Pozyx.regWrite(POZYX_CONFIG_LEDS, &amp;configuration_leds, 1);
      </code></pre>

      <p>Next, we want the same behavior on the remote Pozyx device. This can be easily performed with the following function,
        which writes a single byte to the specified register address of a remote device:</p>

      <pre class="prettyprint"style="padding-left: 20px"><code>
Pozyx.remoteRegWrite(destination_id, POZYX_CONFIG_LEDS, &amp;configuration_leds, 1);
      </code></pre>

      <p>
      Writing to or reading from the registers is the main method of communicating with the Pozyx device. Through the registers
      it's possible to configure the device like we just did with the LEDs, but also get status information, read sensor data or trigger some functionalities like calibration.
      In the Pozyx Arduino library, we have a number of function for communicating with the registers. All other function in the library
      use these core functions to perform their task. These important core functions (as defined in Pozyx.h) are listed below:
      </p>

     <ul>
        <li>static int regRead(uint8_t reg_address, uint8_t *pData, int size);</li>
        <li>static int regWrite(uint8_t reg_address, const uint8_t *pData, int size);</li>
        <li>static int regFunction(uint8_t reg_address, uint8_t *params, int param_size, uint8_t *pData, int size);</li>
      </ul>
      <p>and the remote equivalents</p>
      <ul>
        <li>static int remoteRegWrite(uint16_t destination, uint8_t reg_address, uint8_t *pData, int size);</li>
        <li>static int remoteRegRead(uint16_t destination, uint8_t reg_address, uint8_t *pData, int size);</li>
        <li>static int remoteRegFunction(uint16_t destination, uint8_t reg_address, uint8_t *params, int param_size, uint8_t *pData, int size);</li>

      </ul>

      <p>
      The <a href="<?php echo site_url('Documentation/Datasheet/RegisterOverview'); ?>">register overview</a>, that lists all the registers available on the Pozyx device,
      is a good source of (low-level) information for working with the Pozyx device.
      </p>


<h3>The main loop of the sketch</h3>
      <p>
      We move on to the <code>loop()</code> function which contains the program logic.
      </p>

<pre class="prettyprint linenums"style="padding-left: 20px"><code>void loop(){

  int status = 1;
  device_range_t range;

  // let's do ranging with the destination
  status = Pozyx.doRanging(destination_id, &amp;range);

  if (status == POZYX_SUCCESS){
    Serial.print(range.timestamp);
    Serial.print("ms \t");
    Serial.print(range.distance);
    Serial.println("mm \t");

    // now control some LEDs; the closer the two devices are, the more LEDs will be lit
    if (ledControl(range.distance) == POZYX_FAILURE){
      Serial.println("ERROR: setting (remote) leds");
    }
  }
  else{
    Serial.println("ERROR: ranging");
  }
}</code></pre>

      <p>In the loop function ranging is initiated using <code>status &amp;= Pozyx.doRanging(destination_id);</code>. This
      function returns <code>POZYX_SUCCESS</code> when finished. The range information is collected in the
      <code>range</code> variable which is an instance of the <code>device_range_t</code> structure.
      As seen below by its definition (in Pozyx.h), the <code>device_range_t</code> structure holds
      3 variables that contain valuable information about the measurement.
      More specifically: the timestamp in milliseconds, the measured distance in mm and the
      RSS-value (the received signal strength) expressed in dBm. The RSS-value is an indication of the
      signal quality. Depending on the UWB settings, -103dBm is more or less the lowest RSS at which reception is possible.
      Note that the <code> __attribute__((packed))</code> for the struct is used to pack the bits together,
      this is a necessary detail to make sure that there are no unused bits in the structure; in a wireless system,
      we do not want to waste transmission time on such unused bits.
      </p>

<pre class="prettyprint"style="padding-left: 20px"><code>typedef struct __attribute__((packed))_device_range {
    uint32_t timestamp;
    uint32_t distance;
    int8_t RSS;
}device_range_t;
</code></pre>

      <p>
        With the range data we now control the LEDs with the function <code>ledControl()</code>.
      </p>

<pre class="prettyprint"style="padding-left: 20px"><code>
int ledControl(uint32_t range){
  int status = 1;

  // set the LEDs of this pozyx device
  status &amp;= Pozyx.setLed(4, (range &lt; range_step_mm));
  status &amp;= Pozyx.setLed(3, (range &lt; 2*range_step_mm));
  status &amp;= Pozyx.setLed(2, (range &lt; 3*range_step_mm));
  status &amp;= Pozyx.setLed(1, (range &lt; 4*range_step_mm));

  // set the LEDs of the remote pozyx device
  status &amp;= Pozyx.setLed(4, (range &lt; range_step_mm), destination_id);
  status &amp;= Pozyx.setLed(3, (range &lt; 2*range_step_mm), destination_id);
  status &amp;= Pozyx.setLed(2, (range &lt; 3*range_step_mm), destination_id);
  status &amp;= Pozyx.setLed(1, (range &lt; 4*range_step_mm), destination_id);

  // status will be zero if setting the LEDs failed somewhere along the way
  return status;
}
</code></pre>

      <p>
      This function turns the LEDs on or off if they fall within a certain range. For example,
      <code>Pozyx.setLed(2, (range &lt; 3*range_step_mm))</code> will turn on the
      second LED (green) if the range is below 3 meter.
      </p>

      <p>This concludes the first example.
      In the <a href="<?php echo site_url('Documentation/Tutorials/ready_to_localize'); ?>">next example</a>, anchor calibration and positioning is explained.
      </p>


      </div>

      <div class="col-md-12" style="margin-top:100px;">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Example 1: Ready to localize
        </p>

        </div>

    </div>
</div>

<script>
 $( document ).ready(function() {
    // add links to function calls
    $("#content").html($("#content").html().replace(/Pozyx\.([a-zA-Z0-9\_]{3,})/g, "Pozyx.<a href='<?php echo site_url('Documentation/Datasheet/Arduino#');?>$1'>$1</a>"));

 });
</script>
