<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

<style>
.bittable{
      width:700px;
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}

h3 a{
      text-decoration: none;
      color: #5F945F;
}

h3 a:hover{
      text-decoration: none;
      color: #5F945F;
}

h4{
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
      font-weight: bold;
      margin-bottom: 0px;
}

.step:hover{
      cursor: pointer;
      text-decoration: underline;
}

.txrx-ol {
    display: block;
    padding-left: 25px;
    counter-reset:li; /* Initiate a counter */
}
.txrx-ol li {
      height: 24px;
      overflow-y: hidden;
      position: relative;
      padding-left: 40px;
    list-style:none; /* Disable the normal item numbering */
    /*background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
    background-position: -5px;
    background-repeat: no-repeat;
    padding-left: 7px;*/
}

.txrx-ol > li:before {
    font-weight: bold;
    content:counter(li); /* Use the counter as content */
    counter-increment:li; /* Increment the counter by 1 */
    /* Position and style the number */
    position:absolute;
    /*left:-2.2em;*/
    left: 0px;
    width:2em;
    /* Some space between the number and the content in browsers that support
       generated content but not positioning it (Camino 2 is one example) */
    margin-right:8px;
    color:#fff;
    font-weight:bold;
    text-align:center;
    background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
}

li.L0, li.L1, li.L2, li.L3,
li.L5, li.L6, li.L7, li.L8
{ list-style-type: decimal !important }

.prettyprint{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint li{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint code{
      font-size: 14px;
      line-height: 17px;
}


</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Example 3: Orientation 3D
        </p>

        <h2>Example 3: Orientation 3D</h2>

        </div>

      <div id="content" class="col-md-12">

<h3><a>Orientation 3D</a></h3>
      <p>
      Open up the orientation 3D example in the Arduino IDE under File > Examples > Pozyx > orientation_3D.
      This example requires one or two pozyx shields and one Arduino. In this example,
      the sensor data is read from the pozyx shield (either the shield mounted on the Arduino, or a remote shield).
      To visualize all the data, we use Processing. Below a screenshot of the Processing program showing all
      the sensor data graphically.
      </p>

      <img src="<?php echo(base_url('assets/images/docs/tutorials/remote_imu_processing.png')); ?>" style="width: 50%; height: 50%; margin: auto; margin-bottom: 20px; margin-left: 25px">

      <br>

<h3>Understanding the sensor data</h3>
<p>The plots show a lot of data, but what does it all mean?</p>
<ul>

      <li><b>Acceleration (g):</b> <br>
      the acceleration is measured along 3 axes (i.e., in 3 directions) which is shown by three different colors on the plot.
      The acceleration is expressed in g's (from gravity).
      1g is exactly the acceleration from earth's gravitation pull (1g = 9.81m/s2). Because the acceleration from gravity
      is always present and equal to 1g it can be used to determine how the device is tilted or pitched.
      Try rotating the Pozyx device by 90 degrees (slowly) and you will see the the acceleration change from one axis
      to the other.
      </li>
      <li><b>Magnetic field strength (µT):</b><br>
      the magnetic field strength is also measured along 3 axes and is expressed in µT. It can be used to measure
      the earth's magnetic field which varies between 30 and 60µT over the earth's surface. The measured magnetic
      field strength can be used to estimate the magnetic north (similar to a compass).
      However, magnetic fields exist wherever electric current flows or
      magnetic materials are present. Because of this, they will influence the sensor data and it will become impossible
      to determine the magnetic north exactly. Try holding a magnet or something iron close to the pozyx device and you will
      see the magnetic field strength fluctuate.
      </li>
      <li><b>Angular velocity (deg/s):</b><br>
      The angular velocity is measured by the gyroscope and measures how fast the device rotates around itself. By integrating
      the angular velocity it is possible to obtain the angles of the device. However, due to drift this method does not give
      accurate results for a prolonged time. What is drift you wonder? Well if the device is standing still, the angular
      velocity should be exactly equal to zero. This is not the case however, it will be slightly different and this
      error will accumulate over time when integrating to angles.
      </li>
      <li><b>3D Orientation:</b><br>
      The 3D orientation is shown by the 3D model in the middle. The orientation is computed by combining the sensor data
      from all three sensors together. By combining all sensors it is possible to overcome the limitations of each sensor
      separately. The 3D orientation can be expressed in Euler angles: yaw, pitch, roll or in quaternions.
      Quaternions are a mathematical representation using 4 numbers.
      In many situations, quaternions are preferred because they do not have singularity problems like the Euler angles.
      </li>

      <li><b>Gravity vector (g): </b><br>
      If the pozyx device is standing still, the acceleration is exactly equal to the gravity.
      The gravity vector is shown by the black line and is always pointing down. Notice that even when moving
      the device (which introduces an additional acceleration) the gravity vector still points down. This is due to
      the fusion algorithm that can separate gravity from an arbitrary acceleration;
      </li>

      <li><b>Linear acceleration in body coordinates (g):</b><br>
      The linear acceleration is the acceleration that remains after the gravity has been removed. When you
      hold the device horizontal, pointed forward, and shake it from left to right the circle will also move from left to right in the plot.
      However, if you rotate the device by 90 degrees
      and shake it again from left to right, the circle will now move in a different direction. This is because the linear acceleration
      is expressed in body coordinates, i.e., relative to the device. Note that all the above sensor data is expressed in
      body coordinates.
      </li>
      <li><b>Linear acceleration in world coordinates (g):</b><br>
      Once the orientation of the device is known, it is possible to express the acceleration in world coordinates.
      By doing this, the rotation of the device no longer affects the linear acceleration in the plot.
      </li>
</ul>


<h3><a>The Arduino code explained</a></h3>
      <p>
            The Arduino sketch code begins as follows:
      </p>
<pre class="prettyprint linenums" style="padding-left: 20px"><code>#include &lt;Pozyx.h&gt;
#include &lt;Pozyx_definitions.h&gt;
#include &lt;Wire.h&gt;

////////////////////////////////////////////////
////////////////// PARAMETERS //////////////////
////////////////////////////////////////////////

boolean bRemote = false;                  // boolean to indicate if we want to read sensor data from the attached pozyx shield (value 0) or from a remote pozyx device (value 1)
uint16_t destination_id = 0x6606;     // the network id of the other pozyx device: fill in the network id of the other device
uint32_t last_millis;                 // used to compute the measurement interval in milliseconds

////////////////////////////////////////////////

void setup()
{
  Serial.begin(115200);

  if(Pozyx.begin() == POZYX_FAILURE){
    Serial.println("ERROR: Unable to connect to POZYX shield");
    Serial.println("Reset required");
    delay(100);
    abort();
  }

  last_millis = millis();
  delay(10);

}
</code></pre>

      <p>This code simply loads the Pozyx library and the I²C library, and sets up the serial communication and the pozyx device.
      When variable <code>bRemote</code> can be set to <code>false</code>, the program will read the sensor data from the pozyx
      device mounted on the Arduino. When <code>bRemote=true</code>, the sensor data from another pozyx device (with a
      network id specified by <code>destination_id</code>) will be read wirelessly.
      </p>

      <p>
      Moving on to the main loop:
      </p>


<pre class="prettyprint linenums" style="padding-left: 20px"><code>void loop()
{
  int16_t sensor_data[24];
  uint8_t calib_status = 0;
  int i, dt;

  if(bRemote == true)
  {
    // remotely read the sensor data
    int status = Pozyx.remoteRegRead(destination_id, POZYX_PRESSURE, (uint8_t*)&amp;sensor_data, 24*sizeof(int16_t));
    if(status != POZYX_SUCCESS){
      return;
    }

  }else
  {
    // wait until this device gives an interrupt
    if (Pozyx.waitForFlag(POZYX_INT_STATUS_IMU, 10))
    {
      // we received an interrupt from pozyx telling us new IMU data is ready, now let's read it!
      Pozyx.regRead(POZYX_PRESSURE, (uint8_t*)&amp;sensor_data, 24*sizeof(int16_t));

      // also read out the calibration status
      Pozyx.regRead(POZYX_CALIB_STATUS, &amp;calib_status, 1);
    }else{
      // we didn't receive an interrupt
      uint8_t interrupt_status = 0;
      Pozyx.regRead(POZYX_INT_STATUS, &amp;interrupt_status, 1);

      return;
    }
  }

  // print out the results

  // print the measurement interval
  dt = millis() - last_millis;
  last_millis += dt;
  Serial.print(dt, DEC);

  // print out the presure (this is not an int16 but rather an uint32
  uint32_t pressure = ((uint32_t)sensor_data[0]) + (((uint32_t)sensor_data[1])&lt;&lt;16);
  Serial.print(",");
  Serial.print(pressure);

  // print out all remaining sensors
  for(i=2; i&lt;24; i++){
    Serial.print(",");
    Serial.print(sensor_data[i]);
  }

  // finally, print out the calibration status (remotely this is not available and all equal to zero)
  Serial.print(",");
  Serial.print(calib_status&amp;0x03);
  Serial.print(",");
  Serial.print((calib_status&amp;0x0C)>>2);
  Serial.print(",");
  Serial.print((calib_status&amp;0x30)>>4);
  Serial.print(",");
  Serial.print((calib_status&amp;0xC0)>>6);

  Serial.println();
}
</code></pre>

      <p>
      The main loop will first read out all the sensor data and then print it to the serial monitor as a comma separated list.
      When the data should come form a remote device (<code>bRemote=true</code>), the pozyx device will remotely read out
      the register data. This is performed with the function <code>remoteRegRead()</code>, which does exactly the same as
      <code>regRead()</code> but remotely from a pozyx device with a given network id.
      The data is read starting from the register address <?php register_url("POZYX_PRESSURE");?> and reading a total of
      48 bytes (<code>24*sizeof(int16_t)</code>), up to the register <?php register_url("POZYX_GRAV_Z");?>. The data is stored in an array of signed 16 bit ints (int16) because all sensor values we
      are reading (except the pressure) are represented with an int16 type.
      </p>

      <p>
      When the data is coming from the pozyx device mounted on the Arduino (<code>bRemote=true</code>), we use <code>regRead()</code>
      to read out the register data. However, here we wait for an interrupt from the pozyx device to notify us that new data is ready.
      This is achieved by <code>Pozyx.waitForFlag(POZYX_INT_STATUS_IMU, 10)</code> that waits up to 10ms for an IMU interrupt
      (<a href="<?php echo site_url('Documentation/Datasheet/FunctionalDescription#Interrupts');?>">more information about interrupts here</a>).
      Notice that whenever IMU data is ready, the Arduino also reads out the calibration status from the register address
      <?php register_url("POZYX_CALIB_STATUS");?>. The calibration status gives information about the quality of the
      sensor orientation data. When the system is not fully calibrated, the orientation estimation can be bad.
      </p>

      <p>
      Finally, after all data is read, it is printed to the serial monitor. The following piece of code combines two int16's into
      a single uint32 which represents the pressure.
      </p>
<pre class="prettyprint" style="padding-left: 20px"><code>uint32_t pressure = ((uint32_t)sensor_data[0]) + (((uint32_t)sensor_data[1])<<16);</code></pre>
      <p>
      The calibration status is a 8bit variable where every 2 bits represent some piece of calibration info. The last 10 lines of code
      mask out the bits in pairs and print them. For example, the following line of code:
      </p>
<pre class="prettyprint" style="padding-left: 20px"><code>Serial.print((calib_status&0x30)>>4);</code></pre>
      <p>
      masks out bits 4 and 5 (represented with the hexadecimal number 0x30) and then shifts the bits 4 places to the
      rights such that the result is a number between 0 and 3. A 0 represents no calibration and a 3 full calibration.
      </p>

      <p>This concludes the example.
      In the <a href="<?php echo site_url('Documentation/Tutorials/chat_room'); ?>">next example</a>, we will look at the wireless
      messaging capabilities to transfer custom data bytes. We will use this to create a wireless chat room.
      </p>
<!--
<h3><a>Visualization with processing</a></h3>

      <p>


      </p>



    -->
      </div>

       <div class="col-md-12" style="margin-top:100px;">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Example 3: Orientation 3D
        </p>

        </div>
    </div>
</div>

<script>
 $( document ).ready(function() {
    // add links to function calls
    $("#content").html($("#content").html().replace(/Pozyx\.([a-zA-Z0-9\_]{3,})/g, "Pozyx.<a href='<?php echo site_url('Documentation/Datasheet/Arduino#');?>$1'>$1</a>"));

 });
</script>
