<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

<style>
.bittable{
      width:700px;
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}

h3 a{
      text-decoration: none;
      color: #5F945F;
}

h3 a:hover{
      text-decoration: none;
      color: #5F945F;
}

h4{
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
      font-weight: bold;
      margin-bottom: 0px;
}

.step:hover{
      cursor: pointer;
      text-decoration: underline;
}

.txrx-ol {
    display: block;
    padding-left: 25px;
    counter-reset:li; /* Initiate a counter */
}
.txrx-ol li {
      height: 24px;
      overflow-y: hidden;
      position: relative;
      padding-left: 40px;
    list-style:none; /* Disable the normal item numbering */
    /*background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
    background-position: -5px;
    background-repeat: no-repeat;
    padding-left: 7px;*/
}

.txrx-ol > li:before {
    font-weight: bold;
    content:counter(li); /* Use the counter as content */
    counter-increment:li; /* Increment the counter by 1 */
    /* Position and style the number */
    position:absolute;
    /*left:-2.2em;*/
    left: 0px;
    width:2em;
    /* Some space between the number and the content in browsers that support
       generated content but not positioning it (Camino 2 is one example) */
    margin-right:8px;
    color:#fff;
    font-weight:bold;
    text-align:center;
    background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
}

li.L0, li.L1, li.L2, li.L3,
li.L5, li.L6, li.L7, li.L8
{ list-style-type: decimal !important }

.prettyprint{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint li{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint code{
      font-size: 14px;
      line-height: 17px;
}


</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Remote localization
        </p>

        <h2>Remote localization</h2>

        </div>

      <div id="content" class="col-md-12">

<h3><a name="Setup">Configuration of the UWB parameters</a></h3>
      <p>
      In this first section, we cover the configuration of the UWB parameters. 
      This is not absolutely necessary for remote localization, however, it enables 

      For this part, no coding is necessary as we will use one of the Arduino sketches that takes input.
      Start by opening the sketch pozyx_UWB_configurator that can be found in File > Examples > Pozyx > useful.
      Upload the sketch to an Arduino equipped with a Pozyx shield and open the serial monitor.
      Once the sketch starts, it will search and show all pozyx devices using all the different UWB parameters.
      You may see something similar to this:
      </p>

      <img src="<?php echo(base_url('assets/images/docs/tutorials/UWB_configurator.png')); ?>" style="margin: auto; margin-bottom: 20px; margin-left: 25px">
      <p><b>Fig. The UWB configurator has found 6 pozyx anchors, of which 5 are configured on channel 2 and, one is configured on channel 5.</b></p>
      <br><br>
      <p>      
      After scanning, it is possible to select on of the devices and change its UWB settings. 
      The new settings will be saved to the flash memory of the selected device and the device will keep using these settings even after restart.
      The following settings can be set:
      </p>

      <ul>
      <li><b>channel</b> 
      This sets the UWB channel. The posyx device can use 6 independent UWB channels. devices on different UWB channels
      cannot communicate and do not interfere with each other.
      In general, lower frequencies (i.e., lower channel numbers) also result in an increased communication range.
       More details can be found in the register description of <?php register_url("POZYX_UWB_CHANNEL");?>
      </li>

      <li><b>bitrate</b> 
      This sets the UWB bitrate. Three possible settings are possible: 110kbit/sec, 850kbit/sec and 6.81Mbit/sec.
      A higher bitrate will result is short messages and thus faster communication. However, this comes at the expense of a reduced operating range.
      The effect of the bitrate on the duration of ranging or positioning is shown in the figures below. 
      More details can be found in the register description of <?php register_url("POZYX_UWB_RATES");?>
      </li>

      <li><b>pulse repetition frequency</b> 
      This sets the UWB pulse repitition frequency. Two possible settings are possible: 16MHz or 64MHz. This settings has little effect on the communiction rate. However, on the same UWB channel, these two settings can live next to each other without interfering.
      More details can be found in the register description of <?php register_url("POZYX_UWB_RATES");?>
      </li>

      <li><b>preamble length</b> 
      This sets the UWB preable length. This setting has 8 different options: 4096, 2048, 1536, 1024, 512 , 256 , 128 , or 64 symbols.
      A shorter preamble length results in shorter messages and thus faster communication. However, this again comes at the expense of a reduced operating range.
      The effect of the preamble length on the duration of ranging or positioning is shown in the figures below. 
      More details can be found in the register description of <?php register_url("POZYX_UWB_PLEN");?>
      </li>
      </ul>

      </div>
      <div class="col-md-6">
        <img src="<?php echo(base_url('assets/images/news/ranging_speed_64MHz.png')); ?>" style="margin:auto" alt='ranging update rate Pozyx' title='ranging update rate Pozyx' width='100%'>
        <p style='text-align: center'><b>Fig 1. Ranging duration on the Pozyx device.</b></p><br>
      </div>
      <div class="col-md-6">
        <img src="<?php echo(base_url('assets/images/news/positioning_speed_64MHz.png')); ?>" style="margin:auto" alt='ranging update rate Pozyx' title='ranging update rate Pozyx' width='100%'>
        <p style='text-align: center'><b>Fig 2. Positioning duration on the Pozyx device.</b></p><br>
      </div>
      <div class="col-md-12">

      <p>
      With these things in mind, we now know that we must set all devices with the same UWB settings in order to make them communicate properly.
      For the sake of the tutorial, please set the parameters for all 4 anchors and two or more tags to channel 2 with bitrate 6.81Mbit/sec, 64MHz prf and a preamble length of 64. This should make the system work in a 10m x 10m area while giving the highest update rates.

      
      </p>
<h3><a name="Setup">Multi-tag localization</a></h3>
      <p>
      Now that all devices are configured with the same UWB settings, we can configure a tag for positioning.
      For this we will again use a sketch that will allow us to save the anchor information and some configurations information on the tag.
      This will allow us to perform remote positioning with Pozyx devices that are not connected with an Arduino.
      Start by opening the sketch pozyx_anchor_configurator that can be found in File > Examples > Pozyx > useful.
      Upload the sketch to an Arduino equipped with a Pozyx shield and open the serial monitor.
      </p>
      
      <img src="<?php echo(base_url('assets/images/docs/tutorials/anchor_configurator.png')); ?>" style="margin: auto; margin-bottom: 20px; margin-left: 25px">
      <p><b>Fig. The anchor configurator shows 6 anchors with their coordinates.</b></p>
      <br><br>

      <p>
      For multi-tag localization, we must follow the same procedure for each tag.
      After configuration, we can use the tags for remote multi-tag positioning. 
      </p>

<h3><a name="Setup">The master tag</a></h3>     
      <p>
      In order to do multi-tag localization, we need a master tag that initiates positioning on the remote tags.
      This is needed to make sure that no two tags are positioning at the same time as they would interfere with one-another.
      This approach has the additional advantage that all positioning data is available to the master tag which can send the data to a computer.
      A downfall of the approach is that all remote tags must stay within range of the master tag.
      </p>

      <p>
      If all devices are correctly configured in the step above, the master tag sketch on Arduino is very straightforward.
      An example sketch is shown below where two tags with the network id 0x6000 and 0x5000 will perform positioning and send the result to the master tag.
      Note that the remote tags do not require an Arduino. However, it is still possible to connect the remote tags with an Arduino and read out the 
      position coordinates locally.

      </p> 

      <pre class="prettyprint linenums" style="padding-left: 20px"><code>#include &lt;Pozyx.h&gt;
#include &lt;Pozyx_definitions.h&gt;
#include &lt;Wire.h&gt;

int numRemote = 2;
uint16_t remote_ids[2] = {0x6000, 0x5000};

void setup(){
  Serial.begin(115200);
  
  if(Pozyx.begin() == POZYX_FAILURE){
    Serial.println(F("ERROR: Unable to connect to POZYX shield"));
    Serial.println(F("Reset required"));
    delay(100);
    abort();
  }
}

void loop(){

  for(int i=0; i&lt;numRemote; i++)
  {
    // perform remote positioning
    coordinates_t position;  
    int status = Pozyx.doRemotePositioning(remote_ids[i], &amp;position);
    
    if (status == POZYX_SUCCESS)
    {
      Serial.print("POS,0x");
      Serial.print(remote_ids[i],HEX);
      Serial.print(",\t");
      Serial.print(position.x);
      Serial.print(",\t");
      Serial.print(position.y);
      Serial.print(",\t");
      Serial.println(position.z);  
    }
  }
}
</code></pre>


      </div>

      <div class="col-md-12" style="margin-top:100px;">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Multi-target localization
        </p>

        </div>

    </div>
</div>

<!--
<script>
 $( document ).ready(function() {
    // add links to function calls
    $("#content").html($("#content").html().replace(/Pozyx\.([a-zA-Z0-9\_]{3,})/g, "Pozyx.<a href='<?php echo site_url('Documentation/Datasheet/Arduino#');?>$1'>$1</a>"));

 });
</script>
-->
