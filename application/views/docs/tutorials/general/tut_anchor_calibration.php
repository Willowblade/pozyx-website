<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

<style>
.bittable{
      width:700px;
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}

h3 a{
      text-decoration: none;
      color: #5F945F;
}

h3 a:hover{
      text-decoration: none;
      color: #5F945F;
}

h4{
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
      font-weight: bold;
      margin-bottom: 0px;
}

.step:hover{
      cursor: pointer;
      text-decoration: underline;
}

.txrx-ol {
    display: block;
    padding-left: 25px;
    counter-reset:li; /* Initiate a counter */
}
.txrx-ol li {
      height: 24px;
      overflow-y: hidden;
      position: relative;
      padding-left: 40px;
    list-style:none; /* Disable the normal item numbering */
    /*background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
    background-position: -5px;
    background-repeat: no-repeat;
    padding-left: 7px;*/
}

.txrx-ol > li:before {
    font-weight: bold;
    content:counter(li); /* Use the counter as content */
    counter-increment:li; /* Increment the counter by 1 */
    /* Position and style the number */
    position:absolute;
    /*left:-2.2em;*/
    left: 0px;
    width:2em;
    /* Some space between the number and the content in browsers that support
       generated content but not positioning it (Camino 2 is one example) */
    margin-right:8px;
    color:#fff;
    font-weight:bold;
    text-align:center;
    background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
}

li.L0, li.L1, li.L2, li.L3,
li.L5, li.L6, li.L7, li.L8
{ list-style-type: decimal !important }

.prettyprint{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint li{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint code{
      font-size: 14px;
      line-height: 17px;
}

</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Automatic anchor calibration
        </p>

        <h2>Automatic anchor calibration</h2>

        </div>

      <div id="content" class="col-md-12">

 <p>
      The anchor calibration will automatically try to determine the relative anchor positions. This way,
      the anchor positions should not be measured manually. In the firmware version v1.0, 2D and semi-3D anchor
      calibration is supported. In these modes the height should still be provided manually for 3D positioning.
      This is because the accuracy of the height measurement greatly depends on the anchor places. And usually it
      is not accurate enough for a calibration. Fortunately, the height of the device is the easy to measure.
      In principle, the anchor calibration should only be performed once, unless the anchors have moved.

      The pieces of code for the anchor calibration are placed in the <code>setup()</code> function.  Let's take a look
      at the first part of the sketch
      </p>

      <pre class="prettyprint linenums" style="padding-left: 20px"><code>#include &lt;Pozyx.h&gt;
#include &lt;Pozyx_definitions.h&gt;
#include &lt;Wire.h&gt;

////////////////////////////////////////////////
////////////////// PARAMETERS //////////////////
////////////////////////////////////////////////

uint8_t num_anchors = 4;
uint16_t anchors[4] = {0x1156, 0x256B, 0x3325, 0x4244};
int32_t heights[4]  = {2750, 2000, 1900, 2350};

////////////////////////////////////////////////
///////////////////// SETUP ////////////////////
////////////////////////////////////////////////

void setup(){
  Serial.println();
  Serial.begin(115200);

  if(Pozyx.begin() == POZYX_FAILURE){
    Serial.println(F("ERROR: Unable to connect to POZYX shield"));
    Serial.println(F("Reset required"));
    delay(100);
    abort();
  }

  Serial.println(F("----------POZYX POSITIONING V1.0----------"));
  Serial.println(F("NOTES:"));
  Serial.println(F("- No parameters required."));
  Serial.println();
  Serial.println(F("- System will auto start calibration"));
  Serial.println();
  Serial.println(F("- System will auto start positioning"));
  Serial.println(F("----------POZYX POSITIONING V1.0----------"));
  Serial.println();
  Serial.println(F("Performing auto anchor calibration:"));

  // clear all previous devices in the device list
  Pozyx.clearDevices();

  int status = Pozyx.doAnchorCalibration(POZYX_2_5D, 20, num_anchors, anchors, heights);

  if (status != POZYX_SUCCESS){
    Serial.println(status);
    Serial.println(F("ERROR: calibration"));
    Serial.println(F("Reset required"));
    abort();
  }

  printCalibrationResultProcessing();


  delay(1000);

  Serial.println(F("Starting positioning: "));

  uint8_t num_anchors = (1&lt;&lt;7) | (0x5);
  Pozyx.regWrite(POZYX_POS_NUM_ANCHORS, &amp;num_anchors, 1);
  delay(5);

  Pozyx.regRead(POZYX_POS_NUM_ANCHORS, &amp;num_anchors, 1);
  Serial.println(num_anchors, HEX);

  uint8_t err;
  Pozyx.regRead(POZYX_ERRORCODE, &amp;err, 1);
  Serial.println(err, HEX);

}
</code></pre>

      <p>
      As normal, the sketch begins by including the Pozyx Library header files and Wire.h for the I²C communication.
      Some global variables are defined that describe the number of anchors, their network ids and their respective heights in mm.
      For the anchor calibration it is not necessary to know the network ids, however, if we do, the anchor calibration
      result will be easier to understand.
      </p>
      <p>
      The <code>setup()</code> function begins by initializing the Serial communication and the Pozyx device, after which some
      explanation text is printed to the Serial monitor.
      Next, the device list is cleared. The device list is a list that does the bookkeeping of all Pozyx devices in the
      area. We clear this list to get a fresh start. Next we start the anchor calibration procedure using the function
      <code>Pozyx.doAnchorCalibration()</code> defined in the library as follows:
      </p>

      <pre class="prettyprint" style="padding-left: 20px"><code>int doAnchorCalibration(int dimension = POZYX_2D, int num_measurements = 10, int num_anchors = 0, uint16_t anchors[] = NULL,  int32_t heights[] = NULL);</code></pre>

      <p>
      The function can take up to 5 parameters.
      The first parameter indicates the dimension which can be <code>POZYX_2D</code> or <code>POZYX_2_5D</code> for 2D anchor
      calibration or semi-3D anchor calibration, respectively. For 2D all the anchors are required to be placed in the same
      horizontal plane. For semi-3D the anchors can be place at arbitrary heights, however, in this case the heights
      must be supplied by the <code>heights</code> parameter.
      The <code>num_measurements</code> defines how many measurements should be made for the calibration. The larger this number,
      the more accurate the calibration, but the longer it will take. Next, with <code>num_anchors</code> we can define how many anchors we wish
      to use for calibration and give their network ids with the <code>anchors[]</code> parameter. The default value for the number
      of anchors is 0 in which case the Pozyx device will first perform a discovery operation to identify all neighboring anchors
      within range. However, there is a good reason to supply the anchor ids as we will see next.</p>

      <p>
      <b>Fixing the coordinate system: </b><br>
      In general, the relative positions of the anchors will be obtained with the anchor calibration. This means that the shape
      of how the anchors are positioned is obtained but that you can still rotate or translate (move) the shape. The
      calibration algorithm doesn't know which rotation or translation you like, unless you specify it by supplying the anchor
      ids in a fixed order. More specifically:</p>

      <p>
      The first anchor supplied to the anchor calibration will be used to determine the origin. It's x and y coordinates will
      always be equal to (0,0). The second anchor will be fixed on the x-axis such that it's (x,y) coordinates are (d, 0) with
      d the distance to the first anchor. The third anchor will describe which way is up for the y-axis. The y-coordinate of this
      anchor will be fixed to be positive. The remaining anchors have no effect on the coordinate system.
      </p>

      <p>
      Let's try out two different anchor orderings and see the result:
      </p>
      <pre class="prettyprint" style="padding-left: 20px"><code>uint16_t anchors1[4] = {0x1156, 0x256B, 0x3325, 0x4244};
uint16_t anchors2[4] = {0x3325, 0x1156, 0x256B, 0x4244};</code></pre>

      <p>
      For the first anchor ordering with <code>anchors1[4]</code> we obtain the coordinate system as seen in the left figure.
      With the ordering in <code>anchors2[4]</code> we have the coordinate system on the right figure.
      </p>
      <img src="<?php echo(base_url('assets/images/docs/tutorials/sketch_room_orientation.png')); ?>" style="dispaly: block; align:center; margin: auto; margin-bottom: 20px; margin-left: 25px">

      <p>
      In our example we have taken the first coordinate system because this nicely aligns with the room. The line of code that performs
      the anchor calibration in the example is:
      </p>
      <pre class="prettyprint" style="padding-left: 20px"><code>int status = Pozyx.doAnchorCalibration(POZYX_2_5D, num_anchors, 20, anchors, heights);
      </code></pre>

<!--
      <p>
       Let's see some example usages:</p>
      <ul>
        <li><code>int status = Pozyx.doAnchorCalibration();</code>
        Performs 2D anchor calibration by first discovering the anchors in range.</li>
        <li><code>int status = Pozyx.doAnchorCalibration(POZYX_2D, 20, num_anchors, anchors);</code>
        Performs 2D anchor calibration using the anchors specified in the array anchors. 20 measurements per link
        will be used.
        </li>
        <li><code>int status = Pozyx.doAnchorCalibration(POZYX_2_5D, 10, num_anchors, anchors, heights);</code>
        Semi-3D anchor calibration using the anchors specified in the array anchors. 10 measurements per link
        will be used. The height of each anchor is given in mm in the heights array.
         </li>
      </ul>
      -->

      <p>This performs semi-3D anchor calibration making 20 measurements and with a given number of anchors (4), anchor ordering and fixed heights.
      After the function returns (which takes about 1 second for 4 anchors and 10 measurements) we can read the anchor coordinates from the device list.
      This is performed by using the function <code>Pozyx.getDeviceCoordinates(anchors[i], &amp;anchor_coor);</code>
      which stores the anchor coordinates of the i-th anchor of the anchors array in the anchor_coor variable. Calibration is now finished!
      </p>
      <br>

<h3><a name="ManualAnchorCalibration">Manual anchor calibration</a></h3>

      <p>
        In the previous section, we used the automatic anchor calibration to figure out the coordinates of the anchors.
        However, in some cases you may want to have better control over this process.
        Roughly speaking, the error in the anchor positions is 'transferred'
        to the error of the tag position. Because the automatic anchor calibration introduces some small errors, it is
        recommended to manually measure the anchor positions if you require the best possible positioning accuracy. Do this as accurately as possible, antenna to antenna!
        It's pretty simple to set your own coordinates for the anchors. The following snippet of code
        adds an anchor with network id 0x3325 and with coordinates x=0mm, y=10000mm and z=2000mm.
        <!--for the anchors with the function <code>addAnchor(uint16_t network_id, int x_mm, int y_mm, int z_mm)</code>. In the following snippet of code, we manually define
        the coordinates for the 4 anchors:-->
      </p>

        <pre class="prettyprint" style="padding-left: 20px"><code>
device_coordinates_t anchor;

anchor.network_id = 0x3325;
anchor.flag = 0x1; // indicate that it is an anchor.
anchor.pos.x = 0;  // the measured x-coordinate in mm.
anchor.pos.y = 10000;  // the measured y-coordinate in mm.
anchor.pos.z = 2000;   // the measured z-coordinate in mm.
Pozyx.addDevice(anchor);
        </code></pre>

        <!--
        Pozyx.addAnchor(0x1156, 0, 0, 2000);
        Pozyx.addAnchor(0x256B, 10000, 0, 2000);
        Pozyx.addAnchor(0x3325, 0, 6000, 2000);
        Pozyx.addAnchor(0x4244, 10000, 6000, 2000);
        -->

        <br>

      </div>

      <div class="col-md-12" style="margin-top:100px;">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Automatic anchor calibration
        </p>

        </div>

    </div>
</div>

<!--
<script>
 $( document ).ready(function() {
    // add links to function calls
    $("#content").html($("#content").html().replace(/Pozyx\.([a-zA-Z0-9\_]{3,})/g, "Pozyx.<a href='<?php echo site_url('Documentation/Datasheet/Arduino#');?>$1'>$1</a>"));

 });
</script>
-->
