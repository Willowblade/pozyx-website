<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

<style>
.Arduino{
  <?php if($type!="Arduino") echo "display:none;"?>
}

.Python{
  <?php if($type!="Python") echo "display:none;"?>
}

.bittable{
      width:700px;
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}

h3 a{
      text-decoration: none;
      color: #5F945F;
}

h3 a:hover{
      text-decoration: none;
      color: #5F945F;
}

h4{
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
      font-weight: bold;
      margin-bottom: 0px;
}

.step:hover{
      cursor: pointer;
      text-decoration: underline;
}

.txrx-ol {
    display: block;
    padding-left: 25px;
    counter-reset:li; /* Initiate a counter */
}
.txrx-ol li {
      height: 24px;
      overflow-y: hidden;
      position: relative;
      padding-left: 40px;
    list-style:none; /* Disable the normal item numbering */
    /*background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
    background-position: -5px;
    background-repeat: no-repeat;
    padding-left: 7px;*/
}

.txrx-ol > li:before {
    font-weight: bold;
    content:counter(li); /* Use the counter as content */
    counter-increment:li; /* Increment the counter by 1 */
    /* Position and style the number */
    position:absolute;
    /*left:-2.2em;*/
    left: 0px;
    width:2em;
    /* Some space between the number and the content in browsers that support
       generated content but not positioning it (Camino 2 is one example) */
    margin-right:8px;
    color:#fff;
    font-weight:bold;
    text-align:center;
    background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
}

li.L0, li.L1, li.L2, li.L3,
li.L5, li.L6, li.L7, li.L8
{ list-style-type: decimal !important }

.prettyprint{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint li{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint code{
      font-size: 14px;
      line-height: 17px;
}


</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Getting started
        </p>

        <h2>Getting started</h2>

        <p><a class="Python" href="<?php echo site_url('Documentation/Tutorials/getting_started/Arduino'); ?>">Go to Arduino version</a><a class="Arduino" href="<?php echo site_url('Documentation/Tutorials/getting_started/Python'); ?>">Go to Python version</a></p>

        </div>

      <div class="col-md-12">


<h3><a name="requiredTools">Downloads</a></h3>
      <p>
      The following tools or libraries are recommended to start with the Pozyx system.
      </p>

      <ul style="margin-left: 25px">
            <li class="Arduino"><b>The Arduino IDE.</b> Download the IDE from the <a href="https://www.arduino.cc/en/Main/Software">Arduino website</a> and install it.</li>
            <li class="Arduino"><b>The Pozyx Arduino Library.</b> The library can be obtained from <a href="https://github.com/pozyxLabs/Pozyx-Arduino-library">github</a> or <a href="https://github.com/pozyxLabs/Pozyx-Arduino-library/archive/master.zip">downloaded from the zip file directly</a>.
            Once downloaded, open the Arduino IDE and go to Sketch > Include Library > Add .ZIP library... and select the downloaded zip file.
            The examples should now show up under File > Examples > Pozyx (a restart of the program may be necessary first).
            </li>
            <li class="Python"><b>Python.</b> You probably have this installed already. If you want to run the tutorials, you'll need <b>Python 3</b>, as we'll be using python-osc to facilitate external visualisations.</li>
            <li class="Python"><b>The Pozyx Python library.</b> The library can be obtained from <a href="https://github.com/pozyxLabs/Pozyx-Python-library">github</a> or <a href="https://github.com/pozyxLabs/Pozyx-Python-library/archive/master.zip">downloaded from the zip file directly</a>.
            You should follow the installation instructions on the GitHub page. The tutorial and example scripts are also available there.</li>
            <li class="Python"><b>The python-osc library.</b> <a href="http://opensoundcontrol.org/spec-1_0">The Open Sounce Control protocol</a> will be used in the tutorials to communicate with other software on your computer. If you have pip installed, you can do this easily by running <code>pip install python-osc</code>. Further information can be found on <a href="https://pypi.python.org/pypi/python-osc">its PyPi site</a></li>
            <li><b>The Processing IDE.</b> Download the IDE from the <a href="https://processing.org/download/?processing">Processing website</a> and install it.
            The Processing IDE is a minimal java environment that allows you to quickly write graphical programs. It is used in a few tutorials.</li>
            <li><b>Processing libraries and examples</b>. Download the <a href="https://github.com/pozyxLabs/Pozyx-processing">Processing sketches on github</a>.</li>
            <li><b>Firmware update tool.</b> <a href="http://www.st.com/web/en/catalog/tools/FM147/CL1794/SC961/SS1533/PF257916">Download this tool from ST microelectronics</a> to upload new versions of the firmware on the device.
            The firmware update process is <a href="<?php echo site_url('Documentation/Tutorials/gettingStarted#firmwareUpdate'); ?>">explained below</a>.</li>
      </ul>

      <br>

<h3><a name="introduction">Documentation</a></h3>
      <p>
      A lot of documentation for the Pozyx system is available on the website.
      If you are new to Pozyx it is recommended to start with the tutorials:</p>

      <ul style="margin-left: 25px">
          <li><a href="<?php echo site_url('Documentation/Tutorials/ready_to_range/'.$type); ?>">Tutorial 1: Ready to range</a></li>
          <li><a href="<?php echo site_url('Documentation/Tutorials/ready_to_localize/'.$type); ?>">Tutorial 2: Ready to localize</a></li>
          <li><a href="<?php echo site_url('Documentation/Tutorials/orientation_3D/'.$type); ?>">Tutorial 3: Orientation 3D</a></li>
          <li><a href="<?php echo site_url('Documentation/Tutorials/multitag_positioning/'.$type); ?>">Tutorial 4: Multitag</a></li>
      </ul>
      <p>
      To dig deeper in the functionalities of the Pozyx system you can check the online datasheet that has the following information:
      </p>

      <ul style="margin-left: 25px">
      <li><a href="<?php echo site_url('Documentation/Datasheet/FunctionalDescription'); ?>">Functional description</a></li>
      <li><a href="<?php echo site_url('Documentation/Datasheet/RegisterOverview'); ?>">Register overview</a></li>
      <li><a href="<?php echo site_url('Documentation/Datasheet/Interfaces'); ?>">Interfaces protocols (I²C and USB)</a></li>
      <li><a href="<?php echo site_url('Documentation/Datasheet/pinOut'); ?>">Pin-out</a></li>
      </ul>

      <p>
      Finally, if you want to learn more about the theory behind the Pozyx device, check out the Pozyx Academy.
      </p>

      <ul style="margin-left: 25px">
          <li><a href="<?php echo site_url('Documentation/doc_howDoesPositioningWork'); ?>">How does positioning work?</a></li>
          <li><a href="<?php echo site_url('Documentation/doc_howDoesUwbWork'); ?>">How does ultra-wideband work?</a></li>
          <li><a href="<?php echo site_url('Documentation/doc_whereToPlaceTheAnchors'); ?>">Where to place the anchors?</a></li>
      </ul>

      <br>

<h3><a name="firmwareUpdate">Firmware update</a></h3>

      <p>Updating the firmware is a relatively easy process which only requires a few steps.
      The tool requires Windows 10 or earlier versions. For a full technical description of the tool we refer to
      <a href="http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/CD00155676.pdf">this application note</a>.</p>
      <ol style="margin-left: 25px">
        <li><a href="<?php echo base_url('documents/downloads/en.stsw-stm32080.zip'); ?>">Download and install the DFU tool.</a></li>
        <li>Latest firmware download-link can be found on your order page, a notification e-mail will be send.</li>
        <li>Download the latest firmware version (.dfu file).</li>
        <li>Open the DFU tool called "DfuSe Demomonstration".</li>
        <li>remove the jumper from the BOOT0 pin on the pozyx device and insert the USB to your computer.
        After the driver has automatically installed, "STM device in DFU mode" should become visible under
        Available DFU devices.</li>
        <li>Under "Upgrade or Verify action", click on choose and select the .dfu file.</li>
        <li>Make sure targetid is set to: 00 - Internal Flash </li>
        <li>Click on Upgrade to upload the new firmware version.</li>
        <li>Place the jumper on the BOOT0 pins again to use the Pozyx device.</li>

      </ol>

      <img src="<?php echo(base_url('assets/images/docs/tutorials/firmware_update.png')); ?>" style="margin: auto; margin-bottom: 20px; margin-left: 25px">



      <br>

      </div>

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Getting started
        </p>

        </div>

    </div>
</div>
