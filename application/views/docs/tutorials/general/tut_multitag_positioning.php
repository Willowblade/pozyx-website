<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

<style>
.Arduino{
  <?php if($type!="Arduino") echo "display:none;"?>
}

.Python{
  <?php if($type!="Python") echo "display:none;"?>
}

.bittable{
      width:700px;
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}

h3 a{
      text-decoration: none;
      color: #5F945F;
}

h3 a:hover{
      text-decoration: none;
      color: #5F945F;
}

h4{
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
      font-weight: bold;
      margin-bottom: 0px;
}

.step:hover{
      cursor: pointer;
      text-decoration: underline;
}

.txrx-ol {
    display: block;
    padding-left: 25px;
    counter-reset:li; /* Initiate a counter */
}

.Euler{
  <?php if($type!="Euler") echo "display:none;"?>
}

.txrx-ol li {
      height: 24px;
      overflow-y: hidden;
      position: relative;
      padding-left: 40px;
    list-style:none; /* Disable the normal item numbering */
    /*background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
    background-position: -5px;
    background-repeat: no-repeat;
    padding-left: 7px;*/
}

.txrx-ol > li:before {
    font-weight: bold;
    content:counter(li); /* Use the counter as content */
    counter-increment:li; /* Increment the counter by 1 */
    /* Position and style the number */
    position:absolute;
    /*left:-2.2em;*/
    left: 0px;
    width:2em;
    /* Some space between the number and the content in browsers that support
       generated content but not positioning it (Camino 2 is one example) */
    margin-right:8px;
    color:#fff;
    font-weight:bold;
    text-align:center;
    background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
}

li.L0, li.L1, li.L2, li.L3,
li.L5, li.L6, li.L7, li.L8
{ list-style-type: decimal !important }

.prettyprint{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint li{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint code{
      font-size: 14px;
      line-height: 17px;
}

</style>

<div class="container">
      <!-- Example row of columns -->
  <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

    <div class="col-md-12">
      <h1 class="Euler">RANDOM EULER</h1>
      <p>
          <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
          Tutorial 4: Multitag positioning
      </p>

      <h2>Tutorial 4: Multitag positioning</h2>

      <p><a class="Python" href="<?php echo site_url('Documentation/Tutorials/multitag_positioning/Arduino'); ?>">Go to Arduino version</a><a class="Arduino" href="<?php echo site_url('Documentation/Tutorials/multitag_positioning/Python'); ?>">Go to Python version</a></p>

    </div>

    <div id="content" class="col-md-12">

      <h3><a>Multitag positioning</a></h3>

      <p>
        The multitag positioning is an extension of <a href=<?php echo site_url('Documentation/Tutorials/ready_to_localize/'.$type) ?>>tutorial 2</a>, where we went over the positioning of a single device, and this being an extension is also visible in the code. Therefore, this tutorial will seem rather short and will go over the additions to allow for multitag. It is thus essential to follow that tutorial first.
      </p>

      <p class="Arduino">
        For this tutorial you'll need an Arduino, at least four anchors and at least three other powered devices. These can be either tags or anchors, but remember that anchors don't have an IMU.<br />
        Open up the multitag positioning example in the Arduino IDE under File > Examples > Pozyx > multitag_positioning.
      </p>

      <p class="Python">
        For this tutorial you'll need at least four anchors and at least three other powered devices. These can be either tags or anchors, but remember that anchors don't have an IMU.<br />
        If all tools are installed, open up multitag_positioning.py in the Pozyx library’s tutorial folder. Probably, the path to this file will be "Downloads/Pozyx-Python-library/tutorials/multitag_positioning.py". You can run the script from either command line or a text editor that allows running the scripts as well. If you're on Windows and have installed Python, you might be able to simply double-click it. If you you get an error, you likely forgot to install pythonosc, you can do this easily using <code>pip install python-osc</code>.
      </p>

      <h3><a>Plug and play</a></h3>

      <p>
        Looking at the parameters of the multitag positioning, you will see one major addition over the regular positioning example, namely the <code>tags</code> parameter which replaced the remote_id and remote parameters. In this list, you'll have to write the IDs of the remote devices you'll be using. For your anchors, use the setup you measured out in the positioning tutorial. You can also again choose the dimension and algorithm used for the positioning.
      </p>
      <p class="Arduino">
        There is also the <code>num_tags</code> parameter, which you'll need to change to the size of your tags array.
      </p>

      <pre class="prettyprint linenums Arduino"style="padding-left: 20px"><code>int num_tags = 3;
  uint16_t tags[3] = {0x0001, 0x0002, 0x0003};</code></pre>

      <pre class="prettyprint linenums Python"style="padding-left: 20px"><code>tags=[0x0001, 0x0002, 0x0003]        # remote tags</code></pre>

      <p>
        Once you put the IDs of your tags here, you can start with multitag positioning. You can use the positioning Processing sketch straight from the bat as well if you set your <code>use_processing</code> to true and set your Processing <code>serial</code> and port correctly. However, reading the terminal output offers a more deterministic way of knowing whether you've got it working.
      </p>

      <h3><a>Code additions and changes</a></h3>

      <p>
        As this is an extension of the ready to localize tutorial, the code has been modified to support multiple remote devices through the added tags parameter. Printing both error and position now takes an additional parameter: the ID of the device we're working with.
      </p>

      <p><b>Anchor configuration</b><p>
        <p>
          Each device needs to be configured with the anchors to successfully perform positioning. The anchor configuration is performed on every device using <code>setAnchorsManual</code>, which iterates over every remote device, and feedback on this configuration is printed.
        </p>

      <pre class="prettyprint linenums Arduino"style="padding-left: 20px"><code>void setAnchorsManual(){
  for (int i = 0; i < num_tags; i++){
    int status = Pozyx.clearDevices(tags[i]);
    for(int i = 0; i < num_anchors; i++){
      device_coordinates_t anchor;
      anchor.network_id = anchors[i];
      anchor.flag = 0x1;
      anchor.pos.x = anchors_x[i];
      anchor.pos.y = anchors_y[i];
      anchor.pos.z = heights[i];
      status &amp;= Pozyx.addDevice(anchor, tags[i]);
    }
    if (status == POZYX_SUCCESS){
      Serial.print("Configuring ID 0x");
      Serial.print(tags[i], HEX);
      Serial.println(" success!");
    }else{
      printErrorCode("configuration", tags[i]);
    }
  }
}</code></pre>

      <pre class="prettyprint linenums Python"style="padding-left: 20px"><code>def setAnchorsManual(self):
    """Adds the manually measured anchors to the Pozyx's device list one for one."""
    for tag in self.tags:
        status = self.pozyx.clearDevices(tag)
        for anchor in self.anchors:
            status &amp;= self.pozyx.addDevice(anchor, tag)
        if len(anchors) > 4:
            status &amp;= self.pozyx.setSelectionOfAnchors(POZYX_ANCHOR_SEL_AUTO, len(anchors))
        self.printConfigurationResult(status, tag)</code></pre>

      <p><b>Positioning loop</b><p>

      <p>
        Positioning is done in the same way as the anchor configuration: every tag gets its turn. If successful, the position is printed and otherwise an error message gets printed. This is effectively a TDMA approach, where the remote devices won't interfere with each other, unlike if you perform positioning on different devices at the same time, which will not work.
      </p>

      <pre class="prettyprint linenums Arduino"style="padding-left: 20px"><code>void loop(){
  for (int i = 0; i < num_tags; i++){
    coordinates_t position;
    int status = Pozyx.doRemotePositioning(tags[i], &amp;position, dimension, height, algorithm);
    if (status == POZYX_SUCCESS){
    // prints out the result
    printCoordinates(position, tags[i]);
    }else{
      // prints out the error code
      printErrorCode("positioning", tags[i]);
    }
  }
}</code></pre>

      <pre class="prettyprint linenums Python"style="padding-left: 20px"><code>def loop(self):
    """Performs positioning and prints the results."""
    for tag in self.tags:
        position = Coordinates()
        status = self.pozyx.doPositioning(
            position, self.dimension, self.height, self.algorithm, remote_id=tag)
        if status == POZYX_SUCCESS:
            self.printPublishPosition(position, tag)
        else:
            self.printPublishErrorCode("positioning", tag)</code></pre>

      <h3><a>Visualisation</a></h3>

      <p>
        This is identical to the single tag positioning, and the same Processing sketch can be re-used for multitag. The same format is also reused, so if you made your own visualisation using the same format, you should be able to quickly enable multitag in your own application when parsing on the device ID.
      </p>


      <h3><a>Caveats and closing remarks</a></h3>

      <ul style="margin-left:20px">
        <li>Positioning multiple devices at once is performed in a TDMA manner so that they don't interfere with each other. If your devices crash, try adding a small delay between the devices positioning to add robustness.</li>
        <li>Since you're using TDMA, you're splitting the update rate between the different devices. If you'd start with 24 Hz for a single device, you'd get at most 12 Hz per device if you're using two devices on the same UWB settings. 8 Hz if you're using three, and so on. Depending on your usecase, this could be important.</li>
        <li>Having more devices in a line-of-sight of both your master device and your anchors might be harder depending on your use case. Account for this to smartly set up your anchors and master.</li>
        <li>If your area gets larger and you want to maintain a good update rate, adding more anchors which will loosen the constraints on your UWB settings is recommended.</li>
      </ul>

      <p>
        Now that you finished all the tutorials, you should be all set for any usecase implementation that Pozyx is currently capable of. If you have any questions, remarks, or problems with the tutorials, please mail to support@pozyx.io
      </p>

    </div>

    <div class="col-md-12" style="margin-top:100px;">
      <p>
          <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
          Tutorial 4: Multitag positioning
      </p>
    </div>
  </div>
</div>


<script>
 $( document ).ready(function() {
    // add links to function calls
    $("#content").html($("#content").html().replace(/Pozyx\.([a-zA-Z0-9\_]{3,})/g, "Pozyx.<a href='<?php echo site_url('Documentation/Datasheet/Arduino#');?>$1'>$1</a>"));

 });
</script>
