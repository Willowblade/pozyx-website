<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

<style>
.bittable{
      width:700px;
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}

h3 a{
      text-decoration: none;
      color: #5F945F;
}
f
h3 a:hover{
      text-decoration: none;
      color: #5F945F;
}

h4{
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
      font-weight: bold;
      margin-bottom: 0px;
}

.step:hover{
      cursor: pointer;
      text-decoration: underline;
}

.txrx-ol {
    display: block;
    padding-left: 25px;
    counter-reset:li; /* Initiate a counter */
}
.txrx-ol li {
      height: 24px;
      overflow-y: hidden;
      position: relative;
      padding-left: 40px;
    list-style:none; /* Disable the normal item numbering */
    /*background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
    background-position: -5px;
    background-repeat: no-repeat;
    padding-left: 7px;*/
}

.txrx-ol > li:before {
    font-weight: bold;
    content:counter(li); /* Use the counter as content */
    counter-increment:li; /* Increment the counter by 1 */
    /* Position and style the number */
    position:absolute;
    /*left:-2.2em;*/
    left: 0px;
    width:2em;
    /* Some space between the number and the content in browsers that support
       generated content but not positioning it (Camino 2 is one example) */
    margin-right:8px;
    color:#fff;
    font-weight:bold;
    text-align:center;
    background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
}

li.L0, li.L1, li.L2, li.L3,
li.L5, li.L6, li.L7, li.L8
{ list-style-type: decimal !important }

.prettyprint{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint li{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint code{
      font-size: 14px;
      line-height: 17px;
}


</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Example 2: Ready to localize
        </p>

        <h2>Example 2: Ready to localize</h2>

        </div>

      <div id="content" class="col-md-12">

<h3><a>Indoor positioning</a></h3>
      <p>
      In this example we guide you through the process of performing (remote) positioning with the Pozyx system.
      For this example, a <?php echo("<a href='". site_url('store/detail/2') . "'>"); ?>Ready to Localize kit</a> and an Arduino is required (We recommend the use of the Arduino UNO).
      </p>

      <p>Overview of this tutorial:</p>
      <ol style="margin-left:20px">
        <li><a href="#Setup">Setup</a></li>
        <li><a href="#ManualAnchorCalibration">Inputting the anchor coordinates</a></li>
        <li><a href="#Positioning">Positioning</a></li>
        <li><a href="#RemotePositioning">Remote positioning</a></li>
        <li><a href="#Visualization">Visualization</a></li>
      </ol>
      <br>


<h3><a name="Setup">Setup</a></h3>
      <p>
      The Pozyx positioning system requires that the 4 anchors are placed inside the area where you wish to do positioning.
      In the guide '<a href="<?php echo site_url('Documentation/doc_whereToPlaceTheAnchors'); ?>">Where to place the anchors?</a>', it
      is explained how to place the anchors for the best possible positioning accuracy. The 4 rules of thumb were:
      </p>
      <ol style='margin-left: 30px;'>
          <li>Place the anchors high and in line-of-sight of the user.</li>
          <li>Spread the anchors around the user. Never on a straight line!</li>
          <li>Place the anchors vertically with the antenna at the top.</li>
          <li>For 3D positioning: place the anchors at different heights.</li>
      </ol>

            <img src="<?php echo(base_url('assets/images/docs/tutorials/sketch_room.png')); ?>" style="display: block; align:center; margin: auto; margin-bottom: 20px; margin-left: 25px">
      <p>

      Before you install the anchors (with the provided Velcro's or screws) on the walls or ceilings, it is usually a good
      idea to make a small sketch of the room and write down the network ids of the anchors and where you will place them.
      The network id is the 16 bit hexadecimal number printed on the label of each Pozyx device.
      </p>

      <p>
      From the best antenna performance, it is recommended to place the anchors vertically against the walls with the antenna pointing upwards (the white antenna is placed opposite of the usb and the leds). Also, make sure that no heavy metallic objects are close to the antenna as this might degrade performance.
      </p>

      <br>

<h3><a name="ManualAnchorCalibration">Tag configuration</a></h3>

      <p>
      In the Pozyx system, each tag computes its own position onboard which requires the tag to know the coordinates of the anchors.
      In the tutorial automatic anchor calibration, it is explained how the Pozyx system can automatically determine the coordinates of the anchors.
      However, because the automatic anchor calibration can be tricky sometimes, we will start by explaining how we can get the system running by manually measuring the anchor coordinates. Note that any errors in the coordinates of the anchors will directly affect the accuracy of positioning.
      </p>

      <p>
      The Pozyx library includes a useful sketch to configure the positioning parameters and anchor coordinates for a tag. The sketch saves this information to the flash memory such that the tag will remember these settings after reset. Start by opening the sketch pozyx_anchor_configurator that can be found in File > Examples > Pozyx > useful. Upload the sketch to an Arduino equipped with a Pozyx shield and open the serial monitor. Make sure to select both NL/CR in the bottom right corner of the serial monitor.
      </p>

      <p>
      The first time you run this sketch there will be no devices in the device list. After this we are given some options: [add, remove, remove all, localization options]. You can select an option by typing it and pressing enter. For example, in order to add the first anchor with network id 0x1156 and coordinates (0, 0, 0) we type:
      </p>

      <pre class=""><code>'add'+'enter', '1156'+'enter', '0'+'enter', '0'+'enter', '0'+'enter'</code></pre>

      <p>
      We must repeat this process for each anchor. In total, up to 20 anchors can be added to the device list. For the example image above, the device list would look something like this:
      </p>

      <img src="<?php echo(base_url('assets/images/docs/tutorials/anchor_configurator.png')); ?>" style="margin: auto; margin-bottom: 20px; margin-left: 25px">
      <p><b>Fig. The anchor configurator shows 4 anchors with their coordinates.</b></p>
      <br><br>

      <p>
        Finally, we can also change some of the positioning settings by selecting 'localization options'. This allows us to control the follosing things:
      </p>

      <p><b>Algorithm</b></p>
      <ul>
        <li>Least-Squares. This algorithm is described on the Pozyx website in the documentation. It is a simple algorithm that only uses UWB-measurments and that cannot handle NLOS range measurements well.</li>
        <li>UWB-only (Default value). This algorithm estimates the position using UWB measurments only. It is specifically designed to deal with NLOS-measurements.</li>
      </ul>

      <p><b>Dimension</b></p>
      <ul>
        <li>2D (Default value). In two dimensional mode, the x and y coordinates are estimated. It is expected that all tags and anchors are located in the same horizontal plane.</li>
        <li>2,5D. In this mode, the x and y coordinates are estimated. However, anchors and tags are not required to be located in the same horizontal plane. For this mode it is necessary that the z-coordinates of the anchors and tag are known. For the tag it must be stored in the <?php register_url("POYX_POS_Z"); ?> register. In general this mode results in superior positioning accuracy as compared to full 3D when the anchors cannot be placed well. It is especially usefull when the tag is mounted on a robot or VR-helmet.</li>
        <li>3D. In three dimensional mode, the x,y and z coordinates are estimated. In order to obtain a good vertical accuracy, it is required to place the anchors at different heights. Check out the documentation for more information on anchor placement.</li>
      </ul>

      <p><b>Number of anchors</b></p>
      <p>Specify how many anchors should be used to compute the position. By default 4 anchors are selected from the device list.
      More anchors means a lower update rate, but will increase the accuracy and robustness of the position estimate.</p>

      <p><b>Anchor selection</b></p>
      <ul>
      <li>Manual. If you select this option you can enter the network ids of the anchors that must be used for positioning.</li>
      <li>Automatic. Anchors are automatically selected from the internal anchor list are used to make the selection.
      Currently, the selection algorithm simply selects the first anchors from the device list. This is expected to change in the following
      firmware update.</li>
      </ul>



<h3><a name="Positioning">Positioning</a></h3>

      <p>
      With the anchor coordinates configured on the tag, we can begin positioning.
      Mount the Pozyx tag on the Arduino and upload the example Arduino
      sketch (File > Examples > Pozyx > ready_to_localize).

      The code can be found in the very short <code>loop()</code> function:

      </p>

<pre class="prettyprint linenums" style="padding-left: 20px"><code>void loop(){

  coordinates_t position;

  int status = Pozyx.doPositioning(&amp;position);

  if (status == POZYX_SUCCESS){
    printCoordinates(position);
  }
}

void printCoordinates(coordinates_t coor){

  Serial.print("x_mm: ");
  Serial.print(coor.x);
  Serial.print("\t y_mm: ");
  Serial.print(coor.y);
  Serial.print("\t z_mm: ");
  Serial.print(coor.z);
  Serial.println();
}</code></pre>

      <p>
      That's it for positioning!
      </p>

<h3><a name="RemotePositioning">Remote positioning</a></h3>
    <p>The sketch above will work for a single tag, but it won't work for multiple tags (Pozyx dev kit).
    With multiple tags, positioning must be performed in an orderly fashion such that the UWB signals don't interfere with one-another.
    To make this work, we need a master tag that tells each remote tag when to do positioning. In response, the remote tag will perform
    positioning and send the result to the master tag. The sketch for the master tag looks really simple as follows:
    </p>

<pre class="prettyprint linenums" style="padding-left: 20px"><code>void loop(){

  coordinates_t position;

  int status = Pozyx.doRemotePositioning(remote_tag_id, &amp;position);

  if (status == POZYX_SUCCESS){
    printCoordinates(position);
  }
}</code></pre>

    <p>
    For this to work, it is required to configure each of the remote tags as was described above. The remote tags do not require an Arduino to work.
    It can be seen that, by performing positioning one after the other, the update rate for each tag will be reduced.
    </p>

<h3><a name="Visualization">Visualization</a></h3>

      <p>
      Printing out coordinates to a terminal isn't all that sexy, and that's where Processing comes in.
      Using Processing, we can <!--easily -->setup a sketch that reads out the coordinates from the serial port and
      draws them on the screen.
      Please <a href="https://github.com/pozyxLabs/Pozyx-processing">download the processing sketch</a> and edit the serial port such that it matches with your Arduino.
      Press play and you should see all the anchors and the tag. Note that the Processing sketch reads out
      all the data from the serial port which is required to be in a fixed format. More specifically, the
      Processing sketch expects the anchor data prefixed with the string "ANCHOR" as follows:</p>
      <pre>ANCHOR,network_id,posx,posy,posz</pre>
      <p>And the position data prefixed with "POS" as follows:</p>
      <pre>POS,network_id,posx,posy,posz,errx,erry,errz,errXY,errXZ,errYZ,range1,rss1,...</pre>
      <p>The result should look something like this:</p>

<img src="<?php echo(base_url('assets/images/docs/tutorials/ready_to_localize.png')); ?>" style="width: 50%; height: 50%; margin: auto; margin-bottom: 20px; margin-left: 25px">

      <br>

      <p>
      This concludes the ready to localize example.
        In the <a href="<?php echo site_url('Documentation/Tutorials/orientation_3D'); ?>">next example</a>, we look at the IMU to obtain the 3D orientation of the Pozyx tag.
      </p>

      </div>

      <div class="col-md-12" style="margin-top:100px;">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
            Example 2: Ready to localize
        </p>

        </div>

    </div>
</div>

<!--
<script>
 $( document ).ready(function() {
    // add links to function calls
    $("#content").html($("#content").html().replace(/Pozyx\.([a-zA-Z0-9\_]{3,})/g, "Pozyx.<a href='<?php echo site_url('Documentation/Datasheet/Arduino#');?>$1'>$1</a>"));

 });
</script>
-->
