<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

<style>
.Arduino{
  <?php if($type!="Arduino") echo "display:none;"?>
}

.Python{
  <?php if($type!="Python") echo "display:none;"?>
}

.bittable{
      width:700px;
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}

h3 a{
      text-decoration: none;
      color: #5F945F;
}

h3 a:hover{
      text-decoration: none;
      color: #5F945F;
}

h4{
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
      font-weight: bold;
      margin-bottom: 0px;
}

.step:hover{
      cursor: pointer;
      text-decoration: underline;
}

.txrx-ol {
    display: block;
    padding-left: 25px;
    counter-reset:li; /* Initiate a counter */
}
.txrx-ol li {
      height: 24px;
      overflow-y: hidden;
      position: relative;
      padding-left: 40px;
    list-style:none; /* Disable the normal item numbering */
    /*background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
    background-position: -5px;
    background-repeat: no-repeat;
    padding-left: 7px;*/
}

.txrx-ol > li:before {
    font-weight: bold;
    content:counter(li); /* Use the counter as content */
    counter-increment:li; /* Increment the counter by 1 */
    /* Position and style the number */
    position:absolute;
    /*left:-2.2em;*/
    left: 0px;
    width:2em;
    /* Some space between the number and the content in browsers that support
       generated content but not positioning it (Camino 2 is one example) */
    margin-right:8px;
    color:#fff;
    font-weight:bold;
    text-align:center;
    background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
}

li.L0, li.L1, li.L2, li.L3,
li.L5, li.L6, li.L7, li.L8
{ list-style-type: decimal !important }

.prettyprint{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint li{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint code{
      font-size: 14px;
      line-height: 17px;
}

</style>

<div class="container">
      <!-- Example row of columns -->
  <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

    <div class="col-md-12">
      <p>
          <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
          Tutorial 3: Orientation 3D
      </p>

      <h2>Tutorial 3: Orientation 3D</h2>

      <p><a class="Python" href="<?php echo site_url('Documentation/Tutorials/orientation_3D/Arduino'); ?>">Go to Arduino version</a><a class="Arduino" href="<?php echo site_url('Documentation/Tutorials/orientation_3D/Python'); ?>">Go to Python version</a></p>

    </div>

    <div id="content" class="col-md-12">
      <p>
        In this example, the sensor data is read from the pozyx shield (either the shield mounted on the Arduino, or a remote shield). To visualize all the data, we use Processing. Below a screenshot of the Processing program showing all the sensor data graphically.
      </p>

      <p class="Arduino">
        This example requires one or two pozyx shields and one Arduino.
        Open up the orientation 3D example in the Arduino IDE under File > Examples > Pozyx > orientation_3D.
      </p>

      <p class="Python">
        This example requires one or two pozyx shields. If all tools are installed, open up orientation_3D.py in the Pozyx library’s tutorial folder. Probably, the path to this file will be "Downloads/Pozyx-Python-library/tutorials/orientation_3D.py". You can run the script from either command line or a text editor that allows running the scripts as well. If you're on Windows and have installed Python, you might be able to simply double-click it. If you you get an error, you likely forgot to install pythonosc, you can do this easily using <code>pip install python-osc</code>.
      </p>

      <p>
        Open up the pozyx_orientation3D.pde sketch in Processing, as this example will be directly visualized.
      </p>

      <img src="<?php echo(base_url('assets/images/docs/tutorials/remote_imu_processing.png')); ?>" style="width: 50%; height: 50%; margin: auto; margin-bottom: 20px; margin-left: 25px">

      <h3><a>Plug and play</a></h3>

      <p class="Arduino">
        In Processing, make sure <code>serial</code> is set to true, and that your <code>serialPort</code> matches the one used by your Arduino. Upload the Arduino sketch, and, when this is done, press the play button in Processing.
      </p>

      <p class="Python">
        In Processing, make sure <code>serial</code> is set to false. Unless you need port 8888 for something else, you can leave both the Python script and Processing sketch intact. The data will automatically be sent using OSC on port 8888.
      </p>

      <p>
        You should be able to physically rotate your Pozyx now and directly see it rotate in Processing as well. There are plots on the side and at the top, but what do they all mean?
      </p>

      <h3>Understanding the sensor data</h3>
      <ul>

            <li><b>Acceleration (g):</b> <br>
            the acceleration is measured along 3 axes (i.e., in 3 directions) which is shown by three different colors on the plot.
            The acceleration is expressed in g's (from gravity).
            1g is exactly the acceleration from earth's gravitation pull (1g = 9.81m/s2). Because the acceleration from gravity
            is always present and equal to 1g it can be used to determine how the device is tilted or pitched.
            Try rotating the Pozyx device by 90 degrees (slowly) and you will see the the acceleration change from one axis
            to the other.
            </li>
            <li><b>Magnetic field strength (µT):</b><br>
            the magnetic field strength is also measured along 3 axes and is expressed in µT. It can be used to measure
            the earth's magnetic field which varies between 30 and 60µT over the earth's surface. The measured magnetic
            field strength can be used to estimate the magnetic north (similar to a compass).
            However, magnetic fields exist wherever electric current flows or
            magnetic materials are present. Because of this, they will influence the sensor data and it will become impossible
            to determine the magnetic north exactly. Try holding a magnet or something iron close to the pozyx device and you will
            see the magnetic field strength fluctuate.
            </li>
            <li><b>Angular velocity (deg/s):</b><br>
            The angular velocity is measured by the gyroscope and measures how fast the device rotates around itself. By integrating
            the angular velocity it is possible to obtain the angles of the device. However, due to drift this method does not give
            accurate results for a prolonged time. What is drift you wonder? Well if the device is standing still, the angular
            velocity should be exactly equal to zero. This is not the case however, it will be slightly different and this
            error will accumulate over time when integrating to angles.
            </li>
            <li><b>3D Orientation:</b><br>
            The 3D orientation is shown by the 3D model in the middle. The orientation is computed by combining the sensor data
            from all three sensors together. By combining all sensors it is possible to overcome the limitations of each sensor
            separately. The 3D orientation can be expressed in Euler angles: yaw, pitch, roll or in quaternions.
            Quaternions are a mathematical representation using 4 numbers.
            In many situations, quaternions are preferred because they do not have singularity problems like the Euler angles.
            </li>

            <li><b>Gravity vector (g): </b><br>
            If the pozyx device is standing still, the acceleration is exactly equal to the gravity.
            The gravity vector is shown by the black line and is always pointing down. Notice that even when moving
            the device (which introduces an additional acceleration) the gravity vector still points down. This is due to
            the fusion algorithm that can separate gravity from an arbitrary acceleration;
            </li>

            <li><b>Linear acceleration in body coordinates (g):</b><br>
            The linear acceleration is the acceleration that remains after the gravity has been removed. When you
            hold the device horizontal, pointed forward, and shake it from left to right the circle will also move from left to right in the plot.
            However, if you rotate the device by 90 degrees
            and shake it again from left to right, the circle will now move in a different direction. This is because the linear acceleration
            is expressed in body coordinates, i.e., relative to the device. Note that all the above sensor data is expressed in
            body coordinates.
            </li>
            <li><b>Linear acceleration in world coordinates (g):</b><br>
            Once the orientation of the device is known, it is possible to express the acceleration in world coordinates.
            By doing this, the rotation of the device no longer affects the linear acceleration in the plot.
            </li>
      </ul>

      <h3><a>The code explained</a></h3>

      <p>
        We'll now go over the code that's needed to retrieve all sensor data. Looking at the code's parameters, we can see that we can once again use a remote Pozyx for this. This means that you could for example attach a Pozyx to a ball, and watch the ball's spin directly on your screen as well with the Pozyx.
      </p>

      <p><b>Imports and setup</b><p>

      <p class="Arduino">
        The imports are the default Pozyx library imports and <code>Wire.h</code> for the I²C functionality encountered in the previous tutorials. <br />
        The Pozyx and serial connection are then initialized, and the last measured time is set.
      </p>

      <p class="Python">
        We import the pypozyx library and the IMU interrupt flag from its bitmasks, and pythonosc so that we can send the sensor data to Processing over OSC. The addition of the default <code>time</code> library import allows us to measure the time between different measurements.<br />
        We initialize the <code>PozyxSerial</code> and UDP socket over which we'll send the data, and the <code>setup</code> then sets the last measured time right.
      </p>

      <pre class="prettyprint linenums Arduino"style="padding-left: 20px"><code>#include &lt;Pozyx.h&gt;
#include &lt;Pozyx_definitions.h&gt;
#include &lt;Wire.h&gt;</code></pre>

      <pre class="prettyprint linenums Python"style="padding-left: 20px"><code>from time import time

from pypozyx import *
from pypozyx.definitions.bitmasks import POZYX_INT_MASK_IMU
from pythonosc.osc_message_builder import OscMessageBuilder
from pythonosc.udp_client import SimpleUDPClient</code></pre>

      <pre class="prettyprint linenums Arduino"style="padding-left: 20px"><code>void setup()
{
  Serial.begin(115200);

  if(Pozyx.begin(false, MODE_INTERRUPT, POZYX_INT_MASK_IMU) == POZYX_FAILURE){
    Serial.println("ERROR: Unable to connect to POZYX shield");
    Serial.println("Reset required");
    delay(100);
    abort();
  }

  if(!remote)
    remote_id = NULL;

  last_millis = millis();
  delay(10);
}</code></pre>

      <pre class="prettyprint linenums Python"style="padding-left: 20px"><code>def setup(self):
    """There is no specific setup functionality"""
    self.current_time = time()</code></pre>

    <p><b>Loop</b><p>

    <p>
      The main loop deserves to be elaborated on. The Pozyx's IMU sensors trigger an interrupt flag when there is new data available, and in the code we wait for this flag to trigger explicitly.
    </p>

    <p class="Arduino">
      When it does, or when we read the sensor data remotely, the code will read out all sensor data and it's calibration status from the (remote) Pozyx, and then print it serially as comma separated values, which will be converted to standard units and interpreted by Processing. This is done in <code>getAllRawSensorData</code>, where the sensor registers are read directly and their values are stored in a <code>sensor_raw_t</code> struct. There is also a <code>getAllSensorData</code> function available, where this raw sensor data gets converted to standard units and is stored in a <code>sensor_data_t</code> struct, but we choose the former because we prefer this conversion to be done in Processing, on a powerful CPU, offsetting the Arduino's processing and allowing a faster retrieval rate of this IMU data.
    </p>

    <p class="Arduino">
      When it doesn't, it tries again.
    </p>

    <p class="Python">
      When <code>checkForFlag</code> returns <code>POZYX_SUCCESS</code>, meaning that <code>POZYX_INT_MASK_IMU</code> was raised and new IMU data is available, or when we're retrieving sensor data remotely, all sensor data and the calibration status will be read from the (remote) Pozyx and packed in a OSC message, which is then interpreted by the Processing sketch. You can see that there is a <code>SensorData</code> object available for this, which automatically converts the sensor data to its respective standard units.
    </p>

    <pre class="prettyprint linenums Arduino"style="padding-left: 20px"><code>void loop(){
  sensor_raw_t sensor_raw;
  uint8_t calibration_status = 0;
  int dt;
  int status;
  if(remote){
     status = Pozyx.getRawSensorData(&amp;sensor_raw, remote_id);
     status &amp;= Pozyx.getCalibrationStatus(&amp;calibration_status, remote_id);
    if(status != POZYX_SUCCESS){
      return;
    }
  }else{
    if (Pozyx.waitForFlag(POZYX_INT_STATUS_IMU, 10)){
      Pozyx.getRawSensorData(&amp;sensor_raw);
      Pozyx.getCalibrationStatus(&amp;calibration_status);
    }else{
      uint8_t interrupt_status = 0;
      Pozyx.getInterruptStatus(&amp;interrupt_status);
      return;
    }
  }

  dt = millis() - last_millis;
  last_millis += dt;
  // print time difference between last measurement in ms, sensor data, and calibration data
  Serial.print(dt, DEC);
  Serial.print(",");
  printRawSensorData(sensor_raw);
  Serial.print(",");
  // will be zeros for remote devices as unavailable remotely.
  printCalibrationStatus(calibration_status);
  Serial.println();
}</code></pre>

    <pre class="prettyprint linenums Python"style="padding-left: 20px"><code>def loop(self):
    """Gets new IMU sensor data"""
    sensor_data = SensorData()
    calibration_status = SingleRegister()
    if self.remote_id is not None or self.pozyx.checkForFlag(POZYX_INT_MASK_IMU, 0.01) == POZYX_SUCCESS:
        status = self.pozyx.getAllSensorData(sensor_data, self.remote_id)
        status &amp;= self.pozyx.getCalibrationStatus(calibration_status, self.remote_id)
        if status == POZYX_SUCCESS:
            self.publishSensorData(sensor_data, calibration_status)</code></pre>

    <p>
      The calibration status gives information about the quality of the sensor orientation data. When the system is not fully calibrated, the orientation estimation can be bad. The calibration status is a 8-bit variable where every 2 bits represent a different piece of calibration info.
    </p>

    <h3><a>What's next?</a></h3>

    <p>
      You're at the end of the standard tutorials that introduce you to all of Pozyx's basic functionality: ranging, positioning, and its IMU. If you haven't already done so and have the number of devices necessary, you can read the <a href=<?php echo site_url('Documentation/Tutorials/multitag_positioning/'.$type); ?>>multitag tutorial</a>. When you start work on your own prototype, don't be afraid to delve into our documentation which should suffice for all your needs.
    </p>

    <p class="Arduino">
      There's also the <a href=<?php echo site_url('Documentation/Tutorials/chat_room'); ?>>chat room tutorial</a>, for which you will need two Arduinos and two Pozyxs, in which you will build a small chat room where you use Pozyx to communicate between two computers. This opens the door to adding real communication and control to two Arduino-enabled Pozyxs, as you can send and interpret Arduino commands this way as well.
    </p>

    </div>

    <div class="col-md-12" style="margin-top:100px;">
      <p>
          <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
          Tutorial 3: Orientation 3D
      </p>
    </div>
  </div>
</div>


<script>
 $( document ).ready(function() {
    // add links to function calls
    $("#content").html($("#content").html().replace(/Pozyx\.([a-zA-Z0-9\_]{3,})/g, "Pozyx.<a href='<?php echo site_url('Documentation/Datasheet/Arduino#');?>$1'>$1</a>"));

 });
</script>
