<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

<style>
.Arduino{
  <?php if($type!="Arduino") echo "display:none;"?>
}

.Python{
  <?php if($type!="Python") echo "display:none;"?>
}

.bittable{
      width:700px;
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}

h3 a{
      text-decoration: none;
      color: #5F945F;
}

h3 a:hover{
      text-decoration: none;
      color: #5F945F;
}

h4{
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
      font-weight: bold;
      margin-bottom: 0px;
}

.step:hover{
      cursor: pointer;
      text-decoration: underline;
}

.txrx-ol {
    display: block;
    padding-left: 25px;
    counter-reset:li; /* Initiate a counter */
}
.txrx-ol li {
      height: 24px;
      overflow-y: hidden;
      position: relative;
      padding-left: 40px;
    list-style:none; /* Disable the normal item numbering */
    /*background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
    background-position: -5px;
    background-repeat: no-repeat;
    padding-left: 7px;*/
}

.txrx-ol > li:before {
    font-weight: bold;
    content:counter(li); /* Use the counter as content */
    counter-increment:li; /* Increment the counter by 1 */
    /* Position and style the number */
    position:absolute;
    /*left:-2.2em;*/
    left: 0px;
    width:2em;
    /* Some space between the number and the content in browsers that support
       generated content but not positioning it (Camino 2 is one example) */
    margin-right:8px;
    color:#fff;
    font-weight:bold;
    text-align:center;
    background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
}

li.L0, li.L1, li.L2, li.L3,
li.L5, li.L6, li.L7, li.L8
{ list-style-type: decimal !important }

.prettyprint{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint li{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint code{
      font-size: 14px;
      line-height: 17px;
}

</style>

<div class="container">
      <!-- Example row of columns -->
  <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

    <div class="col-md-12">
      <p>
          <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
          Tutorial 1: Ready to range
      </p>
      <p><a href="<?php echo site_url('Documentation/Tutorials/ready_to_range/Arduino'); ?>">Arduino</a> <a href="<?php echo site_url('Documentation/Tutorials/ready_to_range/Python'); ?>">Python</a></p>

      <h2>Tutorial 1: Ready to range</h2>
    </div>

    <div id="content" class="col-md-12">

      <h3><a>Ready to range</a></h3>
      <p>
        This is the first general Pozyx tutorial. If you haven’t read the <a href="<?php echo site_url('Documentation/Tutorials/getting_started/'.$type); ?>">getting started</a>, please do so and install the necessary tools and libraries.
      <p>
      <p class="Arduino">
        This example requires two Pozyx devices and one supported Arduino device. Remember that we recommend the Arduino Uno Rev. 3. If all tools are installed, open up the ready to range example in the Arduino IDE under File > Examples > Pozyx > ready_to_range.
      </p>

      <p class="Python">
        This example requires two Pozyx devices. If all tools are installed, open up ready_to_range.py in the Pozyx library’s tutorial folder. Probably, the path to this file will be "Downloads/Pozyx-Python-library/tutorials/ready_to_range.py". You can run the script from either command line or a text editor that allows running the scripts as well. If you're on Windows and have installed Python, you might be able to simply double-click it.
      </p>

      <p>
        In this example, the distance between the two devices is measured and the onboard LEDs  will show in which range the devices are to each other. The destination Pozyx's LEDs will be doing the same, commanded by our local Pozyx. The LEDs' changes with respect to the distance is sketched in the figure below:
      </p>

      <img src="<?php echo(base_url('assets/images/docs/tutorials/sketch_ranging.png')); ?>" style="margin: auto; margin-bottom: 20px; margin-left: 25px">

      <h3><a>Plug and play</a></h3>

      <p>
        To see the tutorial in action before we delve into the code, the parameters will need to be changed to match your destination device's ID.
      </p>

      <p class="Arduino">
        You also have to set the Arduino IDE’s serial monitor’s baudrate to 115200. At the top of the Arduino sketch, we find the sketch’s parameters:
      </p>

      <p class="Python">
        At the bottom of the Python script, in the ifmain structure, we find the script’s parameters. You can see that the first one is the serial port address of your device. Finding the serial port is described in getting started, but you can automate the port selection using serial_port = get_serial_ports()[0].device. This automatic serial port selection will be used in the next examples.
      </p>

      <pre class="prettyprint linenums:21 Arduino"style="padding-left: 20px"><code>uint16_t destination_id = 0x6670; // the network ID of the ranging destination
signed int range_step_mm = 1000;  // distance between each LED lighting up.

uint16_t remote_id = 0x605D;      // the network ID of the remote device
bool remote = false;              // whether to use the given remote device for ranging</code></pre>

      <pre class="prettyprint linenums:68 Python"style="padding-left: 20px">
<code>if __name__ == "__main__":
    port = 'COM1'            # COM port of the Pozyx device

    remote_id = 0x605D       # the network ID of the remote device
    remote = False           # whether to use the given remote device for ranging
    if not remote:
        remote_id = None

    destination_id = 0x1000  # network ID of the ranging destination
    range_step_mm = 1000     # distance between each LED lighting up.</code></pre>

    <p>
      You’ll have to change the <code>destination_id</code> parameter to the ID of your destination Pozyx. Optionally, you can change the <code>range_step_mm</code> as well, which will make the distance range indicated by the LEDs either higher or lower depending on your change. <code>remote_id</code> and <code>remote</code> will allow measuring the distance between two remote Pozyx devices, and will be explained at the end of this tutorial, so don't change those for now.<br />
      If you’ve correctly changed your device ID, run the example! You should see that the LEDs of your two Pozyx devices now burn identically, and change when you move them closer or further apart. The output you’re receiving will look like this:
    </p>
    <p>
      16691ms, 7564mm, -93dBm<br />
      16753ms, 7718mm, -91dBm<br />
      16830ms, 7779mm, -92dBm<br />
      etc...
    </p>

    <p>
      The first value is the device’s timestamp of the range measurement. The second value then is the measured distance between both devices. The third value, expressed in dBm, signifies the signal strength, varying between -79 and -103 dB typically. You might be reading either:
    </p>

    <p>
      0ms, 0mm, 0dB<br />
      or<br />
      ERROR: ranging<br />
    </p>

    <p>
      If this is the case, you probably miswrote the ID (did you write it with the 0x prefix?) or your device is not in range currently. As the LEDs will only indicate differences of a meter by default, keeping the distance between a couple of meters for this first try is recommended. If the error persists despite having put in everything correctly, check out the <a href="<?php echo site_url('Documentation/Tutorials/troubleshoot_basics/'.$type); ?>">troubleshooting guide</a>.
    </p>

    <p class="Arduino">If you’re getting gibberish, you probably forgot to set your serial monitor’s baudrate to 115200.</p>

    <h3><a>The code explained</a></h3>
    <p>
      Now that you’ve had a taste of what exactly this example provides, let’s look at the code that provides this functionality. We’ll begin with the imports and the setup functions, and then go into the continuous loop.
    </p>
    <p><b>Imports</b></p>

    <pre class="prettyprint linenums:13 Arduino"style="padding-left: 20px"><code>#include &lt;Pozyx.h&gt;
#include &lt;Pozyx_definitions.h&gt;
#include &lt;Wire.h&gt;</code></pre>

    <pre class="prettyprint linenums:13 Python"style="padding-left: 20px"><code>from pypozyx import *</code></pre>

    <p class="Arduino">
      We can see that the sketch requires both “Pozyx.h” and
    </td> “Pozyx_definitions.h”, and the "Wire.h" file for the I2C.. “Pozyx.h” provides the library’s functionality, “while Pozyx_definitions.h” provides constants such as <code>POZYX_SUCCESS</code> and <code>POZYX_FAILURE</code>, register addresses… these are useful to keep your code working with both current, previous, and future versions of the library.
    </p>

    <p class="Python">
      We import everything from pypozyx. This import is made to provide you with the Pozyx constants, data containers, and the <code>PozyxSerial</code> class. This allows for general Pozyx use, as you have every library function available to you this way through PozyxSerial. If you need access to the register addresses, you can <code>import pypozyx.constants.registers</code>. For the bitmasks, <code>import pypozyx.constants.bitmasks</code>.
    </p>
    <p class="Python"><b>Emulating the Arduino flow</b></p>

    <p class="Python">
      As a Python script isn't executed as an Arduino sketch, we emulate this with the following code. The setup function is run once, after which the loop function is executed continuously. We also pass the PozyxSerial object as a parameter, which makes the Pozyx not exclusive to this class if we want to add other functionality elsewhere. The other parameters defined above are passed, this has the purpose of not making this ReadyToRange class exclusive to this script, and stimulates reuse.
    </p>

    <pre class="prettyprint linenums Python"style="padding-left: 20px"><code>pozyx = PozyxSerial(port)
      r = ReadyToRange(pozyx, destination_id, range_step_mm, remote_id)
      r.setup()
      while True:
          r.loop()</code></pre>

    <p><b>Setup</b></p>
    <p class="Arduino">
      This function will run only once, when the Arduino is powered on or reset. It sets up the serial communication at 115200 baud, so be sure to set your Serial Monitor to this same baudrate if you hadn’t already. The Pozyx is then initialized by calling Pozyx.begin(), which checks whether the Pozyx is connected and working properly.
    </p>

    <p class="Python">
      This function will be run only once at the script’s execution, as you can see in the ifmain section, after this the loop will be continuously called. The PozyxSerial is initialized with the port we supplied in the parameters, so it’s important to provide the correct port, as you’ll notice when trying to run the code with an incorrect port. This initialization also checks for correct operation of the connected Pozyx.
    </p>

    <pre class="prettyprint linenums:29 Arduino"style="padding-left: 20px"><code>void setup(){
  Serial.begin(115200);

  if(Pozyx.begin() == POZYX_FAILURE){
    Serial.println("ERROR: Unable to connect to POZYX shield");
    Serial.println("Reset required");
    delay(100);
    abort();
  }
  // setting the remote_id back to NULL will use the local Pozyx
  if (!remote){
    remote_id = NULL;
  }
  Serial.println("------------POZYX RANGING V1.0------------");
  Serial.println("NOTES:");
  Serial.println("- Change the parameters:");
  Serial.println("\tdestination_id (target device)");
  Serial.println("\trange_step (mm)");
  Serial.println();
  Serial.println("- Approach target device to see range and");
  Serial.println("led control");
  Serial.println("------------POZYX RANGING V1.0------------");
  Serial.println();
  Serial.println("START Ranging:");

  // make sure the pozyx system has no control over the LEDs, we're the boss
  uint8_t led_config = 0x0;
  Pozyx.setLedConfig(led_config);
  // do the same with the remote device
  Pozyx.setLedConfig(led_config, destination_id);
}</code></pre>

<pre class="prettyprint linenums:25 Python"style="padding-left: 20px"><code>def setup(self):
    """Sets up both the ranging and destination Pozyx's LED configuration"""
    print("------------POZYX RANGING V1.0 - -----------")
    print("NOTES: ")
    print(" - Change the parameters: ")
    print("\tdestination_id(target device)")
    print("\trange_step(mm)")
    print()
    print("- Approach target device to see range and")
    print("led control")
    print("- -----------POZYX RANGING V1.0 ------------")
    print()
    print("START Ranging: ")

    # make sure the local/remote pozyx system has no control over the LEDs.
    led_config = 0x0
    self.pozyx.setLedConfig(led_config, self.remote_id)
    # do the same for the destination.
    self.pozyx.setLedConfig(led_config, self.destination_id)</code></pre>

    <p>
      Depending on the value of the remote parameters, we set the remote_id to use either the local or a remote Pozyx. After initializing the Pozyx, we configure both the local and destination Pozyx’s LEDs to be in our control instead of displaying system status, which is their default behavior. This is done by setting the <code>POZYX_CONFIG_LEDS</code> register’s value to 0b00000000 on both devices. While the library’s functionality hides away a lot of low-level functionality, it’s important to know how to access registers directly low-level and to find your way in the <a href="<?php echo site_url('Documentation/Datasheet/RegisterOverview'); ?>">register overview</a> for your own personal goals. These core low-level functions are:
    </p>

    <ul class="Arduino">
      <li><code>static int regRead(uint8_t reg_address, uint8_t *pData, int size);</code></li>
      <li><code>static int regWrite(uint8_t reg_address, const uint8_t *pData, int size);</code></li>
      <li><code>static int regFunction(uint8_t reg_address, uint8_t *params, int param_size, uint8_t *pData, int size);</code></li>
    </ul>
    <p class="Arduino">and the remote equivalents</p>
    <ul class="Arduino">
      <li><code>static int remoteRegWrite(uint16_t destination, uint8_t reg_address, uint8_t *pData, int size);</code></li>
      <li><code>static int remoteRegRead(uint16_t destination, uint8_t reg_address, uint8_t *pData, int size);</code></li>
      <li><code>static int remoteRegFunction(uint16_t destination, uint8_t reg_address, uint8_t *params, int param_size, uint8_t *pData, int size);</code></li>
    </ul>

    <ul class="Python">
      <li><code>getRead(address, data, remote_id=None)</code></li>
      <li><code>setWrite(address, data, remote_id=None)</code></li>
      <li><code>useFunction(function, params=None, data=None, remote_id=None)</code></li>
    </ul>
    <p class="Python">You can see that these work both locally and remotely through the <code>remote_id</code> parameter, so you can access the registers on remote devices as well.</p>

    <p><b>Loop</b></p>
    <p>
      Let’s move on to the <code>loop</code> function, which will run continuously and executes the ranging functionality.
    </p>

    <pre class="prettyprint linenums:61 Arduino"style="padding-left: 20px"><code>void loop(){
  device_range_t range;
  int status = 0;

  // let's perform ranging with the destination
  if(!remote)
    status = Pozyx.doRanging(destination_id, &range);
  else
    status = Pozyx.doRemoteRanging(remote_id, destination_id, &range);

  if (status == POZYX_SUCCESS){
    Serial.print(range.timestamp);
    Serial.print("ms, ");
    Serial.print(range.distance);
    Serial.print("mm, ");
    Serial.print(range.RSS);
    Serial.println("dB");

    // now control some LEDs; the closer the two devices are, the more LEDs will be lit
    if (ledControl(range.distance) == POZYX_FAILURE){
      Serial.println("ERROR: setting (remote) leds");
    }
  }
  else{
    Serial.println("ERROR: ranging");
  }
}</code></pre>

    <pre class="prettyprint linenums:45 Python"style="padding-left: 20px"><code>def loop(self):
    """Performs ranging and sets the LEDs accordingly"""
    device_range = DeviceRange()
    status = self.pozyx.doRanging(self.destination_id, device_range, self.remote_id)
    if status == POZYX_SUCCESS:
        print(device_range)
        if self.ledControl(device_range.distance) == POZYX_FAILURE:
            print("ERROR: setting (remote) leds")
    else:
        print("ERROR: ranging")</code></pre>

    <p>
      In the loop function, ranging is performed with the Pozyx library’s <code>doRanging</code> function. The range information will be contained in <code>device_range</code>.
    </p>

    <p class="Arduino">
      When we want to perform ranging on a remote Pozyx, we use <code>doRemoteRanging</code> instead. The <code>device_range</code> variable is an instance of the <code>device_range_t</code> structure. Looking at its definition (in Pozyx.h) we can see that the device range consists of three variables providing information about the measurement. By using the <code>__attribute__((packed))</code> attribute, the struct’s bytes are packed together.
    </p>

    <pre class="prettyprint Arduino"style="padding-left: 20px"><code>typedef struct __attribute__((packed))_device_range {
        uint32_t timestamp;
        uint32_t distance;
        int8_t RSS;
    }device_range_t;</code></pre>

    <p class="Python">
      The <code>device_range</code> variable is an instance of the <code>DeviceRange</code> class. Looking at its definition (in device.py in the pypozyx/structures folder, we can see that the device range consists of three variables providing information about the measurement. The library also provides human-readable string conversion of its important data structures, and <code>print(device_range)</code> prints the range data in our desired format. The ByteStructure parent class provides functionality that makes sure the structure's bytes are packed together.
    </p>

    <pre class="prettyprint Python"style="padding-left: 20px"><code>class DeviceRange(ByteStructure):
    def __init__(self, timestamp=0, distance=0, RSS=0):
        """Initializes the DeviceRange object."""
        self.timestamp = timestamp
        self.distance = distance
        self.RSS = RSS</code></pre>

    <p>
      In a wireless system, we don’t want to waste transmission time on unused  padding bytes, after all. The library provides similar structures for all relevant data structures related to Pozyx, which you'll encounter if you follow the other tutorials and get into further reading. The measurement consists of the timestamp in milliseconds, the measured distance in mm and the RSS-value (the received signal strength) expressed in dBm. Depending on the UWB settings, -103 dBm is more or less the lowest RSS at which reception is possible, and -80 is a normal value for devices close to each other. Upon a successful measurement, these three variables are printed and the LEDs are updated with the <code>ledControl</code> function.
    </p>

    <p><b>ledControl</b></p>

    <pre class="prettyprint linenums:89 Arduino"style="padding-left: 20px"><code>int ledControl(uint32_t range){
  int status = POZYX_SUCCESS;
  // set the LEDs of the pozyx device
  status &= Pozyx.setLed(4, (range < range_step_mm), remote_id);
  status &= Pozyx.setLed(3, (range < 2*range_step_mm), remote_id);
  status &= Pozyx.setLed(2, (range < 3*range_step_mm), remote_id);
  status &= Pozyx.setLed(1, (range < 4*range_step_mm), remote_id);

  // set the LEDs of the destination pozyx device
  status &= Pozyx.setLed(4, (range < range_step_mm), destination_id);
  status &= Pozyx.setLed(3, (range < 2*range_step_mm), destination_id);
  status &= Pozyx.setLed(2, (range < 3*range_step_mm), destination_id);
  status &= Pozyx.setLed(1, (range < 4*range_step_mm), destination_id);

  // status will be zero if setting the LEDs failed somewhere along the way
  return status;
}</code></pre>

    <pre class="prettyprint linenums:56 Python"style="padding-left: 20px"><code>def ledControl(self, distance):
    """Sets LEDs according to the distance between two devices"""
    status = POZYX_SUCCESS
    ids = [self.remote_id, self.destination_id]
    # set the leds of both local/remote and destination pozyx device
    for id in ids:
        status &= self.pozyx.setLed(4, (distance < range_step_mm), id)
        status &= self.pozyx.setLed(3, (distance < 2 * range_step_mm), id)
        status &= self.pozyx.setLed(2, (distance < 3 * range_step_mm), id)
        status &= self.pozyx.setLed(1, (distance < 4 * range_step_mm), id)
    return status</code></pre>

    <p>
      The <code>ledControl</code> function turns the LEDs on or off if they fall within a certain range, using a simple boolean expression, for example <code>range < 3*range_step_mm</code>, which returns true if the distance is within three times the <code>range_step_mm</code> parameter. <br />
      Nothing is stopping you from designing your own <code>ledControl</code> function, where you can use other indexes or, as a challenge, even use the four LEDs as 4 bits indicating up to sixteen <code>range_step_mm</code> differences instead of four.
    </p>

    <h3><a>Remote operation</a></h3>
    <p>
      As you have probably seen when looking at the code and the register functions, Pozyx is capable of communicating with remote Pozyx devices: thanks to UWB communication, we can perform every local functionality on a remote device as well. In this tutorial, this can be done by changing the <code>remote_id</code> and <code>remote</code> parameters. Setting <code>remote = true;</code> will perform all the functionality on a device with ID <code>remote_id</code>, so you will need to set that ID correctly as well and own three devices to perform remote ranging.
    </p>
    <p>
      A general rule with the Pozyx API is that almost every function has <code>remote_id</code> as a keyword argument, which allows the writing of code that can easily switch between being executed locally or remotely, as seen in this example. When <code>remote_id</code> is uninitialized, the function will be executed locally.
    </p>
    <p>
      The disadvantages of working remotely need to be kept in mind, however. The possibility of losing connection, something that is much harder with a cable. A delay, as the UWB communication between both devices is not instantaneous. And you need a local controller, which is an extra device in your setup. In turn, you gain a lot of mobility, as the remote device only needs to be powered, not needing a controller.
    </p>
    <p>
      This concludes the first Pozyx example. In the <a href="<?php echo site_url('Documentation/Tutorials/ready_to_localize/'.$type); ?>">next example</a>, we’ll cover positioning and the necessary setup to perform positioning successfully.
    </p>

    </div>

    <div class="col-md-12" style="margin-top:100px;">
      <p>
          <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
          Tutorial 1: Ready to range
      </p>
    </div>
  </div>
</div>


<script>
 $( document ).ready(function() {
    // add links to function calls
    $("#content").html($("#content").html().replace(/Pozyx\.([a-zA-Z0-9\_]{3,})/g, "Pozyx.<a href='<?php echo site_url('Documentation/Datasheet/Arduino#');?>$1'>$1</a>"));

 });
</script>
