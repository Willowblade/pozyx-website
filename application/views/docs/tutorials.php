<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<div class="container">
    <div class="row" style=" margin-top: 20px;">

      <div class="col-md-7">

        <h2>Documentation</h2>
        <p>Here you can find all documentation to start working with the pozyx system.<br>
        Download the <a href="https://github.com/pozyxLabs/Pozyx-Arduino-library">Arduino</a> or <a href="https://github.com/pozyxLabs/Pozyx-Python-library">Python</a> API from <a href="https://github.com/pozyxLabs">github</a> <i class="fa fa-github"></i>.
        </p>
      </div>

      <div class="col-md-5" style='text-align: right'>
      <br>
      <form action='<?php echo site_url('Documentation/search');?>' method="post" class="form-inline" style='text-align: right'>
      <div class="form-group has-feedback">
          <input type="text" class="form-control" name="search" placeholder="search for keywords" required>
          <span class="glyphicon form-control-feedback glyphicon-search"></span>

      </div>
      <?php
        $this->load->helper('form');
        echo form_submit('submit', 'Search', 'class="btn btn-primary"');
      ?>

      </form>
      </div>

    </div>
    <div class="row">
      <div class="col-md-4 col-sm-6" style='margin-top:30px'>

        <img src="<?php echo(base_url('assets/images/little_guy_kid.jpg')); ?>">

        <h3>Tutorials</h3>
        <ul>
          <li><a href="<?php echo site_url('Documentation/Tutorials/getting_started'); ?>">Getting started</a></li>
          <li><a href="<?php echo site_url('Documentation/Tutorials/ready_to_range'); ?>">Tutorial 1: Ready to range</a></li>
          <li><a href="<?php echo site_url('Documentation/Tutorials/ready_to_localize'); ?>">Tutorial 2: Ready to localize</a></li>
          <li><a href="<?php echo site_url('Documentation/Tutorials/orientation_3D'); ?>">Tutorial 3: Orientation 3D</a></li>
          <li><a href="<?php echo site_url('Documentation/Tutorials/multitag_positioning'); ?>">Tutorial 4: Multitag positioning</a></li>
        </ul>
        <p><b>Further reading</b></p>
        <ul>
          <li><a href="<?php echo site_url('Documentation/Tutorials/troubleshoot_basics'); ?>">Troubleshoot basics</a></li>
          <li><a href="<?php echo site_url('Documentation/Tutorials/uwb_settings'); ?>">Configuration of the UWB parameters</a></li>
          <li><a href="<?php echo site_url('Documentation/Tutorials/longer_use'); ?>">Setting up for longer use</a></li>
          <li><a href="<?php echo site_url('Documentation/Tutorials/anchor_configuration'); ?>">Anchor configuration</a></li>
          <li><a href="<?php echo site_url('Documentation/Tutorials/changing_id'); ?>">Changing the network ID</a></li>

        </ul>
       </div>
     <!--
       <div class="col-md-4 col-sm-6" style='margin-top:30px'>
         <img src="<?php echo(base_url('assets/images/little_guy_research.jpg')); ?>">
         <h3>Extra</h3>
         <p><b>Usecases</b></p>
         <ul>
           <li><a href="<?php echo site_url('Documentation/Tutorials/chat_room'); ?>">Adidas</a></li>
         </ul>
         <p><b>Arduino-specific</b></p>
         <ul>
           <li><a href="<?php echo site_url('Documentation/Tutorials/chat_room'); ?>">Pozyx chat room</a></li>
         </ul>

         <p><b>Python-specific</b></p>
         <ul>
           <li><a href="<?php echo site_url('Documentation/Tutorials/combining_functionality'); ?>">Combining functionality</a></li>
           <li><a href="<?php echo site_url('Documentation/Tutorials/ros_pozyx'); ?>">ROS with Pozyx</a></li>
         </ul>
       </div>
     -->

      <div class="col-md-4 col-lg-4 col-sm-6" style='margin-top:30px'>
        <img src="<?php echo(base_url('assets/images/little_guy_research.jpg')); ?>">
        <h3>Documentation</h3>

        <p><b>General</b></p>
        <ul>
          <li><a href="<?php echo site_url('Documentation/Datasheet'); ?>">Pozyx Datasheet</a></li>
          <li><a href="<?php echo site_url('Documentation/Datasheet/arduino'); ?>">Arduino Library</a></li>
          <!--<li><a href="<?php echo site_url('Documentation/Datasheet/versionHistory'); ?>">Version history</a></li>-->
        </ul>

        <p><b>Pozyx Academy</b></p>
        <ul>
          <li><a href="<?php echo site_url('Documentation/doc_howDoesPositioningWork'); ?>">How does positioning work?</a></li>
          <li><a href="<?php echo site_url('Documentation/doc_howDoesUwbWork'); ?>">How does ultra-wideband work?</a></li>
          <li><a href="<?php echo site_url('Documentation/doc_whereToPlaceTheAnchors'); ?>">Where to place the anchors?</a> <b><i>(updated)</i></b></li>
          <li><a href="<?php echo site_url('Documentation/doc_firmware_update'); ?>">How does positioning work?</a></li>
        </ul>
       </div>

       <div class="visible-sm clearfix"></div>

       <div class="col-md-4 col-lg-4 col-sm-6" style='margin-top:30px'>

        <img src="<?php echo(base_url('assets/images/little_guy_faq.jpg')); ?>">

        <h3>FAQ</h3>
        <p><b>Frequently asked questions</b></p>
        <ul id="FAQ">
          <?php
            foreach($faq as $row){
              echo "<li><div class='FAQ-question'>";
              echo $row->question;
              echo "</div><div class='FAQ-answer'>";
              echo $row->answer;
              echo '</div></li>';
            }
          ?>
        </ul>

       </div>

    </div>
</div>
