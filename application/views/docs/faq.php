
<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<!--<div class="container">      
    <div class="row" style='padding-bottom: 50px; margin-top: 20px; margin-bottom: 50px'>        

      <div class="col-md-6 col-lg-6">
        <h2>FAQ</h2>       

        <ul id="FAQ">
          <li><div class="FAQ-question">What is pozyx?</div>
            <div class="FAQ-answer">
              Pozyx is a hardware solution that provides accurate positioning and motion information. It can be used as an Arduino shield.    
            </div>
          </li>
          <li><div class="FAQ-question">Does it work inside and outside?</div>
            <div class="FAQ-answer">
              Yes. The pozyx system uses ultra-wideband technology for positioning. This wireless technology works both indoor and outdoor.    
            </div>
          </li>
          <li><div class="FAQ-question">How many modules do I need for localization</div>
            <div class="FAQ-answer">
              For 3D localization you need at least 5 modules. One module for which you want to know the position and 4 modules that act as anchors.
            </div>
          </li>              
          <li><div class="FAQ-question">What are anchors?</div>
            <div class="FAQ-answer">
              Anchors are modules with a fixed and known position. For 3D positioning you need at least 4 anchors. The anchors have the same role as satellites in GPS positioning. Note that, with our automatic anchor calibration feature, you do not need to acquire the position of the anchor yourself, it is done automatically for you.
            </div>
          </li>
          <li>
            <div class="FAQ-question">Where should I place the anchors?</div>
            <div class="FAQ-answer">
              The placement of the anchors will affect the positioning accuracy. For the best accuracy, place the anchors in a circle around the module to localize. The worst performance is expected when all anchors are on a straight line.
            </div>
          </li>          
          <li><div class="FAQ-question">Why kickstarter?</div>
            <div class="FAQ-answer">
              Building our first prototypes quickly took a few months: we had to design, assemble and program everything ourselves. With kickstarter you can support us such that it doesn't stay with some prototypes. With enough people backing us, we can go from a prototype to production and offer the positioning system to all our backers at an affordable price.
            </div>    
          </li>
        </ul>  
      </div>

    </div>
</div>

-->

<script>
    $( document ).ready(function() {

      $(".FAQ-answer").hide();

      $("#FAQ li").on("click", function(){
        $(this).find(".FAQ-answer").toggle();
      });  
    });

</script>