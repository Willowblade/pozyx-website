<!--<script type="text/javascript" src="http://latex.codecogs.com/latexit.js"></script>
<script type="text/javascript">
LatexIT.add('p',true);
LatexIT.add('li',true);
</script>-->

<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  TeX: { equationNumbers: { autoNumber: "AMS" } },
  tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}
  
});
</script>
<script type="text/javascript"
  src="//cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>

<script type="text/javascript">
$(document).ready(function() {
// Tooltip only Text
    $('.masterTooltip').hover(function(){
        // Hover over code
        var title = $(this).attr('title');
        $(this).data('tipText', title).removeAttr('title');
        $('<p class="tooltip"></p>')
        .text(title)
        .appendTo('body')
        .fadeIn('slow');
    }, function() {
        // Hover out code
        $(this).attr('title', $(this).data('tipText'));
        $('.tooltip').remove();
    }).mousemove(function(e) {
        var mousex = e.pageX + 20; //Get X coordinates
        var mousey = e.pageY + 10; //Get Y coordinates
        $('.tooltip')
        .css({ top: mousey, left: mousex })
    });
});
</script>

<style>
.latex_eq{
    margin-left: 25px;
    margin-bottom: 12px;
}

h3{
  margin-top: 40px;
}

#img_trilatteration{
    background-image:url('<?php echo(base_url('assets/images/docs/trilatteration.jpg')); ?>');
    width: 404px;
    height: 445px;
    border: 5px;    
}

#img_trilatteration:hover{
    background-image:url('<?php echo(base_url('assets/images/docs/trilatteration_hover.jpg')); ?>');
}

th{
    border-bottom: 1px solid #454545;
}

td{
    vertical-align: top;
    padding-right: 15px;
}

.tooltip {
    display:none;
    position:absolute;
    border:1px solid #333;
    background-color:#161616;
    border-radius:5px;
    padding:10px;
    color:#fff;
    font-size:12px Arial;
}

.note{
    cursor: pointer;
}

</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            How does positioning work
        </p>  

        <h2>How does positioning work</h2>            

        </div>  

      <div class="col-md-12">
        <h3>Introduction</h3>  
        <p>   
        In general, you only need two things to perform positioning:
        <ul>
        <li><b>Measurements.</b> In a sense, almost everything you can measure will depend on the position and could be used for positioning. 
        However, it's the most sensible to measure things that are very sensitive to the position. For example, measuring the temperature might tell you in what continent you are but that's not very accurate. 
        Measuring the distance or the angle to some point will probably be more accurate.
        </li>
        <li><b>Reference points.</b> It is only possible to describe a position relative to some reference points. Reference points can be 'your home', the north star or some satellites in the sky.
        For us, we will be using anchors. With 3 anchors it is possible to describe a two-dimensional coordinate system in which we will find our position. For three-dimensional positioning we need 4 anchors.</li>        
        </ul>

        <p>Here are some examples:</p>

        <table style="margin:auto; margin-top: 40px; margin-bottom: 40px;">
        <tr>
            <th>Positioning System</th>
            <th>Measurements</th>
            <th>Reference points</th>
        </tr>

        <tr>
            <td>GPS</td>
            <td>(pseudo-)distances<a class="note" onclick="javascript:note('Some GPS receivers also use the doppler shift for positioning.');">*</a>: 
            </td>
            <td>satellites</td>
        </tr>

        <tr>
            <td>Pozyx</td>
            <td>distances</td>
            <td>anchors</td>
        </tr>

        <tr>
            <td>camera based</td>
            <td>video image</td>
            <td>camera (with orientation specified)</td>
        </tr>

        <tr>
            <td>Wifi or BLE fingerprinting</td>
            <td>Received signal strength (RSS)</td>
            <td>the  <a class="note" onclick="javascript:note('In fingerprinting, the RSS value with respect to several access points is measured in advance at hundreds of known positions. These so-called fingerprints are then saved in a database.');">fingerprints</a> in the database</td>
        </tr>

        <tr>
            <td>digital compass</td>
            <td>magnetic field vector</td>
            <td>the magnetic north</td>
        </tr>

        <tr>
            <td>dead reckoning</td>
            <td>acceleration and angular velocity (from the gyroscope)</td>
            <td>your initial position and direction</td>
        </tr>

        </table>        
<!--
        <ul>
        <li>In GPS: the measurements are (pseudo-)distances and the reference points are the satellites.</li>
        <li>In Pozyx: the measurements are the distances and the refence points are the anchors.</li>
        <li>In camera based positioning: the measurements are the video feed and the reference points are the cameras (with the orientation specified)</li>
        <li>In Wifi or BLE fingerprinting the measurements are the received signal strength (RSS) and the reference points are the fingerprints in the database 
        (a fingerprint is a position associated with the RSS values of several beacons or access points)</li>
        <li>With the digital compass: the measurements are the magnetic field vector and the reference point is magnetic north. 
        Note that with only a single reference point, it is impossible to find your actual position.</li>
        <li>In dead reckoning the measurments are the acceleration and the angular velocity (from the gyroscope) and the reference point is your initial position and direction.</li>
        </ul>  
        -->  

        </p>
        <div style="float: right; width: 406x; text-align: center; margin-left: 20px; margin-top: 40px;"><div id='img_trilatteration'></div><br>
        <p>Trilateration. Hover this image to see<br>the mathematical definitions.</p>
        </div>
        <h3>Trilatteration</h3>     
        <p>
        
        The most commonly used method of positioning uses basic geometry to estimate the position. By measuring the distance to a number of anchors with a known position it is possible to obtain your own position.
        If we measure a certain distance, then we know we will be in a circle of that radius around the anchor. If we make distance measurements with 3 anchors we see that our position is uniquely determined by the
        intersection of the three circles. This method is called trilatteration (or multilatteration if more than 3 anchors are used).
        </p>
        <p>
        The difficulty of this approach lies in the fact that the measurements are not perfect. There will always be some noise on the measurements and because of this, the circles will not intersect at exactly one point.
        To circumvent this issue, we try to find the point that is closest to all circles.

        </p>     

        
        <div>
        <p>
        <img style="float: left; margin-right: 20px;" src='<?php echo(base_url('assets/images/little_guy_research.jpg')); ?>'>
        <br><b>Watch out, mathematics ahead!</b><br>
        You can choose to skip the description of the algorithm and go directly to the next article: 
        <a href="<?php echo site_url('Documentation/doc_howDoesUwbWork'); ?>">How does ultra-wideband work?</a>        
        </p>   
        </div>
        <br>



        <h3>A basic algorithm</h3>     
        <p>In this section, we will explain a simple algorithm to compute the position from a number of range measurements. 
        This basic algorithm is by no means optimal but when the range measurements are sufficiently accurate it will work quite well.
        </p>
        <p>
        We will explain the algorithm for 2D positioning.
        The user position $\mathbf{p}$ is given by the coordinates $x$ and $y$. The position of the $i$th anchor is given by $\mathbf{p}_i$ with coordinates $x_i$ and $y_i$ which are assumed to be known in advance.
        In total we have $N$ anchors so $i$ goes from $1$ to $N$.        
        </p>

        <p>Now the distance between the user and the $i$th anchor is denoted by $d_i$ and is given by the following formula:</p>

        <div class="latex_eq" lang="latex">
            $$
            \begin{equation}
            d_i = \sqrt{ (x-x_i)^2 + (y-y_i)^2 }
            \end{equation}
            $$
        </div>

        <p>We now take the square of this equation on both sides:</p>

        <div class="latex_eq" lang="latex">
            $$\begin{eqnarray}
            d_{i}^{2} & = & (x-x_{i})^{2}+(y-y_{i})^{2}\\
             & = & x^{2}-2xx_{i}+x_{i}^{2}+y^{2}-2yy_{i}+y_{i}^{2}\label{eq-distances}
            \end{eqnarray}$$
        </div>

        <p>The problem with equation $\ref{eq-distances}$ is the non-linear terms $x^2$ and $y^2$. We can eliminate these by subtracting $d_N^2$ from $d_i^2$ resulting in $N-1$ equations:</p>
        <div class="latex_eq" lang="latex">
            $$\begin{eqnarray}
            d_{i}^{2}-d_{N}^{2} & = & x^{2}-2xx_{i}+x_{i}^{2}+y^{2}-2yy_{i}+y_{i}^{2}-(x^{2}-2xx_{N}+x_{N}^{2}+y^{2}-2yy_{N}+y_{N}^{2})\\
             & = & -2x(x_{i}-x_{N})+x_{i}^{2}-x_{N}^{2}-2y(y_{i}-y_{N})+y_{i}^{2}-y_{N}^{2}
            \end{eqnarray}$$
        </div>

        <p>Now we have a number of equation that are linear in the unknown coordinates $x$ and $y$. That's great because linear equations are easy to solve. Let's write this in a matrix form:</p>

        <div class="latex_eq" lang="latex">
            $$
            \begin{equation}
            \mathbf{b} = \mathbf{A}\left[\begin{array}{c}x\\y\end{array}\right]
            \end{equation}
            $$
        </div>
        <p>with</p>
        <div class="latex_eq" lang="latex">
            $$
            \begin{equation}    
            \mathbf{b}=\left[\begin{array}{c}
            d_{1}^{2}-x_{1}^{2}-y_{1}^{2}-d_{N}^{2}+x_{N}^{2}+y_{N}^{2}\\
            d_{2}^{2}-x_{2}^{2}-y_{2}^{2}-d_{N}^{2}+x_{N}^{2}+y_{N}^{2}\\
            \vdots\\
            d_{N-1}^{2}-x_{N-1}^{2}-y_{N-1}^{2}-d_{N}^{2}+x_{N}^{2}+y_{N}^{2}
            \end{array}\right]
            \end{equation}
            $$
        </div>
        <p>and</p>

        <div class="latex_eq" lang="latex">
            $$
            \begin{equation}
            \mathbf{A}=-2\left[\begin{array}{cc}
            x_{1}-x_{N} & y_{1}-y_{N}\\
            x_{2}-x_{N} & y_{2}-y_{N}\\
            \vdots & \vdots\\
            x_{N-1}-x_{N} & \, x_{N-1}-x_{N}
            \end{array}\right]
            \end{equation}
            $$
        </div>

        <p>
        We can now solve this system of equations:<br>
        <ul>
        <li><b>if we have exactly 3 anchors: $N=3$</b>, we will have exactly 2 equations to find the two unknowns and we can find the position as follows:
            <div class="latex_eq" lang="latex">
            $$
            \begin{equation}
            \mathbf{p} = \mathbf{A}^{-1}\mathbf{b}
            \end{equation}
            $$
            </div>
        </li>
        <li><b>if we have more than 3 anchors: $N>3$</b>, we will have more equations than unknowns and the inverse of $\mathbf{A}$ will not exist. To deal with this situation we can use the pseudo-inverse to 
        compute the position. This results in the following equation:</p>

        <div class="latex_eq" lang="latex">
            $$
            \begin{equation}
            \mathbf{p} = (\mathbf{A}^{\top}\mathbf{A})^{-1} \mathbf{A}^{\top}\mathbf{b}
            \end{equation}
            $$
        </div>

        <p>where $\mathbf{A}^{\top}$ is the transpose of matrix $\mathbf{A}$. Note that the above formula will try to fit the coordinates $x$ and $y$ as much as possible to all the different equations (see here for <a href='http://s-mat-pcs.oulu.fi/~mpa/matreng/ematr5_5.htm' target='_new'>more information</a>). 
        Because of this, the method will have an increased accuracy when more anchors are used. </p></li>
        </ul>

        <p>
        The algorithm described above is called the linear least squares algorithm. 'Linear' because we linearize the equations (by taking the square), and 'least squares' because the (pseudo-)inverse of the matrix will result
        in a minimization of the squared error of all equations.
        </p>

        <h3>Going further</h3>   
        <p>
        The algorithm described above is a very simple and low-complex algorithm. In the pozyx system, you will be able to select from a number of different algorithms; the least-squares algorithm will be one of them.
        If you want to learn more about advanced positioning techniques it is recommended to look for the following topics:
        non-linear least squares, Kalman filter, particle filter, belief propagation, ..
        


        </p>
      </div>

      <div class="col-md-12" style='margin-top: 40px;'>
        <p>
        <b>Next:</b> <a href="<?php echo site_url('Documentation/doc_howDoesUwbWork'); ?>">How does ultra-wideband work?</a>
        </p>
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            How does positioning work
        </p>          

      </div>  


      <!-- Comments section with Disqus -->
      <div class="col-md-12" style='margin-top: 40px;'>

        <h3>Comments section</h3>   

        <div id="disqus_thread"></div>
        <script type="text/javascript">
            /* * * CONFIGURATION VARIABLES * * */
            var disqus_shortname = 'pozyx';            
            var disqus_identifier = 'How-does-positioning-work';
            var disqus_title = 'How does positioning work';            
            
            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function() {
                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

      </div>


    </div>
</div>

<script>

    function note(str){
        swal({   title: "More info...",   text: str,   imageUrl: "<?php echo(base_url('assets/images/little_guy_research.jpg')); ?>" });
    }

</script>


