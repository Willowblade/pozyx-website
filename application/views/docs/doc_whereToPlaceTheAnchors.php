<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  TeX: { equationNumbers: { autoNumber: "AMS" } },
  tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}
  
});
</script>
<script type="text/javascript"
  src="//cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>

<style>
.latex_eq{
    margin-left: 25px;
    margin-bottom: 12px;
}

h3{
  margin-top: 40px;
}
</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            Where to place the anchors
        </p>  

        <h2>Where to place the anchors</h2>            

        </div>  

      <div class="col-md-12">
        <h3>Rules of thumb</h3>    
        <p>
        The following rules apply to distance based positioning and can be used for the Pozyx platform.
        </p> 

        <ol style='margin-left: 30px;'>
          <li>Place the anchors high and in line-of-sight of the user.</li>
          <li>Spread the anchors around the user. Never on a straight line!</li>
          <li>For 3D positioning: place the anchors at different heights.</li>
        </ol>

        <h3>The rules explained</h3>
        <p><b>Rule number 1:</b> Place the anchors high and in line-of-sight of the user.</p>
        <p>
            The first rule is straightforward: placing the anchor high (on the ceiling or on the walls) increases the chance of receiving a good signal because there are less obstructions.
            Obstructions generally have a negative influence on the accuracy of the range measurements which has a direct effect on the positioning accuracy.             
        </p>

        <p><b>Rule number 2:</b> Spread the anchors around the user. Never on a straight line!</p>
        <p>
            A single range measurements will only give information in a single direction. This direction is exactly the direction from the user to the anchor. Because of this, it is best to spread the anchors such that they cover all directions.
            If the anchors are all on a straight line, the positioning error will be very large. This can be seen on the image below. You can see that a small change in radius (for example due to noise), will result in a very large change
            in the position of the intersection(s). In other words, the error on the range measurements is amplified! This is the same principle as in GPS, shere it is called the geometric dillution of precision (GDOP).   

        </p>   

        <div class="col-md-6" style="text-align:center">       
            <img src="<?php echo(base_url('assets/images/docs/bad_geometry.jpg')); ?>" style="align: center; display: block; margin: auto; margin-top: 10px; margin-bottom: 20px;">
            <p><i>Rule nr 2.</i></p>
        </div>
        <div class="col-md-6" style="text-align:center">       
            <img src="<?php echo(base_url('assets/images/docs/anchor_placement.jpg')); ?>" style="align: center; display: block; margin: auto; margin-top: 10px; margin-bottom: 20px;">
            <p><i>Rule nr 3.</i></p>
        </div>

        <p><b>Rule number 3:</b> Place the anchors vertically with the antenna at the top.</p>
        <p>
            The Pozyx system uses wireless UWB signals for positioning and this requires an antenna. 
            However, it is physically impossible to create an antenna that performs good in all directions.
            The monopole antenna (the white chip) on the Pozyx devices radiates omnidirectionally in the xz-plane, but doesn't radiate as well along the y-axis (see figure). 
            To make sure that we have the best possible reception, it is recommended to place the anchor vertically. Note that the same is true for the Pozyx tags.    
        </p>    

        <p><b>Rule number 4:</b> For 3D positioning: place the anchors at different heights.</p>
        <p>
            If the user and the anchors are all in the same horizontal plane, it is not possible to have a very good accuracy for the height (it is the same principle of GDOP in rule 2).
            For the pozyx system however, this rule is less important because the boards are equipped with an altimeter which can already measure the height.            
        </p>

<!--
        <h3>The error ellipse</h3>
        <p>
        <img src="<?php echo(base_url('assets/images/docs/phone_uncertainty.jpg')); ?>" style="float:left; margin-right: 20px;">

          In many cases, if you obtain a position for the user, you would also like to know how accurate this position estimation is. For example, on smartphones they sometimes show a circle around your position. 
          This means you can be anywhere within the circle, your smartphone doesn't know any better. However, in reality you can never be 100% certain of being within a given position and you can always show some circle.<br>
          <br>
          In general, 


          The geometry of where the anchors are placed play an important role in positioning. Because of this, it may be interesting to be able to quantify if a certain anchor placement is good or not. 
          In GPS, the geometry of the satellites is quantified by the so called geometric dilution of precision (GDOP). For our range-based system we use something very similar which is called the Fisher information matrix $\mathbf{F}$:

          <div class="latex_eq" lang="latex">
            $$
            \begin{equation}
            \mathbf{F} = \sum_{i=1}^{N}\mathbf{u}_i\mathbf{u}_i^{\top}
            \end{equation}
            $$
        </div>

        with 

        <div class="latex_eq" lang="latex">
            $$
            \begin{equation}
            \mathbf{u}_i = \frac{1}{\sigma}\frac{\mathbf{p}-\mathbf{p}_i}{\| \mathbf{p}-\mathbf{p}_i \|} 
            \end{equation}
            $$
        </div>

        where $\sigma$ is the standard deviation of the noise on the range measurements. For Pozyx (and assuming line-of-sight) this is $\sigma=0.03$m. $\mathbf{p}$ and $\mathbf{p}_i$ are the positions of the user and the $i$th anchor, respectively.
        This very abstract matrix $\mathbf{F}$ can now be used plot the error ellipse. Let's take a look at the equation for an ellipse:

        <div class="latex_eq" lang="latex">
            $$
            \begin{equation}
            \mathbf{x}^{\top}\mathbf{F}^{-1}\mathbf{x} = c
            \end{equation}
            $$
        </div>

        If we fix the constant $c$ than all the possible values for $\mathbf{x}$ will lay on an ellipse of which the shape is defined by the matrix $\mathbf{F}$.


        </p>
        -->

        
      </div>

      <div class="col-md-12" style='margin-top: 60px;'>
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
           Where to place the anchors
        </p>          

      </div>  

      <!-- Comments section with Disqus -->
      <div class="col-md-12" style='margin-top: 40px;'>

        <h3>Comments section</h3>   

        <div id="disqus_thread"></div>
        <script type="text/javascript">
            /* * * CONFIGURATION VARIABLES * * */
            var disqus_shortname = 'pozyx';            
            var disqus_identifier = 'Where-to-place-the-anchors';
            var disqus_title = 'Where to place the anchors?';            
            
            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function() {
                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

      </div>

    </div>
</div>

