<!--
Credits to the tutorial on tutorialzine.com:
http://tutorialzine.com/2010/09/google-powered-site-search-ajax-jquery/
-->

<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>

<style>

/* Web & news results */


.webResult{ margin-bottom:30px;}

.webResult p.category{  line-height:1.5; font-style: italic; padding-top: 5px; padding-left: 10px; }
.webResult p.content{ padding-top: 8px; }
.webResult p b{ }
.webResult > a{ }
.webResult {padding: 5px;}

#more{
	cursor: pointer;
}

</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-8">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            Search
        </p>  

        <h2>Search results</h2>   

        </div>  

        <div class="col-md-4" style="text-align: right">      
	      <form action='<?php echo site_url('Documentation/search');?>' method="post" class="form-inline" style="text-align: right">
	          <div class="form-group has-feedback">
	          <input type="text" class="form-control" name="search" placeholder="search for keywords" required>
	          <span class="glyphicon form-control-feedback glyphicon-search"></span>    
	          
	      </div>  
	      <?php 
	        $this->load->helper('form');    
	        echo form_submit('submit', 'Search', 'class="btn btn-primary"');
	      ?>
	       
	      </form>
      	</div>  

	    <div class="col-md-12">
	    	

	      <h3>Search results for keywords '<?php echo($keywords); ?>':</h3>
	      <br><br>         

	    </div>
	    
		

		<div class="col-md-12">
			<div id="resultsDiv"></div>
		         
    	</div>
	</div>
</div>

<script>
    $( document ).ready(function() {

    	var config = {
			siteURL		: 'pozyx.io',	// Change this to your site
			searchSite	: true,
			type		: 'web',
			append		: false,
			perPage		: 8,			// A maximum of 8 is allowed by Google
			page		: 0,				// The start page
			term 		: '<?php echo($keywords); ?>'
		}

		var config2 = {
			/*siteURL		: 'pozyx.io',	// Change this to your site
			searchSite	: true,
			type		: 'web',*/
			page		: 0,
			perPage		: 8,			// A maximum of 8 is allowed by Google
			cx		: "007005980111498682071:_i5morzetki",				// The start page
			q 		: '<?php echo($keywords); ?>',
			apikey	: "AIzaSyBb5MRBkA04nvBemG1aclxVzOZN1cHkVeI"
		}

		$('<a>',{id:'more', text:'More results'}).appendTo(resultsDiv).click(function(){
						
			$(this).fadeOut();
		});

      googleSearch2();
      
      
      function googleSearch2(settings){
		
		// If no parameters are supplied to the function,
		// it takes its defaults from the config object above:
		
		settings = $.extend({},config2,settings);
		settings.q = settings.q || $('#s').val();
		
		// if(settings.searchSite){
		// 	// Using the Google site:example.com to limit the search to a
		// 	// specific domain:
		// 	settings.term = 'site:'+settings.siteURL+' '+settings.term;
		// }
		
		// URL of Google's AJAX search API
		var apiURL = 'https://www.googleapis.com/customsearch/v1?num='+settings.perPage;
		var resultsDiv = $('#resultsDiv');
		
		$.getJSON(apiURL,{q:settings.q,cx:settings.cx,num:settings.perPage,start:(1+settings.page*settings.perPage), key:settings.apikey},function(r){
			
			var results = r.items;

			$('#more').remove();
			
			if(results.length){
				
				// If results were returned, add them to a pageContainer div,
				// after which append them to the #resultsDiv:
				
				var pageContainer = $('<table>',{class:'table table-condensed table-hover'});
				pageContainer.append('<thead>');
				pageContainer.append('<tr><th style="width: 180px">Section</th><th>Thumbnail</th><th>Search result</th></tr>');
				
				for(var i=0;i<results.length;i++){
					// Creating a new result object and firing its toString method:
					pageContainer.append(new result2(results[i]) + '');
				}
				
				if(!settings.append){
					// This is executed when running a new search, 
					// instead of clicking on the More button:
					resultsDiv.empty();
				}
				
				pageContainer.append('<div class="clear"></div>')
							 .hide().appendTo(resultsDiv)
							 .fadeIn('slow');
				
				var searchinfo = r.searchInformation;
				
				// Checking if there are more pages with results, 
				// and deciding whether to show the More button:
				
				if( +searchinfo.totalResults > (settings.page+1)*settings.perPage){					

					$('<a>',{id:'more', text:'More results'}).appendTo(resultsDiv).click(function(){
						googleSearch2({append:true,page:settings.page+1});
						$(this).fadeOut();
					});
				}
			}
			else {
				
				// No results were found for this search.
				
				resultsDiv.empty();
				$('<p>',{className:'notFound',html:'No Results Were Found!'}).hide().appendTo(resultsDiv).fadeIn();
			}
		});
	}

     
	
	function result2(r){
		
		// This is class definition. Object of this class are created for
		// each result. The markup is generated by the .toString() method.
		
		var arr = [];
		
		// GsearchResultClass is passed by the google API
		switch(r.kind){

			case 'customsearch#result':

				var category = "";
				var url = r.link.toLowerCase();
				if(url.indexOf("tutorial") > 0){
					category = "<p class='category'><span class='glyphicon glyphicon-education'></span> Tutorials</p>";
				}else if(url.indexOf("documentation/datasheet") > 0){
					category = "<p class='category'><span class='glyphicon glyphicon-book'></span> Datasheet</p>";
				}else if(url.indexOf("documentation") > 0){
					category = "<p class='category'><span class='glyphicon glyphicon-book'></span> Documentations</p>";
				}else if(url.indexOf("store") > 0){
					category = "<p class='category'><span class='glyphicon glyphicon-shopping-cart'></span> Store</p>";
				}

				var thumbnail = "";
				if (typeof r.pagemap.cse_thumbnail !== 'undefined') {					
					// thumbnail = "<img src='"+r.pagemap.cse_thumbnail[0].src +"' width=" + r.pagemap.cse_thumbnail[0].width + " height="+r.pagemap.cse_thumbnail[0].height +">";
					thumbnail = "<img src='"+r.pagemap.cse_thumbnail[0].src +"' height=70>";
				}

				arr = [
					'<tr><td>',category,'</td>',
					'<td>',thumbnail,'</td><td class="webResult">',
					'<a href="',r.link,'">', r.title,'</a>',					

					'<p class="content">',r.htmlSnippet,'</p>',
					/*'<a href="',r.unescapedUrl,'" target="_blank">',r.visibleUrl,'</a>',*/
					'</td></tr>'
				];
			break;
		/*
			case 'GimageSearch':
				arr = [
					'<div class="imageResult">',
					'<a href="',r.unescapedUrl,'" title="',r.titleNoFormatting,'" class="pic" style="width:',r.tbWidth,'px;height:',r.tbHeight,'px;">',
					'<img src="',r.tbUrl,'" width="',r.tbWidth,'" height="',r.tbHeight,'" /></a>',
					'<div class="clear"></div>','<a href="',r.originalContextUrl,'" target="_blank">',r.visibleUrl,'</a>',
					'</div>'
				];
			break;
			case 'GvideoSearch':
				arr = [
					'<div class="imageResult">',
					'<a target="_blank" href="',r.url,'" title="',r.titleNoFormatting,'" class="pic" style="width:150px;height:auto;">',
					'<img src="',r.tbUrl,'" width="100%" /></a>',
					'<div class="clear"></div>','<a href="',r.originalContextUrl,'" target="_blank">',r.publisher,'</a>',
					'</div>'
				];
			break;
			case 'GnewsSearch':
				arr = [
					'<div class="webResult">',
					'<h2><a href="',r.unescapedUrl,'" target="_blank">',r.title,'</a></h2>',
					'<p>',r.content,'</p>',
					'<a href="',r.unescapedUrl,'" target="_blank">',r.publisher,'</a>',
					'</div>'
				];
			break;
		*/
		}
		
		// The toString method.
		this.toString = function(){
			return arr.join('');
		}
	}


    });

</script>