<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

<style>
.bittable{
      width:700px;
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}

h3 a{
      text-decoration: none;
      color: #5F945F;
}

h3 a:hover{
      text-decoration: none;
      color: #5F945F;
}

h4{
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
      font-weight: bold;
      margin-bottom: 0px;
}

.step:hover{
      cursor: pointer;
      text-decoration: underline;
}

.txrx-ol {
    display: block;
    padding-left: 25px;
    counter-reset:li; /* Initiate a counter */
}
.txrx-ol li {
      height: 24px;
      overflow-y: hidden;
      position: relative;
      padding-left: 40px;
    list-style:none; /* Disable the normal item numbering */
    /*background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
    background-position: -5px;
    background-repeat: no-repeat;
    padding-left: 7px;*/
}

.txrx-ol > li:before {
    font-weight: bold;
    content:counter(li); /* Use the counter as content */
    counter-increment:li; /* Increment the counter by 1 */
    /* Position and style the number */
    position:absolute;
    /*left:-2.2em;*/
    left: 0px;
    width:2em;
    /* Some space between the number and the content in browsers that support
       generated content but not positioning it (Camino 2 is one example) */
    margin-right:8px;
    color:#fff;
    font-weight:bold;
    text-align:center;
    background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
}

li.L0, li.L1, li.L2, li.L3,
li.L5, li.L6, li.L7, li.L8
{ list-style-type: decimal !important }

.prettyprint{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint li{
      font-size: 14px;
      line-height: 17px;
}

.prettyprint code{
      font-size: 14px;
      line-height: 17px;
}

</style>

<div class="container">
      <!-- Example row of columns -->
  <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

    <div class="col-md-12">
      <p>
          <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
          Specific template
      </p>

      <h2>Specific template</h2>
    </div>

    <div id="content" class="col-md-12">
      <h3><a>First title</a></h3>
      <p>
      Text paragraph.
      </p>

      <pre class="prettyprint linenums"style="padding-left: 20px">
        <code>
        #include &lt;Pozyx.h&gt;
        #include &lt;Pozyx_definitions.h&gt;
        #include &lt;Wire.h&gt;

        // this is just some code
        </code>
      </pre>
    </div>

    <div class="col-md-12" style="margin-top:100px;">
      <p>
          <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
          <a href="<?php echo site_url('Documentation'); ?>">Tutorials</a> &gt;
          Specific template
      </p>
    </div>
  </div>
</div>


<script>
 $( document ).ready(function() {
    // add links to function calls
    $("#content").html($("#content").html().replace(/Pozyx\.([a-zA-Z0-9\_]{3,})/g, "Pozyx.<a href='<?php echo site_url('Documentation/Datasheet/Arduino#');?>$1'>$1</a>"));

 });
</script>
