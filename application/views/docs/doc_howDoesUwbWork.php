<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  TeX: { equationNumbers: { autoNumber: "AMS" } },
  tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}
  
});
</script>
<script type="text/javascript"
  src="//cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>

<style>
.latex_eq{
    margin-left: 25px;
    margin-bottom: 12px;
}

h3{
  margin-top: 40px;
}

.note{
    cursor: pointer;
}
</style>


<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            How does ultra-wideband work
        </p>  

        <h2>How does ultra-wideband work</h2>            

        </div>  

      <div class="col-md-12">
        <h3>Traveling at the speed of light</h3>     

        <p>
        In the previous post, we saw that we can obtain our position by measuring the distances to the anchors. So how do we do this?
        For this we use radio waves. The idea is to send radio waves from one module to another and measure the time of flight (TOF), or in other words, how long it takes. 
        Because radio waves travel at the speed of light ($c=299792458$m/s) we can simply divide the time of flight by this speed to get the distance.
        </p>
        <p>
        However, perhaps you've noticed that the radio waves travel fast. Very fast! In a single nanosecond, which is a billionth of a second, a wave has traveled almost 30cm. So if we want to perform
        centimeter accurate ranging, we have to be able to measure the timing very very accurately! So now the question is, how can we do this? How can we even measure the timing of.. a wave?
        </p>

        <h3>It's all about the bandwidth</h3>     

        <p>
        In physics, there is something called the Heisenberg's uncertainty principle. 
        The principle states that it is impossible to know both the frequency and the timing of a signal. 
        Consider for example a sinusoid; a signal with a well known frequency but a very ill-determined timing: the signal has no beginning or end.
        However, if we combine multiple sinusoidal signals with a slightly different frequency, we can create a 'pulse' with more defined timing, i.e., the peak of the pulse. 
        This is seen in the following figure from <a href='https://en.wikipedia.org/wiki/Uncertainty_principle' target='_new'>wikipedia</a> that sequentially adds sinusoids to a signal to get a sharper pulse:
        </p>
        <img src="<?php echo(base_url('assets/images/docs/Sequential_superposition_of_plane_waves.gif')); ?>" style="align: center; display: block; margin: auto; margin-top: 10px; margin-bottom: 20px;">

        <p>
        The range of frequencies that are used for this signal is called the bandwidth $\Delta f$.
        Using Heisenberg's uncertainty principle we can roughly determine the width $\Delta t$ of the pulse, given a certain bandwidth $\Delta f$<a class="note" onclick="javascript:note('In this formula, the bandwidth and width of the pulse are defined by the root of the second central moment of the squared signal in frequency domain and time domain, respectively.');">*</a>:
        </p>

        <div class="latex_eq" lang="latex">
            $$
            \begin{equation}
            \Delta f \Delta t \geq \frac{1}{4\pi}
            \end{equation}
            $$
        </div>

        <p>
        From this simple formula we can see that if we want a narrow pulse, which is necessary for accurate timing, we need to use a large bandwidth. 
        For example, using the bandwidth $\Delta f=20$MHz available for wifi systems we obtain a pulse-width larger than $\Delta t\geq 4$ns. 
        Using the speed of light this relates to a pulse of 1.2m 'long' which is too much for accurate ranging. Firstly, because it is hard to accurately determine the peak of such a wide pulse, and secondly because of reflections.
        Reflections come from the signals bouncing onto object (walls, ceilings, closets, desks, etc..) within the surrounding environment. 
        These reflections are also captured by the receiver and may overlap with line-of-sight pulse which makes it very hard to measure the true peak of the pulse. 
        With pulses of $4$ns wide, any object within 1.2m of the receiver or the transmitter will
        cause an overlapping pulse. Because of this, ranging with wifi using time-of-flight is not suitable for indoor applications.

        </p>

        <p>
        The ultra-wideband signals that are used in Pozyx have a bandwidth of $500$MHz resulting in pulses of $0.16$ns wide! 
        This timing resolution is so fine that at the receiver, we are able to distinguish several reflections of the signal. 
        Hence, it remains possible to do accurate ranging even in places with a lot of reflectors, such as indoor environments.
        </p>          
        
        <img src="<?php echo(base_url('assets/images/docs/doc_uwb_RX_signal.png')); ?>" style="align: center; display: block; margin: auto; margin-top: 10px; margin-bottom: 20px;">      

        <h3>Where to put all this bandwidth?</h3>
        <p>
            So we need a lot of bandwidth. Unfortunately, everybody wants it: in wireless communication systems, more bandwidth means faster downloads. 
            However, if everybody would transmit signals on the same frequencies, all the signals would interfere and nobody would be able to receive anything meaningful. 
            Because of this, the use of the frequency spectrum is highly regulated.             
        </p>
        <p>
            So how is it possible that UWB gets 500MHz of precious bandwidth and most other systems have to satisfy with a lot less? Well, the UWB systems are only allowed to transmit at very low power (the power spectrum density must be below -41.3dBm/MHz).
            This very strict power constraint means that a single pulse is not able to reach far: at the receiver, the pulse will likely be below the noise level. 
            In order to solve this issue, a train of pulses are sent by the transmitter (typically 128 of 1024) to represent a single bit of information. At the receiver, the received pulses are accumulated and with enough pulses, the power of the 'accumulated pulse' will rise above 
            the noise level and reception is possible. Hooray!
        </p>
         


        <h3>A note on the received signal strength (RSS)</h3>
        <p>
        There exists another way to measure the distance between two points by using radio waves, and that is by using the received signal strength. The further the two points are,
        the smaller the received signal strength will be. Hence, from this RSS-value we should be able to derive the distance. Unfortunately, it's not that simple. 
        In general, the received signal strength will be a combination of the power of all the reflections and not only of the desired line-of sight. Because of this it becomes very hard to relate the RSS value to the true distance.
        To show how bad this can be, here is a figure:        
        </p>

        <img src="<?php echo(base_url('assets/images/docs/RSS.png')); ?>" style="align: center; display: block; margin: auto; margin-top: 10px; margin-bottom: 20px;">  

        <p>
        In this figure, the RSS value of a bluetooth signal is measured at certain distances. At every distance, the error bars show how the RSS value behaves at the given distance.
        Clearly, the variation on the RSS value is very large which makes RSS unsuitable for accurate ranging or positioning.
        </p>
        
      </div>

      <div class="col-md-12" style='margin-top: 40px;'>
        <p>
        <b>Next:</b> <a href="<?php echo site_url('Documentation/doc_whereToPlaceTheAnchors'); ?>">Where to place the anchors?</a>
        </p>       

        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            How does ultra-wideband work
        </p>          

      </div>  

      <!-- Comments section with Disqus -->
      <div class="col-md-12" style='margin-top: 40px;'>

        <h3>Comments section</h3>   

        <div id="disqus_thread"></div>
        <script type="text/javascript">
            /* * * CONFIGURATION VARIABLES * * */
            var disqus_shortname = 'pozyx';            
            var disqus_identifier = 'How-does-ultra-wideband-work';
            var disqus_title = 'How does ultra-wideband work';            
            
            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function() {
                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

      </div>



    </div>
</div>

<script>

    function note(str){
        swal({   title: "More info...",   text: str,   imageUrl: "<?php echo(base_url('assets/images/little_guy_research.jpg')); ?>" });
    }

</script>