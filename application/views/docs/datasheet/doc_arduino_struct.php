<?php

function linkify($text) {

  //Convert urls to <a> links
  $text = preg_replace("/([\w]+\:\/\/[\w-?&;#~=\.\/\@]+[\w\/])/", "<a target=\"_blank\" href=\"$1\">$1</a>", $text);

  // insert the pozyx register link, by replacing everything that begins with POZYX_ (except when a hashtag is present)
  //$text = preg_replace("/\b(?<!\#)(POZYX_[A-Za-z0-9\_]*)/", "<a href='".site_url('Documentation/Datasheet/RegisterOverview')."#$1'>$1</a>", $text);
  $text = preg_replace("/\b(?<!\#)reg:(POZYX_[A-Za-z0-9\_]*)/", "<a href='".site_url('Documentation/Datasheet/RegisterOverview')."#$1'>$1</a>", $text);


  // everything with a hashtag # in front is converted surrounded with <code> tags
  $text = preg_replace("/(^|\s)#([A-Za-z0-9\_\/\.]*)/", " <code>$2</code>", $text);

  // for structs, add a _t behind and delete the leading underscore
  $text = preg_replace("/\b(?<!\#)\_([]a-zA-Z0-9\_]*)/", "$1_t", $text);

  
  //Convert attags to twitter profiles in <a> links
  //$text = preg_replace("/@([A-Za-z0-9\/\.]*)/", "<a href=\"http://www.twitter.com/$1\">@$1</a>", $text);

  return $text;

}

function recursively_find_text_nodes($dom_element)
{
	if(!isset($dom_element))
		return;

    $return = array();

    foreach ( $dom_element->childNodes as $dom_child )
    {
        switch ( $dom_child->nodeType )
        {
            case XML_TEXT_NODE:
            	if(!$dom_child->isWhitespaceInElementContent())
            		echo linkify($dom_child->wholeText);

            break;
            case XML_ELEMENT_NODE:
            	//$return[] = $dom_child->tagName . ": ";

            	if($dom_child->tagName == "definition" ||
            		$dom_child->tagName == "bitfield"  )
            		continue;

            	if($dom_child->tagName == "ref"){
            		echo "<a href='". site_url('Documentation/Datasheet/Arduino/'. $dom_child->getAttributeNode("refid")->value) ."'>";
            	}else if($dom_child->tagName == "defval"){
            		echo  " = <code>";
            	}else if($dom_child->tagName == "type"){
            		echo  "<td><span class='type'>";
            	}elseif ($dom_child->tagName == "heading"){
            		echo  "<h3>";
            	}elseif ($dom_child->tagName == "briefdescription"){
            		echo "<p>";
            	}elseif ($dom_child->tagName == "name"){
            		echo  "<td>";
            	}elseif ($dom_child->tagName == "linebreak"){
            		echo  "<br>";
            	}elseif($dom_child->tagName == "itemizedlist"){
            		echo "<ul style='list-style-type: none; margin-left: 10px'>";
            	}elseif($dom_child->tagName == "listitem"){
            		echo "<li>";
            	}

                echo recursively_find_text_nodes($dom_child);

                if($dom_child->tagName == "ref"){
            		echo "</a>";
            	}else if($dom_child->tagName == "defval"){
            		echo  "</code>";
            	}else if($dom_child->tagName == "type"){
            		echo  "</span></td>";
            	}elseif ($dom_child->tagName == "heading"){
            		echo  "</h3>";
            	}elseif ($dom_child->tagName == "briefdescription"){
            		echo "</p>";
            	}elseif ($dom_child->tagName == "name"){
            		echo  "</td><td>";
            	}elseif($dom_child->tagName == "itemizedlist"){
            		echo "</ul>";
            	}elseif($dom_child->tagName == "listitem"){
            		echo "</li>";
            	}
            break;
        }
    }

}

?>

<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<style>
	.type{
		color: gray; /*#52d9b1*/;
		
		/*float: left; */
	}

	.params{
		margin-left: 40px;
		padding-left: 0px;
		list-style-type: none;
	}

	.params li{
		
	}

	.parametername{
		width: 160px; 
		float: left; 
		clear: both;

	}

	li a, li a:hover{
		color: black;
	}	

	h3 a, h3 a:hover {
		color: inherit;
		text-decoration: none;
	}
	

</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
             <a href="<?php echo site_url('Documentation/Datasheet/Arduino'); ?>">Arduino library</a> &gt;
             <?php echo linkify($struct_data["name"]); ?> 
        </p>  

        <h2><?php echo linkify($struct_data["name"]); ?></h2>            

        </div>  

      <div class="col-md-12">

      <p>
      <?php

      $dom_element = dom_import_simplexml($struct_data["briefdescription"]);
	  
	  $txt = recursively_find_text_nodes($dom_element);
	  /*foreach($txt as $part){
		echo($part);
	  }	*/ 

	  echo "</p><h3>Struct members</h3><table class='table table-condensed table-hover'>";
	  echo "<tr><th style='width:15%'>type</th><th style='width:25%'>Variable name</th><th>Description</th></tr>";

	  foreach($struct_data["members"] as $member) {	  
	    		
	    echo("<tr>");		
		$dom_element = dom_import_simplexml($member);
		$txt = recursively_find_text_nodes($dom_element);
		/*foreach($txt as $part){
			echo($part);
		}*/	    		
		echo("</td></tr>");

		
	  }
	  echo("</table><br>");
 
      
      ?>

      </div>
      
  	</div>

  	<div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
             <a href="<?php echo site_url('Documentation/Datasheet/Arduino'); ?>">Arduino library</a> &gt;
             <?php echo linkify($struct_data["name"]); ?> 
        </p>  
    </div>  
</div>

<script>

function scroll_if_anchor(href) {

    href = typeof(href) == "string" ? href : $(this).attr("href");
    
    // You could easily calculate this dynamically if you prefer
    var fromTop = 75;
    
    // If our Href points to a valid, non-empty anchor, and is on the same page (e.g. #foo)
    // Legacy jQuery and IE7 may have issues: http://stackoverflow.com/q/1593174
    if(href.indexOf("#") == 0) {
        var $target = $('a[name='+ href.substr(1) +']');
        //var $target = $(href);   
        
        // Older browser without pushState might flicker here, as they momentarily
        // jump to the wrong position (IE < 10)
        //console.log($target);
        if($target.length) {           
            $('html, body').animate({ scrollTop: $target.offset().top - fromTop });
            /*if(history && "pushState" in history) {
                history.pushState({}, document.title, window.location.pathname + href);
                return false;
            }*/
        }
    }
}

// When our page loads, check to see if it contains and anchor
scroll_if_anchor(window.location.hash);

// Intercept all anchor clicks
$("body").on("click", "a", scroll_if_anchor);
</script>