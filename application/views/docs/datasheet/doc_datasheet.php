<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            Pozyx datasheet
        </p>  

        <h2>Pozyx datasheet</h2>            

        </div>  

      <div class="col-md-12">

      <h3>Overview</h3>	
      <p>
      Preliminary version of the online datasheet. More details will be released in the coming weeks.
      </p>
      <ol style='margin-left: 20px'>
      <!--<li><a href="<?php echo site_url('Documentation/Datasheet/SystemDescription'); ?>">System description</a></li>-->
      <li><a href="<?php echo site_url('Documentation/Datasheet/SystemDescription'); ?>">System description</a></li>
      <li><a href="<?php echo site_url('Documentation/Datasheet/FunctionalDescription'); ?>">Functional description</a></li>      
      <li><a href="<?php echo site_url('Documentation/Datasheet/RegisterOverview'); ?>">Register overview</a></li>
      <li><a href="<?php echo site_url('Documentation/Datasheet/Interfaces'); ?>">Interfaces protocols (I²C and USB)</a></li>
      <li><a href="<?php echo site_url('Documentation/Datasheet/pinOut'); ?>">Pin-out</a></li>
      <!--<li><a href="<?php echo site_url('Documentation/Datasheet/RegisterDescription'); ?>">Register description</a></li>-->
      </ol>
      </div>
      
  	</div>
</div>
