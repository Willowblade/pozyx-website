<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<style>
.bittable{
      width:700px; 
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}
.seperator{
  margin: 2px;
  padding: 2px;
}

.descr-table{
  width:700px; 
  margin-left: 20px
}

.copy {  margin-top: 25px;  }
.copy .clipboard{cursor: pointer;}
.copy .clipboard:hover{  text-decoration: underline;  }

</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation/Datasheet'); ?>">Datasheet</a> &gt;
            Register overview
        </p>  

        <h2>Register overview</h2>            

        </div>  

      <div class="col-md-12">

      <h3>General remarks</h3>	
      <p>
      Communication with the Pozyx device is performed by reading from and writing to certain register addresses. 
      This can be performed through the I2C bus or wirelessly with ultra-wideband. In total 256 register addresses are available, denoted by their hexadecimal value. 
      We can subdivide the address space in a number of logical groups.
      </p>

      <table class='table' style='width: 600px' align="center">
      <thead>
      	<tr>
      		<th>Register addresses</th>
      		<th>Description</th>
      	</tr>
      </thead>
      <tbody>
      	<tr>
      		<td>0x00 - 0x0F</td>
      		<td>Status data</td>
      	</tr>
      	<tr>
      		<td>0x10 - 0x2F</td>
      		<td>Configuration data</td>
      	</tr>
      	<tr>
      		<td>0x30 - 0x9F</td>
      		<td>Sensor data</td>
      	</tr>
      	<tr>
      		<td>0xA0 - 0xFFF</td>
      		<td>Function addresses</td>
      	</tr>
      </tbody>
      </table>

      <p>
      The first three groups, i.e., status, configuration and sensor data, are directly pointed to memory. Each address is pointed to a single byte of data. 
      This memory can be accessed by reading or writing to it. All data is internally stored using the <b>little-endian</b> representation.
      For example, the x-position is a 32bit integer (4 bytes) and is stored at register addresses 0x30 till 0x33. 
      Because we use the little-endian representation the least significant byte of the 32bit integer is stored at address 0x30, the second least significant byte is stored at the address 0x31, and so on.       
      Note that multiple subsequent registers can be read from or written to in one sequence.</p>

      <p>
      The last logical group, the function addresses are not related to memory bytes. Rather, by accessing these registers a function
      on the pozyx device is triggered. This typically involves writing data (i.e. the function parameters) to that memory address and
      reading the return value of the function. All data is again represented using the little-endian representation. 
      Note that, unlike the data registers, it is not possible to call multiple subsequent addresses in one call.
      </p>
      <br><br><br>

 

      <h3>Register map</h3>
      
      <div style="float:right"> 
      <span class='form-group'>
      <b>Firmware Version:</b> <select class="form-control" onchange="changeVersion()" id="version" style="width: 100px; display: inline">
        <option value="v0.9" <?php if($version == "v0.9") echo("selected"); ?> >v0.9</option>        
        <option value="v1.0" <?php if($version == "v1.0") echo("selected"); ?>>v1.0</option>
        <?php if($this->ion_auth->logged_in()) { ?>
        <option value="internal" <?php if($version == "internal") echo("selected"); ?>>internal</option>
        <?php } // end logged in ?>

      </select>
      </span>
      <a href='<?php echo site_url('Documentation/Datasheet/Registerheader/'.$version); ?>' class='btn btn-primary'><span class="glyphicon glyphicon-download-alt"></span>  Download the register header file</a>
      </div>
      <div style="clear:both"></div><br>



      <p>The type R represents that an address is read only. R/W represents readable and writable data registers.</p>
      <br>


<!-- DO NOT REMOVE: THIS IS A TEMPLATE FOR BIT FLAGS
<br>
Register contents:<br>
<table class='table table-bordered table-condensed bittable'>
<tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

<tr><td><b>Bit flag</b></td>
<td>PIN</td><td>-</td><td>-</td><td>RX_DATA</td><td>RANGE</td><td>IMU</td><td>POS</td><td>ERR</td></tr>
</table>

<table class='table table-bordered table-condensed' style=' width:700px; '>
<tr><th>Bit flag</th><th>Description</th></tr>
<tr>
<td>ERR</td><td>Enables interrupts whenever an error occurs.</td></tr>
<td>POS</td><td>Enables interrupts whenever a new positiong update is availabe.</td></tr>
<td>IMU</td><td>Enables interrupts whenever a new IMU update is availabe.</td></tr>
<td>RANGE</td><td>Enables interrupts whenever an new ranging update is availabe.</td></tr>
<td>RX_DATA</td><td>Enables interrupts whenever data is received through the ultra-wideband network.</td></tr>
<td>PIN</td><td>Configures the interrupt pin.<br>Value 0: Pin 0.<br>Value 1: Pin 1.</td>

</tr>
</table>
-->

<table class='table table-condensed table-hover'><thead><tr> 
<th style='width:80px'>Register<br>address</th>
<th style='width:50px'>Num bytes</th> 
<th style='width:50px; text-algin:center'>Type</th>
<th style='width:320px'>Name</th> 
<th style='width:100px'>Variable<br>type</th> 
<th>Short description</th></tr> </thead>


<?php       
  
        $category = "";

        if(1){
            foreach($reg_data as $register){  

              // don't show hidden registers
              if($register->is_hidden == 1 && !$this->ion_auth->logged_in())
                continue;

              if($category != $register->category){
                $category = $register->category;
                echo "<thead><tr><th colspan=6><br><br>". $category ."</th></tr></thead><tbody>";

              }

              echo "<tr";
              if($register->is_hidden == 1)
                echo " class='warning'";  
              echo "><td colspan='6'><table style='width:100%'><tr id='row". $register->id ."'><td style='width:80px'>";
              echo $register->reg_address;
              echo "</td><td style='width:50px; text-align:center'>";
              echo $register->num_bytes; 
              echo "</td><td style='width:50px; text-align:center'>";
              echo $register->type; 
              echo "</td>";


              if( $register->description_long != ""){
                echo "<td style='width:320px;'><a name='". $register->reg_name ."' href='' onclick='showDetails(".$register->id."); return false'>";
                echo $register->reg_name . "</a></td>";      
              }else{
                echo "<td style='width:320px;'>". $register->reg_name ."</td>";      
              }                

              $parts = explode(" ", $register->internal);
              echo "<td style='width:100px;'>".$parts[0]."</td>";

              echo "<td sytle='width:600px'>";
              if($register->is_new == 1)
                  echo " <b>new</b> - ";
              echo $register->description_short . "</td>";
              //echo "<td sytle='width:600px'>". $register->description_short . "</td>";

              // only for admins!
              /*
              if($this->ion_auth->logged_in()) {
                echo "<td style='width:50px'>";
                echo "<a title='report a bug' href='' onclick='reportBug(". $register->id ."); return false;' style='color:#454545'><i class='fa fa-bug'></i></a>";
                echo "<a href='".site_url('Documentation/Datasheet/RegisterEdit/'. $register->id ) . "' title='edit'><span class='glyphicon glyphicon-pencil'></span></a> ";
                echo "</td>";
              }
              */


              echo "</tr>";

              echo "<tr id='row_append".$register->id."' style='display:none'><td style='padding-left:180px; padding-right: 50px; padding-bottom:10px; padding-top:20px;' colspan='6'>";
              echo $register->description_long;

              if($register->code_example != ""){
                echo "<div class='copy'><div class='clipboard'><span class='glyphicon glyphicon-copy'></span> Copy example to clipboard</div>";
                echo "<pre><code class='copy_code'>".$register->code_example."</code></pre>";
                echo "</div>";
              }
              echo "</td></tr></table></td></tr>";
            }

        }else{
          echo "<tr><td colspan='9'><i>No results found for your query.</i></td></tr> ";
        }
          ?>

</tbody></table><br>
<h3>Register Functions</h3>
<table class='table table-condensed table-hover'>      

</tbody></table><table class='table table-condensed table-hover'>
<thead><tr> <th style='width:80px'>Register<br>address</th><th style='width:100px; text-align:center'>Param bytes</th><th style='width:100px; text-align:center'>Return bytes</th>
<th style='width:320px'>Name</th><th>Short description</th> </tr> </thead><tbody>


      <?php
      $category = "Functions";

      foreach($reg_functions as $register){  

          // don't show hidden registers
          if($register->is_hidden == 1 && !$this->ion_auth->logged_in())
            continue;

          if($category != $register->category){
            $category = $register->category;
            echo "<thead><tr><th colspan=6><br><br>". $category ."</th></tr></thead><tbody>";

          }

          echo "<tr";
          if($register->is_hidden == 1)
            echo " class='warning'";  
          echo "><td colspan='6'><table style='width:100%'><tr id='row". $register->id ."'><td style='width:80px'>";
          echo $register->reg_address;
          echo "</td><td style='width:100px; text-align:center'>";
          echo $register->num_bytes; 
          echo "</td><td style='width:100px; text-align:center'>";
          echo $register->default_val; 
          echo "</td>";


          if( $register->description_long != ""){
            echo "<td style='width:320px;'><a name='". $register->reg_name ."' href='' onclick='showDetails(".$register->id."); return false'>";
            echo $register->reg_name . "</a></td>";        
          }else{
            echo "<td style='width:320px;'>". $register->reg_name ."</td>";      
          }                

          echo "<td>";
          if($register->is_new == 1)
              echo " <b>new</b> - ";
          echo $register->description_short . "</td>";

          // only for admins!
          /*
          if($this->ion_auth->logged_in()) {
            echo "<td style='width:50px'>";
            echo "<a href='".site_url('store/thank_you/') . "' title='report a bug' style='color:#454545'><i class='fa fa-bug'></i></a>";
            echo "<a href='".site_url('Documentation/Datasheet/RegisterEdit/'. $register->id ) . "' title='edit'><span class='glyphicon glyphicon-pencil'></span></a> ";
            echo "</td>";
          }
          */

          echo "</tr>";

          echo "<tr id='row_append".$register->id."' style='display:none'><td style='padding-left:180px; padding-right: 100px; padding-bottom:10px; padding-top:20px;' colspan='6'>";
          echo $register->description_long;
          echo "</td></tr></table></td></tr>";
      }
      
      ?>

</tbody></table>
  


</div>
</div>

<script>

function showDetails(row_id){
      $("#row_append" + row_id).toggle();//slideToggle(800);
      /*
      var info_row = $("#row_append" + row_id);
      if(info_row.length == 0) {
            //it doesn't exist
            $('#row'+row_id).after('<tr id="row_append'+row_id+'"><td colspan="6" style="padding-bottom:10px; padding-top:20px">This register identifies the Pozyx device. This can be used to make sure that the Pozyx is connected properly.<br><b>Default value:</b> 0x43.</td></tr>');
      }else{
            info_row.remove();
      }
      */

      return false;
}

function changeVersion(){
  
  location.href = "<?php echo site_url('Documentation/Datasheet/RegisterOverview'); ?>" + "/" + $("#version").val();
}

function scroll_if_anchor(href) {

    href = typeof(href) == "string" ? href : $(this).attr("href");
    
    // You could easily calculate this dynamically if you prefer
    var fromTop = 75;
    
    // If our Href points to a valid, non-empty anchor, and is on the same page (e.g. #foo)
    // Legacy jQuery and IE7 may have issues: http://stackoverflow.com/q/1593174
    if(href.indexOf("#") == 0) {
        var $target = $('a[name='+ href.substr(1) +']');
        //var $target = $(href);   
        
        // Older browser without pushState might flicker here, as they momentarily
        // jump to the wrong position (IE < 10)
        //console.log($target);
        if($target.length) {           
            $('html, body').animate({ scrollTop: $target.offset().top - fromTop });
            if(history && "pushState" in history) {
                history.pushState({}, document.title, window.location.pathname + href);
                return false;
            }
        }
    }
}

function reportBug(register_id, register_name){
  swal({   
    title: "Report a bug",   
    html: "<input id='input-field' class='dialogInput' placeholder='enter your emailaddress'><br><b>Bug description:</b><br><textarea class='form-control' rows='6' id='bug'></textarea>",   
    confirmButtonText: "Subscribe",          
    type: "input",   
    width: 500,
    showCancelButton: true,   
    closeOnConfirm: false,            
    inputPlaceholder: "Your email address for an email reminder"
  }, function(){
    inputValue = $('#input-field').val();

    if (inputValue === false) return false;      
    if (inputValue === "" || !isValidEmailAddress(inputValue)) {     
      swal.showInputError("Please enter a valid email address");     
      return false;   
    }      

    subscribe(inputValue); 
    
  });

  return false;
}    

// When our page loads, check to see if it contains and anchor
scroll_if_anchor(window.location.hash);

// Intercept all anchor clicks
$("body").on("click", "a", scroll_if_anchor);


$( document ).ready(function() {
  //$(".copy").click(copyToClipboard);
  $(".copy .clipboard").click(function(){
      if( copyToClipboard($(this))){
        //alert("Code example copied to clipboard.");
        $(this).append(" <span id='tmp_ok' class='glyphicon glyphicon-ok' style='color: green'></span>");
        $("#tmp_ok").show().delay(3000).queue(function (next) {
            $(this).hide().remove();
            next();
        });
      }else
        $(this).append("<span id='tmp_nok' class='glyphicon glyphicon-remove' style='color: red'></span>");
        $("#tmp_nok").show().delay(3000).queue(function (next) {
            $(this).hide().remove();
            next();
        });
      return false;
  });
});


function copyToClipboard(link) {

    code = link.parent().find("code");
    elem = code[0];
    //alert(elem.parent().find("code").text());

    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {        
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
          
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }

    // select the content
    var currentFocus = document.activeElement;
    //target.focus();
    target.setSelectionRange(0, target.textContent.length);
    
    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    
    return succeed;
}

</script>

