<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>

<style>
.bittable{
      width:700px; 
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}

h3 a{
      text-decoration: none;
      color: #5F945F;
}

h3 a:hover{
      text-decoration: none;
      color: #5F945F;
}

h4{
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
      font-weight: bold;
      margin-bottom: 0px;
}

.step:hover{
      cursor: pointer;
      text-decoration: underline;
}

.txrx-ol {    
    display: block;
    padding-left: 25px;
    counter-reset:li; /* Initiate a counter */
}
.txrx-ol li {
      height: 24px;
      overflow-y: hidden;
      position: relative;
      padding-left: 40px;
    list-style:none; /* Disable the normal item numbering */
    /*background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
    background-position: -5px;
    background-repeat: no-repeat;
    padding-left: 7px;*/
}

.txrx-ol > li:before {
    font-weight: bold;
    content:counter(li); /* Use the counter as content */
    counter-increment:li; /* Increment the counter by 1 */
    /* Position and style the number */
    position:absolute;    
    /*left:-2.2em;*/
    left: 0px;
    width:2em;
    /* Some space between the number and the content in browsers that support
       generated content but not positioning it (Camino 2 is one example) */
    margin-right:8px;
    color:#fff;    
    font-weight:bold;
    text-align:center;
    background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
}


</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation/Datasheet'); ?>">Datasheet</a> &gt;
            Functional description
        </p>  

        <h2>Functional description</h2>            

        </div>  

      <div class="col-md-12">

      <h3>Overview</h3>	

      <ol style='margin-left: 20px'>
            <li><a href='#Wirelesscommunication'>Wireless communication</a></li>
            <li><a href='#Positioning'>Positioning</a></li>
            <li><a href='#Devicelist'>Device list</a></li>         
            <li><a href='#Interrupts'>Interrupts</a></li>
      </ol>
      <p>
      </p>

 
<h3><a name="Wirelesscommunication">Wireless communication</a></h3>
      <p>
      The Pozyx system provides single-hop wireless communication between neighboring pozyx devices by making
      use of the ultra-wideband signals. Every device in the network is uniquely identified by its
      16-bit network id stored in the
      <?php register_url("POZYX_NETWORK_ID"); ?> register.
      To make wireless communication possible between two devices, they must have the same UWB settings
      and the receiver must be turned on. 
      </p>

      <p>
      With Pozyx, it is possible to send custom data bytes or to wirelessly interact with the registers of a
      remote device, allowing for remote control of that device. This will be explained in the following subsections.
      </p>


<h4>Sending data</h4>
      <p>

      Transmission of data is initiated by device A by calling the register function 
      <?php register_url("POZYX_TX_SEND"); ?> with parameters the network id of device B and option REG_DATA.
      The transmission process is outlined in the figure on the right.<br>

      <img src="<?php echo(base_url('assets/images/docs/sending_data.png')); ?>" style="float: right; display: block; margin: auto; margin-bottom: 20px;; margin-left: 25px">
     
      <ol class="txrx-ol">
      <li><div class="step">Step 1</div><div>Device A sends the data in the TX buffer (accessible through <?php register_url("POZYX_TX_DATA");?>) to device B.
      </div></li>
      <li><div class="step">Step 2</div><div>Upon reception of the data, device B will<br>
      place the data in its RX buffer (accessible through <?php register_url("POZYX_RX_DATA");?>),<br>
      place the length of the received data in the register <?php register_url("POZYX_RX_DATA_LEN");?>,<br>
      place the network id of device A in <?php register_url("POZYX_RX_NETWORK_ID");?>, <br>and
      mark the RX_DATA bit in <?php register_url("POZYX_INT_STATUS");?>.
      If the interrupts are enabled, device B will also generate an interrupt.<br>
      Finally, device B will send a wireless acknowledgment to device A saying that it successfully received the data.
      </div></li>
      <li><div class="step">Step 3</div><div>Device A receives the acknowledgment (this does not fire an interrupt).</div></li>
      </ol>


      </p>
      <br style='clear: both'>

<h4>Remote register write</h4>
      <p>

      Performing a remote register write is initiated by device A by calling the register function 
      <?php register_url("POZYX_TX_SEND"); ?> with parameters the network id of device B and option REG_WRITE.
      The transmission process is outlined in the figure on the right.<br>

<img src="<?php echo(base_url('assets/images/docs/sending_data.png')); ?>" style="float: right; display: block; margin: auto; margin-bottom: 20px;; margin-left: 25px">
     
      <ol class="txrx-ol">
      <li><div class="step">Step 1</div><div>Device A sends the data in the TX buffer (accessible through <?php register_url("POZYX_TX_DATA");?>) to device B. 
      For a register write, the data packet that is transmitted must have the following content.

          <table class='table table-bordered table-condensed' style=' width:700px;'>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
              <td>Register address. The register address to start writing to.</td></tr>
          <tr><td>byte 1</td><td>data byte 0 (will be written at the register address)</td></tr>
          <tr><td>byte 2</td><td>data byte 1 (will be written at the register address + 1)</td></tr>
          <tr><td>...</td><td>...</td></tr>
          <tr><td>byte 99</td><td>data byte 98 (will be written at the register address + 98)</td></tr>
          </table>   

          </div>
      </li>
      <li><div class="step">Step 2</div><div>Upon reception of the data, device B will perform the requested register write operation. <br>     
      Finally, device B will send a wireless acknowledgment to device A saying that it successfully received the data.
      </div>
      </li>
      <li><div class="step">Step 3</div><div>Device A receives the acknowledgment (this does not fire an interrupt).</div></li>
      </ol>

      </p>
      <br style='clear: both'>

<h4>Remote register read</h4>
      <p>
      Performing a remote register read is initiated by device A by calling the register function 
      <?php register_url("POZYX_TX_SEND"); ?> with parameters the network id of device B and option REG_READ.
      The transmission process is outlined in the figure on the right.<br>

      <img src="<?php echo(base_url('assets/images/docs/sending_data.png')); ?>" style="float: right; display: block; margin: auto; margin-bottom: 20px;; margin-left: 25px">
     
      <ol class="txrx-ol">
      <li><div class="step">Step 1</div><div>Device A sends the data in the TX buffer (accessible through <?php register_url("POZYX_TX_DATA");?>) to device B.
      For a register read, the data packet that is transmitted must have the following content.
      
      <table class='table table-bordered table-condensed' style=' width:700px;'>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
              <td>Register address. The register address to start reading from.</td></tr>
          <tr><td>byte 1</td><td>Read length. The number of bytes to read in total.</td></tr>
          </table>         
      </div>

      </li>
      <li><div class="step">Step 2</div><div>Upon reception of the data, device B will process the register read command
      and send a data acknowledgment to device A that contains the requested register data.
      </div>
      </li>
      <li><div class="step">Step 3</div><div>
      Upon reception of the data acknowledgment, device A will<br>
      place the data in its RX buffer (accessible through <?php register_url("POZYX_RX_DATA");?>),<br>
      place the length of the received data in the register <?php register_url("POZYX_RX_DATA_LEN");?>,<br>
      place the network id of device A in <?php register_url("POZYX_RX_NETWORK_ID");?>, <br>and
      mark the RX_DATA bit in <?php register_url("POZYX_INT_STATUS");?>.
      If the interrupts are enabled, device A will also generate an interrupt.<br>
      </div>
      </li>
      </ol>


      </p>
      <br style='clear: both'>


<h4>Remote function call</h4>
      <p>
      Performing a remote register function is initiated by device A by calling the register function 
      <?php register_url("POZYX_TX_SEND"); ?> with parameters the network id of device B and option REG_FUNC.
      The transmission process is outlined in the figure on the right.<br>

      <img src="<?php echo(base_url('assets/images/docs/reg_function.png')); ?>" style="float: right; display: block; margin: auto; margin-bottom: 20px;; margin-left: 25px">
     
      <ol class="txrx-ol">
      <li><div class="step">Step 1</div><div>Device A sends the data in the TX buffer (accessible through <?php register_url("POZYX_TX_DATA");?>) to device B.
      For a register function call, the data packet that is transmitted must have the following content.
      
      <table class='table table-bordered table-condensed' style=' width:700px;'>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
              <td>Register address. The register address of the function</td></tr>
          <tr><td>byte 1</td><td>function parameter byte 0</td></tr>
          <tr><td>byte 2</td><td>function parameter byte 1</td></tr>
          <tr><td>...</td><td>...</td></tr>
          <tr><td>byte 99</td><td>function parameter byte 98</td></tr>
          </table>         
      </div>

      </li>
      <li><div class="step">Step 2</div><div>Upon reception of the function request, device B will process the register function command
      and send a data acknowledgment to device A that contains the function return data.
      </div>
      </li>
      <li><div class="step">Step 3</div><div>
      Upon reception of the data acknowledgment, device A will<br>
      place the data in its RX buffer (accessible through <?php register_url("POZYX_RX_DATA");?>),<br>
      place the length of the received data in the register <?php register_url("POZYX_RX_DATA_LEN");?>,<br>
      place the network id of device A in <?php register_url("POZYX_RX_NETWORK_ID");?>, <br>and
      mark the RX_DATA bit in <?php register_url("POZYX_INT_STATUS");?>.
      If the interrupts are enabled, device A will also generate an interrupt.<br>
      </div>
      <li><div class="step">Step 4 (optional)</div><div>
      For certain remote function calls, it is possible that device B will send a second response
      that contains data related to the function call. More specifically, for 
      POZYX_DO_RANGING and POZYX_DO_POSITIONING, device B will signal device A when this operation
      (which may take tens of milliseconds) has
      finished. Furthermore, the acknowledgment will also contain relevant data bytes to the function
      in order to reduce the number of wireless calls.
      </div></li>
      <li><div class="step">Step 5 (optional)</div><div>
      Upon reception of the data acknowledgment, device A will<br>
      place the data in its RX buffer (accessible through <?php register_url("POZYX_RX_DATA");?>),<br>
      place the length of the received data in the register <?php register_url("POZYX_RX_DATA_LEN");?>,<br>
      place the network id of device A in <?php register_url("POZYX_RX_NETWORK_ID");?>, <br>and
      mark the RX_DATA bit in <?php register_url("POZYX_INT_STATUS");?>.
      If the interrupts are enabled, device A will also generate an interrupt.<br>
      </div>
      </li>
      <li><div class="step">Step 6 (optional)</div>
      <div>Device B receives the acknowledgment (this does not fire an interrupt).</div></li>
      </ol>


      </p>
      <br style='clear: both'>


<h3><a name="Positioning">Positioning</a></h3>
      <p>
      The pozyx device is capable of obtaining accurate positioning information. 
      A description of how positioning works can be found here: <a href="<?php echo site_url('Documentation/doc_howDoesPositioningWork'); ?>">How does positioning work?</a>.
      The device can be configured to acquire positioning information at regular intervals or a single time.
      When positioning is finished, the position estimates and error estimates are stored in the positioning data registers.
      </p>

      <ol class="txrx-ol">
      <li><div class="step">Provide the anchor positions.</div>
      <div>
      For positioning, the coordinates of the anchors must be available in the device list. 
      This can be provided either manually or automatically.
      </div>
      </li>

      <li><div class="step">Configure the positioning algorithm (optional).</div>
      <div>
      By default, the Pozyx device is configured to perform 3D positioning using 4 anchors that 
      are automatically selected from the device list. These settings can be altered in order
      to obtain greater accuracy.<br>
      The following registers can be used for configuring the positioning algorithm:<br>
      <?php register_url("POZYX_POS_ALG");?>, 
      <?php register_url("POZYX_POS_NUM_ANCHORS");?>, and
      <?php register_url("POZYX_POS_SET_ANCHOR_IDS");?>.
      </div>

      </li>
      <li><div class="step">Start positioning.</div>
      <div>
      Positioning can be initiated in two ways. Either directly by calling <?php register_url("POZYX_DO_POSITIONING");?>
      which will estimate the tag position once. Or by setting the positioning interval in <?php register_url("POZYX_INTERVAL_MS");?>.     
      Whenever the positioning interval is set, a timer will start which will continuously perform positioning with
      a fixed interval in between the estimates.

      </div></li>
      <li><div class="step">Read out the positioning data.</div>
      <div>
      Depending on the algorithm configuration, positioning may take some time. Using the default settings, it takes
      approximately 70ms to obtain a position estimate. When positioning is finished, the pozyx device
      will signal this by setting the POS-bit high in <?php register_url("POZYX_INT_STATUS");?>. In case
      the interrupts are enabled, this event will cause an interrupt to signal the host that new positioning
      data is available. When positioning is finished, the following data can be read:<br>
      <div style="padding-left: 30px">
      <b>Coordinates:</b> the estimated coordinates are stored in the registers
      <?php register_url("POZYX_POS_X");?>, <?php register_url("POZYX_POS_Y");?>, 
      <?php register_url("POZYX_POS_Z");?> that can be read by the host.<br>
      <b>Error covariance:</b> information about the uncertainty of the position estimate is provided by the registers 
      starting from <?php register_url("POZYX_POS_ERR_XX");?>. These registers hold the estimated error covariance
      of the position estimate. This information can be used to assess the quality of the estimation or the
      anchor placement.<br>      
      <b>Selected anchors:</b> when Pozyx is configured for automatic anchor selection (in <?php register_url("POZYX_POS_NUM_ANCHORS");?>),
      the selected anchor's can be obtained using <?php register_url("POZYX_POS_GET_ANCHOR_IDS");?>.<br>
      <b>Range measurements:</b> the range measurements made during positioning can be obtained using 
      <?php register_url("POZYX_DEVICE_GETRANGEINFO"); ?>.
      </div>

      </div>

      </li>
      </ol>

      <br><br>
      

<!--
      <h4>One-shot positioning</h4>
      <p>

      First, make sure that the anchor positions are available to the tag. They must be stored in the internal device list.
      Secondly, we must tell the device which anchors should be used for positioning. This can be specified
      by the

      The pozyx device is configured by default to use 4 anchors that are automatically selected from 
      the device list. When you want to use more than 4 anchors, or you want to manually specify which
      anchors should be used, please use the following registers:
      <?php register_url("POZYX_POS_ALG");?>, 
      <?php register_url("POZYX_POS_NUM_ANCHORS");?>, 
      <?php register_url("POZYX_POS_SET_ANCHOR_IDS");?>.<br>
      Triggering the positioning itself is performed by calling the function register
      <?php register_url("POZYX_DO_POSITIONING");?>. Depending on the number of anchors used for positioning, 
      this process may take several tens of milliseconds. With 4 anchors, positioning takes about 70ms.
      
      The coordinates are stored in the registers
      <?php register_url("POZYX_POS_X");?>, <?php register_url("POZYX_POS_Y");?>, 
      <?php register_url("POZYX_POS_Z");?>.<br><br> 

      </p>
      



      <h4>Continuous positioning</h4>
      <p>
      Call POZYX_DEVICE_ADD for each anchor to add anchor data.<br>
      Enable the positioning data interrupts in POZYX_INT_MASK.<br>
      Configure POZYX_INTERVAL_MS to give a positioning update every 100ms.<br>
      Wait until an interrupt is triggered.<br>
      Read the event source from the POZYX_INT_STATUS register.<br>
      Obtain all positioning data by reading out 24 bytes starting from register POZYX_POS_X.<br>

      </p>
      <br>
      -->


<h3><a name="Devicelist">Device list</a></h3>

<img src="<?php echo(base_url('assets/images/docs/device_list.png')); ?>" style="float: right; display: block; margin: auto; margin-bottom: 20px;; margin-left: 25px">
     
      <p>
      Because the Pozyx device operates wirelessly, it must have a sense of pozyx devices in the network.
      To this end, an internal device list is maintained that stores all important information about
      a other network devices such as:
      </p>
      <ul>
      <li>The network identifier.</li>
      <li>The type: tag or anchor.</li>
      <li>The position (in case it is known for anchors).</li>
      <li>The range to the device.</li>
      <li>The signal strength indicator.</li>
      <li>A timestamp when it was last seen.</li>
      </ul>

      <p>
      The information inside the device list is used by the positioning algorithm      
      </p>

      <h4>Device discovery</h4>
      <p>
      This functionality automatically adds all neighboring pozyx devices in the internal device list.
      Note that all pozyx devices must be configured with the same ultra-wideband settings to make
      wireless communication possible. This function is triggered by calling the function register 
      <?php register_url("POZYX_DEVICES_DISCOVER");?>.
      </p>

      <h4>Anchor calibration</h4>
      <p>
      For positioning, the coordinates of the anchors must be known. In general, the accuracy of the coordinates
      of the anchors directly influences the positioning accuracy of the tag; roughly speaking, 10cm error in the
      position of the anchors will result in 10cm error in the position of the tags. Because of this, accurately
      defining the coordinates of the anchors is an important aspect of accurate positioning.
      Obtaining the anchor coordinates can be performed manually, by utilizing a laser meter, an accurate floor plan
      or some other method. Alternatively or it can be performed automatically by calling the function register
      <?php register_url("POZYX_DEVICES_CALIBRATE");?>. This function will automatically discover all neighboring
      pozyx devices and will estimate their relative coordinates.
      </p>


<h3><a name="Interrupts">Interrupts</a></h3>
      <p>  
      Pozyx has a number of interrupt events that can be configured to drive one of the two user-selectable IRQ output pins.
      The interrupt trigger is configured as raising edge.       
      A number of status registers are provided in the system to monitor and report data related to the interrupt.  
      More specifically, interrupts for certain events can be enabled or disabled in the 
      <?php register_url("POZYX_INT_MASK");?> register. 
      Interrupts are available for the following events
      </p>
      <ul style='margin-left:20px'>
      <li>Error event.</li>
      <li>New positioning data event.</li>
      <li>New IMU data event</li>
      <li>Wireless data received event</li>
      <li>Register Function ready event</li>

      </ul>
      <p>
      The event that triggered an interrupt can be read from the 
      <?php register_url("POZYX_INT_STATUS");?>      
      register. Note that even with interrupts disabled, 
      the interrupt status register will still indicate which events have occurred. This way, the system can be used without
      interrupts by regularly polling this register. However, polling should be avoided whenever possible because it slows
      down the overall system.<br>
      <br>
      By default, on power-up, all interrupt generating events are masked and interrupts are disabled.

      </p>
      <br>

</div>

      <div class="col-md-12" style='margin-top:50px'>
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation/Datasheet'); ?>">Datasheet</a> &gt;
            Functional description
        </p>          

        </div>  

</div>

<script>

$(".txrx-ol > li").one("click", function(){      
      $(this).find(".step").css('text-decoration', 'underline');
      $(this).css('height', 'auto');
});

</script>