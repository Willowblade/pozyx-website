<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>

<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

<style>
.bittable{
      width:700px; 
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}
.seperator{
  margin: 2px;
  padding: 2px;
}
</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation/Datasheet'); ?>">Datasheet</a> &gt;
            Register overview
        </p>  

        <h2>Register headers</h2>            

        </div>  

      <div class="col-md-12">

      <pre class="prettyprint" style="padding-left: 20px; overflow-x:scroll"><code><?php       
  
      echo "/* Begin auto generated defines */ \n";  
      echo "/* POZYX REGISTERS firmware version " . $reg_data[0]->version . " */ \n";  
  

        $category = "";
        $readable = "";
        $read_beginidx = -1;
        $read_idx = 0;
        $writable = "";
        $write_beginidx = -1;
        $write_idx = 0;
        $function_addr = "";
        $hidden = "";

        foreach($reg_data as $register){  

          if($register->is_hidden == 0)
          {

            if($category != $register->category){
              $category = $register->category;
              echo "\n/* ". $category ." */\n\n";

            }

            echo "#define ";
            echo $register->reg_name;
            $tabs = (64 - (8 + strlen($register->reg_name)))/8;
            for($k=1; $k<$tabs; $k++){
              echo "\t";
            }       
            echo $register->reg_address;
            echo "\t /* ";
            echo $register->description_short;
            echo " */\n";
          }else{
            if(!$this->ion_auth->logged_in())
              continue;

            $hidden .= "#define ";
            $hidden .= $register->reg_name;
            $tabs = (64 - (8 + strlen($register->reg_name)))/8;
            for($k=1; $k<$tabs; $k++){
              $hidden .= "\t";
            }       
            $hidden .= $register->reg_address;
            $hidden .= "\t /* ";
            $hidden .= $register->description_short;
            $hidden .= " */\n";
          }          

          // construct a macro function that tells if a register is readable or writable
          if($register->type == "R" || $register->type == "R/W"){
            if($read_beginidx == -1)
            {
              $read_beginidx = hexdec($register->reg_address);
              $read_idx = $read_beginidx;
            }

            if($read_idx != $register->reg_address)
            {
              $readable .= ' || (((x)>=0x'.dechex($read_beginidx).')&&((x)<0x'. dechex($read_idx) .'))';
              $read_beginidx = hexdec($register->reg_address);
            }
            $read_idx = hexdec($register->reg_address) + $register->num_bytes;            
          }

          if($register->type == "W" || $register->type == "R/W"){
            if($write_beginidx == -1)
            {
              $write_beginidx = hexdec($register->reg_address);
              $write_idx = $write_beginidx;
            }

            if($write_idx != $register->reg_address)
            {
              $writable .= ' || (((x)>=0x'.dechex($write_beginidx).')&&((x)<0x'. dechex($write_idx) .'))';
              $write_beginidx = hexdec($register->reg_address);
            }
            $write_idx = hexdec($register->reg_address) + $register->num_bytes;     

            //$writable .= '||((x)=='. $register->reg_address .')';
          }
        }

        $readable .= ' || (((x)>=0x'.dechex($read_beginidx).')&&((x)<0x'. dechex($read_idx) .'))';
        $writable .= ' || (((x)>=0x'.dechex($write_beginidx).')&&((x)<0x'. dechex($write_idx) .'))';

        if($hidden != "")
        {
          echo "\n/* Reserved registers */\n\n";
          echo $hidden;
        }

        ////// function registers
        $hidden = "";
        $func_idx = 0;
        $func_beginidx = -1;

        foreach($reg_functions as $register){  

          if($register->is_hidden == 0)
          {
            if($category != $register->category){
              $category = $register->category;
              echo "\n/*". $category ."*/\n\n";

            }

            echo "#define ";
            echo $register->reg_name;
            $tabs = (64 - (8 + strlen($register->reg_name)))/8;
            for($k=1; $k<$tabs; $k++){
              echo "\t";
            }    
            echo $register->reg_address;
            echo "\t /* ";
            echo $register->description_short;
            echo " */\n";
          }else{
            if(!$this->ion_auth->logged_in())
              continue;
            
            $hidden .= "#define ";
            $hidden .= $register->reg_name;
            $tabs = (64 - (8 + strlen($register->reg_name)))/8;
            for($k=1; $k<$tabs; $k++){
              $hidden .= "\t";
            }       
            $hidden .= $register->reg_address;
            $hidden .= "\t /* ";
            $hidden .= $register->description_short;
            $hidden .= " */\n";

          }   

          if($func_beginidx == -1)
          {
            $func_beginidx = hexdec($register->reg_address);
            $func_idx = $func_beginidx;
            //echo "--------------------------------". dechex($func_beginidx) . "\n\n";
          }
          if($func_idx != $register->reg_address)
          {
            $function_addr .= ' || (((x)>=0x'.dechex($func_beginidx).')&&((x)<0x'. dechex($func_idx) .'))';
            $func_beginidx = hexdec($register->reg_address);
          }
          $func_idx = hexdec($register->reg_address) + 1;             
        }

        $function_addr .= ' || (((x)>=0x'.dechex($func_beginidx).')&&((x)<0x'. dechex($func_idx) .'))';

        if($hidden != "")
        {
          echo "\n/* Reserved functions */\n\n";
          echo $hidden;
        }

        // create two functions to check if a register is writable or readable
     
        
        echo  "\n/* Macro's to test if registers are readable/writable */\n\n";
        if(strlen($readable)>0)
          echo "#define IS_REG_READABLE(x) \t\t\t ((". substr($readable, 4).")?1:0) \n";
        if(strlen($writable)>0)
          echo "#define IS_REG_WRITABLE(x) \t\t\t ((". substr($writable, 4).")?1:0) \n";
        if(strlen($function_addr)>0)
          echo "#define IS_FUNCTIONCALL(x) \t\t\t ((". substr($function_addr, 4).")?1:0) \n";
  
        echo "\n/* End of auto generated defines */ ";  
        
        ?>
</code></pre>

<?php
if($this->ion_auth->logged_in())
{
  $defines = "";
  $bit_defines = "";
  $tabs;
  $latest_REG = "";
  $index = 0;
  $filler_idx = 0;
  $filler_size = 0;
  $category = "";
  
  // create a list of defines
  
  $struct = "/* Begin auto generated struct */ \n\n";  
  $struct .= "/* POZYX REGISTER STRUCT */ \n";  
  $struct .= "typedef struct<br>{\n";
    
   foreach($reg_data as $register){  

      if($category != $register->category){
        $category = $register->category;
        $struct .= "\n/* ". $category ." */\n\n";
      }
         
      $reg_idx = hexdec($register->reg_address);
      if($reg_idx > $index){
       // add filler 
        $struct .= "\tuint8_t filler" . ($filler_idx++) . "[" . ($reg_idx-$index) . "];\n";
        $filler_size += $reg_idx-$index;
      }else if($reg_idx < $index){
        $struct = "ERROR: register indices not in ascending order!<br>or not enough space between registers.<br>";
        //$struct .= "ERROR located at line " + (i+1) + ": " + data[i][0];  
        break;
      }
      
      $struct .= "\t" . $register->internal . ";<br>";
      
      $index = $reg_idx+$register->num_bytes;    
    //}else if(data[i][0] != "" && data[i][1] == ""){
      //$struct += "<br>\t/* " + data[i][0] + " */<br><br>"; 
    //}
  }
  
  // add filler 
  $struct .= "\tuint8_t filler" . ($filler_idx++) . "[" . (256-$index) . "];\n";
  
  // close the struct
  $struct .= "}__attribute__ ((packed)) pozyx_register_t;";
  
  $filler_size += 256-$index;
  $struct .= "\n\n// Still " . $filler_size . " bytes available in the 256 byte register";

  echo "<br><h3>Struct</h3><br><pre class='prettyprint' style='padding-left: 20px; overflow-x:scroll'><code>" . $struct . "</code></pre>";
}
?>
  


</div>
</div>

<script>

function showDetails(row_id){
      $("#row_append" + row_id).toggle();//slideToggle(800);
      /*
      var info_row = $("#row_append" + row_id);
      if(info_row.length == 0) {
            //it doesn't exist
            $('#row'+row_id).after('<tr id="row_append'+row_id+'"><td colspan="6" style="padding-bottom:10px; padding-top:20px">This register identifies the Pozyx device. This can be used to make sure that the Pozyx is connected properly.<br><b>Default value:</b> 0x43.</td></tr>');
      }else{
            info_row.remove();
      }
      */

      return false;
}

function scroll_if_anchor(href) {

    href = typeof(href) == "string" ? href : $(this).attr("href");
    
    // You could easily calculate this dynamically if you prefer
    var fromTop = 75;
    
    // If our Href points to a valid, non-empty anchor, and is on the same page (e.g. #foo)
    // Legacy jQuery and IE7 may have issues: http://stackoverflow.com/q/1593174
    if(href.indexOf("#") == 0) {
        var $target = $('a[name='+ href.substr(1) +']');
        //var $target = $(href);   
        
        // Older browser without pushState might flicker here, as they momentarily
        // jump to the wrong position (IE < 10)
        //console.log($target);
        if($target.length) {           
            $('html, body').animate({ scrollTop: $target.offset().top - fromTop });
            if(history && "pushState" in history) {
                history.pushState({}, document.title, window.location.pathname + href);
                return false;
            }
        }
    }
}    

// When our page loads, check to see if it contains and anchor
//scroll_if_anchor(window.location.hash);

// Intercept all anchor clicks
$("body").on("click", "a", scroll_if_anchor);

</script>

