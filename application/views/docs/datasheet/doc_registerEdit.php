<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<style>
.bittable{
      width:700px; 
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}
.seperator{
  margin: 2px;
  padding: 2px;
}



</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation/Datasheet'); ?>">Datasheet</a> &gt;
            Register overview
        </p>  

        <h2>Edit register</h2>   

        <h3>Edit</h3>         

        </div>  

      
        
        <form action="<?php echo site_url('Documentation/Datasheet/RegisterSave'); ?>" method="post">
        <input type="hidden" name="id" value="<?php echo $register->id; ?>">

        <?php
       
        $this->form_builder->text('reg_name', 'register name', $register->reg_name, 'col-xs-6');
        $this->form_builder->text('reg_address', 'register address', $register->reg_address, 'col-xs-6');

        $this->form_builder->text('internal', 'Internal representation', $register->internal, 'col-xs-6');

        $types = array(
          (object) array('id' => 'R', 'name' => 'R'),
          (object) array('id' => 'R/W', 'name' => 'R/W'),
          (object) array('id' => 'F', 'name' => 'F'),
        );
        $this->form_builder->option('type', 'type', $types, $register->type, 'col-xs-3', 'form-control');
        
        
        $this->form_builder->text('default_val', 'default value', $register->default_val, 'col-xs-3');
        
        $this->form_builder->text('description_short', 'Description short', $register->description_short, 'col-xs-6');
        $this->form_builder->text('version', 'version', $register->version, 'col-xs-3');
        $this->form_builder->text('category', 'category', $register->category, 'col-xs-3');

        echo "<div class='clearfix'></div>";
        $this->form_builder->textarea('description_long', 'Description long', $register->description_long, 'col-xs-12', 'form-control', 15);

        

        echo "<div class='clearfix'></div>";

        $this->form_builder->button('submit_id', 'Edit', 'col-xs-3', 'btn-primary', '', 'submit');
        ?>
        </form>

        <div class="col-md-12">

        <h3>Preview</h3>

        <div id='preview'><?php echo $register->description_long; ?></div>



</div>
</div>


