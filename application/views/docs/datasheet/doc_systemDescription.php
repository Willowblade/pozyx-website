<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>

<style>
.bittable{
      width:700px; 
}

.bittable td{
      width:70px;
      text-align: center;
      font-size: 12px;
}

h3 a{
      text-decoration: none;
      color: #5F945F;
}

h3 a:hover{
      text-decoration: none;
      color: #5F945F;
}

h4{
      font-size: 16px;
      letter-spacing: 0.03em;
      line-height: 24px;
      color: #454545;
      font-weight: bold;
      margin-bottom: 0px;
}

.step:hover{
      cursor: pointer;
      text-decoration: underline;
}

.txrx-ol {    
    display: block;
    padding-left: 25px;
    counter-reset:li; /* Initiate a counter */
}
.txrx-ol li {
      height: 24px;
      overflow-y: hidden;
      position: relative;
      padding-left: 40px;
    list-style:none; /* Disable the normal item numbering */
    /*background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
    background-position: -5px;
    background-repeat: no-repeat;
    padding-left: 7px;*/
}

.txrx-ol > li:before {
    font-weight: bold;
    content:counter(li); /* Use the counter as content */
    counter-increment:li; /* Increment the counter by 1 */
    /* Position and style the number */
    position:absolute;    
    /*left:-2.2em;*/
    left: 0px;
    width:2em;
    /* Some space between the number and the content in browsers that support
       generated content but not positioning it (Camino 2 is one example) */
    margin-right:8px;
    color:#fff;    
    font-weight:bold;
    text-align:center;
    background-image: url('<?php echo(base_url('assets/images/docs/circled_number.png')); ?>');
}


</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation/Datasheet'); ?>">Datasheet</a> &gt;
            System description
        </p>  

        <h2>System description</h2>            

        </div>  

      <div class="col-md-12">

<!--
      <h3>Overview</h3>	

      <ol style='margin-left: 20px'>
            <li><a href='#Wirelesscommunication'>Wireless communication</a></li>
            <li><a href='#Positioning'>Positioning</a></li>
            <li><a href='#Devicelist'>Device list</a></li>         
            <li><a href='#Interrupts'>Interrupts</a></li>
      </ol>
      <p>
      </p>
-->
 
<h3><a name="Wirelesscommunication">Device diagram</a></h3>
      <p>
      In the figure below you can see the device diagram for the pozyx tag with all the high-level component blocks.      
      Note that the pozyx anchors are similar but without the motion sensors as they are assumed to be immobile.
      </p>

      <img src="<?php echo(base_url('assets/images/docs/system_tag.png')); ?>">

      <br><br>
      <p>
        The heart of he board is the microcontroller unit (MCU) that provides all the pozyx functionalities.
        The MCU communicates with all the on-board sensors and performs the ranging, positioning and calibration algorithms.
        The MCU also provides an interface to any external device (such as the Arduino) through the I2C protocol and an interrupt line.
        For the best performance, the MCU runs a real-time operating system, such that all functionalities are executed with little delay and high reliability.
        The MCU firmware can be upgraded through the onboard SWD interface or through USB.
      </p>

      <p>
        The board contains various sensors such as a magnetometer, gyroscope, accelerometer and pressure sensor (altimeter).
        The sensor data is used by the MCU for positioning and can be also be captured by the user by interfacing with the MCU.
        The board is also equipped with an ultra-wideband (UWB) chip that provides the wireless capability of the device. 
        The UWB chip requires a dedicated clock source which is calibrated during production up to 1 ppm for the best performance.
      </p>

      
      <br><br>     


</div>

      <div class="col-md-12" style='margin-top:50px'>
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation/Datasheet'); ?>">Datasheet</a> &gt;
            Functional description
        </p>          

        </div>  

</div>

<script>

$(".txrx-ol > li").one("click", function(){      
      $(this).find(".step").css('text-decoration', 'underline');
      $(this).css('height', 'auto');
});

</script>