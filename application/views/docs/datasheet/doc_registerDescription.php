<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>

<style>
.seperator{
  margin: 2px;
  padding: 2px;
}
</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation/Datasheet'); ?>">Datasheet</a> &gt;
            Register description
        </p>  

        <h2>Register description</h2>            

        </div>  

      <div class="col-md-12">

      <h3>0x0 - POZYX_WHO_AM_I</h3>



      <p>

<h3>0x1 - POZYX_FIRMWARE_VER</h3>
      The value of this register describes the firmware version installed on the devices. It is recommended to have
      all devices run on the same firmware version.<br>
      <br>
      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td colspan='4'>MAJOR</td><td colspan='4'>MINOR</td></tr>
      </table>

      <table class='table table-bordered table-condensed' style=' width:700px; '>
      <tr><th style='min-width:70px'>Bit flag</th><th>Description</th></tr>
      <tr>
      <td>MINOR</td><td>Indicate the minor version number<br>
      The full version is given as v{MAJOR}.{MINOR}.
      </td></tr>
      <td>MAJOR</td><td>Indicate the major version number<br>
      The full version is given as v{MAJOR}.{MINOR}.
      </td></tr>
      </table>  

<h3>0x2 - POZYX_HARDWARE_VER</h3>

      The value of this register describes the hardware type and version. The value is programmed during production and cannot
      be changed.<br>
      <br>
      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td colspan='3'>TYPE</td><td colspan='5'>VERSION</td></tr>
      </table>

      <table class='table table-bordered table-condensed' style=' width:700px; '>
      <tr><th style='min-width:70px'>Bit flag</th><th>Description</th></tr>
      <tr>
      <td>VERSION</td><td>Indicate the Hardware version. Possible values:<br>
      0x2 : version 1.2<br>
      0x3 : version 1.3<br>
      </td></tr>
      <td>TYPE</td><td>The type of hardware<br>
      0x0 : Pozyx anchor <br>
      0x1 : Pozyx Arduino Shield<br>
      </td></tr>
      </table>        

      
<h3>0x3 - POZYX_ST_RESULT</h3>
      <br>
      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td>-</td><td>-</td><td>UWB</td><td>PRESS</td><td>IMU</td><td>GYRO</td><td>MAGN</td><td>ACC</td></tr>
      </table>

      <table class='table table-bordered table-condensed' style=' width:700px; '>
      <tr><th>Bit flag</th><th>Description</th></tr>
      <tr>
      <td>ACC</td><td>A value of 1 means that the selftest passed for the accelerometer. A 0 means failure.</td></tr>
      <td>MAGN</td><td>A value of 1 means that the selftest passed for the mangetometer. A 0 means failure. This value is 0 for the pozyx anchors because they are not equipped with this sensor.</td></tr>
      <td>GYRO</td><td>A value of 1 means that the selftest passed for the gyroscope. A 0 means failure. This value is 0 for the pozyx anchors because they are not equipped with this sensor.</td></tr>
      <td>IMU</td><td>A value of 1 means that the selftest passed for the IMU microcontroller. A 0 means failure. This value is 0 for the pozyx anchors because they are not equipped with this sensor.</td></tr>
      <td>PRESS</td><td>A value of 1 means that the selftest passed for the pressure sensor. A 0 means failure. This value is 0 for the pozyx anchors because they are not equipped with this sensor.</td></tr>
      <td>UWB</td><td>A value of 1 means that the selftest passed for the ultra-wideband transceiver. A 0 means failure.</td>
      </tr>
      </table>



<h3>POZYX_STATUS</h3>

      
<h3>POZYX_ERRORCODE</h3>
      This register hold the error code whenever an error occurs on the pozyx platform. 
      The presence of an error is indicated by the ERR-bit in the <a href="#POZYX_INT_STATUS">POZYX_INT_STATUS</a> register.
      <br><br>
      Possible values:

      <table class='table table-bordered table-condensed' style=' width:700px; '>
      <tr><th>Value</th><th>Description</th></tr>
      
      <tr><td>0x00</td><td>POZYX_ERROR_NONE   </td></tr>
      <tr><td>0x01</td><td>POZYX_ERROR_I2C_WRITE   </td></tr>
      <tr><td>0x02</td><td>POZYX_ERROR_I2C_CMDFULL   </td></tr>
      <tr><td>0x03</td><td>POZYX_ERROR_ANCHOR_ADD    </td></tr>
      <tr><td>0x04</td><td>POZYX_ERROR_COMM_QUEUE_FULL </td></tr>
      <tr><td>0x05</td><td>POZYX_ERROR_I2C_READ    </td></tr>
      <tr><td>0x06</td><td>POZYX_ERROR_UWB_CONFIG    </td></tr>
      <tr><td>0x07</td><td>POZYX_ERROR_OPERATION_QUEUE_FULL</td></tr>
      <tr><td>0xA0</td><td>POZYX_ERROR_TDMA      </td></tr>
      <tr><td>0x08</td><td>POZYX_ERROR_STARTUP_BUSFAULT</td></tr>
      <tr><td>0x09</td><td>POZYX_ERROR_FLASH_INVALID </td></tr>
      <tr><td>0X0A</td><td>POZYX_ERROR_NOT_ENOUGH_ANCHORS</td></tr>      
      <tr><td>0X0B</td><td>POZYX_ERROR_DISCOVERY</td></tr>
      <tr><td>0x0C</td><td>POZYX_ERROR_CALIBRATION</td></tr>
      <tr><td>0x0D</td><td>POZYX_ERROR_FUNC_PARAM</td></tr>
      <tr><td>0x0E</td><td>POZYX_ERROR_ANCHOR_NOT_FOUND</td></tr>
      <tr><td>0xFF</td><td>POZYX_ERROR_GENERAL</td></tr>
      <tr><td>0xFF</td><td>POZYX_ERROR_GENERAL</td></tr>   
      </table>  



<h3>POZYX_INT_STATUS</h3>
      <br>
      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td>-</td><td>-</td><td>-</td><td>FUNC</td><td>RX_DATA</td><td>IMU</td><td>POS</td><td>ERR</td></tr>
      </table>

      <table class='table table-bordered table-condensed' style=' width:700px; '>
      <tr><th>Bit flag</th><th>Description</th></tr>
      <tr><td>ERR</td><td>Indicates that an has error occured.</td></tr>
      <tr><td>POS</td><td>Indicates that a new position estimate is available.</td></tr>
      <tr><td>IMU</td><td>Indicates that a new IMU measurement is available.</td></tr>      
      <tr><td>RX_DATA</td><td>Indicates that the pozyx device has received some data over its wireless uwb link.</td></tr>
      <tr><td>FUNC</td><td>Indicates that a register function call has finished (excluding positioning).</td></tr>
      </table>      
      
<h3>POZYX_CALIB_STATUS</h3>
      <br>
      Part of the calibration of the motion sensors occurs in the background when the system is running. 
      For this calibration, each sensor requires its own typical device movements to become fully calibrated.
      This register contains information about the calibration status of the motion sensors.<br>
      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td colspan="2">SYS Calib Status</td><td colspan="2">GYR Calib Status</td><td colspan="2"> ACC Calib Status</td><td colspan="2">MAG Calib Status</td></tr>
      </table>

      <table class='table table-bordered table-condensed' style=' width:700px; '>
      <tr><th>Bit flag</th><th>Description</th></tr>
      <tr><td>SYS Calib Status</td><td>
        Current system calibration status, depends on status of all sensors. Possible values<br>
        0x3 : indicates fully calibrated<br>
        0x0 : indicates not calibrated<br>

      </td></tr>
      <tr><td>GYR Calib Status</td><td>
        Current gyroscope calibration status, depends on status of the gyroscope. Possible values<br>
        0x3 : indicates fully calibrated<br>
        0x0 : indicates not calibrated<br>
        Place the device in a single stable position for a period of few seconds to allow the gyroscope to calibrate.

      </td></tr>
      <tr><td>ACC Calib Status</td><td>
        Current accelerometer calibration status, depends on status of the accelerometer. Possible values<br>
        0x3 : indicates fully calibrated<br>
        0x0 : indicates not calibrated<br>
        Place the device in 6 different stable positions for a period of few seconds to allow the accelerometer to calibrate.
        Make sure that there is slow movement between 2 stable positions.
        The 6 stable positions could be in any direction, but make sure that the device is lying at least once perpendicular to the x, y and z axis.

      </td></tr>
      <tr><td>MAG Calib Status</td><td>
        Current magnetometer calibration status, depends on status of the magnetometer. Possible values<br>
        0x3 : indicates fully calibrated<br>
        0x0 : indicates not calibrated<br>
        Make some random movements (for example: writing the number '8' on air) to allow calibration of the magnetometer.
        The magnetometer is very susceptible to external magnetic fields, When the magnetometer is not calibrated, it may
        not be used to compute the device orientation, which may result in erroneous absolute heading information.

      </td></tr>
     
      </table>    


<h3>POZYX_INT_MASK</h3>
      <br>
      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td>PIN</td><td>-</td><td>-</td><td>FUNC</td><td>RX_DATA</td><td>IMU</td><td>POS</td><td>ERR</td></tr>
      </table>

      <table class='table table-bordered table-condensed' style=' width:700px; '>
      <tr><th>Bit flag</th><th>Description</th></tr>
      <tr><td>ERR</td><td>Enables interrupts whenever an error occurs.</td></tr>
      <tr><td>POS</td><td>Enables interrupts whenever a new positiong update is availabe.</td></tr>
      <tr><td>IMU</td><td>Enables interrupts whenever a new IMU update is availabe.</td></tr>
      <tr><td>RX_DATA</td><td>Enables interrupts whenever data is received through the ultra-wideband network.</td></tr>
      <tr><td>FUNC</td><td>Enables interrupts whenever a register function call has completed.</td></tr>
      <tr><td>PIN</td><td>Configures the interrupt pin.<br>Value 0: Pin 0.<br>Value 1: Pin 1.</td>

      </tr>
      </table>
      
<h3>POZYX_INT_CONFIG</h3>
      This register configures the external interrupt pin of the Pozyx device. 
      It should be configured in combination with the POZYX_INT_MASK register.<br>
      <br>
      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td>-</td><td>-</td><td>LATCH</td><td>ACT</td><td>MODE</td><td colspan="3">PINNUM</td></tr>
      </table>

      <table class='table table-bordered table-condensed' style=' width:700px; '>
      <tr><th>Bit flag</th><th>Description</th></tr>
      <tr><td>PINNUM</td><td>Selects the pin used by the interrupt. Possible values:<br>
      0 - No pin <b>(default)</b><br>
      1 - GPIO pin 1 (pin 9 on the pozyx tag)<br>
      2 - GPIO pin 2 (pin 10 on the pozyx tag)<br>
      3 - GPIO pin 3 (pin 11 on the pozyx tag)<br>
      4 - GPIO pin 4 (pin 12 on the pozyx tag)<br>
      5 - GPIO pin 5 (pin 2 on the pozyx tag)<br>
      6 - GPIO pin 6 (pin 3 on the pozyx tag)
      </td></tr>
      <tr><td>MODE</td><td>Select the interrupt pin mode. Possible values:<br>
      0 - Push-pull <b>(default)</b>: the pin will actively set the interrupt line high or low. The line cannot be shared with multiple devices.<br>      
      1 - open drain: this allows the user to share the interrupt line with multiple devices. This mode requires an external pull-up or pull-down resistor.<br> 
      </td></tr>
      <tr><td>ACT</td><td>The voltage level when an interrupt happens. Possible values:<br>
      0 - active low <b>(default)</b>: 0V<br>      
      1 - active high: 3.3V
      </td></tr>
      <tr><td>LATCH</td><td>Select if the interrupt pin should latch after an interrupt. Possible values:<br>
      0 - No latch <b>(default)</b>: the interrupt is a short pulse of about 6&micro;s<br>
      1 - Latch: after an interrupt, the interrupt pin will stay at the active level until the POZYX_INT_STATE register is read from      
      </td></tr>     

      </tr>
      </table>

<h3>POZYX_POS_NUM_ANCHORS</h3>
      <br>
      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td>MODE</td><td>-</td><td>-</td><td>-</td><td colspan='4'>NUM</td></tr>
      </table>

      <table class='table table-bordered table-condensed' style=' width:700px; '>
      <tr><th style='min-width:70px'>Bit flag</th><th>Description</th></tr>
      <tr>
      <td>NUM</td><td>4 bits to indicate the maximum number of anchors to be used for positioning. Value between 3 and 15.

      </td></tr>
      <td>MODE</td><td>a single bit to indicate wether to choose from a fixed set of anchors or perform automatic anchor selection. Possible values:<br>
      0 : indicates fixed anchor set. The anchor network IDs that have been supplied by POZYX_POS_SET_ANCHOR_IDS are used for positioning.<br>
      1 : indicates automatic anchor selection. Anchors from the internal anchor list are used to make the selection.
      </td></tr>
      </table>   

<h3>POZYX_POS_INTERVAL</h3>
      Pozyx can be run in continuous mode to provide continuous positioning and/or ranging data. The interval in milliseconds between successive updates can be configured with this register. The value is capped between 10ms and 60000ms (1 minute). Writing the value 0 to this registers disables the continuous mode.



<h3>POZYX_CONFIG_GPIO1</h3>
      <br>
      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td>-</td><td>LATCH</td><td>ACT</td><td colspan='2'>PULL</td><td colspan='3'>MODE</td></tr>
      </table>

      <table class='table table-bordered table-condensed' style=' width:700px; '>
      <tr><th style='min-width:70px'>Bit flag</th><th>Description</th></tr>
      <tr>
      <td>MODE</td><td>Indicates the input or output mode of the pin<br>
      0 : digital input<br>
      1 : digital output (push-pull)<br>
      2 : digital output (open-drain)<br>
      3 : digital interrupt output (push-pull)<br>
      4 : digital interrupt output (open-drain)<br>
      </td></tr>
      <td>PULL</td><td>When selecting input or open-drain output, the pin can be internally
      connected with a pull-up (to 3.3V) or pull-down (to 0V) resistor.
      <br>
      0 : no pull-up or pull-down resistor. <br>
      1 : pull-up resistor. <br>
      2 : pull-down resistor
      </td></tr>
      <td>ACT</td><td>When in push-pull interrupt mode, this bit indicates the interrupt pin level in active mode
      <br>
      0 : active low.<br>
      1 : active high.<br>
      </td></tr>
      <td>LATCH</td><td>When in interrupt mode, this bit indicates the latch behaviour of the interrupt.
      <br>
      0 : no latch. The interrupt is a small pulse of 50µs wide.<br>
      1 : latch. The pin level is held until the user reads the INT_STATUS register.<br>
      </td></tr>
      </table>   
      <br><br>   

<h3>POZYX_GPIO1</h3>
      <br>
      This register can be read from to obtain the current state of the GPIO pin if it is configured as an input.
      When the pin is configured as an output, the value written will determine the new state of the pin.
      <br><br>
      Possible values:<br>
      0 : The digital state of the pin is LOW at 0V.<br>
      1 : The digital state of the pin is HIGH at 3.3V.<br>
      <b>Default value: </b> 0<br>
      <br>

<h3>POZYX_POS_ALG</h3>
      This register selects and configures the positioning algorithm used by the pozyx device.<br>
      <br>
      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td>-</td><td>-</td><td colspan='2' align='center'>DIM</td><td colspan='4' align='center'>ALGORITHM</td></tr>
      </table>

      <table class='table table-bordered table-condensed' style=' width:700px; '>
      <tr><th style='min-width:70px'>Bit flag</th><th>Description</th></tr>
      <tr>
      <td>ALGORITHM</td><td>Indicates which algorithm to use for positioning<br>
      0x0 : UWB-only <b>(Default value)</b>. This algorithm estimates the position using UWB measurments only. It is specifically designed
      to deal with NLOS-measurements.<br>
      <hr class='seperator'>
      0x1 : Tracking. This tracking algorithm is designed for high speed operation at update rates > 1Hz. It
      uses UWB-measurements, IMU data and information from previous timesteps.<br>
      <hr class='seperator'>
      0x2 : Least-Squares. This algorithm is described on the Pozyx website in the 
       <a href="<?php echo site_url('Documentation/doc_howDoesPositioningWork'); ?>">documentation</a>. It is a simple algorithm
       that only uses UWB-measurments and that cannot handle NLOS range measurements well.

      <br>
      </td></tr>      
      <tr>
      <td>DIM</td><td>This indicates the spatial dimension. Possible values:<br>
      0x2 : 2D <b>(Default value)</b>. In two dimensional mode, the x and y coordinates are estimated. It is expected that all tags and anchors are located in the same horizontal plane.<br>
      <hr class='seperator'>
      0x1 : 2,5D. In this mode, the x and y coordinates are estimated. However, anchors and tags are not required to be located in the same horizontal plane. For this mode
      it is necessary that the z-coordinates of the anchors and tag are known. For the tag it must be stored in the <?php register_url("POZYX_POS_Z"); ?> register. In general this mode 
      results in superior positioning accuracy as compared to full 3D when the anchors cannot be placed well. It is especially usefull when the 
      tag is mounted on a robot or VR-helmet.<br>
      <hr class='seperator'>
      0x3 : 3D. In three dimensional mode, the x,y and z coordinates are estimated. In order to obtain a good vertical accuracy, 
      it is required to place the anchors at different heights. Check out the <a href="<?php echo site_url('Documentation/doc_whereToPlaceTheAnchors'); ?>">documentation</a> for more information on anchor placement.

      </td></tr> 
      </table>   
      <br><br>  

<h3>POZYX_UWB_CHANNEL</h3>
Select the ultra-wideband transmission and reception channel. 
In general the transmission range increases at lower frequencies, i.e., lower channels.

Allow up to 20ms to let the device switch channel.
<br><b>Warning: </b>to enable wireless communication between two devices they must operate on the same channel.<br><b>Default value: </b>0x05

      <br><br>
      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td colspan='8'>UWB_CHANNEL</td></tr>
      </table>

      <table class='table table-bordered table-condensed'>
      <tr><th>Bit flag</th><th>Description</th></tr>
      <tr>
      <td>UWN_CHANNEL</td><td>Indicate the UWB channel. Possible values:<br>
1 : Centre frequency 3494.4MHz, using the band(MHz): 3244.8 – 3744, bandwidth 499.2 MHz <br>
2 : Centre frequency 3993.6MHz, using the band(MHz): 3774 – 4243.2, bandwidth 499.2 MHz<br>
3 : Centre frequency 4492.8MHz, using the band(MHz): 4243.2 – 4742.4 bandwidth 499.2 MHz<br>
4 : Centre frequency 3993.6MHz, using the band(MHz): 3328 – 4659.2 bandwidth 1331.2 MHz (capped to 900MHz) <br>
5 : Centre frequency 6489.6MHz, using the band(MHz): 6240 – 6739.2 bandwidth 499.2 MHz<br>
7 : Centre frequency 6489.6MHz, using the band(MHz): 5980.3 – 6998.9 bandwidth 1081.6 MHz (capped to 900MHz)       </td></tr>
      </table> 


<h3>POZYX_UWB_RATES</h3>
      This register describes the UWB bitrate and nominal pulse repition frequency (PRF). 
      Generally speaking, lower data rates give increased receiver sensitivity, increased link margin and longer range but due to longer frame lengths for a given number of data bytes they result in increased air occupancy per frame and a reduction in the number of individual transmissions that can take place per unit time.  <br>
      16 MHz PRF gives a marginal reduction in transmitter power consumption over 64 MHz PRF. 16 MHz and 64 MHz PRF can coexist on the same physical channel without interfering. <br>
       For more information we refer to UWB settings.
      <br><br>
      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td>-</td><td>-</td><td>-</td><td>-</td><td colspan='2'>PRF</td><td colspan='2'>BITRATE</td></tr>
      </table>

      <table class='table table-bordered table-condensed' style=' width:700px; '>
      <tr><th style='min-width:70px'>Bit flag</th><th>Description</th></tr>
      <tr>
      <td>BITRATE</td><td>Indicate the UWB bitrate. Possible values:<br>
      0 : bitrate 110 kbits/s (Default value)<br>
      1 : bitrate 850 kbits/s<br>
      2 : bitrate 6.8 Mbits/s<br>
      </td></tr>
      <td>PRF</td><td>Indicates the pulse repetition frequency to be used. Possible values<br>
      1 : 16 MHz <br>
      2 : 64 MHz (default value) <br>
      </td></tr>
      </table>    


<a name="POZYX_UWB_PLEN"><h3>POZYX_UWB_PLEN</h3></a>
      This register describes the preamble length of the UWB wireless packets. In general, a larger preamble length
      will result in increased range at the expense of longer airtime. For more information we refer to UWB settings.
      <br><br>
      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td colspan='8'>PLEN</td></tr>
      </table>

      <table class='table table-bordered table-condensed' style=' width:700px; '>
      <tr><th style='min-width:70px'>Bit flag</th><th>Description</th></tr>
      <tr>
      <td>PLEN</td><td>Indicate the UWB preamble length. Possible values:<br>
      
        0x0C : 4096 symbols. Standard preamble length 4096 symbols<br>
        0x28 : 2048 symbols. Non-standard preamble length 2048 symbols<br>
        0x18 : 1536 symbols. Non-standard preamble length 1536 symbols<br>
        0x08 : 1024 symbols. Standard preamble length 1024 symbols <b>(default value)</b><br>
        0x34 : 512  symbols. Non-standard preamble length 512 symbols<br>
        0x24 : 256  symbols. Non-standard preamble length 256 symbols<br>
        0x14 : 128  symbols. Non-standard preamble length 128 symbols<br>
        0x04 : 64   symbols. Standard preamble length 64 symbols<br>

      </td></tr>
      </table> 

<a name="POZYX_UWB_GAIN"><h3>POZYX_UWB_GAIN</h3></a>
      <b>Warning: </b>when changing channel, bitrate or preamble length, the power is also overwritten to the default value for this UWB configuration.<br>
      <b>Warning: </b>changing this value can make the Pozyx device fall out of regulation.<br><br>

      This register contains the UWB transmit gain. A larger gain will result in more tranmission power and a larger range.
      Before using this device in a non-experimental setup, the gain must be adjusted to meet regulations.
      Possible values are between 0 and 67. 1dB = 2 int.

<a name="POZYX_UWB_XTALTRIM"><h3>POZYX_UWB_XTALTRIM</h3></a>
      This register contains the trimming value to fine-tune the operating frequency of the crystal oscillator used by the ultra-wideband front-end. 
      By carefully selecting this value, the operating frequency can be tuned to an error within 1ppm. A small error on the operating frequency will
      increase the sensitivity of the UWB receiver. This register is calibrated by default. However, due to crystal aging or 
      under varying temperature ranges the calibration should be redone. The xtal trimming value is preset during production.
      <br><br>

      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td>-</td><td>-</td><td>-</td><td colspan='5'>TRIM</td></tr>
      </table>
      <br>

<a name="POZYX_TRIM_STARTMEASURE"><h3>POZYX_TRIM_STARTMEASURE</h3></a>
      This function starts the measurement the number of DW ticks in the DW in a one second interval (one tick is 1ms/249600~=4ns. The interval should be provided with a very accurate external PPS signal at GPIO-0. This function will not change the trim value.
      <br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes 0 bytes as input.<br><br>         

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success.

<a name="POZYX_TRIM_STOPMEASURE"><h3>POZYX_TRIM_STOPMEASURE</h3></a>
      This functions stops the measuring of the number of DW ticks in the DW in a one second interval.
      <br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes 0 bytes as input.<br><br>         

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success.

<a name="POZYX_TRIM_RESULT"><h3>POZYX_TRIM_RESULT</h3></a>
      This functions returns the latest number of DW ticks in the DW in a one second interval.
      In order to obtain information from this function, POZYX_TRIM_STARTMEASURE should be called first.
      <br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes 0 bytes as input.<br><br>         

      <b>OUTPUT</b><br>
      The function returns 5 bytes of information.<br><br>

      <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
           <td>Return byte indicating success or failure of the function. 0 is failure. 1 is success.                  
           </td></tr>

          <tr><td>byte 1-4</td>
          <td>The number of ticks in a one second interval.
          </td></tr>
          </tr>
      </table>

<a name="POZYX_PRODUCTION_TRIM"><h3>POZYX_PRODUCTION_TRIM</h3></a>    
      Initiate the calibration of the onboard crystal (xtal) by setting the XTAL trim value. 
      This functionality requires that a very accurate extermal PPS signal is connected to pin GPIO-0.
      Upon completion of the trimming procedure, an interrupt will be generated and the XTAL trim value can 
      read from the <a href="#POZYX_UWB_XTALTRIM">POZYX_UWB_XTALTRIM</a> register.
      <br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes 0 bytes as input.<br><br>         

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success.
  

<a name="POZYX_CONFIG_LEDS"><h3>POZYX_CONFIG_LEDS</h3></a>
      This register configures the functionality of the 6 LEDs on the pozyx device. 
      At all times, the user can control LEDs 1 through 4 using 
      <a href="#POZYX_LED_CTRL">POZYX_LED_CTRL</a>.<br><br>

      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td>-</td><td>-</td><td>LED_RX</td><td>LED_TX</td><td>LED_4</td><td>LED_3</td><td>LED_2</td><td>LED_1</td></tr>
      </table>
      <br>

      <table class='table table-bordered table-condensed descr-table'> 
      <tr><th style='min-width:70px'>Bit flag</th><th>Description</th></tr>
      
      <tr>
      <td>LED_1</td><td>Possible values:<br>
      0 : The LED is not controlled by the Pozyx system.<br>
      1 : The LED is controlled by the Pozyx system. The LED will blink (roughly) every 2s to indicate that the device is working properly.<br>      
      </td></tr>
      <tr>

      <tr>
      <td>LED_2</td><td>Possible values:<br>
      0 : The LED is not controlled by the Pozyx system.<br>
      1 : The LED is controlled by the Pozyx system. 
      The LED will be turned on when the device is performing a register write operation or a register function (i.e., calibrating, positioning, ..). <br>      
      </td></tr>
      <tr>

      <tr>
      <td>LED_3</td><td>Possible values:<br>
      0 : The LED is not controlled by the Pozyx system.<br>
      1 : The LED is controlled by the Pozyx system. Currently no purpose is assigned to this LED.<br>      
      </td></tr>
      <tr>


      <tr>
      <td>LED_4</td><td>Possible values:<br>
      0 : The LED is not controlled by the Pozyx system.<br>
      1 : The LED is controlled by the Pozyx system. The LED is turned on whenever an error occurs.
      The error can be read from the <a href="#POZYX_ERRORCODE">POZYX_ERRORCODE</a> register.<br>      
      </td></tr>
      <tr>   

      <tr>
      <td>LED_TX</td><td>Possible values:<br>
      0 : The LED will not blink upon transmission of an UWB message.<br>
      1 : The LED will blink upon transmission of an UWB message.<br>      
      </td></tr>
      <tr>  

      <tr>
      <td>LED_RX</td><td>Possible values:<br>
      0 : The LED will not blink upon reception of an UWB message.<br>
      1 : The LED will blink upon reception of an UWB message.<br>      
      </td></tr>
      <tr>      

      </table>      


<a name="POZYX_OPERATION_MODE"><h3>POZYX_OPERATION_MODE</h3></a>
      This register defines the operation modus of the pozyx device. It's default value is determined by the
      presence of the T/A jumper on the pozyx board. When the jumper is present, the device is in tag mode.
      Note that the operation mode is independent of the hardware type.<br><br>

      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>OP MODE</td></tr>
      </table>
      <br>

      <table class='table table-bordered table-condensed' style=' width:700px; '>
      <tr><th>Bit flag</th><th>Description</th></tr>
      <tr>
      <td>OP MODE</td><td>Possible values:<br>      
      0 : Anchor mode. In anchor mode the device is assumed to be immobile. The device can be used
      by other devices for positioning.
      <br>
      1 : Tag mode. In tag mode, the device can more around. In this mode the device cannot be used by other
      devices for positioning.<br>        
      <br>
      </td></tr>

      </tr>
      </table>


<a name="POZYX_SENSORS_MODE"><h3>POZYX_SENSORS_MODE</h3></a>
      This register configures the mode of operation for the onboard sensors.
      It takes about 7ms to switch between two modes of operation, or 19ms to go from MODE_OFF to any other mode.
      For more information, we refer to <a href="">sensor settings</a>.
      <br><br>

      Register contents:<br>
      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>
      <tr><td><b>Bit flag</b></td>
      <td>-</td><td>-</td><td>-</td><td>-</td><td colspan='4'>SENSOR MODE</td></tr>
      </table>
      <br>

      <table class='table table-bordered table-condensed' style=' width:700px; '>
      <tr><th>Bit flag</th><th>Description</th></tr>
      <tr>
      <td>SENSOR MODE</td><td>Possible values:<br>
      Non-fusion modes:<br>
      0 : MODE_OFF<br>
      1 : ACCONLY <br>
      2 : MAGONLY <br>
      3 : GYROONLY <br>
      4 : ACCMAGx<br>
      5 : ACCGYRO <br>
      6 : MAGGYRO <br>
      7 : AMG <br>
      <br>
      Fusion modes:<br>
      8 : IMU <br>
      9 : COMPASS <br>
      10 : M4G <br>
      11 : NDOF_FMC_OFF<br>
      12 : NDOF <br>
      <br>
      </td></tr>

      </tr>
      </table>


<a name="POZYX_DEVICE_LIST_SIZE"><h3>POZYX_DEVICE_LIST_SIZE</h3></a>
      See the <a href="<?php echo site_url('Documentation/Datasheet/FunctionalDescription'); ?>">functional description</a> for more info on the device list.

<a name="POZYX_RX_NETWORK_ID"><h3>POZYX_RX_NETWORK_ID</h3></a>
      See the <a href="<?php echo site_url('Documentation/Datasheet/FunctionalDescription'); ?>">functional description</a> for more info on receiving messages.

<a name="POZYX_RX_DATA_LEN"><h3>POZYX_RX_DATA_LEN</h3></a>
      See the <a href="<?php echo site_url('Documentation/Datasheet/FunctionalDescription'); ?>">functional description</a> for more info on receiving messages.


<a name="POZYX_TX_DATA"><h3>POZYX_LED_CTRL </h3></a>
      This function gives control over the 4 onboard pozyx LEDS.
      <br>
      <br>
      <b>INPUT PARAMETERS</b><br>
      As an input a single byte is required.

      <table class='table table-bordered table-condensed bittable'>
      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

      <tr><td><b>Bit flag</b></td>
      <td>USELED_4</td><td>USELED 3</td><td>USELED 2</td><td>USELED 1</td>
      <td>LED_4</td><td>LED_3</td><td>LED_2</td><td>LED_1</td></tr>
      </table>

      <table class='table table-bordered table-condensed descr-table'>
      <tr><th style='min-width:70px'>Bit flag</th><th>Description</th></tr>
      <tr>
      <td>LED_1<br>LED_2<br>LED_3<br>LED_4<br></td><td>Give the status of the given LED . Possible values:<br>
      0 : Turn off LED<br>
      1 : Turn on LED<br>      
      </td></tr>
      <tr>      
      <tr>
      <td>USELED_1<br>USELED_2<br>USELED_3<br>USELED_4<br></td><td>Bit to indicate if we want to override the status of the given LED.

      Possible values:<br>
      0 : Do not overwrite status of LED<br>
      1 : Overwrite status of LED<br>      
      </td></tr>
      <tr>
      </table>      

      
      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success. <br>

<a name="POZYX_TX_DATA"><h3>POZYX_TX_DATA</h3></a>
      This function fills the transmit buffer with data bytes. Ready to be transmitted on the next call to 
      <a href="#POZYX_TX_SEND">POZYX_TX_SEND</a>. 
      <br>
      <br>
      <b>INPUT PARAMETERS</b><br>
      This function takes 3 bytes as input parameters.<br><br>
          <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
              <td>Buffer offset. This value indicates where the offset inside the transmit buffer to start writing the 
              the data bytes. To start from the beginning of the buffer, use the value 0.</td></tr>
          <tr><td>byte 1</td><td>data byte 0</td></tr>
          <tr><td>byte 2</td><td>data byte 1</td></tr>
          <tr><td>...</td><td>...</td></tr>
          <tr><td>byte 100</td><td>data byte 99</td></tr>
          </table>      

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure (for example when the data buffer is full). 1 is success. 


<a name="POZYX_TX_SEND"><h3>POZYX_TX_SEND</h3></a>
      This function initiates the wireless transfer of all the data stored in the transmitter buffer 
      (which should be filled by calling <a href="#POZYX_TX_DATA">POZYX_TX_DATA</a>). Upon successful reception of the
      message by the destination node, the destination node will answer with an acknowledgement (ACK) message. When a REG_READ or a REG_FUNC message
      was transmitted, the ACK will contain the requested data and the RX_DATA bit is set <a href="#POZYX_INT_STATUS">POZYX_INT_STATUS</a> and 
      an interrupt is fired if the RX_DATA bit is enabled in <a href="#POZYX_INT_MASK">POZYX_INT_MASK</a>. The received ACK data
      can be read from <a href="#POZYX_RX_DATA">POZYX_RX_DATA</a>. Depending on the UWB settings, it may take a few up to
      tens of milliseconds before an ACK is to be expected. After sending the data, the transmission buffer is emptied.
      <br>
      <br>
      <b>INPUT PARAMETERS</b><br>
      This function takes 3 bytes as input parameters.<br><br>
          <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
          <td rowspan='2'>Network address of destination node.<br>Type: uint16_t</td></tr>
          <tr><td>byte 1</td></tr>
          <tr><td>byte 2</td><td>Option byte. Possible values are:<br><br>

            <table class='table table-condensed' >
            <!--<tr><th style='min-width:70px'>Value</th><th>Short name</th><th>Description</th></tr>-->
            <tr>
            <td>0x02</td><td>REG_READ</td><td>Perform a wireless register read operation. The first databyte in the transmit buffer should be the register address.</td></tr>
            <td>0x04</td><td>REG_WRITE</td><td>Perform a wireless register write operation. The first databyte in the transmit buffer should be the register address. The sencond by the number of bytes that will be written, the following bytes are the actual data bytes to be written</td></tr>
            <td>0x08</td><td>REG_FUNC</td><td>Perform a wireless register write operation. The first databyte in the transmit buffer should be the function address, followed by the parameter bytes.</td></tr>
            <td>0x06</td><td>REG_DATA</td><td>Simply send the data stored in the transmit buffer to the destination. The destination will place the received data in a buffer accessible by the RX_DATA function.</td></tr>
            </table>    
          
          </td></tr>
          </table>      

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success. 


<a name="POZYX_RX_DATA"><h3>POZYX_RX_DATA</h3></a>
      This function let's you read from the received data buffer. 
      This buffer is filled whenever data is wirelessly received by the UWB receiver.
      Upon this event, the RX_DATA bit is set <a href="#POZYX_INT_STATUS">POZYX_INT_STATUS</a> and 
      an interrupt is fired if the RX_DATA bit is enabled in <a href="#POZYX_INT_MASK">POZYX_INT_MASK</a>.
      The RX data buffer is cleared after reading it. When a new data message arrives before the RX buffer
      was read, it will be overwritten and the previously received data will be lost.
      Note that the receiver must be turned on the receive incoming messages. This is done automatically
      whenever an acknowledgment is expected (after a <a href="#POZYX_TX_SEND">POZYX_TX_SEND</a> operation).
      <br>
      <br>
      <b>INPUT PARAMETERS</b><br>
      This function takes 2 bytes as input parameters.<br><br>

          <table class='table table-bordered table-condensed descr-table'>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
              <td>buffer offset (optional). This value indicates where the offset inside the receive buffer to start reading the 
              the data bytes. To start from the beginning of the buffer, use the value 0. Default value = 0.</td></tr>          
          </table>   
                   

      <b>OUTPUT</b><br>
      This function returns the requested bytes from the receive buffer.<br><br>
      <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
              <td>Success byte<br>
                  0 is failure. 1 is success.
              </td></tr>
          <tr><td>byte 1</td><td>data byte 0</td></tr>
          <tr><td>byte 2</td><td>data byte 1</td></tr>
          <tr><td>...</td><td>...</td></tr>
          <tr><td>byte 100</td><td>data byte 99</td></tr>
          </table>   


<a name="POZYX_RX_CIR"><h3>POZYX_RX_CIR</h3></a>
      This function returns the channel impulse response (CIR) of the last received ultra-wideband message.
      The CIR can be used for diagnostic purposes, or to run custom timing algorithms.
      Using the default PRF of 64MHz, a total of 1016 complex coefficients are available. For a PRF of 
      16MHz, 996 complex coefficients are available. Every complex coefficient is represented by 4 bytes
      (2 for the real part and 2 for the imaginary part). The coefficients are taken at an interval of 1.0016ns, 
      or more precisely, at half the period of a 499.2MHz sampling frequency.
      <br>
      <br>
      <b>INPUT PARAMETERS</b><br>
      This function takes 3 bytes as input parameters.<br><br>

          <table class='table table-bordered table-condensed descr-table'>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0-1</td>
              <td>CIR buffer offset. This value indicates where the offset inside the CIR buffer to start reading the 
              the data bytes. Possible values range between 0 and 1015.
              <br>
              Type: uint16_t</td></tr>  
              <tr><td>byte 2</td><td>Data length. The number of coefficients to read from the CIR buffer. Possible values
              range between 1 and 49.</td></tr>
          </table>   
                   

      <b>OUTPUT</b><br>
      This function returns the requested bytes from the CIR buffer.<br><br>
      <table class='table table-bordered table-condensed descr-table'>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
              <td>Return byte indicating success or failure of the function. 0 is failure. 1 is success. 
                  
              </td></tr>
          
          <tr><td>byte 1-2</td><td>CIR coefficient 0+offset (real value).<br>Type: int16. </td></tr>
          <tr><td>byte 3-4</td><td>CIR coefficient 0+offset (imaginary value).<br>Type: int16. </td></tr>
          <tr><td>byte 5-6</td><td>CIR coefficient 1+offset (real value).<br>Type: int16. </td></tr>
          <tr><td>byte 7-8</td><td>CIR coefficient 1+offset (imaginary value).<br>Type: int16. </td></tr>

          <tr><td>...</td><td>...</td></tr>
          <tr><td>byte 95-96</td><td>CIR coefficient 48+offset (real value).<br>Type: int16. </td></tr>
          <tr><td>byte 97-98</td><td>CIR coefficient 48+offset (imaginary value).<br>Type: int16. </td></tr>
         
          </table>   

<h3>POZYX_RX_INFO</h3>

<h3>POZYX_POS_ESTIMATE</h3>

<h3>POZYX_POS_SET_ANCHOR_IDS</h3>
      This function sets the anchor network IDs that can be used for the positioning algorithm.
      The positioning algorithm will select the first NUM (as given in POZYX_POS_NUM_ANCHORS) anchors from this list only when the MODE (as given in POZYX_POS_NUM_ANCHORS) is set to the fixed anchor set.
      Note that the actual anchor coordinates must be provided by POZYX_ADD_ANCHOR for each anchor id.
      <br><br>
      <b>INPUT PARAMETERS</b><br>
      This function takes a variable number of input bytes. Note that the total number must be dividable by two.<br><br>
          
          <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
          <tr><td>byte 0</td>
          <td rowspan='2'>Network address of 1st anchor node.<br>Type: uint16_t</td></tr>
          <tr><td>byte 1</td></tr>
          <tr><td>byte 2</td>
          <td rowspan='2'>Network address of 2nd anchor node.<br>Type: uint16_t</td></tr>
          <tr><td>byte 3</td></tr>
          <tr><td>byte 4</td>
          <td rowspan='2'>Network address of 3th anchor node.<br>Type: uint16_t</td></tr>
          <tr><td>byte 5</td></tr>
          <tr><td>byte ..</td><td>...</td></tr>
          <tr><td>byte 18</td>
          <td rowspan='2'>Network address of 10th anchor node.<br>Type: uint16_t</td></tr>
          <tr><td>byte 19</td></tr>
          </table>

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success. 

      <h3>POZYX_POS_GET_ANCHOR_IDS</h3>
      This function returns the anchor IDs that are used for the positioning algorithm. When the positioning algorithm is set to the automatic anchor selection mode in (POZYX_POS_NUM_ANCHORS), this list
      will be filled automatically with the anchors chosen by the anchor selection algorithm.<br><br>

      <b>INPUT PARAMETERS</b><br>
      No input parameters (0 bytes)<br><br>

      <b>OUTPUT</b><br>
      This function will return up to 20 anchor ID's.<br><br>

      <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
      <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
      <tr><td>byte 0</td><td>Success byte. 0 is failure. 1 is success. </td></tr>
      <tr><td>byte 1</td>
      <td rowspan='2'>Network address of 1st anchor node.<br>Type: uint16_t</td></tr>
      <tr><td>byte 2</td></tr>
      <tr><td>byte 3</td>
      <td rowspan='2'>Network address of 2nd anchor node.<br>Type: uint16_t</td></tr>
      <tr><td>byte 4</td></tr>
      <tr><td>byte 5</td>
      <td rowspan='2'>Network address of 3th anchor node.<br>Type: uint16_t</td></tr>
      <tr><td>byte 6</td></tr>
      <tr><td>byte ..</td><td>...</td></tr>
      <tr><td>byte 19</td>
      <td rowspan='2'>Network address of 10th anchor node.<br>Type: uint16_t</td></tr>
      <tr><td>byte 20</td></tr>
      </table>

<a name="POZYX_MEM_CLEAR">POZYX_MEM_ERASE</a>  
      Erase (part) of the non-volatile flash memory. This will revert the registers to their
      default values upon reboot of the system.
      <br><br>
      <b>INPUT PARAMETERS</b><br>
      This function takes 1 byte as input parameter.<br><br>

      <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
          <td>Memory section. Select which part of the flash memory should be erased. Possible values are:<br>
          0 : REGISTER_DATA. Erase the register data in flash memory.<br>
          1 : NETWORK_LIST. Erase the network list from flash memory.<br>
          2 : USER. Erase user data from flash memory<br>  
          </td></tr>
         
      </table>

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success. 

<a name="POZYX_MEM_SAVE">POZYX_MEM_SAVE</a>   
      This function allows the user to save parts of the configuration in non-volatile flash memory.
      On the next startup of th device, these values will be loaded and used.
      <br><br>
      <b>INPUT PARAMETERS</b><br>
      This function takes 1 byte as input parameters.<br><br>
          
          <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
          <td>Configuration part. This byte determines which part of the configuration registers must be stored in flash memory. Possible values:<br>
          0 : UWB settings. This will store the contents of the registers 
          <a href='#POZYX_UWB_CHANNEL'>POZYX_UWB_CHANNEL</a>, 
          <a href='#POZYX_UWB_RATES'>POZYX_UWB_RATES</a>, 
          <a href='#POZYX_UWB_PLEN'>POZYX_UWB_PLEN</a>, 
          <a href='#POZYX_UWB_GAIN'>POZYX_UWB_GAIN</a>, 
          <a href='#POZYX_UWB_XTALTRIM'>POZYX_UWB_XTALTRIM</a> and 
          <a href='#POZYX_UWB_RXENABLE'>POZYX_UWB_RXENABLE</a>
          in flash memory.<br>


          1 : Network address. This will store the contents of <a href='#POZYX_NETWORK_ID'>POZYX_NETWORK_ID</a> in flash memory.<br>
          2 : Coordinates. This will store the contents of 
          <a href='#POZYX_POS_X'>POZYX_POS_X</a>, 
          <a href='#POZYX_POS_Y'>POZYX_POS_Y</a>, 
          <a href='#POZYX_POS_Z'>POZYX_POS_Z</a>
          in flash memory.
          </td></tr>
         
          </table>

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success. 

<a name="POZYX_DEVICE_ADD"><h3>POZYX_DEVICE_ADD</h3></a>
      This function adds a device to the internal list of devices. When the device is already present in the device list, the values will be overwritten.
      Read more about the internal devices list here.
      <br><br>
      <b>INPUT PARAMETERS</b><br>
      This function takes 15 bytes as input parameters.<br><br>
          
          <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
          <td rowspan='2'>Network address of the device.<br>Type: uint16_t</td></tr>
          <tr><td>byte 1</td></tr>

          <tr><td>byte 2</td>
          <td>Special flag describing the device.<br><br>

            <table class='table table-bordered table-condensed bittable'>
            <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>

            <tr><td><b>Bit flag</b></td>
            <td>-</td><td>-</td><td>FIXED_Z</td><td colspan='3'>AXIS_FLAGS</td><td colspan='2'>TYPE</td></tr>
            </table>

            <table class='table table-bordered table-condensed' style=' width:700px; '>
            <tr><th style='min-width:70px'>Bit flag</th><th>Description</th></tr>
            <tr>
            <td>TYPE</td><td>Type of device Possible values:<br>
            0 : Anchor<br>
            1 : Tag<br>            
            </td></tr>
            <td>AXIS_FLAGS</td><td>Optional hints to the anchor calibration algorithm to map some relative coordinates. Possible values:<br>
            0x01 : AXIS_ORIGIN. Place this anchor in the origin.<br>
            0x02 : AXIS_X. Place this anchor on the x-axis.<br>
            0x03 : AXIS_Y. Place this anchor on the y-axis.<br>
            0x04 : POSITIVE_X. Place this anchor above the x-axis.<br>
            0x05 : NEGATIVE_X. Place this anchor above the x-axis.<br>
            0x06 : POSITIVE_Y. Place this anchor above the y-axis.<br>
            0x07 : NEGATIVE_Y. Place this anchor below the y-axis.<br>

            <br>
            </td></tr>
            <td>FIXED_Z</td><td>Hint to the anchor calibration algorithm to use the z-coordinate given.

            </td></tr>
            </table>          




          </tr>

          <tr><td>byte 3</td>
          <td rowspan='4'>x-coordinate of the device<br>Type: int32_t</td></tr>
          <tr><td>byte 4</td></tr>
          <tr><td>byte 5</td></tr>
          <tr><td>byte 6</td></tr>
          </tr>

          <tr><td>byte 7</td>
          <td rowspan='4'>y-coordinate of the device<br>Type: int32_t</td></tr>
          <tr><td>byte 8</td></tr>
          <tr><td>byte 9</td></tr>
          <tr><td>byte 10</td></tr>
          </tr>

          <tr><td>byte 11</td>
          <td rowspan='4'>z-coordinate of the device<br>Type: int32_t</td></tr>
          <tr><td>byte 12</td></tr>
          <tr><td>byte 13</td></tr>
          <tr><td>byte 14</td></tr>
         
          </table>

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success. 

<a name="POZYX_DEVICES_CLEAR"><h3>POZYX_DEVICES_CLEAR</h3></a>
      This function empties the internal list of devices.<br><br>

      <b>INPUT PARAMETERS</b><br>
      No input parameters (0 bytes)<br><br>

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success. 


<a name="POZYX_DEVICES_GETIDS"><h3>POZYX_DEVICES_GETIDS</h3></a>
      This function returns the network IDs of all devices in the internal device list.<br><br>

      <b>INPUT PARAMETERS</b><br>
      Up to 2 optional input parameters (<=2 bytes)<br><br>
      <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
      <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
      <tr><td>byte 0</td>
      <td>Offset (optional). This function will return network ID's starting from this offset in the list of network ID's. The default value is 0.                
      </td></tr>
      <tr><td>byte 1</td>
      <td>size (optional). Number of network ID's to return, starting from the offset. The default value is (20-offset), i.e., returning the complete list. Possible values are between 1 and 20.             
      </td></tr>
      </table>

      <b>OUTPUT</b><br>
      This function will return up to 20 device ID's (41 bytes) depending on the input parameters.<br><br>

      <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
      <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
      <tr><td>byte 0</td>
      <td>Return byte indicating success or failure of the function. 0 is failure. 1 is success.                  
      </td></tr>

      <tr><td>byte 1</td>
      <td rowspan='2'>Network address of the network device number (offset+1).<br>Type: uint16_t</td></tr>
      <tr><td>byte 2</td></tr>
      <tr><td>byte 3</td>
      <td rowspan='2'>Network address of the network device number (offset+2).<br>Type: uint16_t</td></tr>
      <tr><td>byte 4</td></tr>
      <tr><td>byte 5</td>
      <td rowspan='2'>Network address of the network device number (offset+3).<br>Type: uint16_t</td></tr>
      <tr><td>byte 6</td></tr>
      <tr><td>...</td><td>...</td></tr>
      <tr><td>byte 39</td>
      <td rowspan='2'>Network address of the network device number (offset+size).<br>Type: uint16_t</td></tr>
      <tr><td>byte 34</td></tr>
      </table>

      <br><br>

<a name="POZYX_DO_RANGING"><h3>POZYX_DO_RANGING</h3></a>
      This function initiates a ranging operation. When ranging is finished (after about 15ms)  the FUNC bit is set <a href="#POZYX_INT_STATUS">POZYX_INT_STATUS</a> and 
      an interrupt is fired if the FUNC bit is enabled in <a href="#POZYX_INT_MASK">POZYX_INT_MASK</a>.
      The range information can be obtained using <a href="#POZYX_DEVICE_GETRANGEINFO">POZYX_DEVICE_GETRANGEINFO</a>.
      The device will be added to the device list if it wasn't present before.
      <br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes 2 bytes of input parameters.<br><br>

      <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
      <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
      
      <tr><td>byte 0</td>
      <td rowspan='2'>Network address of the remote device.<br>Type: uint16_t</td></tr>
      <tr><td>byte 1</td></tr>
      
      </table>

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success.
      <br><br>

<a name="POZYX_DO_POSITIONING"><h3>POZYX_DO_POSITIONING</h3></a>
      This function initiates a positioning operation. Calling this function will turn of continuous positioning.
      When positioning is finished (after about 70ms) the POS bit is set <a href="#POZYX_INT_STATUS">POZYX_INT_STATUS</a> and 
      an interrupt is fired if the POS bit is enabled in <a href="#POZYX_INT_MASK">POZYX_INT_MASK</a>. The result is stored in the positioning registers starting from <a href="#POZYX_POS_X">POZYX_POS_X</a><br> 
      See the functional description for more info on the positioning process.
      <br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes 0 bytes of input parameters.<br><br>     

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success.
      <br><br>


<a name="POZYX_DEVICES_DISCOVER"><h3>POZYX_DEVICES_DISCOVER</h3></a>
      This function performs a discovery operation to identify other pozyx devices within radio range. 
      Newly discovered devices will be added to the internal device list.
      This process may take several milliseconds.<br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes up to 3 bytes of input parameters. When omitted, the default values are used.<br><br>

      <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
      <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
       <tr><td>byte 0</td>
      <td>Discover options (optional). This determines which type of devices should be discovered. 
      The type is given by theoperation mode in <a href="#POZYX_OPERATION_MODE">POZYX_OPERATION_MODE</a>.
      Possible values:<br>
      0x0 : Anchors only <b>(default value)</b><br>
      0x1 : Tags only <br>
      0x2 : All Pozyx devices
      </td></tr> 
      <tr><td>byte 1</td>
      <td>Number of Idle slots (optional). The number of slots to wait for a response of an undiscovered device. If no response was received the discovery process is terminated.
      The default value is 3 idle slots.<br>Type: uint8_t
      </td></tr>     
      <tr><td>byte 2</td>
      <td rowspan='2'>Idle slot duration (optional). The time duration in milliseconds of the idle slot. 
      Depending on the ultra-wideband settings a shorter or longer slot duration must be chosen. 
      The default value is 10ms.<br>Type: uint8_t
      </td></tr>
     
      </table>

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success.
      <br><br>


<a name="POZYX_DEVICES_CALIBRATE"><h3>POZYX_DEVICES_CALIBRATE</h3></a>
      This function estimates the relative position of up to 6 pozyx devices within range. This function can be used for quickly setting up the positioning system.
      This procedure may take several hundres of milliseconds depending on the number of devices in range and the number of range measurements requested.
      During the calibration proces LED 2 will turned on. At the end of calibration the corresponding bit in the <a href='#POZYX_CALIB_STATUS'>POZYX_CALIB_STATUS</a> register will be set. 
      Note that only the coordinates of pozyx devices within range are determined. The resulting coordinates
      are stored in the internal device list. It is advised that during the calibration process, the pozyx tag is not used for other wireless communication.
      More information can be found here.<br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes a number of optional bytes as input parameters. When omitted, the default values are used.
      Also, when no network id's of the anchors are supplied, the anchors will be automatically discovered.
      <br><br>

      <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
      <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
      
      <tr><td>byte 0</td>
      <td>Calibration options (optional). Possible values:<br>
      0x02 : 2D <b>(default)</b>. The relative x and y coordinates of the anchors are estimated. It is expected that all 
      anchors are located in the same 2D plane.<br>
      0x01 : 2.5D. The relative x and y coordinates of the anchors are estimated. However it is not expected that all
      anchors are located in the same 2D plane. For this option to work, the z-coordinates of the anchors 
      must be available in the device list.<br>
      <!--0x00 : 3D. The relative x, y and z coordinates for the anchors are estimated. It is not possible to select this
      option if only 4 anchors are available.-->
      </td></tr> 

      <tr><td>byte 1</td>
      <td>Number of Measurements (optional). This describes the number of range measurements that should be made for the estimation. Note that a larger number of 
      measurements will increase the accuracy of the relative coordinates, but will also make the process take longer. The default value is 10 measurments.
      <br>Type: uint8_t
      </td></tr>   

      <tr><td>byte 2-3</td>
      <td>Network id anchor 0 (optional). Optionally the network id of the first anchor is given. This anchor will be used 
      to define the origin, i.e., it's coordinates will be forced to zero. 
      <br>Type: uint16_t
      </td></tr>  

      <tr><td>byte 4-5</td>
      <td>Network id anchor 1 (optional). Optionally the network id of the second anchor is given. This anchor will be used to
      determine the x-axis, i.e., its y coordinate will be forced to zero. 
      <br>Type: uint16_t
      </td></tr>  

      <tr><td>byte 6-7</td>
      <td>Network id anchor 2 (optional). Optionally the network id of the third anchor is given. This anchor will be used to
      determine the which way is up for the y-coordinate, i.e., its y coordinate will be forced to be positive. 
      <br>Type: uint16_t
      </td></tr>

      <tr><td>byte 8-9</td>
      <td>Network id anchor 3 (optional). Optionally the network id of the fourth anchor is given.  
      <br>Type: uint16_t
      </td></tr>

      <tr><td>byte 10-11</td>
      <td>Network id anchor 4 (optional). Optionally the network id of the fifth anchor is given.  
      <br>Type: uint16_t
      </td></tr>

      <tr><td>byte 12-13</td>
      <td>Network id anchor 5 (optional). Optionally the network id of the sixth anchor is given.  
      <br>Type: uint16_t
      </td></tr>

      </table>

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success.
      <br><br>

<a name="POZYX_GET_RXDIAGNOS"><h3>POZYX_GET_RXDIAGNOS</h3></a>    
      Read out more detailed diagnostic data after an UWB message was received.
      <br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes 0 bytes as input.<br><br>         

      <b>OUTPUT</b><br>
      The function returns 21 bytes of data.<br><br>

      <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
          <td>Return byte indicating success or failure of the function. 0 is failure. 1 is success.                  
          </td></tr>

          <tr><td>byte 1</td>
          <td rowspan='2'>LDE max value of noise.<br>Type: uint16_t</td></tr>
          <tr><td>byte 2</td></tr>

          <tr><td>byte 3</td>
          <td rowspan='2'>First path amplitude, sample 1<br>Type: uint16_t</td></tr>
          <tr><td>byte 4</td></tr>

          <tr><td>byte 5</td>
          <td rowspan='2'>Standard deviation of noise.<br>Type: uint16_t</td></tr>
          <tr><td>byte 6</td></tr>

          <tr><td>byte 7</td>
          <td rowspan='2'>First path amplitude, sample 2<br>Type: uint16_t</td></tr>
          <tr><td>byte 8</td></tr>    

          <tr><td>byte 9</td>
          <td rowspan='2'>First path amplitude, sample 3<br>Type: uint16_t</td></tr>
          <tr><td>byte 10</td></tr>    

          <tr><td>byte 11</td>
          <td rowspan='2'>Maximum growth of the CIR<br>Type: uint16_t</td></tr>
          <tr><td>byte 12</td></tr>    

          <tr><td>byte 13</td>
          <td rowspan='2'>RX preamble count<br>Type: uint16_t</td></tr>
          <tr><td>byte 14</td></tr>    

          <tr><td>byte 15</td>
          <td rowspan='2'>Index of the first path (given in DW ticks)<br>Type: uint16_t</td></tr>
          <tr><td>byte 16</td></tr>   

          <tr><td>byte 16</td>
          <td rowspan='2'>LDE index of the signal maximum (given in DW CIR ticks)<br>Type: uint16_t</td></tr>
          <tr><td>byte 18</td></tr>   

          <tr><td>byte 19</td>
          <td rowspan='2'>LDE max value of the received signal<br>Type: uint16_t</td></tr>
          <tr><td>byte 20</td></tr>    
         
      </table>

<a name="POZYX_DEVICE_GETINFO"><h3>POZYX_DEVICE_GETINFO</h3></a>
      This function returns all the data available about a given pozyx device.<br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes 2 bytes as input parameters.<br><br>
          <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
          <td rowspan='2'>Network address of pozyx device.<br>Type: uint16_t</td></tr>
          <tr><td>byte 1</td></tr>          
          </table>      

      <b>OUTPUT</b><br>
      The function returns 15 bytes of data.<br><br>

      <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
          <td rowspan='2'>Network address of the device.<br>Type: uint16_t</td></tr>
          <tr><td>byte 1</td></tr>

          <tr><td>byte 2</td>
          <td>Special flag describing the device<br>Type: uint8_t</td></tr>

          <tr><td>byte 3</td>
          <td rowspan='4'>x-coordinate of the device<br>Type: int32_t</td></tr>
          <tr><td>byte 4</td></tr>
          <tr><td>byte 5</td></tr>
          <tr><td>byte 6</td></tr>
          </tr>

          <tr><td>byte 7</td>
          <td rowspan='4'>y-coordinate of the device<br>Type: int32_t</td></tr>
          <tr><td>byte 8</td></tr>
          <tr><td>byte 9</td></tr>
          <tr><td>byte 10</td></tr>
          </tr>

          <tr><td>byte 11</td>
          <td rowspan='4'>z-coordinate of the device<br>Type: int32_t</td></tr>
          <tr><td>byte 12</td></tr>
          <tr><td>byte 13</td></tr>
          <tr><td>byte 14</td></tr>       
         
      </table>


<a name="POZYX_CONF_SAVE"><h3>POZYX_CONF_SAVE</h3></a>
      This function saves selected parts of the configuration in non-volatile flash memory. On the next boot of the
      system this configuration will be loaded.
      <br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes 1 byte as input parameter.<br><br>
          <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
          <td>Type. What part of the configuration you want to store. Possible values:<br>
          0x01 : CONF. All the configuration registers starting from POZYX_INT_MASK.<br> 
          0x02 : POS. The position informaiton stored in POZYX_POS_X, POZYX_POS_Y, and POZYX_POS_Z<br>
          0x03 : DEVICE LIST. The internal device list.<br>
          0x04 : ANCHOR LIST. This stores the list of anchors to be used in positioning.
          </td></tr>
          </table>      

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success.
      <br><br>

<a name="POZYX_CONF_SAVE"><h3>POZYX_CONF_RESET</h3></a>
      This function resets selected parts of the configuration to the factory defaults. On the next boot of the
      system this configuration will be loaded.
      <br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes 1 byte as input parameter.<br><br>
          <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
          <td>Type. What part of the configuration you want to store. Possible values:<br>
          0x01 : CONF. All the configuration registers starting from POZYX_INT_MASK.<br> 
          0x02 : POS. The position informaiton stored in POZYX_POS_X, POZYX_POS_Y, and POZYX_POS_Z<br>
          0x03 : DEVICE LIST. The internal device list.<br>
          0x04 : ANCHOR LIST. This stores the list of anchors to be used in positioning.
          </td></tr>
          </table>      

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success.
      <br><br>      

<a name="POZYX_FLASH_STORE"><h3>POZYX_FLASH_STORE</h3></a>
      This function let's you store the content of writable registers in the non-volatile flash memory. 
      On the next boot of the system the saved content of these registers is automatically loaded.
      This way, any configuration is not lost after the device is reset or powered down. 
      Note that storing data in the flash memory may take some time.
      <br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes a minimum of of 1 byte as input parameters.<br><br>
          <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
          <td>Type. What type of data do you want to store. Possible values:<br>
          0x01 : Register data. The current content of the registers can be stored in flash memory. The register data 
          stored this way will be automatically loaded on the next boot of the system.<br> 
          0x02 : anchor list. Save the list of positioning anchor IDs that have been configured with POZYX_POS_SET_ANCHOR_IDS<br>
          0x03 : Device list data. Save the device list information that has been discovered or added manually.
          </td></tr>
          <tr><td>byte 1</td>
          <td>
                <i>only required if byte 0 = 0x01; </i><br>
                REG 1 : the address of the first register variable you wish to store. Note that
                the full variable will be stored, i.e., for a 4 byte register variable only the 
                start address is required. For example, using POZYX_POS_X will store 4 bytes.<br>
          </td>          
          </tr>  
          <tr><td>byte 2-10</td>
          <td>
                <i>optional if byte 0 = 0x01;</i><br>
                REG 2-10 (optional): additional addresses of of register variables you wish to store. Note that
                the full variable will be stored, i.e., for a 4 byte register variable only the 
                start address is required. For example, using POZYX_POS_X will store 4 bytes.<br>
                data byte 0 : First user data byte
          </td>          
          </tr>            
          </table>      

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success.
      <br><br>

<a name="POZYX_FLASH_RESET"><h3>POZYX_FLASH_RESET</h3></a>
      Clear the saved flash memory content and reset all the registers to their default values.
      Clearing data from the flash memory may take some time.
      <br><br>

      <b>INPUT PARAMETERS</b><br>
      This function does not require input.<br><br>
          
      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success.
      <br><br>

<a name="POZYX_FLASH_DETAILS"><h3>POZYX_FLASH_DETAILS</h3></a>
      This function returns detailed information about which data registers are stored in flash memory.
      <br><br>

      <b>INPUT PARAMETERS</b><br>
      This function does not require input.<br><br>

      <b>OUTPUT</b><br>
      The function returns 21 bytes of information.<br><br>

      <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
           <td>Return byte indicating success or failure of the function. 0 is failure. 1 is success.                  
           </td></tr>

          <tr><td>byte 1-20</td>
          <td>Every bit in these 20 bytes (160 bits) represents a register ranging from register 0x00 to 0x9F.
          The first bit represents register 0x00, the second bit represents register 0x01, etc..  
          A bit that is set, means the register content has been saved in flash. If the bit is not set, the register is not saved.
          </td></tr>
          </tr>

      </table>

      <br><br>

<a name="POZYX_FLASH_LOAD"><h3>POZYX_FLASH_LOAD</h3></a>
      This function let's you store data in the non-volatile flash memory. This way it is not lost after the device
      is reset or powered down. Storing data in the flash memory may take some time.
      <br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes 3 bytes as input parameters.<br><br>
          <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
          <td>Type. What type of data do you want to store. Possible values:<br>
          0x01 : Register data. Any of the configuration registers can be stored in flash.<br> 
          0x02 : Device list data. No furter parameters are required<br>
          0x03 : Custom user data. Up to 1kb of data is available for custom user data<br>
          </td></tr>
          <tr><td>byte 1</td>
          <td>
                REG 1 : the address of the first register variable you wish to store. Note that
                the full variable will be stored, i.e., for a 4 byte register variable only the 
                start address is required. For example, using POZYX_POS_X will store 4 bytes.<br>
                offset : the offset 
          </td>          
          </tr>  
          <tr><td>byte 2</td>
          <td>
                REG 2 (optional): the address of the 2nd register variable you wish to store. Note that
                the full variable will be stored, i.e., for a 4 byte register variable only the 
                start address is required. For example, using POZYX_POS_X will store 4 bytes.<br>
                data byte 0 : First user data byte
          </td>          
          </tr>  
          <tr><td>byte 3</td>
          <td>
                REG 3 (optional): the address of the 3th register variable you wish to store. Note that
                the full variable will be stored, i.e., for a 4 byte register variable only the 
                start address is required. For example, using POZYX_POS_X will store 4 bytes.<br>
                data byte 0 : First user data byte
          </td>          
          </tr>        
          </table>      

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success.
      <br><br>

<a name="POZYX_DEVICE_GETCOORDS"><h3>POZYX_DEVICE_GETCOORDS</h3></a>
      This function returns the coordinates of a given pozyx device as they are stored in the internal device list.
      The coordinates are either inputted by the user using <a href='#POZYX_DEVICES_ADD'>POZYX_DEVICES_ADD</a> 
      or obtained automatically with <a href='#POZYX_DEVICES_CALIBRATE'>POZYX_DEVICES_CALIBRATE</a>. 

      <br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes 2 bytes as input parameters.<br><br>
          <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
          <td rowspan='2'>Network address of pozyx device.<br>Type: uint16_t</td></tr>
          <tr><td>byte 1</td></tr>          
          </table>      

      <b>OUTPUT</b><br>
      The function returns 13 bytes of information.<br><br>

      <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
           <td>Return byte indicating success or failure of the function. 0 is failure. 1 is success.                  
           </td></tr>

          <tr><td>byte 1</td>
          <td rowspan='4'>x-coordinate of the device<br>Type: int32_t</td></tr>
          <tr><td>byte 2</td></tr>
          <tr><td>byte 3</td></tr>
          <tr><td>byte 4</td></tr>
          </tr>

          <tr><td>byte 5</td>
          <td rowspan='4'>y-coordinate of the device<br>Type: int32_t</td></tr>
          <tr><td>byte 6</td></tr>
          <tr><td>byte 7</td></tr>
          <tr><td>byte 8</td></tr>
          </tr>

          <tr><td>byte 9</td>
          <td rowspan='4'>z-coordinate of the device<br>Type: int32_t</td></tr>
          <tr><td>byte 10</td></tr>
          <tr><td>byte 11</td></tr>
          <tr><td>byte 12</td></tr>

      </table>

      <br><br>


<a name="POZYX_DEVICE_SETCOORDS"><h3>POZYX_DEVICE_SETCOORDS</h3></a>



<a name="POZYX_DEVICE_GETRANGEINFO"><h3>POZYX_DEVICE_GETRANGEINFO</h3></a>
      This function returns all the latest range information about a given pozyx device.
      This function does not initiate any ranging operation. Range information will be available
      as a result of calling <a href='#POZYX_DO_RANGING'>POZYX_DO_RANGING</a> directly or after positioning (where the range measurements with the anchors will be available)
      <br><br>

      <b>INPUT PARAMETERS</b><br>
      This function takes 2 bytes as input parameters.<br><br>
          <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td>
          <td rowspan='2'>Network address of pozyx device.<br>Type: uint16_t</td></tr>
          <tr><td>byte 1</td></tr>          
          </table>      

      <b>OUTPUT</b><br>
      The function returns 11 bytes of information.<br><br>

      <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          
          <tr><td>byte 0</td>
          <td>Return byte indicating success or failure of the function. 0 is failure. 1 is success.                  
          </td></tr>

          <tr><td>byte 1</td>
          <td rowspan='4'>Timestamp of last range measurement<br>Type: uint32_t</td></tr>
          <tr><td>byte 2</td></tr>
          <tr><td>byte 3</td></tr>
          <tr><td>byte 4</td></tr>

          <tr><td>byte 5</td>
          <td rowspan='4'>Last range measurement in mm. The resolution of 
          a range measurement is 4.69mm (corresponding to a timing resolution of 15.65ps)
          <br>Type: uint32_t</td></tr>
          <tr><td>byte 6</td></tr>
          <tr><td>byte 7</td></tr>
          <tr><td>byte 8</td></tr>

          <tr><td>byte 9</td>
          <td rowspan='2'>Received signal strength value of last range measurement<br>Type: int16_t</td></tr>
          <tr><td>byte 10</td>
          </tr> 
         
      </table>

      <br><br>


<a name="POZYX_TDMA_START_SUPERFRAME"><h3>POZYX_TDMA_START_SUPERFRAME</h3></a>
      This function immediately iniates a TDMA superframe. The number of slots and duration of each slot is 
      configured in the registers <a href="#POZYX_TDMA_NUMSLOTS">POZYX_TDMA_NUMSLOTS</a> and
      <a href="#POZYX_TDMA_SLOTDURATION_MS">POZYX_TDMA_SLOTDURATION_MS</a>, respectively.<br><br>

      <b>INPUT PARAMETERS</b><br>
      None.<br><br>

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success. <br><br>


<a name="POZYX_TDMA_SCHEDULE_RANGING"><h3>POZYX_TDMA_SCHEDULE_RANGING</h3></a>

      This function schedules a ranging operation at a given timeslot with a given network address. Whenever the 
      range measurement is finished, an interrupt will be triggered and the range data can be read out using 
      <a href="#POZYX_DEVICE_GETRANGEINFO">POZYX_DEVICE_GETRANGEINFO</a> as normal.
      <br>
      <br>
      <b>INPUT PARAMETERS</b><br>
      This function takes 4 bytes as input parameters.<br><br>
          <table class='table table-bordered table-condensed' style=' width:700px; margin-left: 20px '>
          <tr><th style='min-width:70px'>byte number</th><th>Description</th></tr>
          <tr><td>byte 0</td><td  rowspan='2'>Slot number. Start with the number 0. 499 is the maximum value.
          <br>Type: uint16_t</td></tr>
          <tr><td>byte 1</td></tr>
          <tr><td>byte 2</td>
          <td rowspan='2'>Network address of destination node.<br>Type: uint16_t</td></tr>
          <tr><td>byte 3</td></tr>
          </table>      

      <b>OUTPUT</b><br>
      The function returns one result byte for success or failure. 0 is failure. 1 is success. <br><br>



      </div>
</div>
