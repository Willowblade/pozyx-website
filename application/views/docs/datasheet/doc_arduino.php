<?php

// we parse the text in the XML file and add links where necessary
function linkify($text) {

  //Convert urls to <a> links
  $text = preg_replace("/([\w]+\:\/\/[\w-?&;#~=\.\/\@]+[\w\/])/", "<a target=\"_blank\" href=\"$1\">$1</a>", $text);

  // insert the pozyx register link, by replacing everything that begins with POZYX_ (except when a hashtag is present)
  //$text = preg_replace("/\b(?<!\#)(POZYX_[A-Za-z0-9\_]*)/", "<a href='".site_url('Documentation/Datasheet/RegisterOverview')."#$1'>$1</a>", $text);
  $text = preg_replace("/\b(?<!\#)reg:(POZYX_[A-Za-z0-9\_]*)/", "<a href='".site_url('Documentation/Datasheet/RegisterOverview')."#$1'>$1</a>", $text);


  // everything with a hashtag # in front is converted surrounded with <code> tags
  $text = preg_replace("/(^|\s)#([A-Za-z0-9\_\/\.]*)/", " <code>$2</code>", $text);

  // for structs, add a _t behind and delete the leading underscore
  $text = preg_replace("/\b(?<!\#)\_([]a-zA-Z0-9\_]*)/", "$1_t", $text);

  
  //Convert attags to twitter profiles in <a> links
  //$text = preg_replace("/@([A-Za-z0-9\/\.]*)/", "<a href=\"http://www.twitter.com/$1\">@$1</a>", $text);

  return $text;

}

// this function parses the doxygen XML elements and adds styling to it
function recursively_find_text_nodesX($dom_element)
{
	if(!isset($dom_element))
		return;

    foreach ( $dom_element->childNodes as $dom_child )
    {
        switch ( $dom_child->nodeType )
        {
            case XML_TEXT_NODE:
            	if(!$dom_child->isWhitespaceInElementContent())
                	echo linkify($dom_child->wholeText);

            break;
            case XML_ELEMENT_NODE:           	          	
            	
				if($dom_child->hasAttribute("kind")){
					$kind = $dom_child->getAttributeNode("kind")->value;
					if($kind == "param"){
						echo "<p><b>parameters:</b></p><ul class='params'>";
					}else if($kind == "retval"){
						echo "<p><b>returns:</b></p><ul class='params'>";
					}else if($kind == "return"){
						echo "<p><b>return:</b></p><p>";
					}elseif($kind == "see"){
						echo "<p><b>Also see</b></p><p>";							
					}elseif($kind == "note"){
						echo "<p><span class='text-warning'><span class='glyphicon glyphicon-alert'></span> <b>Note: </b></span><i>";	
					}elseif($kind == "version"){
						echo "<p><span class='text-warning'><span class='glyphicon glyphicon-flag'></span> <b>Note: </b></span><i>";
					}						
					
				}
            	
                switch($dom_child->tagName)
                {
                	case "member":
                		continue;
                	case "ref":
	                	// verify what kind of reference it is. does it start with struct_, group_, or something else?
	            		// we use a different referencing system than doxygen so we must reroute the references.
	            		$match[0] = "";
	            		preg_match("/([a-zA-Z0-9]*)\_/im", $dom_child->getAttributeNode("refid")->value, $match);
	            		if($match[0] == "struct_")
	            		{
	            			echo "<a href='". site_url('Documentation/Datasheet/Arduino/'.$dom_child->getAttributeNode("refid")->value) ."'>";
	            		}else{
	            			echo "<a href='#" . $dom_child->textContent ."'>";
	            		}

						echo recursively_find_text_nodesX($dom_child);
            			echo "</a>";
            			break;

	            	case "defval":

	            		echo  " = <code>";					// default value for parameters
	            		echo recursively_find_text_nodesX($dom_child);
	            		echo  "</code>";
	            		break;

	            	case "type":
	            		echo  "<span class='type'>";			// the variable type	
	            		echo recursively_find_text_nodesX($dom_child);
	            		echo  "</span>";
	            		break;
	            	case "compound":
	            		echo recursively_find_text_nodesX($dom_child);
	            		echo  "</a>";
	            		break;
	            	case "parameteritem":

						echo "<li style='clear:both'>";		
	            		echo recursively_find_text_nodesX($dom_child);		
						echo  "</li>";
						break;

					case "parameternamelist":
						
						echo "<div style='float: left; width: auto'>";		
	            		echo recursively_find_text_nodesX($dom_child);		
						echo  "</div>";
						break;

					case "parameterdescription":
						
						echo "<div style='margin-left: 180px'>";		
	            		echo recursively_find_text_nodesX($dom_child);		
						echo  "</div>";
						break;	

					case "parametername":

						echo "<div class='parametername'>"; 
						echo recursively_find_text_nodesX($dom_child);
						echo "</div>"; 
						break;

					default:
						echo recursively_find_text_nodesX($dom_child);
						break;

				}                

            	if($dom_child->hasAttribute("kind")){
            		$kind = $dom_child->getAttributeNode("kind")->value;
            		if($kind == "param" || $kind == "retval"){
						echo  "</ul>";
					}else if($kind == "return" || $kind == "see"){
						echo  "</p>";
					}else if($kind == "note"){
						echo "</i></p>";
					}else if($kind == "version")
						echo "</i></p>";
				}

            break;
        }
    }

}

function print_members($members, &$num){

	echo "<table class='table table-condensed table-hover'>";
	echo "<thead><tr><th></th><th>Function name</th></tr></thead>";
	echo "<tbody>";
	
	foreach($members as $member) {

		echo '<tr><td align="right" style="width:100px">';
		echo $member->type;			

		echo "</td><td>";
		echo "<div id='mainrow".$num."' class='mainrow' style='width:100%'><a name='".$member->name."' href='' onclick='showDetails(".$num."); return false;'>";
	    echo $member->name;
	    echo "</a>";	    
		echo "<div style='width: 60%; float: right;'>". $member->briefdescription->para ."</div>";

		echo "</div>";

		

		if(!empty($member->detaileddescription)){	
			echo "<div id='row".$num."' class='hiddenrow' style='display:none; padding-right: 50px; padding-bottom: 25px'>";

			echo "<a name='".$member->name."' href='' onclick='showDetails(".$num."); return false;'>";
		    echo $member->name;
		    echo "</a>(<br>";
		    //echo $member->argsstring;	

			$i = 0;
			$len = count($member->param);
	    	foreach($member->param as $param) {	  
	    		
	    		$dom_element = dom_import_simplexml($param);
	    		recursively_find_text_nodesX($dom_element);	    		  		

	    		$i++;
	    		if($i < $len){
	    			echo(",<br>");
	    		}
	    	}

	    	echo ")<br><hr>";	
			

			$dom_element = dom_import_simplexml($member->detaileddescription);
			recursively_find_text_nodesX($dom_element);
				    	
	    	echo "</div>";
	    }


	    echo '</td></tr>';

	    $num++;
	    
	}
	echo("</tbody></table>");
}
?>


<script src="<?php echo(base_url('assets/js/jquery-2.1.4.min.js')); ?>"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<style>
	.type{
		color: gray; /*#52d9b1*/;
		min-width: 200px;
		padding-left: 20px;
		margin-right: 10px;
		/*float: left; */
	}

	.params{
		margin-left: 40px;
		padding-left: 0px;
		list-style-type: none;
	}

	.params li{
		
	}

	.parametername{
		width: 160px; 
		float: left; 
		clear: both;

	}

	li a, li a:hover{
		color: black;
	}	

	h3 a, h3 a:hover {
		color: inherit;
		text-decoration: none;
	}
	

</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            Arduino library
        </p>  

        <h2>Arduino library</h2>            

        </div>  

      <div class="col-md-12">

      <p>The Pozyx Arduino library can be downloaded from <a href="https://github.com/pozyxLabs/Pozyx-Arduino-library">github</a> <i class="fa fa-github"></i>.  
      </p>

      <p>
      The functions in the library can be grouped as follows:  
      </p>

      <ul style='margin-left:25px'>
      	<li><b><a href='#core_functions'>Core functions:</a></b> basic functions to communicate with the pozyx device.</li>
      	<li><b><a href='#system_functions'>System functions:</a></b> these give you system information about the pozyx device.</li>
      	<li><b><a href='#positioning_functions'>Ranging and positioning functions:</a></b> a group of functions for ranging and positioning.</li>
      	<li><b><a href='#sensor_functions'>Sensor functions:</a></b> a group of functions to obtain the (motion) sensor information.</li>
      	<li><b><a href='#communication_functions'>Communication functions:</a></b> a group of functions for wireless communication.</li>
      </ul>
      <br>


      <h3>All typedefs</h3>
      <p>
      <?php

	    foreach($structs as $struct) 
	    {	      
			$dom_element = dom_import_simplexml($struct);
			echo "<a href='". site_url('Documentation/Datasheet/Arduino/'.$dom_element->getAttributeNode("refid")->value) ."'>";
			$txt = recursively_find_text_nodesX($dom_element);
			/*foreach($txt as $part){
				echo(substr($part, 1));
			} */   		

			echo("</a> | ");
		}
	  ?>
	  </p><br>
	  
<h3><a name="core_functions">Core functions</a></h3>
<p>
	These are the core library functions which are sufficient for full control of the Pozyx device.
	They govern the initialization, the interrupts and the i2c communication. 
	This part is specific for the Arduino platform and is currently tested for Arduino Uno.
	These are the functions that should be adjusted when porting the library to another platform.
</p><br>

	<?php
	
	print_members($core_members, $num);

	?>
	<br>

<h3><a name="system_functions">System functions</a></h3>
	<p>This set of functions allow you to configure general system related properties such as the GPIOs, LEDs, interrupts, etc.</p>

	<?php
	
	print_members($system_functions, $num);

	?>
	<br>

<h3><a name="positioning_functions">Ranging and positioning functions</a></h3>
	<p>
	This group of functions are related to ranging and positioning. They are not grouped in the sensor functions because these functions also require UWB communication.
	</p>
	<?php
	
	print_members($positioning_functions, $num);

	?>
	<br>

<h3><a name="sensor_functions">Sensor functions</a></h3>
	<p>
	These are library functions that interact with the Pozyx registers to perform certain actions.
	For a full overview of all the available registers, please refer to the <a href="<?php echo site_url('Documentation/Datasheet/RegisterOverview'); ?>">register overview</a>.
	These functions are platform-independent and work for every Arduino.
</p>

	<?php

	print_members($sensor_data, $num);
	?>
	<br>

<h3><a name="device_list">Device list functions</a></h3>
	<p>
	Functions that control the device list. The device list holds information such as network id and coordinates about other pozyx devices in the network.
	</p>
	<?php
	
	print_members($device_list, $num);

	?>
	<br>	

<h3><a name="communication_functions">Wireless communication functions</a></h3>
	<p>
	Although most functions in the library can be used remotely, these functions are used specifically
	for wireless communication of your own data.
	</p>
	<?php
	
	print_members($communication_functions, $num);

	?>
	<br>


      </div>
      
  	</div>


  	<div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            Arduino library
        </p>            

    </div>  
</div>

<script>
function showDetails(row_id){
	$("#mainrow" + row_id).toggle();//slideToggle(800);
    $("#row" + row_id).toggle();//slideToggle(800);  

    return false;
}

function scroll_if_anchor(href) {

	// only perform a delay on an initial page load. Inside the page, we do a nice animation.	
	var delay = 600;
    if(typeof(href) == "string"){	
    	delay = 0;

    	// open up the 
    	var $vis_row = $('a[name='+ href.substr(1) +']').parent(".mainrow");
    	var $inv_row = $('a[name='+ href.substr(1) +']').parent(".hiddenrow");

    	console.log($vis_row);
    	console.log($inv_row);

    	$vis_row.hide();
    	$inv_row.show();
    }

    href = typeof(href) == "string" ? href : $(this).attr("href");
    
    // You could easily calculate this dynamically if you prefer
    var fromTop = 75;
    
    // If our Href points to a valid, non-empty anchor, and is on the same page (e.g. #foo)
    // Legacy jQuery and IE7 may have issues: http://stackoverflow.com/q/1593174
    if(href.indexOf("#") == 0) {
        var $target = $('a[name='+ href.substr(1) +']:visible');
        //var $target = $(href);   
        
        // Older browser without pushState might flicker here, as they momentarily
        // jump to the wrong position (IE < 10)   

        console.log($target);    
        if($target.length) {  
        	
    		$('html, body').animate(
    			{ scrollTop: $target.offset().top - fromTop }, 
    			{ duration: delay });
        	            
            /*if(history && "pushState" in history) {
                history.pushState({}, document.title, window.location.pathname + href);
                return false;
            }*/

            return false;
        }
    }
}

$( document ).ready(function() {
	// When our page loads, check to see if it contains an anchor
	scroll_if_anchor(window.location.hash);

	// this is for older browsers.
	window.setTimeout(function() {
   		scroll_if_anchor(window.location.hash);
	}, 1);
	

	// Intercept all anchor clicks
	$("body").on("click", "a", scroll_if_anchor);
});


</script>