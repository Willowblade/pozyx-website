<style>
  
  .release_date{
    color: #AAA;
    line-height: 10px;
  }

  ul{
    margin-left: 25px;
  }

</style>


<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation/Datasheet'); ?>">Datasheet</a> &gt;
            Version History
        </p>  

        <h2>Version History</h2>            

        <p>An overview the features introduced by each firmware version. 
        A tutorial on how to upload a new firmware version on the pozyx device can be found in the <a href="<?php echo site_url('Documentation/Tutorials/gettingStarted#firmwareUpdate'); ?>">getting started tutorial</a>.</p>

        </div>  

      <div class="col-md-12">

      <h3>Coming up</h3>   
      <ul>
        <li>Save and load configuration on Flash memory.</li>
        <li>LOS / NLOS identification readout from range measurements.</li>
        <li>UWB + IMU Tracking.</li>
        <li>Improved UWB-only accuracy.</li>
        <li>NLOS anchor calibration.</li>
        <li>Increase positioning update.</li>
      </ul>

      <h3>Version v0.9 - beta</h3>	
      <p class="release_date"><a href='<?php echo site_url('assets/documents/firmware_v09.zip'); ?>' class='btn btn-primary'><span class="glyphicon glyphicon-download-alt"></span>  Download firmware</a> - Release 1 feb 2016</p>
      
      <ul>
        <li>3D orientation.</li>
        <li>UWB-only Positioning.</li>
        <li>Accurate ranging.</li>
        <li>Wireless communication.</li>
        <li>Remote control.</li>
        <li>2D and 2.5D anchor calibration in LOS.</li>
        <li>LED control.</li>
        <li>GPIO control.</li>
      </ul> 

      </div>

    </div>
</div>