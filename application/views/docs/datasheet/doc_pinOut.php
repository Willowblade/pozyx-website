
<style>
.note{
  margin-left: 25px;
  margin-top: 10px;
}
</style>

<div class="container">
      <!-- Example row of columns -->
    <div class="row" style="padding-bottom: 50px; padding-top: 30px;">

        <div class="col-md-12">
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation/Datasheet'); ?>">Datasheet</a> &gt;
            Pin-out
        </p>  

        <h2>Pin-out</h2>            

        </div>  

      <div class="col-md-12">

      <h3>Pozyx Arduino shield</h3>	
      <p>
      In the picture below, all the pins that are connected with the Pozyx Arduino shield are shown.
      
      </p>
      <br>

      <img src="<?php echo(base_url('assets/images/docs/pozyx_pins2.jpg')); ?>">

      <br><br>

<h3>Pins with the Arduino</h3> 
      <p>
      In the list below are all the pins from the Arduino stacking headers that are connected with the Pozyx system.
      All other pins of the Arduino stacking headers are not connected and can be used for other purposes.
      When using a non-standard Arduino board, make sure that the required pins are available on the device. 
      For the different types of Arduino systems, it can be verified if these pins are also connected to other interfaces such as 
      <a href="https://www.arduino.cc/en/Reference/SPI">SPI</a> or <a href="https://www.arduino.cc/en/Reference/Wire">I2C</a>.
      </p>
      <br>
      <table class="table table-striped table-hover">

      <tr>
      <th>Pozyx pin</th>
      <th>Arduino Pin</th>
      <th>Description</th>
      </tr>

      <tr>
      <td>5V</td>
      <td>5V</td>
      <td>This is a 5V input pin. This pin is used as a reference for the pull-up resistors on the I2C bus (i.e., for SCL and SDA).
          <p class='note'>
          <span class='text-warning'><span class='glyphicon glyphicon-alert'></span> <b>Note: </b></span><i>
          It is possible to apply 3.3V to this pin which will make the I2C work with 3.3V devices. 
          In this case, the board should be powered with another pin (USB, jack or DEBUG VDD).
          </i>
          </p>

      </td>
      </tr>

      <tr>
      <td>GND</td>
      <td>GND</td>
      <td>The ground</td>
      </tr>

      <tr>
      <td>SCL</td>
      <td>SCL</td>
      <td>The I2C clock line. This pin is internally pulled up to the voltage applied on the 5V pin. No external pull-up resistor is necessary.</td>
      </tr>

      <tr>
      <td>SDA</td>
      <td>SDA</td>
      <td>The I2C data line. This pin is internally pulled up to the voltage applied on the 5V pin. No external pull-up resistor is necessary.</td>
      </tr>

      <tr>
      <td>GPIO1</td>
      <td>Pin 9</td>
      <td>A general purpose input/output pin. By default this pin is not configured and it can be used for other purposes.
      When configured as an output pin, the maximum output voltage is 3.3V (the operating voltage of the board). The pin is 5V tolerant.  
      </td>
      </tr>

      <tr>
      <td>GPIO2</td>
      <td>Pin 10</td>
      <td>A general purpose input/output pin. By default this pin is not configured and it can be used for other purposes.
      When configured as an output pin, the maximum output voltage is 3.3V (the operating voltage of the board). The pin is 5V tolerant.  
      </td>
      </tr>

      <tr>
      <td>GPIO3</td>
      <td>Pin 11</td>
      <td>A general purpose input/output pin. By default this pin is not configured and it can be used for other purposes.
      When configured as an output pin, the maximum output voltage is 3.3V (the operating voltage of the board). The pin is 5V tolerant.  
      </td>
      </tr>

      <tr>
      <td>GPIO4</td>
      <td>Pin 12</td>
      <td>A general purpose input/output pin. By default this pin is not configured and it can be used for other purposes.
      When configured as an output pin, the maximum output voltage is 3.3V (the operating voltage of the board). The pin is 5V tolerant.  
      </td>
      </tr>

      <tr>
      <td>INT 1</td>
      <td>Pin 2</td>
      <td>The first interrupt output pin. This pin is configured by default and should not be used for other purposes on the Arduino (on Firmware version v0.9).
      The maximum output voltage is 3.3V (the operating voltage of the board).
      </td>
      </tr>

      <tr>
      <td>INT 2</td>
      <td>Pin 3</td>
      <td>The second interrupt output pin. The maximum output voltage is 3.3V (the operating voltage of the board).
      </td>
      </tr>

      </table>
      <br>

<h3>Other pins / connections</h3> 
      <table class="table table-striped table-hover">

      <tr>
      <th>Pozyx pin</th>
      <th>Description</th>
      </tr>

      <tr>
      <td>TAG/ANCHOR</td>
      <td>This pin indicated whether the system should behave as a tag or an anchor. When the jumper is present the device behaves as a tag.</td>
      </tr>

      <tr>
      <td>BOOT pin</td>
      <td>The jumper on this pin allows the system to boot. The jumper should only be removed when performing a firmware update through USB.</td>
      </tr>

      <tr>
      <td>2.1mm Jack</td>
      <td>The input jack. The input voltage should be 5V.</td>
      </tr>

      <tr>
      <td>USB</td>
      <td>A micro-USB connector. This connector can be used for power or for performing firmware updates.</td>
      </tr>

      <tr>
      <td>DEBUG</td>
      <td>Pins for an SWD debugger. Note that the DEBUG VDD pin expects 3.3V. This pin can also be used to power the device.
          <p class='note'>
          <span class='text-warning'><span class='glyphicon glyphicon-alert'></span> <b>Warning: </b></span><i>
          The DEBUG VDD pin has no input protection. Applying more than 3.3V may cause damage to the shield.
          </i>
          </p>
      </td>
      </tr>


      </table>
 

      </div>

      <div class="col-md-12" style='margin-top:50px'>
        <p>
            <a href="<?php echo site_url('/'); ?>">Home</a> &gt;
            <a href="<?php echo site_url('Documentation'); ?>">Documentation</a> &gt;
            <a href="<?php echo site_url('Documentation/Datasheet'); ?>">Datasheet</a> &gt;
            Pin-out
        </p>          

      </div>  

    </div>
</div>