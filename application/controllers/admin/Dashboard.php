<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Dashboard extends Admin_Controller
{
 
  function __construct()
  {
    parent::__construct();
  }
 
  public function index()
  {
  	$user = $this->ion_auth->user()->row();
  	$this->data["username"] = $user->first_name;
    $this->data["userid"] = $user->id;
    $this->data["lastlogin"] = $user->last_login;

 
    $this->load->model('order_model'); 
    $this->data["num_orders"] = $this->order_model->num_orders();
    $this->data["order_data"] = $this->order_model->get_order_overview();
    $this->data["num_error"] = $this->order_model->num_error_orders();
    $this->data["num_shipping"] = $this->order_model->num_shipping_orders();
    $this->data["num_unpaid_shipped"] = $this->order_model->num_unpaid_shipped_orders();
    $this->data["num_unpaid"] = $this->order_model->num_unpaid_orders();
    $this->data["num_open_quotes"] = $this->order_model->num_open_quotes();



    $this->data["open_orders"] = $this->order_model->search_orders('pending', 'payment', 0, 100); 
    $this->data["num_results"] = $this->order_model->get_num_returned_rows();
    $this->data["open_quotes"] = $this->order_model->search_orders('quote', 'payment', 0, 100); 
    $this->data["num_oq_results"] = $this->order_model->get_num_returned_rows();
    $this->data["stock_info"] = $this->order_model->get_stock_info();

    $this->load->model('tasks_model'); 
    $this->data["num_open_tasks"] = $this->tasks_model->get_user_open_tasks($user->id);
 


    // load wunderlist
    /*require_once(APPPATH . 'libraries/wunderlist/src/Wunderlist.php');

    // define API
    $api = new \Wunderlist\Wunderlist("info@pozyx.io", "kickstart2015");
    $items = $api->getProfile();
    print_r($items);


    use Wunderlist\Entity\WList;
    use Wunderlist\ClientBuilder;

    // Instanciate wunderlist API manager
    $builder = new ClientBuilder();
    $wunderlist = $builder->build('yourClientId', 'yourClientSecret', 'http://domain.com/oauth/callback');

    //Here we get all lists for the authenticated user
    $lists = $wunderlist->getService(WList::class)->all();

    //For each list on the lists
    $lists->map(function($list) {
        echo $list->getTitle();
    });*/
    

    $this->render('admin/dashboard_view');
  }

  public function view_error_orders($page = 1){

    $orders_per_page = 15;

    $this->load->model('order_model'); 
    $this->data['orders'] = $this->order_model->get_error_orders(($page-1)*$orders_per_page, $orders_per_page); 
    $this->data['page'] = $page;
    $this->data["num_results"] = $this->order_model->get_num_returned_rows();
    $this->data['num_pages'] = ceil($this->data["num_results"] / $orders_per_page); 
    $this->data['base_url'] = site_url('admin/dashboard/view_error_orders');

    $this->render('admin/orders_view');

  }

  public function view_shipping_list($page = 1){

    $orders_per_page = 15;

    $this->load->model('order_model'); 
    $this->data['orders'] = $this->order_model->get_shipping_orders(($page-1)*$orders_per_page, $orders_per_page); 
    $this->data['page'] = $page;
    $this->data["num_results"] = $this->order_model->get_num_returned_rows();
    $this->data['num_pages'] = ceil($this->data["num_results"] / $orders_per_page); 
    $this->data['base_url'] = site_url('admin/dashboard/view_shipping_list');

    $this->render('admin/orders_view');

  }

  public function view_unpaid_shipped_orders($page = 1){

    $orders_per_page = 15;

    $this->load->model('order_model'); 
    $this->data['orders'] = $this->order_model->get_unpaid_shipped_orders(($page-1)*$orders_per_page, $orders_per_page); 
    $this->data['page'] = $page;
    $this->data["num_results"] = $this->order_model->get_num_returned_rows();
    $this->data['num_pages'] = ceil($this->data["num_results"] / $orders_per_page); 
    $this->data['base_url'] = site_url('admin/dashboard/view_unpaid_shipped_orders');

    $this->render('admin/orders_view');

  }

  public function view_unpaid_orders($page = 1){

    $orders_per_page = 15;

    $this->load->model('order_model'); 
    $this->data['orders'] = $this->order_model->get_unpaid_orders(($page-1)*$orders_per_page, $orders_per_page); 
    $this->data['page'] = $page;
    $this->data["num_results"] = $this->order_model->get_num_returned_rows();
    $this->data['num_pages'] = ceil($this->data["num_results"] / $orders_per_page); 
    $this->data['base_url'] = site_url('admin/dashboard/view_unpaid_orders');

    $this->render('admin/orders_view');

  }

  public function view_orders_by_country()
  {
    $this->load->model('country_model'); 
    //$this->data["shipping_rates"] = $this->country_model->get_shipping_rates();
    $this->data["shipping_rates"] = $this->country_model->get_popular_shipping_rates();

    $this->render('admin/orders_by_country_view');
  }

  public function create_pozyx_shipping_rates()
  {
    $this->load->model('shipping_rates_model'); 
    $this->shipping_rates_model->create_pozyx_shipping_rates();

    return;

  }

  public function view_orders_stats($option = 'this year')
  {
    $this->load->model('statistics_model'); 

    $this->data["option"] = $option;
    $this->statistics_model->set_data_period($option);
    
    // geographical stats
    $this->data["shipping_rates"] = $this->statistics_model->get_country_stats();
    $this->data["region_stats"] = $this->statistics_model->get_region_stats();
    
    // order insights
    $this->data["sector_stats"] = $this->statistics_model->get_sector_stats();
    $this->data["tracking_stats"] = $this->statistics_model->get_tracking_stats();
    $this->data["monthly_webstats"] = $this->statistics_model->get_monthly_stats('weborder');
    $this->data["monthly_quotestats"] = $this->statistics_model->get_monthly_stats('quote');

    $this->data["quarterly_stats"] = $this->statistics_model->get_quarterly_stats();
    $this->data["customertype_stats"] = $this->statistics_model->get_customer_type_stats();

    // payment stats
    $this->data["paymentmethod_stats"] = $this->statistics_model->get_paymentmethod_stats();
    $this->data["wiretransfer_stats"] = $this->statistics_model->get_monthly_wiretransfers_stats();

    // product insights
    $this->data["product_stats"] = $this->statistics_model->get_product_stats();
    $this->data["anchortag_stats"] = $this->statistics_model->get_anchortag_stats();

    // order behavior
    $this->data["week_stats"] = $this->statistics_model->get_week_stats();    
    $this->data["hour_stats"] = $this->statistics_model->get_avg_hourly_stats(); 

    // shipping stats
    $this->data["shipping_stats"] = $this->statistics_model->get_shipping_stats(); 
    $this->data["monthly_shipping_stats"] = $this->statistics_model->get_monthly_shipping_stats();
    
       

    $this->render('admin/orders_stats');
  }

  public function view_open_quotes($page = 1){

    $orders_per_page = 15;

    $this->load->model('order_model'); 
    $this->data['orders'] = $this->order_model->get_open_quotes(($page-1)*$orders_per_page, $orders_per_page); 
    $this->data['page'] = $page;
    $this->data["num_results"] = $this->order_model->get_num_returned_rows();
    $this->data['num_pages'] = ceil($this->data["num_results"] / $orders_per_page); 
    $this->data['base_url'] = site_url('admin/dashboard/view_open_quotes');

    $this->render('admin/orders_view');

  }

  public function view_shipping_rates($carrier = 'pozyx')
  {
    //$this->load->model('shipping_rates_model'); 
    //$this->data["shipping_rates"] = $this->shipping_rates_model->as_object()->order_by('countryCode', $order = 'ASC')->get_many_by(array('carrier'=>'TNTExpress'));

    // load all the countries
    $this->load->model('country_model');
    //$countries = $this->country_model->load_country_data();

    //$this->load->model('country_model'); 
    $this->data["shipping_rates"] = $this->country_model->get_shipping_rates($carrier);
    //$this->data["shipping_rates"] = $this->country_model->get_popular_shipping_rates();

    $this->render('admin/shipping_rates_view');
  }

  public function view_emails(){
    
    $user = $this->ion_auth->user()->row();
    $this->data["username"] = $user->first_name;

    $this->render('admin/emails_view');
  }

  public function view_calendar($all_users = 1)
  {    

    $key_file_location = APPPATH . "config/production/pozyx website-4a7875d1b749.p12";
    $key = file_get_contents($key_file_location);  

    // separate additional scopes with a comma   
    $scopes ="https://www.googleapis.com/auth/calendar";   
    $cred = new Google_Auth_AssertionCredentials(  
      "558333811159-3i3aiu2r1oalfmdhg51812bcob5nlv81@developer.gserviceaccount.com",    
      array($scopes),   
      $key     
      );   

      $client = new Google_Client(); 
      $client->setAssertionCredentials($cred);
      if($client->getAuth()->isAccessTokenExpired()) {    
          $client->getAuth()->refreshTokenWithAssertion($cred);   
      }
      
      //558333811159-ij41odo7tqf6cvglcorve18q53rj88c6.apps.googleusercontent.com
    //JsSjI1PWGdfCF86lHFRl2e27
      /*$client = new Google_Client();
      $client->setClientId("558333811159-ij41odo7tqf6cvglcorve18q53rj88c6.apps.googleusercontent.com");
      $client->setClientSecret("JsSjI1PWGdfCF86lHFRl2e27");
      $client->setApplicationName("My Calendar"); //DON'T THINK THIS MATTERS
      $client->setDeveloperKey("AIzaSyDgBkb99yQSGmBzTC2KhWNueOPILSLG6NE");
      $client->setAssertionCredentials();*/

      // Get the API client and construct the service object.
      //$client = getClient();
      $service = new Google_Service_Calendar($client);

      /*$event = new Google_Service_Calendar_Event();
      $event->setSummary('Halloween');
      $event->setLocation('The Neighbourhood');
      $start = new Google_Service_Calendar_EventDateTime();
      $start->setDateTime('2015-9-29T10:00:00.000-05:00');
      $event->setStart($start);
      $end = new Google_Service_Calendar_EventDateTime();
      $end->setDateTime('2015-9-29T10:25:00.000-05:00');
      $event->setEnd($end);
      $createdEvent = $service->events->insert('###', $event);
      */

      // Print the next 10 events on the user's calendar.
      $calendarId = '4nac82ffec4jll7j7sek6m5cko@group.calendar.google.com';
      $optParams = array(
        'maxResults' => 10,
        'orderBy' => 'startTime',
        'singleEvents' => TRUE,
        'timeMin' => date('c')
      );
      $this->data["events"] = $service->events->listEvents($calendarId, $optParams);     

      // Load the tasks
      $this->data["all_users"] = $all_users;
      $user = $this->ion_auth->user()->row();  

      $this->load->database();
      $this->load->model('tasks_model');
      if($all_users == 1){             
        $this->data["task_data"] = $this->tasks_model->get_tasks( $user->id );
      }else{
         $this->data["task_data"] = $this->tasks_model->get_tasks();
      }        

      // get some info about the current user's tasks
      $this->data["num_completed_tasks"] = $this->tasks_model->get_user_completed_tasks($user->id);
      $this->data["num_open_tasks"] = $this->tasks_model->get_user_open_tasks($user->id);

      $this->render('admin/calendar_view');
      
  }  

  public function task_add(){

      $post = $this->input->post();      

      if(isset($post["title"]))
      {
        $this->load->database();
        $this->load->model('tasks_model');


        $post_object = json_decode(json_encode($post, FALSE));
        unset($post_object->submit_id);

        $this->tasks_model->add_task($post_object);    

        $user = $this->ion_auth_model->user($post_object->user_id)->result();      
        $email = $user[0]->email;

        // send an email
        $this->load->library('email'); 

        $gezegden = array("It is bad to suppress laughter. It goes back down and spreads to your hips.","You can't have everything. Where would you put it? ","The trouble with being punctual is that nobody's there to appreciate it.","If you are afraid of being lonely, don't try to be right. ","When you battle with your conscience and lose, you win. ","You might as well fall flat on your face as lean over too far backward. ","Don't mind your make-up, you'd better make your mind up.","Never hold discussions with the monkey when the organ grinder is in the room. ","Money won't make you happy ... but everybody wants to find out for themselves. ","People who fly into a rage always make a bad landing. ","Always do right. This will gratify some people and astonish the rest. ","When I hear someone sigh that life is hard, I am tempted to ask, ‘compared to what?'");


        $message = "Hello ".$user[0]->first_name.",\n\na new task was created for you: ".$post_object->title;
        $message .= "\n\n" . $post_object->description;  
        $message .= "\n\nThis task is due ". $post_object->end_date; 
        $message .= "\n\nView all your tasks at: https://www.pozyx.io/admin";  
        $message .= "\n\n".$gezegden[array_rand($gezegden, 1)];
        

        $this->email->from('info@pozyx.io', 'Pozyx Labs');
        $this->email->to($email);   
        $this->email->reply_to('noreply@pozyx.io', 'no reply');       
        $this->email->subject('Pozyx new task: '. $post_object->title);      
        $this->email->message($message);  
        $this->email->send();

        redirect('admin/dashboard/view_calendar', 'refresh');
      }

      $this->load->library('form_validation');
      $this->load->library('form_builder');

      $this->load->database();
      $this->data["users"] = $this->ion_auth_model->users()->result();
      $user = $this->ion_auth->user()->row();    
      $this->data["userid"] = $user->id;

      $this->render('admin/task_add');
  }

  public function complete_task(){
      
      $post = $this->input->post();
      if(isset($post['task_id'])){
        
        $this->load->database();
        $this->load->model('tasks_model');
        $this->tasks_model->complete_task($post['task_id']);    
      }
  }

  public function view_email(){
      $id = $this->input->post("emailID");
      $stream = imap_open('{mail.pozyx.io:993/imap/ssl/novalidate-cert/norsh}', 'info+pozyx.io', 'kickstart2015');

      
      // Fetch the email's overview and show subject, from and date. 
      $overview = imap_fetch_overview($stream, $id, 0);               
      $message = "<div style='width:100%; height: 500px; text-align:left; overflow-y:scroll;'>".nl2br(imap_qprint(imap_fetchbody($stream, $id, 1))) . "</plaintext></div>";

      // Close our imap stream.
      imap_close($stream); 

      $array = array('message' => $message, 'id' => $id, 'subject' => $this->_decode_imap_text($overview[0]->subject), 'from' => $overview[0]->from, 'date' => $overview[0]->date );
      echo json_encode($array);      

  }

  function _decode_imap_text($str){
      $result = '';
      $decode_header = imap_mime_header_decode($str);
      foreach ($decode_header AS $obj) {
          $result .= htmlspecialchars(rtrim($obj->text, "\t"));
      }
      return $result;
  }

  public function orders($page = 1){

    $orders_per_page = 15;

    $this->load->model('order_model'); 
    $this->data['orders'] = $this->order_model->get_orders(($page-1)*$orders_per_page, $orders_per_page); 
    $this->data['page'] = $page;
    $this->data["num_results"] = $this->order_model->get_num_returned_rows();
    $this->data['num_pages'] = ceil($this->data["num_results"] / $orders_per_page); 
    $this->data['base_url'] = site_url('admin/dashboard/orders');

    $this->render('admin/orders_view');
  }

  public function search_orders($page = 1){

    $orders_per_page = 15;

    $this->load->model('order_model'); 
    $this->data['orders'] = $this->order_model->search_orders($this->input->get('search_keyword'), $this->input->get('search_in'), ($page-1)*$orders_per_page, $orders_per_page); 
    $this->data['test'] = 'testing';
    $this->data['page'] = $page;
    $this->data["num_results"] = $this->order_model->get_num_returned_rows();
    $this->data['num_pages'] = ceil($this->data["num_results"] / $orders_per_page); 
    $this->data['keyword'] = $this->input->get('search_keyword');
    $this->data['base_url'] = site_url('admin/dashboard/search_orders');


    $this->render('admin/orders_view');
  }

  public function order_paid(){

    $this->data['page_title'] = 'Pozyx Labs Admin - orders';
    $orderID = $this->input->post("orderID");
    $this->load->model('order_model'); 
    $this->order_model->update_payment($orderID, 'completed');

    // send a confirmation email
    $this->_send_confirmation_email($orderID);
  }

  public function order_shipped(){
    
    $orderID = $this->input->post("orderID");
    $this->load->model('order_model'); 
    $this->order_model->order_shipped($orderID, 'completed');
  }

  public function get_order_shipinfo(){
      $orderID = $this->input->post("orderID");
      $this->load->model('order_model'); 
      $ship_number = $this->order_model->get_order_shipnumber($orderID);
      $ship_date = $this->order_model->get_order_delivery_date($orderID);

      $ship_info = array(
          'ship_number' => $ship_number,
          'ship_date' => $ship_date
      );

      echo json_encode($ship_info);    
  }

  public function order_edit_remark(){

    $orderID = $this->input->post("orderID");
    $remarks = $this->input->post("remarks");
    $this->load->model('order_model'); 
    $this->order_model->edit_remark_order($orderID, $remarks);

  }

  public function make_priority(){
    $orderID = $this->input->post("orderID");
    $this->load->model('order_model');

    $this->order_model->update_priority($orderID, 1);
  }

  public function order_delete(){

    $this->data['page_title'] = 'Pozyx Labs Admin - orders';
    $orderID = $this->input->post("orderID");
    $remarks = $this->input->post("remarks");
    $this->load->model('order_model'); 
    $this->order_model->delete_order($orderID, $remarks);
  }

  public function _send_confirmation_email($orderID){
    
    log_message('info', 'function _send_confirmation_email() called');

    if(isset($orderID))
    {
      log_message('info', 'function send_confirmation_email() called for orderID '. $orderID);

      // now send a confirmation email        
      $this->load->library('email');
      $this->load->model('order_model');      

      // get order data
      $data = $this->order_model->get_details($orderID);
      if(empty($data)){
        log_message('error', 'function send_confirmation_email() could not find requested order information.');
      }
      $data["order_items"] = $this->order_model->get_items($orderID);

      // create the pro forma invoice
      $invoice_path = $this->order_model->generate_pdf_invoice("pro forma invoice", $orderID);
      $this->email->attach($invoice_path);      

      // email
      $this->email->from('info@pozyx.io', 'Pozyx Labs');
      $this->email->to($data["email_address"]);   
      $this->email->reply_to('noreply@pozyx.io', 'no reply');       
      $this->email->subject('Payment confirmation');      
      $this->email->message($this->load->view('store/email_txt_payment_confirmation', $data, true));  

      
      if ( ! $this->email->send() )
      { 
        // failed to send message !
        // Loop through the debugger messages.        
        foreach ( $this->email->get_debugger_messages() as $debugger_message ){
          log_message("error", $debugger_message);          
        }

        // Remove the debugger messages as they're not necessary for the next attempt.
        $this->email->clear_debugger_messages();        
      }      

    }else{
      log_message('error', 'function send_confirmation_email() received empty POST request');
    }     

  } 

  public function send_emails(){

    // let this script run a longer time
    set_time_limit(60);

    //$notification_event = "kickstart survey";
    $notification_event = "kickstart survey retry";

    $this->load->model('order_model');
    $this->load->model('notifications_model'); 
    $this->load->library('email');    
    $orders = $this->order_model->get_orders(0, 200);

    $this->load->view('templates/header.php');
    $this->load->view('templates/nav.php');
    
    echo "<br><br><br><br>";

    $fail = 1;
    $success = 1;
    foreach($orders as $order){
            
        if($order["payment_method"] == "kickstarter" && $order["grand_total"] < 200000 && $order["firstname"] == "")
        {    
            //echo $order["id"] . " no reaction. <br>";
            //$fail++;

            if(!$this->notifications_model->is_notified($order["orderid"], $notification_event))
            {
                $encodedOrderID = $this->order_model->encode_orderID($order["orderid"]);
                $data["encodedOrderID"]= $encodedOrderID;      

                // email
                $this->email->from('info@pozyx.io', 'Pozyx Labs');
                $this->email->to($order["email_address"]);   
                $this->email->reply_to('info@pozyx.io', 'Pozyx Labs');       
                $this->email->subject('pozyx kickstarter survey');      
                $this->email->message($this->load->view('kickstarter/email_txt_survey', $data, true));  
                
                if ( ! $this->email->send() )
                { 
                  // failed to send message !
                  // Loop through the debugger messages.        
                  foreach ( $this->email->get_debugger_messages() as $debugger_message ){
                    log_message("error", $debugger_message);          
                  }  

                  // Remove the debugger messages as they're not necessary for the next attempt.
                  $this->email->clear_debugger_messages();
                  
                  echo "<span style='color:red'>" . ($fail++) . " " . number_format(($order["grand_total"])/100,2,",",".") . " " . $order["email_address"] . " " . $encodedOrderID . "</span><br><br>";

                }else{

                  // success
                  $this->notifications_model->add_notification($order["orderid"], $notification_event);

                  echo ($success++) . " " . number_format(($order["grand_total"])/100,2,",",".") . " " . $order["email_address"] . " " . $encodedOrderID . "<br><br>";
                }
            }else{
                echo "email already sent for order " . $order["orderid"];
            }
            
        }
    }

    echo "Number of fails: ". ($fail-1) . "<br>";
    echo "Number of success: ". ($success-1) . "<br>";


    $this->load->view('templates/footer.php');

  }

}

