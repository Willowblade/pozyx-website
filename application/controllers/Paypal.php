<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paypal extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');		
	}

	// controller to show main store page
	public function index(){
	}

	public function paypal_checkout()
	{

		require_once(APPPATH . "libraries/paypal.class.php");

		$this->load->library('cart');
		$this->load->config('credentials');

		$paypalmode = ($this->config->item("PayPalMode")=='sandbox') ? '.sandbox' : '';


		// double check the grand total and tax rate
	    //$subtotal = ($this->cart->total())*100; 		
	    //$tax_rate = $this->_compute_tax_rate($this->input->post('country'), $this->input->post('vatNumber'));
	    //$grand_total = $this->_compute_grand_total($subtotal, $this->input->post('shipping_cost'), $tax_rate);

		//Other important variables like tax, shipping cost		
		$HandalingCost 		= 0.00;  //Handling cost for this order.
		$InsuranceCost 		= 0.00;  //shipping insurance cost for this order.
		$ShippinDiscount 	= 0.00; //Shipping discount for this order. Specify this as negative number.

		$this->load->model('shipping_rates_model');
		$rate = $this->shipping_rates_model->get_pozyx_rate($this->session->userdata('countryCode'), $this->cart->contents());
		$ShippinCost =  round($rate->rate_cents/100, 2); //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.

		//we need 4 variables from product page Item Name, Item Price, Item Number and Item Quantity.
		//Please Note : People can manipulate hidden field amounts in form,
		//In practical world you must fetch actual price from database using item id. 
		//eg : $ItemPrice = $mysqli->query("SELECT item_price FROM products WHERE id = Product_Number");
		$paypal_data ='';
		$ItemTotalPrice = 0;


		// add every item ordered to the paypal list
		$number = 0;
		foreach ($this->cart->contents() as $items){
			$paypal_data .= '&L_PAYMENTREQUEST_0_NAME'.$number.'='.urlencode($items['name']);
	        $paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER'.$number.'='.urlencode($items['id']);
	        $paypal_data .= '&L_PAYMENTREQUEST_0_AMT'.$number.'='.urlencode($items['subtotal']/$items['qty']);		
			$paypal_data .= '&L_PAYMENTREQUEST_0_QTY'.$number.'='. urlencode($items['qty']);	
		
	        //total price
	        $ItemTotalPrice = $ItemTotalPrice + $items['subtotal'];		
	        $number++;		
		} 

		// add shipping
		$paypal_data .= '&L_PAYMENTREQUEST_0_NAME'.$number.'='.urlencode('Shipping');
        $paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER'.$number.'='.urlencode('1000');
        $paypal_data .= '&L_PAYMENTREQUEST_0_AMT'.$number.'='.urlencode($ShippinCost);		
		$paypal_data .= '&L_PAYMENTREQUEST_0_QTY'.$number.'='. urlencode('1');	
		$ItemTotalPrice +=  $ShippinCost;

		$tax_rate = $this->_compute_tax_rate($this->session->userdata('countryCode'), "");
		$TotalTaxAmount = round($ItemTotalPrice*$tax_rate/100, 2);  //Sum of tax for all items in this order. 	
					
		//Grand total including all tax, insurance, shipping cost and discount
		$GrandTotal = round($ItemTotalPrice + $TotalTaxAmount + $HandalingCost + $InsuranceCost  + $ShippinDiscount, 2);		
			
		//Parameters for SetExpressCheckout, which will be sent to PayPal
		$padata = 	'&METHOD=SetExpressCheckout'.
					'&RETURNURL='.urlencode($this->config->item("PayPalReturnURL")).
					'&CANCELURL='.urlencode($this->config->item("PayPalCancelURL")).
					'&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE").
					$paypal_data.				
					'&NOSHIPPING=0'. //set 1 to hide buyer's shipping address, in-case products that does not require shipping
					'&PAYMENTREQUEST_0_ITEMAMT='.urlencode($ItemTotalPrice).
					'&PAYMENTREQUEST_0_TAXAMT='.urlencode($TotalTaxAmount).
					'&PAYMENTREQUEST_0_SHIPPINGAMT='.urlencode(0).
					'&PAYMENTREQUEST_0_HANDLINGAMT='.urlencode($HandalingCost).
					'&PAYMENTREQUEST_0_SHIPDISCAMT='.urlencode($ShippinDiscount).
					'&PAYMENTREQUEST_0_INSURANCEAMT='.urlencode($InsuranceCost).
					'&PAYMENTREQUEST_0_AMT='.urlencode($GrandTotal).
					'&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode($this->config->item("PayPalCurrencyCode")).
					'&LOCALECODE=GB'. //PayPal pages to match the language on your website.
					'&LOGOIMG=https://www.pozyx.io/assets/images/paypal_header.png'. //site logo
					'&CARTBORDERCOLOR=FFFFFF'. //border color of cart
					'&ALLOWNOTE=1';
			
		//We need to execute the "SetExpressCheckOut" method to obtain paypal token
		$paypal= new MyPayPal();
		$httpParsedResponseAr = $paypal->PPHttpPost('SetExpressCheckout', $padata, $this->config->item("PayPalApiUsername"), $this->config->item("PayPalApiPassword"), $this->config->item("PayPalApiSignature"), $this->config->item("PayPalMode"));
		
		//Respond according to message we receive from Paypal
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
		{
				//Redirect user to PayPal store with Token received.
			 	$paypalurl ='https://www'.$paypalmode.'.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$httpParsedResponseAr["TOKEN"].'';
				header('Location: '.$paypalurl);
		}else
		{
			//error, could not redirect to paypal express checkout
			$this->_paypal_error('<b>Error : </b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"])."<br><br>Could not redirect to Paypal express checkout. Please try again or use a different payment method.", $httpParsedResponseAr);

		}		
	}

	public function paypal_process()
	{
		$this->load->helper('form');
	    $this->load->library('cart');

	    if($this->cart->total() <= 0)
	    	redirect('/store/', 'refresh');

		// load all the countries
		$this->load->model('country_model');

		$this->country_model->order_by("countryName");	
		$data['countries'] = $this->country_model->as_object()->get_all();	

		//Paypal redirects back to this page using ReturnURL, We should receive TOKEN and Payer ID
		if(isset($_GET["token"]) && isset($_GET["PayerID"]))
		{
			//we will be using these two variables to execute the "DoExpressCheckoutPayment"
			//Note: we haven't received any payment yet.			
			$token = $_GET["token"];
			$payer_id = $_GET["PayerID"];

			require_once(APPPATH . "libraries/paypal.class.php");			
			$this->load->config('credentials');

			$paypalmode = ($this->config->item("PayPalMode")=='sandbox') ? '.sandbox' : '';

			$paypal= new MyPayPal();

			// Get User details
			$CheckoutDetails = $paypal->PPHttpPost('GetExpressCheckoutDetails', '&TOKEN='.urlencode($token), $this->config->item("PayPalApiUsername"), $this->config->item("PayPalApiPassword"), $this->config->item("PayPalApiSignature"), $this->config->item("PayPalMode"));
		
			$data['order'] = (object)array(
				'firstName'=>(isset($CheckoutDetails["FIRSTNAME"]) ? urldecode($CheckoutDetails["FIRSTNAME"]):""),
				'lastName'=>(isset($CheckoutDetails["LASTNAME"]) ? urldecode($CheckoutDetails["LASTNAME"]):""),
				'email'=>urldecode($CheckoutDetails["EMAIL"]),
				'tel'=>(isset($CheckoutDetails["PAYMENTREQUEST_0_SHIPTOPHONENUM"]) ? urldecode($CheckoutDetails["PAYMENTREQUEST_0_SHIPTOPHONENUM"]):(isset($CheckoutDetails["PHONENUM"]) ? urldecode($CheckoutDetails["PHONENUM"]) : "")),
				'countryCode'=>(isset($CheckoutDetails["PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE"]) ? urldecode($CheckoutDetails["PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE"]):""),
				'companyName'=>(isset($CheckoutDetails["BUSINESS"]) ? urldecode($CheckoutDetails["BUSINESS"]):""),
				'vatNumber'=>"",
				'isCompany'=>((isset($CheckoutDetails["BUSINESS"]) && $CheckoutDetails["BUSINESS"] != "") ? 1:0),
				'plug_type'=>'C',
				'billing_addressLine1'=>(isset($CheckoutDetails["PAYMENTREQUEST_0_SHIPTOSTREET"]) ? urldecode($CheckoutDetails["PAYMENTREQUEST_0_SHIPTOSTREET"]):""),
				'billing_addressLine2'=>(isset($CheckoutDetails["PAYMENTREQUEST_0_SHIPTOSTREET2"]) ? urldecode($CheckoutDetails["PAYMENTREQUEST_0_SHIPTOSTREET2"]):""),
				'billing_State'=>(isset($CheckoutDetails["PAYMENTREQUEST_0_SHIPTOSTATE"]) ? urldecode($CheckoutDetails["PAYMENTREQUEST_0_SHIPTOSTATE"]):""),
				'billing_City'=>(isset($CheckoutDetails["PAYMENTREQUEST_0_SHIPTOCITY"]) ? urldecode($CheckoutDetails["PAYMENTREQUEST_0_SHIPTOCITY"]):""),
				'billing_ZIP'=>(isset($CheckoutDetails["PAYMENTREQUEST_0_SHIPTOZIP"]) ? urldecode($CheckoutDetails["PAYMENTREQUEST_0_SHIPTOZIP"]):""),
				'billingAddressIsShipping'=>1,
				'shipping_addressLine1'=>"",
				'shipping_addressLine2'=>"",
				'shipping_State'=>"",
				'shipping_City'=>"",
				'shipping_ZIP'=>"",
				'payment_option' => 'paypal',
				'user_remarks' => (isset($CheckoutDetails["PAYMENTREQUEST_0_NOTETEXT"]) ? urldecode($CheckoutDetails["PAYMENTREQUEST_0_NOTETEXT"]):"")
			);		
		}

	    // load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
	    $this->load->view('store/checkout2', $data);
	    $this->load->view('templates/footer.php');
	}	

	/*
	public function paypal_process()
	{

		//Paypal redirects back to this page using ReturnURL, We should receive TOKEN and Payer ID
		if(isset($_GET["token"]) && isset($_GET["PayerID"]))
		{
			//we will be using these two variables to execute the "DoExpressCheckoutPayment"
			//Note: we haven't received any payment yet.			
			$token = $_GET["token"];
			$payer_id = $_GET["PayerID"];

			require_once(APPPATH . "libraries/paypal.class.php");
			$this->load->library('cart');
			$this->load->model('order_model');
			$this->load->config('credentials');

			$paypalmode = ($this->config->item("PayPalMode")=='sandbox') ? '.sandbox' : '';

			$paypal= new MyPayPal();

			// Get User details
			$CheckoutDetails = $paypal->PPHttpPost('GetExpressCheckoutDetails', '&TOKEN='.urlencode($token), $this->config->item("PayPalApiUsername"), $this->config->item("PayPalApiPassword"), $this->config->item("PayPalApiSignature"), $this->config->item("PayPalMode"));

			//echo "<br><br>" . $CheckoutDetails["SHIPTOCOUNTRYCODE"];
			
			//get session variables
			$HandalingCost 		= 0.00;  //Handling cost for this order.
			$InsuranceCost 		= 0.00;  //shipping insurance cost for this order.
			$ShippinDiscount 	= 0.00; //Shipping discount for this order. Specify this as negative number.
			$ShippinCost 		= 0.00; //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.

			$paypal_data = '';
			$ItemTotalPrice = 0;

			$number = 0;
			foreach ($this->cart->contents() as $items)
			{
				$paypal_data .= '&L_PAYMENTREQUEST_0_NAME'.$number.'='.urlencode($items['name']);
		        $paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER'.$number.'='.urlencode($items['id']);
		        $paypal_data .= '&L_PAYMENTREQUEST_0_AMT'.$number.'='.urlencode($items['subtotal']/$items['qty']);		
				$paypal_data .= '&L_PAYMENTREQUEST_0_QTY'.$number.'='. urlencode($items['qty']);	
			
		        //total price
		        $ItemTotalPrice = $ItemTotalPrice + $items['subtotal'];		
		        $number++;		
			} 

			$tax_rate = $this->_compute_tax_rate($CheckoutDetails["SHIPTOCOUNTRYCODE"], "");
			$TotalTaxAmount = $ItemTotalPrice*($tax_rate/100);  //Sum of tax for all items in this order. 	

			//Grand total including all tax, insurance, shipping cost and discount
			$GrandTotal = ($ItemTotalPrice + $TotalTaxAmount + $HandalingCost + $InsuranceCost + $ShippinCost + $ShippinDiscount);			    

			$padata = 	'&TOKEN='.urlencode($token).
						'&PAYERID='.urlencode($payer_id).
						'&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE").
						$paypal_data.
						'&PAYMENTREQUEST_0_ITEMAMT='.urlencode($ItemTotalPrice).
						'&PAYMENTREQUEST_0_TAXAMT='.urlencode($TotalTaxAmount).
						'&PAYMENTREQUEST_0_SHIPPINGAMT='.urlencode($ShippinCost).
						'&PAYMENTREQUEST_0_HANDLINGAMT='.urlencode($HandalingCost).
						'&PAYMENTREQUEST_0_SHIPDISCAMT='.urlencode($ShippinDiscount).
						'&PAYMENTREQUEST_0_INSURANCEAMT='.urlencode($InsuranceCost).
						'&PAYMENTREQUEST_0_AMT='.urlencode($GrandTotal).
						'&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode($this->config->item("PayPalCurrencyCode"));

			//We need to execute the "DoExpressCheckoutPayment" at this point to Receive payment from user.			
			$httpParsedResponseAr = $paypal->PPHttpPost('DoExpressCheckoutPayment', $padata, $this->config->item("PayPalApiUsername"), $this->config->item("PayPalApiPassword"), $this->config->item("PayPalApiSignature"), $this->config->item("PayPalMode"));
			


			//Check if everything went ok..
			if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) 
			{				
				// echo 'Your Transaction ID : '.urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]);
				

				//Sometimes Payment are kept pending even when transaction is complete. 
				//hence we need to notify user about it and ask him manually approve the transiction
				$payment_status = strtolower(urldecode($httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]));
												

				// we can retrieve transection details using either GetTransactionDetails or GetExpressCheckoutDetails
				// GetTransactionDetails requires a Transaction ID, and GetExpressCheckoutDetails requires Token returned by SetExpressCheckOut
				$padata = 	'&TOKEN='.urlencode($token);
				$paypal= new MyPayPal();
				$httpParsedResponseAr = $paypal->PPHttpPost('GetExpressCheckoutDetails', $padata, $this->config->item("PayPalApiUsername"), $this->config->item("PayPalApiPassword"), $this->config->item("PayPalApiSignature"), $this->config->item("PayPalMode"));

				if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) 
				{
					$country_code = (isset($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE"]):$this->session->userdata('countryCode'));

					// save the order in the database
					$order_information = array(
						'firstName'=>(isset($httpParsedResponseAr["FIRSTNAME"]) ? urldecode($httpParsedResponseAr["FIRSTNAME"]):""),
						'lastName'=>(isset($httpParsedResponseAr["LASTNAME"]) ? urldecode($httpParsedResponseAr["LASTNAME"]):""),
						'email'=>urldecode($httpParsedResponseAr["EMAIL"]),
						'tel'=>(isset($httpParsedResponseAr["PHONENUM"]) ? urldecode($httpParsedResponseAr["PHONENUM"]):""),
						'country'=>(isset($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE"]):""),
						'companyName'=>(isset($httpParsedResponseAr["BUSINESS"]) ? urldecode($httpParsedResponseAr["BUSINESS"]):""),
						'vatNumber'=>"",
						'billing_addressLine1'=>(isset($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOSTREET"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOSTREET"]):""),
						'billing_addressLine2'=>(isset($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOSTREET2"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOSTREET2"]):""),
						'billing_State'=>(isset($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOSTATE"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOSTATE"]):""),
						'billing_City'=>(isset($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOCITY"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOCITY"]):""),
						'billing_ZIP'=>(isset($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOZIP"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_SHIPTOZIP"]):""),
						'billingAddressIsShipping'=>1,
						'tax_rate' => $tax_rate,
						'grand_total'=> $GrandTotal*100,
						'shipping_cost' => $ShippinCost*100,
						'paymentOption' => 'paypal',
						'user_remarks' => (isset($httpParsedResponseAr["PAYMENTREQUEST_0_NOTETEXT"]) ? urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_NOTETEXT"]):"")
					);

					$orderID = $this->order_model->new_order($order_information, $this->cart->contents(), $ItemTotalPrice*100);
				    if($orderID == -1) throw new Exception("Website Error: database error. Please report this problem to the webmaster.");
				    $encodedOrderID = $this->order_model->encode_orderID($orderID);

				    // payment status: pending, completed or failed
				    $this->order_model->update_payment($orderID, $payment_status);

				    $this->_send_confirmation_email($orderID);

		    		// destroy the shopping cart of the user
		    		$this->cart->destroy();		    

		    		// redirect to the thank you page
		    		redirect('store/thank_you/'.$encodedOrderID, 'refresh');
									
					
				}else{					
					// error could not retrieve customer details
					$this->_paypal_error('<b>GetTransactionDetails failed:</b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]) . '<br><br>We were unable to obtain the transaction details. Please contact sales@pozyx.io.', $httpParsedResponseAr);				

				}
	
			}else{
				// error, could not make payment
				$this->_paypal_error('<b>Error : </b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]). '<br><br>Payment was not received. Please try again.', $httpParsedResponseAr);								
			}		
		}
	}*/

	public function _paypal_error($str, $data){

		$var = print_r($data, true);
		log_message('error', $str);
		log_message('error', 'error data: ' . $var);

		$view_data["error_str"] = $str;
		$view_data["error_source"] = "Paypal";

		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('templates/error_view.php', $view_data);
		$this->load->view('templates/footer.php');
	}

	public function _send_confirmation_email($orderID){
		
		log_message('info', 'function _send_confirmation_email() called');

		if(isset($orderID))
		{
			log_message('info', 'function send_confirmation_email() called for orderID '. $orderID);

			// now send a confirmation email		    
	        $this->load->library('email');
	        $this->load->model('order_model');      

			// get order data
			$data = $this->order_model->get_details($orderID);
			if(empty($data)){
				log_message('error', 'function send_confirmation_email() could not find requested order information.');
			}
			$data["order_items"] = $this->order_model->get_items($orderID);

			// create the pro forma invoice
			if($data["payment_method"] == "credit card"){
				$invoice_path = $this->order_model->generate_pdf_invoice("pro forma invoice", $orderID);
				$this->email->attach($invoice_path);
			}

			// email
			$this->email->from('info@pozyx.io', 'Pozyx Labs');
			$this->email->to($data["email_address"]); 	
			$this->email->reply_to('noreply@pozyx.io', 'no reply');				
			$this->email->subject('Order confirmation');			
			$this->email->message($this->load->view('store/email_txt_order_confirmation', $data, true));	

			$color = "#5F945F";		// green color
			if ( ! $this->email->send() )
			{	
				// failed to send message !
				// Loop through the debugger messages.				
				foreach ( $this->email->get_debugger_messages() as $debugger_message ){
					log_message("error", $debugger_message);					
				}

				// Remove the debugger messages as they're not necessary for the next attempt.
				$this->email->clear_debugger_messages();

				$color = "#d9534f";		// red color
			}

			// send a message to slack:
		    $slack_msg = $data["firstname"] . " " . $data["lastname"]. " ordered on the webstore.";
		    $attachment = array(
	            'fallback' => $slack_msg,
	            'color' => $color,
	            'fields' => array(
	                array(
	                    'title' => 'New order in the online store',
	                    'value' => $data["firstname"] . " " . $data["lastname"]. " (".$data["email_address"].") ordered for ".($data["grand_total"]/100)."€\nPayment with ".$data["payment_method"]."\nshipping to ".$data["countryName"],
	                    'short' => false
	                )
	            )
	        );
			$this->_slack_notify($slack_msg, '#sales', $attachment);

		}else{
			log_message('error', 'function send_confirmation_email() received empty POST request');
		}			

	}	

	private function _slack_notify($msg, $channel, $attachment)
	{
	    $curl = curl_init();
	    $url = 'https://hooks.slack.com/services/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';
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $payload = array(
	        'channel' => $channel,
	        'username' => 'Pozyx webstore',
	        'text' => $msg
	    );	    
	    
	    if ($attachment) {
	        $payload['attachments'] = array($attachment);
	    }
	    $data = 'payload=' . json_encode($payload);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);			// only do this on localhost
	    $result = curl_exec($curl);
	    if($result === FALSE) {
		    die(curl_error($curl));
		}
	    curl_close($curl);
	    //echo "\n" . 'message sent to ' . $channel;
	}

	public function _compute_tax_rate($countryCode, $vatNumber){		

		$this->load->library('vat_checker');

		// compute the tax rate
	    $tax_rate = 0;
	    if($this->vat_checker->EU_country($countryCode)){	    	
	    	//check for valid VAT number
	    	if($vatNumber == "" || $countryCode == "BE" || !$this->vat_checker->valid_EU_VAT($vatNumber, $countryCode)){
	    		$tax_rate = 21;
	    	}	    	
	    }

	    return $tax_rate;
	}


}