<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');		
	}

	public function index()
	{
		$this->load->view('welcome');
		$this->load->view('templates/footer');
	}

	public function about()
	{

		$this->load->database();
		$this->load->model('foundersClub_model');
	    $data['foundersWall'] = $this->foundersClub_model->as_object()->get_many_by('Type',  'Wall');
	    $data['foundersVideo'] = $this->foundersClub_model->as_object()->get_many_by('Type',  'Video');

		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('about', $data);
		$this->load->view('templates/footer');
	}

	public function jobs()
	{

		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('jobs');
		$this->load->view('templates/footer');
	}

	public function pozyxLabs()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('pozyx_labs_view');
		$this->load->view('templates/footer');
	}	

	public function news()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('news/detail_firmwareupdate');
		$this->load->view('templates/footer');
	}
}
