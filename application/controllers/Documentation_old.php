<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documentation extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');		
	}

	public function index()
	{
		// load the FAQ
		$this->load->database();
		$this->load->model('faq_model');
	    $data['faq'] = $this->faq_model->as_object()->get_all();



		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/faq');
		$this->load->view('docs/tutorials', $data);
		$this->load->view('templates/footer');
	}

	public function search(){

		$keywords = $this->input->post('search');
		if($keywords){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('search', 'Search', 'required|trim');
			if($this->form_validation->run()===TRUE)
			{
				$data['keywords'] = $keywords;

				$this->load->database();
				$this->load->model('faq_model');
				$data['query'] = $this->faq_model->get_search($keywords);

				// load the view
				$this->load->view('templates/header.php');
				$this->load->view('templates/nav.php');
				$this->load->view('docs/doc_search_results', $data);
				$this->load->view('templates/footer');
			}else{
				echo('error');
				//redirect('documentation', 'refresh');
			}
		}	

	}

	public function doc_Datasheet(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/doc_datasheet');
		$this->load->view('templates/footer');
	}

	public function doc_howDoesUwbWork(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/doc_howDoesUwbWork');
		$this->load->view('templates/footer');
	}

	public function doc_howDoesPositioningWork(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/doc_howDoesPositioningWork');
		$this->load->view('templates/footer');
	}

	public function doc_whereToPlaceTheAnchors(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/doc_whereToPlaceTheAnchors');
		$this->load->view('templates/footer');
	}

	public function doc_cookieLegal()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/cookie_legal_view');
		$this->load->view('templates/footer');
	}
}
