<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');		
	}

	// controller to show main store page
	public function index(){

		$this->load->helper('form');
		$this->load->helper('url');
	    $this->load->library('cart');

	    $this->load->model('product_model');
	    $data['products_kits'] = $this->product_model->as_object()->get_many_by('category', 'kit');
	    $data['products_units'] = $this->product_model->as_object()->get_many_by('category', 'single unit');

	    $this->rememberCountry();

		// Insert the product to cart		
		if ($this->input->get('id') != '')
		{
			$this->_add_to_cart($this->input->get('id'));
		}

		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('store/store.php', $data);
		$this->load->view('templates/footer.php');

	}

	// ajax call to add item to the cart
	function addItem(){
		if ($this->input->post('product_id') != '')
		{
			$this->load->helper('form');
			$this->load->helper('url');
		    $this->load->library('cart');

		    if($this->input->post('product_id') == 999)
		    {
				$result = $this->_add_to_cart($this->input->post('product_id'), $this->input->post('prod_name'), $this->input->post('prod_price'));
		    }else{		    	
				$result = $this->_add_to_cart($this->input->post('product_id'));
		    }
			$data["checkout"] = false;			
			$this->load->view('store/shopping_cart', $data);	
		}
	}

	// controller to show the product details page
	public function detail($id){

		$this->load->model('product_model');
	    $product = $this->product_model->as_array()->get($id);
	    
	    $data['productName'] = $product['name'];
	    $data['description'] = $product['description'];
	    $data['price'] = $product['price_eurocent']/100.0;

	    // load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		if($id == 4){
	    	$this->load->view('store/product_pozyx_tag', $data);
		}else if($id == 5){
	    	$this->load->view('store/product_pozyx_anchor', $data);
		}else if($id == 3){
	    	$this->load->view('store/product_pozyx_devkit', $data);
		}else if($id == 2){
	    	$this->load->view('store/product_pozyx_ready2loc', $data);
		}else if($id == 1){
	    	$this->load->view('store/product_pozyx_ready2range', $data);
		}else{
			$this->load->view('store/product_detail', $data);
		}
	    $this->load->view('templates/footer.php');

	}	

	// controller to show the checkout page
	public function checkout(){		

		$this->load->helper('form');
	    $this->load->library('cart');

	    if($this->cart->total() <= 0)
	    	redirect('/store/', 'refresh');

		// load all the countries
		$this->load->model('country_model');

		$this->country_model->order_by("countryName");	
		$data['countries'] = $this->country_model->as_object()->get_many_by('is_available', '1');
		//get_all();	

		if($this->ion_auth->logged_in()) {
			$user = $this->ion_auth->user()->row();
	  		$data["username"] = $user->first_name;
	    	$data["userid"] = $user->id;
	    }

		// default settings 
		$data['order'] = (object)array(
			'firstName'=>'',
			'lastName'=>'',
			'email'=>'',
			'tel'=>'',
			'isCompany'=>0,
			'countryCode'=> $this->session->userdata('countryCode'),
			'plug_type'=> $this->country_model->get_plug_type($this->session->userdata('countryCode')),
			'companyName'=>'',
			'vatNumber'=>'',
			'billing_addressLine1'=>'',
			'billing_addressLine2'=>'',
			'billing_State'=>'',
			'billing_City'=>'',
			'billing_ZIP'=>'',
			'billingAddressIsShipping'=>1,
			'shipping_addressLine1'=>'',
			'shipping_addressLine2'=>'',
			'shipping_State'=>'',
			'shipping_City'=>'',
			'shipping_ZIP'=>'',
			'payment_option'=>'wire transfer'
		);

	    // load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
	    //$this->load->view('store/checkout', $data);
	    $this->load->view('store/checkout2', $data);
	    $this->load->view('templates/footer.php');
	}

	private function _returnErrorWithMessage($message) 
	{
	    $a = array('result' => 1, 'errorMessage' => $message);
	    echo json_encode($a);
	}	

	/*
	 * process_order()
	 *
	 * This function is called after payment. This can be for a new order or an existing order.
	 * Everything is double checked (serverside) in terms of pricing in order to reject ordering with tampered data
	 */
	public function process_order($encodedOrderID = ''){
				
		$this->load->model('order_model');
		$this->load->model('customer_model');
		$this->load->library('cart');
		$this->load->library('encrypt');		
		
		$new_order = true;
		$email = "";
		$orderID = -1;
		$grand_total = 0;
		$orderType = $this->input->post('OrderType');

		/*----------------------- Create the new order in the database ----------------------------*/
		if($encodedOrderID == '')
		{
			// Get all the values from the form		
			$email = $this->input->post('email');
			$firstName = $this->input->post('firstName');
			$lastName = $this->input->post('lastName');	
			$countryCode = $this->input->post('country');	
			$payment_method	= $this->input->post('paymentOption');	
			

			// double check the grand total and tax rate
		    $subtotal = ($this->cart->total())*100; 
		    $tax_rate = $this->_compute_tax_rate($this->input->post('country'), $this->input->post('vatNumber'));
		    
		    $this->load->model('shipping_rates_model');
			$rate = $this->shipping_rates_model->get_pozyx_rate($this->input->post('country'), $this->cart->contents());
		    $shipping_cost = $rate->rate_cents;
		    //$shipping_cost = $rate->rate_cents;
		    $discount = 0;		    
		    if($this->ion_auth->logged_in()) {
		    	$discount = $this->input->post('reduction_percent');
		    	$shipping_cost = $this->input->post('shipping_cost');
		    }	    
		    $grand_total = $this->_compute_grand_total($subtotal, $shipping_cost, $tax_rate, $discount);

		    try
			{
			    // We must have all of this information to proceed. If it's missing, balk.		
			    if ($tax_rate != $this->input->post('tax_rate')) throw new Exception("Error: Tax rate not consistent. Possibly we were not able to verify your VAT number. Your credit card was NOT charged. Please report this problem to the webmaster.");  
			    if ($grand_total != round($this->input->post('grand_total'),0)) throw new Exception("Error: Grand total ".round($this->input->post('grand_total'),0)." and ".$grand_total." not consistent. Your credit card was NOT charged. Please report this problem to the webmaster.");	
			    if ($email == '') throw new Exception("Website Error: The email address was NULL in the payment handler script. Your credit card was NOT charged. Please report this problem to the webmaster.");
			    if (!isset($firstName)) throw new Exception("Website Error: FirstName was NULL in the payment handler script. Your credit card was NOT charged. Please report this problem to the webmaster.");
			    if (!isset($lastName)) throw new Exception("Website Error: LastName was NULL in the payment handler script. Your credit card was NOT charged. Please report this problem to the webmaster.");
			    if (!isset($subtotal)) throw new Exception("Website Error: Price was NULL in the payment handler script. Your credit card was NOT charged. Please report this problem to the webmaster.");	
			    if( !$this->ion_auth->logged_in() and $orderType != 'weborder')  throw new Exception("Website Error: You do not have the authorisation for this action. Your credit card was NOT charged. Please report this problem to the webmaster.");	

			    // add the order to the database
			    $orderID = $this->order_model->new_order($this->input->post(), $this->cart->contents(), $subtotal);
			    if($orderID == -1) throw new Exception("Website Error: database error. Your credit card was NOT charged. Please report this problem to the webmaster.");
			    $encodedOrderID = $this->order_model->encode_orderID($orderID);
			}
			catch (Exception $e) 
			{
			    // One or more variables was NULL
			    $message = $e->getMessage();
			    $this->_returnErrorWithMessage($message);
			    exit;
			}	
		}else{
			// the order is already stored in the database
			$new_order = false;
			$payment_method = "credit card";

			// get order information
			$orderID = $this->order_model->decode_orderID($encodedOrderID);
			$data = $this->order_model->get_details($orderID);
			if($data != NULL)
			{
				$grand_total = $data["grand_total"];
				$email = $data["email_address"];
			}else{
				$this->_returnErrorWithMessage("Website Error: Order number was not valid. Your credit card was NOT charged. Please report this problem to the webmaster.");
			    exit;
			}
		}
		
		    
	    /*----------------------- Process the payment ----------------------------*/
	    if($payment_method == "credit card")
	    {
	    	$stripe_token = $this->input->post('stripeToken'); 

	    	if(isset($stripe_token))
	    	{
	    		// process the stripe payment
		    	$this->load->helper('payment_helper');
		    	$message = stripe_process($orderID, $stripe_token);

		    	if($message != "")
		    	{
		    		// we receied an error message, payment failed
					$this->_returnErrorWithMessage($message);
		    		exit();
		    	}
		    }else{
		    	$this->_returnErrorWithMessage("Website Error: authorization failed. Your credit card was NOT charged. Please report this problem to the webmaster.");
		    	exit();
		    }

		}else if($payment_method == "paypal")
		{
			
			$paypal_token = $this->input->post('paypal_token');
			$paypal_payerID = $this->input->post('paypal_payerID');

			if(isset($paypal_token) && isset($paypal_payerID))
			{
				// process the paypal payment
				$this->load->helper('payment_helper');							    	
		    	$message = paypal_process($orderID, $paypal_token, $paypal_payerID);

		    	echo "paypall processed " . "<br>";

		    	if($message != "")
		    	{
		    		// we receied an error message, payment failed
					$this->_returnErrorWithMessage($message);
		    		exit();
		    	}
			}
		}else{
			// payment method is wire transfer, don't do anything
		}
   

		/*----------------------- send a confirmation email asynchronously ----------------------------*/ 
		if($new_order)
	    {
		    if(!$orderType == 'weborder'){
				$this->_send_confirmation_email($orderID);
			}else{
				// update the payment to quote
				if($orderType == 'quote')
			    	$this->order_model->update_payment($orderID, 'quote');
			}
		}else
		{
			// send a payment confirmation
			$this->_send_payment_confirmation_email($orderID);
		}

	    // destroy the shopping cart of the user
	    $this->cart->destroy();		    

		/*----------------------- Return some json data to indicate success ----------------------------*/
        // This is accessible to the jQuery Ajax call return-handler in the checkout page
        $array = array('result' => 0, 'encodedOrderID' => $encodedOrderID, 'message' => 'Thank you; your transaction was successful!');
        if (ob_get_contents()) ob_end_clean();		// clean the output buffer in case there was an email error
        echo json_encode($array);

        exit;
	}

	public function invoice_html($id){
		$this->load->model('order_model');

		// load the order data
		$data = $this->order_model->get_details($id);
		$data["order_items"] = $this->order_model->get_items($id);
		$data["document_type"] = "invoice";
		
		$this->load->view('templates/pdf/invoice', $data);
	}

	public function proformainvoice($encodedOrder_id){
		$this->load->model('order_model');

		// decode the order number	
		$order_ID = $this->order_model->decode_orderID($encodedOrder_id);	
		$path = $this->order_model->generate_pdf_invoice("pro forma invoice", $order_ID);

		redirect($path, 'refresh');
	}

	public function quote($encodedOrder_id){
		$this->load->model('order_model');

		// decode the order number	
		$order_ID = $this->order_model->decode_orderID($encodedOrder_id);	
		$path = $this->order_model->generate_pdf_invoice("quote", $order_ID);

		echo($path);

		//redirect($path, 'refresh');
	}

	public function invoice($encodedOrder_id){

		$this->load->library('encrypt');
		$this->load->model('order_model');

		// decode the order number	
		$order_ID = $this->order_model->decode_orderID($encodedOrder_id);	
		$path = $this->order_model->generate_pdf_invoice("invoice", $order_ID);

		echo($path);

		//redirect($path, 'refresh');		
	}

	public function change_plug_type(){
		$this->load->model('order_model');
		$this->order_model->update_plug_type( $this->input->post('orderID'), $this->input->post('plugType'));
	}

	// remember the country code using the user IP address
	public function rememberCountry(){

		$this->load->library('session');				
		
		// in case get the Country code from the IP address
		if (! $this->session->userdata('countryCode')) {
			/*
			This product includes GeoLite data created by MaxMind, available from 
			<a href="http://www.maxmind.com">http://www.maxmind.com</a>.

			database file 'GeoIP.dat' downloaded from: http://dev.maxmind.com/geoip/legacy/geolite/#Databases			
			*/

			try{
				switch (ENVIRONMENT)
				{
					case 'production':
						require_once "Net/GeoIP.php";
						$geoip = Net_GeoIP::getInstance("/home/pozydhjv/php/Net/GeoIP.dat");
						$countryCode = $geoip->lookupCountryCode( $_SERVER['REMOTE_ADDR'] );
						$this->session->set_userdata('countryCode', $countryCode);	
						break;
					default:
						$this->session->set_userdata('countryCode', 'BE');	
						break;
				}
				
			}catch(Exception $e) {
				$this->session->set_userdata('countryCode', 'BE');	
			}	
		}		
	}

	// add a product to the cart (with shipping cost) and return to the main store page
	private function _add_to_cart($id, $name="", $price_eur=0.0){		

		$this->load->library('cart');
	    $this->load->model('product_model');

		// Insert the product to cart
		if ($id == 999 && is_numeric($price_eur))
		{
			$prod_obj = array(
					'id'      => $id."_".preg_replace('/[^A-Za-z0-9\-]/', '', $name)."_".preg_replace('/[^A-Za-z0-9\-]/', '', $price_eur.""),
	               'qty'     => 1,
	               'price'   => $price_eur,
	               'name'    => $name               
	            );
				
			// modify the rules to allow quotes in the product name
			$this->cart->product_name_rules = "\.\:\-_\"\' a-z0-9";
			
			if($this->cart->insert($prod_obj))
			{		
				// success
				return 0;
			}else{
				return "could not add custom item to the shopping cart.";
			}
		}else if ($id != '')
		{
			// get the product from the database
			$product = $this->product_model->as_array()->get($id);

			if(!empty($product)){
				$prod_obj = array(
					'id'      => $product["id"],
	               'qty'     => 1,
	               'price'   => ($product["price_eurocent"]/100.0),
	               'name'    => $product["name"]               
	            );
				
				// modify the rules to allow quotes in the product name
				$this->cart->product_name_rules = "\.\:\-_\"\' a-z0-9";
				
				if($this->cart->insert($prod_obj)){				

					// success
					return 0;
				}else{
					return "could not add item to the shopping cart.";
				}
				

			}else{				
				// invalide product id			
				return "Product not found.";
			}				
		}else{
			return "Product id illigal";
		}
	}

	// Empty the shopping cart and return to the store main page
	public function empty_cart(){

		$this->load->library('cart');
		$this->cart->destroy();

		redirect('/store/', 'refresh');
	}	

	public function get_shipping_rate($country_code)
	{
		$this->load->library('cart');
		$this->load->model('product_model');
		$this->load->model('shipping_rates_model');
			  		
		$rate = $this->shipping_rates_model->get_pozyx_rate($country_code, $this->cart->contents());

		
		return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode((array)$rate));
	}

	public function thank_you($encodedOrder_id){
		$this->load->library('encrypt');
		$this->load->model('order_model');

		// decode the order number	
		$order_ID = $this->order_model->decode_orderID($encodedOrder_id);
		$data = $this->order_model->get_details($order_ID);
		if(!empty($data)){
			$data["order_items"] = $this->order_model->get_items($order_ID);	
			$data["firmware_version"] = $this->latestFirmware();
			$data["allow_download"]	= $this->validateDownload($order_ID,$encodedOrder_id);			

			// load the view
			$this->load->view('templates/header.php');
			$this->load->view('templates/nav.php');
		    $this->load->view('store/thank_you_view', $data);
		    $this->load->view('templates/footer.php');
		}else{
			redirect('/store/', 'refresh');
		}
	}

	public function test()
	{
		// add a test dummy item to the cart
		$this->_add_to_cart(6);
		redirect('/store/', 'refresh');
	}

	public function test_email()
	{

		// send the email asynchronously

		$data = array("OrderId" => 5);
		$this->_start_asyncTask('https://www.pozyx.io/Store/send_confirmation_email.php', $data);

		$this->_send_confirmation_email(5);

		echo('this is it!');
		echo($_SERVER['SERVER_PORT']);
		echo($_SERVER['SERVER_ADDR']);
	}

	public function send_confirmation_email(){

		$orderID = $this->input->post("OrderId");
		log_message('info', 'function send_confirmation_email() called');

		if(isset($orderID))
		{
			log_message('info', 'function send_confirmation_email() called for orderID '. $orderID);

			// now send a confirmation email		    
	        $this->load->library('email');
	        $this->load->model('order_model');        
			$this->load->library('encrypt');	

			// get order data
			$data = $this->order_model->get_details($orderID);
			if(empty($data)){
				log_message('error', 'function send_confirmation_email() could not find requested order information.');
			}
			$data["order_items"] = $this->order_model->get_items($order_ID);

			// create the pro forma invoice
			if($data["payment_method"] == "credit card"){
				$invoice_path = $this->order_model->generate_pdf_invoice("pro forma invoice", $orderID);
				$this->email->attach($invoice_path);
			}

			// email
			$this->email->from('info@pozyx.io', 'Pozyx Labs');
			$this->email->to($data["email_address"]); 	
			$this->email->reply_to('noreply@pozyx.io', 'no reply');				
			$this->email->subject('Order confirmation');
			

			
			$this->email->message($this->load->view('store/email_txt_order_confirmation', $data, true));	

			$color = "#5F945F";		// green color
			if ( ! $this->email->send() )
			{	
				// failed to send message !
				// Loop through the debugger messages.				
				foreach ( $this->email->get_debugger_messages() as $debugger_message ){
					log_message("error", $debugger_message);
					echo($debugger_message);
				}

				// Remove the debugger messages as they're not necessary for the next attempt.
				$this->email->clear_debugger_messages();

				$color = "#d9534f";		// red color
			}

			// send a message to slack:
		    $slack_msg = $data["firstname"] . " " . $data["lastname"]. " ordered on the webstore.";
		    $attachment = array(
	            'fallback' => $slack_msg,
	            'color' => $color,
	            'fields' => array(
	                array(
	                    'title' => 'New order in the online store',
	                    'value' => $data["firstname"] . " " . $data["lastname"]. " (".$data["email_address"].") ordered for ".($data["grand_total"]/100)."€\nPayment with ".$data["payment_method"]."\nshipping to ".$data["countryName"],
	                    'short' => false
	                )
	            )
	        );
			$this->_slack_notify($slack_msg, '#sales', $attachment);



		}else{
			log_message('error', 'function send_confirmation_email() received empty POST request');
		}			

	}	

	public function _send_payment_confirmation_email($orderID){
		
		log_message('info', 'function _send_confirmation_email() called');

		if(isset($orderID))
		{
			log_message('info', 'function send_confirmation_email() called for orderID '. $orderID);

			// now send a confirmation email		    
	        $this->load->library('email');
	        $this->load->model('order_model');      

			// get order data
			$data = $this->order_model->get_details($orderID);
			if(empty($data)){
				log_message('error', 'function send_confirmation_email() could not find requested order information.');
			}
			$data["order_items"] = $this->order_model->get_items($orderID);

			// create the pro forma invoice
			if($data["payment_method"] == "credit card"){
				$invoice_path = $this->order_model->generate_pdf_invoice("invoice", $orderID);
				$this->email->attach($invoice_path);
			}

			// email
			$this->email->from('info@pozyx.io', 'Pozyx Labs');
			$this->email->to($data["email_address"]); 	
			$this->email->reply_to('noreply@pozyx.io', 'no reply');				
			$this->email->subject('Order confirmation');		
			$this->email->message($this->load->view('store/email_txt_payment_confirmation', $data, true));  	

		}else{
			log_message('error', 'function send_confirmation_email() received empty POST request');
		}			

	}	

	public function _send_confirmation_email($orderID){
		
		log_message('info', 'function _send_confirmation_email() called');

		if(isset($orderID))
		{
			log_message('info', 'function send_confirmation_email() called for orderID '. $orderID);

			// now send a confirmation email		    
	        $this->load->library('email');
	        $this->load->model('order_model');      

			// get order data
			$data = $this->order_model->get_details($orderID);
			if(empty($data)){
				log_message('error', 'function send_confirmation_email() could not find requested order information.');
			}
			$data["order_items"] = $this->order_model->get_items($orderID);

			// create the pro forma invoice
			if($data["payment_method"] == "credit card"){
				$invoice_path = $this->order_model->generate_pdf_invoice("pro forma invoice", $orderID);
				$this->email->attach($invoice_path);
			}

			// email
			$this->email->from('info@pozyx.io', 'Pozyx Labs');
			$this->email->to($data["email_address"]); 	
			$this->email->reply_to('noreply@pozyx.io', 'no reply');				
			$this->email->subject('Order confirmation');			
			$this->email->message($this->load->view('store/email_txt_order_confirmation', $data, true));	

			$color = "#5F945F";		// green color
			if ( ! $this->email->send() )
			{	
				// failed to send message !
				// Loop through the debugger messages.				
				foreach ( $this->email->get_debugger_messages() as $debugger_message ){
					log_message("error", $debugger_message);					
				}

				// Remove the debugger messages as they're not necessary for the next attempt.
				$this->email->clear_debugger_messages();

				$color = "#d9534f";		// red color
			}

			// send a message to slack:
		    $slack_msg = $data["firstname"] . " " . $data["lastname"]. " ordered on the webstore.";
		    $attachment = array(
	            'fallback' => $slack_msg,
	            'color' => $color,
	            'fields' => array(
	                array(
	                    'title' => 'New order in the online store',
	                    'value' => $data["firstname"] . " " . $data["lastname"]. " (".$data["email_address"].") ordered for ".($data["grand_total"]/100)."€\nPayment with ".$data["payment_method"]."\nshipping to ".$data["countryName"],
	                    'short' => false
	                )
	            )
	        );
			$this->_slack_notify($slack_msg, '#sales', $attachment);

		}else{
			log_message('error', 'function send_confirmation_email() received empty POST request');
		}			

	}	

	// function to do a background operation such as send email or create a pdf invoice. This opens a socket to the url of the task
	function _start_asyncTask($url, $params)
	{
	    foreach ($params as $key => &$val) {
	      if (is_array($val)) $val = implode(',', $val);
	        $post_params[] = $key.'='.urlencode($val);
	    }
	    $post_string = implode('&', $post_params);

	    $parts=parse_url($url);

	    print_r($parts);

		$port = 80;
        $host = $parts['host'];
	    if ( strcmp( $parts['scheme'], 'https' ) == 0 )        {
            $port = 443;
            $host = "ssl://" . $parts['host'];
        }

	    $fp = fsockopen($host, $port, $errno, $errstr, 30);

	    if($fp==0)
	    	echo("Couldn't open a socket to ".$url." (".$errstr.")");

	    $out = "POST ".$parts['path']." HTTP/1.1\r\n";
	    $out.= "Host: ".$host."\r\n";
	    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
	    $out.= "Content-Length: ".strlen($post_string)."\r\n";
	    $out.= "Connection: Close\r\n\r\n";
	    if (isset($post_string)) $out.= $post_string;

	    if(!fwrite($fp, $out)){
	    	echo('Could not write to socket.');
	    }

	    fclose($fp);
	}

	// Send a message to slack on a given slack channel
	private function _slack_notify($msg, $channel, $attachment)
	{
	    $curl = curl_init();
	    $url = 'https://hooks.slack.com/services/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';
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $payload = array(
	        'channel' => $channel,
	        'username' => 'Pozyx webstore',
	        'text' => $msg
	    );	    
	    
	    if ($attachment) {
	        $payload['attachments'] = array($attachment);
	    }
	    $data = 'payload=' . json_encode($payload);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);			// only do this on localhost
	    $result = curl_exec($curl);
	    if($result === FALSE) {
		    die(curl_error($curl));
		}
	    curl_close($curl);
	    //echo "\n" . 'message sent to ' . $channel;
	}

	public function _compute_tax_rate($countryCode, $vatNumber){		

		$this->load->library('vat_checker');

		// compute the tax rate
	    $tax_rate = 0;
	    if($this->vat_checker->EU_country($countryCode)){	    	
	    	//check for valid VAT number
	    	if($vatNumber == "" || $countryCode == "BE" || !$this->vat_checker->valid_EU_VAT($vatNumber, $countryCode)){
	    		$tax_rate = 21;
	    	}	    	
	    }

	    return $tax_rate;
	}

	public function _compute_grand_total($subtotal, $shipping_cost, $tax_rate, $discount){

	    return round(($subtotal*(1-$discount/100) + $shipping_cost)*(1.0+$tax_rate/100.0), 0);
	}

	public function show_allrates()
	{
		set_time_limit ( 1000 );

		// load all the countries
		$this->load->model('shipping_rates_model');		
		$this->load->model('country_model');      

		$this->country_model->order_by("countryName");	
		$countries = $this->country_model->as_object()->get_all();	

		/*
		echo "Loading countries";

		$curl = curl_init();
	    $url = 'https://restcountries.eu/rest/v1/all';
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);			// only do this on localhost
	    $result = curl_exec($curl);
	    if($result === FALSE) {
		    die(curl_error($curl));
		}		
	    curl_close($curl);
		*/




		require_once(APPPATH . 'libraries/Easypost/lib/easypost.php');

		$this->load->config('credentials');
		\EasyPost\EasyPost::setApiKey($this->config->item("EasypostKey"));
		echo('easypost loaded.<br><br>');

		$address_BE = \EasyPost\Address::create(array(
		'name' => 'Samuel Van de Velde',
		'street1' => 'Spellewerkstraat 46',
		'city' => 'Mariakerke',
		'state' => 'Oost-Vlaanderen',
		'zip' => '9030',
		'country' => 'BE',
		'email' => 'info@pozyx.io'
		));		

		// small 1kg box
		/*
		$parcel = \EasyPost\Parcel::create(array(
		'length' => 9,
		'width' => 6,
		'height' => 2,
		'weight' => 35	
		));
		*/

		// default TNT 3kg box
		$parcel = \EasyPost\Parcel::create(array(
		'length' => 15,
		'width' => 12,
		'height' => 6,
		'weight' => 106	
		));
		
		$i = 0;

		echo "<table style='border:1px solid gray'>";
		echo "<th><td>Num</td><td>Code</td><td>Country name</td><td>capital</td><td>Rates</td></th>";      

		foreach ($countries as $country)
		{
			/*echo $country->alpha2Code . " - ";
			echo $country->name . ", ";
			echo $country->capital;
			echo "<br><br>";
			*/	

			$i++;
			/*if($i <97 || $i > 110	){
				continue;
			}*/
	
			if( !$country->is_available )
				continue;

    		echo "<tr><td>". ($i) . "</td><td>";                         
        	echo "<td>" . $country->countryCode . "</td>";
        	echo "<td>" . $country->countryName . "</td>";   
        	echo "<td>" . $country->capital . "</td>";           	
        	echo "<td>";


        	$db_rates = $this->shipping_rates_model->as_object()->get_many_by(array('carrier'=>'TNTExpress', 'countryCode'=>$country->countryCode, 'weight_g'=>3000, 'box_size3D_inch'=>'15x12x6'));
        	
        	if( count($db_rates) == 0 ){
      		
      			$zip = $country->capital_postalcode;
      			if($zip == '')
      			{

      				continue;

		        	$zip = 0;

		        	$curl = curl_init();
				    $url = 'http://api.geonames.org/findNearbyPostalCodesJSON?placename=' . urlencode($country->capital) . '&country=' . urlencode($country->countryCode) . '&radius=10&username=supersammeken';
				    curl_setopt($curl, CURLOPT_URL, $url);
				    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
				    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);			// only do this on localhost
				    $result2 = curl_exec($curl);
				    if($result2 === FALSE) {
					    die(curl_error($curl));
					}		
				    curl_close($curl);

				    $tmp = json_decode($result2);

				    if(count($tmp->postalCodes) > 0)
				    {				    	
				    	$zip = $tmp->postalCodes[0]->postalCode;
				    	$this->country_model->update_by(array('countryCode'=>$country->countryCode), array('capital_postalcode'=>$zip));
				    }
			    }

				//if(count($tmp->postalCodes) > 0)
				if(true)
				{
					//$zip = $country->capital_postalcode;
					echo "<b>ZIP: " . $zip . "</b><br>";
				
					echo "looking for rates<br>";
					$address_TO = \EasyPost\Address::create(array(
					    'country' => $country->countryCode,
					    'city' => $country->capital,
					    'zip' => $zip
					  ));	

		    		$shipment = \EasyPost\Shipment::create(array(					
						"from_address" => $address_BE,
						"to_address" => $address_TO,
						"parcel" => $parcel
					));

		        	foreach ($shipment->rates as $rate) {

		        		//echo($rate->carrier."<br>");
		        		if($rate->carrier == "TNTExpress")
		        		{
			        		$mysql_date_now = date("Y-m-d H:i:s");
			        		$shipping_rate = array(
								'countryCode'      => $country->countryCode,
					        	'carrier' => $rate->carrier,
					        	'service' => $rate->service,
					        	'rate_cents' => $rate->rate*100,
					        	'currency' => $rate->currency,		        	
					        	'delivery_days' => $rate->delivery_days,
					        	'last_updated' => $mysql_date_now,
					        	'weight_g' => 3000,
					        	'box_size3D_inch'=>'15x12x6'

					        );
							$this->shipping_rates_model->insert($shipping_rate);	        			
						}
							//echo $rate->currency . " ";
		        		
					  	//echo $rate->carrier . " ";
						//echo $rate->service . " ";
						//echo $rate->rate . " ";
						//echo $rate->currency . " ";
						//echo $rate->delivery_days . " days";
						//echo $rate->id . " ";
						//echo "<br>";
						
					}
				}
				

				// reload all the rates from the database
				$db_rates = $this->shipping_rates_model->as_object()->get_many_by(array('carrier'=>'TNTExpress', 'countryCode'=>$country->countryCode, 'weight_g'=>3000, 'box_size3D_inch'=>'15x12x6'));
        	}

        	// print all the rates from the database
        	foreach ($db_rates as $rate) 
        	{        		
	        	echo round($rate->rate_cents*0.89)/100.0 . " ";
				echo("EUR ");
				echo $rate->service . "<br>";
			}
        	

        	echo "</td>";            	    

        	echo "</tr>";
        }

        echo "</table>";

	}

	public function show_rates(){

		require_once(APPPATH . 'libraries/Easypost/lib/easypost.php');

		$this->load->config('credentials');

		\EasyPost\EasyPost::setApiKey($this->config->item("EasypostKey"));

		echo('easypost loaded.<br><br>');

		$address_BE = \EasyPost\Address::create(array(
		'name' => 'Samuel Van de Velde',
		'street1' => 'Spellewerkstraat 46',
		'city' => 'Mariakerke',
		'state' => 'Oost-Vlaanderen',
		'zip' => '9030',
		'country' => 'BE',
		'email' => 'info@pozyx.io'
		));		

		$parcel = \EasyPost\Parcel::create(array(
		'length' => 9,
		'width' => 6,
		'height' => 2,
		'weight' => 35	
		));

		// load all the countries
		$this->load->model('country_model');

		$this->country_model->order_by("countryName");	
		$countries = $this->country_model->as_object()->get_all();	
		$i = 1;

		echo "<table style='border:1px solid gray'>";
		echo "<th><td>Num</td><td>Code</td><td>Country name</td><td>TNT GlobalExpress</td><td>TNT EconomyExpress</td></th>";      

		foreach ($countries as $country) {
			if($i == 5){
				break;
			}

        	if ($country->countryName != "") { 

        		$address_TO = \EasyPost\Address::create(array(
				    'country' => $country->countryCode
				  ));	

        		$shipment = \EasyPost\Shipment::create(array(					
					"from_address" => $address_BE,
					"to_address" => $address_TO,
					"parcel" => $parcel
				));

        		echo "<tr><td>". ($i++) . "</td><td>";                         
            	echo "<td>" . $country->countryCode . "</td>";
            	echo "<td>" . $country->countryName . "</td>";   
            	echo "<td>";

            	foreach ($shipment->rates as $rate) {
            		if($rate->service == "GlobalExpress")
            		{
            			echo $rate->rate . " ";
						echo $rate->currency . " ";
            		}
				  	/*echo $rate->carrier . " ";
					echo $rate->service . " ";
					echo $rate->rate . " ";
					echo $rate->currency . " ";
					echo $rate->delivery_days . " days";
					//echo $rate->id . " ";
					echo "<br>";
					*/
				}

            	echo "</td>";    

            	echo "<td>";

            	foreach ($shipment->rates as $rate) {
            		if($rate->service == "EconomyExpress")
            		{
            			echo $rate->rate . " ";
						echo $rate->currency . " ";
            		}
				  	/*echo $rate->carrier . " ";
					echo $rate->service . " ";
					echo $rate->rate . " ";
					echo $rate->currency . " ";
					echo $rate->delivery_days . " days";
					//echo $rate->id . " ";
					echo "<br>";
					*/
				}

            	echo "</td>";           

            	echo "</tr>";
            }
        }

        echo "</table>";

	}

	public function ship(){

		// load all the countries
		$this->load->model('country_model');
		$countries = $this->country_model->load_country_data();

		return;

		require_once(APPPATH . 'libraries/Easypost/lib/easypost.php');

		$this->load->config('credentials');

		\EasyPost\EasyPost::setApiKey($this->config->item("EasypostKey"));

		echo('easypost loaded.<br><br>');

		/*$shipment = \EasyPost\Shipment::create(array(
		  'to_address' => array(
		    'name' => 'George Costanza',
		    'company' => 'Vandelay Industries',
		    'street1' => '1 E 161st St.',
		    'city' => 'Bronx',
		    'state' => 'NY',
		    'zip' => '10451'
		  ),
		  'from_address' => array(
		    'company' => 'EasyPost',
		    'street1' => '118 2nd Street',
		    'street2' => '4th Floor',
		    'city' => 'San Francisco',
		    'state' => 'CA',
		    'zip' => '94105',
		    'phone' => '415-528-7555'
		  ),
		  'parcel' => array(
		    'length' => 9,
		    'width' => 6,
		    'height' => 2,
		    'weight' => 10
		  )
		));*/

		/*$shipment = \EasyPost\Shipment::create(array(
		  'to_address' => array(
		    'name' => 'George Costanza',
		    'company' => 'Vandelay Industries',
		    'street1' => '1 E 161st St.',
		    'city' => 'Bronx',
		    'state' => 'NY',
		    'zip' => '10451'
		  ),
		  'from_address' => array(
		    'company' => 'Pozyx Labs',
		    'street1' => 'Spellewerkstraat 43',
		    'street2' => '',
		    'city' => 'Gent',
		    'state' => 'Oost-Vlaanderen',
		    'zip' => '9030',
		    'phone' => '0472883167'
		  ),
		  'parcel' => array(
		    'predefined_package' => 'FedExBox',
		    'weight' => 35		    
		  )
		));*/

	/*
		foreach ($shipment->rates as $rate) {
  			echo $rate->carrier . " ";
			echo $rate->service . " ";
			echo $rate->rate . " ";
			echo $rate->id . " ";
			echo "<br>";
		}
		*/

		//$shipment->buy($shipment->lowest_rate(array('USPS'), array('First')));


		$address_BE = \EasyPost\Address::create(array(
		'name' => 'Samuel Van de Velde',
		'street1' => 'Brusselstraat 5',
		'city' => 'Ternat',
		'state' => 'Vlaams-Brabant',
		'zip' => '1740',
		'country' => 'BE',
		'email' => 'test@gmail.com'
		));

		$address_US2 = \EasyPost\Address::create(array(
		    'company' => 'EasyPost',
		    'street1' => '118 2nd Street',
		    'street2' => '4th Floor',
		    'city' => 'San Francisco',
		    'state' => 'CA',
		    'zip' => '94105',
		    'phone' => '415-528-7555'
		  ));

		$address_US3 = \EasyPost\Address::create(array(
		    'country' => 'MA',
		    'city' => 'Rabat' 
		  ));

		$address_US = \EasyPost\Address::create(array(
		'name' => 'George Costanza',
		'company' => 'Vandelay Industries',
		'street1' => '1 E 161st St.',
		'city' => 'Bronx',
		'state' => 'NY',
		'zip' => '10451'

		));

		$parcel = \EasyPost\Parcel::create(array(
		'length' => 9,
		'width' => 6,
		'height' => 2,
		'weight' => 35	
		));

		$shipment = \EasyPost\Shipment::create(array(
		"to_address" => $address_US3,
		"from_address" => $address_BE,
		"parcel" => $parcel
		));
			
		foreach ($shipment->messages as $msg) {
			echo $msg->type . " ";
			echo $msg->carrier . ": ";
			print_r($msg->message);
			echo "<br>";
		}

		echo "<br><br>";
		foreach ($shipment->rates as $rate) {
		  	echo $rate->carrier . " ";
		echo $rate->service . " ";
		echo $rate->rate . " ";
		echo $rate->currency . " ";
		echo $rate->delivery_days . " days";
		//echo $rate->id . " ";
		echo "<br>";
		}
		
/*
		$address_home = \EasyPost\Address::create(array(
		    'name' => 'Samuel Van de Velde',
		    'street1' => 'Spellewerkstraat 46',
		    'city' => 'Mariakerke',
		    'state' => 'Oost-Vlaanderen',
		    'zip' => '9030',
		    'country' => 'BE',
		    'email' => 'info@pozyx.io'
		  ));

		$address_home2 = \EasyPost\Address::create(array(
		    'name' => 'Samuel Van de Velde',
		    'street1' => 'Koningin Elisabethlaan 120',
		    'city' => 'Gent',
		    'state' => 'Oost-Vlaanderen',
		    'zip' => '9000',
		    'country' => 'BE',
		    'email' => 'test2@gmail.com'
		  ));

		$address_US1 = \EasyPost\Address::create(array(
		    'name' => 'George Costanza',
		    'company' => 'Vandelay Industries',
		    'street1' => '1 E 161st St.',
		    'city' => 'Bronx',
		    'state' => 'NY',
		    'zip' => '10451'
		  ));

		$address_US2 = \EasyPost\Address::create(array(
		    'company' => 'EasyPost',
		    'street1' => '118 2nd Street',
		    'street2' => '4th Floor',
		    'city' => 'San Francisco',
		    'state' => 'CA',
		    'zip' => '94105',
		    'phone' => '415-528-7555'
		  ));
		
		$parcel = \EasyPost\Parcel::create(array(
		    //'predefined_package' => 'FedExBox',
		    'length' => 9,
		    'width' => 6,
		    'height' => 2,
		    'weight' => 35		    
		  ));

		$customs_info = \EasyPost\CustomsInfo::create(array(
			'currency' => 'EUR',
			'delivery_confirmation' => 'SIGNATURE',
			'invoice_number' => 12345
			));

		try{
			$shipment = \EasyPost\Shipment::create(array(
			"to_address" => $address_US1,
			"from_address" => $address_home,
			"parcel" => $parcel//,
			//"customs_info" => $customs_info,
			//"options" => array('address_validation_level' => 0)
			));
		} catch (Exception $e) {
		    echo "Status: " . $e->getHttpStatus() . ":\n";
		    echo $e->getMessage();
		    if (!empty($e->param)) {
		        echo "\nInvalid param: {$e->param}";
		    }
		}

		echo "<br><br>";
		foreach ($shipment->rates as $rate) {
  			echo $rate->carrier . " ";
			echo $rate->service . " ";
			echo $rate->rate . " ";
			echo $rate->id . " ";
			echo "<br>";
		}
*/
		/*$shipment->buy($shipment->lowest_rate(array('USPS'), array('First')));

		// Print PNG link
		echo($shipment->postage_label->label_url . "<br>");
		// Print Tracking Code
		echo($shipment->tracking_code . "<br><br>");*/

		

	}


	public function shippo(){
		require_once(APPPATH . 'libraries/shippo/lib/Shippo.php');

		Shippo::setApiKey("f134390f646e687ede51e17f14ed96e769c5488d");

		echo("Shippo loaded.<br>");

		// example fromAddress
		$fromAddress = array(
		    'object_purpose' => 'PURCHASE',
		    'name' => 'Shippo Itle"',
		    'company' => 'Shippo',
		    'street1' => '215 Clayton St.',
		    'city' => 'San Francisco',
		    'state' => 'CA',
		    'zip' => '94117',
		    'country' => 'US',
		    'phone' => '+1 555 341 9393',
		    'email' => 'support@goshipppo.com');

		$homeAddress = array(
			'object_purpose' => 'PURCHASE',
			'name' => 'Samuel Van de Velde',
			'company' => 'Pozyx Labs BVBA',
		    'street1' => 'Spellewerkstraat 46',
		    'city' => 'Mariakerke',
		    'state' => 'Oost-Vlaanderen',
		    'zip' => '9030',
		    'country' => 'BE',
		    'phone' => '+32472883167',
		    'email' => 'info@pozyx.io');

		// example fromAddress
		$toAddress = array(
		    'object_purpose' => 'PURCHASE',
		    'name' => 'Mr Hippo"',
		    'company' => 'San Diego Zoo"',
		    'street1' => '2920 Zoo Drive"',
		    'city' => 'San Diego',
		    'state' => 'CA',
		    'zip' => '92101',
		    'country' => 'US',
		    'phone' => '+1 555 341 9393',
		    'email' => 'hippo@goshipppo.com');

		// example parcel
		$parcel = array(
		    'length'=> '5',
		    'width'=> '5',
		    'height'=> '5',
		    'distance_unit'=> 'in',
		    'weight'=> '2',
		    'mass_unit'=> 'lb',
		);

		// example Shipment object
		$shipment = Shippo_Shipment::create(
		array(
		    'object_purpose'=> 'PURCHASE',
		    'address_from'=> $toAddress,
		    'address_to'=> $homeAddress,
		    'parcel'=> $parcel,
		    'submission_type'=> 'PICKUP',
		    'insurance_amount'=> '30',
		    'insurance_currency'=> 'USD'
		));

		// Wait for rates to be generated
		$ratingStartTime=time();
		while (($shipment["object_status"] == "QUEUED" || $shipment["object_status"] == "WAITING")){
		    $shipment = Shippo_Shipment::retrieve($shipment["object_id"]);
		    usleep(200000); //sleeping 200ms
		    if (time()-$ratingStartTime>25) break;
		    }

		// Get all rates for shipment.
		$rates = Shippo_Shipment::get_shipping_rates(array('id'=> $shipment["object_id"]));

		// Get the first rate from the rates results
		echo "getting rates..";
		//print_r($rates);
		echo "<br><br>";

		foreach ($rates["results"] as $ratex) {
			echo $ratex["amount_local"] .  " " . $ratex["currency_local"] . " " . $ratex["provider"] .  " " . $ratex["servicelevel_name"] . " " ;
			echo "<br>";
  			
		}

		/*
		// Purchase the desired rate
		$transaction = Shippo_Transaction::create(array('rate'=> $rate["object_id"]));

		// Wait for transaction to be proccessed
		$transactionStartTime=time();
		while (($transaction["object_status"] == "QUEUED" || $transaction["object_status"] == "WAITING")){
		    $transaction = Shippo_Transaction::retrieve($transaction["object_id"]);
		    usleep(200000);  //sleeping 200ms
		    if (time()-$transactionStartTime>25) break;
		    }

		// label_url and tracking_number
		if ($transaction["object_status"] == "SUCCESS"){
		    echo($transaction["label_url"]);
		    echo("\n");
		    echo($transaction["tracking_number"]);
		}else {
		    echo($transaction["messages"]);
		}
		*/

	}

	public function creditcard_payment($encodedOrder_id){
		$this->load->model('order_model');
		$this->load->helper('form');

		// decode the order number	
		$order_ID = $this->order_model->decode_orderID($encodedOrder_id);
		$data = $this->order_model->get_details($order_ID);
		$data["order_items"] = $this->order_model->get_items($order_ID);
		$data["encodedOrder_id"] = $encodedOrder_id;

		// load the view
		if(!empty($data)){
			$this->load->view('templates/header.php');
			$this->load->view('templates/nav.php');
		    $this->load->view('store/creditcard_payment', $data);
		    $this->load->view('templates/footer.php');
	    }else{
			$this->session->set_flashdata('message','ERROR: wrong URL');
			redirect('/store/', 'refresh');
		}
	}

	public function edit_details($encodedOrder_id){
		$this->load->model('order_model');

		if($this->input->post()){
			$this->order_model->update_order($this->input->post());
			
			redirect('/store/thank_you/'.$encodedOrder_id, 'refresh');
		}

		// decode the order number	
		$order_ID = $this->order_model->decode_orderID($encodedOrder_id);
		$data = $this->order_model->get_details($order_ID);
		if(!empty($data)){
			$data["order_items"] = $this->order_model->get_items($order_ID);	

			// load all the countries
			$this->load->model('country_model');

			$this->country_model->order_by("countryName");	
			$data['countries'] = $this->country_model->as_object()->get_all();			

			// load the view
			$this->load->view('templates/header.php');
			$this->load->view('templates/nav.php');
		    $this->load->view('store/edit_details_view', $data);
		    $this->load->view('templates/footer.php');


		}else{
			$this->session->set_flashdata('message','ERROR: wrong URL');
			redirect('/store/', 'refresh');
		}
	}

	public function validateDownload($order_id = 0, $validation_id = 0){

		// validate if order id matches with validation id
		// validate if downloads are left

		// return true or false depending

		$query = $this->db->query("SELECT orders.id FROM orders 
			inner join downloads on orders.id = downloads.order_id 
			WHERE orders.id=" . $order_id . " and orders.encoded='" . $validation_id . "' and downloads_left > 0");

		foreach ($query->result_array() as $row)
		{
			return true;

		}
		return false;
	}

	public function latestFirmware(){
		$version = $this->db->query("select version from firmware_versions WHERE master = 1")->row()->version;
		return $version;
	}

	public function firmwareDownload($order_id = 0, $validation_id = 0){
		$this->load->helper('download');
        
		//Create filename that customer will see
        $file_name = "PozyxFirmware" . $this->latestFirmware() . ".zip";
        
        //Internal location of download
        $download_name = $this->db->query("select filename from firmware_versions WHERE master = 1")->row()->filename;
        $file_path = "./assets/downloads/" . $download_name;
        
        // Validate download 
        if ($this->validateDownload($order_id, $validation_id) && is_file($file_path)){
        	$version = $this->db->query("select id from firmware_versions WHERE master = 1")->row()->id;
        	$this->db->query("UPDATE downloads SET downloads_left = downloads_left -1, version_id = " . $version . ",last_download = CURRENT_TIMESTAMP where order_id = " . $order_id);
        	$data = file_get_contents($file_path); 
        	force_download($file_name, $data); 
       		// update DB
       		
        }
        else{
        	//echo('Error downloading please contact support!');
        	//Should use swal but unclear how to trigger it from within a function
        	echo "<script> alert('Error downloading please contact support'); window.history.back(); </script>";
        }
	}	

}