<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use HelpScoutApp\DynamicApp;

//include 'src/HelpScoutApp/DynamicApp.php';
require_once(APPPATH . "libraries/HelpScoutApp/DynamicApp.php");


class API extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');		
	}

	public function index()
	{
		
	}

	public function make_priority($encodedorder_id){
		$this->load->model('order_model');

		$this->order_model->update_priority($this->order_model->decode_orderID($encodedorder_id), 1);

		echo("The order is now a priority order.");
	}

	/*
	* helpscout() geeft de html die wordt weergeven in helpscout als dynamic App
	*/
	public function helpscout()
	{
		$app = new DynamicApp('keynaarpozyx');
		if ($app->isSignatureValid()) {
	        $customer = $app->getCustomer();
	        $user     = $app->getUser();
	        $convo    = $app->getConversation();
	        $mailbox  = $app->getMailbox();

	        $this->load->model('order_model'); 

	        $firstname = $customer->getFirstName();
	        $lastname = $customer->getLastName();
	        if($firstname == "" && $lastname == "")
	        	$firstname = "jsfdk";

	        $orders = $this->order_model->search_orders_helpscout($customer->getEmail() , $firstname, $lastname);
	        //$orders = $this->order_model->search_orders("vandevelde.samuel@gmail.com" , "customers.email_address", 0, 5);
	        
	        $html = array();

			// create calendar event
			array_push($html, "<div><h4><a href='https://calendar.google.com/calendar/render#eventpage_6'><i class='icon-case'></i> Create a calendar event</a></h4></div>");

	        // add the new task option
			array_push($html, "<div><h4><a href='https://www.pozyx.io/admin/dashboard/task_add'><i class='icon-star'></i> Create a task</a></h4></div>");

			array_push($html, "<div class='toggleGroup open'><h4><a href='#' class='toggleBtn'><i class='icon-cart'></i> Order history (". sizeof($orders) .")</h4></a><div class='toggle indent'>");	
		    if(!empty($orders)){
		    	
		        foreach($orders as $order){					
					array_push($html, "<p><a href='https://www.pozyx.io/store/thank_you/".$this->order_model->encode_orderID($order["orderid"])."'>weborder ".$order["orderid"]."</a> on: ".$order["created_at"]." for ".number_format(($order["grand_total"])/100,2,",",".")."€. Payment by ".$order["payment_method"]." <b>".$order["payment"]."</b>");
					
					if($order["delivery"] == "pending")
					{
						if($order["is_priority"] == 0){
							array_push($html, "<br><a href='https://www.pozyx.io/API/make_priority/".$this->order_model->encode_orderID($order["orderid"])."'>make priority</a></p>");
						}else{
							array_push($html, " <span class='badge orange'>priority</span></p>");
						}
					}else{
						// order has been shipped
						array_push($html, " <a href='http://www.tnt.com/express/en_us/site/home/applications/tracking.html?cons=ORDER%20".$order["orderid"]."&searchType=REF'><span class='badge green'>shipped</span></a></p>");
					}

					array_push($html, "<ul  class='unstyled'>");
					$items = $this->order_model->get_items($order["orderid"]);
					foreach($items as $item){
						array_push($html, "<li>".$item["quantity"] . "x " . $item["productname"]."</li>");
					}
					array_push($html, "</ul>");
		        }   
		    }else{
		    	array_push($html, "<p>No orders found.</p>");
		    }
		    array_push($html, "</div></div>");

			$this->load->model('customer_model');
			$customers = $this->customer_model->get_many_by("email_address", $customer->getEmail());
	        
	        foreach($customers as $customer){		
	        if(!empty($customer)){
	        	array_push($html, "<div class='toggleGroup open'><h4><a href='#' class='toggleBtn'><i class='icon-person'></i> Customer info</a></h4><div class='toggle indent'><ul class='unstyled'>");
		    	array_push($html, "<li>Name: ".$customer->firstname." " .$customer->lastname. "</li>");
		    	
		    	if($customer->company_name != ""){
		    		array_push($html, "<li>Company: ".$customer->company_name."</li>");
		    	}
		    	array_push($html, "<li>Country: ".$customer->country_code."</li>");

		    	array_push($html, "</ul></div></div>");
			}	
			}		


	        echo $app->getResponse($html);
			} else {
			        echo 'Invalid Request';
			}		
	}

	public function post_helpscout_to_slack()
	{

	require_once(APPPATH . 'libraries/shuber_curl/curl.php');
	include_once(APPPATH . 'libraries/helpscout-api-php-master/src/HelpScout/ApiClient.php');

	$this->load->config('credentials');          

	$hs = \HelpScout\ApiClient::getInstance();
	//$this->config->item("helpscout_APIkey")
	$hs->setKey("bf040fa08b1e97f994402a13b5d1afc46d9bf90c");

	/*$report = $hs->getNewConversationsReport([
	  'start' => '2016-01-01T00:00:00Z',
	  'end' => '2016-12-31T23:59:59Z'
	]);

	echo "<pre>";
	  echo print_r($report);
	  echo "</pre>";
	*/

	$mailboxes = $hs->getMailboxes();


	$msg = "";
	if ($mailboxes) {

	  foreach($mailboxes->getItems() as $mailbox){
	    $msg.= "\n";
	    $msg.= $mailbox->getName();
	    $msg.= ": ";        
	    

	    $folder = $mailbox->getUnassignedFolder();
	    $msg.= $folder->getActiveCount();
	    $msg.= "+"; // plus sign
	    //$msg.= $folder->getName();
	    //$msg.= ", ";
	    


	    $folder = $mailbox->getAssignedFolder();
	    $msg.= $folder->getActiveCount();
	    $msg.= " new emails.";
	    //$msg.= $folder->getName();
	    //$msg.= "\n";

	  }  

	  echo $msg;

	  // send a message to slack:

	  $color = "#5F945F";   // green color
	  $slack_msg = "<https://secure.helpscout.net/dashboard|Help Scout> overview.";
	  $attachment = array(
	      'fallback' => $slack_msg,
	      'color' => $color,
	      'fields' => array(
	          array(
	              'title' => 'Daily support overview.',
	              'value' => urlencode($msg),
	              'short' => false
	          )
	      )
	  );
	  $this->_slack_notify($slack_msg, '#customerservice', $attachment);        


	}else{
	  echo("failure");
	}    

	}

	// Send a message to slack on a given slack channel
	private function _slack_notify($msg, $channel, $attachment)
	{
	  $curl = curl_init();
	  $url = 'https://hooks.slack.com/services/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';
	  curl_setopt($curl, CURLOPT_URL, $url);
	  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
	  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	  $payload = array(
	      'channel' => $channel,
	      'username' => 'Pozyx reminder',
	      'text' => $msg
	  );      
	  
	  if ($attachment) {
	      $payload['attachments'] = array($attachment);
	  }
	  $data = 'payload=' . json_encode($payload);
	  curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

	  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);      // only do this on localhost
	  $result = curl_exec($curl);
	  if($result === FALSE) {
	    die(curl_error($curl));
	}
	  curl_close($curl);
	  //echo "\n" . 'message sent to ' . $channel;
	}	
}
