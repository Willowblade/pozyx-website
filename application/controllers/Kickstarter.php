<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kickstarter extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');		
	}

	// controller to show main store page
	public function index(){

	    if($this->input->post())
	    {
	      $this->load->model('order_model');
	      $this->load->library('form_validation');
	      $this->form_validation->set_rules('identity', 'Identity', 'required');
	      if($this->form_validation->run()===TRUE)
	      {
	      	// find email in database	        
	      	$order_id = $this->order_model->get_kickstarter_order($this->input->post('identity'));
	        if ($order_id != -1)	        {
	        	$encodedOrder_id = $this->order_model->encode_orderID($order_id);
	          	redirect('kickstarter/edit_details/'.$encodedOrder_id, 'refresh');
	        }else
	        {
				$this->session->set_flashdata('message','Email address not found. Make sure you are using the same email you use for kickstarter.');
				redirect('kickstarter', 'refresh');
	        }
	      }
	    }

	    $this->load->helper('form');

		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		
		//$this->load->model('order_model');
		//$this->order_model->insert_kickstarter_orders();
		$this->load->view('kickstarter/email_view.php');
		$this->load->view('templates/footer.php');
	}

	public function edit_details($encodedOrder_id){
		$this->load->model('order_model');

		if($this->input->post()){
			$this->order_model->update_order($this->input->post());
			
			redirect('/kickstarter/thank_you/'.$encodedOrder_id, 'refresh');
		}

		// decode the order number	
		$order_ID = $this->order_model->decode_orderID($encodedOrder_id);
		$data = $this->order_model->get_details($order_ID);
		if(!empty($data)){
			$data["order_items"] = $this->order_model->get_items($order_ID);	

			// load all the countries
			$this->load->model('country_model');

			$this->country_model->order_by("countryName");	
			$data['countries'] = $this->country_model->as_object()->get_all();			

			// load the view
			$this->load->view('templates/header.php');
			$this->load->view('templates/nav.php');
		    $this->load->view('kickstarter/edit_details_view', $data);
		    $this->load->view('templates/footer.php');


		}else{
			$this->session->set_flashdata('message','ERROR: wrong URL');
			redirect('/kickstarter/', 'refresh');
		}
	}

	public function thank_you($encodedOrder_id){
		
		$this->load->model('order_model');

		// decode the order number	
		$order_ID = $this->order_model->decode_orderID($encodedOrder_id);
		$data = $this->order_model->get_details($order_ID);
		if(!empty($data)){
			$data["order_items"] = $this->order_model->get_items($order_ID);			

			// load the view
			$this->load->view('templates/header.php');
			$this->load->view('templates/nav.php');
		    $this->load->view('kickstarter/thank_you_view', $data);
		    $this->load->view('templates/footer.php');
		}else{
			$this->session->set_flashdata('message','ERROR: wrong URL');
			redirect('/kickstarter/', 'refresh');
		}
	}

}

