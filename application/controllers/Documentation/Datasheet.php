<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datasheet extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');		
	}

	public function index()
	{
		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/datasheet/doc_datasheet');
		$this->load->view('templates/footer');
	}	

	public function Interfaces(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/datasheet/doc_interfaces.php');
		$this->load->view('templates/footer');
	}

	public function RegisterOverview_old(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/datasheet/doc_registerOverview');
		$this->load->view('templates/footer');
	}	

	public function RegisterOverview_ext(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/datasheet/doc_registerOverview_ext');
		$this->load->view('templates/footer');
	}	

	public function RegisterOverview($version = 'v1.0'){


		$this->load->database();
		$this->load->model('registers_model');
		$data["reg_data"] = $this->registers_model->get_regs_data($version);
		$data["reg_functions"] = $this->registers_model->get_regs_function($version);
		$data["version"] = $version;

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/datasheet/doc_registerOverview_db', $data);
		$this->load->view('templates/footer');
	}	

	public function Registerheader($version = 'v0.9'){
		$this->load->database();
		$this->load->model('registers_model');
		$data["reg_data"] = $this->registers_model->get_regs_data($version);
		$data["reg_functions"] = $this->registers_model->get_regs_function($version);

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/datasheet/doc_registerHeaders', $data);
		$this->load->view('templates/footer');
	}

	public function RegisterEdit($id){

		$this->load->library('form_validation');
		$this->load->library('form_builder');

		$this->load->database();
		$this->load->model('registers_model');
		$data["register"] = $this->registers_model->get_reg($id);

		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/datasheet/doc_registerEdit', $data);
		$this->load->view('templates/footer');

	}

	public function RegisterSave(){		

		$this->load->database();
		$this->load->model('registers_model');
		$post_object = json_decode(json_encode($this->input->post()), FALSE);
		unset($post_object->submit_id);
		$data["register"] = $this->registers_model->update_reg($post_object);

		redirect('Documentation/Datasheet/RegisterOverview_db/'.$post_object->version.'#'.$post_object->reg_name, 'refresh');
	}

	public function RegisterDescription(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/datasheet/doc_registerDescription');
		$this->load->view('templates/footer');
	}	

	public function FunctionalDescription(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/datasheet/doc_functionalDescription');
		$this->load->view('templates/footer');
	}	

	public function SystemDescription(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/datasheet/doc_systemDescription');
		$this->load->view('templates/footer');
	}	

	public function PinOut(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/datasheet/doc_pinOut');
		$this->load->view('templates/footer');
	}

	public function Arduino($ref = ""){

		if($ref == "")
		{
			$this->load->model('arduinoLib_model');
			$data["sensor_data"] =  $this->arduinoLib_model->get_group_members("sensor__data");
			$data["core_members"] =  $this->arduinoLib_model->get_group_members("core");
			$data["positioning_functions"] =  $this->arduinoLib_model->get_group_members("positioning__functions");
			$data["configuration_functions"] =  $this->arduinoLib_model->get_group_members("configuration__functions");
			$data["system_functions"] =  $this->arduinoLib_model->get_group_members("system__functions");
			$data["communication_functions"] =  $this->arduinoLib_model->get_group_members("communication__functions");
			$data["device_list"] =  $this->arduinoLib_model->get_group_members("device__list");
			$data["num"] = 0;

			$alldata = $this->arduinoLib_model->get_all_xml_data();
			$data["structs"] = $alldata["structs"];

			// load the view
			$this->load->view('templates/header.php');
			$this->load->view('templates/nav.php');
			$this->load->view('docs/datasheet/doc_arduino', $data);
			//$this->load->view('docs/datasheet/arduino_doc/class_pozyx_class.html');
			$this->load->view('templates/footer');
		}else{
			$this->load->model('arduinoLib_model');
			$data["struct_data"] =  $this->arduinoLib_model->get_struct_members($ref);
			$data["num"] = 0;

			// load the view
			$this->load->view('templates/header.php');
			$this->load->view('templates/nav.php');
			$this->load->view('docs/datasheet/doc_arduino_struct', $data);
			//$this->load->view('docs/datasheet/arduino_doc/class_pozyx_class.html');
			$this->load->view('templates/footer');
		}
	}

	public function versionHistory(){
		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/datasheet/doc_versionHistory');
		$this->load->view('templates/footer');	
	}
}
