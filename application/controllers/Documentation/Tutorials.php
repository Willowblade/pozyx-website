<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tutorials extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
	}

	// General tutorials
	public function getting_started($type = "Arduino"){
		$data['type'] = $type;
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tutorials/general/tut_getting_started', $data);
		$this->load->view('templates/footer');
	}
  // Tutorials
	public function ready_to_range($type = "Arduino"){
		$data['type'] = $type;
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tutorials/general/tut_ready_to_range', $data);
		$this->load->view('templates/footer');
	}

	public function ready_to_localize($type = "Arduino"){
		$data['type'] = $type;
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tutorials/general/tut_ready_to_localize', $data);
		$this->load->view('templates/footer');
	}

	public function orientation_3D($type = "Arduino"){
		$data['type'] = $type;
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tutorials/general/tut_orientation_3D', $data);
		$this->load->view('templates/footer');
	}

	public function multitag_positioning($type = "Arduino"){
		$data['type'] = $type;
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tutorials/general/tut_multitag_positioning', $data);
		$this->load->view('templates/footer');
	}

	// Further reading
	public function troubleshoot_basics($type = "Arduino"){
		$data['type'] = $type;
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tutorials/general/tut_basics', $data);
		$this->load->view('templates/footer');
	}

	public function uwb_settings($type = "Arduino"){
		$data['type'] = $type;
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tutorials/general/tut_uwb_settings', $data);
		$this->load->view('templates/footer');
	}

	public function anchor_configuration($type = "Arduino"){
		$data['type'] = $type;
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tutorials/general/tut_anchor_configuration', $data);
		$this->load->view('templates/footer');
	}

	public function longer_use($type = "Arduino"){
		$data['type'] = $type;
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tutorials/general/tut_longer_use', $data);
		$this->load->view('templates/footer');
	}

	public function changing_id($type = "Arduino"){
		$data['type'] = $type;
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tutorials/general/tut_changing_id', $data);
		$this->load->view('templates/footer');
	}
  // Arduino specific
	public function chat_room(){
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tutorials/arduino/tut_chat_room');
		$this->load->view('templates/footer');
	}

	// Python specific
	public function ros_pozyx(){
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tutorials/python/tut_ros_pozyx');
		$this->load->view('templates/footer');
	}

	public function combining_functionality(){
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tutorials/python/tut_combining_functionality');
		$this->load->view('templates/footer');
	}

	// Use-cases
	public function adidas(){
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/usecases/usecase_adidas');
		$this->load->view('templates/footer');
	}

  // Function templates
	public function general_template($type = "Arduino"){
		$data['type'] = $type;
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tutorials/tut_general_template', $data);
		$this->load->view('templates/footer');
	}

	public function specific_template(){
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tutorials/tut_specific_template');
		$this->load->view('templates/footer');
	}


}
