<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documentation extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');		
	}

	public function index()
	{
		// load the FAQ
		$this->load->database();
		$this->load->model('faq_model');
	    $data['faq'] = $this->faq_model->as_object()->get_all();



		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/faq');
		$this->load->view('docs/tutorials', $data);
		$this->load->view('templates/footer');
	}

	public function search(){

		$keywords = $this->input->post('search');

		$bGoogleSearch = true;

		// save the keywords for later data analysis
		$this->load->model('keywords_model');

		$countryCode = "";
		try{
			if (ENVIRONMENT == 'production')
			{
				require_once "Net/GeoIP.php";
				$geoip = Net_GeoIP::getInstance("/home/pozydhjv/php/Net/GeoIP.dat");
				$countryCode = $geoip->lookupCountryCode( $_SERVER['REMOTE_ADDR'] );								
			}			
		}catch(Exception $e) {
			
		}	

		if(isset($keywords) && $keywords != ""){
			$this->keywords_model->insert(array("ip_address"=>$_SERVER['REMOTE_ADDR'], "country_code"=>$countryCode, "keywords"=> $this->input->post('search'), 'date_search'=>date('Y-m-d H:i:s',now()) ));
		}

		if($bGoogleSearch){
			$data['keywords'] = $keywords;

			// Gebruikt de google Custom Search engine API
			// https://developers.google.com/apis-explorer/?hl=en_US#p/customsearch/v1/search.cse.list?q=test&cx=007005980111498682071%253A_i5morzetki&_h=8&
			// Max 100 queries per dag
			// https://console.developers.google.com/apis/api/customsearch/usage?project=carbon-segment-107811&duration=PT6H
			// manage the cse 
			// https://cse.google.com/cse/manage/all

			$this->load->view('templates/header.php');
			$this->load->view('templates/nav.php');
			$this->load->view('docs/doc_google_search_results', $data);
			//$this->load->view('templates/footer');
		}
		else
		{
			if($keywords){
				$this->load->library('form_validation');
				$this->form_validation->set_rules('search', 'Search', 'required|trim');
				if($this->form_validation->run()===TRUE)
				{
					$data['keywords'] = $keywords;

					$this->load->database();
					$this->load->model('faq_model');
					$data['query'] = $this->faq_model->get_search($keywords);

					// load the view
					$this->load->view('templates/header.php');
					$this->load->view('templates/nav.php');
					$this->load->view('docs/doc_search_results', $data);
					$this->load->view('templates/footer');
				}else{
					echo('error');
					//redirect('documentation', 'refresh');
				}
			}	
		}

	}


	/*
	public function i2cProtocol(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/datasheet/doc_i2cProtocol');
		$this->load->view('templates/footer');
	}

	public function RegisterOverview(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/datasheet/doc_registerOverview');
		$this->load->view('templates/footer');
	}

	public function Datasheet(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/datasheet/doc_datasheet');
		$this->load->view('templates/footer');
	}*/

	public function doc_howDoesUwbWork(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/doc_howDoesUwbWork');
		$this->load->view('templates/footer');
	}

	public function doc_howDoesPositioningWork(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/doc_howDoesPositioningWork');
		$this->load->view('templates/footer');
	}

	public function doc_whereToPlaceTheAnchors(){

		// load the view
		$this->load->view('templates/header.php');
		$this->load->view('templates/nav.php');
		$this->load->view('docs/doc_whereToPlaceTheAnchors');
		$this->load->view('templates/footer');
	}

	public function doc_cookieLegal()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/cookie_legal_view');
		$this->load->view('templates/footer');
	}

	public function tut_gettingStarted(){
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('docs/tut_gettingStarted');
		$this->load->view('templates/footer');
	}
}
