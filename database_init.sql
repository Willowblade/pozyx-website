-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Aug 01, 2016 at 08:25 AM
-- Server version: 5.5.49-cll-lve
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pozydhjv_pozyx`
--

-- --------------------------------------------------------

--
-- Table structure for table `baskets`
--

CREATE TABLE IF NOT EXISTS `baskets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=606 ;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `countryCode` char(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `countryName` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `vat_code` enum('DOM','EU','NEU') NOT NULL DEFAULT 'NEU',
  PRIMARY KEY (`id`),
  UNIQUE KEY `countryCode` (`countryCode`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=251 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `countryCode`, `countryName`, `vat_code`) VALUES
(1, 'AD', 'Andorra', 'NEU'),
(2, 'AE', 'United Arab Emirates', 'NEU'),
(3, 'AF', 'Afghanistan', 'NEU'),
(4, 'AG', 'Antigua and Barbuda', 'NEU'),
(5, 'AI', 'Anguilla', 'NEU'),
(6, 'AL', 'Albania', 'NEU'),
(7, 'AM', 'Armenia', 'NEU'),
(8, 'AO', 'Angola', 'NEU'),
(9, 'AQ', 'Antarctica', 'NEU'),
(10, 'AR', 'Argentina', 'NEU'),
(11, 'AS', 'American Samoa', 'NEU'),
(12, 'AT', 'Austria', 'EU'),
(13, 'AU', 'Australia', 'NEU'),
(14, 'AW', 'Aruba', 'NEU'),
(15, 'AX', 'Åland', 'NEU'),
(16, 'AZ', 'Azerbaijan', 'NEU'),
(17, 'BA', 'Bosnia and Herzegovina', 'NEU'),
(18, 'BB', 'Barbados', 'NEU'),
(19, 'BD', 'Bangladesh', 'NEU'),
(20, 'BE', 'Belgium', 'DOM'),
(21, 'BF', 'Burkina Faso', 'NEU'),
(22, 'BG', 'Bulgaria', 'EU'),
(23, 'BH', 'Bahrain', 'NEU'),
(24, 'BI', 'Burundi', 'NEU'),
(25, 'BJ', 'Benin', 'NEU'),
(26, 'BL', 'Saint Barthélemy', 'NEU'),
(27, 'BM', 'Bermuda', 'NEU'),
(28, 'BN', 'Brunei', 'NEU'),
(29, 'BO', 'Bolivia', 'NEU'),
(30, 'BQ', 'Bonaire', 'NEU'),
(31, 'BR', 'Brazil', 'NEU'),
(32, 'BS', 'Bahamas', 'NEU'),
(33, 'BT', 'Bhutan', 'NEU'),
(34, 'BV', 'Bouvet Island', 'NEU'),
(35, 'BW', 'Botswana', 'NEU'),
(36, 'BY', 'Belarus', 'NEU'),
(37, 'BZ', 'Belize', 'NEU'),
(38, 'CA', 'Canada', 'NEU'),
(39, 'CC', 'Cocos [Keeling] Islands', 'NEU'),
(40, 'CD', 'Democratic Republic of the Congo', 'NEU'),
(41, 'CF', 'Central African Republic', 'NEU'),
(42, 'CG', 'Republic of the Congo', 'NEU'),
(43, 'CH', 'Switzerland', 'NEU'),
(44, 'CI', 'Ivory Coast', 'NEU'),
(45, 'CK', 'Cook Islands', 'NEU'),
(46, 'CL', 'Chile', 'NEU'),
(47, 'CM', 'Cameroon', 'NEU'),
(48, 'CN', 'China', 'NEU'),
(49, 'CO', 'Colombia', 'NEU'),
(50, 'CR', 'Costa Rica', 'NEU'),
(51, 'CU', 'Cuba', 'NEU'),
(52, 'CV', 'Cape Verde', 'NEU'),
(53, 'CW', 'Curacao', 'NEU'),
(54, 'CX', 'Christmas Island', 'NEU'),
(55, 'CY', 'Cyprus', 'EU'),
(56, 'CZ', 'Czech Republic', 'EU'),
(57, 'DE', 'Germany', 'EU'),
(58, 'DJ', 'Djibouti', 'NEU'),
(59, 'DK', 'Denmark', 'EU'),
(60, 'DM', 'Dominica', 'NEU'),
(61, 'DO', 'Dominican Republic', 'NEU'),
(62, 'DZ', 'Algeria', 'NEU'),
(63, 'EC', 'Ecuador', 'NEU'),
(64, 'EE', 'Estonia', 'EU'),
(65, 'EG', 'Egypt', 'NEU'),
(66, 'EH', 'Western Sahara', 'NEU'),
(67, 'ER', 'Eritrea', 'NEU'),
(68, 'ES', 'Spain', 'EU'),
(69, 'ET', 'Ethiopia', 'NEU'),
(70, 'FI', 'Finland', 'EU'),
(71, 'FJ', 'Fiji', 'NEU'),
(72, 'FK', 'Falkland Islands', 'NEU'),
(73, 'FM', 'Micronesia', 'NEU'),
(74, 'FO', 'Faroe Islands', 'NEU'),
(75, 'FR', 'France', 'EU'),
(76, 'GA', 'Gabon', 'NEU'),
(77, 'GB', 'United Kingdom', 'EU'),
(78, 'GD', 'Grenada', 'NEU'),
(79, 'GE', 'Georgia', 'NEU'),
(80, 'GF', 'French Guiana', 'NEU'),
(81, 'GG', 'Guernsey', 'NEU'),
(82, 'GH', 'Ghana', 'NEU'),
(83, 'GI', 'Gibraltar', 'NEU'),
(84, 'GL', 'Greenland', 'NEU'),
(85, 'GM', 'Gambia', 'NEU'),
(86, 'GN', 'Guinea', 'NEU'),
(87, 'GP', 'Guadeloupe', 'NEU'),
(88, 'GQ', 'Equatorial Guinea', 'NEU'),
(89, 'GR', 'Greece', 'EU'),
(90, 'GS', 'South Georgia and the South Sandwich Islands', 'NEU'),
(91, 'GT', 'Guatemala', 'NEU'),
(92, 'GU', 'Guam', 'NEU'),
(93, 'GW', 'Guinea-Bissau', 'NEU'),
(94, 'GY', 'Guyana', 'NEU'),
(95, 'HK', 'Hong Kong', 'NEU'),
(96, 'HM', 'Heard Island and McDonald Islands', 'NEU'),
(97, 'HN', 'Honduras', 'NEU'),
(98, 'HR', 'Croatia', 'EU'),
(99, 'HT', 'Haiti', 'NEU'),
(100, 'HU', 'Hungary', 'EU'),
(101, 'ID', 'Indonesia', 'NEU'),
(102, 'IE', 'Ireland', 'EU'),
(103, 'IL', 'Israel', 'NEU'),
(104, 'IM', 'Isle of Man', 'NEU'),
(105, 'IN', 'India', 'NEU'),
(106, 'IO', 'British Indian Ocean Territory', 'NEU'),
(107, 'IQ', 'Iraq', 'NEU'),
(108, 'IR', 'Iran', 'NEU'),
(109, 'IS', 'Iceland', 'NEU'),
(110, 'IT', 'Italy', 'EU'),
(111, 'JE', 'Jersey', 'NEU'),
(112, 'JM', 'Jamaica', 'NEU'),
(113, 'JO', 'Jordan', 'NEU'),
(114, 'JP', 'Japan', 'NEU'),
(115, 'KE', 'Kenya', 'NEU'),
(116, 'KG', 'Kyrgyzstan', 'NEU'),
(117, 'KH', 'Cambodia', 'NEU'),
(118, 'KI', 'Kiribati', 'NEU'),
(119, 'KM', 'Comoros', 'NEU'),
(120, 'KN', 'Saint Kitts and Nevis', 'NEU'),
(121, 'KP', 'North Korea', 'NEU'),
(122, 'KR', 'South Korea', 'NEU'),
(123, 'KW', 'Kuwait', 'NEU'),
(124, 'KY', 'Cayman Islands', 'NEU'),
(125, 'KZ', 'Kazakhstan', 'NEU'),
(126, 'LA', 'Laos', 'NEU'),
(127, 'LB', 'Lebanon', 'NEU'),
(128, 'LC', 'Saint Lucia', 'NEU'),
(129, 'LI', 'Liechtenstein', 'NEU'),
(130, 'LK', 'Sri Lanka', 'NEU'),
(131, 'LR', 'Liberia', 'NEU'),
(132, 'LS', 'Lesotho', 'NEU'),
(133, 'LT', 'Lithuania', 'EU'),
(134, 'LU', 'Luxembourg', 'EU'),
(135, 'LV', 'Latvia', 'EU'),
(136, 'LY', 'Libya', 'NEU'),
(137, 'MA', 'Morocco', 'NEU'),
(138, 'MC', 'Monaco', 'NEU'),
(139, 'MD', 'Moldova', 'NEU'),
(140, 'ME', 'Montenegro', 'NEU'),
(141, 'MF', 'Saint Martin', 'NEU'),
(142, 'MG', 'Madagascar', 'NEU'),
(143, 'MH', 'Marshall Islands', 'NEU'),
(144, 'MK', 'Macedonia', 'NEU'),
(145, 'ML', 'Mali', 'NEU'),
(146, 'MM', 'Myanmar [Burma]', 'NEU'),
(147, 'MN', 'Mongolia', 'NEU'),
(148, 'MO', 'Macao', 'NEU'),
(149, 'MP', 'Northern Mariana Islands', 'NEU'),
(150, 'MQ', 'Martinique', 'NEU'),
(151, 'MR', 'Mauritania', 'NEU'),
(152, 'MS', 'Montserrat', 'NEU'),
(153, 'MT', 'Malta', 'EU'),
(154, 'MU', 'Mauritius', 'NEU'),
(155, 'MV', 'Maldives', 'NEU'),
(156, 'MW', 'Malawi', 'NEU'),
(157, 'MX', 'Mexico', 'NEU'),
(158, 'MY', 'Malaysia', 'NEU'),
(159, 'MZ', 'Mozambique', 'NEU'),
(160, 'NA', 'Namibia', 'NEU'),
(161, 'NC', 'New Caledonia', 'NEU'),
(162, 'NE', 'Niger', 'NEU'),
(163, 'NF', 'Norfolk Island', 'NEU'),
(164, 'NG', 'Nigeria', 'NEU'),
(165, 'NI', 'Nicaragua', 'NEU'),
(166, 'NL', 'Netherlands', 'EU'),
(167, 'NO', 'Norway', 'NEU'),
(168, 'NP', 'Nepal', 'NEU'),
(169, 'NR', 'Nauru', 'NEU'),
(170, 'NU', 'Niue', 'NEU'),
(171, 'NZ', 'New Zealand', 'NEU'),
(172, 'OM', 'Oman', 'NEU'),
(173, 'PA', 'Panama', 'NEU'),
(174, 'PE', 'Peru', 'NEU'),
(175, 'PF', 'French Polynesia', 'NEU'),
(176, 'PG', 'Papua New Guinea', 'NEU'),
(177, 'PH', 'Philippines', 'NEU'),
(178, 'PK', 'Pakistan', 'NEU'),
(179, 'PL', 'Poland', 'EU'),
(180, 'PM', 'Saint Pierre and Miquelon', 'NEU'),
(181, 'PN', 'Pitcairn Islands', 'NEU'),
(182, 'PR', 'Puerto Rico', 'NEU'),
(183, 'PS', 'Palestine', 'NEU'),
(184, 'PT', 'Portugal', 'EU'),
(185, 'PW', 'Palau', 'NEU'),
(186, 'PY', 'Paraguay', 'NEU'),
(187, 'QA', 'Qatar', 'NEU'),
(188, 'RE', 'Réunion', 'NEU'),
(189, 'RO', 'Romania', 'EU'),
(190, 'RS', 'Serbia', 'NEU'),
(191, 'RU', 'Russia', 'NEU'),
(192, 'RW', 'Rwanda', 'NEU'),
(193, 'SA', 'Saudi Arabia', 'NEU'),
(194, 'SB', 'Solomon Islands', 'NEU'),
(195, 'SC', 'Seychelles', 'NEU'),
(196, 'SD', 'Sudan', 'NEU'),
(197, 'SE', 'Sweden', 'EU'),
(198, 'SG', 'Singapore', 'NEU'),
(199, 'SH', 'Saint Helena', 'NEU'),
(200, 'SI', 'Slovenia', 'EU'),
(201, 'SJ', 'Svalbard and Jan Mayen', 'NEU'),
(202, 'SK', 'Slovakia', 'EU'),
(203, 'SL', 'Sierra Leone', 'NEU'),
(204, 'SM', 'San Marino', 'NEU'),
(205, 'SN', 'Senegal', 'NEU'),
(206, 'SO', 'Somalia', 'NEU'),
(207, 'SR', 'Suriname', 'NEU'),
(208, 'SS', 'South Sudan', 'NEU'),
(209, 'ST', 'São Tomé and Príncipe', 'NEU'),
(210, 'SV', 'El Salvador', 'NEU'),
(211, 'SX', 'Sint Maarten', 'NEU'),
(212, 'SY', 'Syria', 'NEU'),
(213, 'SZ', 'Swaziland', 'NEU'),
(214, 'TC', 'Turks and Caicos Islands', 'NEU'),
(215, 'TD', 'Chad', 'NEU'),
(216, 'TF', 'French Southern Territories', 'NEU'),
(217, 'TG', 'Togo', 'NEU'),
(218, 'TH', 'Thailand', 'NEU'),
(219, 'TJ', 'Tajikistan', 'NEU'),
(220, 'TK', 'Tokelau', 'NEU'),
(221, 'TL', 'East Timor', 'NEU'),
(222, 'TM', 'Turkmenistan', 'NEU'),
(223, 'TN', 'Tunisia', 'NEU'),
(224, 'TO', 'Tonga', 'NEU'),
(225, 'TR', 'Turkey', 'NEU'),
(226, 'TT', 'Trinidad and Tobago', 'NEU'),
(227, 'TV', 'Tuvalu', 'NEU'),
(228, 'TW', 'Taiwan', 'NEU'),
(229, 'TZ', 'Tanzania', 'NEU'),
(230, 'UA', 'Ukraine', 'NEU'),
(231, 'UG', 'Uganda', 'NEU'),
(232, 'UM', 'U.S. Minor Outlying Islands', 'NEU'),
(233, 'US', 'United States', 'NEU'),
(234, 'UY', 'Uruguay', 'NEU'),
(235, 'UZ', 'Uzbekistan', 'NEU'),
(236, 'VA', 'Vatican City', 'NEU'),
(237, 'VC', 'Saint Vincent and the Grenadines', 'NEU'),
(238, 'VE', 'Venezuela', 'NEU'),
(239, 'VG', 'British Virgin Islands', 'NEU'),
(240, 'VI', 'U.S. Virgin Islands', 'NEU'),
(241, 'VN', 'Vietnam', 'NEU'),
(242, 'VU', 'Vanuatu', 'NEU'),
(243, 'WF', 'Wallis and Futuna', 'NEU'),
(244, 'WS', 'Samoa', 'NEU'),
(245, 'XK', 'Kosovo', 'NEU'),
(246, 'YE', 'Yemen', 'NEU'),
(247, 'YT', 'Mayotte', 'NEU'),
(248, 'ZA', 'South Africa', 'NEU'),
(249, 'ZM', 'Zambia', 'NEU'),
(250, 'ZW', 'Zimbabwe', 'NEU');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `address_line1` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address_line2` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `country_code` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `email_address` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `visa_token` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `company_vat` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `billing_address_line1` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `billing_address_line2` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `billing_state` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `billing_city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `billing_zip` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `billing_equals_shipping` int(11) NOT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=566 ;


CREATE TABLE IF NOT EXISTS `faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(500) NOT NULL,
  `answer` text NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `question` (`question`),
  FULLTEXT KEY `answer` (`answer`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `question`, `answer`, `date_created`) VALUES
(1, 'What is pozyx?', 'Pozyx is a hardware solution that provides accurate positioning and motion information. It can be used as an Arduino shield.  ', '2015-08-03 16:07:22'),
(2, 'Does it work inside and outside?', 'Yes. The pozyx system uses ultra-wideband technology for positioning. This wireless technology works both indoor and outdoor.    ', '2015-08-03 16:07:22'),
(3, 'How many modules do I need for localization?', 'For 3D localization you need at least 5 modules. One module for which you want to know the position and 4 modules that act as anchors.', '2015-08-03 16:07:22'),
(4, 'What are anchors?', 'Anchors are modules with a fixed and known position. For 3D positioning you need at least 4 anchors. The anchors have the same role as satellites in GPS positioning. Note that, with our automatic anchor calibration feature, you do not need to acquire the position of the anchor yourself, it is done automatically for you.', '2015-08-03 16:07:22'),
(5, 'What is the range of the system?', 'The maximum range of the ultra-wideband signals is 100m in clear line-of-sight (LOS). In indoor environments this will be less due to obstructions such as walls. Our test have shown that the signal can usually penetrate 1 or 2 thick concrete walls.', '2015-08-03 16:07:22'),
(6, 'Does the system interfere with wifi or bluetooth or any other RF system?', 'No. The ultra-wideband technology transmits very short pulses using one of the 6 RF bands between 3.5GHz to 6.5GHz. Because of the very large bandwidth the transmit power is strictly regulated and very low. The UWB chip we use is designed to comply with FCC &amp; ETSI UWB spectral masks.', '2015-08-03 16:07:22'),
(7, 'What is the update rate?', 'At the moment the maximum update rate for the position is between 5Hz and 10Hz for a single tag, depending on the positioning algorithm selected. We are still trying hard to optimize the software to get an even higher update rate.', '2015-08-03 16:07:22'),
(8, 'What about multiple tags?', 'If you are looking at the developer''s kit or the bulk order, you may wonder if multi-tag localization is supported. And yes, this is possible. For this, every tag localizes one after the other which is directed by a master tag. The master tag sends out messages to the tags to initiate localization and will gather all the results. The master tag can be connected to a computer to see visually see all positions on a screen. In this scheme, the maximum update rate is divided by the number of tags and all tags must be within range of the master tag.', '2015-08-03 16:07:22'),
(9, 'Can it be made smaller?', 'To ensure Arduino compatibility, the tags are now the same size as an Arduino so they can easily click on an Arduino board. Without the Arduino compatibility, we can probably shrink the tag by a factor of 4 or 5. Perhaps something for the future?', '2015-08-03 16:07:22'),
(10, 'What is the difference between pozyx and an ultrasonic rangefinder?', 'In pozyx, ultra-wideband(uwb) is used to accurately measure the distance to another uwb module (even through objects). An ultrasonic rangefinder will measure the distance to any object in front of it. Also, the rangefinder is very directional: it can only measure in one direction, whereas pozyx is omnidirectional. ', '2015-08-06 00:00:00'),
(11, 'Is it FCC certified?', 'No, the system is not FCC certified. Now, the user is able to change all the settings of the UWB or reprogram the module entirely. Because the certification depends on certain settings (such as the transmit power or delay) it is not possible to certify it.', '2015-08-06 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `foundersclub`
--

CREATE TABLE IF NOT EXISTS `foundersclub` (
  `Backer_Number` int(3) NOT NULL AUTO_INCREMENT,
  `Backer_UID` int(10) NOT NULL,
  `Backer_Name` varchar(100) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Shipping_Country` varchar(2) DEFAULT NULL,
  `Pledge_Amount` varchar(10) DEFAULT NULL,
  `Total` varchar(10) NOT NULL,
  `Pledged_At` varchar(17) NOT NULL,
  `Pledged_Status` varchar(9) NOT NULL,
  `Type` varchar(17) DEFAULT NULL,
  `Notes` varchar(33) DEFAULT NULL,
  `whyPozyx` varchar(450) NOT NULL,
  `hasPicture` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Backer_Number`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=174 ;

--
-- Dumping data for table `foundersclub`
--

INSERT INTO `foundersclub` (`Backer_Number`, `Backer_UID`, `Backer_Name`, `Email`, `Shipping_Country`, `Pledge_Amount`, `Total`, `Pledged_At`, `Pledged_Status`, `Type`, `Notes`, `whyPozyx`, `hasPicture`) VALUES
(1, 831595826, 'Joris Van Kerrebrouck', 'jorisvankerrebrouck@gmail.com', 'BE', '€ 179.00', '€ 179.00', '2015/06/01, 07:40', 'collected', 'Ready to Range', NULL, '', 0),
(2, 1083843142, 'Haolin Li', 'haolin198951@gmail.com', NULL, '€ 50.00', '€ 60.00', '2015/06/01, 07:47', 'collected', 'Video', NULL, 'Pozyx accurate positioning making your life more accurate!', 0),
(3, 1275994452, 'cubergreen', 'nagamendo@gmx.ch', NULL, NULL, '€ 1.00', '2015/06/01, 08:19', 'collected', 'Name', NULL, '', 0),
(5, 857895592, 'Samuel Van de Velde', 'supersammeken@hotmail.com', 'BE', '€ 399.00', '€ 399.00', '2015/06/01, 08:54', 'collected', 'Ready to Localize', NULL, '', 0),
(6, 63378502, 'Derek', 'derek_verleye@hotmail.com', NULL, '€ 50.00', '€ 50.00', '2015/06/01, 11:02', 'collected', 'Video', NULL, 'Being a hardcore golf player along with my main man Koene, I''m getting annoyed by ducks who fly away with my precious balls and hide them. I excpect my life quality to improve significantly if Pozyx were able to track them down!', 1),
(7, 70654429, 'Wouter Rogiest', 'wouter.rogiest@gmail.com', NULL, '€ 5.00', '€ 5.00', '2015/06/01, 12:10', 'collected', 'Wall', NULL, 'One day a Pozyx cleaning robot will sweep your house!', 1),
(8, 252404179, 'Michael Karliner', 'mike@modern-industry.com', 'GB', '€ 179.00', '€ 194.00', '2015/06/01, 13:28', 'collected', 'Ready to Range', NULL, '', 0),
(9, 1615937286, 'matt howard', 'matterantimatter@gmail.com', 'US', '€ 399.00', '€ 419.00', '2015/06/01, 13:30', 'collected', 'Ready to Localize', NULL, '', 0),
(11, 283728018, 'Shabaz Mohammad', 'shabaz@appscinated.com', NULL, NULL, '€ 1.00', '2015/06/01, 14:07', 'collected', 'Name', NULL, '', 0),
(12, 1830659999, 'Yoni Baert', 'baertyoni@hotmail.com', NULL, '€ 50.00', '€ 50.00', '2015/06/01, 14:34', 'collected', 'Video', NULL, 'Pozyx: facilitating households!', 1),
(13, 2131579392, 'Andrew Morgan', 'andrew@nagrom.org', 'US', '€ 399.00', '€ 419.00', '2015/06/01, 15:11', 'collected', 'Ready to Localize', NULL, '', 0),
(14, 54558556, 'Tim Swan', 'from_kickstarter@timmo.com', 'GB', '€ 399.00', '€ 414.00', '2015/06/01, 15:30', 'collected', 'Ready to Localize', NULL, '', 0),
(16, 652618701, 'Joon Wayn Cheong', 'cjwayn@msn.com', 'AU', '€ 899.00', '€ 919.00', '2015/06/01, 23:44', 'collected', 'Developper pack', NULL, '', 0),
(17, 1044183804, 'Lieven Standaert', 'lievenstandaert@hotmail.com', 'BE', '€ 399.00', '€ 399.00', '2015/06/02, 00:22', 'collected', 'Ready to Localize', NULL, '', 0),
(18, 435276642, 'Richard Thornton', 'richie.thornton@gmail.com', NULL, NULL, '€ 1.00', '2015/06/02, 00:46', 'collected', 'Name', NULL, '', 0),
(19, 570409477, 'John Boreczky', 'johnboreczky@yahoo.com', 'US', '€ 399.00', '€ 419.00', '2015/06/02, 02:12', 'collected', 'Ready to Localize', NULL, '', 0),
(20, 1585167765, 'Leo', 'leo_chanfw@yahoo.com.hk', 'HK', '€ 399.00', '€ 419.00', '2015/06/02, 03:51', 'collected', 'Ready to Localize', NULL, '', 0),
(21, 2145277024, 'Dourdin Stanislas', 'sdourdin@gmail.com', NULL, '€ 5.00', '€ 5.00', '2015/06/02, 06:48', 'collected', 'Wall', NULL, 'We hope Pozyx will help you one day in finding your stuff, animals and persons!', 0),
(22, 2131327554, 'Dimitrie Grigorescu', 'dimi86g@yahoo.com', NULL, '€ 5.00', '€ 5.00', '2015/06/02, 16:59', 'collected', 'Wall', NULL, 'That Pozyx might be the next step in interactive architecture!', 0),
(23, 956489730, 'Dejan Borota', 'dborota90@gmail.com', NULL, '€ 5.00', '€ 5.00', '2015/06/02, 22:28', 'collected', 'Wall', NULL, 'Pozyx will enable autonomous operation of various robots working in a local positioning reference frame!', 0),
(24, 1362513691, 'Majda Azermai', 'majda.azermai@ugent.be', NULL, '€ 50.00', '€ 50.00', '2015/06/03, 09:03', 'collected', 'Video', NULL, '', 0),
(25, 1002531650, 'Vinnie', 'vincent_va_87@hotmail.com', NULL, '€ 5.00', '€ 20.00', '2015/06/03, 09:56', 'collected', 'Wall', NULL, 'We hope Pozyx brings a more comfortable life standard.', 0),
(26, 1365895348, 'Sofoklis Papasofokli', 'sofoklis24@gmail.com', 'CY', '€ 399.00', '€ 414.00', '2015/06/04, 08:32', 'collected', 'Ready to Localize', NULL, '', 0),
(28, 1789007641, 'Heidi Steendam', 'hs@telin.ugent.be', 'BE', '€ 3,499.00', '€ 3,499.00', '2015/06/05, 14:13', 'collected', 'Bulk order', NULL, '', 0),
(29, 433364274, 'olivier', 'olivier+kickstarter@guerriat.be', NULL, '€ 5.00', '€ 5.00', '2015/06/05, 18:42', 'collected', 'Wall', NULL, 'Pozyx will make indoor route planning, way finding (train stations, airports, stores, warehouses,...) and home automation posible!', 0),
(31, 139596804, 'Kazuhito Hyodou', 'kazz@gate01.com', 'JP', '€ 449.00', '€ 469.00', '2015/06/06, 09:40', 'collected', 'Ready to Localize', NULL, '', 0),
(32, 968181204, 'Alper Güngörmüsler', 'alpynaq@gmail.com', NULL, '€ 50.00', '€ 50.00', '2015/06/06, 15:19', 'collected', 'Video', NULL, '', 0),
(33, 1205486110, 'josh levine', 'kickstarter@junk.josh.com', NULL, '€ 5.00', '€ 5.00', '2015/06/07, 01:01', 'collected', 'Wall', NULL, 'With Pozyx local navigation will become more accurate than GPS!', 0),
(34, 744319798, 'RoboBill', 'bill@autocourtdryer.com', 'US', '€ 399.00', '€ 419.00', '2015/06/07, 13:59', 'collected', 'Ready to Localize', NULL, '', 0),
(35, 1410550421, 'Dietmar Henrich', 'drhenrich@outlook.com', 'DE', '€ 449.00', '€ 464.00', '2015/06/07, 16:52', 'collected', 'Ready to Localize', NULL, '', 0),
(36, 1064205812, 'Hans Fleurkens', 'hnsflv@gmail.com', 'NL', '€ 449.00', '€ 464.00', '2015/06/07, 17:52', 'collected', 'Ready to Localize', NULL, '', 0),
(37, 1381179480, 'Karel Van den Berghe', 'kvandenberghe@me.com', 'BE', '€ 449.00', '€ 449.00', '2015/06/07, 20:42', 'collected', 'Ready to Localize', NULL, '', 0),
(38, 1947830023, 'Daito Manabe', 'daito@rhizomatiks.com', 'JP', '€ 899.00', '€ 919.00', '2015/06/07, 23:19', 'collected', 'Developper pack', NULL, '', 0),
(39, 1264811669, 'Toshiyuki Hashimoto', 'aircord@gmail.com', 'JP', '€ 449.00', '€ 469.00', '2015/06/08, 01:11', 'collected', 'Ready to Localize', NULL, '', 0),
(40, 1682875308, 'KAZUMASA HAYASHI', 'k515h@yahoo.co.jp', 'JP', '€ 449.00', '€ 469.00', '2015/06/08, 05:32', 'collected', 'Ready to Localize', NULL, '', 0),
(41, 2082417531, 'Jelle De Greef', 'jelledegreef@hotmail.com', NULL, '€ 50.00', '€ 55.00', '2015/06/08, 08:07', 'collected', 'Video', NULL, 'Pozyx in my future personal life: No more losing things around the house. No more "where did I put those goddamn keys?!". Pozyx in the future world: an essential technology, like WiFi. "How could we ever live without this?!"', 0),
(42, 1218210283, 'Thilo Grundmann', 'thilo.grundmann@gmail.com', NULL, '€ 5.00', '€ 5.00', '2015/06/08, 12:23', 'collected', 'Wall', NULL, 'Pozyx will determine your robots'' position!', 0),
(43, 1697554547, 'Dave Cutler', 'dcutler@dcsconcepts.com', NULL, '€ 50.00', '€ 50.00', '2015/06/08, 22:59', 'collected', 'Video', NULL, 'Pozyx assits in accurate location of those in needs!', 0),
(44, 1717814643, 'Vijay Singh', 'vsingh@art-engineers.com', 'US', '€ 449.00', '€ 469.00', '2015/06/09, 01:24', 'collected', 'Ready to Localize', NULL, '', 0),
(45, 611032228, 'BerkelaarMRT', 'info@BerkelaarMRT.com', 'NL', '€ 449.00', '€ 464.00', '2015/06/09, 09:02', 'collected', 'Ready to Localize', NULL, '', 0),
(46, 1942463413, 'Sembian', 'v.sembian@anabond.com', NULL, '€ 5.00', '€ 10.00', '2015/06/09, 10:17', 'collected', 'Wall', NULL, 'Pozyx will allow robots and rones taking care of entire warehouses!', 0),
(49, 339849615, 'Artists & Engineers', 'office@artistsandengineers.co.uk', NULL, '€ 5.00', '€ 20.00', '2015/06/10, 09:43', 'collected', 'Wall', NULL, 'Thanks to Pozyx you will know where all your cameras are!', 1),
(50, 1852746093, 'John Cole', 'johncole1155@gmail.com', NULL, '€ 50.00', '€ 50.00', '2015/06/10, 20:45', 'collected', 'Video', NULL, 'Pozyx will become a definite leader in knowing where a robot is within a specified space.', 1),
(51, 600012854, 'Stefano Savazzi', 'ssavazzi@gmail.com', 'IT', '€ 179.00', '€ 194.00', '2015/06/11, 19:50', 'collected', 'Ready to Range', NULL, '', 0),
(52, 398883345, 'David Cofer', 'dcofer@mindcreators.com', NULL, '€ 5.00', '€ 5.00', '2015/06/11, 21:20', 'collected', 'Wall', NULL, 'Pozyx will be an inexpensive position system for indoor robots!', 0),
(53, 18900713, 'Mauricio Caceres', 'dr_eme@live.com', NULL, '€ 5.00', '€ 5.00', '2015/06/12, 07:28', 'collected', 'Wall', NULL, 'Seamless accurate positioning provided by Pozyx, will have a significant impact on many applications. Ranging from personal navigation indoors, robotics and SLAM, formation flying of UAVs and many more!', 0),
(55, 697950556, 'alexandre burton', 'kick_burton@artificiel.org', 'CA', '€ 899.00', '€ 919.00', '2015/06/12, 15:10', 'collected', 'Developper pack', 'timing ten laatste 15 October !!!', '', 0),
(56, 1109118784, 'Philippe', 'contact@anomes.com', 'FR', '€ 449.00', '€ 464.00', '2015/06/12, 15:39', 'collected', 'Ready to Localize', NULL, '', 0),
(57, 2050400502, 'Anish Mohammed', 'anish.mohammed@singularityu.org', 'GB', '€ 449.00', '€ 464.00', '2015/06/12, 18:59', 'collected', 'Ready to Localize', NULL, '', 0),
(58, 533727331, 'david barras', 'jaqijak@gmail.com', 'CH', '€ 449.00', '€ 469.00', '2015/06/12, 13:09', 'collected', 'Ready to Localize', NULL, '', 0),
(59, 1904438347, 'Robert Price', 'robert.price.01@gmail.com', 'AU', '€ 449.00', '€ 469.00', '2015/06/13, 00:25', 'collected', 'Ready to Localize', NULL, '', 0),
(60, 1663196650, 'Patrick Mulder', 'mulder.patrick@gmail.com', 'DE', '€ 179.00', '€ 194.00', '2015/06/13, 09:21', 'collected', 'Ready to Range', NULL, '', 0),
(61, 1449569945, 'kicklix', '101551.753@compuserve.com', NULL, '€ 50.00', '€ 50.00', '2015/06/13, 10:01', 'collected', 'Video', NULL, 'Pozyx: Accurate - Android (Helping Bot) positioning ...', 0),
(62, 1987172569, 'Ben Gerenstein', 'gerenb@me.com', 'US', '€ 449.00', '€ 469.00', '2015/06/14, 06:15', 'collected', 'Ready to Localize', NULL, '', 0),
(63, 204993437, 'Michael', 'mkiles81@gmail.com', NULL, NULL, '€ 1.00', '2015/06/14, 14:03', 'collected', 'Name', NULL, '', 0),
(64, 1370300718, 'Stuart Heath', 'stuartheath@gmail.com', NULL, NULL, '€ 10.00', '2015/06/14, 14:49', 'collected', 'Wall', NULL, '', 0),
(65, 884879484, 'Katia Munoz', 'katia.munoz@gmail.com', NULL, '€ 5.00', '€ 5.00', '2015/06/14, 18:49', 'collected', 'Wall', NULL, 'Pozyx can automate activities without breaking jars & glasses, nor spending unnecessary energy to properly function!', 0),
(67, 434946291, 'Joshua Ben Thompson', 'jbthompson.eng@gmail.com', NULL, '€ 5.00', '€ 5.00', '2015/06/15, 07:22', 'collected', 'Wall', NULL, 'Better drone navigation with Pozyx!', 0),
(68, 775037403, 'HungChing.Chao', 'hungchingchao@me.com', NULL, '€ 5.00', '€ 5.00', '2015/06/15, 07:32', 'collected', 'Wall', NULL, '', 0),
(69, 1216099228, 'Frieder Hansen', 'friederhansen@gmx.de', 'DE', '€ 179.00', '€ 194.00', '2015/06/15, 13:31', 'collected', 'Ready to Range', NULL, '', 0),
(70, 98641048, 'Justin Brundage', 'justinbrundage@me.com', 'US', '€ 449.00', '€ 469.00', '2015/06/15, 14:49', 'collected', 'Ready to Localize', NULL, '', 0),
(71, 964182439, 'Bradley Johnson', 'bjohnsonmn@gmail.com', NULL, '€ 5.00', '€ 5.00', '2015/06/15, 16:19', 'collected', 'Wall', NULL, 'Pozyx will enable interior basic measurements for an entire house!', 1),
(73, 1998193479, 'Duerinckx Luc', 'lucduerinckx@gmail.com', 'BE', '€ 899.00', '€ 899.00', '2015/06/16, 07:15', 'collected', 'Developper pack', NULL, '', 0),
(74, 471567685, 'Michael Heeney', 'm.heeney@mdx.ac.uk', 'GB', '€ 899.00', '€ 914.00', '2015/06/16, 09:49', 'collected', 'Developper pack', NULL, '', 0),
(75, 2071089939, 'Jim Wells', 'enginears@fantasonics.com', NULL, '€ 5.00', '€ 5.00', '2015/06/16, 18:17', 'collected', 'Wall', NULL, 'With Pozyx, model railroaders might use it to know where things are in a layout!', 0),
(76, 1538766891, 'Eliasbone', 'elias@alvord.com', 'US', '€ 449.00', '€ 469.00', '2015/06/16, 18:38', 'collected', 'Ready to Localize', NULL, '', 0),
(77, 878645393, 'Jason B. Ernst', 'jernst@uoguelph.ca', NULL, '€ 5.00', '€ 5.00', '2015/06/16, 19:14', 'collected', 'Wall', NULL, 'Pozyx is one of the missing pieces in actually delivering service robotics to people''s homes, hospitals, restaurants, ...', 0),
(79, 1095385619, 'David vitrant', 'david@synteny.us', 'US', '€ 449.00', '€ 469.00', '2015/06/16, 23:31', 'collected', 'Ready to Localize', NULL, '', 0),
(80, 84496339, 'Kuantsai Lee', 'Kuantsai.Lee@gmail.com', 'US', '€ 449.00', '€ 469.00', '2015/06/17, 03:15', 'collected', 'Ready to Localize', NULL, '', 0),
(81, 1681862173, 'Abdullah Binsabaan', 'asabaan@yahoo.com', 'SA', '€ 899.00', '€ 919.00', '2015/06/17, 08:58', 'collected', 'Developper pack', NULL, '', 0),
(82, 2105173569, 'Sven Meisman', 'info@automate-it.be', 'BE', '€ 449.00', '€ 449.00', '2015/06/17, 10:34', 'collected', 'Ready to Localize', NULL, '', 0),
(83, 38717753, 'Wim Bonjean', 'wimb@but.ae', 'AE', '€ 449.00', '€ 469.00', '2015/06/17, 14:11', 'collected', 'Ready to Localize', NULL, '', 0),
(84, 1187002473, 'Jerry Steenhoek', 'jerry.steenhoek@interstates.com', 'US', '€ 899.00', '€ 919.00', '2015/06/17, 18:31', 'collected', 'Developper pack', NULL, '', 0),
(85, 1284485780, 'Vyacheslav Kononenko', 'vyacheslav@kononenko.net', 'US', '€ 899.00', '€ 919.00', '2015/06/17, 19:54', 'collected', 'Developper pack', NULL, '', 0),
(86, 392844056, 'Jeff Schmidt', 'digitalfalcon@rogers.com', NULL, '€ 5.00', '€ 5.00', '2015/06/18, 01:27', 'collected', 'Wall', NULL, 'Pozyx: centimeter accurate indoor robot positioning!', 0),
(87, 1797622979, 'Brad Bond', 'bradbond07@gmail.com', 'AU', '€ 449.00', '€ 469.00', '2015/06/18, 02:35', 'collected', 'Ready to Localize', NULL, '', 0),
(88, 870654296, 'Cyril Russo', 'boite.pour.spam@gmail.Com', 'FR', '€ 179.00', '€ 194.00', '2015/06/18, 10:04', 'collected', 'Ready to Range', NULL, '', 0),
(89, 2061093847, 'Peter', 'dopatmvn@gmail.com', 'CH', '€ 449.00', '€ 469.00', '2015/06/18, 11:25', 'collected', 'Ready to Localize', NULL, '', 0),
(90, 1587018560, 'GAGLION', 'mgaglion@groupeseb.com', 'FR', '€ 449.00', '€ 464.00', '2015/06/18, 12:48', 'collected', 'Ready to Localize', NULL, '', 0),
(91, 458116681, 'Chris Babcock', 'cbabcock@acm.org', 'US', '€ 899.00', '€ 919.00', '2015/06/18, 13:26', 'collected', 'Developper pack', NULL, '', 0),
(92, 503909432, 'zhangshu', 'zhangshu9206@163.com', 'CN', '€ 449.00', '€ 469.00', '2015/06/18, 13:35', 'collected', 'Ready to Localize', NULL, '', 0),
(93, 1473195823, 'Tandy Trower', 'tangosquared@hotmail.com', 'US', '€ 449.00', '€ 469.00', '2015/06/18, 21:43', 'collected', 'Ready to Localize', NULL, '', 0),
(94, 2087805064, 'Alex Hafner', 'alex.hafner@xmode.com', 'GB', '€ 899.00', '€ 914.00', '2015/06/18, 23:35', 'collected', 'Developper pack', NULL, '', 0),
(95, 885774601, 'Thomas van Gulick', 'thomas@partyflock.nl', NULL, '€ 5.00', '€ 5.00', '2015/06/19, 15:27', 'collected', 'Wall', NULL, '', 0),
(96, 487929855, 'Naeem Bari', 'nbari@agilissystems.com', 'US', '€ 449.00', '€ 469.00', '2015/06/19, 16:51', 'collected', 'Ready to Localize', NULL, '', 0),
(97, 1009029910, 'William Aegerter', 'wca@spiretech.com', 'US', '€ 449.00', '€ 469.00', '2015/06/19, 19:17', 'collected', 'Ready to Localize', NULL, '', 0),
(98, 1439674700, 'Kyle Rippe', 'kyle.rippe@gmail.com', 'US', '€ 449.00', '€ 469.00', '2015/06/19, 22:33', 'collected', 'Ready to Localize', NULL, '', 0),
(99, 950378247, 'Derek Yap', 'zangzi@gmail.com', 'US', '€ 449.00', '€ 469.00', '2015/06/19, 23:32', 'collected', 'Ready to Localize', NULL, '', 0),
(101, 1047012204, 'Louis-Philippe Loncke', 'lonckelph@gmail.com', NULL, NULL, '€ 1.00', '2015/06/20, 21:03', 'collected', 'Name', NULL, '', 0),
(102, 1613090495, 'ZigZagZeng', 'zzhxhx@gmail.com', 'US', '€ 449.00', '€ 469.00', '2015/06/20, 22:06', 'collected', 'Ready to Localize', NULL, '', 0),
(103, 1972600231, 'Adam Eisenstadt', 'adam22030@gmail.com', NULL, '€ 5.00', '€ 5.00', '2015/06/21, 02:53', 'collected', 'Wall', NULL, 'Pozyx will be great for makers and prototypers who are looking to easily add localization to their projects. Adding mapping abilities could open up SLAM functionality for Arduino based robots!', 1),
(104, 18871387, 'Richard Johnson', 'rejohnson@ozemail.com.au', 'AU', '€ 899.00', '€ 919.00', '2015/06/21, 04:40', 'collected', 'Developper pack', NULL, '', 0),
(105, 1132898200, 'Dennis Oosterlinck', 'dennis.oosterlinck@esko.com', NULL, '€ 50.00', '€ 50.00', '2015/06/21, 18:06', 'collected', 'Video', NULL, '', 0),
(106, 770252633, 'Kelly McDaniel', 'seasearch101@yahoo.com', NULL, NULL, '€ 1.00', '2015/06/21, 20:20', 'collected', 'Name', NULL, '', 0),
(107, 341365438, 'Hackster', 'adam@hackster.io', NULL, '€ 5.00', '€ 5.00', '2015/06/22, 04:30', 'collected', 'Wall', NULL, '', 0),
(108, 852541907, 'Paul Meissner', 'paul.meissner@tugraz.at', 'AT', '€ 899.00', '€ 914.00', '2015/06/22, 09:14', 'collected', 'Developper pack', NULL, '', 0),
(109, 1518782253, 'Erwin Coumans', 'erwin.coumans@gmail.com', 'US', '€ 449.00', '€ 469.00', '2015/06/22, 20:27', 'collected', 'Ready to Localize', NULL, '', 0),
(110, 1125946130, 'Jos', 'josrijke@xs4all.nl', NULL, '€ 5.00', '€ 5.00', '2015/06/23, 21:21', 'collected', 'Wall', NULL, 'Pozyx as a better alternative to iBeacons!', 0),
(111, 1760095108, 'George Small', 'gsmall@moog.com', 'US', '€ 179.00', '€ 199.00', '2015/06/24, 00:09', 'collected', 'Ready to Range', NULL, '', 0),
(112, 2073784517, 'Stephanie A. Chuah', 'stephaniea.chuah@gmail.com', NULL, NULL, '€ 1.00', '2015/06/24, 03:05', 'collected', 'Name', NULL, '', 0),
(113, 1482438693, 'David Tran', 'mczsnow@yahoo.com', 'US', '€ 449.00', '€ 469.00', '2015/06/24, 03:36', 'collected', 'Ready to Localize', NULL, '', 0),
(114, 319971915, 'Bricom Solutions', 'kicstr@bricomsolutions.net', 'US', '€ 179.00', '€ 199.00', '2015/06/24, 03:38', 'collected', 'Ready to Range', NULL, '', 0),
(115, 1590627382, 'Hilke Heremans', 'hilke.heremans@gmail.com', NULL, '€ 50.00', '€ 50.00', '2015/06/24, 06:30', 'collected', 'Video', NULL, 'With Pozyx you can finally build a super secret device to track your wife inside the house! When she''s up to no good, wreaking havoc all over the place. You will know when she enters your two square foot man cave also known as the crawling basement. Seriously though, just make something cool. Vadim will know what to do :-)', 0),
(116, 2004847304, '3DEVO', 'info@3devo.eu', NULL, '€ 5.00', '€ 5.00', '2015/06/24, 11:29', 'collected', 'Wall', NULL, 'Pozyx is just a very awsome way to improve your vacuum cleaning robot!', 0),
(117, 598526678, 'Slingshot', 'accounts@slingshoteffect.co.uk', NULL, '€ 5.00', '€ 5.00', '2015/06/24, 16:17', 'collected', 'Wall', NULL, '', 0),
(118, 1913873121, 'Mark Hall', 'mhall7@its.jnj.com', 'US', '€ 899.00', '€ 919.00', '2015/06/24, 17:04', 'collected', 'Developper pack', NULL, '', 0),
(119, 825492694, 'Chengpeng Wang', 'chpengwang@gmail.com', 'CN', '€ 899.00', '€ 919.00', '2015/06/24, 18:42', 'collected', 'Developper pack', NULL, '', 0),
(120, 1317513598, 'matthew wall', 'mathew.wall@gmail.com', 'US', '€ 449.00', '€ 469.00', '2015/06/25, 13:58', 'collected', 'Ready to Localize', NULL, '', 0),
(121, 972141613, 'Frank Anjeaux Arobose', 'frank.anjeaux@arobose.com', 'FR', '€ 449.00', '€ 464.00', '2015/06/25, 15:53', 'collected', 'Ready to Localize', NULL, '', 0),
(122, 388658059, 'Tom Cool', 'thraal@gmail.com', NULL, '€ 5.00', '€ 20.00', '2015/06/26, 12:36', 'collected', 'Wall', NULL, '', 0),
(123, 10384975, 'Chris Anderson', 'chris@3drobotics.com', 'US', '€ 449.00', '€ 469.00', '2015/06/26, 14:51', 'collected', 'Ready to Localize', NULL, '', 0),
(124, 1990637636, 'Salem Bin Kenaid', 'binkenaid@hotmail.com', 'AE', '€ 899.00', '€ 919.00', '2015/06/26, 19:10', 'collected', 'Developper pack', NULL, '', 0),
(125, 1119353268, 'Hikmet Gumus', 'gumush@hitstar.org', 'TR', '€ 449.00', '€ 469.00', '2015/06/27, 15:30', 'collected', 'Ready to Localize', NULL, '', 0),
(126, 167313100, 'Sasha S.', 'alexandra.shturma@gmail.com', 'US', '€ 449.00', '€ 469.00', '2015/06/27, 18:18', 'collected', 'Ready to Localize', NULL, '', 0),
(127, 1127069312, 'Eva Pascoe', 'eva@never.com', NULL, '€ 5.00', '€ 20.00', '2015/06/27, 19:17', 'collected', 'Wall', NULL, 'Pozyx can help find your way aroud Barbican in London, Westfield shopping centre, Tate Gallery, Camden Council and many other places where you might get lost. Also, it can be used to create fun stuff for retail clients!', 1),
(128, 1559250481, 'Andrew', 'aufranc@me.com', NULL, '€ 5.00', '€ 5.00', '2015/06/28, 01:03', 'collected', 'Wall', NULL, 'Pozyx will make any game of Hide and Seek super easy to win!!', 0),
(129, 1915609357, 'Ben Trapnell', 'trapnell@aero.und.edu', NULL, '€ 50.00', '€ 50.00', '2015/06/28, 09:07', 'collected', 'Video', NULL, 'In studies of robotic technologies, accurate and automatic positioning information for robotics, including unmanned aircraft, to autonomously move trough any environment where GPS, or other satellite, positioning information is not available. Pozyx will fill this gap. Underground or in buildings, the availability of accurate positioning information can be life-saving!', 1),
(130, 408191488, 'Stefano Piermatteo', 'stefano.piermatteo@gmail.com', NULL, NULL, '€ 50.00', '2015/06/28, 13:49', 'collected', 'Video', NULL, '', 0),
(131, 1544252762, 'Tim', 'tim.debleser@tempora.be', NULL, '€ 5.00', '€ 10.00', '2015/06/28, 21:36', 'collected', 'Wall', NULL, 'Imagine Pozyx in musea, providing postion based info on a vistors device!', 0),
(132, 1927387140, 'Byron Guernsey', 'bguernsey@me.com', 'US', '€ 449.00', '€ 469.00', '2015/06/29, 00:20', 'collected', 'Ready to Localize', NULL, '', 0),
(133, 1169378941, 'Affan', 'affanad@hotmail.com', NULL, '€ 5.00', '€ 5.00', '2015/06/29, 05:39', 'collected', 'Wall', NULL, 'Pozyx will find a pair of socks!', 0),
(134, 1842636600, 'Markus Steiner', 'steinerm@gmail.com', 'CH', '€ 449.00', '€ 469.00', '2015/06/29, 06:39', 'collected', 'Ready to Localize', NULL, '', 0),
(135, 1231717776, 'Carl Jacobs', 'jacobs.carl@gmail.com', NULL, '€ 50.00', '€ 50.00', '2015/06/29, 08:11', 'collected', 'Video', NULL, 'Pozyx will find your way in goverment buildings!', 0),
(136, 476142850, 'Paul Flaherty', 'wypfl9@gmail.com', 'GB', '€ 449.00', '€ 464.00', '2015/06/29, 08:25', 'collected', 'Ready to Localize', NULL, '', 0),
(137, 231776397, 'Adam Young', 'jeep15cba@gmail.com', 'AU', '€ 179.00', '€ 199.00', '2015/06/29, 08:27', 'collected', 'Ready to Range', NULL, '', 0),
(138, 516479955, 'chandra', 'vc_sasc@outlook.com', 'SG', '€ 899.00', '€ 919.00', '2015/06/29, 08:59', 'collected', 'Developper pack', NULL, '', 0),
(139, 1452614891, 'Hermes', 'hermes.frangoudis@gmail.com', 'US', '€ 179.00', '€ 199.00', '2015/06/29, 09:06', 'collected', 'Ready to Range', NULL, '', 0),
(140, 844956077, 'Michal', 'michalsmielak@gmail.com', NULL, '€ 5.00', '€ 5.00', '2015/06/29, 09:07', 'collected', 'Wall', NULL, 'Pozyx could be used for scientific research to precisely track animal movement!', 0),
(141, 2002313919, 'Eric Castro', 'errrick@gmail.com', 'FR', '€ 449.00', '€ 624.00', '2015/06/29, 10:48', 'collected', 'Ready to Localize', '3 tags and 4 anchors', '', 0),
(142, 305613438, 'Hamish Harvey', 'hamish@hamishharvey.com', 'GB', '€ 449.00', '€ 464.00', '2015/06/29, 14:51', 'collected', 'Ready to Localize', NULL, '', 0),
(143, 1292105269, 'Marc Poulain', 'poulainmarc@yahoo.fr', NULL, '€ 5.00', '€ 5.00', '2015/06/29, 19:39', 'collected', 'Wall', NULL, 'Pozyx technology could be used in designed alive tables. It''s not always about where you are, but associating many actions according to locations and movements of persons or objects!', 0),
(144, 1634195630, 'Fredrik Wallander', 'wallerron79@gmail.com', NULL, '€ 5.00', '€ 5.00', '2015/06/29, 23:25', 'collected', 'Wall', NULL, 'Pozyx will bring accurate indoor robotics and/or drone positioning!', 1),
(145, 514951632, 'marco surace', 'marco.surace@gmail.com', 'US', '€ 449.00', '€ 469.00', '2015/06/29, 23:39', 'collected', 'Ready to Localize', NULL, '', 0),
(146, 750126227, 'Aaron McMahon', 'aaronmcmahon1972@hotmail.com', NULL, '€ 5.00', '€ 6.00', '2015/06/29, 23:48', 'collected', 'Wall', NULL, 'Pozyx and robototics!', 0),
(147, 219457420, 'Michio Tamura', 'tamchan@pp.iij4u.or.jp', 'JP', '€ 449.00', '€ 469.00', '2015/06/30, 00:32', 'collected', 'Ready to Localize', NULL, '', 0),
(148, 1990352531, 'Kenichiro Ito', 'code0red@gmail.com', NULL, '€ 5.00', '€ 5.00', '2015/06/30, 01:54', 'collected', 'Wall', NULL, 'Pozyx will enhance my shopping experience!', 0),
(149, 515396291, 'Dieter Deriemaeker', 'deriemaeker_dieter@hotmail.com', NULL, '€ 50.00', '€ 50.00', '2015/06/30, 09:01', 'collected', 'Video', NULL, 'Pozyx will help locating firefighters when they are in a building!', 1),
(150, 942725941, 'Steven Wijgerse', 's.wijgerse@re-lion.com', 'NL', '€ 899.00', '€ 914.00', '2015/06/30, 09:38', 'collected', 'Developper pack', NULL, '', 0),
(151, 398628387, 'Alf Morten Tveiten', 'alfmorten.tveiten@gmail.com', NULL, '€ 5.00', '€ 5.00', '2015/06/30, 11:06', 'collected', 'Wall', NULL, 'Pozyx for tracking your kids and keeping the elderly safe!', 0),
(152, 2033134306, 'Stefanie Hoppe-Fuchs', 'stefanie.hoppe-fuchs@klangerfinder.de', 'DE', '€ 899.00', '€ 914.00', '2015/06/30, 12:20', 'collected', 'Developper pack', NULL, '', 0),
(153, 1761404629, 'Akira Kojima', 'wysnatall@yahoo.co.jp', 'JP', '€ 449.00', '€ 469.00', '2015/06/30, 12:27', 'collected', 'Ready to Localize', NULL, '', 0),
(154, 1550383131, 'Le Kern Lim', 'lekern@innoglass.com.my', 'MY', '€ 899.00', '€ 919.00', '2015/06/30, 14:38', 'collected', 'Developper pack', NULL, '', 0),
(155, 485185897, 'Peter Von Buskirk', 'peter.vonbuskirk@gmail.com', NULL, NULL, '€ 5.00', '2015/06/30, 15:26', 'collected', 'Wall', NULL, '', 0),
(156, 2002950384, 'sam povilus', 'kickstarter@povil.us', 'US', '€ 449.00', '€ 469.00', '2015/06/30, 17:19', 'collected', 'Ready to Localize', NULL, '', 0),
(157, 1122674423, 'Jobst von Heintze', 'info@jobst-von-heintze.de', 'DE', '€ 449.00', '€ 464.00', '2015/06/30, 18:08', 'collected', 'Ready to Localize', NULL, '', 0),
(158, 1685998366, 'Hannes Kruppa', 'hannes.kruppa@here.com', 'DE', '€ 449.00', '€ 464.00', '2015/06/30, 18:59', 'collected', 'Ready to Localize', NULL, '', 0),
(159, 1250980570, 'Chris Ziegler', 'cz@movingimages.de', 'US', '€ 449.00', '€ 469.00', '2015/06/30, 19:53', 'collected', 'Ready to Localize', NULL, '', 0),
(160, 202365665, 'David Ho', 'hokailok@gmail.com', 'HK', '€ 449.00', '€ 469.00', '2015/06/30, 20:27', 'collected', 'Ready to Localize', NULL, '', 0),
(161, 1236644899, 'J. Poston', 'poston@vt.edu', 'US', '€ 449.00', '€ 469.00', '2015/06/30, 20:45', 'collected', 'Ready to Localize', NULL, '', 0),
(162, 1793268706, 'Vic Verheyen', 'vic.verheyen@gmail.com', NULL, '€ 50.00', '€ 50.00', '2015/06/30, 21:04', 'collected', 'Video', NULL, 'With a lot of sympathy I whish the whole Pozyx team all the best, a lot of energy and inspiration to complete the initial goals of this project. As new future challenges I mainly see the upscaling to a more performant micocontroller platform, so that more cpu intensive applications and larger sets of shields can be handled. A further challenge may be to setup the right licensing model. Keep on going Pozyx !!!', 0),
(163, 105489525, 'Jeroen Van Hecke', 'jeroenvhecke@gmail.com', NULL, '€ 5.00', '€ 20.00', '2015/06/30, 21:42', 'collected', 'Wall', NULL, '', 0),
(164, 989739812, 'Edi', 'edi.ruffa@gmail.com', 'IT', '€ 899.00', '€ 914.00', '2015/06/30, 22:40', 'collected', 'Developper pack', NULL, '', 0),
(165, 497570711, 'DT', 'dttworld@aol.com', 'US', '€ 899.00', '€ 919.00', '2015/07/01, 01:47', 'collected', 'Developper pack', NULL, '', 0),
(166, 1006180403, 'Shuichi Yokoi', 'yokoi@yokoisekkei.com', 'JP', '€ 899.00', '€ 919.00', '2015/07/01, 03:04', 'collected', 'Developper pack', NULL, '', 0),
(167, 2016244481, 'shinichi.masakawa', 'masakawa@cande-micro.com', 'JP', '€ 399.00', '€ 419.00', '2015/07/01, 03:34', 'collected', 'Ready to Localize', NULL, '', 0),
(168, 1155455986, 'Vadim Vermeiren', 'vadim.vermeiren@gmail.com', NULL, '€ 50.00', '€ 50.00', '2015/07/01, 04:28', 'collected', 'Video', NULL, 'Finaly get a Pozyx robot that can fetch my triathlon stuff on game day!', 0),
(169, 1964264319, 'George Hope', 'PracticalCode@GMail.com', 'US', '€ 449.00', '€ 469.00', '2015/07/01, 05:23', 'collected', 'Ready to Localize', NULL, '', 0),
(170, 1573170187, 'Serge Ayer', 'Serge.Ayer@hotmail.com', 'CH', '€ 449.00', '€ 469.00', '2015/07/01, 05:37', 'collected', 'Ready to Localize', NULL, '', 0),
(171, 1541521453, 'David Tilley', 'comebacktomorrow@hotmail.com', NULL, '€ 5.00', '€ 5.00', '2015/07/01, 06:00', 'collected', 'Wall', NULL, 'Pozyx will bring a new wind in interactive media applications!', 0),
(172, 1578399115, 'Lanseloet Erbuer', 'lanseloeterbuer@gmail.com', NULL, '€ 50.00', '€ 50.00', '2015/07/01, 06:49', 'collected', 'Video', NULL, 'Go Sam!!!!!!!!!!!', 0),
(173, 1754262488, 'Masashi Honma', 'masashi.honma@gmail.com', 'JP', '€ 449.00', '€ 469.00', '2015/07/01, 07:04', 'collected', 'Ready to Localize', NULL, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'employee', 'employee of pozyx');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `legal_id` varchar(10) NOT NULL DEFAULT '',
  `order_id` int(11) NOT NULL,
  `invoice_type` enum('IN','CN','DUB') NOT NULL DEFAULT 'IN',
  `payment_term` enum('PAID','N15','N30') NOT NULL DEFAULT 'PAID',
  `invoice_date` date NOT NULL,
  `due_date` date NOT NULL,
  `linked_invoice_id` int(11) NOT NULL,
  `invoice_status` enum('send','credited') NOT NULL DEFAULT 'send',
  `invoice_year` year(4) NOT NULL,
  `vat_code` enum('DOM','EU','NEU') NOT NULL DEFAULT 'NEU',
  `vat_type` enum('service','goods') NOT NULL DEFAULT 'goods',
  `legal_idCopy` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `legal_id` (`legal_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=317 ;

--
-- Table structure for table `keywords`
--

CREATE TABLE IF NOT EXISTS `keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `keywords` varchar(100) NOT NULL,
  `date_search` datetime NOT NULL,
  `country_code` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=150 ;

--
-- Dumping data for table `keywords`
--

INSERT INTO `keywords` (`id`, `ip_address`, `keywords`, `date_search`, `country_code`) VALUES
(1, '195.144.75.190', 'test', '2016-03-14 14:47:24', 'BE'),
(2, '195.144.75.190', 'samuel', '2016-03-14 14:48:04', 'BE'),
(3, '91.118.153.53', 'firmware', '2016-03-17 10:53:34', 'AT'),
(4, '80.109.212.222', 'endian', '2016-03-29 19:01:35', 'AT'),
(5, '101.78.244.197', 'doRemotePositioning', '2016-04-01 09:11:35', 'HK'),
(6, '213.224.14.37', 'sensor_data_t', '2016-04-04 04:45:04', 'BE'),
(7, '213.224.14.37', 'linear_acceleration   ', '2016-04-04 05:45:30', 'BE'),
(8, '213.224.14.37', 'sensor_data_t', '2016-04-04 08:20:36', 'BE'),
(9, '213.224.14.37', 'device_range_t', '2016-04-05 04:49:57', 'BE'),
(10, '95.183.156.67', 'firmware', '2016-04-05 07:44:59', 'TR'),
(11, '213.224.14.37', 'device_range_t', '2016-04-06 04:47:13', 'BE'),
(12, '82.130.84.12', 'firmware', '2016-04-06 08:50:37', 'CH'),
(13, '195.176.111.174', 'regRead', '2016-04-06 08:54:22', 'CH'),
(14, '217.27.185.218', 'range', '2016-04-10 03:50:35', 'SE'),
(15, '77.119.131.72', 'getDeviceIds', '2016-04-10 10:02:28', 'AT'),
(16, '84.143.189.20', 'Volt', '2016-04-17 16:45:58', 'DE'),
(17, '84.143.189.20', '5v', '2016-04-17 16:46:07', 'DE'),
(18, '2.50.14.21', 'pozyx libary', '2016-04-25 04:04:31', 'AE'),
(19, '175.125.29.142', 'arduino model', '2016-04-25 04:40:16', 'KR'),
(20, '175.125.29.142', 'arduino', '2016-04-25 04:41:01', 'KR'),
(21, '175.125.29.142', 'POZYX_3D', '2016-04-28 22:14:59', 'KR'),
(22, '175.125.29.142', 'POZYX_3D', '2016-04-28 22:15:45', 'KR'),
(23, '175.125.29.142', 'version', '2016-04-29 01:58:42', 'KR'),
(24, '175.125.29.142', 'doRemotePosi', '2016-05-01 23:08:51', 'KR'),
(25, '175.125.29.142', 'range', '2016-05-06 02:00:44', 'KR'),
(26, '95.252.192.50', 'anchor', '2016-05-06 10:18:20', 'IT'),
(27, '95.252.192.50', 'tag', '2016-05-06 10:18:25', 'IT'),
(28, '134.28.254.10', 'cir', '2016-05-10 04:14:44', 'DE'),
(29, '134.28.254.10', 'channel impulse', '2016-05-10 04:14:49', 'DE'),
(30, '2.2.216.58', 'reset', '2016-05-10 09:08:43', 'FR'),
(31, '109.139.121.254', 'Battery', '2016-05-10 14:07:54', 'BE'),
(32, '109.139.121.254', 'Range', '2016-05-10 14:08:16', 'BE'),
(33, '109.139.121.254', 'Uwb', '2016-05-10 14:08:43', 'BE'),
(34, '81.82.236.39', 'jobs', '2016-05-11 09:16:00', 'BE'),
(35, '81.82.236.39', 'career', '2016-05-11 09:16:09', 'BE'),
(36, '94.143.189.253', 'range', '2016-05-13 05:01:49', 'BE'),
(37, '134.102.26.86', 'Library ', '2016-05-13 08:01:26', 'DE'),
(38, '46.18.101.30', 'Kalman filter', '2016-05-17 03:43:22', 'FR'),
(39, '175.125.29.142', 'spec', '2016-05-19 04:56:31', 'KR'),
(40, '134.102.97.242', 'POZYX_TIMEOUT', '2016-05-19 06:49:20', 'DE'),
(41, '82.204.36.186', 'int1', '2016-05-23 09:58:23', 'NL'),
(42, '172.13.179.76', 'raspberry', '2016-05-23 22:05:44', 'US'),
(43, '172.13.179.76', 'odroid', '2016-05-23 22:05:49', 'US'),
(44, '172.13.179.76', 'linux', '2016-05-23 22:15:10', 'US'),
(45, '149.243.232.3', 'size', '2016-05-24 10:41:01', 'DE'),
(46, '81.169.105.244', 'Test', '2016-05-24 14:05:39', 'BE'),
(47, '94.143.189.253', 'test', '2016-05-25 07:23:42', 'BE'),
(48, '94.143.189.253', 'test', '2016-05-25 07:26:55', 'BE'),
(49, '94.143.189.253', 'testing', '2016-05-25 07:28:08', 'BE'),
(50, '94.143.189.253', 'test', '2016-05-25 08:32:58', 'BE'),
(51, '94.143.189.253', 'test', '2016-05-25 08:34:37', 'BE'),
(52, '94.143.189.253', 'test', '2016-05-25 08:36:24', 'BE'),
(53, '94.143.189.253', 'test', '2016-05-25 08:37:58', 'BE'),
(54, '94.143.189.253', 'test', '2016-05-25 08:44:09', 'BE'),
(55, '94.143.189.253', 'test', '2016-05-25 08:45:54', 'BE'),
(56, '94.143.189.253', 'test', '2016-05-25 08:46:57', 'BE'),
(57, '94.143.189.253', 'test', '2016-05-25 08:53:22', 'BE'),
(58, '94.143.189.253', 'test', '2016-05-25 08:57:34', 'BE'),
(59, '94.143.189.253', 'pozyx', '2016-05-25 09:06:49', 'BE'),
(60, '94.143.189.253', 'pozyx', '2016-05-25 09:16:23', 'BE'),
(61, '94.143.189.253', 'range', '2016-05-25 10:02:42', 'BE'),
(62, '94.143.189.253', 'range', '2016-05-25 10:02:54', 'BE'),
(63, '94.143.189.253', 'test', '2016-05-25 10:03:16', 'BE'),
(64, '94.143.189.253', 'test', '2016-05-25 10:04:58', 'BE'),
(65, '94.143.189.253', 'test', '2016-05-25 10:05:30', 'BE'),
(66, '96.57.147.98', 'labview', '2016-05-25 10:07:32', 'US'),
(67, '94.143.189.253', 'range', '2016-05-25 10:32:27', 'BE'),
(68, '94.143.189.253', 'RF', '2016-05-25 10:35:49', 'BE'),
(69, '134.28.142.161', 'regFunction', '2016-05-25 11:14:06', 'DE'),
(70, '134.28.142.161', 'regFunction', '2016-05-25 11:16:17', 'DE'),
(71, '195.37.187.176', 'register', '2016-05-26 03:20:36', 'DE'),
(72, '46.107.199.32', 'android', '2016-05-27 07:49:30', 'HU'),
(73, '81.247.139.59', '3D', '2016-05-27 11:26:13', 'BE'),
(74, '87.66.161.225', 'arduino due', '2016-05-29 17:51:11', 'BE'),
(75, '87.66.161.225', 'channel impulse', '2016-05-29 17:53:00', 'BE'),
(76, '87.66.161.225', 'linux', '2016-05-29 17:53:07', 'BE'),
(77, '87.66.161.225', 'reset', '2016-05-29 17:53:27', 'BE'),
(78, '81.15.98.186', '802.15', '2016-05-31 08:35:39', 'IS'),
(79, '81.15.98.186', 'IEEE', '2016-05-31 08:35:44', 'IS'),
(80, '81.15.98.186', '802.15', '2016-05-31 08:35:56', 'IS'),
(81, '129.241.10.161', 'power', '2016-06-03 10:16:10', 'NO'),
(82, '129.241.10.161', 'dbm', '2016-06-03 10:16:30', 'NO'),
(83, '129.241.10.161', 'firmware', '2016-06-03 13:48:14', 'NO'),
(84, '188.113.92.132', 'dbm', '2016-06-05 11:23:55', 'NO'),
(85, '118.5.230.194', 'arduino', '2016-06-07 20:23:43', 'JP'),
(86, '59.92.151.71', 'processing', '2016-06-09 12:57:16', 'IN'),
(87, '62.42.153.112', 'Positioning values', '2016-06-09 21:04:40', 'ES'),
(88, '203.197.123.132', 'coordinates', '2016-06-10 00:25:14', 'IN'),
(89, '203.197.123.132', 'processing sketch', '2016-06-10 04:25:26', 'IN'),
(90, '203.197.123.132', 'Processing', '2016-06-10 04:29:15', 'IN'),
(91, '78.53.46.165', 'drone', '2016-06-11 10:16:11', 'DE'),
(92, '78.53.46.165', 'quadcopter', '2016-06-11 10:16:32', 'DE'),
(93, '5.107.40.160', 'remoteregread', '2016-06-13 18:21:17', 'AE'),
(94, '5.107.40.160', 'remoteregread', '2016-06-13 18:22:29', 'AE'),
(95, '203.197.123.132', 'dbm', '2016-06-17 04:16:49', 'IN'),
(96, '175.125.29.142', 'remoteRegFunction', '2016-06-21 03:03:57', 'KR'),
(97, '175.125.29.142', 'regFunction', '2016-06-21 03:05:32', 'KR'),
(98, '175.125.29.142', 'firmware', '2016-06-21 04:06:29', 'KR'),
(99, '129.210.115.242', 'direction', '2016-06-22 19:17:13', 'US'),
(100, '129.210.115.242', 'direction', '2016-06-22 19:19:30', 'US'),
(101, '129.210.115.242', 'direction', '2016-06-22 19:31:26', 'US'),
(102, '67.161.69.16', 'precision', '2016-06-24 06:33:43', 'US'),
(103, '129.210.115.242', 'FAQ', '2016-06-24 17:17:28', 'US'),
(104, '129.27.104.9', 'wall', '2016-06-27 08:44:56', 'AT'),
(105, '209.117.122.226', 'maximum speed', '2016-06-28 09:44:01', 'US'),
(106, '209.117.122.226', 'speed', '2016-06-28 17:05:24', 'US'),
(107, '131.107.174.148', 'least squares', '2016-06-29 12:48:55', 'US'),
(108, '131.107.174.148', 'firmware', '2016-06-29 12:49:20', 'US'),
(109, '131.107.174.148', 'firmware', '2016-06-29 13:09:51', 'US'),
(110, '131.107.174.118', 'dfu', '2016-06-30 12:38:24', 'US'),
(111, '90.176.252.153', 'rss', '2016-07-01 08:50:58', 'CZ'),
(112, '1.39.61.208', 'warrenty', '2016-07-04 02:43:52', 'IN'),
(113, '1.39.61.208', 'warranty', '2016-07-04 02:44:03', 'IN'),
(114, '147.87.243.54', 'tracking', '2016-07-04 05:54:53', 'CH'),
(115, '189.207.198.57', 'raspberry pi', '2016-07-04 23:00:19', 'MX'),
(116, '86.151.49.192', 'battery', '2016-07-05 10:43:08', 'GB'),
(117, '86.151.49.192', 'wireless', '2016-07-05 10:43:21', 'GB'),
(118, '86.151.49.192', 'BAUDRATE', '2016-07-05 11:05:06', 'GB'),
(119, '86.151.49.192', 'baudrate', '2016-07-05 11:05:12', 'GB'),
(120, '86.151.49.192', 'led', '2016-07-05 11:28:11', 'GB'),
(121, '86.151.49.192', 'sensor', '2016-07-05 11:29:48', 'GB'),
(122, '88.196.196.216', 'sqm', '2016-07-07 17:31:43', 'EE'),
(123, '189.207.198.57', 'raspberry pi', '2016-07-08 00:53:52', 'MX'),
(124, '189.207.198.57', 'i2c', '2016-07-08 00:54:01', 'MX'),
(125, '65.51.217.8', 'data output', '2016-07-08 11:48:39', 'US'),
(126, '129.210.115.242', 'comments', '2016-07-08 18:28:49', 'US'),
(127, '80.109.94.12', 'outdoor', '2016-07-09 10:21:30', 'AT'),
(128, '80.109.94.12', 'outdoor', '2016-07-09 10:21:46', 'AT'),
(129, '14.3.81.50', 'dfu', '2016-07-11 20:23:10', 'JP'),
(130, '37.74.226.8', 'firmware', '2016-07-12 10:24:20', 'NL'),
(131, '87.138.200.154', 'automatic anchor position', '2016-07-12 17:14:20', 'DE'),
(132, '85.201.61.113', 'reach', '2016-07-13 10:49:29', 'BE'),
(133, '94.143.189.253', 'least', '2016-07-13 12:52:51', 'BE'),
(134, '95.91.213.255', 'range', '2016-07-15 09:58:26', 'DE'),
(135, '212.61.149.228', 'range', '2016-07-18 04:15:48', 'NL'),
(136, '125.186.189.212', 'raspberry', '2016-07-19 10:21:56', 'KR'),
(137, '125.186.189.212', 'raspberry', '2016-07-19 10:22:04', 'KR'),
(138, '39.121.103.30', 'firmware', '2016-07-20 02:47:21', 'KR'),
(139, '39.121.103.30', 'firmware', '2016-07-20 02:47:34', 'KR'),
(140, '39.121.103.30', '.dfu', '2016-07-20 02:48:38', 'KR'),
(141, '77.58.44.171', 'tracking', '2016-07-20 09:38:25', 'CH'),
(142, '88.13.247.62', 'firmware', '2016-07-22 11:58:29', 'ES'),
(143, '188.188.128.66', 'I2c', '2016-07-23 08:36:36', 'BE'),
(144, '206.117.31.206', 'firmware update', '2016-07-23 16:28:58', 'US'),
(145, '62.4.208.6', 'i2c', '2016-07-24 12:15:04', 'BE'),
(146, '130.79.220.18', 'reset', '2016-07-27 09:25:37', 'FR'),
(147, '94.143.189.253', 'anchor placement', '2016-07-27 09:51:33', 'BE'),
(148, '130.79.220.18', 'power', '2016-07-27 10:18:00', 'FR'),
(149, '89.206.158.107', 'No signal', '2016-07-28 09:55:48', 'GB');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('email') NOT NULL DEFAULT 'email',
  `event` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `orderID` int(11) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=125 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `event`, `date`, `orderID`, `email_address`) VALUES
(4, 'email', 'kickstart survey', '2015-08-29 09:40:32', 5, ''),
(5, 'email', 'kickstart survey', '2015-08-29 09:48:46', 173, ''),
(6, 'email', 'kickstart survey', '2015-08-29 09:48:47', 170, ''),
(7, 'email', 'kickstart survey', '2015-08-29 09:48:47', 169, ''),
(8, 'email', 'kickstart survey', '2015-08-29 09:48:48', 167, ''),
(9, 'email', 'kickstart survey', '2015-08-29 09:48:48', 166, ''),
(10, 'email', 'kickstart survey', '2015-08-29 09:48:49', 165, ''),
(11, 'email', 'kickstart survey', '2015-08-29 09:48:50', 164, ''),
(12, 'email', 'kickstart survey', '2015-08-29 09:48:50', 161, ''),
(13, 'email', 'kickstart survey', '2015-08-29 09:48:51', 160, ''),
(14, 'email', 'kickstart survey', '2015-08-29 09:48:52', 159, ''),
(15, 'email', 'kickstart survey', '2015-08-29 09:48:57', 158, ''),
(16, 'email', 'kickstart survey', '2015-08-29 09:48:58', 157, ''),
(17, 'email', 'kickstart survey', '2015-08-29 09:48:58', 156, ''),
(18, 'email', 'kickstart survey', '2015-08-29 09:48:59', 154, ''),
(19, 'email', 'kickstart survey', '2015-08-29 09:49:00', 153, ''),
(20, 'email', 'kickstart survey', '2015-08-29 09:49:00', 152, ''),
(21, 'email', 'kickstart survey', '2015-08-29 09:49:01', 150, ''),
(22, 'email', 'kickstart survey', '2015-08-29 09:49:01', 147, ''),
(23, 'email', 'kickstart survey', '2015-08-29 09:49:02', 145, ''),
(24, 'email', 'kickstart survey', '2015-08-29 09:49:03', 142, ''),
(25, 'email', 'kickstart survey', '2015-08-29 09:49:03', 141, ''),
(26, 'email', 'kickstart survey', '2015-08-29 09:49:04', 139, ''),
(27, 'email', 'kickstart survey', '2015-08-29 09:49:05', 138, ''),
(28, 'email', 'kickstart survey', '2015-08-29 09:49:05', 137, ''),
(29, 'email', 'kickstart survey', '2015-08-29 09:49:06', 136, ''),
(30, 'email', 'kickstart survey', '2015-08-29 09:49:07', 134, ''),
(31, 'email', 'kickstart survey', '2015-08-29 09:49:08', 132, ''),
(32, 'email', 'kickstart survey', '2015-08-29 09:49:08', 126, ''),
(33, 'email', 'kickstart survey', '2015-08-29 09:49:09', 125, ''),
(34, 'email', 'kickstart survey', '2015-08-29 09:49:09', 124, ''),
(35, 'email', 'kickstart survey', '2015-08-29 09:49:10', 123, ''),
(36, 'email', 'kickstart survey', '2015-08-29 09:49:11', 121, ''),
(37, 'email', 'kickstart survey', '2015-08-29 09:49:11', 120, ''),
(38, 'email', 'kickstart survey', '2015-08-29 09:49:12', 119, ''),
(39, 'email', 'kickstart survey', '2015-08-29 09:49:13', 118, ''),
(40, 'email', 'kickstart survey', '2015-08-29 09:49:13', 114, ''),
(41, 'email', 'kickstart survey', '2015-08-29 09:49:14', 113, ''),
(42, 'email', 'kickstart survey', '2015-08-29 09:49:14', 111, ''),
(43, 'email', 'kickstart survey', '2015-08-29 09:49:15', 109, ''),
(44, 'email', 'kickstart survey', '2015-08-29 09:49:15', 108, ''),
(45, 'email', 'kickstart survey', '2015-08-29 09:49:16', 104, ''),
(46, 'email', 'kickstart survey', '2015-08-29 09:49:17', 102, ''),
(47, 'email', 'kickstart survey', '2015-08-29 09:49:17', 99, ''),
(48, 'email', 'kickstart survey', '2015-08-29 09:49:18', 98, ''),
(49, 'email', 'kickstart survey', '2015-08-29 09:49:18', 97, ''),
(50, 'email', 'kickstart survey', '2015-08-29 09:49:19', 96, ''),
(51, 'email', 'kickstart survey', '2015-08-29 09:49:20', 94, ''),
(52, 'email', 'kickstart survey', '2015-08-29 09:49:20', 93, ''),
(53, 'email', 'kickstart survey', '2015-08-29 09:49:23', 92, ''),
(54, 'email', 'kickstart survey', '2015-08-29 09:49:24', 91, ''),
(55, 'email', 'kickstart survey', '2015-08-29 09:49:24', 90, ''),
(56, 'email', 'kickstart survey', '2015-08-29 09:49:25', 89, ''),
(57, 'email', 'kickstart survey', '2015-08-29 09:49:25', 88, ''),
(58, 'email', 'kickstart survey', '2015-08-29 09:49:26', 87, ''),
(59, 'email', 'kickstart survey', '2015-08-29 09:49:27', 85, ''),
(60, 'email', 'kickstart survey', '2015-08-29 09:49:27', 84, ''),
(61, 'email', 'kickstart survey', '2015-08-29 09:49:28', 83, ''),
(62, 'email', 'kickstart survey', '2015-08-29 09:49:29', 82, ''),
(63, 'email', 'kickstart survey', '2015-08-29 09:49:29', 81, ''),
(64, 'email', 'kickstart survey', '2015-08-29 09:49:30', 80, ''),
(65, 'email', 'kickstart survey', '2015-08-29 09:49:30', 79, ''),
(66, 'email', 'kickstart survey', '2015-08-29 09:49:31', 76, ''),
(67, 'email', 'kickstart survey', '2015-08-29 09:49:31', 74, ''),
(68, 'email', 'kickstart survey', '2015-08-29 09:49:32', 73, ''),
(69, 'email', 'kickstart survey', '2015-08-29 09:49:32', 70, ''),
(70, 'email', 'kickstart survey', '2015-08-29 09:49:33', 69, ''),
(71, 'email', 'kickstart survey', '2015-08-29 09:49:33', 62, ''),
(72, 'email', 'kickstart survey', '2015-08-29 09:49:34', 60, ''),
(73, 'email', 'kickstart survey', '2015-08-29 09:49:34', 59, ''),
(74, 'email', 'kickstart survey', '2015-08-29 09:49:35', 57, ''),
(75, 'email', 'kickstart survey', '2015-08-29 09:49:36', 56, ''),
(76, 'email', 'kickstart survey', '2015-08-29 09:49:36', 55, ''),
(77, 'email', 'kickstart survey', '2015-08-29 09:49:37', 58, ''),
(78, 'email', 'kickstart survey', '2015-08-29 09:49:37', 51, ''),
(79, 'email', 'kickstart survey', '2015-08-29 09:49:38', 45, ''),
(80, 'email', 'kickstart survey', '2015-08-29 09:49:38', 44, ''),
(81, 'email', 'kickstart survey', '2015-08-29 09:49:39', 40, ''),
(82, 'email', 'kickstart survey', '2015-08-29 09:49:39', 39, ''),
(83, 'email', 'kickstart survey', '2015-08-29 09:49:40', 38, ''),
(84, 'email', 'kickstart survey', '2015-08-29 09:49:41', 37, ''),
(85, 'email', 'kickstart survey', '2015-08-29 09:49:41', 36, ''),
(86, 'email', 'kickstart survey', '2015-08-29 09:49:42', 35, ''),
(87, 'email', 'kickstart survey', '2015-08-29 09:49:42', 34, ''),
(88, 'email', 'kickstart survey', '2015-08-29 09:49:43', 31, ''),
(89, 'email', 'kickstart survey', '2015-08-29 09:49:44', 26, ''),
(90, 'email', 'kickstart survey', '2015-08-29 09:49:44', 20, ''),
(91, 'email', 'kickstart survey', '2015-08-29 09:49:45', 19, ''),
(92, 'email', 'kickstart survey', '2015-08-29 09:49:45', 17, ''),
(93, 'email', 'kickstart survey', '2015-08-29 09:49:46', 16, ''),
(94, 'email', 'kickstart survey', '2015-08-29 09:49:46', 14, ''),
(95, 'email', 'kickstart survey', '2015-08-29 09:49:47', 13, ''),
(96, 'email', 'kickstart survey', '2015-08-29 09:49:48', 9, ''),
(97, 'email', 'kickstart survey', '2015-08-29 09:49:48', 8, ''),
(98, 'email', 'kickstart survey', '2015-08-29 09:49:49', 1, ''),
(99, 'email', 'kickstart survey retry', '2015-09-30 06:06:12', 170, ''),
(100, 'email', 'kickstart survey retry', '2015-09-30 06:06:12', 169, ''),
(101, 'email', 'kickstart survey retry', '2015-09-30 06:06:13', 160, ''),
(102, 'email', 'kickstart survey retry', '2015-09-30 06:06:14', 154, ''),
(103, 'email', 'kickstart survey retry', '2015-09-30 06:06:15', 153, ''),
(104, 'email', 'kickstart survey retry', '2015-09-30 06:06:15', 147, ''),
(105, 'email', 'kickstart survey retry', '2015-09-30 06:06:16', 145, ''),
(106, 'email', 'kickstart survey retry', '2015-09-30 06:06:17', 138, ''),
(107, 'email', 'kickstart survey retry', '2015-09-30 06:06:18', 136, ''),
(108, 'email', 'kickstart survey retry', '2015-09-30 06:06:19', 124, ''),
(109, 'email', 'kickstart survey retry', '2015-09-30 06:06:20', 121, ''),
(110, 'email', 'kickstart survey retry', '2015-09-30 06:06:21', 104, ''),
(111, 'email', 'kickstart survey retry', '2015-09-30 06:06:22', 93, ''),
(112, 'email', 'kickstart survey retry', '2015-09-30 06:06:22', 89, ''),
(113, 'email', 'kickstart survey retry', '2015-09-30 06:06:23', 82, ''),
(114, 'email', 'kickstart survey retry', '2015-09-30 06:06:24', 76, ''),
(115, 'email', 'kickstart survey retry', '2015-09-30 06:06:25', 69, ''),
(116, 'email', 'kickstart survey retry', '2015-09-30 06:06:26', 60, ''),
(117, 'email', 'kickstart survey retry', '2015-09-30 06:06:28', 40, ''),
(118, 'email', 'kickstart survey retry', '2015-09-30 06:06:32', 39, ''),
(119, 'email', 'kickstart survey retry', '2015-09-30 06:06:33', 31, ''),
(120, 'email', 'kickstart survey retry', '2015-09-30 06:06:34', 19, ''),
(121, 'email', 'kickstart survey retry', '2015-09-30 06:06:35', 17, ''),
(122, 'email', 'kickstart survey retry', '2015-09-30 06:06:36', 16, ''),
(123, 'email', 'kickstart survey retry', '2015-09-30 06:06:37', 9, ''),
(124, 'email', 'kickstart survey retry', '2015-09-30 06:06:37', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_shipped` datetime NOT NULL,
  `total_price_eurocents` int(11) NOT NULL,
  `payment` enum('quote','pending','completed','failed') NOT NULL DEFAULT 'pending',
  `delivery` enum('pending','shipped','finished') NOT NULL DEFAULT 'pending',
  `delivery_address` varchar(100) NOT NULL,
  `payment_method` enum('credit card','wire transfer','paypal','kickstarter') NOT NULL DEFAULT 'credit card',
  `tax_rate` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `discount_rate` float NOT NULL DEFAULT '0',
  `shipping_cost` int(11) NOT NULL,
  `grand_total` int(11) NOT NULL,
  `plug_type` enum('A','C','D','G','I') NOT NULL DEFAULT 'C',
  `remarks` text,
  `user_remarks` text NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `is_priority` int(1) NOT NULL DEFAULT '0',
  `is_error` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=587 ;

-- --------------------------------------------------------

--
-- Table structure for table `payment_terms`
--

CREATE TABLE IF NOT EXISTS `payment_terms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `payment_term` enum('PAID','N15','N30') NOT NULL DEFAULT 'PAID',
  `payment_text` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `payment_terms`
--

INSERT INTO `payment_terms` (`id`, `payment_term`, `payment_text`) VALUES
(1, 'PAID', 'Already paid'),
(2, 'N15', '15 days after invoice date'),
(3, 'N30', '30 days after invoice date');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `price_eurocent` decimal(10,0) NOT NULL,
  `imageURL` varchar(200) NOT NULL,
  `stock` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `category` enum('kit','single unit','admin test','old') NOT NULL,
  `tags` int(11) NOT NULL,
  `anchors` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `SKU`, `price_eurocent`, `imageURL`, `stock`, `date_created`, `category`, `tags`, `anchors`) VALUES
(1, 'Ready to Range', '<p>\r\nHow far is your Arduino? With this kit you receive two Arduino compatible tags allowing you to do accurate ranging, motion sensing and wireless ultra-wideband communication. \r\n</p>\r\n', '', '24900', '', 0, '2016-02-16 00:00:00', 'kit', 2, 0),
(2, 'Ready to Localize', '<p>\r\nWhere is my Arduino? This is the complete Pozyx positioning platform for 3D indoor positioning. It includes:<br> \r\n1x Arduino compatible tag <br>\r\n4x Pozyx anchor (with casing) <br>\r\n4x micro usb power adapter</p>\r\n', '', '59900', '', 100, '2016-02-16 00:00:00', 'kit', 1, 4),
(3, 'Developer''s kit', '<p>\nGet started with multi-tag positioning! Track the position and orientation of multiple arduino''s with this kit. Furthermore, we include an SWD programmer so you can easily reprogram the board for your own development. The kit includes: <br>\n5x Arduino compatible tag <br>\n4x pozyx anchor (with casing) <br>\n4x micro usb power adapter <br>\n</p>\n', '', '99900', '', 0, '2016-02-16 00:00:00', 'kit', 4, 5),
(4, 'Pozyx tag', '<p>A single Arduino compatible pozyx tag. Localization is not possible without the anchors.</p>', '', '13500', '', 0, '2016-02-16 00:00:00', 'single unit', 1, 0),
(5, 'Pozyx anchor', '<p>A single Pozyx anchor with usb power adaptor. Can be used to support positioning of the Pozyx tag.</p>', '', '13500', '', 0, '2016-02-16 00:00:00', 'single unit', 0, 1),
(6, 'test product', 'This is a test product only visible to admins for testing the payment gateways.', '', '100', '', 10000, '2016-02-16 00:00:00', 'admin test', 0, 0),
(7, 'test product', 'This is a test product only visible to admins for testing the payment gateways.', '', '100', '', 10000, '2016-02-16 00:00:00', 'admin test', 0, 0),
(8, 'Pozyx ultra-wideband research testbed ', '', '', '349900', '', 0, '2016-02-16 00:00:00', 'admin test', 50, 0),
(10, 'Ready to Localize', '<p>\r\nWhere is my Arduino? This is the complete Pozyx positioning platform for 3D indoor positioning. It includes:<br> \r\n1x Arduino compatible tag <br>\r\n4x Pozyx anchor (with casing) <br>\r\n4x micro usb power adapter</p>\r\n', '', '49500', '', 100, '2015-07-07 00:00:00', 'old', 1, 4),
(11, 'Developer''s kit', '<p>\r\nGet started with multi-tag positioning! Track the position and orientation of multiple arduino''s with this kit. Furthermore, we include an SWD programmer so you can easily reprogram the board for your own development. The kit includes: <br>\r\n6x Arduino compatible tag <br>\r\n6x pozyx anchor (with casing) <br>\r\n6x micro usb power adapter <br>\r\n</p>\r\n', '', '99000', '', 0, '2015-07-07 00:00:00', 'old', 6, 6),
(12, 'Pozyx tag', '<p>A single Arduino compatible pozyx tag. Localization is not possible without the anchors.</p>', '', '10900', '', 0, '2015-08-05 16:45:16', 'old', 1, 0),
(13, 'Pozyx anchor', '<p>A single Pozyx anchor with usb power adaptor. Can be used to support positioning of the Pozyx tag.</p>', '', '10900', '', 0, '2015-08-05 16:45:51', 'old', 0, 1),
(9, 'Ready to range', '<p>\r How far is your Arduino? With this kit you receive two Arduino compatible tags allowing you to do accurate ranging, motion sensing and wireless ultra-wideband communication. \r </p>\r ', '', '19900', '', 0, '2015-08-05 16:45:51', 'old', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_orders`
--

CREATE TABLE IF NOT EXISTS `purchase_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `delivery_date` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=260 ;
-- --------------------------------------------------------

--
-- Table structure for table `registers`
--

CREATE TABLE IF NOT EXISTS `registers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reg_address` varchar(10) NOT NULL,
  `reg_name` varchar(100) NOT NULL,
  `type` enum('R','W','R/W','F') NOT NULL,
  `num_bytes` varchar(11) NOT NULL DEFAULT '1',
  `default_val` varchar(10) NOT NULL,
  `internal` varchar(200) NOT NULL,
  `description_short` varchar(300) NOT NULL,
  `description_long` text NOT NULL,
  `version` varchar(200) NOT NULL DEFAULT '',
  `category` varchar(200) NOT NULL,
  `code_example` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `registers`
--

INSERT INTO `registers` (`id`, `reg_address`, `reg_name`, `type`, `num_bytes`, `default_val`, `internal`, `description_short`, `description_long`, `version`, `category`, `code_example`) VALUES
(1, '0x0', 'POZYX_WHO_AM_I', 'R', '1', '43', 'uint8_t whoami', 'Returns the constant value 0x43.', 'This register identifies the Pozyx device. This can be used to make sure that Pozyx is connected properly.<br><b>Default value:</b> 0x43.', 'v0.9', 'Status registers', 'uint8_t whoami;\r\nPozyx.regRead(POZYX_WHO_AM_I, &whoami, 1);'),
(2, '0x1', 'POZYX_FIRMWARE_VER', 'R', '1', '1', 'uint8_t firmware_version', 'Returns the POZYX firmware version.', 'The value of this register describes the firmware version installed on the devices. It is recommended to have all devices run on the same firmware version.\r\n<br> <br> Register contents:<br> <table class=''table table-bordered table-condensed bittable''> <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr> <tr><td><b>Bit flag</b></td> <td colspan=''4''>MAJOR</td><td colspan=''4''>MINOR</td></tr> </table> <table class=''table table-bordered table-condensed'' style='' width:700px; ''> <tr><th style=''min-width:70px''>Bit flag</th><th>Description</th></tr> <tr> <td>MINOR</td><td>Indicate the minor version number<br> The full version is given as v{MAJOR}.{MINOR}. </td></tr> <td>MAJOR</td><td>Indicate the major version number<br> The full version is given as v{MAJOR}.{MINOR}. </td></tr> </table> \r\nNote that the very first firmware version, i.e. v0.9, follows a different numbering convention and will return the value 1 as the firmware version.', 'v0.9', 'Status registers', 'uint8_t firmware_version;\r\nPozyx.regRead(POZYX_FIRMWARE_VER, &firmware_version, 1);'),
(3, '0x2', 'POZYX_HARDWARE_VER', 'R', '1', '13', 'uint8_t hardware_version', 'Returns the POZYX hardware version.', 'The value of this register describes the hardware type and version. The value is programmed during production and cannot\r\n      be changed.<br>\r\n      <br>\r\n      Register contents:<br>\r\n      <table class=''table table-bordered table-condensed bittable''>\r\n      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>\r\n\r\n      <tr><td><b>Bit flag</b></td>\r\n      <td colspan=''3''>TYPE</td><td colspan=''5''>VERSION</td></tr>\r\n      </table>\r\n\r\n      <table class=''table table-bordered table-condensed'' style='' width:700px; ''>\r\n      <tr><th style=''min-width:70px''>Bit flag</th><th>Description</th></tr>\r\n      <tr>\r\n      <td>VERSION</td><td>Indicate the Hardware version. Possible values:<br>\r\n      0x2 : version 1.2<br>\r\n      0x3 : version 1.3<br>\r\n      </td></tr>\r\n      <td>TYPE</td><td>The type of hardware<br>\r\n      0x0 : Pozyx anchor <br>\r\n      0x1 : Pozyx Arduino Shield<br>\r\n      </td></tr>\r\n      </table>  ', 'v0.9', 'Status registers', 'uint8_t hardware_version;\r\nPozyx.regRead(POZYX_HARDWARE_VER, &hardware_version, 1);'),
(4, '0x3', 'POZYX_ST_RESULT', 'R', '1', '1F', 'uint8_t selftest', 'Returns the self-test result', 'This register shows the results of the internal selftest. The self test is automatically initiated at device startup.<br>Register contents:<br><table class=''table table-bordered table-condensed bittable''><tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr><tr><td><b>Bit flag</b></td><td>-</td><td>-</td><td>UWB</td><td>PRESS</td><td>IMU</td><td>GYRO</td><td>MAGN</td><td>ACC</td></tr></table><table class=''table table-bordered table-condensed'' style='' width:700px; ''><tr><th>Bit flag</th><th>Description</th></tr><tr><td>ACC</td><td>A value of 1 means that the selftest passed for the accelerometer. A 0 means failure.</td></tr><td>MAGN</td><td>A value of 1 means that the selftest passed for the mangetometer. A 0 means failure. This value is 0 for the pozyx anchors because they are not equipped with this sensor.</td></tr><td>GYRO</td><td>A value of 1 means that the selftest passed for the gyroscope. A 0 means failure. This value is 0 for the pozyx anchors because they are not equipped with this sensor.</td></tr><td>IMU</td><td>A value of 1 means that the selftest passed for the IMU microcontroller. A 0 means failure. This value is 0 for the pozyx anchors because they are not equipped with this sensor.</td></tr><td>PRESS</td><td>A value of 1 means that the selftest passed for the pressure sensor. A 0 means failure. This value is 0 for the pozyx anchors because they are not equipped with this sensor.</td></tr><td>UWB</td><td>A value of 1 means that the selftest passed for the ultra-wideband transceiver. A 0 means failure.</td></tr></table>', 'v0.9', 'Status registers', 'uint8_t selftest;\r\nPozyx.regRead(POZYX_ST_RESULT, &selftest, 1);'),
(5, '0x4', 'POZYX_ERRORCODE', 'R', '1', '0', 'uint8_t error_code', 'Describes a possibly system error.', 'This register hold the error code whenever an error occurs on the pozyx platform. \r\n      The presence of an error is indicated by the ERR-bit in the <a href="#POZYX_INT_STATUS">POZYX_INT_STATUS</a> register.\r\n      <br><br>\r\n      Possible values:\r\n\r\n      <table class=''table table-bordered table-condensed'' [removed] width:700px; ''>\r\n      <tr><th>Value</th><th>Description</th></tr>\r\n      \r\n      <tr><td>0x00</td><td>POZYX_ERROR_NONE   </td></tr>\r\n      <tr><td>0x01</td><td>POZYX_ERROR_I2C_WRITE   </td></tr>\r\n      <tr><td>0x02</td><td>POZYX_ERROR_I2C_CMDFULL   </td></tr>\r\n      <tr><td>0x03</td><td>POZYX_ERROR_ANCHOR_ADD    </td></tr>\r\n      <tr><td>0x04</td><td>POZYX_ERROR_COMM_QUEUE_FULL </td></tr>\r\n      <tr><td>0x05</td><td>POZYX_ERROR_I2C_READ    </td></tr>\r\n      <tr><td>0x06</td><td>POZYX_ERROR_UWB_CONFIG    </td></tr>\r\n      <tr><td>0x07</td><td>POZYX_ERROR_OPERATION_QUEUE_FULL</td></tr>\r\n      <tr><td>0xA0</td><td>POZYX_ERROR_TDMA      </td></tr>\r\n      <tr><td>0x08</td><td>POZYX_ERROR_STARTUP_BUSFAULT</td></tr>\r\n      <tr><td>0x09</td><td>POZYX_ERROR_FLASH_INVALID </td></tr>\r\n      <tr><td>0X0A</td><td>POZYX_ERROR_NOT_ENOUGH_ANCHORS</td></tr>      \r\n      <tr><td>0X0B</td><td>POZYX_ERROR_DISCOVERY</td></tr>\r\n      <tr><td>0x0C</td><td>POZYX_ERROR_CALIBRATION</td></tr>\r\n      <tr><td>0x0D</td><td>POZYX_ERROR_FUNC_PARAM</td></tr>\r\n      <tr><td>0x0E</td><td>POZYX_ERROR_ANCHOR_NOT_FOUND</td></tr>\r\n      <tr><td>0xFF</td><td>POZYX_ERROR_GENERAL</td></tr>\r\n      </table>  ', 'v0.9', 'Status registers', 'uint8_t error_code;\r\nPozyx.regRead(POZYX_ERRORCODE, &error_code, 1);'),
(6, '0x5', 'POZYX_INT_STATUS', 'R', '1', '0', 'uint8_t interrupt_status', 'Indicates the source of the interrupt.', '<br> Register contents:<br> <table class=''table table-bordered table-condensed bittable''> <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr> <tr><td><b>Bit flag</b></td> <td>-</td><td>-</td><td>-</td><td>FUNC</td><td>RX_DATA</td><td>IMU</td><td>POS</td><td>ERR</td></tr> </table> <table class=''table table-bordered table-condensed'' style='' width:700px; ''> <tr><th>Bit flag</th><th>Description</th></tr> <tr><td>ERR</td><td>Indicates that an has error occured.</td></tr> <tr><td>POS</td><td>Indicates that a new position estimate is available.</td></tr> <tr><td>IMU</td><td>Indicates that a new IMU measurement is available.</td></tr> <tr><td>RX_DATA</td><td>Indicates that the pozyx device has received some data over its wireless uwb link.</td></tr> <tr><td>FUNC</td><td>Indicates that a register function call has finished (excluding positioning).</td></tr> </table>', 'v0.9', 'Status registers', 'uint8_t interrupt_status;\r\nPozyx.regRead(POZYX_INT_STATUS, &interrupt_status, 1);'),
(7, '0x6', 'POZYX_CALIB_STATUS', 'R', '1', '0', 'uint8_t calib_status', 'Returns the calibration status.', '<br>\r\n      Part of the calibration of the motion sensors occurs in the background when the system is running. \r\n      For this calibration, each sensor requires its own typical device movements to become fully calibrated.\r\n      This register contains information about the calibration status of the motion sensors.<br>\r\n      Register contents:<br>\r\n      <table class=''table table-bordered table-condensed bittable''>\r\n      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>\r\n\r\n      <tr><td><b>Bit flag</b></td>\r\n      <td colspan="2">SYS Calib Status</td><td colspan="2">GYR Calib Status</td><td colspan="2"> ACC Calib Status</td><td colspan="2">MAG Calib Status</td></tr>\r\n      </table>\r\n\r\n      <table class=''table table-bordered table-condensed'' style='' width:700px; ''>\r\n      <tr><th>Bit flag</th><th>Description</th></tr>\r\n      <tr><td>SYS Calib Status</td><td>\r\n        Current system calibration status, depends on status of all sensors. Possible values<br>\r\n        0x3 : indicates fully calibrated<br>\r\n        0x0 : indicates not calibrated<br>\r\n\r\n      </td></tr>\r\n      <tr><td>GYR Calib Status</td><td>\r\n        Current gyroscope calibration status, depends on status of the gyroscope. Possible values<br>\r\n        0x3 : indicates fully calibrated<br>\r\n        0x0 : indicates not calibrated<br>\r\n        Place the device in a single stable position for a period of few seconds to allow the gyroscope to calibrate.\r\n\r\n      </td></tr>\r\n      <tr><td>ACC Calib Status</td><td>\r\n        Current accelerometer calibration status, depends on status of the accelerometer. Possible values<br>\r\n        0x3 : indicates fully calibrated<br>\r\n        0x0 : indicates not calibrated<br>\r\n        Place the device in 6 different stable positions for a period of few seconds to allow the accelerometer to calibrate.\r\n        Make sure that there is slow movement between 2 stable positions.\r\n        The 6 stable positions could be in any direction, but make sure that the device is lying at least once perpendicular to the x, y and z axis.\r\n\r\n      </td></tr>\r\n      <tr><td>MAG Calib Status</td><td>\r\n        Current magnetometer calibration status, depends on status of the magnetometer. Possible values<br>\r\n        0x3 : indicates fully calibrated<br>\r\n        0x0 : indicates not calibrated<br>\r\n        Make some random movements (for example: writing the number ''8'' on air) to allow calibration of the magnetometer.\r\n        The magnetometer is very susceptible to external magnetic fields, When the magnetometer is not calibrated, it may\r\n        not be used to compute the device orientation, which may result in erroneous absolute heading information.\r\n\r\n      </td></tr>\r\n     \r\n      </table>', 'v0.9', 'Status registers', 'uint8_t calib_status;\r\nPozyx.regRead(POZYX_CALIB_STATUS, &calib_status, 1);'),
(8, '0x10', 'POZYX_INT_MASK', 'R/W', '1', '0', 'uint8_t interrupt_mask:7;<br> uint8_t interrupt_pin:1', 'Indicates which interrupts are enabled.', ' <br> Register contents:<br> \r\n\r\n<table class=''table table-bordered table-condensed bittable''> <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr> <tr><td><b>Bit flag</b></td> <td>PIN</td><td>-</td><td>-</td><td>FUNC</td><td>RX_DATA</td><td>IMU</td><td>POS</td><td>ERR</td></tr> </table> \r\n\r\n<table class=''table table-bordered table-condensed'' [removed] width:700px; ''> <tr><th>Bit flag</th><th>Description</th></tr> <tr><td>ERR</td><td>Enables interrupts whenever an error occurs.</td></tr> <tr><td>POS</td><td>Enables interrupts whenever a new positiong update is availabe.</td></tr> <tr><td>IMU</td><td>Enables interrupts whenever a new IMU update is availabe.</td></tr> <tr><td>RX_DATA</td><td>Enables interrupts whenever data is received through the ultra-wideband network.</td></tr> <tr><td>FUNC</td><td>Enables interrupts whenever a register function call has completed.</td></tr> <tr><td>PIN</td><td>Configures the interrupt pin.<br>Value 0: Pin 0.<br>Value 1: Pin 1.</td> </tr> </table>', 'v0.9', 'Configuration registers', '// disable all interrupts\r\nuint8_t interrupt_mask = 0;\r\nPozyx.regWrite(POZYX_INT_MASK, &interrupt_mask, 1);'),
(9, '0x11', 'POZYX_CONFIG_GPIO1', 'R/W', '1', '0', 'uint8_t gpio_conf_1', 'Configure GPIO pin 1.', '<br> Register contents:<br> <table class=''table table-bordered table-condensed bittable''> <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr> <tr><td><b>Bit flag</b></td> <td>-</td><td>-</td><td>-</td><td colspan=''2''>PULL</td><td colspan=''3''>MODE</td></tr> </table> <table class=''table table-bordered table-condensed'' style='' width:700px; ''> <tr><th style=''min-width:70px''>Bit flag</th><th>Description</th></tr> <tr> <td>MODE</td><td>Indicates the input or output mode of the pin<br> 0 : digital input<br> 1 : digital output (push-pull)<br> 2 : digital output (open-drain)<br> </td></tr> <td>PULL</td><td>When selecting input or open-drain output, the pin can be internally connected with a pull-up (to 0V) or pull-down (to 3.3V) resistor. <br> 0 : no pull-up or pull-down resistor. <br> 1 : pull-up resistor. <br> 2 : pull-down resistor </td></tr> </table>', 'v0.9', 'Configuration registers', '// configure the GPIO as a digital input without pull up or pull down resistors.\r\nuint8_t gpio_pull_1 = 0;\r\nuint8_t gpio_mode_1 = 0;\r\nuint8_t gpio_conf_1 = (gpio_pull_1 << 3) | gpio_mode_1 ;\r\nPozyx.regWrite(POZYX_CONFIG_GPIO1, &gpio_conf_1, 1);'),
(10, '0x12', 'POZYX_CONFIG_GPIO2', 'R/W', '1', '0', 'uint8_t gpio_conf_2', 'Configure GPIO pin 2.', '<br> Register contents:<br> <table class=''table table-bordered table-condensed bittable''> <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr> <tr><td><b>Bit flag</b></td> <td>-</td><td>-</td><td>-</td><td colspan=''2''>PULL</td><td colspan=''3''>MODE</td></tr> </table> <table class=''table table-bordered table-condensed'' style='' width:700px; ''> <tr><th style=''min-width:70px''>Bit flag</th><th>Description</th></tr> <tr> <td>MODE</td><td>Indicates the input or output mode of the pin<br> 0 : digital input<br> 1 : digital output (push-pull)<br> 2 : digital output (open-drain)<br> </td></tr> <td>PULL</td><td>When selecting input or open-drain output, the pin can be internally connected with a pull-up (to 0V) or pull-down (to 3.3V) resistor. <br> 0 : no pull-up or pull-down resistor. <br> 1 : pull-up resistor. <br> 2 : pull-down resistor </td></tr> </table>', 'v0.9', 'Configuration registers', '// configure the GPIO as a digital input without pull up or pull down resistors.\r\nuint8_t gpio_pull_2 = 0;\r\nuint8_t gpio_mode_2 = 0;\r\nuint8_t gpio_conf_2 = (gpio_pull_2 << 3) | gpio_mode_2 ;\r\nPozyx.regWrite(POZYX_CONFIG_GPIO2, &gpio_conf_2, 1);'),
(11, '0x13', 'POZYX_CONFIG_GPIO3', 'R/W', '1', '0', 'uint8_t gpio_conf_3', 'Configure GPIO pin 3.', '<br> Register contents:<br> <table class=''table table-bordered table-condensed bittable''> <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr> <tr><td><b>Bit flag</b></td> <td>-</td><td>-</td><td>-</td><td colspan=''2''>PULL</td><td colspan=''3''>MODE</td></tr> </table> <table class=''table table-bordered table-condensed'' style='' width:700px; ''> <tr><th style=''min-width:70px''>Bit flag</th><th>Description</th></tr> <tr> <td>MODE</td><td>Indicates the input or output mode of the pin<br> 0 : digital input<br> 1 : digital output (push-pull)<br> 2 : digital output (open-drain)<br> </td></tr> <td>PULL</td><td>When selecting input or open-drain output, the pin can be internally connected with a pull-up (to 0V) or pull-down (to 3.3V) resistor. <br> 0 : no pull-up or pull-down resistor. <br> 1 : pull-up resistor. <br> 2 : pull-down resistor </td></tr> </table>', 'v0.9', 'Configuration registers', '// configure the GPIO as a digital input without pull up or pull down resistors.\r\nuint8_t gpio_pull_3 = 0;\r\nuint8_t gpio_mode_3 = 0;\r\nuint8_t gpio_conf_3 = (gpio_pull_3 << 3) | gpio_mode_3 ;\r\nPozyx.regWrite(POZYX_CONFIG_GPIO3, &gpio_conf_3, 1);'),
(12, '0x14', 'POZYX_CONFIG_GPIO4', 'R/W', '1', '0', 'uint8_t gpio_conf_4', 'Configure GPIO pin 4.', '<br> Register contents:<br> <table class=''table table-bordered table-condensed bittable''> <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr> <tr><td><b>Bit flag</b></td> <td>-</td><td>-</td><td>-</td><td colspan=''2''>PULL</td><td colspan=''3''>MODE</td></tr> </table> <table class=''table table-bordered table-condensed'' style='' width:700px; ''> <tr><th style=''min-width:70px''>Bit flag</th><th>Description</th></tr> <tr> <td>MODE</td><td>Indicates the input or output mode of the pin<br> 0 : digital input<br> 1 : digital output (push-pull)<br> 2 : digital output (open-drain)<br> </td></tr> <td>PULL</td><td>When selecting input or open-drain output, the pin can be internally connected with a pull-up (to 0V) or pull-down (to 3.3V) resistor. <br> 0 : no pull-up or pull-down resistor. <br> 1 : pull-up resistor. <br> 2 : pull-down resistor </td></tr> </table>', 'v0.9', 'Configuration registers', '// configure the GPIO as a digital input without pull up or pull down resistors.\r\nuint8_t gpio_pull_4 = 0;\r\nuint8_t gpio_mode_4 = 0;\r\nuint8_t gpio_conf_4 = (gpio_pull_4 << 3) | gpio_mode_4 ;\r\nPozyx.regWrite(POZYX_CONFIG_GPIO4, &gpio_conf_4, 1);'),
(13, '0x15', 'POZYX_CONFIG_LEDS', 'R/W', '1', '3F', 'uint8_t led_conf', 'Configure the LEDs', 'This register configures the functionality of the 6 LEDs on the pozyx device. At all times, the user can control LEDs 1 through 4 using <a href="#POZYX_LED_CTRL">POZYX_LED_CTRL</a>.<br><br> Register contents:<br> <table class=''table table-bordered table-condensed bittable''> <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr> <tr><td><b>Bit flag</b></td> <td>-</td><td>-</td><td>LEDTX</td><td>LEDRX</td><td>LED4</td><td>LED3</td><td>LED2</td><td>LED1</td></tr> </table> <br> <table class=''table table-bordered table-condensed'' style='' width:700px; ''> <tr><th style=''min-width:70px''>Bit flag</th><th>Description</th></tr> <tr> <td>LED_1</td><td>Possible values:<br> 0 : The LED is not controlled by the Pozyx system.<br> 1 : The LED is controlled by the Pozyx system. The LED will blink (roughly) every 2s to indicate that the device is working properly.<br> </td></tr> <tr> <tr> <td>LED_2</td><td>Possible values:<br> 0 : The LED is not controlled by the Pozyx system.<br> 1 : The LED is controlled by the Pozyx system. The LED will be turned on when the device is performing a register write operation or a register function (i.e., calibrating, positioning, ..). <br> </td></tr> <tr> <tr> <td>LED_3</td><td>Possible values:<br> 0 : The LED is not controlled by the Pozyx system.<br> 1 : The LED is controlled by the Pozyx system. Currently no purpose is assigned to this LED.<br> </td></tr> <tr> <tr> <td>LED_4</td><td>Possible values:<br> 0 : The LED is not controlled by the Pozyx system.<br> 1 : The LED is controlled by the Pozyx system. The LED is turned on whenever an error occurs. The error can be read from the <a href="#POZYX_ERRORCODE">POZYX_ERRORCODE</a> register.<br> </td></tr> <tr> <tr> <td>LED_RX</td><td>Possible values:<br> 0 : The LED will not blink upon reception of an UWB message.<br> 1 : The LED will blink upon reception of an UWB message.<br> </td></tr> <tr> <tr> <td>LED_TX</td><td>Possible values:<br> 0 : The LED will not blink upon transmission of an UWB message.<br> 1 : The LED will blink upon transmission of an UWB message.<br> </td></tr> <tr> </table> ', 'v0.9', 'Configuration registers', '// configure all leds in manual mode\r\nuint8_t led_conf = 0;\r\nPozyx.regWrite(POZYX_CONFIG_LEDS, &led_conf, 1);'),
(14, '0x16', 'POZYX_POS_ALG', 'R/W', '1', '1', 'uint8_t algorithm:4; uint8_t pos_dim:2; uint8_t dummy:2', 'Algorithm used for positioning', 'This register selects and configures the positioning algorithm used by the pozyx device.<br> <br> Register contents:<br> <table class=''table table-bordered table-condensed bittable''> <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr> <tr><td><b>Bit flag</b></td> <td>-</td><td>-</td><td colspan=''2'' align=''center''>DIM</td><td colspan=''4'' align=''center''>ALGORITHM</td></tr> </table> \r\n\r\n<table class=''table table-bordered table-condensed'' [removed]=''min-width:70px''>Bit flag</th><th>Description</th></tr> <tr> <td>ALGORITHM</td><td>Indicates which algorithm to use for positioning<br> \r\n\r\n0x0 : UWB-only <b>(Default value)</b>. This algorithm estimates the position using UWB measurments only. It is specifically designed to deal with NLOS-measurements.<br> <hr class=''seperator''> \r\n\r\n0x1 : Tracking. This algorithm is scheduled for firmware version v1.0, it is not available in version v0.9. This tracking algorithm is designed for high speed operation at update rates > 1Hz. It uses UWB-measurements, IMU data and information from previous timesteps.<br> <hr class=''seperator''> \r\n\r\n0x2 : Least-Squares. This algorithm is described on the Pozyx website in the <a href="<?php echo site_url(''Documentation/doc_howDoesPositioningWork''); ?>">documentation</a>. It is a simple algorithm that only uses UWB-measurments and that cannot handle NLOS range measurements well. <br> </td></tr> <tr> <td>DIM</td><td>This indicates the spatial dimension. Possible values:<br> 0x2 : 2D <b>(Default value)</b>. In two dimensional mode, the x and y coordinates are estimated. It is expected that all tags and anchors are located in the same horizontal plane.<br> <hr class=''seperator''> 0x1 : 2,5D. In this mode, the x and y coordinates are estimated. However, anchors and tags are not required to be located in the same horizontal plane. For this mode it is necessary that the z-coordinates of the anchors and tag are known. For the tag it must be stored in the <?php register_url("POZYX_POS_Z"); ?>; register. In general this mode results in superior positioning accuracy as compared to full 3D when the anchors cannot be placed well. It is especially usefull when the tag is mounted on a robot or VR-helmet.<br> <hr class=''seperator''> 0x3 : 3D. In three dimensional mode, the x,y and z coordinates are estimated. In order to obtain a good vertical accuracy, it is required to place the anchors at different heights. Check out the <a href="<?php echo site_url(''Documentation/doc_whereToPlaceTheAnchors''); ?>">documentation</a> for more information on anchor placement. </td></tr> </table> <br><br> ', 'v0.9', 'Configuration registers', '// use the UWB_ONLY algorithm in 2D\r\nuint8_t algorithm = 0; \r\nuint8_t pos_dim = 2; \r\nuint8_t pos_alg = algorithm | (pos_dim<<4);\r\nPozyx.regWrite(POZYX_POS_ALG, &pos_alg, 1);'),
(15, '0x17', 'POZYX_POS_NUM_ANCHORS', 'R/W', '1', '4', 'uint8_t num_anchors:7; <br> boolean_t b_autoselect_anchors:1', 'Configure the number of anchors and selection procedure', '<br>Register contents:<br><table class=''table table-bordered table-condensed bittable''><tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr><tr><td><b>Bit flag</b></td><td>MODE</td><td>-</td><td>-</td><td>-</td><td colspan=''4''>NUM</td></tr></table><table class=''table table-bordered table-condensed'' style='' width:700px; ''><tr><th style=''min-width:70px''>Bit flag</th><th>Description</th></tr><tr><td>NUM</td><td>4 bits to indicate the maximum number of anchors to be used for positioning. Value between 3 and 15.</td></tr><td>MODE</td><td>a single bit to indicate wether to choose from a fixed set of anchors or perform automatic anchor selection. Possible values:<br>0 : indicates fixed anchor set. The anchor network IDs that have been supplied by POZYX_POS_SET_ANCHOR_IDS are used for positioning.<br>1 : indicates automatic anchor selection. Anchors from the internal anchor list are used to make the selection.</td></tr></table> ', 'v0.9', 'Configuration registers', '// select 4 anchors automatically\r\nuint8_t num_anchors = 4;\r\nboolean_t b_autoselect_anchors = true\r\nuint8_t data = num_anchors | (b_autoselect_anchors << 7);\r\nPozyx.regWrite(POZYX_POS_NUM_ANCHORS, &data , 1);'),
(16, '0x18', 'POZYX_POS_INTERVAL', 'R/W', '2', '0', 'uint16_t interval_ms', 'Defines the update interval in ms in continuous positioning.', 'Pozyx can be run in continuous mode to provide continuous positioning. The interval in milliseconds between successive updates can be configured with this register. The value is capped between 10ms and 60000ms (1 minute). Writing the value 0 to this registers disables the continuous mode.', 'v0.9', 'Configuration registers', '// perform continuous positioning every 1 second\r\nuint16_t interval_ms = 1000;\r\nPozyx.regWrite(POZYX_POS_INTERVAL, (uint8_t*)&interval_ms , 2);'),
(17, '0x1A', 'POZYX_NETWORK_ID', 'R/W', '2', 'xx', 'uint16_t network_id', 'The network id. ', 'This is the 16bit network address of the pozyx device. This value should be unique within the network.<br>', 'v0.9', 'Configuration registers', '// read out the network id\r\nuint16_t network_id;\r\nPozyx.regRead(POZYX_NETWORK_ID, (uint8_t*)&network_id, 2);'),
(18, '0x1C', 'POZYX_UWB_CHANNEL', 'R/W', '1', '5', 'uint8_t uwb_channel', 'UWB channel number.', 'Select the ultra-wideband transmission and reception channel.<br><b>Warning: </b>to enable wireless communication between two devices they must operate on the same channel.<br><b>Default value: </b>0x05\r\n\r\n      <br><br>\r\n      Register contents:<br>\r\n      <table class=''table table-bordered table-condensed bittable''>\r\n      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>\r\n\r\n      <tr><td><b>Bit flag</b></td>\r\n      <td colspan=''8''>UWB_CHANNEL</td></tr>\r\n      </table>\r\n\r\n      <table class=''table table-bordered table-condensed descr-table''>\r\n      <tr><th>Bit flag</th><th>Description</th></tr>\r\n      <tr>\r\n      <td>UWN_CHANNEL</td><td>Indicate the UWB channel. Possible values:<br>\r\n1 : Centre frequency 3494.4MHz, using the band(MHz): 3244.8 – 3744, bandwidth 499.2 <br>\r\n2 : Centre frequency 3993.6MHz, using the band(MHz): 3774 – 4243.2, bandwidth 499.2 <br>\r\n3 : Centre frequency 4492.8MHz, using the band(MHz): 4243.2 – 4742.4 bandwidth 499.2 <br>\r\n4 : Centre frequency 3993.6MHz, using the band(MHz): 3328 – 4659.2 bandwidth 1331.2 (capped to 900MHz) <br>\r\n5 : Centre frequency 6489.6MHz, using the band(MHz): 6240 – 6739.2 bandwidth 499.2 <br>\r\n7 : Centre frequency 6489.6MHz, using the band(MHz): 5980.3 – 6998.9 bandwidth 1081.6 (capped to 900MHz)       </td></tr>\r\n      </table> ', 'v0.9', 'Configuration registers', ''),
(19, '0x1D', 'POZYX_UWB_RATES', 'R/W', '1', '', 'uint8_t uwb_bitrate:6;<br> uint8_t uwb_prf:2', 'Configure the UWB datarate and pulse repetition frequency (PRF)', '<br>Register contents:<br><table class=''table table-bordered table-condensed bittable''><tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr><tr><td><b>Bit flag</b></td><td>-</td><td>-</td><td>-</td><td>-</td><td colspan=''2''>PRF</td><td colspan=''2''>BITRATE</td></tr></table><table class=''table table-bordered table-condensed'' style='' width:700px; ''><tr><th style=''min-width:70px''>Bit flag</th><th>Description</th></tr><tr><td>BITRATE</td><td>Indicate the UWB bitrate. Possible values:<br>0 : bitrate 110 kbits/s (Default value)<br>1 : bitrate 850 kbits/s<br>2 : bitrate 6.8 Mbits/s<br></td></tr><td>PRF</td><td>Indicates the pulse repetition frequency to be used. Possible values<br>1 : 16 MHz <br>2 : 64 MHz (default value) <br></td></tr></table> ', 'v0.9', 'Configuration registers', ''),
(20, '0x1E', 'POZYX_UWB_PLEN', 'R/W', '1', '', 'uint8_t uwb_preamble_len', 'Configure the UWB preamble length. ', 'This register describes the preamble length of the UWB wireless packets. In general, a larger preamble length will result in increased range at the expense of longer airtime. For more information we refer to UWB settings. <br><br> Register contents:<br> <table class=''table table-bordered table-condensed bittable''> <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr> <tr><td><b>Bit flag</b></td> <td colspan=''8''>PLEN</td></tr> </table> <table class=''table table-bordered table-condensed'' style='' width:700px; ''> <tr><th style=''min-width:70px''>Bit flag</th><th>Description</th></tr> <tr> <td>PLEN</td><td>Indicate the UWB preamble length. Possible values:<br> 0x0C : 4096 symbols. Standard preamble length 4096 symbols<br> 0x28 : 2048 symbols. Non-standard preamble length 2048 symbols<br> 0x18 : 1536 symbols. Non-standard preamble length 1536 symbols<br> 0x08 : 1024 symbols. Standard preamble length 1024 symbols <b>(default value)</b><br> 0x34 : 512 symbols. Non-standard preamble length 512 symbols<br> 0x24 : 256 symbols. Non-standard preamble length 256 symbols<br> 0x14 : 128 symbols. Non-standard preamble length 128 symbols<br> 0x04 : 64 symbols. Standard preamble length 64 symbols<br> </td></tr> </table> ', 'v0.9', 'Configuration registers', ''),
(21, '0x1F', 'POZYX_UWB_GAIN', 'R/W', '1', '', 'int8_t uwb_gain', 'Configure the power gain for the UWB transmitter', '<b>Warning: </b>when changing channel, bitrate or preamble length, the power is also overwritten to the default value for this UWB configuration.<br><b>Warning: </b>changing this value can make the Pozyx device fall out of regulation.', 'v0.9', 'Configuration registers', ''),
(22, '0x20', 'POZYX_UWB_XTALTRIM', 'R/W', '1', '', 'uint8_t uwb_xtaltrim', 'Trimming value for the uwb crystal.', ' This register contains the trimming value to fine-tune the operating frequency of the crystal oscillator used by the ultra-wideband front-end. By carefully selecting this value, the operating frequency can be tuned with an error of 1ppm. A smaller error on the operating frequency will increase the sensitivity of the UWB receiver. <br><br> Register contents:<br> <table class=''table table-bordered table-condensed bittable''> <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr> <tr><td><b>Bit flag</b></td> <td>-</td><td>-</td><td>-</td><td colspan=''5''>TRIMVAL</td></tr> </table> <br>', 'v0.9', 'Configuration registers', ''),
(23, '0x21', 'POZYX_RANGE_PROTOCOL', 'R/W', '1', '0', 'uint8_t range_protocol', 'The ranging protocol', '', 'internal', 'Configuration registers', ''),
(24, '0x22', 'POZYX_OPERATION_MODE', 'R/W', '1', 'x', 'uint8_t operation_mode', 'Configure the mode of operation of the pozyx device', ' This register defines the operation modus of the pozyx device. It''s default value is determined by the presence of the T/A jumper on the pozyx board. When the jumper is present, the device is in tag mode. Note that the operation mode is independent of the hardware type.<br><br> Register contents:<br> <table class=''table table-bordered table-condensed bittable''> <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr> <tr><td><b>Bit flag</b></td> <td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>OP MODE</td></tr> </table> <br> <table class=''table table-bordered table-condensed'' style='' width:700px; ''> <tr><th>Bit flag</th><th>Description</th></tr> <tr> <td>OP MODE</td><td>Possible values:<br> 0 : Anchor mode. In anchor mode the device is assumed to be immobile. The device can be used by other devices for positioning. <br> 1 : Tag mode. In tag mode, the device can more around. In this mode the device cannot be used by other devices for positioning.<br> <br> </td></tr> </tr> </table>', 'v0.9', 'Configuration registers', ''),
(25, '0x23', 'POZYX_SENSORS_MODE', 'R/W', '1', '0', 'uint8_t sensors_mode', 'Configure the mode of operation of the sensors', ' This register configures the mode of operation for the onboard sensors. It takes about 7ms to switch between two modes of operation, or 19ms to go from MODE_OFF to any other mode. Register contents:<br> <table class=''table table-bordered table-condensed bittable''> <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr> <tr><td><b>Bit flag</b></td> <td>-</td><td>-</td><td>-</td><td>-</td><td colspan=''4''>OP MODE</td></tr> </table> <br> <table class=''table table-bordered table-condensed'' style='' width:700px; ''> <tr><th>Bit flag</th><th>Description</th></tr> <tr> <td>OP_MODE</td><td>Possible values:<br> Non-fusion modes:<br> 0 : MODE_OFF<br> 1 : ACCONLY <br> 2 : MAGONLY <br> 3 : GYROONLY <br> 4 : ACCMAGx<br> 5 : ACCGYRO <br> 6 : MAGGYRO <br> 7 : AMG <br> <br> Fusion modes:<br> 8 : IMU <br> 9 : COMPASS <br> 10 : M4G <br> 11 : NDOF_FMC_OFF<br> 12 : NDOF <br> <br> </td></tr> </tr> </table>', 'v0.9', 'Configuration registers', ''),
(26, '0x2D', 'POZYX_TDMA_NUMSLOTS', 'R/W', '2', '', 'uint16_t tdma_numslots', 'Number of slots in a superframe. goes in effect the next superframe', 'The maximum number of slots is 500.<br><b>Default value: </b> 100.<br>', 'internal', 'Configuration registers', ''),
(27, '0x2F', 'POZYX_TDMA_SLOTDURATION_MS', 'R/W', '1', '', 'uint8_t tdma_slot_duration_ms', 'Time per slot in milliseconds. goes in effect the next superframe', '<b>Default value: </b> 30ms.<br>', 'internal', 'Configuration registers', ''),
(28, '0x30', 'POZYX_POS_X', 'R/W', '4', '0', 'int32_t pos_x', 'x-coordinate of the device in mm.', 'The x-cooridnate of the device in millimeter (mm). The position is relative to the anchor positions supplied. For more information on positioning, check the\r\n<a href="<?php echo site_url(''Documentation/Datasheet/FunctionalDescription#Positioning''); ?>">functional description</a>.', 'v0.9', 'Positioning data', '// Read out the 3 coordinates in mm\r\nint32_t coordinates[3];\r\nPozyx.regRead(POZYX_POS_X, (uint8_t*)&coordinates, 3*sizeof(int32_t));'),
(29, '0x34', 'POZYX_POS_Y', 'R/W', '4', '0', 'int32_t pos_y', 'y-coordinate of the device in mm.', '', 'v0.9', 'Positioning data', '// Read out the 3 coordinates in mm\r\nint32_t coordinates[3];\r\nPozyx.regRead(POZYX_POS_X, (uint8_t*)&coordinates, 3*sizeof(int32_t));'),
(30, '0x38', 'POZYX_POS_Z', 'R/W', '4', '0', 'int32_t pos_z', 'z-coordinate of the device in mm.', '', 'v0.9', 'Positioning data', '// Read out the 3 coordinates in mm\r\nint32_t coordinates[3];\r\nPozyx.regRead(POZYX_POS_X, (uint8_t*)&coordinates, 3*sizeof(int32_t));'),
(31, '0x3C', 'POZYX_POS_ERR_X', 'R', '2', '', 'int16_t err_xx', 'estimated error covariance of x', '', 'v0.9', 'Positioning data', ''),
(32, '0x3E', 'POZYX_POS_ERR_Y', 'R', '2', '', 'int16_t err_yy', 'estimated error covariance of y', '', 'v0.9', 'Positioning data', ''),
(33, '0x40', 'POZYX_POS_ERR_Z', 'R', '2', '', 'int16_t err_zz', 'estimated error covariance of z', '', 'v0.9', 'Positioning data', ''),
(34, '0x42', 'POZYX_POS_ERR_XY', 'R', '2', '', 'int16_t err_xy', 'estimated covariance of xy', '', 'v0.9', 'Positioning data', ''),
(35, '0x44', 'POZYX_POS_ERR_XZ', 'R', '2', '', 'int16_t err_xz', 'estimated covariance of xz', '', 'v0.9', 'Positioning data', ''),
(36, '0x46', 'POZYX_POS_ERR_YZ', 'R', '2', '', 'int16_t err_yz', 'estimated covariance of yz', '', 'v0.9', 'Positioning data', ''),
(37, '0x50', 'POZYX_PRESSURE', 'R', '4', '0', 'uint32_t pressure', 'Pressure data', 'This register holds the pressure exerted on the pozyx device. At sealevel the pressure is The pressure is stored as an unsigned 32-bit integer. 1mPa = 1 int.', 'v0.9', 'Sensor data', '// read out the raw pressure data\r\nuint32_t pressure_raw;\r\nPozyx.regRead(POZYX_PRESSURE, (uint8_t*)&pressure_raw, sizeof(uint32_t));'),
(38, '0x54', 'POZYX_ACCEL_X', 'R', '2', '0', 'int16_t accel_x', 'Accelerometer data (in mg)', 'This register contains the offset compensated acceleration in the x-axis of the Pozyx device. The sensor data is represented as a signed 16-bit integer. 1mg = 16 int.', 'v0.9', 'Sensor data', '// read out the raw acceleration data\r\nint16_t acceleration_raw[3];\r\nPozyx.regRead(POZYX_ACCEL_X, (uint8_t*)&acceleration_raw, 3*sizeof(int16_t));'),
(39, '0x56', 'POZYX_ACCEL_Y', 'R', '2', '0', 'int16_t accel_y', '', 'This register contains the offset compensated acceleration in the y-axis of the Pozyx device. The sensor data is represented as a signed 16-bit integer. 1mg = 16 int.', 'v0.9', 'Sensor data', '// read out the raw acceleration data\r\nint16_t acceleration_raw[3];\r\nPozyx.regRead(POZYX_ACCEL_X, (uint8_t*)&acceleration_raw, 3*sizeof(int16_t));'),
(40, '0x58', 'POZYX_ACCEL_Z', 'R', '2', '0', 'int16_t accel_z', '', 'This register contains the offset compensated acceleration in the z-axis of the Pozyx device. The sensor data is represented as a signed 16-bit integer. 1mg = 16 int.', 'v0.9', 'Sensor data', '// read out the raw acceleration data\r\nint16_t acceleration_raw[3];\r\nPozyx.regRead(POZYX_ACCEL_X, (uint8_t*)&acceleration_raw, 3*sizeof(int16_t));'),
(41, '0x5A', 'POZYX_MAGN_X', 'R', '2', '0', 'int16_t magn_x', 'Magnemtometer data', 'This register contains the magnetic field strength along the x-axis of the Pozyx device. The sensor data is stored as a signed 16-bit integer. \r\n\r\n1µT = 16 int.', 'v0.9', 'Sensor data', '// read out the raw magnetometer data\r\nint16_t magn_raw[3];\r\nPozyx.regRead(POZYX_MAGN_X, (uint8_t*)&magn_raw, 3*sizeof(int16_t));'),
(42, '0x5C', 'POZYX_MAGN_Y', 'R', '2', '0', 'int16_t magn_y', '', 'This register contains the magnetic field strength along the y-axis of the Pozyx device. The sensor data is stored as a signed 16-bit integer. 1µT = 16 int.', 'v0.9', 'Sensor data', '// read out the raw magnetometer data\r\nint16_t magn_raw[3];\r\nPozyx.regRead(POZYX_MAGN_X, (uint8_t*)&magn_raw, 3*sizeof(int16_t));'),
(43, '0x5E', 'POZYX_MAGN_Z', 'R', '2', '0', 'int16_t magn_z', '', 'This register contains the magnetic field strength along the y-axis of the Pozyx device. The sensor data is stored as a signed 16-bit integer. 1µT = 16 int.', 'v0.9', 'Sensor data', '// read out the raw magnetometer data\r\nint16_t magn_raw[3];\r\nPozyx.regRead(POZYX_MAGN_X, (uint8_t*)&magn_raw, 3*sizeof(int16_t));'),
(44, '0x60', 'POZYX_GYRO_X', 'R', '2', '0', 'int16_t gyro_x', 'Gyroscope data', 'This register contains the offset compensated angular velocity for the x-axis of the Pozyx device. The sensor data is represented as a signed 16-bit integer. 1degree = 16 int.', 'v0.9', 'Sensor data', '// read out the raw gyroscope data\r\nint16_t gyro_raw[3];\r\nPozyx.regRead(POZYX_GYRO_X, (uint8_t*)&gyro_raw, 3*sizeof(int16_t));'),
(45, '0x62', 'POZYX_GYRO_Y', 'R', '2', '0', 'int16_t gyro_y', '', 'This register contains the offset compensated angular velocity for the y-axis of the Pozyx device. The sensor data is represented as a signed 16-bit integer. 1degree = 16 int.', 'v0.9', 'Sensor data', '// read out the raw gyroscope data\r\nint16_t gyro_raw[3];\r\nPozyx.regRead(POZYX_GYRO_X, (uint8_t*)&gyro_raw, 3*sizeof(int16_t));'),
(46, '0x64', 'POZYX_GYRO_Z', 'R', '2', '0', 'int16_t gyro_z', '', 'This register contains the offset compensated angular velocity for the z-axis of the Pozyx device. The sensor data is represented as a signed 16-bit integer. 1degree = 16 int.', 'v0.9', 'Sensor data', '// read out the raw gyroscope data\r\nint16_t gyro_raw[3];\r\nPozyx.regRead(POZYX_GYRO_X, (uint8_t*)&gyro_raw, 3*sizeof(int16_t));'),
(47, '0x66', 'POZYX_EUL_HEADING', 'R', '2', '0', 'int16_t yaw', 'Euler angles heading (or yaw)', 'This register contains the absolute heading or yaw of the device. The sensor data is represented as a signed 16-bit integer. 1degree = 16 int.', 'v0.9', 'Sensor data', '// read out the raw heading data\r\nint16_t heading_raw;\r\nPozyx.regRead(POZYX_EUL_HEADING, (uint8_t*)&heading_raw, sizeof(int16_t));'),
(48, '0x68', 'POZYX_EUL_ROLL', 'R', '2', '0', 'int16_t roll', 'Euler angles roll', 'This register contains the roll of the device. The sensor data is represented as a signed 16-bit integer. 1degree = 16 int.', 'v0.9', 'Sensor data', '// read out the raw roll data\r\nint16_t roll_raw;\r\nPozyx.regRead(POZYX_EUL_ROLL, (uint8_t*)&roll_raw, sizeof(int16_t));'),
(49, '0x6A', 'POZYX_EUL_PITCH', 'R', '2', '0', 'int16_t pitch', 'Euler angles pitch', 'This register contains the pitch of the device. The sensor data is represented as a signed 16-bit integer. 1degree = 16 int.', 'v0.9', 'Sensor data', '// read out the raw roll data\r\nint16_t pitch_raw;\r\nPozyx.regRead(POZYX_EUL_PITCH, (uint8_t*)&pitch_raw, sizeof(int16_t));'),
(50, '0x6C', 'POZYX_QUAT_W', 'R', '2', '0', 'int16_t quat_w', 'Weight of quaternion.', 'The orientation can be represented using quaternions (w, x, y, z). This register contains the weight w and is represented as a signed 16-bit integer. 1quaternion(unit less) = 2^14 int = 16384 int.', 'v0.9', 'Sensor data', '// read out the orientation as a quaternion\r\nint16_t quat_raw[4];\r\nPozyx.regRead(POZYX_QUAT_W, (uint8_t*)&quat_raw, 4*sizeof(int16_t));'),
(51, '0x6E', 'POZYX_QUAT_X', 'R', '2', '0', 'int16_t quat_x', 'x of quaternion', 'The orientation can be represented using quaternions (w, x, y, z). This register contains the x and is represented as a signed 16-bit integer. 1quaternion(unit less) = 2^14 int = 16384 int.', 'v0.9', 'Sensor data', '// read out the orientation as a quaternion\r\nint16_t quat_raw[4];\r\nPozyx.regRead(POZYX_QUAT_W, (uint8_t*)&quat_raw, 4*sizeof(int16_t));'),
(52, '0x70', 'POZYX_QUAT_Y', 'R', '2', '0', 'int16_t quat_y', 'y of quaternion', 'The orientation can be represented using quaternions (w, x, y, z). This register contains the y and is represented as a signed 16-bit integer. 1quaternion(unit less) = 2^14 int = 16384 int.', 'v0.9', 'Sensor data', '// read out the orientation as a quaternion\r\nint16_t quat_raw[4];\r\nPozyx.regRead(POZYX_QUAT_W, (uint8_t*)&quat_raw, 4*sizeof(int16_t));'),
(53, '0x72', 'POZYX_QUAT_Z', 'R', '2', '0', 'int16_t quat_z', 'z of quaternion', 'The orientation can be represented using quaternions (w, x, y, z). This register contains the z and is represented as a signed 16-bit integer. 1quaternion(unit less) = 2^14 int = 16384 int.', 'v0.9', 'Sensor data', '// read out the orientation as a quaternion\r\nint16_t quat_raw[4];\r\nPozyx.regRead(POZYX_QUAT_W, (uint8_t*)&quat_raw, 4*sizeof(int16_t));'),
(54, '0x74', 'POZYX_LIA_X', 'R', '2', '0', 'int16_t linear_acc_x', 'Linear acceleration in x-direction', 'The linear acceleration is the total acceleration minus the gravity. The linear acceleration expressed the acceleration due to movement. This register holds the linear acceleration along the x-axis of the pozyx device (i.e. body coordinates). 1mg = 16 int.', 'v0.9', 'Sensor data', '// read out the linear acceleration data\r\nint16_t lia_raw[3];\r\nPozyx.regRead(POZYX_LIA_X, (uint8_t*)&lia_raw, 3*sizeof(int16_t));'),
(55, '0x76', 'POZYX_LIA_Y', 'R', '2', '0', 'int16_t linear_acc_y', '', 'The linear acceleration is the total acceleration minus the gravity. The linear acceleration expressed the acceleration due to movement. This register holds the linear acceleration along the y-axis of the pozyx device (i.e. body coordinates). 1mg = 16 int.', 'v0.9', 'Sensor data', '// read out the linear acceleration data\r\nint16_t lia_raw[3];\r\nPozyx.regRead(POZYX_LIA_X, (uint8_t*)&lia_raw, 3*sizeof(int16_t));'),
(56, '0x78', 'POZYX_LIA_Z', 'R', '2', '0', 'int16_t linear_acc_z', '', 'The linear acceleration is the total acceleration minus the gravity. The linear acceleration expressed the acceleration due to movement. This register holds the linear acceleration along the z-axis of the pozyx device (i.e. body coordinates). 1mg = 16 int.', 'v0.9', 'Sensor data', '// read out the linear acceleration data\r\nint16_t lia_raw[3];\r\nPozyx.regRead(POZYX_LIA_X, (uint8_t*)&lia_raw, 3*sizeof(int16_t));'),
(57, '0x7A', 'POZYX_GRAV_X', 'R', '2', '0', 'int16_t grav_x', 'x-component of gravity vector ', 'Gravity is a force of 1g( = 9.80665 m/s2 ) directed towards the ground. This register represents the gravity component in the x-axis and is represented by a singed 16-bit integer. 1mg = 16 int.', 'v0.9', 'Sensor data', '// read out the gravity data\r\nint16_t grav_raw[3];\r\nPozyx.regRead(POZYX_GRAV_X, (uint8_t*)&grav_raw, 3*sizeof(int16_t));'),
(58, '0x7C', 'POZYX_GRAV_Y', 'R', '2', '0', 'int16_t grav_y', 'y-component of gravity vector ', 'Gravity is a force of 1g( = 9.80665 m/s2 ) directed towards the ground. This register represents the gravity component in the y-axis and is represented by a singed 16-bit integer. 1mg = 16 int.', 'v0.9', 'Sensor data', '// read out the gravity data\r\nint16_t grav_raw[3];\r\nPozyx.regRead(POZYX_GRAV_X, (uint8_t*)&grav_raw, 3*sizeof(int16_t));'),
(59, '0x7E', 'POZYX_GRAV_Z', 'R', '2', '0', 'int16_t grav_z', 'z-component of gravity vector ', 'Gravity is a force of 1g( = 9.80665 m/s2 ) directed towards the ground. This register represents the gravity component in the z-axis and is represented by a singed 16-bit integer. 1mg = 16 int.', 'v0.9', 'Sensor data', '// read out the gravity data\r\nint16_t grav_raw[3];\r\nPozyx.regRead(POZYX_GRAV_X, (uint8_t*)&grav_raw, 3*sizeof(int16_t));'),
(60, '0x80', 'POZYX_TEMPERATURE', 'R', '1', '0', 'int8_t temperature', 'Temperature', '', 'v0.9', 'Sensor data', ''),
(61, '0x81', 'POZYX_DEVICE_LIST_SIZE', 'R', '1', '0', 'uint8_t anchor_list_size', 'Returns the number of devices stored internally', 'See the <a href="<?php echo site_url(''Documentation/Datasheet/FunctionalDescription''); ?>">functional description</a> for more info on the device list.', 'v0.9', 'General data', 'uint8_t device_list_size;\r\nPozyx.regRead(POZYX_DEVICE_LIST_SIZE, &device_list_size, 1);'),
(62, '0x82', 'POZYX_RX_NETWORK_ID', 'R', '2', '0', 'uint16_t rx_network_id', 'The network id of the latest received message', 'See the <a href="<?php echo site_url(''Documentation/Datasheet/FunctionalDescription''); ?>">functional description</a> for more info on receiving messages.', 'v0.9', 'General data', 'uint16_t rx_network_id;\r\nPozyx.regRead(POZYX_RX_NETWORK_ID, (uint16_t*)&rx_network_id, 2);'),
(63, '0x84', 'POZYX_RX_DATA_LEN', 'R', '1', '0', 'uint8_t rx_datalen', 'The length of the latest received message', 'See the <a href="<?php echo site_url(''Documentation/Datasheet/FunctionalDescription''); ?>">functional description</a> for more info on receiving messages.', 'v0.9', 'General data', 'uint8_t rx_datalen;\r\nPozyx.regRead(POZYX_RX_DATA_LEN, &rx_datalen, 1);'),
(64, '0x85', 'POZYX_GPIO1', 'R/W', '1', '0', 'uint8_t gpio_1', 'Value of the GPIO pin 1', '<br> This register can be read from to obtain the current state of the GPIO pin if it is configured as an input. When the pin is configured as an output, the value written will determine the new state of the pin. <br><br> Possible values:<br> 0 : The digital state of the pin is LOW at 0V.<br> 1 : The digital state of the pin is HIGH at 3.3V.<br> <b>Default value: </b> 0<br> <br>', 'v0.9', 'General data', 'uint8_t gpio_1;\r\nPozyx.regRead(POZYX_GPIO1, &gpio_1, 1);'),
(65, '0x86', 'POZYX_GPIO2', 'R/W', '1', '0', 'uint8_t gpio_2', 'Value of the GPIO pin 2', '<br> This register can be read from to obtain the current state of the GPIO pin if it is configured as an input. When the pin is configured as an output, the value written will determine the new state of the pin. <br><br> Possible values:<br> 0 : The digital state of the pin is LOW at 0V.<br> 1 : The digital state of the pin is HIGH at 3.3V.<br> <b>Default value: </b> 0<br> <br>', 'v0.9', 'General data', 'uint8_t gpio_2;\r\nPozyx.regRead(POZYX_GPIO2, &gpio_2, 1);'),
(66, '0x87', 'POZYX_GPIO3', 'R/W', '1', '0', 'uint8_t gpio_3', 'Value of the GPIO pin 3', '<br> This register can be read from to obtain the current state of the GPIO pin if it is configured as an input. When the pin is configured as an output, the value written will determine the new state of the pin. <br><br> Possible values:<br> 0 : The digital state of the pin is LOW at 0V.<br> 1 : The digital state of the pin is HIGH at 3.3V.<br> <b>Default value: </b> 0<br> <br>', 'v0.9', 'General data', 'uint8_t gpio_3;\r\nPozyx.regRead(POZYX_GPIO3, &gpio_3, 1);'),
(67, '0x88', 'POZYX_GPIO4', 'R/W', '1', '0', 'uint8_t gpio_4', 'Value of the GPIO pin 4', '<br> This register can be read from to obtain the current state of the GPIO pin if it is configured as an input. When the pin is configured as an output, the value written will determine the new state of the pin. <br><br> Possible values:<br> 0 : The digital state of the pin is LOW at 0V.<br> 1 : The digital state of the pin is HIGH at 3.3V.<br> <b>Default value: </b> 0<br> <br>', 'v0.9', 'General data', 'uint8_t gpio_4;\r\nPozyx.regRead(POZYX_GPIO4, &gpio_4, 1);'),
(68, '0xB0', 'POZYX_RESET_SYS', 'F', '0', '1', '', 'Reset the Pozyx device', 'Calling this function resets the Pozyx device. This also clears the device list and returns the settings to their defualt state (including UWB settings)', 'v0.9', 'Functions', '// reset the pozyx system\r\nPozyx.regFunction(POZYX_RESET_SYS, NULL, 0, NULL, 0);');
INSERT INTO `registers` (`id`, `reg_address`, `reg_name`, `type`, `num_bytes`, `default_val`, `internal`, `description_short`, `description_long`, `version`, `category`, `code_example`) VALUES
(69, '0xB1', 'POZYX_LED_CTRL', 'F', '1', '1', '', 'Control LEDS 1 to 4 on the board', 'This function gives control over the 4 onboard pozyx LEDS.\r\n      <br>\r\n      <br>\r\n      <b>INPUT PARAMETERS</b><br>\r\n      As an input a single byte is required.\r\n\r\n      <table class=''table table-bordered table-condensed bittable''>\r\n      <tr><td></td><td>bit 7</td><td>bit 6</td><td>bit 5</td><td>bit 4</td><td>bit 3</td><td>bit 2</td><td>bit 1</td><td>bit 0</td></tr>\r\n\r\n      <tr><td><b>Bit flag</b></td>\r\n      <td>USELED_4</td><td>USELED 3</td><td>USELED 2</td><td>USELED 1</td>\r\n      <td>LED_4</td><td>LED_3</td><td>LED_2</td><td>LED_1</td></tr>\r\n      </table>\r\n\r\n      <table class=''table table-bordered table-condensed descr-table''>\r\n      <tr><th style=''min-width:70px''>Bit flag</th><th>Description</th></tr>\r\n      <tr>\r\n      <td>LED_1<br>LED_2<br>LED_3<br>LED_4<br></td><td>Give the status of the given LED . Possible values:<br>\r\n      0 : Turn off LED<br>\r\n      1 : Turn on LED<br>      \r\n      </td></tr>\r\n      <tr>      \r\n      <tr>\r\n      <td>USELED_1<br>USELED_2<br>USELED_3<br>USELED_4<br></td><td>Bit to indicate if we want to override the status of the given LED.\r\n\r\n      Possible values:<br>\r\n      0 : Do not overwrite status of LED<br>\r\n      1 : Overwrite status of LED<br>      \r\n      </td></tr>\r\n      <tr>\r\n      </table>      \r\n\r\n      \r\n      <b>OUTPUT</b><br>\r\n      The function returns one result byte for success or failure. 0 is failure. 1 is success. <br>', 'v0.9', 'Functions', ''),
(70, '0xB2', 'POZYX_TX_DATA', 'F', '2-100', '1', '', 'Write data in the UWB transmit (TX) buffer', 'This function fills the transmit buffer with data bytes. Ready to be transmitted on the next call to <a href="#POZYX_TX_SEND">POZYX_TX_SEND</a>. <br> <br> <b>INPUT PARAMETERS</b><br> This function takes 3 bytes as input parameters.<br><br> <table class=''table table-bordered table-condensed'' [removed]=''min-width:70px''>byte number</th><th>Description</th></tr> <tr><td>byte 0</td> <td>Buffer offset. This value indicates where the offset inside the transmit buffer to start writing the the data bytes. To start from the beginning of the buffer, use the value 0.</td></tr> <tr><td>byte 1</td><td>data byte 0</td></tr> <tr><td>byte 2</td><td>data byte 1</td></tr> <tr><td>...</td><td>...</td></tr> <tr><td>byte 100</td><td>data byte 99</td></tr> </table> <b>OUTPUT</b><br> The function returns one result byte for success or failure. 0 is failure (for example when the data buffer is full). 1 is success. ', 'v0.9', 'Functions', ''),
(71, '0xB3', 'POZYX_TX_SEND', 'F', '3', '1', '', 'Transmit the TX buffer to some other pozyx device', ' This function initiates the wireless transfer of all the data stored in the transmitter buffer (which should be filled by calling <a href="#POZYX_TX_DATA">POZYX_TX_DATA</a>). Upon successful reception of the message by the destination node, the destination node will answer with an acknowledgement (ACK) message. When a REG_READ or a REG_FUNC message was transmitted, the ACK will contain the requested data and the RX_DATA bit is set <a href="#POZYX_INT_STATUS">POZYX_INT_STATUS</a> and an interrupt is fired if the RX_DATA bit is enabled in <a href="#POZYX_INT_MASK">POZYX_INT_MASK</a>. The received ACK data can be read from <a href="#POZYX_RX_DATA">POZYX_RX_DATA</a>. Depending on the UWB settings, it may take a few up to tens of milliseconds before an ACK is to be expected. After sending the data, the transmission buffer is emptied. <br> <br> <b>INPUT PARAMETERS</b><br> This function takes 3 bytes as input parameters.<br><br> <table class=''table table-bordered table-condensed'' style='' width:700px; margin-left: 20px ''> <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr> <tr><td>byte 0</td> <td rowspan=''2''>Network address of destination node.<br>Type: uint16_t</td></tr> <tr><td>byte 1</td></tr> <tr><td>byte 2</td><td>Option byte. Possible values are:<br><br> <table class=''table table-condensed'' > <!--<tr><th style=''min-width:70px''>Value</th><th>Short name</th><th>Description</th></tr>--> <tr> <td>0x02</td><td>REG_READ</td><td>Perform a wireless register read operation. The first databyte in the transmit buffer should be the register address.</td></tr> <td>0x04</td><td>REG_WRITE</td><td>Perform a wireless register write operation. The first databyte in the transmit buffer should be the register address. The sencond by the number of bytes that will be written, the following bytes are the actual data bytes to be written</td></tr> <td>0x08</td><td>REG_FUNC</td><td>Perform a wireless register write operation. The first databyte in the transmit buffer should be the function address, followed by the parameter bytes.</td></tr> <td>0x06</td><td>REG_DATA</td><td>Simply send the data stored in the transmit buffer to the destination. The destination will place the received data in a buffer accessible by the RX_DATA function.</td></tr> </table> </td></tr> </table> <b>OUTPUT</b><br> The function returns one result byte for success or failure. 0 is failure. 1 is success. ', 'v0.9', 'Functions', ''),
(72, '0xB4', 'POZYX_RX_DATA', 'F', '0', '<=100', '', 'Read data from the UWB receive (RX) buffer', 'This function let''s you read from the received data buffer. \r\n      This buffer is filled whenever data is wirelessly received by the UWB receiver.\r\n      Upon this event, the RX_DATA bit is set <a href="#POZYX_INT_STATUS">POZYX_INT_STATUS</a> and \r\n      an interrupt is fired if the RX_DATA bit is enabled in <a href="#POZYX_INT_MASK">POZYX_INT_MASK</a>.\r\n      The RX data buffer is cleared after reading it. When a new data message arrives before the RX buffer\r\n      was read, it will be overwritten and the previously received data will be lost.\r\n      Note that the receiver must be turned on the receive incoming messages. This is done automatically\r\n      whenever an acknowledgment is expected (after a <a href="#POZYX_TX_SEND">POZYX_TX_SEND</a> operation).\r\n      <br>\r\n      <br>\r\n      <b>INPUT PARAMETERS</b><br>\r\n      This function takes 2 bytes as input parameters.<br><br>\r\n\r\n          <table class=''table table-bordered table-condensed descr-table''>\r\n          <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr>\r\n          <tr><td>byte 0</td>\r\n              <td>buffer offset (optional). This value indicates where the offset inside the receive buffer to start reading the \r\n              the data bytes. To start from the beginning of the buffer, use the value 0. Default value = 0.</td></tr>          \r\n          </table>   \r\n                   \r\n\r\n      <b>OUTPUT</b><br>\r\n      This function returns the requested bytes from the receive buffer.<br><br>\r\n      <table class=''table table-bordered table-condensed'' style='' width:700px; margin-left: 20px ''>\r\n          <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr>\r\n          <tr><td>byte 0</td>\r\n              <td>Success byte<br>\r\n                  0 is failure. 1 is success.\r\n              </td></tr>\r\n          <tr><td>byte 1</td><td>data byte 0</td></tr>\r\n          <tr><td>byte 2</td><td>data byte 1</td></tr>\r\n          <tr><td>...</td><td>...</td></tr>\r\n          <tr><td>byte 100</td><td>data byte 99</td></tr>\r\n          </table>', 'v0.9', 'Functions', ''),
(73, '0xB5', 'POZYX_DO_RANGING', 'F', '2', '1', '', 'Initiate ranging measurement', 'This function initiates a ranging operation. When ranging is finished (after about 15ms)  the FUNC bit is set <a href="#POZYX_INT_STATUS">POZYX_INT_STATUS</a> and \r\n      an interrupt is fired if the FUNC bit is enabled in <a href="#POZYX_INT_MASK">POZYX_INT_MASK</a>.\r\n      The range information can be obtained using <a href="#POZYX_DEVICE_GETRANGEINFO">POZYX_DEVICE_GETRANGEINFO</a>.\r\n      The device will be added to the device list if it wasn''t present before.\r\n      <br><br>\r\n\r\n      <b>INPUT PARAMETERS</b><br>\r\n      This function takes 2 bytes of input parameters.<br><br>\r\n\r\n      <table class=''table table-bordered table-condensed'' style='' width:700px; margin-left: 20px ''>\r\n      <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr>\r\n      \r\n      <tr><td>byte 0</td>\r\n      <td rowspan=''2''>Network address of the remote device.<br>Type: uint16_t</td></tr>\r\n      <tr><td>byte 1</td></tr>\r\n      \r\n      </table>\r\n\r\n      <b>OUTPUT</b><br>\r\n      The function returns one result byte for success or failure. 0 is failure. 1 is success.\r\n      <br><br>', 'v0.9', 'Functions', ''),
(74, '0xB6', 'POZYX_DO_POSITIONING', 'F', '0', '1', '', 'Initiate the positioning process. ', 'This function initiates a positioning operation. Calling this function will turn of continuous positioning.\r\n      When positioning is finished (after about 70ms) the POS bit is set <a href="#POZYX_INT_STATUS">POZYX_INT_STATUS</a> and \r\n      an interrupt is fired if the POS bit is enabled in <a href="#POZYX_INT_MASK">POZYX_INT_MASK</a>. The result is stored in the positioning registers starting from <a href="#POZYX_POS_X">POZYX_POS_X</a><br> \r\n      See the functional description for more info on the positioning process.\r\n      <br><br>\r\n\r\n      <b>INPUT PARAMETERS</b><br>\r\n      This function takes 0 bytes of input parameters.<br><br>     \r\n\r\n      <b>OUTPUT</b><br>\r\n      The function returns one result byte for success or failure. 0 is failure. 1 is success.\r\n      <br><br>', 'v0.9', 'Functions', ''),
(75, '0xB7', 'POZYX_POS_SET_ANCHOR_IDS', 'F', '32', '1', '', 'Set the list of anchor ID''s used for positioning. ', 'This function sets the anchor network IDs that can be used for the positioning algorithm. The positioning algorithm will select the first NUM (as given in POZYX_POS_NUM_ANCHORS) anchors from this list only when the MODE (as given in POZYX_POS_NUM_ANCHORS) is set to the fixed anchor set. Note that the actual anchor coordinates must be provided by POZYX_ADD_ANCHOR for each anchor id. <br><br> <b>INPUT PARAMETERS</b><br> This function takes a variable number of input bytes. Note that the total number must be dividable by two.<br><br> <table class=''table table-bordered table-condensed'' style='' width:700px; margin-left: 20px ''> <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr> <tr><td>byte 0</td> <td rowspan=''2''>Network address of 1st anchor node.<br>Type: uint16_t</td></tr> <tr><td>byte 1</td></tr> <tr><td>byte 2</td> <td rowspan=''2''>Network address of 2nd anchor node.<br>Type: uint16_t</td></tr> <tr><td>byte 3</td></tr> <tr><td>byte 4</td> <td rowspan=''2''>Network address of 3th anchor node.<br>Type: uint16_t</td></tr> <tr><td>byte 5</td></tr> <tr><td>byte ..</td><td>...</td></tr> <tr><td>byte 18</td> <td rowspan=''2''>Network address of 10th anchor node.<br>Type: uint16_t</td></tr> <tr><td>byte 19</td></tr> </table> <b>OUTPUT</b><br> The function returns one result byte for success or failure. 0 is failure. 1 is success. ', 'v0.9', 'Functions', ''),
(76, '0xB8', 'POZYX_POS_GET_ANCHOR_IDS', 'F', '0', '33', '', 'Read the list of anchor ID''s used for positioning.', 'This function returns the anchor IDs that are used for the positioning algorithm. When the positioning algorithm is set to the automatic anchor selection mode in (POZYX_POS_NUM_ANCHORS), this list\r\n      will be filled automatically with the anchors chosen by the anchor selection algorithm.<br><br>\r\n\r\n      <b>INPUT PARAMETERS</b><br>\r\n      No input parameters (0 bytes)<br><br>\r\n\r\n      <b>OUTPUT</b><br>\r\n      This function will return up to 20 anchor ID''s.<br><br>\r\n\r\n      <table class=''table table-bordered table-condensed'' style='' width:700px; margin-left: 20px ''>\r\n      <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr>\r\n      <tr><td>byte 0</td><td>Success byte. 0 is failure. 1 is success. </td></tr>\r\n      <tr><td>byte 1</td>\r\n      <td rowspan=''2''>Network address of 1st anchor node.<br>Type: uint16_t</td></tr>\r\n      <tr><td>byte 2</td></tr>\r\n      <tr><td>byte 3</td>\r\n      <td rowspan=''2''>Network address of 2nd anchor node.<br>Type: uint16_t</td></tr>\r\n      <tr><td>byte 4</td></tr>\r\n      <tr><td>byte 5</td>\r\n      <td rowspan=''2''>Network address of 3th anchor node.<br>Type: uint16_t</td></tr>\r\n      <tr><td>byte 6</td></tr>\r\n      <tr><td>byte ..</td><td>...</td></tr>\r\n      <tr><td>byte 19</td>\r\n      <td rowspan=''2''>Network address of 10th anchor node.<br>Type: uint16_t</td></tr>\r\n      <tr><td>byte 20</td></tr>\r\n      </table>', 'v0.9', 'Functions', ''),
(77, '0xB9', 'POZYX_DEVICES_GETIDS', 'F', '0', '<=40', '', 'Get all the network IDs''s of devices in the device list.', 'This function returns the network IDs of all devices in the internal device list.<br><br>\r\n\r\n      <b>INPUT PARAMETERS</b><br>\r\n      No input parameters (0 bytes)<br><br>\r\n\r\n      <b>OUTPUT</b><br>\r\n      This function will return up to 20 device ID''s (40 bytes).<br><br>\r\n\r\n      <table class=''table table-bordered table-condensed'' style='' width:700px; margin-left: 20px ''>\r\n      <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr>\r\n      <tr><td>byte 0</td>\r\n      <td>Return byte indicating success or failure of the function. 0 is failure. 1 is success.                  \r\n      </td></tr>\r\n\r\n      <tr><td>byte 1</td>\r\n      <td rowspan=''2''>Network address of 1st anchor node.<br>Type: uint16_t</td></tr>\r\n      <tr><td>byte 2</td></tr>\r\n      <tr><td>byte 3</td>\r\n      <td rowspan=''2''>Network address of 2nd anchor node.<br>Type: uint16_t</td></tr>\r\n      <tr><td>byte 4</td></tr>\r\n      <tr><td>byte 5</td>\r\n      <td rowspan=''2''>Network address of 3th anchor node.<br>Type: uint16_t</td></tr>\r\n      <tr><td>byte 6</td></tr>\r\n      <tr><td>...</td><td>...</td></tr>\r\n      <tr><td>byte 39</td>\r\n      <td rowspan=''2''>Network address of 20th anchor node.<br>Type: uint16_t</td></tr>\r\n      <tr><td>byte 34</td></tr>\r\n      </table>\r\n\r\n      <br><br>', 'v0.9', 'Device list functions', ''),
(78, '0xBA', 'POZYX_DEVICES_DISCOVER', 'F', '3', '1', '', 'Obtain the network ID''s of all pozyx devices within range.', 'This function performs a discovery operation to identify other pozyx devices within radio range. Newly discovered devices will be added to the internal device list. This process may take several milliseconds.<br><br> <b>INPUT PARAMETERS</b><br> This function takes up to 3 bytes of input parameters. When omitted, the default values are used.<br><br> <table class=''table table-bordered table-condensed'' style='' width:700px; margin-left: 20px ''> <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr> <tr><td>byte 0</td> <td>Discover options (optional). This determines which type of devices should be discovered. The type is given by theoperation mode in <a href="#POZYX_OPERATION_MODE">POZYX_OPERATION_MODE</a>. Possible values:<br> 0x0 : Anchors only <b>(default value)</b><br> 0x1 : Tags only <br> 0x2 : All Pozyx devices </td></tr> <tr><td>byte 1</td> <td>Number of Idle slots (optional). The number of slots to wait for a response of an undiscovered device. If no response was received the discovery process is terminated. The default value is 3 idle slots.<br>Type: uint8_t </td></tr> <tr><td>byte 2</td> <td rowspan=''2''>Idle slot duration (optional). The time duration in milliseconds of the idle slot. Depending on the ultra-wideband settings a shorter or longer slot duration must be chosen. The default value is 10ms.<br>Type: uint8_t </td></tr> </table> <b>OUTPUT</b><br> The function returns one result byte for success or failure. 0 is failure. 1 is success. <br><br>', 'v0.9', 'Device list functions', ''),
(79, '0xBB', 'POZYX_DEVICES_CALIBRATE', 'F', '1', '1', '', 'Obtain the coordinates of the pozyx (anchor) devices within range.', 'This function estimates the relative position of up to 6 pozyx devices within range. This function can be used for quickly setting up the positioning system. This procedure may take several hundres of milliseconds depending on the number of devices in range and the number of range measurements requested. During the calibration proces LED 2 will turned on. At the end of calibration the corresponding bit in the <a href=''#POZYX_CALIB_STATUS''>POZYX_CALIB_STATUS</a> register will be set. Note that only the coordinates of pozyx devices within range are determined. The resulting coordinates are stored in the internal device list. It is advised that during the calibration process, the pozyx tag is not used for other wireless communication. More information can be found here.<br><br> <b>INPUT PARAMETERS</b><br> This function takes a number of optional bytes as input parameters. When omitted, the default values are used. Also, when no network id''s of the anchors are supplied, the anchors will be automatically discovered. <br><br> <table class=''table table-bordered table-condensed'' style='' width:700px; margin-left: 20px ''> <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr> <tr><td>byte 0</td> <td>Calibration options (optional). Possible values:<br> 0x02 : 2D <b>(default)</b>. The relative x and y coordinates of the anchors are estimated. It is expected that all anchors are located in the same 2D plane.<br> 0x01 : 2.5D. The relative x and y coordinates of the anchors are estimated. However it is not expected that all anchors are located in the same 2D plane. For this option to work, the z-coordinates of the anchors must be available in the device list.<br> <!--0x00 : 3D. The relative x, y and z coordinates for the anchors are estimated. It is not possible to select this option if only 4 anchors are available.--> </td></tr> <tr><td>byte 1</td> <td>Number of Measurements (optional). This describes the number of range measurements that should be made for the estimation. Note that a larger number of measurements will increase the accuracy of the relative coordinates, but will also make the process take longer. The default value is 10 measurments. <br>Type: uint8_t </td></tr> <tr><td>byte 2-3</td> <td>Network id anchor 0 (optional). Optionally the network id of the first anchor is given. This anchor will be used to define the origin, i.e., it''s coordinates will be forced to zero. <br>Type: uint16_t </td></tr> <tr><td>byte 4-5</td> <td>Network id anchor 1 (optional). Optionally the network id of the second anchor is given. This anchor will be used to determine the x-axis, i.e., its y coordinate will be forced to zero. <br>Type: uint16_t </td></tr> <tr><td>byte 6-7</td> <td>Network id anchor 2 (optional). Optionally the network id of the third anchor is given. This anchor will be used to determine the which way is up for the y-coordinate, i.e., its y coordinate will be forced to be positive. <br>Type: uint16_t </td></tr> <tr><td>byte 8-9</td> <td>Network id anchor 3 (optional). Optionally the network id of the fourth anchor is given. <br>Type: uint16_t </td></tr> <tr><td>byte 10-11</td> <td>Network id anchor 4 (optional). Optionally the network id of the fifth anchor is given. <br>Type: uint16_t </td></tr> <tr><td>byte 12-13</td> <td>Network id anchor 5 (optional). Optionally the network id of the sixth anchor is given. <br>Type: uint16_t </td></tr> </table> <b>OUTPUT</b><br> The function returns one result byte for success or failure. 0 is failure. 1 is success. <br><br>', 'v0.9', 'Device list functions', ''),
(80, '0xBC', 'POZYX_DEVICES_CLEAR', 'F', '0', '1', '', 'Clear the list of all pozyx devices.', 'This function empties the internal list of devices.<br><br>\r\n\r\n<b>INPUT PARAMETERS</b><br>\r\nNo input parameters (0 bytes)<br><br>\r\n\r\n<b>OUTPUT</b><br>\r\nThe function returns one result byte for success or failure. 0 is failure. 1 is success. ', 'v0.9', 'Device list functions', ''),
(81, '0xBD', 'POZYX_DEVICE_ADD', 'F', '15', '1', '', 'Add a pozyx device to the devices list', ' This function adds a device to the internal list of devices. When the device is already present in the device list, the values will be overwritten. Read more about the internal devices list here. <br><br> <b>INPUT PARAMETERS</b><br> This function takes 15 bytes as input parameters.<br><br> <table class=''table table-bordered table-condensed'' style='' width:700px; margin-left: 20px ''> <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr> <tr><td>byte 0</td> <td rowspan=''2''>Network address of the device.<br>Type: uint16_t</td></tr> <tr><td>byte 1</td></tr> <tr><td>byte 2</td> <td>Special flag describing the device<br>Type: uint8_t</td></tr> <tr><td>byte 3</td> <td rowspan=''4''>x-coordinate of the device<br>Type: int32_t</td></tr> <tr><td>byte 4</td></tr> <tr><td>byte 5</td></tr> <tr><td>byte 6</td></tr> </tr> <tr><td>byte 7</td> <td rowspan=''4''>y-coordinate of the device<br>Type: int32_t</td></tr> <tr><td>byte 8</td></tr> <tr><td>byte 9</td></tr> <tr><td>byte 10</td></tr> </tr> <tr><td>byte 11</td> <td rowspan=''4''>z-coordinate of the device<br>Type: int32_t</td></tr> <tr><td>byte 12</td></tr> <tr><td>byte 13</td></tr> <tr><td>byte 14</td></tr> </table> <b>OUTPUT</b><br> The function returns one result byte for success or failure. 0 is failure. 1 is success. ', 'v0.9', 'Device list functions', ''),
(82, '0xBE', 'POZYX_DEVICE_GETINFO', 'F', '2', '24', '', 'Get the stored device information for a given pozyx device', 'This function returns all the data available about a given pozyx device.<br><br> <b>INPUT PARAMETERS</b><br> This function takes 2 bytes as input parameters.<br><br> <table class=''table table-bordered table-condensed'' style='' width:700px; margin-left: 20px ''> <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr> <tr><td>byte 0</td> <td rowspan=''2''>Network address of pozyx device.<br>Type: uint16_t</td></tr> <tr><td>byte 1</td></tr> </table> <b>OUTPUT</b><br> The function returns 24 bytes of information.<br><br> <table class=''table table-bordered table-condensed'' style='' width:700px; margin-left: 20px ''> <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr> <tr><td>byte 0</td> <td rowspan=''2''>Network address of the device.<br>Type: uint16_t</td></tr> <tr><td>byte 1</td></tr> <tr><td>byte 2</td> <td>Special flag describing the device<br>Type: uint8_t</td></tr> <tr><td>byte 3</td> <td rowspan=''4''>x-coordinate of the device<br>Type: int32_t</td></tr> <tr><td>byte 4</td></tr> <tr><td>byte 5</td></tr> <tr><td>byte 6</td></tr> </tr> <tr><td>byte 7</td> <td rowspan=''4''>y-coordinate of the device<br>Type: int32_t</td></tr> <tr><td>byte 8</td></tr> <tr><td>byte 9</td></tr> <tr><td>byte 10</td></tr> </tr> <tr><td>byte 11</td> <td rowspan=''4''>z-coordinate of the device<br>Type: int32_t</td></tr> <tr><td>byte 12</td></tr> <tr><td>byte 13</td></tr> <tr><td>byte 14</td></tr> <tr><td>byte 15</td> <td rowspan=''4''>Timestamp of last range measurement<br>Type: uint32_t</td></tr> <tr><td>byte 16</td></tr> <tr><td>byte 17</td></tr> <tr><td>byte 18</td></tr> <tr><td>byte 19</td> <td rowspan=''4''>Last range measurement in mm. The resolution of a range measurement is 4.69mm (corresponding to a timing resolution of 15.65ps)<br>Type: uint32_t</td></tr> <tr><td>byte 20</td></tr> <tr><td>byte 21</td></tr> <tr><td>byte 22</td></tr> <tr><td>byte 23</td> <td>Received signal strength value of last range measurement<br>Type: int8_t</td> </tr> </table> <br><br>', 'v0.9', 'Device list functions', ''),
(83, '0xBF', 'POZYX_DEVICE_GETCOORDS', 'F', '2', '12', '', 'Get the stored coordinates of a given pozyx device', 'This function returns the coordinates of a given pozyx device as they are stored in the internal device list.\r\n      The coordinates are either inputted by the user using <a href=''#POZYX_DEVICES_ADD''>POZYX_DEVICES_ADD</a> \r\n      or obtained automatically with <a href=''#POZYX_DEVICES_CALIBRATE''>POZYX_DEVICES_CALIBRATE</a>. \r\n\r\n      <br><br>\r\n\r\n      <b>INPUT PARAMETERS</b><br>\r\n      This function takes 2 bytes as input parameters.<br><br>\r\n          <table class=''table table-bordered table-condensed'' style='' width:700px; margin-left: 20px ''>\r\n          <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr>\r\n          <tr><td>byte 0</td>\r\n          <td rowspan=''2''>Network address of pozyx device.<br>Type: uint16_t</td></tr>\r\n          <tr><td>byte 1</td></tr>          \r\n          </table>      \r\n\r\n      <b>OUTPUT</b><br>\r\n      The function returns 13 bytes of information.<br><br>\r\n\r\n      <table class=''table table-bordered table-condensed'' style='' width:700px; margin-left: 20px ''>\r\n          <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr>\r\n          <tr><td>byte 0</td>\r\n           <td>Return byte indicating success or failure of the function. 0 is failure. 1 is success.                  \r\n           </td></tr>\r\n\r\n          <tr><td>byte 1</td>\r\n          <td rowspan=''4''>x-coordinate of the device<br>Type: int32_t</td></tr>\r\n          <tr><td>byte 2</td></tr>\r\n          <tr><td>byte 3</td></tr>\r\n          <tr><td>byte 4</td></tr>\r\n          </tr>\r\n\r\n          <tr><td>byte 5</td>\r\n          <td rowspan=''4''>y-coordinate of the device<br>Type: int32_t</td></tr>\r\n          <tr><td>byte 6</td></tr>\r\n          <tr><td>byte 7</td></tr>\r\n          <tr><td>byte 8</td></tr>\r\n          </tr>\r\n\r\n          <tr><td>byte 9</td>\r\n          <td rowspan=''4''>z-coordinate of the device<br>Type: int32_t</td></tr>\r\n          <tr><td>byte 10</td></tr>\r\n          <tr><td>byte 11</td></tr>\r\n          <tr><td>byte 12</td></tr>\r\n\r\n      </table>\r\n\r\n      <br><br>', 'v0.9', 'Device list functions', ''),
(84, '0xC0', 'POZYX_DEVICE_GETRANGEINFO', 'F', '2', '9', '', 'Get the stored range inforamation of a given pozyx device', 'This function returns all the latest range information about a given pozyx device.\r\n      This function does not initiate any ranging operation. Range information will be available\r\n      as a result of calling <a href=''#  POZYX_DO_RANGING''>POZYX_DO_RANGING</a> directly or after positioning (where the range measurements with the anchors will be available)\r\n      <br><br>\r\n\r\n      <b>INPUT PARAMETERS</b><br>\r\n      This function takes 2 bytes as input parameters.<br><br>\r\n          <table class=''table table-bordered table-condensed'' style='' width:700px; margin-left: 20px ''>\r\n          <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr>\r\n          <tr><td>byte 0</td>\r\n          <td rowspan=''2''>Network address of pozyx device.<br>Type: uint16_t</td></tr>\r\n          <tr><td>byte 1</td></tr>          \r\n          </table>      \r\n\r\n      <b>OUTPUT</b><br>\r\n      The function returns 11 bytes of information.<br><br>\r\n\r\n      <table class=''table table-bordered table-condensed'' style='' width:700px; margin-left: 20px ''>\r\n          <tr><th style=''min-width:70px''>byte number</th><th>Description</th></tr>\r\n          \r\n          <tr><td>byte 0</td>\r\n          <td>Return byte indicating success or failure of the function. 0 is failure. 1 is success.                  \r\n          </td></tr>\r\n\r\n          <tr><td>byte 1</td>\r\n          <td rowspan=''4''>Timestamp of last range measurement<br>Type: uint32_t</td></tr>\r\n          <tr><td>byte 2</td></tr>\r\n          <tr><td>byte 3</td></tr>\r\n          <tr><td>byte 4</td></tr>\r\n\r\n          <tr><td>byte 5</td>\r\n          <td rowspan=''4''>Last range measurement in mm. The resolution of \r\n          a range measurement is 4.69mm (corresponding to a timing resolution of 15.65ps)\r\n          <br>Type: uint32_t</td></tr>\r\n          <tr><td>byte 6</td></tr>\r\n          <tr><td>byte 7</td></tr>\r\n          <tr><td>byte 8</td></tr>\r\n\r\n          <tr><td>byte 9</td>\r\n          <td rowspan=''2''>Received signal strength value of last range measurement<br>Type: int16_t</td>\r\n          <td>byte 10</td>\r\n          </tr> \r\n         \r\n      </table>\r\n\r\n      <br><br>', 'v0.9', 'Device list functions', ''),
(85, '0xC1', 'POZYX_CIR_DATA', 'F', '3', '<=40', '', 'Get the channel impulse response (CIR) coefficients', 'This function returns the channel impulse response (CIR) of the last received ultra-wideband message.\r\n      The CIR can be used for diagnostic purposes, or to run custom timing algorithms.\r\n      Using the default PRF of 64MHz, a total of 1016 complex coefficients are available. For a PRF of \r\n      16MHz, 996 complex coefficients are available. Every complex coefficient is represented by 4 bytes\r\n      (2 for the real part and 2 for the imaginary part). The coefficients are taken at an interval of 1.0016ns, \r\n      or more precisely, at half the period of a 499.2MHz sampling frequency.\r\n      <br>\r\n      <br>\r\n      <b>INPUT PARAMETERS</b><br>\r\n      This function takes 3 bytes as input parameters.<br><br>\r\n\r\n          <table class=''table table-bordered table-condensed descr-table''>\r\n          <tr><th>byte number</th><th>Description</th></tr>\r\n          <tr><td>byte 0-1</td>\r\n              <td>CIR buffer offset. This value indicates where the offset inside the CIR buffer to start reading the \r\n              the data bytes. Possible values range between 0 and 1015.\r\n              <br>\r\n              Type: uint16_t</td></tr>  \r\n              <tr><td>byte 2</td><td>Data length. The number of coefficients to read from the CIR buffer. Possible values\r\n              range between 1 and 49.</td></tr>\r\n          </table>   \r\n                   \r\n\r\n      <b>OUTPUT</b><br>\r\n      This function returns the requested bytes from the CIR buffer.<br><br>\r\n      <table class=''table table-bordered table-condensed descr-table''>\r\n          <tr><th>byte number</th><th>Description</th></tr>\r\n          <tr><td>byte 0</td>\r\n              <td>Return byte indicating success or failure of the function. 0 is failure. 1 is success. \r\n                  \r\n              </td></tr>\r\n          \r\n          <tr><td>byte 1-2</td><td>CIR coefficient 0+offset (real value).<br>Type: int16. </td></tr>\r\n          <tr><td>byte 3-4</td><td>CIR coefficient 0+offset (imaginary value).<br>Type: int16. </td></tr>\r\n          <tr><td>byte 5-6</td><td>CIR coefficient 1+offset (real value).<br>Type: int16. </td></tr>\r\n          <tr><td>byte 7-8</td><td>CIR coefficient 1+offset (imaginary value).<br>Type: int16. </td></tr>\r\n\r\n          <tr><td>...</td><td>...</td></tr>\r\n          <tr><td>byte 95-96</td><td>CIR coefficient 48+offset (real value).<br>Type: int16. </td></tr>\r\n          <tr><td>byte 97-98</td><td>CIR coefficient 48+offset (imaginary value).<br>Type: int16. </td></tr>\r\n         \r\n          </table>   ', 'v0.9', 'Device list functions', ''),
(86, '0xE0', 'POZYX_GET_RXDIAGNOS', 'F', '0', '19', '', 'Get more diagnostic data', '', 'internal', 'Device list functions', ''),
(87, '0xFA', 'POZYX_TDMA_START_SUPERFRAME', 'F', '0', '1', '', 'Start a TDMA superframe', 'This function immediately iniates a TDMA superframe. The number of slots and duration of each slot is configured in the registers <a href="#POZYX_TDMA_NUMSLOTS">POZYX_TDMA_NUMSLOTS</a> and <a href="#POZYX_TDMA_SLOTDURATION_MS">POZYX_TDMA_SLOTDURATION_MS</a>, respectively.<br><br> <b>INPUT PARAMETERS</b><br> None.<br><br> <b>OUTPUT</b><br> The function returns one result byte for success or failure. 0 is failure. 1 is success. <br><br>', 'internal', 'Device list functions', ''),
(88, '0xFB', 'POZYX_TDMA_SCHEDULE_RANGING', 'F', '4', '1', '', 'Schedule a ranging operation', 'This function schedules a ranging operation at a given timeslot with a given network address. Whenever the range measurement is finished, an interrupt will be triggered and the range data can be read out using <a href="#POZYX_DEVICE_GETRANGEINFO">POZYX_DEVICE_GETRANGEINFO</a> as normal. <br> <br> <b>INPUT PARAMETERS</b><br> This function takes 4 bytes as input parameters.<br><br> <table class=''table table-bordered table-condensed''>byte number</th><th>Description</th></tr> <tr><td>byte 0</td><td rowspan=''2''>Slot number. Start with the number 0. 499 is the maximum value. <br>Type: uint16_t</td></tr> <tr><td>byte 1</td></tr> <tr><td>byte 2</td> <td rowspan=''2''>Network address of destination node.<br>Type: uint16_t</td></tr> <tr><td>byte 3</td></tr> </table> <b>OUTPUT</b><br> The function returns one result byte for success or failure. 0 is failure. 1 is success. <br><br>', 'internal', 'Device list functions', ''),
(89, '0xC5', 'POZYX_FLASH_STORE', 'F', '1', '1', '', 'Store a portion of the configuration in flash memory', '', 'v1.0', '', ''),
(90, '0xC6', 'POZYX_FLASH_RESET', 'F', '1', '1', 'POZYX_FLASH_ERASE', 'Reset a portion of the configuration in flash memory', 'Reset a portion of the configuration in flash memory to the factory defaults', 'v1.0', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tasklist`
--

CREATE TABLE IF NOT EXISTS `tasklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `status` enum('open','completed') NOT NULL DEFAULT 'open',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;


--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$08$E48ElShBS/kNgEhWizXo0ekVPJKSvxVPKdGSAoQ6yXjY20bidZFi6', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1438273780, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(2, '127.0.0.1', 'vadim vermeiren', '$2a$08$lxeBvShGOLY1fD2NRePM3eLbBeORisBg1ct2EKkcafyOfdg/rLJvO', NULL, 'vadim@pozyx.io', NULL, NULL, NULL, NULL, 1438274431, 1470029094, 1, 'Vadim', 'Vermeiren', 'Pozyx', '12345678'),
(3, '127.0.0.1', 'samuel van de velde', '$2a$08$BX9mJBeERSzR2H/EaEhZWOiQi5cObUPNMoYvVelT.9F3rdmbikLLi', NULL, 'samuel@pozyx.io', NULL, NULL, NULL, 'r7tGJeBnP4Jb/cWWxox.5.', 1438274463, 1470029886, 1, 'Samuel', 'Van de Velde', 'Pozyx', '12345678');
-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(5, 2, 1),
(6, 2, 2),
(7, 3, 1),
(8, 3, 2),
(10, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `vat_text`
--

CREATE TABLE IF NOT EXISTS `vat_text` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vat_code` enum('DOM','EU','NEU') NOT NULL DEFAULT 'NEU',
  `vat_type` enum('service','goods') NOT NULL DEFAULT 'goods',
  `vat_text` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `vat_text`
--

INSERT INTO `vat_text` (`id`, `vat_code`, `vat_type`, `vat_text`) VALUES
(3, 'EU', 'goods', 'Intra-community supply of goods exempt from Belgian VAT art. 138, sub1 of the European VAT Directive'),
(4, 'EU', 'service', 'Tax free intra-community delivery: Reverse charge: VAT to be accounted for by the customer'),
(5, 'NEU', 'goods', 'Supply of goods not subject to Belgian VAT art. 32, par1 of the European VAT Directive'),
(6, 'NEU', 'service', 'Exempt from BE VAT article 44 of the European VAT directive');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
